
/************ Update: Tables ***************/

/******************** Add Table: dbo.Profesor ************************/

/* Build Table Structure */
CREATE TABLE dbo.Profesor
(
	GuidProfesor UniqueIdentifier NOT NULL,
	FullName VARCHAR(255) NULL,
	GuidAcademyLevel UniqueIdentifier NULL
);

/* Add Primary Key */
ALTER TABLE dbo.Profesor ADD CONSTRAINT pkProfesor
	PRIMARY KEY (GuidProfesor);


/******************** Update Table: Student ************************/

ALTER TABLE dbo.Student ADD GuidProfesor UniqueIdentifier NULL;





/************ Add Foreign Keys ***************/

/* Add Foreign Key: fk_Profesor_AcademyLevel */
ALTER TABLE dbo.Profesor ADD CONSTRAINT fk_Profesor_AcademyLevel
	FOREIGN KEY (GuidAcademyLevel) REFERENCES dbo.AcademyLevel (GuidAcademyLevel)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_Student_Profesor */
ALTER TABLE dbo.Student ADD CONSTRAINT fk_Student_Profesor
	FOREIGN KEY (GuidProfesor) REFERENCES dbo.Profesor (GuidProfesor)
	ON UPDATE NO ACTION ON DELETE NO ACTION;