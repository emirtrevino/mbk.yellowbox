

/************ Update: Tables ***************/

/******************** Add Table: dbo.AcademyLevel ************************/

/* Build Table Structure */
CREATE TABLE dbo.AcademyLevel
(
	GuidAcademyLevel UniqueIdentifier NOT NULL,
	Title VARCHAR(255) NULL
);

/* Add Primary Key */
ALTER TABLE dbo.AcademyLevel ADD CONSTRAINT pkAcademyLevel
	PRIMARY KEY (GuidAcademyLevel);


/******************** Update Table: Student ************************/

ALTER TABLE dbo.Student ADD Comments VARCHAR(5000) NULL;

ALTER TABLE dbo.Student ADD GuidAcademyLevel UniqueIdentifier NULL;





/************ Add Foreign Keys ***************/

/* Add Foreign Key: fk_Student_AcademyLevel */
ALTER TABLE dbo.Student ADD CONSTRAINT fk_Student_AcademyLevel
	FOREIGN KEY (GuidAcademyLevel) REFERENCES dbo.AcademyLevel (GuidAcademyLevel)
	ON UPDATE NO ACTION ON DELETE NO ACTION;