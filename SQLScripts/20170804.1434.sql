

/************ Remove Foreign Keys ***************/

ALTER TABLE dbo.Student DROP CONSTRAINT fk_Student_Parent;



/************ Update: Tables ***************/

/******************** Update Table: StudentParent ************************/

/* Remove Primary Key */
ALTER TABLE dbo.StudentParent DROP CONSTRAINT pkParent;

/* Rename: dbo.Parent */
EXEC sp_rename 'dbo.Parent', 'StudentParent';

EXEC sp_rename 'dbo.StudentParent.GuidParent', 'GuidStudentParent','COLUMN';

/* Add Primary Key */
ALTER TABLE dbo.StudentParent ADD CONSTRAINT pkStudentParent
	PRIMARY KEY (GuidStudentParent);





/************ Add Foreign Keys ***************/

/* Add Foreign Key: fk_Student_Parent */
ALTER TABLE dbo.Student ADD CONSTRAINT fk_Student_Parent
	FOREIGN KEY (GuidParent) REFERENCES dbo.StudentParent (GuidStudentParent)
	ON UPDATE NO ACTION ON DELETE NO ACTION;