

/************ Remove Foreign Keys ***************/

ALTER TABLE dbo.Location DROP CONSTRAINT fk_Location_LocationType;

ALTER TABLE dbo.RouteLocation DROP CONSTRAINT fk_RouteLocation_Location;

ALTER TABLE dbo.StudentLocation DROP CONSTRAINT fk_StudentLocation_Location;



/************ Update: Tables ***************/

/******************** Update Table: YBLocation ************************/

/* Rename: dbo.Location */
EXEC sp_rename 'dbo.Location', 'YBLocation';





/************ Add Foreign Keys ***************/

/* Add Foreign Key: fk_RouteLocation_Location */
ALTER TABLE dbo.RouteLocation ADD CONSTRAINT fk_RouteLocation_Location
	FOREIGN KEY (GuidLocation) REFERENCES dbo.YBLocation (GuidLocation)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_StudentLocation_Location */
ALTER TABLE dbo.StudentLocation ADD CONSTRAINT fk_StudentLocation_Location
	FOREIGN KEY (GuidLocation) REFERENCES dbo.YBLocation (GuidLocation)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_Location_LocationType */
ALTER TABLE dbo.YBLocation ADD CONSTRAINT fk_Location_LocationType
	FOREIGN KEY (GuidLocationType) REFERENCES dbo.LocationType (GuidLocationType)
	ON UPDATE NO ACTION ON DELETE NO ACTION;


--------------------------------------------------


/************ Remove Foreign Keys ***************/

ALTER TABLE dbo.RouteLocation DROP CONSTRAINT fk_RouteLocation_Route;

ALTER TABLE dbo.Transport DROP CONSTRAINT fk_Transport_Route;



/************ Update: Tables ***************/

/******************** Update Table: YBRoute ************************/

/* Rename: dbo.Route */
EXEC sp_rename 'dbo.Route', 'YBRoute';





/************ Add Foreign Keys ***************/

/* Add Foreign Key: fk_RouteLocation_Route */
ALTER TABLE dbo.RouteLocation ADD CONSTRAINT fk_RouteLocation_Route
	FOREIGN KEY (GuidRoute) REFERENCES dbo.YBRoute (GuidRoute)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_Transport_Route */
ALTER TABLE dbo.Transport ADD CONSTRAINT fk_Transport_Route
	FOREIGN KEY (GuidRoute) REFERENCES dbo.YBRoute (GuidRoute)
	ON UPDATE NO ACTION ON DELETE NO ACTION;