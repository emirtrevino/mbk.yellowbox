

/************ Update: Tables ***************/

/******************** Add Table: dbo.Location ************************/

/* Build Table Structure */
CREATE TABLE dbo.Location
(
	GuidLocation UniqueIdentifier NOT NULL,
	GuidLocationType UniqueIdentifier NULL,
	Lat FLOAT NULL,
	Long FLOAT NULL,
	Description VARCHAR(255) NULL,
	Address VARCHAR(255) NULL
);

/* Add Primary Key */
ALTER TABLE dbo.Location ADD CONSTRAINT pkLocation
	PRIMARY KEY (GuidLocation);


/******************** Add Table: dbo.LocationType ************************/

/* Build Table Structure */
CREATE TABLE dbo.LocationType
(
	GuidLocationType UniqueIdentifier NOT NULL,
	Name VARCHAR(255) NULL,
	NameKey VARCHAR(20) NULL
);

/* Add Primary Key */
ALTER TABLE dbo.LocationType ADD CONSTRAINT pkLocationType
	PRIMARY KEY (GuidLocationType);


/******************** Add Table: dbo.Parent ************************/

/* Build Table Structure */
CREATE TABLE dbo.Parent
(
	GuidParent UniqueIdentifier NOT NULL,
	FullName VARCHAR(255) NULL
);

/* Add Primary Key */
ALTER TABLE dbo.Parent ADD CONSTRAINT pkParent
	PRIMARY KEY (GuidParent);


/******************** Add Table: dbo.Route ************************/

/* Build Table Structure */
CREATE TABLE dbo.Route
(
	GuidRoute UniqueIdentifier NOT NULL,
	Title VARCHAR(255) NULL
);

/* Add Primary Key */
ALTER TABLE dbo.Route ADD CONSTRAINT pkRoute
	PRIMARY KEY (GuidRoute);


/******************** Add Table: dbo.RouteLocation ************************/

/* Build Table Structure */
CREATE TABLE dbo.RouteLocation
(
	GuidRouteLocation UniqueIdentifier NOT NULL,
	GuidRoute UniqueIdentifier NULL,
	GuidLocation UniqueIdentifier NULL,
	OrderRoute INTEGER NULL,
	GuidStudent UniqueIdentifier NULL
);

/* Add Primary Key */
ALTER TABLE dbo.RouteLocation ADD CONSTRAINT pkRouteLocation
	PRIMARY KEY (GuidRouteLocation);


/******************** Add Table: dbo.Student ************************/

/* Build Table Structure */
CREATE TABLE dbo.Student
(
	GuidStudent UniqueIdentifier NOT NULL,
	FullName VARCHAR(255) NULL,
	BirthDate DATETIME NULL,
	Grade INTEGER NULL,
	[Group] VARCHAR(2) NULL,
	GuidParent UniqueIdentifier NULL
);

/* Add Primary Key */
ALTER TABLE dbo.Student ADD CONSTRAINT pkStudent
	PRIMARY KEY (GuidStudent);


/******************** Add Table: dbo.StudentLocation ************************/

/* Build Table Structure */
CREATE TABLE dbo.StudentLocation
(
	GuidStudentLocation UniqueIdentifier NOT NULL,
	GuidLocation UniqueIdentifier NULL,
	GuidStudent UniqueIdentifier NULL
);

/* Add Primary Key */
ALTER TABLE dbo.StudentLocation ADD CONSTRAINT pkStudentLocation
	PRIMARY KEY (GuidStudentLocation);


/******************** Add Table: dbo.Transport ************************/

/* Build Table Structure */
CREATE TABLE dbo.Transport
(
	GuidTrasport UniqueIdentifier NOT NULL,
	TransportCode VARCHAR(10) NULL,
	Description VARCHAR(255) NULL,
	Capacity INTEGER NULL,
	GuidRoute UniqueIdentifier NULL
);

/* Add Primary Key */
ALTER TABLE dbo.Transport ADD CONSTRAINT pkTransport
	PRIMARY KEY (GuidTrasport);





/************ Add Foreign Keys ***************/

/* Add Foreign Key: fk_Location_LocationType */
ALTER TABLE dbo.Location ADD CONSTRAINT fk_Location_LocationType
	FOREIGN KEY (GuidLocationType) REFERENCES dbo.LocationType (GuidLocationType)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_RouteLocation_Location */
ALTER TABLE dbo.RouteLocation ADD CONSTRAINT fk_RouteLocation_Location
	FOREIGN KEY (GuidLocation) REFERENCES dbo.Location (GuidLocation)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_RouteLocation_Route */
ALTER TABLE dbo.RouteLocation ADD CONSTRAINT fk_RouteLocation_Route
	FOREIGN KEY (GuidRoute) REFERENCES dbo.Route (GuidRoute)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_RouteLocation_Student */
ALTER TABLE dbo.RouteLocation ADD CONSTRAINT fk_RouteLocation_Student
	FOREIGN KEY (GuidStudent) REFERENCES dbo.Student (GuidStudent)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_Student_Parent */
ALTER TABLE dbo.Student ADD CONSTRAINT fk_Student_Parent
	FOREIGN KEY (GuidParent) REFERENCES dbo.Parent (GuidParent)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_StudentLocation_Location */
ALTER TABLE dbo.StudentLocation ADD CONSTRAINT fk_StudentLocation_Location
	FOREIGN KEY (GuidLocation) REFERENCES dbo.Location (GuidLocation)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_StudentLocation_Student */
ALTER TABLE dbo.StudentLocation ADD CONSTRAINT fk_StudentLocation_Student
	FOREIGN KEY (GuidStudent) REFERENCES dbo.Student (GuidStudent)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_Transport_Route */
ALTER TABLE dbo.Transport ADD CONSTRAINT fk_Transport_Route
	FOREIGN KEY (GuidRoute) REFERENCES dbo.Route (GuidRoute)
	ON UPDATE NO ACTION ON DELETE NO ACTION;