
 
 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcSiteMapProvider;
using MBK.YellowBox.Web.Mvc.Resources;
using SFSdotNet.Framework.Web.Mvc.Resources;


namespace MBK.YellowBox.Web.Mvc
{
    public partial class DynamicNodeProvider : DynamicNodeProviderBase
    {
        partial void OnCreatingNodes(object sender, ref List<DynamicNode> nodes);
        partial void OnCreatedNodes(object sender, ref List<DynamicNode> nodes);
   
       public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode startNode)
        {
            List<DynamicNode> nodes = new List<DynamicNode>();
            DynamicNode node = null;
             SFSdotNet.Framework.Globalization.TextUI textUI = new SFSdotNet.Framework.Globalization.TextUI("MBKYellowBox", null);

			node = new DynamicNode();
            node.Title = ModuleResources.MODULE_NAME;
			
            node.Controller = "Navigation";
            node.Area = "";
            node.Action = "Index";
            node.Key = "MBKYellowBox";
			node.RouteValues.Add("id", node.Key);
			node.RouteValues.Add("overrideModule", "MBKYellowBox");

 			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("permissionKey", "r");    
 
 
			textUI.SetTextTo(node, "Title", typeof(ModuleResources), "MODULE_NAME");
                   
            nodes.Add(node);

			
            node = new DynamicNode();
            node.Title = ModuleResources.CATALOGS;
			  
            node.Controller = "Navigation";
            node.Area = "";
            node.Action = "Index";
            node.Key = "MBKYellowBox_Catalogs";
			node.RouteValues.Add("id", node.Key);
			node.RouteValues.Add("overrideModule", "MBKYellowBox");

			node.Attributes.Add("moduleKey", "MBKYellowBox");
			node.ParentKey = "MBKYellowBox";
			 textUI.SetTextTo(node, "Title", typeof(ModuleResources), "CATALOGS");
         
            nodes.Add(node);
			
			
          /*  node = new DynamicNode();
            node.Title = ModuleResources.all_catalogs;
			 
            node.Controller = "Navigation";
            node.Area = "";
            node.Action = "Index";
            node.Key = "MBKYellowBox_all_catalogs";
			node.RouteValues.Add("id", node.Key);
			node.Attributes.Add("moduleKey", "MBKYellowBox");
			node.RouteValues.Add("overrideModule", "MBKYellowBox");

			node.ParentKey = "MBKYellowBox_Catalogs";
			 textUI.SetTextTo(node, "Title", typeof(ModuleResources), "all_catalogs");
          
            nodes.Add(node);*/


            node = new DynamicNode();
            //node.Title = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.USERS_AND_ROLES ;
            node.Controller = "secBusinessObjects";
            node.Area = "SFSdotNetFrameworkSecurity";
            node.Action = "Index";
            node.Key = "MBKYellowBox_all_catalogs";
            node.ParentKey = "MBKYellowBox_Catalogs";
            node.Attributes.Add("moduleKey", "SFSdotNetFrameworkSecurity");
            node.RouteValues.Add("overrideModule", "MBKYellowBox");
            node.RouteValues.Add("usemode", "all-catalogs");

            //node.Attributes.Add("permissionKey", "admin");
            textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "ALL_CATALOGS");

            nodes.Add(node);

            
			//node for contents
			node = new DynamicNode();
            node.Title = ModuleResources.MODULE_NAME;
		  
            node.Controller = "Home";
            node.Area = "";
            node.Action = "App";
            node.Key = "MBKYellowBox_home_app_c";
            node.RouteValues.Add("id", "MBKYellowBox");
			node.Attributes.Add("moduleKey", "MBKYellowBox");
			node.RouteValues.Add("overrideModule", "MBKYellowBox");

            node.ParentKey = "allapps";
				 textUI.SetTextTo(node, "Title", typeof(ModuleResources), "MODULE_NAME");
         
            nodes.Add(node);

			
		    OnCreatingNodes(this, ref nodes);

           List<SFSdotNet.Framework.Globalization.UITexts> entityTexts =   textUI.GetItems("en");
           string single = "";
           string plural = "";
           SFSdotNet.Framework.Globalization.UITexts entityText = null ;
	

			#region LocationType
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "LocationType");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "LocationTypes";
		
		       node.Controller = "LocationTypes";
            node.Area = "MBKYellowBox";
            node.Action = "Index";
            node.Key = "MBKYellowBox_LocationType_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKYellowBox_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "LocationType");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "LocationTypes";
            node.Area = "MBKYellowBox";
            node.Action = "CreateGen";
            node.Key = "MBKYellowBox_LocationType_Create";
			node.ParentKey = "MBKYellowBox_LocationType_List";
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "LocationType");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = LocationTypeResources.LOCATIONTYPES_EDIT;
            //node.Controller = "LocationTypes";
            //node.Area = "MBKYellowBox";
            //node.Action = "EditGen";
            //node.Key = "MBKYellowBox_LocationType_Edit";
			//node.ParentKey = "MBKYellowBox_LocationType_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "LocationType";
            node.Controller = "LocationTypes";
            node.Area = "MBKYellowBox";
            node.Action = "DetailsGen";
            node.Key = "MBKYellowBox_LocationType_Details";
			node.ParentKey = "MBKYellowBox_LocationType_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "LocationType");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region RouteLocation
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "RouteLocation");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "RouteLocations";
		
		       node.Controller = "RouteLocations";
            node.Area = "MBKYellowBox";
            node.Action = "Index";
            node.Key = "MBKYellowBox_RouteLocation_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKYellowBox_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "RouteLocation");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "RouteLocations";
            node.Area = "MBKYellowBox";
            node.Action = "CreateGen";
            node.Key = "MBKYellowBox_RouteLocation_Create";
			node.ParentKey = "MBKYellowBox_RouteLocation_List";
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "RouteLocation");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = RouteLocationResources.ROUTELOCATIONS_EDIT;
            //node.Controller = "RouteLocations";
            //node.Area = "MBKYellowBox";
            //node.Action = "EditGen";
            //node.Key = "MBKYellowBox_RouteLocation_Edit";
			//node.ParentKey = "MBKYellowBox_RouteLocation_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "RouteLocation";
            node.Controller = "RouteLocations";
            node.Area = "MBKYellowBox";
            node.Action = "DetailsGen";
            node.Key = "MBKYellowBox_RouteLocation_Details";
			node.ParentKey = "MBKYellowBox_RouteLocation_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "RouteLocation");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region Student
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "Student");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "Students";
		
		       node.Controller = "Students";
            node.Area = "MBKYellowBox";
            node.Action = "Index";
            node.Key = "MBKYellowBox_Student_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKYellowBox_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "Student");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "Students";
            node.Area = "MBKYellowBox";
            node.Action = "CreateGen";
            node.Key = "MBKYellowBox_Student_Create";
			node.ParentKey = "MBKYellowBox_Student_List";
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "Student");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = StudentResources.STUDENTS_EDIT;
            //node.Controller = "Students";
            //node.Area = "MBKYellowBox";
            //node.Action = "EditGen";
            //node.Key = "MBKYellowBox_Student_Edit";
			//node.ParentKey = "MBKYellowBox_Student_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "Student";
            node.Controller = "Students";
            node.Area = "MBKYellowBox";
            node.Action = "DetailsGen";
            node.Key = "MBKYellowBox_Student_Details";
			node.ParentKey = "MBKYellowBox_Student_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "Student");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region StudentLocation
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "StudentLocation");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "StudentLocations";
		
		       node.Controller = "StudentLocations";
            node.Area = "MBKYellowBox";
            node.Action = "Index";
            node.Key = "MBKYellowBox_StudentLocation_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKYellowBox_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "StudentLocation");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "StudentLocations";
            node.Area = "MBKYellowBox";
            node.Action = "CreateGen";
            node.Key = "MBKYellowBox_StudentLocation_Create";
			node.ParentKey = "MBKYellowBox_StudentLocation_List";
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "StudentLocation");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = StudentLocationResources.STUDENTLOCATIONS_EDIT;
            //node.Controller = "StudentLocations";
            //node.Area = "MBKYellowBox";
            //node.Action = "EditGen";
            //node.Key = "MBKYellowBox_StudentLocation_Edit";
			//node.ParentKey = "MBKYellowBox_StudentLocation_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "StudentLocation";
            node.Controller = "StudentLocations";
            node.Area = "MBKYellowBox";
            node.Action = "DetailsGen";
            node.Key = "MBKYellowBox_StudentLocation_Details";
			node.ParentKey = "MBKYellowBox_StudentLocation_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "StudentLocation");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region Transport
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "Transport");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "Transports";
		
		       node.Controller = "Transports";
            node.Area = "MBKYellowBox";
            node.Action = "Index";
            node.Key = "MBKYellowBox_Transport_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKYellowBox_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "Transport");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "Transports";
            node.Area = "MBKYellowBox";
            node.Action = "CreateGen";
            node.Key = "MBKYellowBox_Transport_Create";
			node.ParentKey = "MBKYellowBox_Transport_List";
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "Transport");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = TransportResources.TRANSPORTS_EDIT;
            //node.Controller = "Transports";
            //node.Area = "MBKYellowBox";
            //node.Action = "EditGen";
            //node.Key = "MBKYellowBox_Transport_Edit";
			//node.ParentKey = "MBKYellowBox_Transport_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "Transport";
            node.Controller = "Transports";
            node.Area = "MBKYellowBox";
            node.Action = "DetailsGen";
            node.Key = "MBKYellowBox_Transport_Details";
			node.ParentKey = "MBKYellowBox_Transport_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "Transport");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region YBLocation
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "YBLocation");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "YBLocations";
		
		       node.Controller = "YBLocations";
            node.Area = "MBKYellowBox";
            node.Action = "Index";
            node.Key = "MBKYellowBox_YBLocation_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKYellowBox_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "YBLocation");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "YBLocations";
            node.Area = "MBKYellowBox";
            node.Action = "CreateGen";
            node.Key = "MBKYellowBox_YBLocation_Create";
			node.ParentKey = "MBKYellowBox_YBLocation_List";
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "YBLocation");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = YBLocationResources.YBLOCATIONS_EDIT;
            //node.Controller = "YBLocations";
            //node.Area = "MBKYellowBox";
            //node.Action = "EditGen";
            //node.Key = "MBKYellowBox_YBLocation_Edit";
			//node.ParentKey = "MBKYellowBox_YBLocation_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "YBLocation";
            node.Controller = "YBLocations";
            node.Area = "MBKYellowBox";
            node.Action = "DetailsGen";
            node.Key = "MBKYellowBox_YBLocation_Details";
			node.ParentKey = "MBKYellowBox_YBLocation_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "YBLocation");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region YBRoute
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "YBRoute");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "YBRoutes";
		
		       node.Controller = "YBRoutes";
            node.Area = "MBKYellowBox";
            node.Action = "Index";
            node.Key = "MBKYellowBox_YBRoute_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKYellowBox_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "YBRoute");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "YBRoutes";
            node.Area = "MBKYellowBox";
            node.Action = "CreateGen";
            node.Key = "MBKYellowBox_YBRoute_Create";
			node.ParentKey = "MBKYellowBox_YBRoute_List";
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "YBRoute");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = YBRouteResources.YBROUTES_EDIT;
            //node.Controller = "YBRoutes";
            //node.Area = "MBKYellowBox";
            //node.Action = "EditGen";
            //node.Key = "MBKYellowBox_YBRoute_Edit";
			//node.ParentKey = "MBKYellowBox_YBRoute_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "YBRoute";
            node.Controller = "YBRoutes";
            node.Area = "MBKYellowBox";
            node.Action = "DetailsGen";
            node.Key = "MBKYellowBox_YBRoute_Details";
			node.ParentKey = "MBKYellowBox_YBRoute_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "YBRoute");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region StudentParent
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "StudentParent");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "StudentParents";
		
		       node.Controller = "StudentParents";
            node.Area = "MBKYellowBox";
            node.Action = "Index";
            node.Key = "MBKYellowBox_StudentParent_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKYellowBox_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "StudentParent");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "StudentParents";
            node.Area = "MBKYellowBox";
            node.Action = "CreateGen";
            node.Key = "MBKYellowBox_StudentParent_Create";
			node.ParentKey = "MBKYellowBox_StudentParent_List";
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "StudentParent");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = StudentParentResources.STUDENTPARENTS_EDIT;
            //node.Controller = "StudentParents";
            //node.Area = "MBKYellowBox";
            //node.Action = "EditGen";
            //node.Key = "MBKYellowBox_StudentParent_Edit";
			//node.ParentKey = "MBKYellowBox_StudentParent_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "StudentParent";
            node.Controller = "StudentParents";
            node.Area = "MBKYellowBox";
            node.Action = "DetailsGen";
            node.Key = "MBKYellowBox_StudentParent_Details";
			node.ParentKey = "MBKYellowBox_StudentParent_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "StudentParent");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region AcademyLevel
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "AcademyLevel");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "AcademyLevels";
		
		       node.Controller = "AcademyLevels";
            node.Area = "MBKYellowBox";
            node.Action = "Index";
            node.Key = "MBKYellowBox_AcademyLevel_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKYellowBox_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "AcademyLevel");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "AcademyLevels";
            node.Area = "MBKYellowBox";
            node.Action = "CreateGen";
            node.Key = "MBKYellowBox_AcademyLevel_Create";
			node.ParentKey = "MBKYellowBox_AcademyLevel_List";
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "AcademyLevel");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = AcademyLevelResources.ACADEMYLEVELS_EDIT;
            //node.Controller = "AcademyLevels";
            //node.Area = "MBKYellowBox";
            //node.Action = "EditGen";
            //node.Key = "MBKYellowBox_AcademyLevel_Edit";
			//node.ParentKey = "MBKYellowBox_AcademyLevel_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "AcademyLevel";
            node.Controller = "AcademyLevels";
            node.Area = "MBKYellowBox";
            node.Action = "DetailsGen";
            node.Key = "MBKYellowBox_AcademyLevel_Details";
			node.ParentKey = "MBKYellowBox_AcademyLevel_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "AcademyLevel");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region Profesor
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "Profesor");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "Profesors";
		
		       node.Controller = "Profesors";
            node.Area = "MBKYellowBox";
            node.Action = "Index";
            node.Key = "MBKYellowBox_Profesor_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKYellowBox_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "Profesor");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "Profesors";
            node.Area = "MBKYellowBox";
            node.Action = "CreateGen";
            node.Key = "MBKYellowBox_Profesor_Create";
			node.ParentKey = "MBKYellowBox_Profesor_List";
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "Profesor");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProfesorResources.PROFESORS_EDIT;
            //node.Controller = "Profesors";
            //node.Area = "MBKYellowBox";
            //node.Action = "EditGen";
            //node.Key = "MBKYellowBox_Profesor_Edit";
			//node.ParentKey = "MBKYellowBox_Profesor_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "Profesor";
            node.Controller = "Profesors";
            node.Area = "MBKYellowBox";
            node.Action = "DetailsGen";
            node.Key = "MBKYellowBox_Profesor_Details";
			node.ParentKey = "MBKYellowBox_Profesor_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.Attributes.Add("businessObjectKey", "Profesor");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion

 			OnCreatedNodes(this, ref nodes);
			
			node = new DynamicNode();
            //node.Title = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.SYSTEM;
            node.Controller = "Navigation";
            node.Area = "";
            node.Action = "Index";
            node.Key = "MBKYellowBox_System_override";
            node.RouteValues.Add("id", node.Key);
            node.RouteValues.Add("overrideModule", "MBKYellowBox");
            
			node.ParentKey = "MBKYellowBox";
			node.Attributes.Add("moduleKey", "MBKYellowBox");
			 node.Attributes.Add("permissionKey", "admin");
			 textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "SYSTEM");

            nodes.Add(node);



			  node = new DynamicNode();
            //node.Title = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.USERS_AND_ROLES ;
            node.Controller = "secRoles";
            node.Area = "SFSdotNetFrameworkSecurity";
            node.Action = "Index";
            node.Key = "SFSdotNetFrameworkSecurity_MBKYellowBox_roles";
            node.ParentKey = "MBKYellowBox_System_override";
            node.Attributes.Add("moduleKey", "MBKYellowBox");
                      node.RouteValues.Add("overrideModule", "MBKYellowBox");
            node.Attributes.Add("permissionKey", "admin");
            textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "ROLES");

            nodes.Add(node);




			 node = new DynamicNode();
            //node.Title = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.USERS_AND_ROLES ;
            node.Controller = "secUserCompanies";
            node.Area = "SFSdotNetFrameworkSecurity";
            node.Action = "Index";
            node.Key = "SFSdotNetFrameworkSecurity_MBKYellowBox_user_companies";
            node.ParentKey = "MBKYellowBox_System_override";
            node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.RouteValues.Add("overrideModule", "MBKYellowBox");
            node.Attributes.Add("permissionKey", "admin");
			textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "USERS_AND_ROLES");

            nodes.Add(node);

			
			 node = new DynamicNode();
            node.Controller = "secCompanies";
            node.Area = "SFSdotNetFrameworkSecurity";
            node.Action = "Index";
            node.Key = "SFSdotNetFrameworkSecurity_MBKYellowBox_child_companies";
            node.ParentKey = "MBKYellowBox_System_override";
            node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.RouteValues.Add("overrideModule", "MBKYellowBox");
			node.RouteValues.Add("usemode", "children");
            node.Attributes.Add("permissionKey", "admin");
			textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "COMPANIES_CHILDS");

            nodes.Add(node);
			 node = new DynamicNode();
            //node.Title = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.EVENT_LOG;
            node.Controller = "secEventLogs";
            node.Area = "SFSdotNetFrameworkSecurity";
            node.Action = "Index";
            node.Key = "SFSdotNetFrameworkSecurity_MBKYellowBox_EventLogs";
            node.ParentKey = "MBKYellowBox_System_override";
            node.Attributes.Add("moduleKey", "MBKYellowBox");

            node.RouteValues.Add("overrideModule", "MBKYellowBox");

			
            node.Attributes.Add("permissionKey", "admin");
			textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "EVENT_LOG");

            nodes.Add(node);
			 node = new DynamicNode();
            //node.Title = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.EVENT_LOG;
            node.Controller = "Dashboard";
            node.Area = "SFSdotNetFrameworkSecurity";
            node.Action = "Statics";
            node.Key = "SFSdotNetFrameworkSecurity_MBKYellowBox_Statics";
            node.ParentKey = "MBKYellowBox_System_override";
            node.Attributes.Add("moduleKey", "MBKYellowBox");

            node.RouteValues.Add("overrideModule", "MBKYellowBox");
            node.Attributes.Add("permissionKey", "admin");
            textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "SERVICE_USE_STATICS");

            nodes.Add(node);


			 node = new DynamicNode();
           // node.Title = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.CHANGE_AUDITING;
            node.Controller = "secAudits";
            node.Area = "SFSdotNetFrameworkSecurity";
            node.Action = "Index";
            node.Key = "SFSdotNetFrameworkSecurity_MBKYellowBox_ChangeAutiting";
            node.ParentKey = "MBKYellowBox_System_override";
            node.Attributes.Add("moduleKey", "MBKYellowBox");
            node.RouteValues.Add("overrideModule", "MBKYellowBox");
            node.Attributes.Add("permissionKey", "admin");
			 textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "CHANGE_AUDITING");

            nodes.Add(node);




            return nodes;
        }
    }
}
