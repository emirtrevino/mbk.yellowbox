
 
 
// <Template>
//   <SolutionTemplate></SolutionTemplate>
//   <Version>20140213.2136</Version>
//   <Update>Agregado el objeto ContextRequest en todas las peticiones de reglas de negocio</Update>
// </Template>using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Collections.Generic;
using SFSdotNet.Framework.Security.BusinessObjects;
using SFSdotNet.Framework.My;


namespace MBK.YellowBox.Web.Mvc
{
    public partial class SecuritySettings
    {
		static partial void OnCreatingRolesAndUsers(object sender, secModule module);
		static partial void OnIntegrationSetting(object sender, SFSdotNet.Framework.Security.BusinessObjects.secModule module, ContextRequest contextRequest);
        
        public static void PermissionsInitialization() {
			ContextRequest contextRequest = new ContextRequest();
            string moduleKey = "MBKYellowBox";

			var module = SFSdotNet.Framework.Security.SiteMapBuilder.AddModuleIfNotExist(moduleKey, moduleKey, "MBK.YellowBox", null);

            #region permissions
            secPermission createPermission = SFSdotNet.Framework.Security.Permissions.AddPermissionIfNotExist("c", "Create", module, contextRequest);
            secPermission readPermission = SFSdotNet.Framework.Security.Permissions.AddPermissionIfNotExist("r", "Read", module, contextRequest);
            secPermission updatePermission = SFSdotNet.Framework.Security.Permissions.AddPermissionIfNotExist("u", "Update", module, contextRequest);
            secPermission deletePermission = SFSdotNet.Framework.Security.Permissions.AddPermissionIfNotExist("d", "Delete", module, contextRequest);
			
			OnCreatingRolesAndUsers(null, module);
			 OnIntegrationSetting(null, module, contextRequest);
		
			//if (SFSdotNet.Framework.Security.BR.secRolesBR.Instance.GetBy(p => p.LoweredRoleName == "superadmin").Count == 0)
            //    SFSdotNet.Framework.Security.BR.secRolesBR.Instance.Create(new secRole() { LoweredRoleName="superadmin", RoleName="superadmin" });


			 if (SFSdotNet.Framework.Security.BR.secRolesBR.Instance.GetBy(p => p.LoweredRoleName == "mbkyellowbox admin", contextRequest).Count == 0)
                SFSdotNet.Framework.Security.BR.secRolesBR.Instance.Create(new secRole() { LoweredRoleName="mbkyellowbox admin", RoleName="MBKYellowBox Admin" }, contextRequest);
            
            
			AddPermissionIfNotExist(module, null, readPermission);
            #endregion
			
			secBusinessObject bo = null;
			secModuleObjectPermission mop =  null;
            secRoleModuleObjectPermission rmop = null;
			#region BusinessObjects
			
            #region LocationType
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "LocationType" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="LocationType",EntitySetName="LocationTypes", Name="LocationType", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "LocationTypes";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region RouteLocation
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "RouteLocation" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="RouteLocation",EntitySetName="RouteLocations", Name="RouteLocation", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "RouteLocations";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region Student
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "Student" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="Student",EntitySetName="Students", Name="Student", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "Students";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region StudentLocation
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "StudentLocation" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="StudentLocation",EntitySetName="StudentLocations", Name="StudentLocation", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "StudentLocations";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region Transport
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "Transport" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="Transport",EntitySetName="Transports", Name="Transport", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "Transports";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region YBLocation
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "YBLocation" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="YBLocation",EntitySetName="YBLocations", Name="YBLocation", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "YBLocations";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region YBRoute
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "YBRoute" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="YBRoute",EntitySetName="YBRoutes", Name="YBRoute", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "YBRoutes";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region StudentParent
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "StudentParent" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="StudentParent",EntitySetName="StudentParents", Name="StudentParent", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "StudentParents";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region AcademyLevel
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "AcademyLevel" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="AcademyLevel",EntitySetName="AcademyLevels", Name="AcademyLevel", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "AcademyLevels";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region Profesor
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "Profesor" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="Profesor",EntitySetName="Profesors", Name="Profesor", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "Profesors";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #endregion
        }
		
		public static void AddPermissionIfNotExist(secModule module, secBusinessObject businessObject, secPermission permission) {
              // se asume que el permiso ya existe
            // se asume que el m?dulo ya existe
             ContextRequest contextRequest = new ContextRequest();
            if (businessObject == null)
            {
                // m?dulo y permiso ya existen
                secPermission mp = SFSdotNet.Framework.Security.BR.secPermissionsBR.Instance.GetBy(
                p => p.PermissionKey == permission.PermissionKey
                && p.secModules.Any(x=>x.ModuleKey == module.ModuleKey)
                , contextRequest).FirstOrDefault();
                if (mp == null) {

                    SFSdotNet.Framework.Security.BR.secPermissionsBR.Instance.AddRelation(permission, module);
                }

                secRoleModulePermission rmp = SFSdotNet.Framework.Security.BR.secRoleModulePermissionsBR.Instance.GetBy(
                    p => p.secRole.LoweredRoleName == "superadmin"
                    && p.secModule.ModuleKey == module.ModuleKey
                
                    && p.secPermission.PermissionKey == permission.PermissionKey, contextRequest).FirstOrDefault();
                if (rmp == null) {
                    rmp = new secRoleModulePermission();
					rmp.secRole = SFSdotNet.Framework.Security.BR.secRolesBR.Instance.GetBy(p => p.LoweredRoleName == "superadmin", contextRequest).FirstOrDefault();
                    rmp.secModule = module;
                    rmp.secPermission = permission;
					rmp.IsAllowed = true;
                    SFSdotNet.Framework.Security.BR.secRoleModulePermissionsBR.Instance.Create(rmp, contextRequest);
                }
				rmp = SFSdotNet.Framework.Security.BR.secRoleModulePermissionsBR.Instance.GetBy(
                    p => p.secRole.LoweredRoleName == "mbkyellowbox admin"
                    && p.secModule.ModuleKey == module.ModuleKey
                
                    && p.secPermission.PermissionKey == permission.PermissionKey, contextRequest).FirstOrDefault();
                if (rmp == null) {
                    rmp = new secRoleModulePermission();
					rmp.secRole = SFSdotNet.Framework.Security.BR.secRolesBR.Instance.GetBy(p => p.LoweredRoleName == "mbkyellowbox admin", contextRequest).FirstOrDefault();
                    rmp.secModule = module;
                    rmp.secPermission = permission;
					rmp.IsAllowed = true;
                    SFSdotNet.Framework.Security.BR.secRoleModulePermissionsBR.Instance.Create(rmp, contextRequest);
                }
            }
            else { 
				secModuleObjectPermission mop = SFSdotNet.Framework.Security.BR.secModuleObjectPermissionsBR.Instance.GetBy(
					p => p.secPermission.PermissionKey == permission.PermissionKey
					&& p.secBusinessObject.BusinessObjectKey == businessObject.BusinessObjectKey
					&& p.secModule.ModuleKey == module.ModuleKey
					, contextRequest).FirstOrDefault();
				if (mop == null)
				{
					mop = new secModuleObjectPermission();
					mop.secModule = module;
					mop.secBusinessObject = businessObject;
					mop.secPermission = permission;

					mop = SFSdotNet.Framework.Security.BR.secModuleObjectPermissionsBR.Instance.Create(mop, contextRequest);
				}

				secRoleModuleObjectPermission rmop = SFSdotNet.Framework.Security.BR.secRoleModuleObjectPermissionsBR.Instance.GetBy(
					p => p.secRole.LoweredRoleName == "superadmin"
					&& p.secModule.ModuleKey == module.ModuleKey 
					&& p.secBusinessObject.BusinessObjectKey == businessObject.BusinessObjectKey 
					&& p.secPermission.PermissionKey == permission.PermissionKey, contextRequest).FirstOrDefault();
				if (rmop == null)
				{
					rmop = new secRoleModuleObjectPermission();
					rmop.secBusinessObject = businessObject ;
					rmop.secModule = module;
					rmop.secRole = SFSdotNet.Framework.Security.BR.secRolesBR.Instance.GetBy(p => p.LoweredRoleName == "superadmin", contextRequest).FirstOrDefault();
					rmop.secPermission = permission ;
					rmop.IsAllowed = true;

					SFSdotNet.Framework.Security.BR.secRoleModuleObjectPermissionsBR.Instance.Create(rmop, contextRequest);

				}
				
				rmop = SFSdotNet.Framework.Security.BR.secRoleModuleObjectPermissionsBR.Instance.GetBy(
					p => p.secRole.LoweredRoleName == "mbkyellowbox admin"
					&& p.secModule.ModuleKey == module.ModuleKey 
					&& p.secBusinessObject.BusinessObjectKey == businessObject.BusinessObjectKey 
					&& p.secPermission.PermissionKey == permission.PermissionKey, contextRequest).FirstOrDefault();
				if (rmop == null)
				{
					rmop = new secRoleModuleObjectPermission();
					rmop.secBusinessObject = businessObject ;
					rmop.secModule = module;
					rmop.secRole = SFSdotNet.Framework.Security.BR.secRolesBR.Instance.GetBy(p => p.LoweredRoleName == "mbkyellowbox admin", contextRequest).FirstOrDefault();
					rmop.secPermission = permission ;
					rmop.IsAllowed = true;

					SFSdotNet.Framework.Security.BR.secRoleModuleObjectPermissionsBR.Instance.Create(rmop, contextRequest);

				}
			}

        }

    }
}
