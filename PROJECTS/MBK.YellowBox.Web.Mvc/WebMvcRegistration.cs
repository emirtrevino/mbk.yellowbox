﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcContrib.PortableAreas;
using System.Web.Mvc;
using System.Configuration;
using System.Web.Routing;
using SFSdotNet.Framework.My;
using SFSdotNet.Framework.Web.Mvc;

namespace MBK.YellowBox.Web.Mvc
{
    public partial class MBKYellowBoxWebMvcRegistration : PortableAreaRegistration, IRegistrationModuleWithInfo
    {

        public override void RegisterArea(System.Web.Mvc.AreaRegistrationContext context, IApplicationBus bus)
        {
            #region MBKYellowBox/Views/Shared/
            context.MapRoute("MBKYellowBox_Views_Shared", "MBKYellowBox/Views/Shared/{resourceName}",
                new { controller = "EmbeddedResource", action = "Index", resourcePath = "Views/Shared" },
                new string[] { "MvcContrib.PortableAreas" });
            #endregion

            #region MBKYellowBox/Content/Themes/Default/
            context.MapRoute("MBKYellowBox_ResourceRoute_theme", "MBKYellowBox/Content/Themes/Default/{resourceName}",
                new { controller = "EmbeddedResource", action = "Index", resourcePath = "Content/Themes/Default" },
                new string[] { "MvcContrib.PortableAreas" });
            #endregion
            #region MBKYellowBox/Content/Themes/Default/css/
            context.MapRoute("MBKYellowBox_ResourceRoute_theme_css", "MBKYellowBox/Content/Themes/Default/css/{resourceName}",
    new { controller = "EmbeddedResource", action = "Index", resourcePath = "Content/Themes/Default/css" },
    new string[] { "MvcContrib.PortableAreas" });
            #endregion



           
            context.MapRoute("MBKYellowBox_ResourceRoute_js", "MBKYellowBox/Content/js/{resourceName}",
    new { controller = "EmbeddedResource", action = "Index", resourcePath = "Content/js" },
    new string[] { "MvcContrib.PortableAreas" });
     


            #region MBKYellowBox/Content/Themes/Default/img/
            context.MapRoute("MBKYellowBox_ResourceRoute_theme_img", "MBKYellowBox/Content/Themes/Default/img/{resourceName}",
    new { controller = "EmbeddedResource", action = "Index", resourcePath = "Content/Themes/Default/img" },
    new string[] { "MvcContrib.PortableAreas" });
            #endregion
            #region MBKYellowBox/Content/img/
            context.MapRoute("MBKYellowBox_ResourceRoute_img", "MBKYellowBox/Content/img/{resourceName}",
new { controller = "EmbeddedResource", action = "Index", resourcePath = "Content/img" },
new string[] { "MvcContrib.PortableAreas" });
            #endregion

            context.MapRoute("MBKYellowBox_ResourceImageRoute", "MBKYellowBox/images/{resourceName}",
                new { controller = "EmbeddedResource", action = "Index", resourcePath = "images" },
                new string[] { "MvcContrib.PortableAreas" });

            context.MapRoute("MBKYellowBox_Default", 
                "MBKYellowBox/{controller}/{action}",
                new { controller = "Home", 
                    action = "index" },
                new string[] { "MBK.YellowBox.Web.Mvc.Controllers" 
                });

            context.MapRoute(
              "MBKYellowBox_Id", // Route name
              "MBKYellowBox/{controller}/{action}/{id}", // URL with parameters
              new { controller = "Home", 
                  action = "Index", 
                  id = UrlParameter.Optional }, 
                  new[] { "MBK.YellowBox.Web.Mvc.Controllers" 
                  });

            //context.MapRoute(
            // "MBKYellowBox_usemode_Id", // Route name
            // "MBKYellowBox/{controller}/usemode/{usemode}/{action}/{id}", // URL with parameters
            // new
            // {
            //     controller = "Home",
            //     action = "Index",
            //     id = UrlParameter.Optional
            // },
            //     new[] { "MBK.YellowBox.Web.Mvc.Controllers" 
            //      });
          

            
        
            //ControllerBuilder.Current.DefaultNamespaces.Add("MBK.YellowBox.Web.Mvc.Controllers");

            this.RegisterAreaEmbeddedResources();
            if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["AutoInjectPermissionsOnStartup"]))
                SecuritySettings.PermissionsInitialization();

            OnAreaRegistration(this, new EventArgs());
        }
        public  SFSdotNet.Framework.My.BusinessModuleApp GetModuleInfo()
        {
            SFSdotNet.Framework.My.BusinessModuleApp result = new SFSdotNet.Framework.My.BusinessModuleApp();
            result.BusinessModulePath = "MBKYellowBox";
            //result.DefaultOwnLayout = VirtualPathUtility.ToAbsolute("~/") + "Areas/" + result.BusinessModulePath + "/Views/Shared/_Layout.cshtml";
            result.UseOwnLayout = false;
            OnBusinessAppInfoRequest(this, result );
            return result;
        }
        partial void OnBusinessAppInfoRequest(object sender, BusinessModuleApp e);
        partial void OnAreaRegistration(object sender, EventArgs e);

        public override string AreaName
        {
            get
            {
                return "MBKYellowBox";
            }
        }
    }
}
