﻿using SFSdotNet.Framework.Web.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MBK.YellowBox.Web.Mvc.Models
{
    public class StudentCustomModel:ModelBase
    {
        public string  Nombre { get; set; }
        public DateTime? FechaNacimiento  { get; set; }

        public string EspacioVacio1 { get; set; }

    }
}