﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MBK.YellowBox.Web.Mvc.Resources;
using System.Runtime.Serialization;
using SFSdotNet.Framework.Web.Mvc.Models;
using SFSdotNet.Framework.Web.Mvc.Extensions;
using BO = MBK.YellowBox.BusinessObjects;
using System.Web.Mvc;
//using SFSdotNet.Framework.Web.Mvc.Validation;
//using SFSdotNet.Framework.Web.Mvc.Models;
using SFSdotNet.Framework.Web.Mvc.Resources;
using SFSdotNet.Framework.Common.Entities.Metadata;
using System.Text;
using MBK.YellowBox.BusinessObjects;
	namespace MBK.YellowBox.Web.Mvc.Models.LocationTypes 
	{
	public partial class LocationTypeModel: ModelBase{

	  public LocationTypeModel(BO.LocationType resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public LocationTypeModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidLocationType.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Name != null)
		
            return this.Name.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidLocationType{ get; set; }
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("NAME"/*, NameResourceType=typeof(LocationTypeResources)*/)]
	public String   Name { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("NAMEKEY"/*, NameResourceType=typeof(LocationTypeResources)*/)]
	public String   NameKey { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("GUIDCOMPANY"/*, NameResourceType=typeof(LocationTypeResources)*/)]
	public Guid  ? GuidCompany { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(LocationTypeResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(LocationTypeResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(LocationTypeResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(LocationTypeResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(LocationTypeResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(LocationTypeResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
		
		[LocalizedDisplayName("YBLOCATIONS"/*, NameResourceType=typeof(LocationTypeResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="YBLocations.Count()", ModelPartialType="YBLocations.YBLocation", BusinessObjectSetName = "YBLocations")]
        public List<YBLocations.YBLocationModel> YBLocations { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidLocationType != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidLocationType").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(LocationTypeModel model){
            
		this.GuidLocationType = model.GuidLocationType;
		this.Name = model.Name;
		this.NameKey = model.NameKey;
		this.GuidCompany = model.GuidCompany;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.LocationType GetBusinessObject()
        {
            BusinessObjects.LocationType result = new BusinessObjects.LocationType();


			       
	if (this.GuidLocationType != null )
				result.GuidLocationType = (Guid)this.GuidLocationType;
				
	if (this.Name != null )
				result.Name = (String)this.Name.Trim().Replace("\t", String.Empty);
				
	if (this.NameKey != null )
				result.NameKey = (String)this.NameKey.Trim().Replace("\t", String.Empty);
				
	if (this.GuidCompany != null )
				result.GuidCompany = (Guid)this.GuidCompany;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				

            return result;
        }
        public void Bind(BusinessObjects.LocationType businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidLocationType = businessObject.GuidLocationType;
				
				
	if (businessObject.Name != null )
				this.Name = (String)businessObject.Name;
				
	if (businessObject.NameKey != null )
				this.NameKey = (String)businessObject.NameKey;
				
	if (businessObject.GuidCompany != null )
				this.GuidCompany = (Guid)businessObject.GuidCompany;
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
           
        }
	}
}
	namespace MBK.YellowBox.Web.Mvc.Models.RouteLocations 
	{
	public partial class RouteLocationModel: ModelBase{

	  public RouteLocationModel(BO.RouteLocation resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public RouteLocationModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidRouteLocation.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.OrderRoute != null)
		
            return this.OrderRoute.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidRouteLocation{ get; set; }
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDROUTE"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	public Guid  ? GuidRoute { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDLOCATION"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	public Guid  ? GuidLocation { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("ORDERROUTE"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	public Int32  ? OrderRoute { get; set; }
	public string _OrderRouteText = null;
    public string OrderRouteText {
        get {
			if (string.IsNullOrEmpty( _OrderRouteText ))
				{
	
            if (OrderRoute != null)
				return OrderRoute.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _OrderRouteText ;
			}			
        }
		set{
			_OrderRouteText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDSTUDENT"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	public Guid  ? GuidStudent { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("GUIDCOMPANY"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	public Guid  ? GuidCompany { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	
 
			//[RelationFilterable( FiltrablePropertyPathName="Student.GuidStudent")]
			[RelationFilterable(DataClassProvider = typeof(Controllers.StudentsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetByJson", DataPropertyText = "FullName", DataPropertyValue = "GuidStudent", FiltrablePropertyPathName="Student.GuidStudent")]	

			//1234
	[LookUp("MBK.YellowBox", "MBKYellowBox","Students", "ListViewGen", "FullName", "GuidStudent")]	
			[LocalizedDisplayName("STUDENT"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	public Guid  ? FkStudent { get; set; }
		[LocalizedDisplayName("STUDENT"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	[Exportable()]
	public string  FkStudentText { get; set; }
    public string FkStudentSafeKey { get; set; }

	
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.YBLocationsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Description", DataPropertyValue = "GuidLocation", FiltrablePropertyPathName="YBLocation.GuidLocation")]	

	[LocalizedDisplayName("YBLOCATION"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	public Guid  ? FkYBLocation { get; set; }
		[LocalizedDisplayName("YBLOCATION"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	[Exportable()]
	public string  FkYBLocationText { get; set; }
    public string FkYBLocationSafeKey { get; set; }

	
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.YBRoutesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Title", DataPropertyValue = "GuidRoute", FiltrablePropertyPathName="YBRoute.GuidRoute")]	

	[LocalizedDisplayName("YBROUTE"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	public Guid  ? FkYBRoute { get; set; }
		[LocalizedDisplayName("YBROUTE"/*, NameResourceType=typeof(RouteLocationResources)*/)]
	[Exportable()]
	public string  FkYBRouteText { get; set; }
    public string FkYBRouteSafeKey { get; set; }

	
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.GuidRouteLocation != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidRouteLocation").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(RouteLocationModel model){
            
		this.GuidRouteLocation = model.GuidRouteLocation;
		this.GuidRoute = model.GuidRoute;
		this.GuidLocation = model.GuidLocation;
		this.OrderRoute = model.OrderRoute;
		this.GuidStudent = model.GuidStudent;
		this.GuidCompany = model.GuidCompany;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.RouteLocation GetBusinessObject()
        {
            BusinessObjects.RouteLocation result = new BusinessObjects.RouteLocation();


			       
	if (this.GuidRouteLocation != null )
				result.GuidRouteLocation = (Guid)this.GuidRouteLocation;
				
	if (this.GuidRoute != null )
				result.GuidRoute = (Guid)this.GuidRoute;
				
	if (this.GuidLocation != null )
				result.GuidLocation = (Guid)this.GuidLocation;
				
	if (this.OrderRoute != null )
				result.OrderRoute = (Int32)this.OrderRoute;
				
	if (this.GuidStudent != null )
				result.GuidStudent = (Guid)this.GuidStudent;
				
	if (this.GuidCompany != null )
				result.GuidCompany = (Guid)this.GuidCompany;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			
			if(this.FkStudent != null )
			result.Student = new BusinessObjects.Student() { GuidStudent= (Guid)this.FkStudent };
				
			
			if(this.FkYBLocation != null )
			result.YBLocation = new BusinessObjects.YBLocation() { GuidLocation= (Guid)this.FkYBLocation };
				
			
			if(this.FkYBRoute != null )
			result.YBRoute = new BusinessObjects.YBRoute() { GuidRoute= (Guid)this.FkYBRoute };
				

            return result;
        }
        public void Bind(BusinessObjects.RouteLocation businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidRouteLocation = businessObject.GuidRouteLocation;
				
				
	if (businessObject.GuidRoute != null )
				this.GuidRoute = (Guid)businessObject.GuidRoute;
				
	if (businessObject.GuidLocation != null )
				this.GuidLocation = (Guid)businessObject.GuidLocation;
				
	if (businessObject.OrderRoute != null )
				this.OrderRoute = (Int32)businessObject.OrderRoute;
				
	if (businessObject.GuidStudent != null )
				this.GuidStudent = (Guid)businessObject.GuidStudent;
				
	if (businessObject.GuidCompany != null )
				this.GuidCompany = (Guid)businessObject.GuidCompany;
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.Student != null){
	                	this.FkStudentText = businessObject.Student.FullName != null ? businessObject.Student.FullName.ToString() : "";; 
										
				this.FkStudent = businessObject.Student.GuidStudent;
                this.FkStudentSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.Student,"GuidStudent").Replace("/","-");

			}
	        if (businessObject.YBLocation != null){
	                	this.FkYBLocationText = businessObject.YBLocation.Description != null ? businessObject.YBLocation.Description.ToString() : "";; 
										
				this.FkYBLocation = businessObject.YBLocation.GuidLocation;
                this.FkYBLocationSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.YBLocation,"GuidLocation").Replace("/","-");

			}
	        if (businessObject.YBRoute != null){
	                	this.FkYBRouteText = businessObject.YBRoute.Title != null ? businessObject.YBRoute.Title.ToString() : "";; 
										
				this.FkYBRoute = businessObject.YBRoute.GuidRoute;
                this.FkYBRouteSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.YBRoute,"GuidRoute").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.YellowBox.Web.Mvc.Models.Students 
	{
	public partial class StudentModel: ModelBase{

	  public StudentModel(BO.Student resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public StudentModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidStudent.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.FullName != null)
		
            return this.FullName.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidStudent{ get; set; }
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("FULLNAME"/*, NameResourceType=typeof(StudentResources)*/)]
	public String   FullName { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("BIRTHDATE"/*, NameResourceType=typeof(StudentResources)*/)]
	public DateTime  ? BirthDate { get; set; }
	public string BirthDateText {
        get {
            if (BirthDate != null)
				return ((DateTime)BirthDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.BirthDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("GRADE"/*, NameResourceType=typeof(StudentResources)*/)]
	public Int32  ? Grade { get; set; }
	public string _GradeText = null;
    public string GradeText {
        get {
			if (string.IsNullOrEmpty( _GradeText ))
				{
	
            if (Grade != null)
				return Grade.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _GradeText ;
			}			
        }
		set{
			_GradeText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GROUP"/*, NameResourceType=typeof(StudentResources)*/)]
	public String   Group { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPARENT"/*, NameResourceType=typeof(StudentResources)*/)]
	public Guid  ? GuidParent { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("GUIDCOMPANY"/*, NameResourceType=typeof(StudentResources)*/)]
	public Guid  ? GuidCompany { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(StudentResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(StudentResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(StudentResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(StudentResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(StudentResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(StudentResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("COMMENTS"/*, NameResourceType=typeof(StudentResources)*/)]
	public String   Comments { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDACADEMYLEVEL"/*, NameResourceType=typeof(StudentResources)*/)]
	public Guid  ? GuidAcademyLevel { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROFESOR"/*, NameResourceType=typeof(StudentResources)*/)]
	public Guid  ? GuidProfesor { get; set; }
		
		
	
[Exportable()]
		
	
 
			//[RelationFilterable( FiltrablePropertyPathName="StudentParent.GuidStudentParent")]
			[RelationFilterable(DataClassProvider = typeof(Controllers.StudentParentsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetByJson", DataPropertyText = "FullName", DataPropertyValue = "GuidStudentParent", FiltrablePropertyPathName="StudentParent.GuidStudentParent")]	

			//1234
	[AutoComplete("StudentParents", "GetByJson", "filter", "FullName", "GuidStudentParent")]	
			[LocalizedDisplayName("STUDENTPARENT"/*, NameResourceType=typeof(StudentResources)*/)]
	public Guid  ? FkStudentParent { get; set; }
		[LocalizedDisplayName("STUDENTPARENT"/*, NameResourceType=typeof(StudentResources)*/)]
	[Exportable()]
	public string  FkStudentParentText { get; set; }
    public string FkStudentParentSafeKey { get; set; }

	
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.AcademyLevelsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Title", DataPropertyValue = "GuidAcademyLevel", FiltrablePropertyPathName="AcademyLevel.GuidAcademyLevel")]	

	[LocalizedDisplayName("ACADEMYLEVEL"/*, NameResourceType=typeof(StudentResources)*/)]
	public Guid  ? FkAcademyLevel { get; set; }
		[LocalizedDisplayName("ACADEMYLEVEL"/*, NameResourceType=typeof(StudentResources)*/)]
	[Exportable()]
	public string  FkAcademyLevelText { get; set; }
    public string FkAcademyLevelSafeKey { get; set; }

	
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.ProfesorsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "FullName", DataPropertyValue = "GuidProfesor", FiltrablePropertyPathName="Profesor.GuidProfesor")]	

	[LocalizedDisplayName("PROFESOR"/*, NameResourceType=typeof(StudentResources)*/)]
	public Guid  ? FkProfesor { get; set; }
		[LocalizedDisplayName("PROFESOR"/*, NameResourceType=typeof(StudentResources)*/)]
	[Exportable()]
	public string  FkProfesorText { get; set; }
    public string FkProfesorSafeKey { get; set; }

	
		
		
		[LocalizedDisplayName("ROUTELOCATIONS"/*, NameResourceType=typeof(StudentResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="RouteLocations.Count()", ModelPartialType="RouteLocations.RouteLocation", BusinessObjectSetName = "RouteLocations")]
        public List<RouteLocations.RouteLocationModel> RouteLocations { get; set; }			
	
		[LocalizedDisplayName("STUDENTLOCATIONS"/*, NameResourceType=typeof(StudentResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="StudentLocations.Count()", ModelPartialType="StudentLocations.StudentLocation", BusinessObjectSetName = "StudentLocations")]
        public List<StudentLocations.StudentLocationModel> StudentLocations { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidStudent != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidStudent").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(StudentModel model){
            
		this.GuidStudent = model.GuidStudent;
		this.FullName = model.FullName;
		this.BirthDate = model.BirthDate;
		this.Grade = model.Grade;
		this.Group = model.Group;
		this.GuidParent = model.GuidParent;
		this.GuidCompany = model.GuidCompany;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
		this.Comments = model.Comments;
		this.GuidAcademyLevel = model.GuidAcademyLevel;
		this.GuidProfesor = model.GuidProfesor;
        }

        public BusinessObjects.Student GetBusinessObject()
        {
            BusinessObjects.Student result = new BusinessObjects.Student();


			       
	if (this.GuidStudent != null )
				result.GuidStudent = (Guid)this.GuidStudent;
				
	if (this.FullName != null )
				result.FullName = (String)this.FullName.Trim().Replace("\t", String.Empty);
				
				if(this.BirthDate != null)
					if (this.BirthDate != null)
				result.BirthDate = (DateTime)this.BirthDate;		
				
	if (this.Grade != null )
				result.Grade = (Int32)this.Grade;
				
	if (this.Group != null )
				result.Group = (String)this.Group.Trim().Replace("\t", String.Empty);
				
	if (this.GuidParent != null )
				result.GuidParent = (Guid)this.GuidParent;
				
	if (this.GuidCompany != null )
				result.GuidCompany = (Guid)this.GuidCompany;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
	if (this.Comments != null )
				result.Comments = (String)this.Comments.Trim().Replace("\t", String.Empty);
				
	if (this.GuidAcademyLevel != null )
				result.GuidAcademyLevel = (Guid)this.GuidAcademyLevel;
				
	if (this.GuidProfesor != null )
				result.GuidProfesor = (Guid)this.GuidProfesor;
				
			
			if(this.FkStudentParent != null )
			result.StudentParent = new BusinessObjects.StudentParent() { GuidStudentParent= (Guid)this.FkStudentParent };
				
			
			if(this.FkAcademyLevel != null )
			result.AcademyLevel = new BusinessObjects.AcademyLevel() { GuidAcademyLevel= (Guid)this.FkAcademyLevel };
				
			
			if(this.FkProfesor != null )
			result.Profesor = new BusinessObjects.Profesor() { GuidProfesor= (Guid)this.FkProfesor };
				

            return result;
        }
        public void Bind(BusinessObjects.Student businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidStudent = businessObject.GuidStudent;
				
				
	if (businessObject.FullName != null )
				this.FullName = (String)businessObject.FullName;
				if (businessObject.BirthDate != null )
				this.BirthDate = (DateTime)businessObject.BirthDate;
				
	if (businessObject.Grade != null )
				this.Grade = (Int32)businessObject.Grade;
				
	if (businessObject.Group != null )
				this.Group = (String)businessObject.Group;
				
	if (businessObject.GuidParent != null )
				this.GuidParent = (Guid)businessObject.GuidParent;
				
	if (businessObject.GuidCompany != null )
				this.GuidCompany = (Guid)businessObject.GuidCompany;
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
				
	if (businessObject.Comments != null )
				this.Comments = (String)businessObject.Comments;
				
	if (businessObject.GuidAcademyLevel != null )
				this.GuidAcademyLevel = (Guid)businessObject.GuidAcademyLevel;
				
	if (businessObject.GuidProfesor != null )
				this.GuidProfesor = (Guid)businessObject.GuidProfesor;
	        if (businessObject.StudentParent != null){
	                	this.FkStudentParentText = businessObject.StudentParent.FullName != null ? businessObject.StudentParent.FullName.ToString() : "";; 
										
				this.FkStudentParent = businessObject.StudentParent.GuidStudentParent;
                this.FkStudentParentSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.StudentParent,"GuidStudentParent").Replace("/","-");

			}
	        if (businessObject.AcademyLevel != null){
	                	this.FkAcademyLevelText = businessObject.AcademyLevel.Title != null ? businessObject.AcademyLevel.Title.ToString() : "";; 
										
				this.FkAcademyLevel = businessObject.AcademyLevel.GuidAcademyLevel;
                this.FkAcademyLevelSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.AcademyLevel,"GuidAcademyLevel").Replace("/","-");

			}
	        if (businessObject.Profesor != null){
	                	this.FkProfesorText = businessObject.Profesor.FullName != null ? businessObject.Profesor.FullName.ToString() : "";; 
										
				this.FkProfesor = businessObject.Profesor.GuidProfesor;
                this.FkProfesorSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.Profesor,"GuidProfesor").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.YellowBox.Web.Mvc.Models.StudentLocations 
	{
	public partial class StudentLocationModel: ModelBase{

	  public StudentLocationModel(BO.StudentLocation resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public StudentLocationModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidStudentLocation.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Bytes != null)
		
            return this.Bytes.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidStudentLocation{ get; set; }
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDLOCATION"/*, NameResourceType=typeof(StudentLocationResources)*/)]
	public Guid  ? GuidLocation { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDSTUDENT"/*, NameResourceType=typeof(StudentLocationResources)*/)]
	public Guid  ? GuidStudent { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("GUIDCOMPANY"/*, NameResourceType=typeof(StudentLocationResources)*/)]
	public Guid  ? GuidCompany { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(StudentLocationResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(StudentLocationResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(StudentLocationResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(StudentLocationResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(StudentLocationResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(StudentLocationResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.StudentsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "FullName", DataPropertyValue = "GuidStudent", FiltrablePropertyPathName="Student.GuidStudent")]	

	[LocalizedDisplayName("STUDENT"/*, NameResourceType=typeof(StudentLocationResources)*/)]
	public Guid  ? FkStudent { get; set; }
		[LocalizedDisplayName("STUDENT"/*, NameResourceType=typeof(StudentLocationResources)*/)]
	[Exportable()]
	public string  FkStudentText { get; set; }
    public string FkStudentSafeKey { get; set; }

	
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.YBLocationsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Description", DataPropertyValue = "GuidLocation", FiltrablePropertyPathName="YBLocation.GuidLocation")]	

	[LocalizedDisplayName("YBLOCATION"/*, NameResourceType=typeof(StudentLocationResources)*/)]
	public Guid  ? FkYBLocation { get; set; }
		[LocalizedDisplayName("YBLOCATION"/*, NameResourceType=typeof(StudentLocationResources)*/)]
	[Exportable()]
	public string  FkYBLocationText { get; set; }
    public string FkYBLocationSafeKey { get; set; }

	
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.GuidStudentLocation != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidStudentLocation").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(StudentLocationModel model){
            
		this.GuidStudentLocation = model.GuidStudentLocation;
		this.GuidLocation = model.GuidLocation;
		this.GuidStudent = model.GuidStudent;
		this.GuidCompany = model.GuidCompany;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.StudentLocation GetBusinessObject()
        {
            BusinessObjects.StudentLocation result = new BusinessObjects.StudentLocation();


			       
	if (this.GuidStudentLocation != null )
				result.GuidStudentLocation = (Guid)this.GuidStudentLocation;
				
	if (this.GuidLocation != null )
				result.GuidLocation = (Guid)this.GuidLocation;
				
	if (this.GuidStudent != null )
				result.GuidStudent = (Guid)this.GuidStudent;
				
	if (this.GuidCompany != null )
				result.GuidCompany = (Guid)this.GuidCompany;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			
			if(this.FkStudent != null )
			result.Student = new BusinessObjects.Student() { GuidStudent= (Guid)this.FkStudent };
				
			
			if(this.FkYBLocation != null )
			result.YBLocation = new BusinessObjects.YBLocation() { GuidLocation= (Guid)this.FkYBLocation };
				

            return result;
        }
        public void Bind(BusinessObjects.StudentLocation businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidStudentLocation = businessObject.GuidStudentLocation;
				
				
	if (businessObject.GuidLocation != null )
				this.GuidLocation = (Guid)businessObject.GuidLocation;
				
	if (businessObject.GuidStudent != null )
				this.GuidStudent = (Guid)businessObject.GuidStudent;
				
	if (businessObject.GuidCompany != null )
				this.GuidCompany = (Guid)businessObject.GuidCompany;
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.Student != null){
	                	this.FkStudentText = businessObject.Student.FullName != null ? businessObject.Student.FullName.ToString() : "";; 
										
				this.FkStudent = businessObject.Student.GuidStudent;
                this.FkStudentSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.Student,"GuidStudent").Replace("/","-");

			}
	        if (businessObject.YBLocation != null){
	                	this.FkYBLocationText = businessObject.YBLocation.Description != null ? businessObject.YBLocation.Description.ToString() : "";; 
										
				this.FkYBLocation = businessObject.YBLocation.GuidLocation;
                this.FkYBLocationSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.YBLocation,"GuidLocation").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.YellowBox.Web.Mvc.Models.Transports 
	{
	public partial class TransportModel: ModelBase{

	  public TransportModel(BO.Transport resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public TransportModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidTrasport.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.TransportCode != null)
		
            return this.TransportCode.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidTrasport{ get; set; }
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("TRANSPORTCODE"/*, NameResourceType=typeof(TransportResources)*/)]
	public String   TransportCode { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("DESCRIPTION"/*, NameResourceType=typeof(TransportResources)*/)]
	public String   Description { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("CAPACITY"/*, NameResourceType=typeof(TransportResources)*/)]
	public Int32  ? Capacity { get; set; }
	public string _CapacityText = null;
    public string CapacityText {
        get {
			if (string.IsNullOrEmpty( _CapacityText ))
				{
	
            if (Capacity != null)
				return Capacity.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _CapacityText ;
			}			
        }
		set{
			_CapacityText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDROUTE"/*, NameResourceType=typeof(TransportResources)*/)]
	public Guid  ? GuidRoute { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("GUIDCOMPANY"/*, NameResourceType=typeof(TransportResources)*/)]
	public Guid  ? GuidCompany { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(TransportResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(TransportResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(TransportResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(TransportResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(TransportResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(TransportResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.YBRoutesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Title", DataPropertyValue = "GuidRoute", FiltrablePropertyPathName="YBRoute.GuidRoute")]	

	[LocalizedDisplayName("YBROUTE"/*, NameResourceType=typeof(TransportResources)*/)]
	public Guid  ? FkYBRoute { get; set; }
		[LocalizedDisplayName("YBROUTE"/*, NameResourceType=typeof(TransportResources)*/)]
	[Exportable()]
	public string  FkYBRouteText { get; set; }
    public string FkYBRouteSafeKey { get; set; }

	
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.GuidTrasport != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidTrasport").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(TransportModel model){
            
		this.GuidTrasport = model.GuidTrasport;
		this.TransportCode = model.TransportCode;
		this.Description = model.Description;
		this.Capacity = model.Capacity;
		this.GuidRoute = model.GuidRoute;
		this.GuidCompany = model.GuidCompany;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.Transport GetBusinessObject()
        {
            BusinessObjects.Transport result = new BusinessObjects.Transport();


			       
	if (this.GuidTrasport != null )
				result.GuidTrasport = (Guid)this.GuidTrasport;
				
	if (this.TransportCode != null )
				result.TransportCode = (String)this.TransportCode.Trim().Replace("\t", String.Empty);
				
	if (this.Description != null )
				result.Description = (String)this.Description.Trim().Replace("\t", String.Empty);
				
	if (this.Capacity != null )
				result.Capacity = (Int32)this.Capacity;
				
	if (this.GuidRoute != null )
				result.GuidRoute = (Guid)this.GuidRoute;
				
	if (this.GuidCompany != null )
				result.GuidCompany = (Guid)this.GuidCompany;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			
			if(this.FkYBRoute != null )
			result.YBRoute = new BusinessObjects.YBRoute() { GuidRoute= (Guid)this.FkYBRoute };
				

            return result;
        }
        public void Bind(BusinessObjects.Transport businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidTrasport = businessObject.GuidTrasport;
				
				
	if (businessObject.TransportCode != null )
				this.TransportCode = (String)businessObject.TransportCode;
				
	if (businessObject.Description != null )
				this.Description = (String)businessObject.Description;
				
	if (businessObject.Capacity != null )
				this.Capacity = (Int32)businessObject.Capacity;
				
	if (businessObject.GuidRoute != null )
				this.GuidRoute = (Guid)businessObject.GuidRoute;
				
	if (businessObject.GuidCompany != null )
				this.GuidCompany = (Guid)businessObject.GuidCompany;
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.YBRoute != null){
	                	this.FkYBRouteText = businessObject.YBRoute.Title != null ? businessObject.YBRoute.Title.ToString() : "";; 
										
				this.FkYBRoute = businessObject.YBRoute.GuidRoute;
                this.FkYBRouteSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.YBRoute,"GuidRoute").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.YellowBox.Web.Mvc.Models.YBLocations 
	{
	public partial class YBLocationModel: ModelBase{

	  public YBLocationModel(BO.YBLocation resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public YBLocationModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidLocation.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Description != null)
		
            return this.Description.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidLocation{ get; set; }
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDLOCATIONTYPE"/*, NameResourceType=typeof(YBLocationResources)*/)]
	public Guid  ? GuidLocationType { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("LAT"/*, NameResourceType=typeof(YBLocationResources)*/)]
	public Double  ? Lat { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("LONG"/*, NameResourceType=typeof(YBLocationResources)*/)]
	public Double  ? Long { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("DESCRIPTION"/*, NameResourceType=typeof(YBLocationResources)*/)]
	public String   Description { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("ADDRESS"/*, NameResourceType=typeof(YBLocationResources)*/)]
	public String   Address { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("GUIDCOMPANY"/*, NameResourceType=typeof(YBLocationResources)*/)]
	public Guid  ? GuidCompany { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(YBLocationResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(YBLocationResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(YBLocationResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(YBLocationResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(YBLocationResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(YBLocationResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.LocationTypesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Name", DataPropertyValue = "GuidLocationType", FiltrablePropertyPathName="LocationType.GuidLocationType")]	

	[LocalizedDisplayName("LOCATIONTYPE"/*, NameResourceType=typeof(YBLocationResources)*/)]
	public Guid  ? FkLocationType { get; set; }
		[LocalizedDisplayName("LOCATIONTYPE"/*, NameResourceType=typeof(YBLocationResources)*/)]
	[Exportable()]
	public string  FkLocationTypeText { get; set; }
    public string FkLocationTypeSafeKey { get; set; }

	
		
		
		[LocalizedDisplayName("ROUTELOCATIONS"/*, NameResourceType=typeof(YBLocationResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="RouteLocations.Count()", ModelPartialType="RouteLocations.RouteLocation", BusinessObjectSetName = "RouteLocations")]
        public List<RouteLocations.RouteLocationModel> RouteLocations { get; set; }			
	
		[LocalizedDisplayName("STUDENTLOCATIONS"/*, NameResourceType=typeof(YBLocationResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="StudentLocations.Count()", ModelPartialType="StudentLocations.StudentLocation", BusinessObjectSetName = "StudentLocations")]
        public List<StudentLocations.StudentLocationModel> StudentLocations { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidLocation != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidLocation").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(YBLocationModel model){
            
		this.GuidLocation = model.GuidLocation;
		this.GuidLocationType = model.GuidLocationType;
		this.Lat = model.Lat;
		this.Long = model.Long;
		this.Description = model.Description;
		this.Address = model.Address;
		this.GuidCompany = model.GuidCompany;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.YBLocation GetBusinessObject()
        {
            BusinessObjects.YBLocation result = new BusinessObjects.YBLocation();


			       
	if (this.GuidLocation != null )
				result.GuidLocation = (Guid)this.GuidLocation;
				
	if (this.GuidLocationType != null )
				result.GuidLocationType = (Guid)this.GuidLocationType;
				
	if (this.Lat != null )
				result.Lat = (Double)this.Lat;
				
	if (this.Long != null )
				result.Long = (Double)this.Long;
				
	if (this.Description != null )
				result.Description = (String)this.Description.Trim().Replace("\t", String.Empty);
				
	if (this.Address != null )
				result.Address = (String)this.Address.Trim().Replace("\t", String.Empty);
				
	if (this.GuidCompany != null )
				result.GuidCompany = (Guid)this.GuidCompany;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			
			if(this.FkLocationType != null )
			result.LocationType = new BusinessObjects.LocationType() { GuidLocationType= (Guid)this.FkLocationType };
				

            return result;
        }
        public void Bind(BusinessObjects.YBLocation businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidLocation = businessObject.GuidLocation;
				
				
	if (businessObject.GuidLocationType != null )
				this.GuidLocationType = (Guid)businessObject.GuidLocationType;
				
	if (businessObject.Lat != null )
				this.Lat = (Double)businessObject.Lat;
				
	if (businessObject.Long != null )
				this.Long = (Double)businessObject.Long;
				
	if (businessObject.Description != null )
				this.Description = (String)businessObject.Description;
				
	if (businessObject.Address != null )
				this.Address = (String)businessObject.Address;
				
	if (businessObject.GuidCompany != null )
				this.GuidCompany = (Guid)businessObject.GuidCompany;
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.LocationType != null){
	                	this.FkLocationTypeText = businessObject.LocationType.Name != null ? businessObject.LocationType.Name.ToString() : "";; 
										
				this.FkLocationType = businessObject.LocationType.GuidLocationType;
                this.FkLocationTypeSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.LocationType,"GuidLocationType").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.YellowBox.Web.Mvc.Models.YBRoutes 
	{
	public partial class YBRouteModel: ModelBase{

	  public YBRouteModel(BO.YBRoute resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public YBRouteModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidRoute.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Title != null)
		
            return this.Title.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidRoute{ get; set; }
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("TITLE"/*, NameResourceType=typeof(YBRouteResources)*/)]
	public String   Title { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("GUIDCOMPANY"/*, NameResourceType=typeof(YBRouteResources)*/)]
	public Guid  ? GuidCompany { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(YBRouteResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(YBRouteResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(YBRouteResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(YBRouteResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(YBRouteResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(YBRouteResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
		
		[LocalizedDisplayName("ROUTELOCATIONS"/*, NameResourceType=typeof(YBRouteResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="RouteLocations.Count()", ModelPartialType="RouteLocations.RouteLocation", BusinessObjectSetName = "RouteLocations")]
        public List<RouteLocations.RouteLocationModel> RouteLocations { get; set; }			
	
		[LocalizedDisplayName("TRANSPORTS"/*, NameResourceType=typeof(YBRouteResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="Transports.Count()", ModelPartialType="Transports.Transport", BusinessObjectSetName = "Transports")]
        public List<Transports.TransportModel> Transports { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidRoute != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidRoute").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(YBRouteModel model){
            
		this.GuidRoute = model.GuidRoute;
		this.Title = model.Title;
		this.GuidCompany = model.GuidCompany;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.YBRoute GetBusinessObject()
        {
            BusinessObjects.YBRoute result = new BusinessObjects.YBRoute();


			       
	if (this.GuidRoute != null )
				result.GuidRoute = (Guid)this.GuidRoute;
				
	if (this.Title != null )
				result.Title = (String)this.Title.Trim().Replace("\t", String.Empty);
				
	if (this.GuidCompany != null )
				result.GuidCompany = (Guid)this.GuidCompany;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				

            return result;
        }
        public void Bind(BusinessObjects.YBRoute businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidRoute = businessObject.GuidRoute;
				
				
	if (businessObject.Title != null )
				this.Title = (String)businessObject.Title;
				
	if (businessObject.GuidCompany != null )
				this.GuidCompany = (Guid)businessObject.GuidCompany;
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
           
        }
	}
}
	namespace MBK.YellowBox.Web.Mvc.Models.StudentParents 
	{
	public partial class StudentParentModel: ModelBase{

	  public StudentParentModel(BO.StudentParent resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public StudentParentModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidStudentParent.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.FullName != null)
		
            return this.FullName.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidStudentParent{ get; set; }
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("FULLNAME"/*, NameResourceType=typeof(StudentParentResources)*/)]
	public String   FullName { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("GUIDCOMPANY"/*, NameResourceType=typeof(StudentParentResources)*/)]
	public Guid  ? GuidCompany { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(StudentParentResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(StudentParentResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(StudentParentResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(StudentParentResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(StudentParentResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(StudentParentResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
		
		[LocalizedDisplayName("STUDENTS"/*, NameResourceType=typeof(StudentParentResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="Students.Count()", ModelPartialType="Students.Student", BusinessObjectSetName = "Students")]
        public List<Students.StudentModel> Students { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidStudentParent != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidStudentParent").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(StudentParentModel model){
            
		this.GuidStudentParent = model.GuidStudentParent;
		this.FullName = model.FullName;
		this.GuidCompany = model.GuidCompany;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.StudentParent GetBusinessObject()
        {
            BusinessObjects.StudentParent result = new BusinessObjects.StudentParent();


			       
	if (this.GuidStudentParent != null )
				result.GuidStudentParent = (Guid)this.GuidStudentParent;
				
	if (this.FullName != null )
				result.FullName = (String)this.FullName.Trim().Replace("\t", String.Empty);
				
	if (this.GuidCompany != null )
				result.GuidCompany = (Guid)this.GuidCompany;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				

            return result;
        }
        public void Bind(BusinessObjects.StudentParent businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidStudentParent = businessObject.GuidStudentParent;
				
				
	if (businessObject.FullName != null )
				this.FullName = (String)businessObject.FullName;
				
	if (businessObject.GuidCompany != null )
				this.GuidCompany = (Guid)businessObject.GuidCompany;
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
           
        }
	}
}
	namespace MBK.YellowBox.Web.Mvc.Models.AcademyLevels 
	{
	public partial class AcademyLevelModel: ModelBase{

	  public AcademyLevelModel(BO.AcademyLevel resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public AcademyLevelModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidAcademyLevel.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Title != null)
		
            return this.Title.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidAcademyLevel{ get; set; }
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("TITLE"/*, NameResourceType=typeof(AcademyLevelResources)*/)]
	public String   Title { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("GUIDCOMPANY"/*, NameResourceType=typeof(AcademyLevelResources)*/)]
	public Guid  ? GuidCompany { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(AcademyLevelResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(AcademyLevelResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(AcademyLevelResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(AcademyLevelResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(AcademyLevelResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(AcademyLevelResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
		
		[LocalizedDisplayName("STUDENTS"/*, NameResourceType=typeof(AcademyLevelResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="Students.Count()", ModelPartialType="Students.Student", BusinessObjectSetName = "Students")]
        public List<Students.StudentModel> Students { get; set; }			
	
		[LocalizedDisplayName("PROFESORS"/*, NameResourceType=typeof(AcademyLevelResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="Profesors.Count()", ModelPartialType="Profesors.Profesor", BusinessObjectSetName = "Profesors")]
        public List<Profesors.ProfesorModel> Profesors { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidAcademyLevel != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidAcademyLevel").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(AcademyLevelModel model){
            
		this.GuidAcademyLevel = model.GuidAcademyLevel;
		this.Title = model.Title;
		this.GuidCompany = model.GuidCompany;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.AcademyLevel GetBusinessObject()
        {
            BusinessObjects.AcademyLevel result = new BusinessObjects.AcademyLevel();


			       
	if (this.GuidAcademyLevel != null )
				result.GuidAcademyLevel = (Guid)this.GuidAcademyLevel;
				
	if (this.Title != null )
				result.Title = (String)this.Title.Trim().Replace("\t", String.Empty);
				
	if (this.GuidCompany != null )
				result.GuidCompany = (Guid)this.GuidCompany;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				

            return result;
        }
        public void Bind(BusinessObjects.AcademyLevel businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidAcademyLevel = businessObject.GuidAcademyLevel;
				
				
	if (businessObject.Title != null )
				this.Title = (String)businessObject.Title;
				
	if (businessObject.GuidCompany != null )
				this.GuidCompany = (Guid)businessObject.GuidCompany;
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
           
        }
	}
}
	namespace MBK.YellowBox.Web.Mvc.Models.Profesors 
	{
	public partial class ProfesorModel: ModelBase{

	  public ProfesorModel(BO.Profesor resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProfesorModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidProfesor.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.FullName != null)
		
            return this.FullName.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidProfesor{ get; set; }
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("FULLNAME"/*, NameResourceType=typeof(ProfesorResources)*/)]
	public String   FullName { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDACADEMYLEVEL"/*, NameResourceType=typeof(ProfesorResources)*/)]
	public Guid  ? GuidAcademyLevel { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("GUIDCOMPANY"/*, NameResourceType=typeof(ProfesorResources)*/)]
	public Guid  ? GuidCompany { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProfesorResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProfesorResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProfesorResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProfesorResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProfesorResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProfesorResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.AcademyLevelsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Title", DataPropertyValue = "GuidAcademyLevel", FiltrablePropertyPathName="AcademyLevel.GuidAcademyLevel")]	

	[LocalizedDisplayName("ACADEMYLEVEL"/*, NameResourceType=typeof(ProfesorResources)*/)]
	public Guid  ? FkAcademyLevel { get; set; }
		[LocalizedDisplayName("ACADEMYLEVEL"/*, NameResourceType=typeof(ProfesorResources)*/)]
	[Exportable()]
	public string  FkAcademyLevelText { get; set; }
    public string FkAcademyLevelSafeKey { get; set; }

	
		
		
		[LocalizedDisplayName("STUDENTS"/*, NameResourceType=typeof(ProfesorResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="Students.Count()", ModelPartialType="Students.Student", BusinessObjectSetName = "Students")]
        public List<Students.StudentModel> Students { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidProfesor != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidProfesor").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProfesorModel model){
            
		this.GuidProfesor = model.GuidProfesor;
		this.FullName = model.FullName;
		this.GuidAcademyLevel = model.GuidAcademyLevel;
		this.GuidCompany = model.GuidCompany;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.Profesor GetBusinessObject()
        {
            BusinessObjects.Profesor result = new BusinessObjects.Profesor();


			       
	if (this.GuidProfesor != null )
				result.GuidProfesor = (Guid)this.GuidProfesor;
				
	if (this.FullName != null )
				result.FullName = (String)this.FullName.Trim().Replace("\t", String.Empty);
				
	if (this.GuidAcademyLevel != null )
				result.GuidAcademyLevel = (Guid)this.GuidAcademyLevel;
				
	if (this.GuidCompany != null )
				result.GuidCompany = (Guid)this.GuidCompany;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			
			if(this.FkAcademyLevel != null )
			result.AcademyLevel = new BusinessObjects.AcademyLevel() { GuidAcademyLevel= (Guid)this.FkAcademyLevel };
				

            return result;
        }
        public void Bind(BusinessObjects.Profesor businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidProfesor = businessObject.GuidProfesor;
				
				
	if (businessObject.FullName != null )
				this.FullName = (String)businessObject.FullName;
				
	if (businessObject.GuidAcademyLevel != null )
				this.GuidAcademyLevel = (Guid)businessObject.GuidAcademyLevel;
				
	if (businessObject.GuidCompany != null )
				this.GuidCompany = (Guid)businessObject.GuidCompany;
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.AcademyLevel != null){
	                	this.FkAcademyLevelText = businessObject.AcademyLevel.Title != null ? businessObject.AcademyLevel.Title.ToString() : "";; 
										
				this.FkAcademyLevel = businessObject.AcademyLevel.GuidAcademyLevel;
                this.FkAcademyLevelSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.AcademyLevel,"GuidAcademyLevel").Replace("/","-");

			}
           
        }
	}
}
