﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MBK.YellowBox.Web.Mvc.Models.Students;
using SFSdotNet.Framework.Web.Mvc;
using SFSdotNet.Framework.Web.Mvc.Models;
using MBK.YellowBox.BusinessObjects;
using MBK.YellowBox.Web.Mvc.Models;
using SFSdotNet.Framework.Globalization;
using SFSdotNet.Framework.Common.Entities;

namespace MBK.YellowBox.Web.Mvc.Controllers
{
    public partial class StudentsController
    {
        TextUI textUI = null;
        public StudentsController()
        {
            textUI = new SFSdotNet.Framework.Globalization.TextUI("MBKYellowBox", "Student", GetContextRequest());
        }

        [HttpPost()]
        public ActionResult MyCustomAction(StudentCustomModel model)
        {

            return ResolveResponse("El usuario e ha guardado", SFSdotNet.Framework.My.MessageResultTypes.Ok);
        }
        /// <summary>
        /// Parcialmente custom
        /// </summary>
        /// <returns></returns>
        public ActionResult MyCustomForm()
        {
            StudentCustomModel model = new Models.StudentCustomModel();
            var uiModel = GetUIModel(model, 2);
            uiModel.Properties.First(p => p.PropertyName == "EspacioVacio1").PartialViewName = "EspacioVacio1" ;
            uiModel.Properties.First(p => p.PropertyName == "EspacioVacio1").RemoveLayout = true;
            uiModel.ActionName = "MyCustomAction";
            uiModel.HeaderPartialView = "";
            //uiModel.AddEmptyField( "EspacioVacio2");
            uiModel.UIVersion = 2;
            uiModel.NewUILayoutTool = true;

            return ResolveView(uiModel, model);
        }

        public ActionResult MyCustomFormClean()
        {
            StudentCustomModel model = new Models.StudentCustomModel();
            var uiModel = GetUIModel(model, 2);
           
            //uiModel.ActionName = "MyCustomAction";
            
            //uiModel.AddEmptyField( "EspacioVacio2");
            uiModel.UIVersion = 2;
            uiModel.NewUILayoutTool = true;
            //uiModel.CleanLayout = true;
           uiModel.UILayoutFile = "~/Views/Templates/AdminLTE-Clean.cshtml";

            return ResolveView(uiModel, model);
        }


        public ActionResult GetProfesorsByAcademyLevel(Guid? AcademyLevel)
        {
            var profesors = BR.ProfesorsBR.Instance.GetBy(p=> p.AcademyLevel.GuidAcademyLevel == AcademyLevel);

            List<SelectListItem> result = new List<SelectListItem>();
            foreach (var profesor in profesors)
            {
                result.Add(new SelectListItem() { Text = profesor.FullName, Value = profesor.GuidProfesor.ToString() });
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        partial void OnGettingExtraData(object sender, MyEventArgs<UIModel<StudentModel>> e)
        {

            ExtraData extraData = new ExtraData();
            extraData.PropertyName = Student.PropertyNames.Profesor;
            extraData.ActionMethod = "GetProfesorsByAcademyLevel";
            extraData.ExtraParameters.Add(Student.PropertyNames.AcademyLevel);
            extraData.Data = new SelectList(new List<SelectListItem>(), "Text", "Value");
            e.UIModel.ExtraData.Add(extraData);

        }
        partial void OnShowing(object sender, MyEventArgs<UIModel<StudentModel>> e)
        {
            if (this.IsEditOrDetailsForm(e.UIModel))
            {
                e.UIModel.HeaderPartialView = "ScriptsForEdit";
                //e.UIModel.BodyPartialView = "ScriptsForEdit";

                e.UIModel.SetOrder(Student.PropertyNames.FullName, HorizontalFieldSizes.H_60_Percent);
                e.UIModel.SetOrder(Student.PropertyNames.BirthDate, HorizontalFieldSizes.H_40_Percent);

                e.UIModel.SetOrder(Student.PropertyNames.Comments);
                e.UIModel.AddEmptyField(HorizontalFieldSizes.H_100_Percent, "miespaciovacio");

               // e.UIModel.Properties.FirstOrDefault(p => p.PropertyName == "miespaciovacio").PartialViewName = "";
                var propertyComments = e.UIModel.Properties.FirstOrDefault(p=> p.PropertyName == Student.PropertyNames.Comments);
                propertyComments.IsMultiline = true;

                NameValue<string> nv = new NameValue<string>();

                propertyComments.PropertyDisplayName = textUI.GetText("Comments", nv.Add("es", "Comentarios"), nv.Add("en", "Comments"));
                e.UIModel.Properties.First(p => p.PropertyName == "Grade").PropertyDisplayName = textUI.GetText("MyGrade", nv.Add("es", "Grado"), nv.Add("en", "Grade"));
                e.UIModel.SetOrder(Student.PropertyNames.Grade, HorizontalFieldSizes.H_33_Percent);
                e.UIModel.SetOrder(Student.PropertyNames.Group, HorizontalFieldSizes.H_33_Percent);
                e.UIModel.SetOrder(Student.PropertyNames.StudentParent, HorizontalFieldSizes.H_33_Percent);


                e.UIModel.SetOrder(Student.PropertyNames.AcademyLevel, HorizontalFieldSizes.H_50_Percent);
                e.UIModel.SetOrder(Student.PropertyNames.Profesor, HorizontalFieldSizes.H_50_Percent);

            }

            if (this.IsItemsOrListForm(e.UIModel))
            {
                e.UIModel.HeaderPartialView = "ScriptsList";

                e.UIModel.SetOrder(Student.PropertyNames.FullName,true);
                e.UIModel.SetOrder(Student.PropertyNames.StudentParent);
                e.UIModel.SetOrder(Student.PropertyNames.AcademyLevel);

                e.UIModel.SetHide(Student.PropertyNames.Grade, true);
                e.UIModel.SetHide(Student.PropertyNames.Group, true);


            }
        }
    }
}