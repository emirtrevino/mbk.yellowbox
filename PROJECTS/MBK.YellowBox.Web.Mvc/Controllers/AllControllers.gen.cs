﻿// <Template>
//   <SolutionTemplate></SolutionTemplate>
//   <Version>20150126.0020</Version>
//   <Update>uiModel.ModuleNamespace</Update>
// </Template>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MBK.YellowBox.BR;
using System.Web.Script.Serialization;
using MBK.YellowBox.Web.Mvc.Models;
using MBK.YellowBox.Web.Mvc.Resources;
using BO = MBK.YellowBox.BusinessObjects;
using SFSdotNet.Framework.Web.Mvc.Security;
using SFSdotNet.Framework.Web.Mvc;
using SFSdotNet.Framework.Web.Mvc.Models;
using SFSdotNet.Framework.Web.Mvc.Resources;
using SFSdotNet.Framework.Web.Mvc.Controllers;
using MvcSiteMapProvider;
using System.Web.Routing;
using System.Collections;

using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using SFSdotNet.Framework.My;

using MBK.YellowBox.BusinessObjects;

namespace MBK.YellowBox.Web.Mvc.Controllers
{
	using MBK.YellowBox.Web.Mvc.Models.LocationTypes;

    public partial class LocationTypesController : MBK.YellowBox.Web.Mvc.ControllerBase<Models.LocationTypes.LocationTypeModel>
    {

       


	#region partial methods
        ControllerEventArgs<Models.LocationTypes.LocationTypeModel> e = null;
        partial void OnValidating(object sender, ControllerEventArgs<Models.LocationTypes.LocationTypeModel> e);
        partial void OnGettingExtraData(object sender, MyEventArgs<UIModel<Models.LocationTypes.LocationTypeModel>> e);
        partial void OnCreating(object sender, ControllerEventArgs<Models.LocationTypes.LocationTypeModel> e);
        partial void OnCreated(object sender, ControllerEventArgs<Models.LocationTypes.LocationTypeModel> e);
        partial void OnEditing(object sender, ControllerEventArgs<Models.LocationTypes.LocationTypeModel> e);
        partial void OnEdited(object sender, ControllerEventArgs<Models.LocationTypes.LocationTypeModel> e);
        partial void OnDeleting(object sender, ControllerEventArgs<Models.LocationTypes.LocationTypeModel> e);
        partial void OnDeleted(object sender, ControllerEventArgs<Models.LocationTypes.LocationTypeModel> e);
    	partial void OnShowing(object sender, MyEventArgs<UIModel<Models.LocationTypes.LocationTypeModel>> e);
    	partial void OnGettingByKey(object sender, ControllerEventArgs<Models.LocationTypes.LocationTypeModel> e);
        partial void OnTaken(object sender, ControllerEventArgs<Models.LocationTypes.LocationTypeModel> e);
       	partial void OnCreateShowing(object sender, ControllerEventArgs<Models.LocationTypes.LocationTypeModel> e);
		partial void OnEditShowing(object sender, ControllerEventArgs<Models.LocationTypes.LocationTypeModel> e);
		partial void OnDetailsShowing(object sender, ControllerEventArgs<Models.LocationTypes.LocationTypeModel> e);
 		partial void OnActionsCreated(object sender, MyEventArgs<UIModel<Models.LocationTypes.LocationTypeModel >> e);
		partial void OnCustomActionExecuting(object sender, MyEventArgs<ContextActionModel<Models.LocationTypes.LocationTypeModel>> e);
		partial void OnCustomActionExecutingBackground(object sender, MyEventArgs<ContextActionModel<Models.LocationTypes.LocationTypeModel>> e);
        partial void OnDownloading(object sender, MyEventArgs<ContextActionModel<Models.LocationTypes.LocationTypeModel>> e);
      	partial void OnAuthorization(object sender, AuthorizationContext context);
		 partial void OnFilterShowing(object sender, MyEventArgs<UIModel<Models.LocationTypes.LocationTypeModel >> e);
         partial void OnSummaryOperationShowing(object sender, MyEventArgs<UIModel<Models.LocationTypes.LocationTypeModel>> e);

        partial void OnExportActionsCreated(object sender, MyEventArgs<UIModel<Models.LocationTypes.LocationTypeModel>> e);


		protected override void OnVirtualFilterShowing(object sender, MyEventArgs<UIModel<LocationTypeModel>> e)
        {
            OnFilterShowing(sender, e);
        }
		 public override void OnVirtualExportActionsCreated(object sender, MyEventArgs<UIModel<LocationTypeModel>> e)
        {
            OnExportActionsCreated(sender, e);
        }
        public override void OnVirtualDownloading(object sender, MyEventArgs<ContextActionModel<LocationTypeModel>> e)
        {
            OnDownloading(sender, e);
        }
        public override void OnVirtualShowing(object sender, MyEventArgs<UIModel<LocationTypeModel>> e)
        {
            OnShowing(sender, e);
        }

	#endregion
	#region API
	 public override ActionResult ApiCreateGen(LocationTypeModel model, ContextRequest contextRequest)
        {
            return CreateGen(model, contextRequest);
        }

              public override ActionResult ApiGetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJson(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }
        public override ActionResult ApiGetByKeyJson(string id, ContextRequest contextRequest)
        {
            return  GetByKeyJson(id, contextRequest, true);
        }
      
		 public override int ApiGetByCount(string filter, ContextRequest contextRequest)
        {
            return GetByCount(filter, contextRequest);
        }
         protected override ActionResult ApiDeleteGen(List<LocationTypeModel> models, ContextRequest contextRequest)
        {
            List<LocationType> objs = new List<LocationType>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                BR.LocationTypesBR.Instance.DeleteBulk(objs, contextRequest);
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        protected override ActionResult ApiUpdateGen(List<LocationTypeModel> models, ContextRequest contextRequest)
        {
            List<LocationType> objs = new List<LocationType>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                foreach (var obj in objs)
                {
                    BR.LocationTypesBR.Instance.Update(obj, contextRequest);

                }
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }


	#endregion
#region Validation methods	
	    private void Validations(LocationTypeModel model) { 
            #region Remote validations

            #endregion
		}

#endregion
		
 		public AuthorizationContext Authorization(AuthorizationContext context)
        {
            OnAuthorization(this,  context );
            return context ;
        }
		public List<LocationTypeModel> GetAll() {
            			var bos = BR.LocationTypesBR.Instance.GetBy("",
					new SFSdotNet.Framework.My.ContextRequest()
					{
						CustomQuery = new SFSdotNet.Framework.My.CustomQuery()
						{
							OrderBy = "Name",
							SortDirection = SFSdotNet.Framework.Data.SortDirection.Ascending
						}
					});
            			List<LocationTypeModel> results = new List<LocationTypeModel>();
            LocationTypeModel model = null;
            foreach (var bo in bos)
            {
                model = new LocationTypeModel();
                model.Bind(bo);
                results.Add(model);
            }
            return results;

        }
        //
        // GET: /LocationTypes/
		[MyAuthorize("r", "LocationType", "MBKYellowBox", typeof(LocationTypesController))]
		public ActionResult Index()
        {
    		var uiModel = GetContextModel(UIModelContextTypes.ListForm, null);
			ViewBag.UIModel = uiModel;
			uiModel.FilterStart = (string)ViewData["startFilter"];
                    MyEventArgs<UIModel<LocationTypeModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<LocationTypeModel>>() { Object = uiModel });

			OnExportActionsCreated(this, (me != null ? me : me = new MyEventArgs<UIModel<LocationTypeModel>>() { Object = uiModel }));

            if (me != null)
            {
                uiModel = me.Object;
            }
            if (me == null)
                me = new MyEventArgs<UIModel<LocationTypeModel>>() { Object = uiModel };
           
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;


            //return View("ListGen");
			return ResolveView(uiModel);
        }
		[MyAuthorize("r", "LocationType", "MBKYellowBox", typeof(LocationTypesController))]
		public ActionResult ListViewGen(string idTab, string fk , string fkValue, string startFilter, ListModes  listmode  = ListModes.SimpleList, PropertyDefinition parentRelationProperty = null, object parentRelationPropertyValue = null )
        {
			ViewData["idTab"] = System.Web.HttpContext.Current.Request.QueryString["idTab"]; 
		 	ViewData["detpop"] = true; // details in popup
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"])) {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"]; 
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
            {
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["startFilter"]))
            {
                ViewData["startFilter"] = Request.QueryString["startFilter"];
            }
			
			UIModel<LocationTypeModel> uiModel = GetContextModel(UIModelContextTypes.ListForm, null);

            MyEventArgs<UIModel<LocationTypeModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<LocationTypeModel>>() { Object = uiModel });
            if (me == null)
                me = new MyEventArgs<UIModel<LocationTypeModel>>() { Object = uiModel };
            uiModel.Properties = GetProperties(uiModel);
            uiModel.ContextType = UIModelContextTypes.ListForm;
             uiModel.FilterStart = (string)ViewData["startFilter"];
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;
			 if (listmode == ListModes.SimpleList)
                return ResolveView(uiModel);
            else
            {
                ViewData["parentRelationProperty"] = parentRelationProperty;
                ViewData["parentRelationPropertyValue"] = parentRelationPropertyValue;
                return PartialView("ListForTagSelectView");
            }
            return ResolveView(uiModel);
        }
		List<PropertyDefinition> _properties = null;

		 protected override List<PropertyDefinition> GetProperties(UIModel uiModel,  params string[] specificProperties)
        { 
            return GetProperties(uiModel, false, null, specificProperties);
        }

		protected override List<PropertyDefinition> GetProperties(UIModel uiModel, bool decripted, Guid? id, params string[] specificProperties)
            {

			bool allProperties = true;    
                if (specificProperties != null && specificProperties.Length > 0)
                {
                    allProperties = false;
                }


			List<CustomProperty> customProperties = new List<CustomProperty>();
			if (_properties == null)
                {
                List<PropertyDefinition> results = new List<PropertyDefinition>();

			string idLocationType = GetRouteDataOrQueryParam("id");
			if (idLocationType != null)
			{
				if (!decripted)
                {
					idLocationType = SFSdotNet.Framework.Entities.Utils.GetPropertyKey(idLocationType.Replace("-","/"), "GuidLocationType");
				}else{
					if (id != null )
						idLocationType = id.Value.ToString();                

				}
			}

			bool visibleProperty = true;	
			 bool conditionalshow =false;
                if (uiModel.ContextType == UIModelContextTypes.EditForm || uiModel.ContextType == UIModelContextTypes.DisplayForm ||  uiModel.ContextType == UIModelContextTypes.GenericForm )
                    conditionalshow = true;
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidLocationType"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidLocationType")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 100,
																	
					CustomProperties = customProperties,

                    PropertyName = "GuidLocationType",

					 MaxLength = 0,
					IsRequired = true ,
					IsHidden = true,
                    SystemProperty =  SystemProperties.Identifier ,
					IsDefaultProperty = false,
                    SortBy = "GuidLocationType",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.LocationTypeResources.GUIDLOCATIONTYPE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Name"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Name")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 101,
																	
					CustomProperties = customProperties,

                    PropertyName = "Name",

					 MaxLength = 255,
					 Nullable = true,
					IsDefaultProperty = true,
                    SortBy = "Name",
					
	
                    TypeName = "String",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.LocationTypeResources.NAME*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("NameKey"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "NameKey")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 102,
																	
					CustomProperties = customProperties,

                    PropertyName = "NameKey",

					 MaxLength = 20,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "NameKey",
					
	
                    TypeName = "String",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.LocationTypeResources.NAMEKEY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 103,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedDate",
					
	
				SystemProperty = SystemProperties.CreatedDate ,
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.LocationTypeResources.CREATEDDATE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 115,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedDate",
					
	
					IsUpdatedDate = true,
					SystemProperty = SystemProperties.UpdatedDate ,
	
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    ,PropertyDisplayName = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.UPDATED

                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 105,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedBy",
					
	
				SystemProperty = SystemProperties.CreatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.LocationTypeResources.CREATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 106,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedBy",
					
	
				SystemProperty = SystemProperties.UpdatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.LocationTypeResources.UPDATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Bytes"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Bytes")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 107,
																	
					CustomProperties = customProperties,

                    PropertyName = "Bytes",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Bytes",
					
	
				SystemProperty = SystemProperties.SizeBytes,
                    TypeName = "Int32",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.LocationTypeResources.BYTES*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("YBLocations"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"LocationType" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="YBLocations.GuidLocation")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"YBLocations.GuidLocation" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"YBLocations" });
			

	
	//fk_Location_LocationType
		//if (this.Request.QueryString["fk"] != "YBLocations")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 108,
																
					Link = VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/YBLocations/ListViewGen?overrideModule=" + GetOverrideApp()  + "&pal=False&es=False&pag=10&filterlinks=1&idTab=YBLocations&fk=LocationType&startFilter="+ (new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext)).Encode("it.LocationType.GuidLocationType = Guid(\"" + idLocationType +"\")")+ "&fkValue=" + idLocationType,
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "YBLocation",
					
					CustomProperties = customProperties,

                    PropertyName = "YBLocations",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "YBLocations.Description",
					
	
                    TypeName = "MBKYellowBoxModel.YBLocation",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = true,
                    PathName = "MBKYellowBox/YBLocations"
                    /*,PropertyDisplayName = Resources.LocationTypeResources.YBLOCATIONS*/
                });
		//	}
	
	}
	
				
                    _properties = results;
                    return _properties;
                }
                else {
                    return _properties;
                }
            }

		protected override  UIModel<LocationTypeModel> GetByForShow(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, params  object[] extraParams)
        {
			if (Request != null )
				if (!string.IsNullOrEmpty(Request.QueryString["q"]))
					filter = filter + HttpUtility.UrlDecode(Request.QueryString["q"]);
 if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
            }
            var bos = BR.LocationTypesBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), contextRequest, extraParams);
			//var bos = BR.LocationTypesBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), context, extraParams);
            LocationTypeModel model = null;
            List<LocationTypeModel> results = new List<LocationTypeModel>();
            foreach (var item in bos)
            {
                model = new LocationTypeModel();
				model.Bind(item);
				results.Add(model);
            }
            //return results;
			UIModel<LocationTypeModel> uiModel = GetContextModel(UIModelContextTypes.Items, null);
            uiModel.Items = results;
			if (Request != null){
				if (SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(Request.RequestContext, "action") == "Download")
				{
					uiModel.ContextType = UIModelContextTypes.ExportDownload;
				}
			}
            Showing(ref uiModel);
            return uiModel;
		}			
		
		//public List<LocationTypeModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params  object[] extraParams)
        //{
		//	var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, null, extraParams);
        public override List<LocationTypeModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest,  params  object[] extraParams)
        {
            var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
           
            return uiModel.Items;
		
        }
		/*
        [MyAuthorize("r", "LocationType", "MBKYellowBox", typeof(LocationTypesController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir)
        {
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir);
        }*/

		  [MyAuthorize("r", "LocationType", "MBKYellowBox", typeof(LocationTypesController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir,ContextRequest contextRequest,  object[] extraParams)
        {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir,contextRequest, extraParams);
        }
/*		  [MyAuthorize("r", "LocationType", "MBKYellowBox", typeof(LocationTypesController))]
       public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJsonBase(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }*/
		[MyAuthorize()]
		public int GetByCount(string filter, ContextRequest contextRequest) {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
            return BR.LocationTypesBR.Instance.GetCount(HttpUtility.UrlDecode(filter), GetUseMode(), contextRequest);
        }
		

		[MyAuthorize("r", "LocationType", "MBKYellowBox", typeof(LocationTypesController))]
        public ActionResult GetByKeyJson(string id, ContextRequest contextRequest,  bool dec = false)
        {
            return Json(GetByKey(id, null, contextRequest, dec), JsonRequestBehavior.AllowGet);
        }
		public LocationTypeModel GetByKey(string id) {
			return GetByKey(id, null,null, false);
       	}
		    public LocationTypeModel GetByKey(string id, string includes)
        {
            return GetByKey(id, includes, false);
        }
		 public  LocationTypeModel GetByKey(string id, string includes, ContextRequest contextRequest)
        {
            return GetByKey(id, includes, contextRequest, false);
        }
		/*
		  public ActionResult ShowField(string fieldName, string idField) {
		   string safePropertyName = fieldName;
              if (fieldName.StartsWith("Fk"))
              {
                  safePropertyName = fieldName.Substring(2, fieldName.Length - 2);
              }

             LocationTypeModel model = new  LocationTypeModel();

            UIModel uiModel = GetUIModel(model, new string[] { "NoField-" });
			
				uiModel.Properties = GetProperties(uiModel, safePropertyName);
		uiModel.Properties.ForEach(p=> p.ContextType = uiModel.ContextType );
            uiModel.ContextType = UIModelContextTypes.FilterFields;
            uiModel.OverrideApp = GetOverrideApp();
            uiModel.UseMode = GetUseMode();

            ViewData["uiModel"] = uiModel;
			var prop = uiModel.Properties.FirstOrDefault(p=>p.PropertyName == safePropertyName);
            //if (prop.IsNavigationProperty && prop.IsNavigationPropertyMany == false)
            //{
            //    ViewData["currentProperty"] = uiModel.Properties.FirstOrDefault(p => p.PropertyName != fieldName + "Text");
            //}else if (prop.IsNavigationProperty == false){
                ViewData["currentProperty"] = prop;
           // }
            ((PropertyDefinition)ViewData["currentProperty"]).RemoveLayout = true;
			ViewData["withContainer"] = false;


            return PartialView("GenericField", model);


        }
      */
	public LocationTypeModel GetByKey(string id, ContextRequest contextRequest, bool dec)
        {
            return GetByKey(id, null, contextRequest, dec);
        }
        public LocationTypeModel GetByKey(string id, string  includes, bool dec)
        {
            return GetByKey(id, includes, null, dec);
        }

        public LocationTypeModel GetByKey(string id, string includes, ContextRequest contextRequest, bool dec) {
		             LocationTypeModel model = null;
            ControllerEventArgs<LocationTypeModel> e = null;
			string objectKey = id.Replace("-","/");
             OnGettingByKey(this, e=  new ControllerEventArgs<LocationTypeModel>() { Id = objectKey  });
             bool cancel = false;
             LocationTypeModel eItem = null;
             if (e != null)
             {
                 cancel = e.Cancel;
                 eItem = e.Item;
             }
			if (cancel == false && eItem == null)
             {
			Guid guidLocationType = Guid.Empty; //new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, "GuidLocationType"));
			if (dec)
                 {
                     guidLocationType = new Guid(id);
                 }
                 else
                 {
                     guidLocationType = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, null));
                 }
			
            
				model = new LocationTypeModel();
                  if (contextRequest == null)
                {
                    contextRequest = GetContextRequest();
                }
				var bo = BR.LocationTypesBR.Instance.GetByKey(guidLocationType, GetUseMode(), contextRequest,  includes);
				 if (bo != null)
                    model.Bind(bo);
                else
                    return null;
			}
             else {
                 model = eItem;
             }
			model.IsNew = false;

            return model;
        }
        // GET: /LocationTypes/DetailsGen/5
		[MyAuthorize("r", "LocationType", "MBKYellowBox", typeof(LocationTypesController))]
        public ActionResult DetailsGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = LocationTypeResources.ENTITY_PLURAL;
			 #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<LocationTypeModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
            #endregion



			 bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null) {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
			//UIModel<LocationTypeModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, decripted), decripted, guidId);
			var item = GetByKey(id, null, null, decripted);
			if (item == null)
            {
                 RouteValueDictionary rv = new RouteValueDictionary();
                string usemode = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"usemode");
                string overrideModule = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "overrideModule");
                string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");

                if(!string.IsNullOrEmpty(usemode)){
                    rv.Add("usemode", usemode);
                }
                if(!string.IsNullOrEmpty(overrideModule)){
                    rv.Add("overrideModule", overrideModule);
                }
                if (!string.IsNullOrEmpty(area))
                {
                    rv.Add("area", area);
                }

                return RedirectToAction("Index", rv);
            }
            //
            UIModel<LocationTypeModel> uiModel = null;
                uiModel = GetContextModel(UIModelContextTypes.DisplayForm, item, decripted, guidId);



            MyEventArgs<UIModel<LocationTypeModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<LocationTypeModel>>() { Object = uiModel });

            if (me != null) {
                uiModel = me.Object;
            }
			
            Showing(ref uiModel);
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			
            //return View("DisplayGen", uiModel.Items[0]);
			return ResolveView(uiModel, uiModel.Items[0]);

        }
		[MyAuthorize("r", "LocationType", "MBKYellowBox", typeof(LocationTypesController))]
		public ActionResult DetailsViewGen(string id)
        {

		 bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<LocationTypeModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
           
			if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
 			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
           
        	 //var uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id));
			 
            bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
            {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true")
                {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
            UIModel<LocationTypeModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, null, decripted), decripted, guidId);
			

            MyEventArgs<UIModel<LocationTypeModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<LocationTypeModel>>() { Object = uiModel });

            if (me != null)
            {
                uiModel = me.Object;
            }
            
            Showing(ref uiModel);
            return ResolveView(uiModel, uiModel.Items[0]);
        
        }
        //
        // GET: /LocationTypes/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        //
        // GET: /LocationTypes/CreateGen
		[MyAuthorize("c", "LocationType", "MBKYellowBox", typeof(LocationTypesController))]
        public ActionResult CreateGen()
        {
			LocationTypeModel model = new LocationTypeModel();
            model.IsNew = true;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model);

			OnCreateShowing(this, e = new ControllerEventArgs<LocationTypeModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }

             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                 ViewData["ispopup"] = true;
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                 ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                 ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

            Showing(ref me);

			return ResolveView(me, me.Items[0]);
        } 
			
		protected override UIModel<LocationTypeModel> GetContextModel(UIModelContextTypes formMode, LocationTypeModel model)
        {
            return GetContextModel(formMode, model, false, null);
        }
			
		 private UIModel<LocationTypeModel> GetContextModel(UIModelContextTypes formMode, LocationTypeModel model, bool decript, Guid ? id) {
            UIModel<LocationTypeModel> me = new UIModel<LocationTypeModel>(true, "LocationTypes");
			me.UseMode = GetUseMode();
			me.Controller = this;
			me.OverrideApp = GetOverrideApp();
			me.ContextType = formMode ;
			me.Id = "LocationType";
			
            me.ModuleKey = "MBKYellowBox";

			me.ModuleNamespace = "MBK.YellowBox";
            me.EntityKey = "LocationType";
            me.EntitySetName = "LocationTypes";

			me.AreaAction = "MBKYellowBox";
            me.ControllerAction = "LocationTypes";
            me.PropertyKeyName = "GuidLocationType";

            me.Properties = GetProperties(me, decript, id);

			me.SortBy = "UpdatedDate";
			me.SortDirection = UIModelSortDirection.DESC;

 			
			if (Request != null)
            {
                string actionName = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam( Request.RequestContext, "action");
                if(actionName != null && actionName.ToLower().Contains("create") ){
                    me.IsNew = true;
                }
            }
			 #region Buttons
			 if (Request != null ){
             if (formMode == UIModelContextTypes.DisplayForm || formMode == UIModelContextTypes.EditForm || formMode == UIModelContextTypes.ListForm)
				me.ActionButtons = GetActionButtons(formMode,model != null ?(Request.QueryString["dec"] == "true" ? model.Id : model.SafeKey)  : null, "MBKYellowBox", "LocationTypes", "LocationType", me.IsNew);

            //me.ActionButtons.Add(new ActionModel() { ActionKey = "return", Title = GlobalMessages.RETURN, Url = System.Web.VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/LocationTypes" });
			if (this.HttpContext != null &&  !this.HttpContext.SkipAuthorization){
				//antes this.HttpContext
				me.SetAction("u", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("u", "LocationType", "MBKYellowBox"));
				me.SetAction("c", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("c", "LocationType", "MBKYellowBox"));
				me.SetAction("d", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("d", "LocationType", "MBKYellowBox"));
			
			}else{
				me.SetAction("u", true);
				me.SetAction("c", true);
				me.SetAction("d", true);

			}
            #endregion              
         
            switch (formMode)
            {
                case UIModelContextTypes.DisplayForm:
					//me.TitleForm = LocationTypeResources.LOCATIONTYPES_DETAILS;
                    me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.MODIFY_DATA;
					 me.Properties.Where(p=>p.PropertyName  != "Id" && p.IsForeignKey == false).ToList().ForEach(p => p.IsHidden = false);

					 me.Properties.Where(p => (p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier) ).ToList().ForEach(p=> me.SetHide(p.PropertyName));

                    break;
                case UIModelContextTypes.EditForm:
				  me.Properties.Where(p=>p.SystemProperty != SystemProperties.Identifier && p.IsForeignKey == false && p.PropertyName != "Id").ToList().ForEach(p => p.IsHidden = false);

					if (model != null)
                    {
						

                        me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.SAVE_DATA;                        
                        me.ActionButtons.First(p => p.ActionKey == "c").Title = GlobalMessages.SAVE_DATA;
						if (model.IsNew ){
							//me.TitleForm = LocationTypeResources.LOCATIONTYPES_ADD_NEW;
							me.ActionName = "CreateGen";
							me.Properties.RemoveAll(p => p.SystemProperty != null || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));
						}else{
							
							me.ActionName = "EditGen";

							//me.TitleForm = LocationTypeResources.LOCATIONTYPES_EDIT;
							me.Properties.RemoveAll(p => p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));	
						}
						//me.Properties.Remove(me.Properties.Find(p => p.PropertyName == "UpdatedDate"));
					
					}
                    break;
                case UIModelContextTypes.FilterFields:
                    break;
                case UIModelContextTypes.GenericForm:
                    break;
                case UIModelContextTypes.Items:
				//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "Name") != null){
						me.Properties.Find(p => p.PropertyName == "Name").IsHidden = false;
					 }
					 
                    
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					 
                    
					

						 if (me.Properties.Find(p => p.PropertyName == "GuidLocationType") != null){
						me.Properties.Find(p => p.PropertyName == "GuidLocationType").IsHidden = false;
					 }
					 
                    
					


                  


					//}
                    break;
                case UIModelContextTypes.ListForm:
					PropertyDefinition propFinded = null;
					//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "Name") != null){
						me.Properties.Find(p => p.PropertyName == "Name").IsHidden = false;
					 }
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					
					me.PrincipalActionName = "GetByJson";
					//}
					//me.TitleForm = LocationTypeResources.LOCATIONTYPES_LIST;
                    break;
                default:
                    break;
            }
            	this.SetDefaultProperties(me);
			}
			if (model != null )
            	me.Items.Add(model);
            return me;
        }
		// GET: /LocationTypes/CreateViewGen
		[MyAuthorize("c", "LocationType", "MBKYellowBox", typeof(LocationTypesController))]
        public ActionResult CreateViewGen()
        {
				LocationTypeModel model = new LocationTypeModel();
            model.IsNew = true;
			e= null;
			OnCreateShowing(this, e = new ControllerEventArgs<LocationTypeModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }
			
            var me = GetContextModel(UIModelContextTypes.EditForm, model);

			me.IsPartialView = true;	
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
            {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                me.Properties.Find(p => p.PropertyName == ViewData["fk"].ToString()).IsReadOnly = true;
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
			
      
            //me.Items.Add(model);
            Showing(ref me);
            return ResolveView(me, me.Items[0]);
        }
		protected override  void GettingExtraData(ref UIModel<LocationTypeModel> uiModel)
        {

            MyEventArgs<UIModel<LocationTypeModel>> me = null;
            OnGettingExtraData(this, me = new MyEventArgs<UIModel<LocationTypeModel>>() { Object = uiModel });
            //bool maybeAnyReplaced = false; 
            if (me != null)
            {
                uiModel = me.Object;
                //maybeAnyReplaced = true;
            }
           
			bool canFill = false;
			 string query = null ;
            bool isFK = false;
			PropertyDefinition prop =null;
			var contextRequest = this.GetContextRequest();
            contextRequest.CustomParams.Add(new CustomParam() { Name="ui", Value= LocationType.EntityName });

                        

        }
		private void Showing(ref UIModel<LocationTypeModel> uiModel) {
          	
			MyEventArgs<UIModel<LocationTypeModel>> me = new MyEventArgs<UIModel<LocationTypeModel>>() { Object = uiModel };
			 OnVirtualLayoutSettings(this, me);


            OnShowing(this, me);

			
			if ((Request != null && Request.QueryString["allFields"] == "1") || Request == null )
			{
				me.Object.Properties.ForEach(p=> p.IsHidden = false);
            }
            if (me != null)
            {
                uiModel = me.Object;
            }
          


			 if (uiModel.ContextType == UIModelContextTypes.EditForm)
			    GettingExtraData(ref uiModel);
            ViewData["UIModel"] = uiModel;

        }
        //
        // POST: /LocationTypes/Create
		[MyAuthorize("c", "LocationType", "MBKYellowBox", typeof(LocationTypesController))]
        [HttpPost]
		[ValidateInput(false)] 
        public ActionResult CreateGen(LocationTypeModel  model,  ContextRequest contextRequest)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
		 	e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<LocationTypeModel>() { Item = model });
           
		  	if (!ModelState.IsValid) {
				model.IsNew = true;
				var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
                 if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                        ViewData["ispopup"] = true;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
                    return ResolveView(me, model);
            }
            try
            {
				if (model.GuidLocationType == null || model.GuidLocationType.ToString().Contains("000000000"))
				model.GuidLocationType = Guid.NewGuid();
	
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnCreating(this, e = new ControllerEventArgs<LocationTypeModel>() { Item = model });
                if (e != null) {
                   if (e.Cancel && e.RedirectValues.Count > 0){
                        RouteValueDictionary rv = new RouteValueDictionary();
                        if (e.RedirectValues["area"] != null ){
                            rv.Add("area", e.RedirectValues["area"].ToString());
                        }
                        foreach (var item in e.RedirectValues.Where(p=>p.Key != "area" && p.Key != "controller" &&  p.Key != "action" ))
	                    {
		                    rv.Add(item.Key, item.Value);
	                    }

                        //if (e.RedirectValues["action"] != null && e.RedirectValues["controller"] != null && e.RedirectValues["area"] != null )
                        return RedirectToAction(e.RedirectValues["action"].ToString(), e.RedirectValues["controller"].ToString(), rv );


                        
                    }else if (e.Cancel && e.ActionResult != null )
                        return e.ActionResult;  
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				if (contextRequest == null || contextRequest.Company == null){
					contextRequest = GetContextRequest();
					
				}
                if (!cancel)
                	model.Bind(LocationTypesBR.Instance.Create(model.GetBusinessObject(), contextRequest ));
				OnCreated(this, e = new ControllerEventArgs<LocationTypeModel>() { Item = model });
                 if (e != null )
					if (e.ActionResult != null)
                    	replaceResult = true;		
				if (!replaceResult)
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))
                    {
                        ViewData["__continue"] = true;
                    }
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"]) &&  string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
                        popupextra.Add("id", model.SafeKey);
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                       {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                            popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"area"));
                            popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                            popupextra.Add("action", actionDetails);
                       if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
                    {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))

                        {
                            var popupextra = GetRouteData();
                            popupextra.Add("id", model.SafeKey);
                            return RedirectToAction("EditViewGen", popupextra);
                        }
                        else
                        {
                            return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.ADD_DONE));
                        }
                    }        			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                        return Redirect(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]);
                    else{

							RouteValueDictionary popupextra = null; 
							if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"])){
                            popupextra = GetRouteData();
							 string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
                            if (!string.IsNullOrEmpty(area))
                                popupextra.Add("area", area);
                            
                            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"])) {
								popupextra.Add("id", model.SafeKey);
                                return RedirectToAction("EditGen", popupextra);

                            }else{
								
                            return RedirectToAction("Index", popupextra);
							}
							}else{
								return Content("ok");
							}
                        }
						 }
                else {
                    return e.ActionResult;
                    }
				}
            catch(Exception ex)
            {
					if (!string.IsNullOrEmpty(Request.QueryString["rok"]))
                {
                    throw  ex;
                }
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                model.IsNew = true;
                var me = GetContextModel(UIModelContextTypes.EditForm, model, true, model.GuidLocationType);
                Showing(ref me);
                if (isPopUp)
                {
                    
                        ViewData["ispopup"] = isPopUp;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
					if (Request != null)
						return ResolveView(me, model);
					else
						return Content("ok");
            }
        }        
        //
        // GET: /LocationTypes/Edit/5 
        public ActionResult Edit(int id)
        {
            return View();
        }
			
		
		[MyAuthorize("u", "LocationType", "MBKYellowBox", typeof(LocationTypesController))]
		[MvcSiteMapNode(Area="MBKYellowBox", Title="sss", Clickable=false, ParentKey = "MBKYellowBox_LocationType_List")]
		public ActionResult EditGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = LocationTypeResources.ENTITY_SINGLE;		 	
  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<LocationTypeModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
            LocationTypeModel model = null;
            // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
			bool dec = false;
            Guid ? idGuidDecripted = null ;
            if (Request != null && Request.QueryString["dec"] == "true")
            {
                dec = true;
                idGuidDecripted = Guid.Parse(id);
            }

            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model,dec,idGuidDecripted);
            Showing(ref me);


            if (!replaceResult)
            {
                 //return View("EditGen", me.Items[0]);
				 return ResolveView(me, me.Items[0]);
            }
            else {
                return e.ActionResult;
            }
        }
			[MyAuthorize("u", "LocationType","MBKYellowBox", typeof(LocationTypesController))]
		public ActionResult EditViewGen(string id)
        {
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

					  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<LocationTypeModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
			
            LocationTypeModel model = null;
			 bool dec = false;
            Guid? guidId = null ;

            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null && System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                dec = true;
                guidId = Guid.Parse(id);
            }
            // si fue implementado el método parcial y no se ha decidido suspender la acción
            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
            var me = GetContextModel(UIModelContextTypes.EditForm, model, dec, guidId);
            Showing(ref me);

            return ResolveView(me, model);
        }
		[MyAuthorize("u", "LocationType",  "MBKYellowBox", typeof(LocationTypesController))]
		[HttpPost]
		[ValidateInput(false)] 
		        public ActionResult EditGen(LocationTypeModel model)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
			e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<LocationTypeModel>() { Item = model });
           
            if (!ModelState.IsValid)
            {
			   	var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
			
				if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"])){
                	ViewData["ispopup"] = true;
					return ResolveView(me, model);
				}
				else
					return ResolveView(me, model);
            }
            try
            {
			
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnEditing(this, e = new ControllerEventArgs<LocationTypeModel>() { Item = model });
                if (e != null) {
                    if (e.Cancel && e.ActionResult != null)
                        return e.ActionResult;
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				ContextRequest context = new ContextRequest();
                context.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;

                LocationType resultObj = null;
			    if (!cancel)
                	resultObj = LocationTypesBR.Instance.Update(model.GetBusinessObject(), GetContextRequest());
				
				OnEdited(this, e = new ControllerEventArgs<LocationTypeModel>() { Item =   new LocationTypeModel(resultObj) });
				if (e != null && e.ActionResult != null) replaceResult = true; 

                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Content("ok");
                }
                else
                {
				if (!replaceResult)
                {
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"])  && string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
						 if (Request != null && Request.QueryString["dec"] == "true")
                        {
                            popupextra.Add("id", model.Id);
                        }
                        else
                        {
							popupextra.Add("id", model.SafeKey);

							
                        }
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                        {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                        popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area"));
                        popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                        popupextra.Add("action", actionDetails);
                        if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
					if (isPopUp)
						return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.UPDATE_DONE));
        			    string returnUrl = null;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"])) {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.Form["ReturnAfter"];
                        else if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"];
                    }
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else{
		RouteValueDictionary popupextra = null; 
						 if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"]))
                            {
							
							popupextra = GetRouteData();
							string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
							if (!string.IsNullOrEmpty(area))
								popupextra.Add("area", area);

							return RedirectToAction("Index", popupextra);
						}else{
							return Content("ok");
						}
						}
				 }
                else {
                    return e.ActionResult;
				}
                }		
            }
          catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
			    if (isPopUp)
                {
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(ex.Message));
                    
                }
                else
                {
                    SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                    
                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
                else {
						  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
							ViewData["ispopup"] = true;
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
							ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
							ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

						var me = GetContextModel(UIModelContextTypes.EditForm, model);
						Showing(ref me);

						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
						{
							ViewData["ispopup"] = true;
							return ResolveView(me, model);
						}
						else
							return ResolveView(me, model);

						
					}
				}
            }
        }
        //
        // POST: /LocationTypes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                //  Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /LocationTypes/Delete/5
        
		[MyAuthorize("d", "LocationType", "MBKYellowBox", typeof(LocationTypesController))]
		[HttpDelete]
        public ActionResult DeleteGen(string objectKey, string extraParams)
        {
            try
            {
					
			
				Guid guidLocationType = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey.Replace("-", "/"), "GuidLocationType")); 
                BO.LocationType entity = new BO.LocationType() { GuidLocationType = guidLocationType };

                BR.LocationTypesBR.Instance.Delete(entity, GetContextRequest());               
                return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.DELETE_DONE));

            }
            catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null)
                    {
                        message = ex.Data["usermessage"].ToString();
                    }

                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }
            }
        }
		/*[MyAuthorize()]
		public FileMediaResult Download(string query, bool? allSelected = false,  string selected = null , string orderBy = null , string direction = null , string format = null , string actionKey=null )
        {
			
            List<Guid> keysSelected1 = new List<Guid>();
            if (!string.IsNullOrEmpty(selected)) {
                foreach (var keyString in selected.Split(char.Parse("|")))
                {
				
                    keysSelected1.Add(Guid.Parse(keyString));
                }
            }
				
            query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(query, allSelected.Value, selected, "GuidLocationType");
            MyEventArgs<ContextActionModel<LocationTypeModel>> eArgs = null;
            List<LocationTypeModel> results = GetBy(query, null, null, orderBy, direction, GetContextRequest(), keysSelected1);
            OnDownloading(this, eArgs = new MyEventArgs<ContextActionModel<LocationTypeModel>>() { Object = new ContextActionModel<LocationTypeModel>() { Query = query, SelectedItems = results, Selected=selected, SelectAll = allSelected.Value, Direction = direction , OrderBy = orderBy, ActionKey=actionKey  } });

            if (eArgs != null)
            {
                if (eArgs.Object.Result != null)
                    return (FileMediaResult)eArgs.Object.Result;
            }
            

            return (new FeaturesController()).ExportDownload(typeof(LocationTypeModel), results, format, this.GetUIPluralText("MBKYellowBox", "LocationType"));
            
        }
			*/
		
		[HttpPost]
        public ActionResult CustomActionExecute(ContextActionModel model) {
		 try
            {
			//List<Guid> keysSelected1 = new List<Guid>();
			List<object> keysSelected1 = new List<object>();
            if (!string.IsNullOrEmpty(model.Selected))
            {
                foreach (var keyString in model.Selected.Split(char.Parse(",")))
                {
				
				keysSelected1.Add(Guid.Parse(keyString.Split(char.Parse("|"))[0]));
                        
                    

			
                }
            }
			DataAction dataAction = DataAction.GetDataAction(Request);
			 model.Selected = dataAction.Selected;

            model.Query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(dataAction.Query, dataAction.AllSelected, dataAction.Selected, "GuidLocationType");
           
            
			
			#region implementaci�n de m�todo parcial
            bool replaceResult = false;
            MyEventArgs<ContextActionModel<LocationTypeModel>> actionEventArgs = null;
           
			if (model.ActionKey != "deletemany" && model.ActionKey != "deleterelmany" && model.ActionKey != "updateRel" &&  model.ActionKey != "delete-relation-fk" && model.ActionKey != "restore" )
			{
				ContextRequest context = SFSdotNet.Framework.My.Context.BuildContextRequestSafe(System.Web.HttpContext.Current);
				context.UseMode = dataAction.Usemode;

				if (model.IsBackground)
				{
					System.Threading.Tasks.Task.Run(() => 
						OnCustomActionExecutingBackground(this, actionEventArgs = new MyEventArgs<ContextActionModel<LocationTypeModel>>() { Object = new ContextActionModel<LocationTypeModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } })
					);
				}
				else
				{
					OnCustomActionExecuting(this, actionEventArgs = new MyEventArgs<ContextActionModel<LocationTypeModel>>() {  Object = new ContextActionModel<LocationTypeModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } });
				}
			}
            List<LocationTypeModel> results = null;
	
			if (model.ActionKey == "deletemany") { 
				
				BR.LocationTypesBR.Instance.Delete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

            }
	
			else if (model.ActionKey == "restore") {
                    BR.LocationTypesBR.Instance.UnDelete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

                }
            else if (model.ActionKey == "updateRel" || model.ActionKey == "delete-relation-fk" || model.ActionKey == "updateRel-proxyMany")
            {
               try {
                   string valueForUpdate = null;
				   string propForUpdate = null;
				   if (!string.IsNullOrEmpty(Request.Params["propertyForUpdate"])){
						propForUpdate = Request.Params["propertyForUpdate"];
				   }
				    if (string.IsNullOrEmpty(propForUpdate) && !string.IsNullOrEmpty(Request.QueryString["propertyForUpdate"]))
                   {
                       propForUpdate = Request.QueryString["propertyForUpdate"];
                   }
                    if (model.ActionKey != "delete-relation-fk")
                    {
                        valueForUpdate = Request.QueryString["valueForUpdate"];
                    }
                    BR.LocationTypesBR.Instance.UpdateAssociation(propForUpdate, valueForUpdate, model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());
					
                    if (model.ActionKey == "delete-relation-fk")
                    {
                        MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]);

                        return PartialView("ResultMessageView", message);
                    }
                    else
                    {
                        return Content("ok");
                    }
       
          
                }
                catch (Exception ex)
                {
				        SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
           
                }
            
            }
		
                if (actionEventArgs == null && !model.IsBackground)
                {
                    //if (model.ActionKey != "deletemany"  && model.ActionKey != "deleterelmany")
                    //{
                     //   throw new NotImplementedException("");
                    //}
                }
                else
                {
					if (model.IsBackground == false )
						 replaceResult = actionEventArgs.Object.Result is ActionResult /*actionEventArgs.Object.ReplaceResult*/;
                }
                #endregion
                if (!replaceResult)
                {
                    if (Request != null && Request.IsAjaxRequest())
                    {
						MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]) ;
                        if (model.IsBackground )
                            message.Message = GlobalMessages.THE_PROCESS_HAS_BEEN_STARTED;
                        
                        return PartialView("ResultMessageView", message);                    
					}
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return (ActionResult)actionEventArgs.Object.Result;
                }
            }
            catch (Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null) {
                        message = ex.Data["usermessage"].ToString();
                    }
					 SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }

            }
        }
        //
        // POST: /LocationTypes/Delete/5
        
			
	
    }
}
namespace MBK.YellowBox.Web.Mvc.Controllers
{
	using MBK.YellowBox.Web.Mvc.Models.RouteLocations;

    public partial class RouteLocationsController : MBK.YellowBox.Web.Mvc.ControllerBase<Models.RouteLocations.RouteLocationModel>
    {

       


	#region partial methods
        ControllerEventArgs<Models.RouteLocations.RouteLocationModel> e = null;
        partial void OnValidating(object sender, ControllerEventArgs<Models.RouteLocations.RouteLocationModel> e);
        partial void OnGettingExtraData(object sender, MyEventArgs<UIModel<Models.RouteLocations.RouteLocationModel>> e);
        partial void OnCreating(object sender, ControllerEventArgs<Models.RouteLocations.RouteLocationModel> e);
        partial void OnCreated(object sender, ControllerEventArgs<Models.RouteLocations.RouteLocationModel> e);
        partial void OnEditing(object sender, ControllerEventArgs<Models.RouteLocations.RouteLocationModel> e);
        partial void OnEdited(object sender, ControllerEventArgs<Models.RouteLocations.RouteLocationModel> e);
        partial void OnDeleting(object sender, ControllerEventArgs<Models.RouteLocations.RouteLocationModel> e);
        partial void OnDeleted(object sender, ControllerEventArgs<Models.RouteLocations.RouteLocationModel> e);
    	partial void OnShowing(object sender, MyEventArgs<UIModel<Models.RouteLocations.RouteLocationModel>> e);
    	partial void OnGettingByKey(object sender, ControllerEventArgs<Models.RouteLocations.RouteLocationModel> e);
        partial void OnTaken(object sender, ControllerEventArgs<Models.RouteLocations.RouteLocationModel> e);
       	partial void OnCreateShowing(object sender, ControllerEventArgs<Models.RouteLocations.RouteLocationModel> e);
		partial void OnEditShowing(object sender, ControllerEventArgs<Models.RouteLocations.RouteLocationModel> e);
		partial void OnDetailsShowing(object sender, ControllerEventArgs<Models.RouteLocations.RouteLocationModel> e);
 		partial void OnActionsCreated(object sender, MyEventArgs<UIModel<Models.RouteLocations.RouteLocationModel >> e);
		partial void OnCustomActionExecuting(object sender, MyEventArgs<ContextActionModel<Models.RouteLocations.RouteLocationModel>> e);
		partial void OnCustomActionExecutingBackground(object sender, MyEventArgs<ContextActionModel<Models.RouteLocations.RouteLocationModel>> e);
        partial void OnDownloading(object sender, MyEventArgs<ContextActionModel<Models.RouteLocations.RouteLocationModel>> e);
      	partial void OnAuthorization(object sender, AuthorizationContext context);
		 partial void OnFilterShowing(object sender, MyEventArgs<UIModel<Models.RouteLocations.RouteLocationModel >> e);
         partial void OnSummaryOperationShowing(object sender, MyEventArgs<UIModel<Models.RouteLocations.RouteLocationModel>> e);

        partial void OnExportActionsCreated(object sender, MyEventArgs<UIModel<Models.RouteLocations.RouteLocationModel>> e);


		protected override void OnVirtualFilterShowing(object sender, MyEventArgs<UIModel<RouteLocationModel>> e)
        {
            OnFilterShowing(sender, e);
        }
		 public override void OnVirtualExportActionsCreated(object sender, MyEventArgs<UIModel<RouteLocationModel>> e)
        {
            OnExportActionsCreated(sender, e);
        }
        public override void OnVirtualDownloading(object sender, MyEventArgs<ContextActionModel<RouteLocationModel>> e)
        {
            OnDownloading(sender, e);
        }
        public override void OnVirtualShowing(object sender, MyEventArgs<UIModel<RouteLocationModel>> e)
        {
            OnShowing(sender, e);
        }

	#endregion
	#region API
	 public override ActionResult ApiCreateGen(RouteLocationModel model, ContextRequest contextRequest)
        {
            return CreateGen(model, contextRequest);
        }

              public override ActionResult ApiGetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJson(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }
        public override ActionResult ApiGetByKeyJson(string id, ContextRequest contextRequest)
        {
            return  GetByKeyJson(id, contextRequest, true);
        }
      
		 public override int ApiGetByCount(string filter, ContextRequest contextRequest)
        {
            return GetByCount(filter, contextRequest);
        }
         protected override ActionResult ApiDeleteGen(List<RouteLocationModel> models, ContextRequest contextRequest)
        {
            List<RouteLocation> objs = new List<RouteLocation>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                BR.RouteLocationsBR.Instance.DeleteBulk(objs, contextRequest);
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        protected override ActionResult ApiUpdateGen(List<RouteLocationModel> models, ContextRequest contextRequest)
        {
            List<RouteLocation> objs = new List<RouteLocation>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                foreach (var obj in objs)
                {
                    BR.RouteLocationsBR.Instance.Update(obj, contextRequest);

                }
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }


	#endregion
#region Validation methods	
	    private void Validations(RouteLocationModel model) { 
            #region Remote validations

            #endregion
		}

#endregion
		
 		public AuthorizationContext Authorization(AuthorizationContext context)
        {
            OnAuthorization(this,  context );
            return context ;
        }
		public List<RouteLocationModel> GetAll() {
            			var bos = BR.RouteLocationsBR.Instance.GetBy("",
					new SFSdotNet.Framework.My.ContextRequest()
					{
						CustomQuery = new SFSdotNet.Framework.My.CustomQuery()
						{
							OrderBy = "OrderRoute",
							SortDirection = SFSdotNet.Framework.Data.SortDirection.Ascending
						}
					});
            			List<RouteLocationModel> results = new List<RouteLocationModel>();
            RouteLocationModel model = null;
            foreach (var bo in bos)
            {
                model = new RouteLocationModel();
                model.Bind(bo);
                results.Add(model);
            }
            return results;

        }
        //
        // GET: /RouteLocations/
		[MyAuthorize("r", "RouteLocation", "MBKYellowBox", typeof(RouteLocationsController))]
		public ActionResult Index()
        {
    		var uiModel = GetContextModel(UIModelContextTypes.ListForm, null);
			ViewBag.UIModel = uiModel;
			uiModel.FilterStart = (string)ViewData["startFilter"];
                    MyEventArgs<UIModel<RouteLocationModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<RouteLocationModel>>() { Object = uiModel });

			OnExportActionsCreated(this, (me != null ? me : me = new MyEventArgs<UIModel<RouteLocationModel>>() { Object = uiModel }));

            if (me != null)
            {
                uiModel = me.Object;
            }
            if (me == null)
                me = new MyEventArgs<UIModel<RouteLocationModel>>() { Object = uiModel };
           
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;


            //return View("ListGen");
			return ResolveView(uiModel);
        }
		[MyAuthorize("r", "RouteLocation", "MBKYellowBox", typeof(RouteLocationsController))]
		public ActionResult ListViewGen(string idTab, string fk , string fkValue, string startFilter, ListModes  listmode  = ListModes.SimpleList, PropertyDefinition parentRelationProperty = null, object parentRelationPropertyValue = null )
        {
			ViewData["idTab"] = System.Web.HttpContext.Current.Request.QueryString["idTab"]; 
		 	ViewData["detpop"] = true; // details in popup
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"])) {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"]; 
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
            {
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["startFilter"]))
            {
                ViewData["startFilter"] = Request.QueryString["startFilter"];
            }
			
			UIModel<RouteLocationModel> uiModel = GetContextModel(UIModelContextTypes.ListForm, null);

            MyEventArgs<UIModel<RouteLocationModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<RouteLocationModel>>() { Object = uiModel });
            if (me == null)
                me = new MyEventArgs<UIModel<RouteLocationModel>>() { Object = uiModel };
            uiModel.Properties = GetProperties(uiModel);
            uiModel.ContextType = UIModelContextTypes.ListForm;
             uiModel.FilterStart = (string)ViewData["startFilter"];
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;
			 if (listmode == ListModes.SimpleList)
                return ResolveView(uiModel);
            else
            {
                ViewData["parentRelationProperty"] = parentRelationProperty;
                ViewData["parentRelationPropertyValue"] = parentRelationPropertyValue;
                return PartialView("ListForTagSelectView");
            }
            return ResolveView(uiModel);
        }
		List<PropertyDefinition> _properties = null;

		 protected override List<PropertyDefinition> GetProperties(UIModel uiModel,  params string[] specificProperties)
        { 
            return GetProperties(uiModel, false, null, specificProperties);
        }

		protected override List<PropertyDefinition> GetProperties(UIModel uiModel, bool decripted, Guid? id, params string[] specificProperties)
            {

			bool allProperties = true;    
                if (specificProperties != null && specificProperties.Length > 0)
                {
                    allProperties = false;
                }


			List<CustomProperty> customProperties = new List<CustomProperty>();
			if (_properties == null)
                {
                List<PropertyDefinition> results = new List<PropertyDefinition>();

			string idRouteLocation = GetRouteDataOrQueryParam("id");
			if (idRouteLocation != null)
			{
				if (!decripted)
                {
					idRouteLocation = SFSdotNet.Framework.Entities.Utils.GetPropertyKey(idRouteLocation.Replace("-","/"), "GuidRouteLocation");
				}else{
					if (id != null )
						idRouteLocation = id.Value.ToString();                

				}
			}

			bool visibleProperty = true;	
			 bool conditionalshow =false;
                if (uiModel.ContextType == UIModelContextTypes.EditForm || uiModel.ContextType == UIModelContextTypes.DisplayForm ||  uiModel.ContextType == UIModelContextTypes.GenericForm )
                    conditionalshow = true;
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidRouteLocation"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidRouteLocation")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 100,
																	
					CustomProperties = customProperties,

                    PropertyName = "GuidRouteLocation",

					 MaxLength = 0,
					IsRequired = true ,
					IsHidden = true,
                    SystemProperty =  SystemProperties.Identifier ,
					IsDefaultProperty = false,
                    SortBy = "GuidRouteLocation",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.RouteLocationResources.GUIDROUTELOCATION*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidRoute"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidRoute")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 101,
																	IsForeignKey = true,

									
					CustomProperties = customProperties,

                    PropertyName = "GuidRoute",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "GuidRoute",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.RouteLocationResources.GUIDROUTE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidLocation"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidLocation")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 102,
																	IsForeignKey = true,

									
					CustomProperties = customProperties,

                    PropertyName = "GuidLocation",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "GuidLocation",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.RouteLocationResources.GUIDLOCATION*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("OrderRoute"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "OrderRoute")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 103,
																	
					CustomProperties = customProperties,

                    PropertyName = "OrderRoute",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = true,
                    SortBy = "OrderRoute",
					
	
                    TypeName = "Int32",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.RouteLocationResources.ORDERROUTE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidStudent"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidStudent")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 104,
																	IsForeignKey = true,

									
					CustomProperties = customProperties,

                    PropertyName = "GuidStudent",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "GuidStudent",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.RouteLocationResources.GUIDSTUDENT*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 105,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedDate",
					
	
				SystemProperty = SystemProperties.CreatedDate ,
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.RouteLocationResources.CREATEDDATE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 121,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedDate",
					
	
					IsUpdatedDate = true,
					SystemProperty = SystemProperties.UpdatedDate ,
	
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    ,PropertyDisplayName = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.UPDATED

                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 107,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedBy",
					
	
				SystemProperty = SystemProperties.CreatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.RouteLocationResources.CREATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 108,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedBy",
					
	
				SystemProperty = SystemProperties.UpdatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.RouteLocationResources.UPDATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Bytes"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Bytes")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 109,
																	
					CustomProperties = customProperties,

                    PropertyName = "Bytes",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Bytes",
					
	
				SystemProperty = SystemProperties.SizeBytes,
                    TypeName = "Int32",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.RouteLocationResources.BYTES*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Student"))
{				
    customProperties = new List<CustomProperty>();

        customProperties.Add(new CustomProperty() { Name="UILookUp", Value=@"true" });
		 
    			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"RouteLocations" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="Student.GuidStudent")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"Student.GuidStudent" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"Students" });
			

	
	//fk_RouteLocation_Student
		//if (this.Request.QueryString["fk"] != "Student")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 110,
																
					
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "Student",
					PropertyNavigationKey = "GuidStudent",
					PropertyNavigationText = "FullName",
					NavigationPropertyType = NavigationPropertyTypes.LookUp,
					GetMethodName = "GetAll",
					GetMethodParameters = "",
					GetMethodDisplayText ="FullName",
					GetMethodDisplayValue = "GuidStudent",
					
					CustomProperties = customProperties,

                    PropertyName = "Student",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Student.FullName",
					
	
                    TypeName = "MBKYellowBoxModel.Student",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/Students"
                    /*,PropertyDisplayName = Resources.RouteLocationResources.STUDENT*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("YBLocation"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"RouteLocations" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="YBLocation.GuidLocation")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"YBLocation.GuidLocation" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"YBLocations" });
			

	
	//fk_RouteLocation_Location
		//if (this.Request.QueryString["fk"] != "YBLocation")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 111,
																
					
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "YBLocation",
					PropertyNavigationKey = "GuidLocation",
					PropertyNavigationText = "Description",
					NavigationPropertyType = NavigationPropertyTypes.SimpleDropDown,
					GetMethodName = "GetAll",
					GetMethodParameters = "",
					GetMethodDisplayText ="Description",
					GetMethodDisplayValue = "GuidLocation",
					
					CustomProperties = customProperties,

                    PropertyName = "YBLocation",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "YBLocation.Description",
					
	
                    TypeName = "MBKYellowBoxModel.YBLocation",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/YBLocations"
                    /*,PropertyDisplayName = Resources.RouteLocationResources.YBLOCATION*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("YBRoute"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"RouteLocations" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="YBRoute.GuidRoute")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"YBRoute.GuidRoute" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"YBRoutes" });
			

	
	//fk_RouteLocation_Route
		//if (this.Request.QueryString["fk"] != "YBRoute")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 112,
																
					
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "YBRoute",
					PropertyNavigationKey = "GuidRoute",
					PropertyNavigationText = "Title",
					NavigationPropertyType = NavigationPropertyTypes.SimpleDropDown,
					GetMethodName = "GetAll",
					GetMethodParameters = "",
					GetMethodDisplayText ="Title",
					GetMethodDisplayValue = "GuidRoute",
					
					CustomProperties = customProperties,

                    PropertyName = "YBRoute",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "YBRoute.Title",
					
	
                    TypeName = "MBKYellowBoxModel.YBRoute",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/YBRoutes"
                    /*,PropertyDisplayName = Resources.RouteLocationResources.YBROUTE*/
                });
		//	}
	
	}
	
				
                    _properties = results;
                    return _properties;
                }
                else {
                    return _properties;
                }
            }

		protected override  UIModel<RouteLocationModel> GetByForShow(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, params  object[] extraParams)
        {
			if (Request != null )
				if (!string.IsNullOrEmpty(Request.QueryString["q"]))
					filter = filter + HttpUtility.UrlDecode(Request.QueryString["q"]);
 if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
            }
            var bos = BR.RouteLocationsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), contextRequest, extraParams);
			//var bos = BR.RouteLocationsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), context, extraParams);
            RouteLocationModel model = null;
            List<RouteLocationModel> results = new List<RouteLocationModel>();
            foreach (var item in bos)
            {
                model = new RouteLocationModel();
				model.Bind(item);
				results.Add(model);
            }
            //return results;
			UIModel<RouteLocationModel> uiModel = GetContextModel(UIModelContextTypes.Items, null);
            uiModel.Items = results;
			if (Request != null){
				if (SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(Request.RequestContext, "action") == "Download")
				{
					uiModel.ContextType = UIModelContextTypes.ExportDownload;
				}
			}
            Showing(ref uiModel);
            return uiModel;
		}			
		
		//public List<RouteLocationModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params  object[] extraParams)
        //{
		//	var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, null, extraParams);
        public override List<RouteLocationModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest,  params  object[] extraParams)
        {
            var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
           
            return uiModel.Items;
		
        }
		/*
        [MyAuthorize("r", "RouteLocation", "MBKYellowBox", typeof(RouteLocationsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir)
        {
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir);
        }*/

		  [MyAuthorize("r", "RouteLocation", "MBKYellowBox", typeof(RouteLocationsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir,ContextRequest contextRequest,  object[] extraParams)
        {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir,contextRequest, extraParams);
        }
/*		  [MyAuthorize("r", "RouteLocation", "MBKYellowBox", typeof(RouteLocationsController))]
       public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJsonBase(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }*/
		[MyAuthorize()]
		public int GetByCount(string filter, ContextRequest contextRequest) {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
            return BR.RouteLocationsBR.Instance.GetCount(HttpUtility.UrlDecode(filter), GetUseMode(), contextRequest);
        }
		

		[MyAuthorize("r", "RouteLocation", "MBKYellowBox", typeof(RouteLocationsController))]
        public ActionResult GetByKeyJson(string id, ContextRequest contextRequest,  bool dec = false)
        {
            return Json(GetByKey(id, null, contextRequest, dec), JsonRequestBehavior.AllowGet);
        }
		public RouteLocationModel GetByKey(string id) {
			return GetByKey(id, null,null, false);
       	}
		    public RouteLocationModel GetByKey(string id, string includes)
        {
            return GetByKey(id, includes, false);
        }
		 public  RouteLocationModel GetByKey(string id, string includes, ContextRequest contextRequest)
        {
            return GetByKey(id, includes, contextRequest, false);
        }
		/*
		  public ActionResult ShowField(string fieldName, string idField) {
		   string safePropertyName = fieldName;
              if (fieldName.StartsWith("Fk"))
              {
                  safePropertyName = fieldName.Substring(2, fieldName.Length - 2);
              }

             RouteLocationModel model = new  RouteLocationModel();

            UIModel uiModel = GetUIModel(model, new string[] { "NoField-" });
			
				uiModel.Properties = GetProperties(uiModel, safePropertyName);
		uiModel.Properties.ForEach(p=> p.ContextType = uiModel.ContextType );
            uiModel.ContextType = UIModelContextTypes.FilterFields;
            uiModel.OverrideApp = GetOverrideApp();
            uiModel.UseMode = GetUseMode();

            ViewData["uiModel"] = uiModel;
			var prop = uiModel.Properties.FirstOrDefault(p=>p.PropertyName == safePropertyName);
            //if (prop.IsNavigationProperty && prop.IsNavigationPropertyMany == false)
            //{
            //    ViewData["currentProperty"] = uiModel.Properties.FirstOrDefault(p => p.PropertyName != fieldName + "Text");
            //}else if (prop.IsNavigationProperty == false){
                ViewData["currentProperty"] = prop;
           // }
            ((PropertyDefinition)ViewData["currentProperty"]).RemoveLayout = true;
			ViewData["withContainer"] = false;


            return PartialView("GenericField", model);


        }
      */
	public RouteLocationModel GetByKey(string id, ContextRequest contextRequest, bool dec)
        {
            return GetByKey(id, null, contextRequest, dec);
        }
        public RouteLocationModel GetByKey(string id, string  includes, bool dec)
        {
            return GetByKey(id, includes, null, dec);
        }

        public RouteLocationModel GetByKey(string id, string includes, ContextRequest contextRequest, bool dec) {
		             RouteLocationModel model = null;
            ControllerEventArgs<RouteLocationModel> e = null;
			string objectKey = id.Replace("-","/");
             OnGettingByKey(this, e=  new ControllerEventArgs<RouteLocationModel>() { Id = objectKey  });
             bool cancel = false;
             RouteLocationModel eItem = null;
             if (e != null)
             {
                 cancel = e.Cancel;
                 eItem = e.Item;
             }
			if (cancel == false && eItem == null)
             {
			Guid guidRouteLocation = Guid.Empty; //new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, "GuidRouteLocation"));
			if (dec)
                 {
                     guidRouteLocation = new Guid(id);
                 }
                 else
                 {
                     guidRouteLocation = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, null));
                 }
			
            
				model = new RouteLocationModel();
                  if (contextRequest == null)
                {
                    contextRequest = GetContextRequest();
                }
				var bo = BR.RouteLocationsBR.Instance.GetByKey(guidRouteLocation, GetUseMode(), contextRequest,  includes);
				 if (bo != null)
                    model.Bind(bo);
                else
                    return null;
			}
             else {
                 model = eItem;
             }
			model.IsNew = false;

            return model;
        }
        // GET: /RouteLocations/DetailsGen/5
		[MyAuthorize("r", "RouteLocation", "MBKYellowBox", typeof(RouteLocationsController))]
        public ActionResult DetailsGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = RouteLocationResources.ENTITY_PLURAL;
			 #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<RouteLocationModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
            #endregion



			 bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null) {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
			//UIModel<RouteLocationModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, decripted), decripted, guidId);
			var item = GetByKey(id, null, null, decripted);
			if (item == null)
            {
                 RouteValueDictionary rv = new RouteValueDictionary();
                string usemode = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"usemode");
                string overrideModule = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "overrideModule");
                string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");

                if(!string.IsNullOrEmpty(usemode)){
                    rv.Add("usemode", usemode);
                }
                if(!string.IsNullOrEmpty(overrideModule)){
                    rv.Add("overrideModule", overrideModule);
                }
                if (!string.IsNullOrEmpty(area))
                {
                    rv.Add("area", area);
                }

                return RedirectToAction("Index", rv);
            }
            //
            UIModel<RouteLocationModel> uiModel = null;
                uiModel = GetContextModel(UIModelContextTypes.DisplayForm, item, decripted, guidId);



            MyEventArgs<UIModel<RouteLocationModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<RouteLocationModel>>() { Object = uiModel });

            if (me != null) {
                uiModel = me.Object;
            }
			
            Showing(ref uiModel);
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			
            //return View("DisplayGen", uiModel.Items[0]);
			return ResolveView(uiModel, uiModel.Items[0]);

        }
		[MyAuthorize("r", "RouteLocation", "MBKYellowBox", typeof(RouteLocationsController))]
		public ActionResult DetailsViewGen(string id)
        {

		 bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<RouteLocationModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
           
			if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
 			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
           
        	 //var uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id));
			 
            bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
            {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true")
                {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
            UIModel<RouteLocationModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, null, decripted), decripted, guidId);
			

            MyEventArgs<UIModel<RouteLocationModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<RouteLocationModel>>() { Object = uiModel });

            if (me != null)
            {
                uiModel = me.Object;
            }
            
            Showing(ref uiModel);
            return ResolveView(uiModel, uiModel.Items[0]);
        
        }
        //
        // GET: /RouteLocations/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        //
        // GET: /RouteLocations/CreateGen
		[MyAuthorize("c", "RouteLocation", "MBKYellowBox", typeof(RouteLocationsController))]
        public ActionResult CreateGen()
        {
			RouteLocationModel model = new RouteLocationModel();
            model.IsNew = true;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model);

			OnCreateShowing(this, e = new ControllerEventArgs<RouteLocationModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }

             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                 ViewData["ispopup"] = true;
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                 ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                 ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

            Showing(ref me);

			return ResolveView(me, me.Items[0]);
        } 
			
		protected override UIModel<RouteLocationModel> GetContextModel(UIModelContextTypes formMode, RouteLocationModel model)
        {
            return GetContextModel(formMode, model, false, null);
        }
			
		 private UIModel<RouteLocationModel> GetContextModel(UIModelContextTypes formMode, RouteLocationModel model, bool decript, Guid ? id) {
            UIModel<RouteLocationModel> me = new UIModel<RouteLocationModel>(true, "RouteLocations");
			me.UseMode = GetUseMode();
			me.Controller = this;
			me.OverrideApp = GetOverrideApp();
			me.ContextType = formMode ;
			me.Id = "RouteLocation";
			
            me.ModuleKey = "MBKYellowBox";

			me.ModuleNamespace = "MBK.YellowBox";
            me.EntityKey = "RouteLocation";
            me.EntitySetName = "RouteLocations";

			me.AreaAction = "MBKYellowBox";
            me.ControllerAction = "RouteLocations";
            me.PropertyKeyName = "GuidRouteLocation";

            me.Properties = GetProperties(me, decript, id);

			me.SortBy = "UpdatedDate";
			me.SortDirection = UIModelSortDirection.DESC;

 			
			if (Request != null)
            {
                string actionName = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam( Request.RequestContext, "action");
                if(actionName != null && actionName.ToLower().Contains("create") ){
                    me.IsNew = true;
                }
            }
			 #region Buttons
			 if (Request != null ){
             if (formMode == UIModelContextTypes.DisplayForm || formMode == UIModelContextTypes.EditForm || formMode == UIModelContextTypes.ListForm)
				me.ActionButtons = GetActionButtons(formMode,model != null ?(Request.QueryString["dec"] == "true" ? model.Id : model.SafeKey)  : null, "MBKYellowBox", "RouteLocations", "RouteLocation", me.IsNew);

            //me.ActionButtons.Add(new ActionModel() { ActionKey = "return", Title = GlobalMessages.RETURN, Url = System.Web.VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/RouteLocations" });
			if (this.HttpContext != null &&  !this.HttpContext.SkipAuthorization){
				//antes this.HttpContext
				me.SetAction("u", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("u", "RouteLocation", "MBKYellowBox"));
				me.SetAction("c", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("c", "RouteLocation", "MBKYellowBox"));
				me.SetAction("d", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("d", "RouteLocation", "MBKYellowBox"));
			
			}else{
				me.SetAction("u", true);
				me.SetAction("c", true);
				me.SetAction("d", true);

			}
            #endregion              
         
            switch (formMode)
            {
                case UIModelContextTypes.DisplayForm:
					//me.TitleForm = RouteLocationResources.ROUTELOCATIONS_DETAILS;
                    me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.MODIFY_DATA;
					 me.Properties.Where(p=>p.PropertyName  != "Id" && p.IsForeignKey == false).ToList().ForEach(p => p.IsHidden = false);

					 me.Properties.Where(p => (p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier) ).ToList().ForEach(p=> me.SetHide(p.PropertyName));

                    break;
                case UIModelContextTypes.EditForm:
				  me.Properties.Where(p=>p.SystemProperty != SystemProperties.Identifier && p.IsForeignKey == false && p.PropertyName != "Id").ToList().ForEach(p => p.IsHidden = false);

					if (model != null)
                    {
						

                        me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.SAVE_DATA;                        
                        me.ActionButtons.First(p => p.ActionKey == "c").Title = GlobalMessages.SAVE_DATA;
						if (model.IsNew ){
							//me.TitleForm = RouteLocationResources.ROUTELOCATIONS_ADD_NEW;
							me.ActionName = "CreateGen";
							me.Properties.RemoveAll(p => p.SystemProperty != null || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));
						}else{
							
							me.ActionName = "EditGen";

							//me.TitleForm = RouteLocationResources.ROUTELOCATIONS_EDIT;
							me.Properties.RemoveAll(p => p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));	
						}
						//me.Properties.Remove(me.Properties.Find(p => p.PropertyName == "UpdatedDate"));
					
					}
                    break;
                case UIModelContextTypes.FilterFields:
                    break;
                case UIModelContextTypes.GenericForm:
                    break;
                case UIModelContextTypes.Items:
				//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "OrderRoute") != null){
						me.Properties.Find(p => p.PropertyName == "OrderRoute").IsHidden = false;
					 }
					 
                    
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					 
                    
					

						 if (me.Properties.Find(p => p.PropertyName == "GuidRouteLocation") != null){
						me.Properties.Find(p => p.PropertyName == "GuidRouteLocation").IsHidden = false;
					 }
					 
                    
					


                  


					//}
                    break;
                case UIModelContextTypes.ListForm:
					PropertyDefinition propFinded = null;
					//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "OrderRoute") != null){
						me.Properties.Find(p => p.PropertyName == "OrderRoute").IsHidden = false;
					 }
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					
					me.PrincipalActionName = "GetByJson";
					//}
					//me.TitleForm = RouteLocationResources.ROUTELOCATIONS_LIST;
                    break;
                default:
                    break;
            }
            	this.SetDefaultProperties(me);
			}
			if (model != null )
            	me.Items.Add(model);
            return me;
        }
		// GET: /RouteLocations/CreateViewGen
		[MyAuthorize("c", "RouteLocation", "MBKYellowBox", typeof(RouteLocationsController))]
        public ActionResult CreateViewGen()
        {
				RouteLocationModel model = new RouteLocationModel();
            model.IsNew = true;
			e= null;
			OnCreateShowing(this, e = new ControllerEventArgs<RouteLocationModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }
			
            var me = GetContextModel(UIModelContextTypes.EditForm, model);

			me.IsPartialView = true;	
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
            {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                me.Properties.Find(p => p.PropertyName == ViewData["fk"].ToString()).IsReadOnly = true;
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
			
      
            //me.Items.Add(model);
            Showing(ref me);
            return ResolveView(me, me.Items[0]);
        }
		protected override  void GettingExtraData(ref UIModel<RouteLocationModel> uiModel)
        {

            MyEventArgs<UIModel<RouteLocationModel>> me = null;
            OnGettingExtraData(this, me = new MyEventArgs<UIModel<RouteLocationModel>>() { Object = uiModel });
            //bool maybeAnyReplaced = false; 
            if (me != null)
            {
                uiModel = me.Object;
                //maybeAnyReplaced = true;
            }
           
			bool canFill = false;
			 string query = null ;
            bool isFK = false;
			PropertyDefinition prop =null;
			var contextRequest = this.GetContextRequest();
            contextRequest.CustomParams.Add(new CustomParam() { Name="ui", Value= RouteLocation.EntityName });

            			canFill = false;
			 query = "";
            isFK =false;
			prop = uiModel.Properties.FirstOrDefault(p => p.PropertyName == "Student");
			if (prop != null)
				if (prop.IsHidden == false && (prop.ContextType == UIModelContextTypes.EditForm || prop.ContextType == UIModelContextTypes.FilterFields  || (prop.ContextType == null && uiModel.ContextType == UIModelContextTypes.EditForm)))
				{
					if (prop.NavigationPropertyType == NavigationPropertyTypes.SimpleDropDown )
						canFill = true;
				}
                else if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidStudent = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidStudent = @GuidStudent";
					
					canFill = true;
                }
				if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidStudent = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidStudent = @GuidStudent";
					canFill = true;
                }
			if (canFill){
			                contextRequest.CustomQuery = new CustomQuery();

				if (!uiModel.ExtraData.Exists(p => p.PropertyName == "Student")) {
					if (!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))				  
						contextRequest.CustomQuery.SetParam("GuidStudent", new Nullable<Guid>(Guid.Parse( Request.QueryString["fkValue"])));

					 if (isFK == true)
                    {
						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.StudentsBR()).GetBy(query, contextRequest), "GuidStudent", "FullName", Request.QueryString["fkValue"]), PropertyName = "Student" });    
                    }
                    else
                    {

						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.StudentsBR()).GetBy(query, contextRequest), "GuidStudent", "FullName"), PropertyName = "Student" });    

					}
    if (isFK)
                    {    
						var FkStudent = ((SelectList)uiModel.ExtraData.First(p => p.PropertyName == "Student").Data).First();
						uiModel.Items[0].GetType().GetProperty("FkStudentText").SetValue(uiModel.Items[0], FkStudent.Text);
						uiModel.Items[0].GetType().GetProperty("FkStudent").SetValue(uiModel.Items[0], Guid.Parse(FkStudent.Value));
                    
					}    
				}
			}
		 
			canFill = false;
			 query = "";
            isFK =false;
			prop = uiModel.Properties.FirstOrDefault(p => p.PropertyName == "YBLocation");
			if (prop != null)
				if (prop.IsHidden == false && (prop.ContextType == UIModelContextTypes.EditForm || prop.ContextType == UIModelContextTypes.FilterFields  || (prop.ContextType == null && uiModel.ContextType == UIModelContextTypes.EditForm)))
				{
					if (prop.NavigationPropertyType == NavigationPropertyTypes.SimpleDropDown )
						canFill = true;
				}
                else if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidLocation = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidLocation = @GuidLocation";
					
					canFill = true;
                }
				if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidLocation = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidLocation = @GuidLocation";
					canFill = true;
                }
			if (canFill){
			                contextRequest.CustomQuery = new CustomQuery();

				if (!uiModel.ExtraData.Exists(p => p.PropertyName == "YBLocation")) {
					if (!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))				  
						contextRequest.CustomQuery.SetParam("GuidLocation", new Nullable<Guid>(Guid.Parse( Request.QueryString["fkValue"])));

					 if (isFK == true)
                    {
						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.YBLocationsBR()).GetBy(query, contextRequest), "GuidLocation", "Description", Request.QueryString["fkValue"]), PropertyName = "YBLocation" });    
                    }
                    else
                    {

						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.YBLocationsBR()).GetBy(query, contextRequest), "GuidLocation", "Description"), PropertyName = "YBLocation" });    

					}
    if (isFK)
                    {    
						var FkYBLocation = ((SelectList)uiModel.ExtraData.First(p => p.PropertyName == "YBLocation").Data).First();
						uiModel.Items[0].GetType().GetProperty("FkYBLocationText").SetValue(uiModel.Items[0], FkYBLocation.Text);
						uiModel.Items[0].GetType().GetProperty("FkYBLocation").SetValue(uiModel.Items[0], Guid.Parse(FkYBLocation.Value));
                    
					}    
				}
			}
		 
			canFill = false;
			 query = "";
            isFK =false;
			prop = uiModel.Properties.FirstOrDefault(p => p.PropertyName == "YBRoute");
			if (prop != null)
				if (prop.IsHidden == false && (prop.ContextType == UIModelContextTypes.EditForm || prop.ContextType == UIModelContextTypes.FilterFields  || (prop.ContextType == null && uiModel.ContextType == UIModelContextTypes.EditForm)))
				{
					if (prop.NavigationPropertyType == NavigationPropertyTypes.SimpleDropDown )
						canFill = true;
				}
                else if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidRoute = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidRoute = @GuidRoute";
					
					canFill = true;
                }
				if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidRoute = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidRoute = @GuidRoute";
					canFill = true;
                }
			if (canFill){
			                contextRequest.CustomQuery = new CustomQuery();

				if (!uiModel.ExtraData.Exists(p => p.PropertyName == "YBRoute")) {
					if (!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))				  
						contextRequest.CustomQuery.SetParam("GuidRoute", new Nullable<Guid>(Guid.Parse( Request.QueryString["fkValue"])));

					 if (isFK == true)
                    {
						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.YBRoutesBR()).GetBy(query, contextRequest), "GuidRoute", "Title", Request.QueryString["fkValue"]), PropertyName = "YBRoute" });    
                    }
                    else
                    {

						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.YBRoutesBR()).GetBy(query, contextRequest), "GuidRoute", "Title"), PropertyName = "YBRoute" });    

					}
    if (isFK)
                    {    
						var FkYBRoute = ((SelectList)uiModel.ExtraData.First(p => p.PropertyName == "YBRoute").Data).First();
						uiModel.Items[0].GetType().GetProperty("FkYBRouteText").SetValue(uiModel.Items[0], FkYBRoute.Text);
						uiModel.Items[0].GetType().GetProperty("FkYBRoute").SetValue(uiModel.Items[0], Guid.Parse(FkYBRoute.Value));
                    
					}    
				}
			}
		 
            

        }
		private void Showing(ref UIModel<RouteLocationModel> uiModel) {
          	
			MyEventArgs<UIModel<RouteLocationModel>> me = new MyEventArgs<UIModel<RouteLocationModel>>() { Object = uiModel };
			 OnVirtualLayoutSettings(this, me);


            OnShowing(this, me);

			
			if ((Request != null && Request.QueryString["allFields"] == "1") || Request == null )
			{
				me.Object.Properties.ForEach(p=> p.IsHidden = false);
            }
            if (me != null)
            {
                uiModel = me.Object;
            }
          


			 if (uiModel.ContextType == UIModelContextTypes.EditForm)
			    GettingExtraData(ref uiModel);
            ViewData["UIModel"] = uiModel;

        }
        //
        // POST: /RouteLocations/Create
		[MyAuthorize("c", "RouteLocation", "MBKYellowBox", typeof(RouteLocationsController))]
        [HttpPost]
		[ValidateInput(false)] 
        public ActionResult CreateGen(RouteLocationModel  model,  ContextRequest contextRequest)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
		 	e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<RouteLocationModel>() { Item = model });
           
		  	if (!ModelState.IsValid) {
				model.IsNew = true;
				var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
                 if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                        ViewData["ispopup"] = true;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
                    return ResolveView(me, model);
            }
            try
            {
				if (model.GuidRouteLocation == null || model.GuidRouteLocation.ToString().Contains("000000000"))
				model.GuidRouteLocation = Guid.NewGuid();
	
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnCreating(this, e = new ControllerEventArgs<RouteLocationModel>() { Item = model });
                if (e != null) {
                   if (e.Cancel && e.RedirectValues.Count > 0){
                        RouteValueDictionary rv = new RouteValueDictionary();
                        if (e.RedirectValues["area"] != null ){
                            rv.Add("area", e.RedirectValues["area"].ToString());
                        }
                        foreach (var item in e.RedirectValues.Where(p=>p.Key != "area" && p.Key != "controller" &&  p.Key != "action" ))
	                    {
		                    rv.Add(item.Key, item.Value);
	                    }

                        //if (e.RedirectValues["action"] != null && e.RedirectValues["controller"] != null && e.RedirectValues["area"] != null )
                        return RedirectToAction(e.RedirectValues["action"].ToString(), e.RedirectValues["controller"].ToString(), rv );


                        
                    }else if (e.Cancel && e.ActionResult != null )
                        return e.ActionResult;  
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				if (contextRequest == null || contextRequest.Company == null){
					contextRequest = GetContextRequest();
					
				}
                if (!cancel)
                	model.Bind(RouteLocationsBR.Instance.Create(model.GetBusinessObject(), contextRequest ));
				OnCreated(this, e = new ControllerEventArgs<RouteLocationModel>() { Item = model });
                 if (e != null )
					if (e.ActionResult != null)
                    	replaceResult = true;		
				if (!replaceResult)
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))
                    {
                        ViewData["__continue"] = true;
                    }
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"]) &&  string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
                        popupextra.Add("id", model.SafeKey);
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                       {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                            popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"area"));
                            popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                            popupextra.Add("action", actionDetails);
                       if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
                    {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))

                        {
                            var popupextra = GetRouteData();
                            popupextra.Add("id", model.SafeKey);
                            return RedirectToAction("EditViewGen", popupextra);
                        }
                        else
                        {
                            return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.ADD_DONE));
                        }
                    }        			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                        return Redirect(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]);
                    else{

							RouteValueDictionary popupextra = null; 
							if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"])){
                            popupextra = GetRouteData();
							 string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
                            if (!string.IsNullOrEmpty(area))
                                popupextra.Add("area", area);
                            
                            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"])) {
								popupextra.Add("id", model.SafeKey);
                                return RedirectToAction("EditGen", popupextra);

                            }else{
								
                            return RedirectToAction("Index", popupextra);
							}
							}else{
								return Content("ok");
							}
                        }
						 }
                else {
                    return e.ActionResult;
                    }
				}
            catch(Exception ex)
            {
					if (!string.IsNullOrEmpty(Request.QueryString["rok"]))
                {
                    throw  ex;
                }
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                model.IsNew = true;
                var me = GetContextModel(UIModelContextTypes.EditForm, model, true, model.GuidRouteLocation);
                Showing(ref me);
                if (isPopUp)
                {
                    
                        ViewData["ispopup"] = isPopUp;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
					if (Request != null)
						return ResolveView(me, model);
					else
						return Content("ok");
            }
        }        
        //
        // GET: /RouteLocations/Edit/5 
        public ActionResult Edit(int id)
        {
            return View();
        }
			
		
		[MyAuthorize("u", "RouteLocation", "MBKYellowBox", typeof(RouteLocationsController))]
		[MvcSiteMapNode(Area="MBKYellowBox", Title="sss", Clickable=false, ParentKey = "MBKYellowBox_RouteLocation_List")]
		public ActionResult EditGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = RouteLocationResources.ENTITY_SINGLE;		 	
  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<RouteLocationModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
            RouteLocationModel model = null;
            // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
			bool dec = false;
            Guid ? idGuidDecripted = null ;
            if (Request != null && Request.QueryString["dec"] == "true")
            {
                dec = true;
                idGuidDecripted = Guid.Parse(id);
            }

            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model,dec,idGuidDecripted);
            Showing(ref me);


            if (!replaceResult)
            {
                 //return View("EditGen", me.Items[0]);
				 return ResolveView(me, me.Items[0]);
            }
            else {
                return e.ActionResult;
            }
        }
			[MyAuthorize("u", "RouteLocation","MBKYellowBox", typeof(RouteLocationsController))]
		public ActionResult EditViewGen(string id)
        {
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

					  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<RouteLocationModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
			
            RouteLocationModel model = null;
			 bool dec = false;
            Guid? guidId = null ;

            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null && System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                dec = true;
                guidId = Guid.Parse(id);
            }
            // si fue implementado el método parcial y no se ha decidido suspender la acción
            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
            var me = GetContextModel(UIModelContextTypes.EditForm, model, dec, guidId);
            Showing(ref me);

            return ResolveView(me, model);
        }
		[MyAuthorize("u", "RouteLocation",  "MBKYellowBox", typeof(RouteLocationsController))]
		[HttpPost]
		[ValidateInput(false)] 
		        public ActionResult EditGen(RouteLocationModel model)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
			e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<RouteLocationModel>() { Item = model });
           
            if (!ModelState.IsValid)
            {
			   	var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
			
				if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"])){
                	ViewData["ispopup"] = true;
					return ResolveView(me, model);
				}
				else
					return ResolveView(me, model);
            }
            try
            {
			
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnEditing(this, e = new ControllerEventArgs<RouteLocationModel>() { Item = model });
                if (e != null) {
                    if (e.Cancel && e.ActionResult != null)
                        return e.ActionResult;
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				ContextRequest context = new ContextRequest();
                context.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;

                RouteLocation resultObj = null;
			    if (!cancel)
                	resultObj = RouteLocationsBR.Instance.Update(model.GetBusinessObject(), GetContextRequest());
				
				OnEdited(this, e = new ControllerEventArgs<RouteLocationModel>() { Item =   new RouteLocationModel(resultObj) });
				if (e != null && e.ActionResult != null) replaceResult = true; 

                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Content("ok");
                }
                else
                {
				if (!replaceResult)
                {
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"])  && string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
						 if (Request != null && Request.QueryString["dec"] == "true")
                        {
                            popupextra.Add("id", model.Id);
                        }
                        else
                        {
							popupextra.Add("id", model.SafeKey);

							
                        }
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                        {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                        popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area"));
                        popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                        popupextra.Add("action", actionDetails);
                        if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
					if (isPopUp)
						return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.UPDATE_DONE));
        			    string returnUrl = null;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"])) {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.Form["ReturnAfter"];
                        else if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"];
                    }
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else{
		RouteValueDictionary popupextra = null; 
						 if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"]))
                            {
							
							popupextra = GetRouteData();
							string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
							if (!string.IsNullOrEmpty(area))
								popupextra.Add("area", area);

							return RedirectToAction("Index", popupextra);
						}else{
							return Content("ok");
						}
						}
				 }
                else {
                    return e.ActionResult;
				}
                }		
            }
          catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
			    if (isPopUp)
                {
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(ex.Message));
                    
                }
                else
                {
                    SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                    
                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
                else {
						  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
							ViewData["ispopup"] = true;
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
							ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
							ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

						var me = GetContextModel(UIModelContextTypes.EditForm, model);
						Showing(ref me);

						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
						{
							ViewData["ispopup"] = true;
							return ResolveView(me, model);
						}
						else
							return ResolveView(me, model);

						
					}
				}
            }
        }
        //
        // POST: /RouteLocations/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                //  Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /RouteLocations/Delete/5
        
		[MyAuthorize("d", "RouteLocation", "MBKYellowBox", typeof(RouteLocationsController))]
		[HttpDelete]
        public ActionResult DeleteGen(string objectKey, string extraParams)
        {
            try
            {
					
			
				Guid guidRouteLocation = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey.Replace("-", "/"), "GuidRouteLocation")); 
                BO.RouteLocation entity = new BO.RouteLocation() { GuidRouteLocation = guidRouteLocation };

                BR.RouteLocationsBR.Instance.Delete(entity, GetContextRequest());               
                return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.DELETE_DONE));

            }
            catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null)
                    {
                        message = ex.Data["usermessage"].ToString();
                    }

                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }
            }
        }
		/*[MyAuthorize()]
		public FileMediaResult Download(string query, bool? allSelected = false,  string selected = null , string orderBy = null , string direction = null , string format = null , string actionKey=null )
        {
			
            List<Guid> keysSelected1 = new List<Guid>();
            if (!string.IsNullOrEmpty(selected)) {
                foreach (var keyString in selected.Split(char.Parse("|")))
                {
				
                    keysSelected1.Add(Guid.Parse(keyString));
                }
            }
				
            query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(query, allSelected.Value, selected, "GuidRouteLocation");
            MyEventArgs<ContextActionModel<RouteLocationModel>> eArgs = null;
            List<RouteLocationModel> results = GetBy(query, null, null, orderBy, direction, GetContextRequest(), keysSelected1);
            OnDownloading(this, eArgs = new MyEventArgs<ContextActionModel<RouteLocationModel>>() { Object = new ContextActionModel<RouteLocationModel>() { Query = query, SelectedItems = results, Selected=selected, SelectAll = allSelected.Value, Direction = direction , OrderBy = orderBy, ActionKey=actionKey  } });

            if (eArgs != null)
            {
                if (eArgs.Object.Result != null)
                    return (FileMediaResult)eArgs.Object.Result;
            }
            

            return (new FeaturesController()).ExportDownload(typeof(RouteLocationModel), results, format, this.GetUIPluralText("MBKYellowBox", "RouteLocation"));
            
        }
			*/
		
		[HttpPost]
        public ActionResult CustomActionExecute(ContextActionModel model) {
		 try
            {
			//List<Guid> keysSelected1 = new List<Guid>();
			List<object> keysSelected1 = new List<object>();
            if (!string.IsNullOrEmpty(model.Selected))
            {
                foreach (var keyString in model.Selected.Split(char.Parse(",")))
                {
				
				keysSelected1.Add(Guid.Parse(keyString.Split(char.Parse("|"))[0]));
                        
                    

			
                }
            }
			DataAction dataAction = DataAction.GetDataAction(Request);
			 model.Selected = dataAction.Selected;

            model.Query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(dataAction.Query, dataAction.AllSelected, dataAction.Selected, "GuidRouteLocation");
           
            
			
			#region implementaci�n de m�todo parcial
            bool replaceResult = false;
            MyEventArgs<ContextActionModel<RouteLocationModel>> actionEventArgs = null;
           
			if (model.ActionKey != "deletemany" && model.ActionKey != "deleterelmany" && model.ActionKey != "updateRel" &&  model.ActionKey != "delete-relation-fk" && model.ActionKey != "restore" )
			{
				ContextRequest context = SFSdotNet.Framework.My.Context.BuildContextRequestSafe(System.Web.HttpContext.Current);
				context.UseMode = dataAction.Usemode;

				if (model.IsBackground)
				{
					System.Threading.Tasks.Task.Run(() => 
						OnCustomActionExecutingBackground(this, actionEventArgs = new MyEventArgs<ContextActionModel<RouteLocationModel>>() { Object = new ContextActionModel<RouteLocationModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } })
					);
				}
				else
				{
					OnCustomActionExecuting(this, actionEventArgs = new MyEventArgs<ContextActionModel<RouteLocationModel>>() {  Object = new ContextActionModel<RouteLocationModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } });
				}
			}
            List<RouteLocationModel> results = null;
	
			if (model.ActionKey == "deletemany") { 
				
				BR.RouteLocationsBR.Instance.Delete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

            }
	
			else if (model.ActionKey == "restore") {
                    BR.RouteLocationsBR.Instance.UnDelete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

                }
            else if (model.ActionKey == "updateRel" || model.ActionKey == "delete-relation-fk" || model.ActionKey == "updateRel-proxyMany")
            {
               try {
                   string valueForUpdate = null;
				   string propForUpdate = null;
				   if (!string.IsNullOrEmpty(Request.Params["propertyForUpdate"])){
						propForUpdate = Request.Params["propertyForUpdate"];
				   }
				    if (string.IsNullOrEmpty(propForUpdate) && !string.IsNullOrEmpty(Request.QueryString["propertyForUpdate"]))
                   {
                       propForUpdate = Request.QueryString["propertyForUpdate"];
                   }
                    if (model.ActionKey != "delete-relation-fk")
                    {
                        valueForUpdate = Request.QueryString["valueForUpdate"];
                    }
                    BR.RouteLocationsBR.Instance.UpdateAssociation(propForUpdate, valueForUpdate, model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());
					
                    if (model.ActionKey == "delete-relation-fk")
                    {
                        MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]);

                        return PartialView("ResultMessageView", message);
                    }
                    else
                    {
                        return Content("ok");
                    }
       
          
                }
                catch (Exception ex)
                {
				        SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
           
                }
            
            }
		
                if (actionEventArgs == null && !model.IsBackground)
                {
                    //if (model.ActionKey != "deletemany"  && model.ActionKey != "deleterelmany")
                    //{
                     //   throw new NotImplementedException("");
                    //}
                }
                else
                {
					if (model.IsBackground == false )
						 replaceResult = actionEventArgs.Object.Result is ActionResult /*actionEventArgs.Object.ReplaceResult*/;
                }
                #endregion
                if (!replaceResult)
                {
                    if (Request != null && Request.IsAjaxRequest())
                    {
						MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]) ;
                        if (model.IsBackground )
                            message.Message = GlobalMessages.THE_PROCESS_HAS_BEEN_STARTED;
                        
                        return PartialView("ResultMessageView", message);                    
					}
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return (ActionResult)actionEventArgs.Object.Result;
                }
            }
            catch (Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null) {
                        message = ex.Data["usermessage"].ToString();
                    }
					 SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }

            }
        }
        //
        // POST: /RouteLocations/Delete/5
        
			
	
    }
}
namespace MBK.YellowBox.Web.Mvc.Controllers
{
	using MBK.YellowBox.Web.Mvc.Models.Students;

    public partial class StudentsController : MBK.YellowBox.Web.Mvc.ControllerBase<Models.Students.StudentModel>
    {

       


	#region partial methods
        ControllerEventArgs<Models.Students.StudentModel> e = null;
        partial void OnValidating(object sender, ControllerEventArgs<Models.Students.StudentModel> e);
        partial void OnGettingExtraData(object sender, MyEventArgs<UIModel<Models.Students.StudentModel>> e);
        partial void OnCreating(object sender, ControllerEventArgs<Models.Students.StudentModel> e);
        partial void OnCreated(object sender, ControllerEventArgs<Models.Students.StudentModel> e);
        partial void OnEditing(object sender, ControllerEventArgs<Models.Students.StudentModel> e);
        partial void OnEdited(object sender, ControllerEventArgs<Models.Students.StudentModel> e);
        partial void OnDeleting(object sender, ControllerEventArgs<Models.Students.StudentModel> e);
        partial void OnDeleted(object sender, ControllerEventArgs<Models.Students.StudentModel> e);
    	partial void OnShowing(object sender, MyEventArgs<UIModel<Models.Students.StudentModel>> e);
    	partial void OnGettingByKey(object sender, ControllerEventArgs<Models.Students.StudentModel> e);
        partial void OnTaken(object sender, ControllerEventArgs<Models.Students.StudentModel> e);
       	partial void OnCreateShowing(object sender, ControllerEventArgs<Models.Students.StudentModel> e);
		partial void OnEditShowing(object sender, ControllerEventArgs<Models.Students.StudentModel> e);
		partial void OnDetailsShowing(object sender, ControllerEventArgs<Models.Students.StudentModel> e);
 		partial void OnActionsCreated(object sender, MyEventArgs<UIModel<Models.Students.StudentModel >> e);
		partial void OnCustomActionExecuting(object sender, MyEventArgs<ContextActionModel<Models.Students.StudentModel>> e);
		partial void OnCustomActionExecutingBackground(object sender, MyEventArgs<ContextActionModel<Models.Students.StudentModel>> e);
        partial void OnDownloading(object sender, MyEventArgs<ContextActionModel<Models.Students.StudentModel>> e);
      	partial void OnAuthorization(object sender, AuthorizationContext context);
		 partial void OnFilterShowing(object sender, MyEventArgs<UIModel<Models.Students.StudentModel >> e);
         partial void OnSummaryOperationShowing(object sender, MyEventArgs<UIModel<Models.Students.StudentModel>> e);

        partial void OnExportActionsCreated(object sender, MyEventArgs<UIModel<Models.Students.StudentModel>> e);


		protected override void OnVirtualFilterShowing(object sender, MyEventArgs<UIModel<StudentModel>> e)
        {
            OnFilterShowing(sender, e);
        }
		 public override void OnVirtualExportActionsCreated(object sender, MyEventArgs<UIModel<StudentModel>> e)
        {
            OnExportActionsCreated(sender, e);
        }
        public override void OnVirtualDownloading(object sender, MyEventArgs<ContextActionModel<StudentModel>> e)
        {
            OnDownloading(sender, e);
        }
        public override void OnVirtualShowing(object sender, MyEventArgs<UIModel<StudentModel>> e)
        {
            OnShowing(sender, e);
        }

	#endregion
	#region API
	 public override ActionResult ApiCreateGen(StudentModel model, ContextRequest contextRequest)
        {
            return CreateGen(model, contextRequest);
        }

              public override ActionResult ApiGetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJson(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }
        public override ActionResult ApiGetByKeyJson(string id, ContextRequest contextRequest)
        {
            return  GetByKeyJson(id, contextRequest, true);
        }
      
		 public override int ApiGetByCount(string filter, ContextRequest contextRequest)
        {
            return GetByCount(filter, contextRequest);
        }
         protected override ActionResult ApiDeleteGen(List<StudentModel> models, ContextRequest contextRequest)
        {
            List<Student> objs = new List<Student>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                BR.StudentsBR.Instance.DeleteBulk(objs, contextRequest);
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        protected override ActionResult ApiUpdateGen(List<StudentModel> models, ContextRequest contextRequest)
        {
            List<Student> objs = new List<Student>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                foreach (var obj in objs)
                {
                    BR.StudentsBR.Instance.Update(obj, contextRequest);

                }
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }


	#endregion
#region Validation methods	
	    private void Validations(StudentModel model) { 
            #region Remote validations

            #endregion
		}

#endregion
		
 		public AuthorizationContext Authorization(AuthorizationContext context)
        {
            OnAuthorization(this,  context );
            return context ;
        }
		public List<StudentModel> GetAll() {
            			var bos = BR.StudentsBR.Instance.GetBy("",
					new SFSdotNet.Framework.My.ContextRequest()
					{
						CustomQuery = new SFSdotNet.Framework.My.CustomQuery()
						{
							OrderBy = "FullName",
							SortDirection = SFSdotNet.Framework.Data.SortDirection.Ascending
						}
					});
            			List<StudentModel> results = new List<StudentModel>();
            StudentModel model = null;
            foreach (var bo in bos)
            {
                model = new StudentModel();
                model.Bind(bo);
                results.Add(model);
            }
            return results;

        }
        //
        // GET: /Students/
		[MyAuthorize("r", "Student", "MBKYellowBox", typeof(StudentsController))]
		public ActionResult Index()
        {
    		var uiModel = GetContextModel(UIModelContextTypes.ListForm, null);
			ViewBag.UIModel = uiModel;
			uiModel.FilterStart = (string)ViewData["startFilter"];
                    MyEventArgs<UIModel<StudentModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<StudentModel>>() { Object = uiModel });

			OnExportActionsCreated(this, (me != null ? me : me = new MyEventArgs<UIModel<StudentModel>>() { Object = uiModel }));

            if (me != null)
            {
                uiModel = me.Object;
            }
            if (me == null)
                me = new MyEventArgs<UIModel<StudentModel>>() { Object = uiModel };
           
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;


            //return View("ListGen");
			return ResolveView(uiModel);
        }
		[MyAuthorize("r", "Student", "MBKYellowBox", typeof(StudentsController))]
		public ActionResult ListViewGen(string idTab, string fk , string fkValue, string startFilter, ListModes  listmode  = ListModes.SimpleList, PropertyDefinition parentRelationProperty = null, object parentRelationPropertyValue = null )
        {
			ViewData["idTab"] = System.Web.HttpContext.Current.Request.QueryString["idTab"]; 
		 	ViewData["detpop"] = true; // details in popup
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"])) {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"]; 
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
            {
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["startFilter"]))
            {
                ViewData["startFilter"] = Request.QueryString["startFilter"];
            }
			
			UIModel<StudentModel> uiModel = GetContextModel(UIModelContextTypes.ListForm, null);

            MyEventArgs<UIModel<StudentModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<StudentModel>>() { Object = uiModel });
            if (me == null)
                me = new MyEventArgs<UIModel<StudentModel>>() { Object = uiModel };
            uiModel.Properties = GetProperties(uiModel);
            uiModel.ContextType = UIModelContextTypes.ListForm;
             uiModel.FilterStart = (string)ViewData["startFilter"];
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;
			 if (listmode == ListModes.SimpleList)
                return ResolveView(uiModel);
            else
            {
                ViewData["parentRelationProperty"] = parentRelationProperty;
                ViewData["parentRelationPropertyValue"] = parentRelationPropertyValue;
                return PartialView("ListForTagSelectView");
            }
            return ResolveView(uiModel);
        }
		List<PropertyDefinition> _properties = null;

		 protected override List<PropertyDefinition> GetProperties(UIModel uiModel,  params string[] specificProperties)
        { 
            return GetProperties(uiModel, false, null, specificProperties);
        }

		protected override List<PropertyDefinition> GetProperties(UIModel uiModel, bool decripted, Guid? id, params string[] specificProperties)
            {

			bool allProperties = true;    
                if (specificProperties != null && specificProperties.Length > 0)
                {
                    allProperties = false;
                }


			List<CustomProperty> customProperties = new List<CustomProperty>();
			if (_properties == null)
                {
                List<PropertyDefinition> results = new List<PropertyDefinition>();

			string idStudent = GetRouteDataOrQueryParam("id");
			if (idStudent != null)
			{
				if (!decripted)
                {
					idStudent = SFSdotNet.Framework.Entities.Utils.GetPropertyKey(idStudent.Replace("-","/"), "GuidStudent");
				}else{
					if (id != null )
						idStudent = id.Value.ToString();                

				}
			}

			bool visibleProperty = true;	
			 bool conditionalshow =false;
                if (uiModel.ContextType == UIModelContextTypes.EditForm || uiModel.ContextType == UIModelContextTypes.DisplayForm ||  uiModel.ContextType == UIModelContextTypes.GenericForm )
                    conditionalshow = true;
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidStudent"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidStudent")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 100,
																	
					CustomProperties = customProperties,

                    PropertyName = "GuidStudent",

					 MaxLength = 0,
					IsRequired = true ,
					IsHidden = true,
                    SystemProperty =  SystemProperties.Identifier ,
					IsDefaultProperty = false,
                    SortBy = "GuidStudent",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentResources.GUIDSTUDENT*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("FullName"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "FullName")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 101,
																	
					CustomProperties = customProperties,

                    PropertyName = "FullName",

					 MaxLength = 255,
					 Nullable = true,
					IsDefaultProperty = true,
                    SortBy = "FullName",
					
	
                    TypeName = "String",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentResources.FULLNAME*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("BirthDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "BirthDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 102,
																	
					CustomProperties = customProperties,

                    PropertyName = "BirthDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "BirthDate",
					
	
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentResources.BIRTHDATE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Grade"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Grade")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 103,
																	
					CustomProperties = customProperties,

                    PropertyName = "Grade",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Grade",
					
	
                    TypeName = "Int32",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentResources.GRADE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Group"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Group")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 104,
																	
					CustomProperties = customProperties,

                    PropertyName = "Group",

					 MaxLength = 2,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Group",
					
	
                    TypeName = "String",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentResources.GROUP*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidParent"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidParent")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 105,
																	IsForeignKey = true,

									
					CustomProperties = customProperties,

                    PropertyName = "GuidParent",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "GuidParent",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentResources.GUIDPARENT*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 106,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedDate",
					
	
				SystemProperty = SystemProperties.CreatedDate ,
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentResources.CREATEDDATE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 128,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedDate",
					
	
					IsUpdatedDate = true,
					SystemProperty = SystemProperties.UpdatedDate ,
	
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    ,PropertyDisplayName = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.UPDATED

                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 108,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedBy",
					
	
				SystemProperty = SystemProperties.CreatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentResources.CREATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 109,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedBy",
					
	
				SystemProperty = SystemProperties.UpdatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentResources.UPDATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Bytes"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Bytes")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 110,
																	
					CustomProperties = customProperties,

                    PropertyName = "Bytes",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Bytes",
					
	
				SystemProperty = SystemProperties.SizeBytes,
                    TypeName = "Int32",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentResources.BYTES*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Comments"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Comments")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 111,
																	
					CustomProperties = customProperties,

                    PropertyName = "Comments",

					 MaxLength = 5000,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Comments",
					
	
                    TypeName = "String",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentResources.COMMENTS*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidAcademyLevel"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidAcademyLevel")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 112,
																	IsForeignKey = true,

									
					CustomProperties = customProperties,

                    PropertyName = "GuidAcademyLevel",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "GuidAcademyLevel",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentResources.GUIDACADEMYLEVEL*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidProfesor"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidProfesor")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 113,
																	IsForeignKey = true,

									
					CustomProperties = customProperties,

                    PropertyName = "GuidProfesor",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "GuidProfesor",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentResources.GUIDPROFESOR*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("RouteLocations"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"Student" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="RouteLocations.GuidRouteLocation")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"RouteLocations.GuidRouteLocation" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"RouteLocations" });
			

	
	//fk_RouteLocation_Student
		//if (this.Request.QueryString["fk"] != "RouteLocations")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 114,
																
					Link = VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/RouteLocations/ListViewGen?overrideModule=" + GetOverrideApp()  + "&pal=False&es=False&pag=10&filterlinks=1&idTab=RouteLocations&fk=Student&startFilter="+ (new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext)).Encode("it.Student.GuidStudent = Guid(\"" + idStudent +"\")")+ "&fkValue=" + idStudent,
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "RouteLocation",
					
					CustomProperties = customProperties,

                    PropertyName = "RouteLocations",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "RouteLocations.OrderRoute",
					
	
                    TypeName = "MBKYellowBoxModel.RouteLocation",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = true,
                    PathName = "MBKYellowBox/RouteLocations"
                    /*,PropertyDisplayName = Resources.StudentResources.ROUTELOCATIONS*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("StudentLocations"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"Student" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="StudentLocations.GuidStudentLocation")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"StudentLocations.GuidStudentLocation" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"StudentLocations" });
			

	
	//fk_StudentLocation_Student
		//if (this.Request.QueryString["fk"] != "StudentLocations")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 115,
																
					Link = VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/StudentLocations/ListViewGen?overrideModule=" + GetOverrideApp()  + "&pal=False&es=False&pag=10&filterlinks=1&idTab=StudentLocations&fk=Student&startFilter="+ (new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext)).Encode("it.Student.GuidStudent = Guid(\"" + idStudent +"\")")+ "&fkValue=" + idStudent,
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "StudentLocation",
					
					CustomProperties = customProperties,

                    PropertyName = "StudentLocations",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "StudentLocations.Bytes",
					
	
                    TypeName = "MBKYellowBoxModel.StudentLocation",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = true,
                    PathName = "MBKYellowBox/StudentLocations"
                    /*,PropertyDisplayName = Resources.StudentResources.STUDENTLOCATIONS*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("StudentParent"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"Students" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="StudentParent.GuidStudentParent")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"StudentParent.GuidStudentParent" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"StudentParents" });
			

	
	//fk_Student_Parent
		//if (this.Request.QueryString["fk"] != "StudentParent")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 116,
																
					
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "StudentParent",
					PropertyNavigationKey = "GuidStudentParent",
					PropertyNavigationText = "FullName",
					NavigationPropertyType = NavigationPropertyTypes.Autocomplete,
					GetMethodName = "GetAll",
					GetMethodParameters = "",
					GetMethodDisplayText ="FullName",
					GetMethodDisplayValue = "GuidStudentParent",
					
					CustomProperties = customProperties,

                    PropertyName = "StudentParent",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "StudentParent.FullName",
					
	
                    TypeName = "MBKYellowBoxModel.StudentParent",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/StudentParents"
                    /*,PropertyDisplayName = Resources.StudentResources.STUDENTPARENT*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("AcademyLevel"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"Students" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="AcademyLevel.GuidAcademyLevel")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"AcademyLevel.GuidAcademyLevel" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"AcademyLevels" });
			

	
	//fk_Student_AcademyLevel
		//if (this.Request.QueryString["fk"] != "AcademyLevel")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 117,
																
					
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "AcademyLevel",
					PropertyNavigationKey = "GuidAcademyLevel",
					PropertyNavigationText = "Title",
					NavigationPropertyType = NavigationPropertyTypes.SimpleDropDown,
					GetMethodName = "GetAll",
					GetMethodParameters = "",
					GetMethodDisplayText ="Title",
					GetMethodDisplayValue = "GuidAcademyLevel",
					
					CustomProperties = customProperties,

                    PropertyName = "AcademyLevel",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "AcademyLevel.Title",
					
	
                    TypeName = "MBKYellowBoxModel.AcademyLevel",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/AcademyLevels"
                    /*,PropertyDisplayName = Resources.StudentResources.ACADEMYLEVEL*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Profesor"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"Students" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="Profesor.GuidProfesor")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"Profesor.GuidProfesor" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"Profesors" });
			

	
	//fk_Student_Profesor
		//if (this.Request.QueryString["fk"] != "Profesor")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 118,
																
					
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "Profesor",
					PropertyNavigationKey = "GuidProfesor",
					PropertyNavigationText = "FullName",
					NavigationPropertyType = NavigationPropertyTypes.SimpleDropDown,
					GetMethodName = "GetAll",
					GetMethodParameters = "",
					GetMethodDisplayText ="FullName",
					GetMethodDisplayValue = "GuidProfesor",
					
					CustomProperties = customProperties,

                    PropertyName = "Profesor",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Profesor.FullName",
					
	
                    TypeName = "MBKYellowBoxModel.Profesor",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/Profesors"
                    /*,PropertyDisplayName = Resources.StudentResources.PROFESOR*/
                });
		//	}
	
	}
	
				
                    _properties = results;
                    return _properties;
                }
                else {
                    return _properties;
                }
            }

		protected override  UIModel<StudentModel> GetByForShow(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, params  object[] extraParams)
        {
			if (Request != null )
				if (!string.IsNullOrEmpty(Request.QueryString["q"]))
					filter = filter + HttpUtility.UrlDecode(Request.QueryString["q"]);
 if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
            }
            var bos = BR.StudentsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), contextRequest, extraParams);
			//var bos = BR.StudentsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), context, extraParams);
            StudentModel model = null;
            List<StudentModel> results = new List<StudentModel>();
            foreach (var item in bos)
            {
                model = new StudentModel();
				model.Bind(item);
				results.Add(model);
            }
            //return results;
			UIModel<StudentModel> uiModel = GetContextModel(UIModelContextTypes.Items, null);
            uiModel.Items = results;
			if (Request != null){
				if (SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(Request.RequestContext, "action") == "Download")
				{
					uiModel.ContextType = UIModelContextTypes.ExportDownload;
				}
			}
            Showing(ref uiModel);
            return uiModel;
		}			
		
		//public List<StudentModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params  object[] extraParams)
        //{
		//	var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, null, extraParams);
        public override List<StudentModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest,  params  object[] extraParams)
        {
            var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
           
            return uiModel.Items;
		
        }
		/*
        [MyAuthorize("r", "Student", "MBKYellowBox", typeof(StudentsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir)
        {
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir);
        }*/

		  [MyAuthorize("r", "Student", "MBKYellowBox", typeof(StudentsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir,ContextRequest contextRequest,  object[] extraParams)
        {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir,contextRequest, extraParams);
        }
/*		  [MyAuthorize("r", "Student", "MBKYellowBox", typeof(StudentsController))]
       public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJsonBase(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }*/
		[MyAuthorize()]
		public int GetByCount(string filter, ContextRequest contextRequest) {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
            return BR.StudentsBR.Instance.GetCount(HttpUtility.UrlDecode(filter), GetUseMode(), contextRequest);
        }
		

		[MyAuthorize("r", "Student", "MBKYellowBox", typeof(StudentsController))]
        public ActionResult GetByKeyJson(string id, ContextRequest contextRequest,  bool dec = false)
        {
            return Json(GetByKey(id, null, contextRequest, dec), JsonRequestBehavior.AllowGet);
        }
		public StudentModel GetByKey(string id) {
			return GetByKey(id, null,null, false);
       	}
		    public StudentModel GetByKey(string id, string includes)
        {
            return GetByKey(id, includes, false);
        }
		 public  StudentModel GetByKey(string id, string includes, ContextRequest contextRequest)
        {
            return GetByKey(id, includes, contextRequest, false);
        }
		/*
		  public ActionResult ShowField(string fieldName, string idField) {
		   string safePropertyName = fieldName;
              if (fieldName.StartsWith("Fk"))
              {
                  safePropertyName = fieldName.Substring(2, fieldName.Length - 2);
              }

             StudentModel model = new  StudentModel();

            UIModel uiModel = GetUIModel(model, new string[] { "NoField-" });
			
				uiModel.Properties = GetProperties(uiModel, safePropertyName);
		uiModel.Properties.ForEach(p=> p.ContextType = uiModel.ContextType );
            uiModel.ContextType = UIModelContextTypes.FilterFields;
            uiModel.OverrideApp = GetOverrideApp();
            uiModel.UseMode = GetUseMode();

            ViewData["uiModel"] = uiModel;
			var prop = uiModel.Properties.FirstOrDefault(p=>p.PropertyName == safePropertyName);
            //if (prop.IsNavigationProperty && prop.IsNavigationPropertyMany == false)
            //{
            //    ViewData["currentProperty"] = uiModel.Properties.FirstOrDefault(p => p.PropertyName != fieldName + "Text");
            //}else if (prop.IsNavigationProperty == false){
                ViewData["currentProperty"] = prop;
           // }
            ((PropertyDefinition)ViewData["currentProperty"]).RemoveLayout = true;
			ViewData["withContainer"] = false;


            return PartialView("GenericField", model);


        }
      */
	public StudentModel GetByKey(string id, ContextRequest contextRequest, bool dec)
        {
            return GetByKey(id, null, contextRequest, dec);
        }
        public StudentModel GetByKey(string id, string  includes, bool dec)
        {
            return GetByKey(id, includes, null, dec);
        }

        public StudentModel GetByKey(string id, string includes, ContextRequest contextRequest, bool dec) {
		             StudentModel model = null;
            ControllerEventArgs<StudentModel> e = null;
			string objectKey = id.Replace("-","/");
             OnGettingByKey(this, e=  new ControllerEventArgs<StudentModel>() { Id = objectKey  });
             bool cancel = false;
             StudentModel eItem = null;
             if (e != null)
             {
                 cancel = e.Cancel;
                 eItem = e.Item;
             }
			if (cancel == false && eItem == null)
             {
			Guid guidStudent = Guid.Empty; //new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, "GuidStudent"));
			if (dec)
                 {
                     guidStudent = new Guid(id);
                 }
                 else
                 {
                     guidStudent = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, null));
                 }
			
            
				model = new StudentModel();
                  if (contextRequest == null)
                {
                    contextRequest = GetContextRequest();
                }
				var bo = BR.StudentsBR.Instance.GetByKey(guidStudent, GetUseMode(), contextRequest,  includes);
				 if (bo != null)
                    model.Bind(bo);
                else
                    return null;
			}
             else {
                 model = eItem;
             }
			model.IsNew = false;

            return model;
        }
        // GET: /Students/DetailsGen/5
		[MyAuthorize("r", "Student", "MBKYellowBox", typeof(StudentsController))]
        public ActionResult DetailsGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = StudentResources.ENTITY_PLURAL;
			 #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<StudentModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
            #endregion



			 bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null) {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
			//UIModel<StudentModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, decripted), decripted, guidId);
			var item = GetByKey(id, null, null, decripted);
			if (item == null)
            {
                 RouteValueDictionary rv = new RouteValueDictionary();
                string usemode = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"usemode");
                string overrideModule = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "overrideModule");
                string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");

                if(!string.IsNullOrEmpty(usemode)){
                    rv.Add("usemode", usemode);
                }
                if(!string.IsNullOrEmpty(overrideModule)){
                    rv.Add("overrideModule", overrideModule);
                }
                if (!string.IsNullOrEmpty(area))
                {
                    rv.Add("area", area);
                }

                return RedirectToAction("Index", rv);
            }
            //
            UIModel<StudentModel> uiModel = null;
                uiModel = GetContextModel(UIModelContextTypes.DisplayForm, item, decripted, guidId);



            MyEventArgs<UIModel<StudentModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<StudentModel>>() { Object = uiModel });

            if (me != null) {
                uiModel = me.Object;
            }
			
            Showing(ref uiModel);
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			
            //return View("DisplayGen", uiModel.Items[0]);
			return ResolveView(uiModel, uiModel.Items[0]);

        }
		[MyAuthorize("r", "Student", "MBKYellowBox", typeof(StudentsController))]
		public ActionResult DetailsViewGen(string id)
        {

		 bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<StudentModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
           
			if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
 			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
           
        	 //var uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id));
			 
            bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
            {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true")
                {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
            UIModel<StudentModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, null, decripted), decripted, guidId);
			

            MyEventArgs<UIModel<StudentModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<StudentModel>>() { Object = uiModel });

            if (me != null)
            {
                uiModel = me.Object;
            }
            
            Showing(ref uiModel);
            return ResolveView(uiModel, uiModel.Items[0]);
        
        }
        //
        // GET: /Students/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        //
        // GET: /Students/CreateGen
		[MyAuthorize("c", "Student", "MBKYellowBox", typeof(StudentsController))]
        public ActionResult CreateGen()
        {
			StudentModel model = new StudentModel();
            model.IsNew = true;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model);

			OnCreateShowing(this, e = new ControllerEventArgs<StudentModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }

             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                 ViewData["ispopup"] = true;
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                 ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                 ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

            Showing(ref me);

			return ResolveView(me, me.Items[0]);
        } 
			
		protected override UIModel<StudentModel> GetContextModel(UIModelContextTypes formMode, StudentModel model)
        {
            return GetContextModel(formMode, model, false, null);
        }
			
		 private UIModel<StudentModel> GetContextModel(UIModelContextTypes formMode, StudentModel model, bool decript, Guid ? id) {
            UIModel<StudentModel> me = new UIModel<StudentModel>(true, "Students");
			me.UseMode = GetUseMode();
			me.Controller = this;
			me.OverrideApp = GetOverrideApp();
			me.ContextType = formMode ;
			me.Id = "Student";
			
            me.ModuleKey = "MBKYellowBox";

			me.ModuleNamespace = "MBK.YellowBox";
            me.EntityKey = "Student";
            me.EntitySetName = "Students";

			me.AreaAction = "MBKYellowBox";
            me.ControllerAction = "Students";
            me.PropertyKeyName = "GuidStudent";

            me.Properties = GetProperties(me, decript, id);

			me.SortBy = "UpdatedDate";
			me.SortDirection = UIModelSortDirection.DESC;

 			
			if (Request != null)
            {
                string actionName = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam( Request.RequestContext, "action");
                if(actionName != null && actionName.ToLower().Contains("create") ){
                    me.IsNew = true;
                }
            }
			 #region Buttons
			 if (Request != null ){
             if (formMode == UIModelContextTypes.DisplayForm || formMode == UIModelContextTypes.EditForm || formMode == UIModelContextTypes.ListForm)
				me.ActionButtons = GetActionButtons(formMode,model != null ?(Request.QueryString["dec"] == "true" ? model.Id : model.SafeKey)  : null, "MBKYellowBox", "Students", "Student", me.IsNew);

            //me.ActionButtons.Add(new ActionModel() { ActionKey = "return", Title = GlobalMessages.RETURN, Url = System.Web.VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/Students" });
			if (this.HttpContext != null &&  !this.HttpContext.SkipAuthorization){
				//antes this.HttpContext
				me.SetAction("u", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("u", "Student", "MBKYellowBox"));
				me.SetAction("c", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("c", "Student", "MBKYellowBox"));
				me.SetAction("d", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("d", "Student", "MBKYellowBox"));
			
			}else{
				me.SetAction("u", true);
				me.SetAction("c", true);
				me.SetAction("d", true);

			}
            #endregion              
         
            switch (formMode)
            {
                case UIModelContextTypes.DisplayForm:
					//me.TitleForm = StudentResources.STUDENTS_DETAILS;
                    me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.MODIFY_DATA;
					 me.Properties.Where(p=>p.PropertyName  != "Id" && p.IsForeignKey == false).ToList().ForEach(p => p.IsHidden = false);

					 me.Properties.Where(p => (p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier) ).ToList().ForEach(p=> me.SetHide(p.PropertyName));

                    break;
                case UIModelContextTypes.EditForm:
				  me.Properties.Where(p=>p.SystemProperty != SystemProperties.Identifier && p.IsForeignKey == false && p.PropertyName != "Id").ToList().ForEach(p => p.IsHidden = false);

					if (model != null)
                    {
						

                        me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.SAVE_DATA;                        
                        me.ActionButtons.First(p => p.ActionKey == "c").Title = GlobalMessages.SAVE_DATA;
						if (model.IsNew ){
							//me.TitleForm = StudentResources.STUDENTS_ADD_NEW;
							me.ActionName = "CreateGen";
							me.Properties.RemoveAll(p => p.SystemProperty != null || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));
						}else{
							
							me.ActionName = "EditGen";

							//me.TitleForm = StudentResources.STUDENTS_EDIT;
							me.Properties.RemoveAll(p => p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));	
						}
						//me.Properties.Remove(me.Properties.Find(p => p.PropertyName == "UpdatedDate"));
					
					}
                    break;
                case UIModelContextTypes.FilterFields:
                    break;
                case UIModelContextTypes.GenericForm:
                    break;
                case UIModelContextTypes.Items:
				//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "FullName") != null){
						me.Properties.Find(p => p.PropertyName == "FullName").IsHidden = false;
					 }
					 
                    
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					 
                    
					

						 if (me.Properties.Find(p => p.PropertyName == "GuidStudent") != null){
						me.Properties.Find(p => p.PropertyName == "GuidStudent").IsHidden = false;
					 }
					 
                    
					


                  


					//}
                    break;
                case UIModelContextTypes.ListForm:
					PropertyDefinition propFinded = null;
					//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "FullName") != null){
						me.Properties.Find(p => p.PropertyName == "FullName").IsHidden = false;
					 }
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					
					me.PrincipalActionName = "GetByJson";
					//}
					//me.TitleForm = StudentResources.STUDENTS_LIST;
                    break;
                default:
                    break;
            }
            	this.SetDefaultProperties(me);
			}
			if (model != null )
            	me.Items.Add(model);
            return me;
        }
		// GET: /Students/CreateViewGen
		[MyAuthorize("c", "Student", "MBKYellowBox", typeof(StudentsController))]
        public ActionResult CreateViewGen()
        {
				StudentModel model = new StudentModel();
            model.IsNew = true;
			e= null;
			OnCreateShowing(this, e = new ControllerEventArgs<StudentModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }
			
            var me = GetContextModel(UIModelContextTypes.EditForm, model);

			me.IsPartialView = true;	
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
            {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                me.Properties.Find(p => p.PropertyName == ViewData["fk"].ToString()).IsReadOnly = true;
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
			
      
            //me.Items.Add(model);
            Showing(ref me);
            return ResolveView(me, me.Items[0]);
        }
		protected override  void GettingExtraData(ref UIModel<StudentModel> uiModel)
        {

            MyEventArgs<UIModel<StudentModel>> me = null;
            OnGettingExtraData(this, me = new MyEventArgs<UIModel<StudentModel>>() { Object = uiModel });
            //bool maybeAnyReplaced = false; 
            if (me != null)
            {
                uiModel = me.Object;
                //maybeAnyReplaced = true;
            }
           
			bool canFill = false;
			 string query = null ;
            bool isFK = false;
			PropertyDefinition prop =null;
			var contextRequest = this.GetContextRequest();
            contextRequest.CustomParams.Add(new CustomParam() { Name="ui", Value= Student.EntityName });

            			canFill = false;
			 query = "";
            isFK =false;
			prop = uiModel.Properties.FirstOrDefault(p => p.PropertyName == "StudentParent");
			if (prop != null)
				if (prop.IsHidden == false && (prop.ContextType == UIModelContextTypes.EditForm || prop.ContextType == UIModelContextTypes.FilterFields  || (prop.ContextType == null && uiModel.ContextType == UIModelContextTypes.EditForm)))
				{
					if (prop.NavigationPropertyType == NavigationPropertyTypes.SimpleDropDown )
						canFill = true;
				}
                else if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidStudentParent = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidStudentParent = @GuidStudentParent";
					
					canFill = true;
                }
				if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidStudentParent = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidStudentParent = @GuidStudentParent";
					canFill = true;
                }
			if (canFill){
			                contextRequest.CustomQuery = new CustomQuery();

				if (!uiModel.ExtraData.Exists(p => p.PropertyName == "StudentParent")) {
					if (!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))				  
						contextRequest.CustomQuery.SetParam("GuidStudentParent", new Nullable<Guid>(Guid.Parse( Request.QueryString["fkValue"])));

					 if (isFK == true)
                    {
						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.StudentParentsBR()).GetBy(query, contextRequest), "GuidStudentParent", "FullName", Request.QueryString["fkValue"]), PropertyName = "StudentParent" });    
                    }
                    else
                    {

						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.StudentParentsBR()).GetBy(query, contextRequest), "GuidStudentParent", "FullName"), PropertyName = "StudentParent" });    

					}
    if (isFK)
                    {    
						var FkStudentParent = ((SelectList)uiModel.ExtraData.First(p => p.PropertyName == "StudentParent").Data).First();
						uiModel.Items[0].GetType().GetProperty("FkStudentParentText").SetValue(uiModel.Items[0], FkStudentParent.Text);
						uiModel.Items[0].GetType().GetProperty("FkStudentParent").SetValue(uiModel.Items[0], Guid.Parse(FkStudentParent.Value));
                    
					}    
				}
			}
		 
			canFill = false;
			 query = "";
            isFK =false;
			prop = uiModel.Properties.FirstOrDefault(p => p.PropertyName == "AcademyLevel");
			if (prop != null)
				if (prop.IsHidden == false && (prop.ContextType == UIModelContextTypes.EditForm || prop.ContextType == UIModelContextTypes.FilterFields  || (prop.ContextType == null && uiModel.ContextType == UIModelContextTypes.EditForm)))
				{
					if (prop.NavigationPropertyType == NavigationPropertyTypes.SimpleDropDown )
						canFill = true;
				}
                else if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidAcademyLevel = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidAcademyLevel = @GuidAcademyLevel";
					
					canFill = true;
                }
				if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidAcademyLevel = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidAcademyLevel = @GuidAcademyLevel";
					canFill = true;
                }
			if (canFill){
			                contextRequest.CustomQuery = new CustomQuery();

				if (!uiModel.ExtraData.Exists(p => p.PropertyName == "AcademyLevel")) {
					if (!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))				  
						contextRequest.CustomQuery.SetParam("GuidAcademyLevel", new Nullable<Guid>(Guid.Parse( Request.QueryString["fkValue"])));

					 if (isFK == true)
                    {
						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.AcademyLevelsBR()).GetBy(query, contextRequest), "GuidAcademyLevel", "Title", Request.QueryString["fkValue"]), PropertyName = "AcademyLevel" });    
                    }
                    else
                    {

						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.AcademyLevelsBR()).GetBy(query, contextRequest), "GuidAcademyLevel", "Title"), PropertyName = "AcademyLevel" });    

					}
    if (isFK)
                    {    
						var FkAcademyLevel = ((SelectList)uiModel.ExtraData.First(p => p.PropertyName == "AcademyLevel").Data).First();
						uiModel.Items[0].GetType().GetProperty("FkAcademyLevelText").SetValue(uiModel.Items[0], FkAcademyLevel.Text);
						uiModel.Items[0].GetType().GetProperty("FkAcademyLevel").SetValue(uiModel.Items[0], Guid.Parse(FkAcademyLevel.Value));
                    
					}    
				}
			}
		 
			canFill = false;
			 query = "";
            isFK =false;
			prop = uiModel.Properties.FirstOrDefault(p => p.PropertyName == "Profesor");
			if (prop != null)
				if (prop.IsHidden == false && (prop.ContextType == UIModelContextTypes.EditForm || prop.ContextType == UIModelContextTypes.FilterFields  || (prop.ContextType == null && uiModel.ContextType == UIModelContextTypes.EditForm)))
				{
					if (prop.NavigationPropertyType == NavigationPropertyTypes.SimpleDropDown )
						canFill = true;
				}
                else if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidProfesor = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidProfesor = @GuidProfesor";
					
					canFill = true;
                }
				if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidProfesor = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidProfesor = @GuidProfesor";
					canFill = true;
                }
			if (canFill){
			                contextRequest.CustomQuery = new CustomQuery();

				if (!uiModel.ExtraData.Exists(p => p.PropertyName == "Profesor")) {
					if (!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))				  
						contextRequest.CustomQuery.SetParam("GuidProfesor", new Nullable<Guid>(Guid.Parse( Request.QueryString["fkValue"])));

					 if (isFK == true)
                    {
						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.ProfesorsBR()).GetBy(query, contextRequest), "GuidProfesor", "FullName", Request.QueryString["fkValue"]), PropertyName = "Profesor" });    
                    }
                    else
                    {

						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.ProfesorsBR()).GetBy(query, contextRequest), "GuidProfesor", "FullName"), PropertyName = "Profesor" });    

					}
    if (isFK)
                    {    
						var FkProfesor = ((SelectList)uiModel.ExtraData.First(p => p.PropertyName == "Profesor").Data).First();
						uiModel.Items[0].GetType().GetProperty("FkProfesorText").SetValue(uiModel.Items[0], FkProfesor.Text);
						uiModel.Items[0].GetType().GetProperty("FkProfesor").SetValue(uiModel.Items[0], Guid.Parse(FkProfesor.Value));
                    
					}    
				}
			}
		 
            

        }
		private void Showing(ref UIModel<StudentModel> uiModel) {
          	
			MyEventArgs<UIModel<StudentModel>> me = new MyEventArgs<UIModel<StudentModel>>() { Object = uiModel };
			 OnVirtualLayoutSettings(this, me);


            OnShowing(this, me);

			
			if ((Request != null && Request.QueryString["allFields"] == "1") || Request == null )
			{
				me.Object.Properties.ForEach(p=> p.IsHidden = false);
            }
            if (me != null)
            {
                uiModel = me.Object;
            }
          


			 if (uiModel.ContextType == UIModelContextTypes.EditForm)
			    GettingExtraData(ref uiModel);
            ViewData["UIModel"] = uiModel;

        }
        //
        // POST: /Students/Create
		[MyAuthorize("c", "Student", "MBKYellowBox", typeof(StudentsController))]
        [HttpPost]
		[ValidateInput(false)] 
        public ActionResult CreateGen(StudentModel  model,  ContextRequest contextRequest)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
		 	e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<StudentModel>() { Item = model });
           
		  	if (!ModelState.IsValid) {
				model.IsNew = true;
				var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
                 if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                        ViewData["ispopup"] = true;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
                    return ResolveView(me, model);
            }
            try
            {
				if (model.GuidStudent == null || model.GuidStudent.ToString().Contains("000000000"))
				model.GuidStudent = Guid.NewGuid();
	
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnCreating(this, e = new ControllerEventArgs<StudentModel>() { Item = model });
                if (e != null) {
                   if (e.Cancel && e.RedirectValues.Count > 0){
                        RouteValueDictionary rv = new RouteValueDictionary();
                        if (e.RedirectValues["area"] != null ){
                            rv.Add("area", e.RedirectValues["area"].ToString());
                        }
                        foreach (var item in e.RedirectValues.Where(p=>p.Key != "area" && p.Key != "controller" &&  p.Key != "action" ))
	                    {
		                    rv.Add(item.Key, item.Value);
	                    }

                        //if (e.RedirectValues["action"] != null && e.RedirectValues["controller"] != null && e.RedirectValues["area"] != null )
                        return RedirectToAction(e.RedirectValues["action"].ToString(), e.RedirectValues["controller"].ToString(), rv );


                        
                    }else if (e.Cancel && e.ActionResult != null )
                        return e.ActionResult;  
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				if (contextRequest == null || contextRequest.Company == null){
					contextRequest = GetContextRequest();
					
				}
                if (!cancel)
                	model.Bind(StudentsBR.Instance.Create(model.GetBusinessObject(), contextRequest ));
				OnCreated(this, e = new ControllerEventArgs<StudentModel>() { Item = model });
                 if (e != null )
					if (e.ActionResult != null)
                    	replaceResult = true;		
				if (!replaceResult)
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))
                    {
                        ViewData["__continue"] = true;
                    }
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"]) &&  string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
                        popupextra.Add("id", model.SafeKey);
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                       {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                            popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"area"));
                            popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                            popupextra.Add("action", actionDetails);
                       if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
                    {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))

                        {
                            var popupextra = GetRouteData();
                            popupextra.Add("id", model.SafeKey);
                            return RedirectToAction("EditViewGen", popupextra);
                        }
                        else
                        {
                            return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.ADD_DONE));
                        }
                    }        			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                        return Redirect(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]);
                    else{

							RouteValueDictionary popupextra = null; 
							if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"])){
                            popupextra = GetRouteData();
							 string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
                            if (!string.IsNullOrEmpty(area))
                                popupextra.Add("area", area);
                            
                            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"])) {
								popupextra.Add("id", model.SafeKey);
                                return RedirectToAction("EditGen", popupextra);

                            }else{
								
                            return RedirectToAction("Index", popupextra);
							}
							}else{
								return Content("ok");
							}
                        }
						 }
                else {
                    return e.ActionResult;
                    }
				}
            catch(Exception ex)
            {
					if (!string.IsNullOrEmpty(Request.QueryString["rok"]))
                {
                    throw  ex;
                }
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                model.IsNew = true;
                var me = GetContextModel(UIModelContextTypes.EditForm, model, true, model.GuidStudent);
                Showing(ref me);
                if (isPopUp)
                {
                    
                        ViewData["ispopup"] = isPopUp;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
					if (Request != null)
						return ResolveView(me, model);
					else
						return Content("ok");
            }
        }        
        //
        // GET: /Students/Edit/5 
        public ActionResult Edit(int id)
        {
            return View();
        }
			
		
		[MyAuthorize("u", "Student", "MBKYellowBox", typeof(StudentsController))]
		[MvcSiteMapNode(Area="MBKYellowBox", Title="sss", Clickable=false, ParentKey = "MBKYellowBox_Student_List")]
		public ActionResult EditGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = StudentResources.ENTITY_SINGLE;		 	
  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<StudentModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
            StudentModel model = null;
            // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
			bool dec = false;
            Guid ? idGuidDecripted = null ;
            if (Request != null && Request.QueryString["dec"] == "true")
            {
                dec = true;
                idGuidDecripted = Guid.Parse(id);
            }

            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model,dec,idGuidDecripted);
            Showing(ref me);


            if (!replaceResult)
            {
                 //return View("EditGen", me.Items[0]);
				 return ResolveView(me, me.Items[0]);
            }
            else {
                return e.ActionResult;
            }
        }
			[MyAuthorize("u", "Student","MBKYellowBox", typeof(StudentsController))]
		public ActionResult EditViewGen(string id)
        {
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

					  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<StudentModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
			
            StudentModel model = null;
			 bool dec = false;
            Guid? guidId = null ;

            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null && System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                dec = true;
                guidId = Guid.Parse(id);
            }
            // si fue implementado el método parcial y no se ha decidido suspender la acción
            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
            var me = GetContextModel(UIModelContextTypes.EditForm, model, dec, guidId);
            Showing(ref me);

            return ResolveView(me, model);
        }
		[MyAuthorize("u", "Student",  "MBKYellowBox", typeof(StudentsController))]
		[HttpPost]
		[ValidateInput(false)] 
		        public ActionResult EditGen(StudentModel model)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
			e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<StudentModel>() { Item = model });
           
            if (!ModelState.IsValid)
            {
			   	var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
			
				if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"])){
                	ViewData["ispopup"] = true;
					return ResolveView(me, model);
				}
				else
					return ResolveView(me, model);
            }
            try
            {
			
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnEditing(this, e = new ControllerEventArgs<StudentModel>() { Item = model });
                if (e != null) {
                    if (e.Cancel && e.ActionResult != null)
                        return e.ActionResult;
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				ContextRequest context = new ContextRequest();
                context.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;

                Student resultObj = null;
			    if (!cancel)
                	resultObj = StudentsBR.Instance.Update(model.GetBusinessObject(), GetContextRequest());
				
				OnEdited(this, e = new ControllerEventArgs<StudentModel>() { Item =   new StudentModel(resultObj) });
				if (e != null && e.ActionResult != null) replaceResult = true; 

                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Content("ok");
                }
                else
                {
				if (!replaceResult)
                {
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"])  && string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
						 if (Request != null && Request.QueryString["dec"] == "true")
                        {
                            popupextra.Add("id", model.Id);
                        }
                        else
                        {
							popupextra.Add("id", model.SafeKey);

							
                        }
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                        {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                        popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area"));
                        popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                        popupextra.Add("action", actionDetails);
                        if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
					if (isPopUp)
						return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.UPDATE_DONE));
        			    string returnUrl = null;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"])) {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.Form["ReturnAfter"];
                        else if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"];
                    }
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else{
		RouteValueDictionary popupextra = null; 
						 if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"]))
                            {
							
							popupextra = GetRouteData();
							string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
							if (!string.IsNullOrEmpty(area))
								popupextra.Add("area", area);

							return RedirectToAction("Index", popupextra);
						}else{
							return Content("ok");
						}
						}
				 }
                else {
                    return e.ActionResult;
				}
                }		
            }
          catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
			    if (isPopUp)
                {
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(ex.Message));
                    
                }
                else
                {
                    SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                    
                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
                else {
						  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
							ViewData["ispopup"] = true;
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
							ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
							ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

						var me = GetContextModel(UIModelContextTypes.EditForm, model);
						Showing(ref me);

						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
						{
							ViewData["ispopup"] = true;
							return ResolveView(me, model);
						}
						else
							return ResolveView(me, model);

						
					}
				}
            }
        }
        //
        // POST: /Students/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                //  Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Students/Delete/5
        
		[MyAuthorize("d", "Student", "MBKYellowBox", typeof(StudentsController))]
		[HttpDelete]
        public ActionResult DeleteGen(string objectKey, string extraParams)
        {
            try
            {
					
			
				Guid guidStudent = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey.Replace("-", "/"), "GuidStudent")); 
                BO.Student entity = new BO.Student() { GuidStudent = guidStudent };

                BR.StudentsBR.Instance.Delete(entity, GetContextRequest());               
                return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.DELETE_DONE));

            }
            catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null)
                    {
                        message = ex.Data["usermessage"].ToString();
                    }

                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }
            }
        }
		/*[MyAuthorize()]
		public FileMediaResult Download(string query, bool? allSelected = false,  string selected = null , string orderBy = null , string direction = null , string format = null , string actionKey=null )
        {
			
            List<Guid> keysSelected1 = new List<Guid>();
            if (!string.IsNullOrEmpty(selected)) {
                foreach (var keyString in selected.Split(char.Parse("|")))
                {
				
                    keysSelected1.Add(Guid.Parse(keyString));
                }
            }
				
            query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(query, allSelected.Value, selected, "GuidStudent");
            MyEventArgs<ContextActionModel<StudentModel>> eArgs = null;
            List<StudentModel> results = GetBy(query, null, null, orderBy, direction, GetContextRequest(), keysSelected1);
            OnDownloading(this, eArgs = new MyEventArgs<ContextActionModel<StudentModel>>() { Object = new ContextActionModel<StudentModel>() { Query = query, SelectedItems = results, Selected=selected, SelectAll = allSelected.Value, Direction = direction , OrderBy = orderBy, ActionKey=actionKey  } });

            if (eArgs != null)
            {
                if (eArgs.Object.Result != null)
                    return (FileMediaResult)eArgs.Object.Result;
            }
            

            return (new FeaturesController()).ExportDownload(typeof(StudentModel), results, format, this.GetUIPluralText("MBKYellowBox", "Student"));
            
        }
			*/
		
		[HttpPost]
        public ActionResult CustomActionExecute(ContextActionModel model) {
		 try
            {
			//List<Guid> keysSelected1 = new List<Guid>();
			List<object> keysSelected1 = new List<object>();
            if (!string.IsNullOrEmpty(model.Selected))
            {
                foreach (var keyString in model.Selected.Split(char.Parse(",")))
                {
				
				keysSelected1.Add(Guid.Parse(keyString.Split(char.Parse("|"))[0]));
                        
                    

			
                }
            }
			DataAction dataAction = DataAction.GetDataAction(Request);
			 model.Selected = dataAction.Selected;

            model.Query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(dataAction.Query, dataAction.AllSelected, dataAction.Selected, "GuidStudent");
           
            
			
			#region implementaci�n de m�todo parcial
            bool replaceResult = false;
            MyEventArgs<ContextActionModel<StudentModel>> actionEventArgs = null;
           
			if (model.ActionKey != "deletemany" && model.ActionKey != "deleterelmany" && model.ActionKey != "updateRel" &&  model.ActionKey != "delete-relation-fk" && model.ActionKey != "restore" )
			{
				ContextRequest context = SFSdotNet.Framework.My.Context.BuildContextRequestSafe(System.Web.HttpContext.Current);
				context.UseMode = dataAction.Usemode;

				if (model.IsBackground)
				{
					System.Threading.Tasks.Task.Run(() => 
						OnCustomActionExecutingBackground(this, actionEventArgs = new MyEventArgs<ContextActionModel<StudentModel>>() { Object = new ContextActionModel<StudentModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } })
					);
				}
				else
				{
					OnCustomActionExecuting(this, actionEventArgs = new MyEventArgs<ContextActionModel<StudentModel>>() {  Object = new ContextActionModel<StudentModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } });
				}
			}
            List<StudentModel> results = null;
	
			if (model.ActionKey == "deletemany") { 
				
				BR.StudentsBR.Instance.Delete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

            }
	
			else if (model.ActionKey == "restore") {
                    BR.StudentsBR.Instance.UnDelete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

                }
            else if (model.ActionKey == "updateRel" || model.ActionKey == "delete-relation-fk" || model.ActionKey == "updateRel-proxyMany")
            {
               try {
                   string valueForUpdate = null;
				   string propForUpdate = null;
				   if (!string.IsNullOrEmpty(Request.Params["propertyForUpdate"])){
						propForUpdate = Request.Params["propertyForUpdate"];
				   }
				    if (string.IsNullOrEmpty(propForUpdate) && !string.IsNullOrEmpty(Request.QueryString["propertyForUpdate"]))
                   {
                       propForUpdate = Request.QueryString["propertyForUpdate"];
                   }
                    if (model.ActionKey != "delete-relation-fk")
                    {
                        valueForUpdate = Request.QueryString["valueForUpdate"];
                    }
                    BR.StudentsBR.Instance.UpdateAssociation(propForUpdate, valueForUpdate, model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());
					
                    if (model.ActionKey == "delete-relation-fk")
                    {
                        MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]);

                        return PartialView("ResultMessageView", message);
                    }
                    else
                    {
                        return Content("ok");
                    }
       
          
                }
                catch (Exception ex)
                {
				        SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
           
                }
            
            }
		
                if (actionEventArgs == null && !model.IsBackground)
                {
                    //if (model.ActionKey != "deletemany"  && model.ActionKey != "deleterelmany")
                    //{
                     //   throw new NotImplementedException("");
                    //}
                }
                else
                {
					if (model.IsBackground == false )
						 replaceResult = actionEventArgs.Object.Result is ActionResult /*actionEventArgs.Object.ReplaceResult*/;
                }
                #endregion
                if (!replaceResult)
                {
                    if (Request != null && Request.IsAjaxRequest())
                    {
						MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]) ;
                        if (model.IsBackground )
                            message.Message = GlobalMessages.THE_PROCESS_HAS_BEEN_STARTED;
                        
                        return PartialView("ResultMessageView", message);                    
					}
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return (ActionResult)actionEventArgs.Object.Result;
                }
            }
            catch (Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null) {
                        message = ex.Data["usermessage"].ToString();
                    }
					 SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }

            }
        }
        //
        // POST: /Students/Delete/5
        
			
	
    }
}
namespace MBK.YellowBox.Web.Mvc.Controllers
{
	using MBK.YellowBox.Web.Mvc.Models.StudentLocations;

    public partial class StudentLocationsController : MBK.YellowBox.Web.Mvc.ControllerBase<Models.StudentLocations.StudentLocationModel>
    {

       


	#region partial methods
        ControllerEventArgs<Models.StudentLocations.StudentLocationModel> e = null;
        partial void OnValidating(object sender, ControllerEventArgs<Models.StudentLocations.StudentLocationModel> e);
        partial void OnGettingExtraData(object sender, MyEventArgs<UIModel<Models.StudentLocations.StudentLocationModel>> e);
        partial void OnCreating(object sender, ControllerEventArgs<Models.StudentLocations.StudentLocationModel> e);
        partial void OnCreated(object sender, ControllerEventArgs<Models.StudentLocations.StudentLocationModel> e);
        partial void OnEditing(object sender, ControllerEventArgs<Models.StudentLocations.StudentLocationModel> e);
        partial void OnEdited(object sender, ControllerEventArgs<Models.StudentLocations.StudentLocationModel> e);
        partial void OnDeleting(object sender, ControllerEventArgs<Models.StudentLocations.StudentLocationModel> e);
        partial void OnDeleted(object sender, ControllerEventArgs<Models.StudentLocations.StudentLocationModel> e);
    	partial void OnShowing(object sender, MyEventArgs<UIModel<Models.StudentLocations.StudentLocationModel>> e);
    	partial void OnGettingByKey(object sender, ControllerEventArgs<Models.StudentLocations.StudentLocationModel> e);
        partial void OnTaken(object sender, ControllerEventArgs<Models.StudentLocations.StudentLocationModel> e);
       	partial void OnCreateShowing(object sender, ControllerEventArgs<Models.StudentLocations.StudentLocationModel> e);
		partial void OnEditShowing(object sender, ControllerEventArgs<Models.StudentLocations.StudentLocationModel> e);
		partial void OnDetailsShowing(object sender, ControllerEventArgs<Models.StudentLocations.StudentLocationModel> e);
 		partial void OnActionsCreated(object sender, MyEventArgs<UIModel<Models.StudentLocations.StudentLocationModel >> e);
		partial void OnCustomActionExecuting(object sender, MyEventArgs<ContextActionModel<Models.StudentLocations.StudentLocationModel>> e);
		partial void OnCustomActionExecutingBackground(object sender, MyEventArgs<ContextActionModel<Models.StudentLocations.StudentLocationModel>> e);
        partial void OnDownloading(object sender, MyEventArgs<ContextActionModel<Models.StudentLocations.StudentLocationModel>> e);
      	partial void OnAuthorization(object sender, AuthorizationContext context);
		 partial void OnFilterShowing(object sender, MyEventArgs<UIModel<Models.StudentLocations.StudentLocationModel >> e);
         partial void OnSummaryOperationShowing(object sender, MyEventArgs<UIModel<Models.StudentLocations.StudentLocationModel>> e);

        partial void OnExportActionsCreated(object sender, MyEventArgs<UIModel<Models.StudentLocations.StudentLocationModel>> e);


		protected override void OnVirtualFilterShowing(object sender, MyEventArgs<UIModel<StudentLocationModel>> e)
        {
            OnFilterShowing(sender, e);
        }
		 public override void OnVirtualExportActionsCreated(object sender, MyEventArgs<UIModel<StudentLocationModel>> e)
        {
            OnExportActionsCreated(sender, e);
        }
        public override void OnVirtualDownloading(object sender, MyEventArgs<ContextActionModel<StudentLocationModel>> e)
        {
            OnDownloading(sender, e);
        }
        public override void OnVirtualShowing(object sender, MyEventArgs<UIModel<StudentLocationModel>> e)
        {
            OnShowing(sender, e);
        }

	#endregion
	#region API
	 public override ActionResult ApiCreateGen(StudentLocationModel model, ContextRequest contextRequest)
        {
            return CreateGen(model, contextRequest);
        }

              public override ActionResult ApiGetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJson(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }
        public override ActionResult ApiGetByKeyJson(string id, ContextRequest contextRequest)
        {
            return  GetByKeyJson(id, contextRequest, true);
        }
      
		 public override int ApiGetByCount(string filter, ContextRequest contextRequest)
        {
            return GetByCount(filter, contextRequest);
        }
         protected override ActionResult ApiDeleteGen(List<StudentLocationModel> models, ContextRequest contextRequest)
        {
            List<StudentLocation> objs = new List<StudentLocation>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                BR.StudentLocationsBR.Instance.DeleteBulk(objs, contextRequest);
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        protected override ActionResult ApiUpdateGen(List<StudentLocationModel> models, ContextRequest contextRequest)
        {
            List<StudentLocation> objs = new List<StudentLocation>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                foreach (var obj in objs)
                {
                    BR.StudentLocationsBR.Instance.Update(obj, contextRequest);

                }
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }


	#endregion
#region Validation methods	
	    private void Validations(StudentLocationModel model) { 
            #region Remote validations

            #endregion
		}

#endregion
		
 		public AuthorizationContext Authorization(AuthorizationContext context)
        {
            OnAuthorization(this,  context );
            return context ;
        }
		public List<StudentLocationModel> GetAll() {
            			var bos = BR.StudentLocationsBR.Instance.GetBy("",
					new SFSdotNet.Framework.My.ContextRequest()
					{
						CustomQuery = new SFSdotNet.Framework.My.CustomQuery()
						{
							OrderBy = "Bytes",
							SortDirection = SFSdotNet.Framework.Data.SortDirection.Ascending
						}
					});
            			List<StudentLocationModel> results = new List<StudentLocationModel>();
            StudentLocationModel model = null;
            foreach (var bo in bos)
            {
                model = new StudentLocationModel();
                model.Bind(bo);
                results.Add(model);
            }
            return results;

        }
        //
        // GET: /StudentLocations/
		[MyAuthorize("r", "StudentLocation", "MBKYellowBox", typeof(StudentLocationsController))]
		public ActionResult Index()
        {
    		var uiModel = GetContextModel(UIModelContextTypes.ListForm, null);
			ViewBag.UIModel = uiModel;
			uiModel.FilterStart = (string)ViewData["startFilter"];
                    MyEventArgs<UIModel<StudentLocationModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<StudentLocationModel>>() { Object = uiModel });

			OnExportActionsCreated(this, (me != null ? me : me = new MyEventArgs<UIModel<StudentLocationModel>>() { Object = uiModel }));

            if (me != null)
            {
                uiModel = me.Object;
            }
            if (me == null)
                me = new MyEventArgs<UIModel<StudentLocationModel>>() { Object = uiModel };
           
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;


            //return View("ListGen");
			return ResolveView(uiModel);
        }
		[MyAuthorize("r", "StudentLocation", "MBKYellowBox", typeof(StudentLocationsController))]
		public ActionResult ListViewGen(string idTab, string fk , string fkValue, string startFilter, ListModes  listmode  = ListModes.SimpleList, PropertyDefinition parentRelationProperty = null, object parentRelationPropertyValue = null )
        {
			ViewData["idTab"] = System.Web.HttpContext.Current.Request.QueryString["idTab"]; 
		 	ViewData["detpop"] = true; // details in popup
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"])) {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"]; 
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
            {
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["startFilter"]))
            {
                ViewData["startFilter"] = Request.QueryString["startFilter"];
            }
			
			UIModel<StudentLocationModel> uiModel = GetContextModel(UIModelContextTypes.ListForm, null);

            MyEventArgs<UIModel<StudentLocationModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<StudentLocationModel>>() { Object = uiModel });
            if (me == null)
                me = new MyEventArgs<UIModel<StudentLocationModel>>() { Object = uiModel };
            uiModel.Properties = GetProperties(uiModel);
            uiModel.ContextType = UIModelContextTypes.ListForm;
             uiModel.FilterStart = (string)ViewData["startFilter"];
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;
			 if (listmode == ListModes.SimpleList)
                return ResolveView(uiModel);
            else
            {
                ViewData["parentRelationProperty"] = parentRelationProperty;
                ViewData["parentRelationPropertyValue"] = parentRelationPropertyValue;
                return PartialView("ListForTagSelectView");
            }
            return ResolveView(uiModel);
        }
		List<PropertyDefinition> _properties = null;

		 protected override List<PropertyDefinition> GetProperties(UIModel uiModel,  params string[] specificProperties)
        { 
            return GetProperties(uiModel, false, null, specificProperties);
        }

		protected override List<PropertyDefinition> GetProperties(UIModel uiModel, bool decripted, Guid? id, params string[] specificProperties)
            {

			bool allProperties = true;    
                if (specificProperties != null && specificProperties.Length > 0)
                {
                    allProperties = false;
                }


			List<CustomProperty> customProperties = new List<CustomProperty>();
			if (_properties == null)
                {
                List<PropertyDefinition> results = new List<PropertyDefinition>();

			string idStudentLocation = GetRouteDataOrQueryParam("id");
			if (idStudentLocation != null)
			{
				if (!decripted)
                {
					idStudentLocation = SFSdotNet.Framework.Entities.Utils.GetPropertyKey(idStudentLocation.Replace("-","/"), "GuidStudentLocation");
				}else{
					if (id != null )
						idStudentLocation = id.Value.ToString();                

				}
			}

			bool visibleProperty = true;	
			 bool conditionalshow =false;
                if (uiModel.ContextType == UIModelContextTypes.EditForm || uiModel.ContextType == UIModelContextTypes.DisplayForm ||  uiModel.ContextType == UIModelContextTypes.GenericForm )
                    conditionalshow = true;
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidStudentLocation"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidStudentLocation")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 100,
																	
					CustomProperties = customProperties,

                    PropertyName = "GuidStudentLocation",

					 MaxLength = 0,
					IsRequired = true ,
					IsHidden = true,
                    SystemProperty =  SystemProperties.Identifier ,
					IsDefaultProperty = false,
                    SortBy = "GuidStudentLocation",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentLocationResources.GUIDSTUDENTLOCATION*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidLocation"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidLocation")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 101,
																	IsForeignKey = true,

									
					CustomProperties = customProperties,

                    PropertyName = "GuidLocation",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "GuidLocation",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentLocationResources.GUIDLOCATION*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidStudent"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidStudent")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 102,
																	IsForeignKey = true,

									
					CustomProperties = customProperties,

                    PropertyName = "GuidStudent",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "GuidStudent",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentLocationResources.GUIDSTUDENT*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 103,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedDate",
					
	
				SystemProperty = SystemProperties.CreatedDate ,
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentLocationResources.CREATEDDATE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 116,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedDate",
					
	
					IsUpdatedDate = true,
					SystemProperty = SystemProperties.UpdatedDate ,
	
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    ,PropertyDisplayName = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.UPDATED

                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 105,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedBy",
					
	
				SystemProperty = SystemProperties.CreatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentLocationResources.CREATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 106,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedBy",
					
	
				SystemProperty = SystemProperties.UpdatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentLocationResources.UPDATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Bytes"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Bytes")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 107,
																	
					CustomProperties = customProperties,

                    PropertyName = "Bytes",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = true,
                    SortBy = "Bytes",
					
	
				SystemProperty = SystemProperties.SizeBytes,
                    TypeName = "Int32",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentLocationResources.BYTES*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Student"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"StudentLocations" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="Student.GuidStudent")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"Student.GuidStudent" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"Students" });
			

	
	//fk_StudentLocation_Student
		//if (this.Request.QueryString["fk"] != "Student")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 108,
																
					
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "Student",
					PropertyNavigationKey = "GuidStudent",
					PropertyNavigationText = "FullName",
					NavigationPropertyType = NavigationPropertyTypes.SimpleDropDown,
					GetMethodName = "GetAll",
					GetMethodParameters = "",
					GetMethodDisplayText ="FullName",
					GetMethodDisplayValue = "GuidStudent",
					
					CustomProperties = customProperties,

                    PropertyName = "Student",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Student.FullName",
					
	
                    TypeName = "MBKYellowBoxModel.Student",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/Students"
                    /*,PropertyDisplayName = Resources.StudentLocationResources.STUDENT*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("YBLocation"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"StudentLocations" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="YBLocation.GuidLocation")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"YBLocation.GuidLocation" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"YBLocations" });
			

	
	//fk_StudentLocation_Location
		//if (this.Request.QueryString["fk"] != "YBLocation")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 109,
																
					
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "YBLocation",
					PropertyNavigationKey = "GuidLocation",
					PropertyNavigationText = "Description",
					NavigationPropertyType = NavigationPropertyTypes.SimpleDropDown,
					GetMethodName = "GetAll",
					GetMethodParameters = "",
					GetMethodDisplayText ="Description",
					GetMethodDisplayValue = "GuidLocation",
					
					CustomProperties = customProperties,

                    PropertyName = "YBLocation",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "YBLocation.Description",
					
	
                    TypeName = "MBKYellowBoxModel.YBLocation",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/YBLocations"
                    /*,PropertyDisplayName = Resources.StudentLocationResources.YBLOCATION*/
                });
		//	}
	
	}
	
				
                    _properties = results;
                    return _properties;
                }
                else {
                    return _properties;
                }
            }

		protected override  UIModel<StudentLocationModel> GetByForShow(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, params  object[] extraParams)
        {
			if (Request != null )
				if (!string.IsNullOrEmpty(Request.QueryString["q"]))
					filter = filter + HttpUtility.UrlDecode(Request.QueryString["q"]);
 if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
            }
            var bos = BR.StudentLocationsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), contextRequest, extraParams);
			//var bos = BR.StudentLocationsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), context, extraParams);
            StudentLocationModel model = null;
            List<StudentLocationModel> results = new List<StudentLocationModel>();
            foreach (var item in bos)
            {
                model = new StudentLocationModel();
				model.Bind(item);
				results.Add(model);
            }
            //return results;
			UIModel<StudentLocationModel> uiModel = GetContextModel(UIModelContextTypes.Items, null);
            uiModel.Items = results;
			if (Request != null){
				if (SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(Request.RequestContext, "action") == "Download")
				{
					uiModel.ContextType = UIModelContextTypes.ExportDownload;
				}
			}
            Showing(ref uiModel);
            return uiModel;
		}			
		
		//public List<StudentLocationModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params  object[] extraParams)
        //{
		//	var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, null, extraParams);
        public override List<StudentLocationModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest,  params  object[] extraParams)
        {
            var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
           
            return uiModel.Items;
		
        }
		/*
        [MyAuthorize("r", "StudentLocation", "MBKYellowBox", typeof(StudentLocationsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir)
        {
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir);
        }*/

		  [MyAuthorize("r", "StudentLocation", "MBKYellowBox", typeof(StudentLocationsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir,ContextRequest contextRequest,  object[] extraParams)
        {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir,contextRequest, extraParams);
        }
/*		  [MyAuthorize("r", "StudentLocation", "MBKYellowBox", typeof(StudentLocationsController))]
       public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJsonBase(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }*/
		[MyAuthorize()]
		public int GetByCount(string filter, ContextRequest contextRequest) {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
            return BR.StudentLocationsBR.Instance.GetCount(HttpUtility.UrlDecode(filter), GetUseMode(), contextRequest);
        }
		

		[MyAuthorize("r", "StudentLocation", "MBKYellowBox", typeof(StudentLocationsController))]
        public ActionResult GetByKeyJson(string id, ContextRequest contextRequest,  bool dec = false)
        {
            return Json(GetByKey(id, null, contextRequest, dec), JsonRequestBehavior.AllowGet);
        }
		public StudentLocationModel GetByKey(string id) {
			return GetByKey(id, null,null, false);
       	}
		    public StudentLocationModel GetByKey(string id, string includes)
        {
            return GetByKey(id, includes, false);
        }
		 public  StudentLocationModel GetByKey(string id, string includes, ContextRequest contextRequest)
        {
            return GetByKey(id, includes, contextRequest, false);
        }
		/*
		  public ActionResult ShowField(string fieldName, string idField) {
		   string safePropertyName = fieldName;
              if (fieldName.StartsWith("Fk"))
              {
                  safePropertyName = fieldName.Substring(2, fieldName.Length - 2);
              }

             StudentLocationModel model = new  StudentLocationModel();

            UIModel uiModel = GetUIModel(model, new string[] { "NoField-" });
			
				uiModel.Properties = GetProperties(uiModel, safePropertyName);
		uiModel.Properties.ForEach(p=> p.ContextType = uiModel.ContextType );
            uiModel.ContextType = UIModelContextTypes.FilterFields;
            uiModel.OverrideApp = GetOverrideApp();
            uiModel.UseMode = GetUseMode();

            ViewData["uiModel"] = uiModel;
			var prop = uiModel.Properties.FirstOrDefault(p=>p.PropertyName == safePropertyName);
            //if (prop.IsNavigationProperty && prop.IsNavigationPropertyMany == false)
            //{
            //    ViewData["currentProperty"] = uiModel.Properties.FirstOrDefault(p => p.PropertyName != fieldName + "Text");
            //}else if (prop.IsNavigationProperty == false){
                ViewData["currentProperty"] = prop;
           // }
            ((PropertyDefinition)ViewData["currentProperty"]).RemoveLayout = true;
			ViewData["withContainer"] = false;


            return PartialView("GenericField", model);


        }
      */
	public StudentLocationModel GetByKey(string id, ContextRequest contextRequest, bool dec)
        {
            return GetByKey(id, null, contextRequest, dec);
        }
        public StudentLocationModel GetByKey(string id, string  includes, bool dec)
        {
            return GetByKey(id, includes, null, dec);
        }

        public StudentLocationModel GetByKey(string id, string includes, ContextRequest contextRequest, bool dec) {
		             StudentLocationModel model = null;
            ControllerEventArgs<StudentLocationModel> e = null;
			string objectKey = id.Replace("-","/");
             OnGettingByKey(this, e=  new ControllerEventArgs<StudentLocationModel>() { Id = objectKey  });
             bool cancel = false;
             StudentLocationModel eItem = null;
             if (e != null)
             {
                 cancel = e.Cancel;
                 eItem = e.Item;
             }
			if (cancel == false && eItem == null)
             {
			Guid guidStudentLocation = Guid.Empty; //new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, "GuidStudentLocation"));
			if (dec)
                 {
                     guidStudentLocation = new Guid(id);
                 }
                 else
                 {
                     guidStudentLocation = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, null));
                 }
			
            
				model = new StudentLocationModel();
                  if (contextRequest == null)
                {
                    contextRequest = GetContextRequest();
                }
				var bo = BR.StudentLocationsBR.Instance.GetByKey(guidStudentLocation, GetUseMode(), contextRequest,  includes);
				 if (bo != null)
                    model.Bind(bo);
                else
                    return null;
			}
             else {
                 model = eItem;
             }
			model.IsNew = false;

            return model;
        }
        // GET: /StudentLocations/DetailsGen/5
		[MyAuthorize("r", "StudentLocation", "MBKYellowBox", typeof(StudentLocationsController))]
        public ActionResult DetailsGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = StudentLocationResources.ENTITY_PLURAL;
			 #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<StudentLocationModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
            #endregion



			 bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null) {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
			//UIModel<StudentLocationModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, decripted), decripted, guidId);
			var item = GetByKey(id, null, null, decripted);
			if (item == null)
            {
                 RouteValueDictionary rv = new RouteValueDictionary();
                string usemode = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"usemode");
                string overrideModule = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "overrideModule");
                string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");

                if(!string.IsNullOrEmpty(usemode)){
                    rv.Add("usemode", usemode);
                }
                if(!string.IsNullOrEmpty(overrideModule)){
                    rv.Add("overrideModule", overrideModule);
                }
                if (!string.IsNullOrEmpty(area))
                {
                    rv.Add("area", area);
                }

                return RedirectToAction("Index", rv);
            }
            //
            UIModel<StudentLocationModel> uiModel = null;
                uiModel = GetContextModel(UIModelContextTypes.DisplayForm, item, decripted, guidId);



            MyEventArgs<UIModel<StudentLocationModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<StudentLocationModel>>() { Object = uiModel });

            if (me != null) {
                uiModel = me.Object;
            }
			
            Showing(ref uiModel);
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			
            //return View("DisplayGen", uiModel.Items[0]);
			return ResolveView(uiModel, uiModel.Items[0]);

        }
		[MyAuthorize("r", "StudentLocation", "MBKYellowBox", typeof(StudentLocationsController))]
		public ActionResult DetailsViewGen(string id)
        {

		 bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<StudentLocationModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
           
			if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
 			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
           
        	 //var uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id));
			 
            bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
            {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true")
                {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
            UIModel<StudentLocationModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, null, decripted), decripted, guidId);
			

            MyEventArgs<UIModel<StudentLocationModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<StudentLocationModel>>() { Object = uiModel });

            if (me != null)
            {
                uiModel = me.Object;
            }
            
            Showing(ref uiModel);
            return ResolveView(uiModel, uiModel.Items[0]);
        
        }
        //
        // GET: /StudentLocations/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        //
        // GET: /StudentLocations/CreateGen
		[MyAuthorize("c", "StudentLocation", "MBKYellowBox", typeof(StudentLocationsController))]
        public ActionResult CreateGen()
        {
			StudentLocationModel model = new StudentLocationModel();
            model.IsNew = true;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model);

			OnCreateShowing(this, e = new ControllerEventArgs<StudentLocationModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }

             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                 ViewData["ispopup"] = true;
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                 ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                 ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

            Showing(ref me);

			return ResolveView(me, me.Items[0]);
        } 
			
		protected override UIModel<StudentLocationModel> GetContextModel(UIModelContextTypes formMode, StudentLocationModel model)
        {
            return GetContextModel(formMode, model, false, null);
        }
			
		 private UIModel<StudentLocationModel> GetContextModel(UIModelContextTypes formMode, StudentLocationModel model, bool decript, Guid ? id) {
            UIModel<StudentLocationModel> me = new UIModel<StudentLocationModel>(true, "StudentLocations");
			me.UseMode = GetUseMode();
			me.Controller = this;
			me.OverrideApp = GetOverrideApp();
			me.ContextType = formMode ;
			me.Id = "StudentLocation";
			
            me.ModuleKey = "MBKYellowBox";

			me.ModuleNamespace = "MBK.YellowBox";
            me.EntityKey = "StudentLocation";
            me.EntitySetName = "StudentLocations";

			me.AreaAction = "MBKYellowBox";
            me.ControllerAction = "StudentLocations";
            me.PropertyKeyName = "GuidStudentLocation";

            me.Properties = GetProperties(me, decript, id);

			me.SortBy = "UpdatedDate";
			me.SortDirection = UIModelSortDirection.DESC;

 			
			if (Request != null)
            {
                string actionName = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam( Request.RequestContext, "action");
                if(actionName != null && actionName.ToLower().Contains("create") ){
                    me.IsNew = true;
                }
            }
			 #region Buttons
			 if (Request != null ){
             if (formMode == UIModelContextTypes.DisplayForm || formMode == UIModelContextTypes.EditForm || formMode == UIModelContextTypes.ListForm)
				me.ActionButtons = GetActionButtons(formMode,model != null ?(Request.QueryString["dec"] == "true" ? model.Id : model.SafeKey)  : null, "MBKYellowBox", "StudentLocations", "StudentLocation", me.IsNew);

            //me.ActionButtons.Add(new ActionModel() { ActionKey = "return", Title = GlobalMessages.RETURN, Url = System.Web.VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/StudentLocations" });
			if (this.HttpContext != null &&  !this.HttpContext.SkipAuthorization){
				//antes this.HttpContext
				me.SetAction("u", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("u", "StudentLocation", "MBKYellowBox"));
				me.SetAction("c", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("c", "StudentLocation", "MBKYellowBox"));
				me.SetAction("d", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("d", "StudentLocation", "MBKYellowBox"));
			
			}else{
				me.SetAction("u", true);
				me.SetAction("c", true);
				me.SetAction("d", true);

			}
            #endregion              
         
            switch (formMode)
            {
                case UIModelContextTypes.DisplayForm:
					//me.TitleForm = StudentLocationResources.STUDENTLOCATIONS_DETAILS;
                    me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.MODIFY_DATA;
					 me.Properties.Where(p=>p.PropertyName  != "Id" && p.IsForeignKey == false).ToList().ForEach(p => p.IsHidden = false);

					 me.Properties.Where(p => (p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier) ).ToList().ForEach(p=> me.SetHide(p.PropertyName));

                    break;
                case UIModelContextTypes.EditForm:
				  me.Properties.Where(p=>p.SystemProperty != SystemProperties.Identifier && p.IsForeignKey == false && p.PropertyName != "Id").ToList().ForEach(p => p.IsHidden = false);

					if (model != null)
                    {
						

                        me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.SAVE_DATA;                        
                        me.ActionButtons.First(p => p.ActionKey == "c").Title = GlobalMessages.SAVE_DATA;
						if (model.IsNew ){
							//me.TitleForm = StudentLocationResources.STUDENTLOCATIONS_ADD_NEW;
							me.ActionName = "CreateGen";
							me.Properties.RemoveAll(p => p.SystemProperty != null || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));
						}else{
							
							me.ActionName = "EditGen";

							//me.TitleForm = StudentLocationResources.STUDENTLOCATIONS_EDIT;
							me.Properties.RemoveAll(p => p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));	
						}
						//me.Properties.Remove(me.Properties.Find(p => p.PropertyName == "UpdatedDate"));
					
					}
                    break;
                case UIModelContextTypes.FilterFields:
                    break;
                case UIModelContextTypes.GenericForm:
                    break;
                case UIModelContextTypes.Items:
				//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					 
                    
					
					 if (me.Properties.Find(p => p.PropertyName == "Bytes") != null){
						me.Properties.Find(p => p.PropertyName == "Bytes").IsHidden = false;
					 }
					 
                    
					

						 if (me.Properties.Find(p => p.PropertyName == "GuidStudentLocation") != null){
						me.Properties.Find(p => p.PropertyName == "GuidStudentLocation").IsHidden = false;
					 }
					 
                    
					


                  


					//}
                    break;
                case UIModelContextTypes.ListForm:
					PropertyDefinition propFinded = null;
					//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					
					 if (me.Properties.Find(p => p.PropertyName == "Bytes") != null){
						me.Properties.Find(p => p.PropertyName == "Bytes").IsHidden = false;
					 }
					
					me.PrincipalActionName = "GetByJson";
					//}
					//me.TitleForm = StudentLocationResources.STUDENTLOCATIONS_LIST;
                    break;
                default:
                    break;
            }
            	this.SetDefaultProperties(me);
			}
			if (model != null )
            	me.Items.Add(model);
            return me;
        }
		// GET: /StudentLocations/CreateViewGen
		[MyAuthorize("c", "StudentLocation", "MBKYellowBox", typeof(StudentLocationsController))]
        public ActionResult CreateViewGen()
        {
				StudentLocationModel model = new StudentLocationModel();
            model.IsNew = true;
			e= null;
			OnCreateShowing(this, e = new ControllerEventArgs<StudentLocationModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }
			
            var me = GetContextModel(UIModelContextTypes.EditForm, model);

			me.IsPartialView = true;	
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
            {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                me.Properties.Find(p => p.PropertyName == ViewData["fk"].ToString()).IsReadOnly = true;
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
			
      
            //me.Items.Add(model);
            Showing(ref me);
            return ResolveView(me, me.Items[0]);
        }
		protected override  void GettingExtraData(ref UIModel<StudentLocationModel> uiModel)
        {

            MyEventArgs<UIModel<StudentLocationModel>> me = null;
            OnGettingExtraData(this, me = new MyEventArgs<UIModel<StudentLocationModel>>() { Object = uiModel });
            //bool maybeAnyReplaced = false; 
            if (me != null)
            {
                uiModel = me.Object;
                //maybeAnyReplaced = true;
            }
           
			bool canFill = false;
			 string query = null ;
            bool isFK = false;
			PropertyDefinition prop =null;
			var contextRequest = this.GetContextRequest();
            contextRequest.CustomParams.Add(new CustomParam() { Name="ui", Value= StudentLocation.EntityName });

            			canFill = false;
			 query = "";
            isFK =false;
			prop = uiModel.Properties.FirstOrDefault(p => p.PropertyName == "Student");
			if (prop != null)
				if (prop.IsHidden == false && (prop.ContextType == UIModelContextTypes.EditForm || prop.ContextType == UIModelContextTypes.FilterFields  || (prop.ContextType == null && uiModel.ContextType == UIModelContextTypes.EditForm)))
				{
					if (prop.NavigationPropertyType == NavigationPropertyTypes.SimpleDropDown )
						canFill = true;
				}
                else if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidStudent = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidStudent = @GuidStudent";
					
					canFill = true;
                }
				if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidStudent = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidStudent = @GuidStudent";
					canFill = true;
                }
			if (canFill){
			                contextRequest.CustomQuery = new CustomQuery();

				if (!uiModel.ExtraData.Exists(p => p.PropertyName == "Student")) {
					if (!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))				  
						contextRequest.CustomQuery.SetParam("GuidStudent", new Nullable<Guid>(Guid.Parse( Request.QueryString["fkValue"])));

					 if (isFK == true)
                    {
						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.StudentsBR()).GetBy(query, contextRequest), "GuidStudent", "FullName", Request.QueryString["fkValue"]), PropertyName = "Student" });    
                    }
                    else
                    {

						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.StudentsBR()).GetBy(query, contextRequest), "GuidStudent", "FullName"), PropertyName = "Student" });    

					}
    if (isFK)
                    {    
						var FkStudent = ((SelectList)uiModel.ExtraData.First(p => p.PropertyName == "Student").Data).First();
						uiModel.Items[0].GetType().GetProperty("FkStudentText").SetValue(uiModel.Items[0], FkStudent.Text);
						uiModel.Items[0].GetType().GetProperty("FkStudent").SetValue(uiModel.Items[0], Guid.Parse(FkStudent.Value));
                    
					}    
				}
			}
		 
			canFill = false;
			 query = "";
            isFK =false;
			prop = uiModel.Properties.FirstOrDefault(p => p.PropertyName == "YBLocation");
			if (prop != null)
				if (prop.IsHidden == false && (prop.ContextType == UIModelContextTypes.EditForm || prop.ContextType == UIModelContextTypes.FilterFields  || (prop.ContextType == null && uiModel.ContextType == UIModelContextTypes.EditForm)))
				{
					if (prop.NavigationPropertyType == NavigationPropertyTypes.SimpleDropDown )
						canFill = true;
				}
                else if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidLocation = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidLocation = @GuidLocation";
					
					canFill = true;
                }
				if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidLocation = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidLocation = @GuidLocation";
					canFill = true;
                }
			if (canFill){
			                contextRequest.CustomQuery = new CustomQuery();

				if (!uiModel.ExtraData.Exists(p => p.PropertyName == "YBLocation")) {
					if (!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))				  
						contextRequest.CustomQuery.SetParam("GuidLocation", new Nullable<Guid>(Guid.Parse( Request.QueryString["fkValue"])));

					 if (isFK == true)
                    {
						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.YBLocationsBR()).GetBy(query, contextRequest), "GuidLocation", "Description", Request.QueryString["fkValue"]), PropertyName = "YBLocation" });    
                    }
                    else
                    {

						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.YBLocationsBR()).GetBy(query, contextRequest), "GuidLocation", "Description"), PropertyName = "YBLocation" });    

					}
    if (isFK)
                    {    
						var FkYBLocation = ((SelectList)uiModel.ExtraData.First(p => p.PropertyName == "YBLocation").Data).First();
						uiModel.Items[0].GetType().GetProperty("FkYBLocationText").SetValue(uiModel.Items[0], FkYBLocation.Text);
						uiModel.Items[0].GetType().GetProperty("FkYBLocation").SetValue(uiModel.Items[0], Guid.Parse(FkYBLocation.Value));
                    
					}    
				}
			}
		 
            

        }
		private void Showing(ref UIModel<StudentLocationModel> uiModel) {
          	
			MyEventArgs<UIModel<StudentLocationModel>> me = new MyEventArgs<UIModel<StudentLocationModel>>() { Object = uiModel };
			 OnVirtualLayoutSettings(this, me);


            OnShowing(this, me);

			
			if ((Request != null && Request.QueryString["allFields"] == "1") || Request == null )
			{
				me.Object.Properties.ForEach(p=> p.IsHidden = false);
            }
            if (me != null)
            {
                uiModel = me.Object;
            }
          


			 if (uiModel.ContextType == UIModelContextTypes.EditForm)
			    GettingExtraData(ref uiModel);
            ViewData["UIModel"] = uiModel;

        }
        //
        // POST: /StudentLocations/Create
		[MyAuthorize("c", "StudentLocation", "MBKYellowBox", typeof(StudentLocationsController))]
        [HttpPost]
		[ValidateInput(false)] 
        public ActionResult CreateGen(StudentLocationModel  model,  ContextRequest contextRequest)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
		 	e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<StudentLocationModel>() { Item = model });
           
		  	if (!ModelState.IsValid) {
				model.IsNew = true;
				var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
                 if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                        ViewData["ispopup"] = true;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
                    return ResolveView(me, model);
            }
            try
            {
				if (model.GuidStudentLocation == null || model.GuidStudentLocation.ToString().Contains("000000000"))
				model.GuidStudentLocation = Guid.NewGuid();
	
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnCreating(this, e = new ControllerEventArgs<StudentLocationModel>() { Item = model });
                if (e != null) {
                   if (e.Cancel && e.RedirectValues.Count > 0){
                        RouteValueDictionary rv = new RouteValueDictionary();
                        if (e.RedirectValues["area"] != null ){
                            rv.Add("area", e.RedirectValues["area"].ToString());
                        }
                        foreach (var item in e.RedirectValues.Where(p=>p.Key != "area" && p.Key != "controller" &&  p.Key != "action" ))
	                    {
		                    rv.Add(item.Key, item.Value);
	                    }

                        //if (e.RedirectValues["action"] != null && e.RedirectValues["controller"] != null && e.RedirectValues["area"] != null )
                        return RedirectToAction(e.RedirectValues["action"].ToString(), e.RedirectValues["controller"].ToString(), rv );


                        
                    }else if (e.Cancel && e.ActionResult != null )
                        return e.ActionResult;  
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				if (contextRequest == null || contextRequest.Company == null){
					contextRequest = GetContextRequest();
					
				}
                if (!cancel)
                	model.Bind(StudentLocationsBR.Instance.Create(model.GetBusinessObject(), contextRequest ));
				OnCreated(this, e = new ControllerEventArgs<StudentLocationModel>() { Item = model });
                 if (e != null )
					if (e.ActionResult != null)
                    	replaceResult = true;		
				if (!replaceResult)
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))
                    {
                        ViewData["__continue"] = true;
                    }
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"]) &&  string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
                        popupextra.Add("id", model.SafeKey);
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                       {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                            popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"area"));
                            popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                            popupextra.Add("action", actionDetails);
                       if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
                    {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))

                        {
                            var popupextra = GetRouteData();
                            popupextra.Add("id", model.SafeKey);
                            return RedirectToAction("EditViewGen", popupextra);
                        }
                        else
                        {
                            return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.ADD_DONE));
                        }
                    }        			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                        return Redirect(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]);
                    else{

							RouteValueDictionary popupextra = null; 
							if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"])){
                            popupextra = GetRouteData();
							 string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
                            if (!string.IsNullOrEmpty(area))
                                popupextra.Add("area", area);
                            
                            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"])) {
								popupextra.Add("id", model.SafeKey);
                                return RedirectToAction("EditGen", popupextra);

                            }else{
								
                            return RedirectToAction("Index", popupextra);
							}
							}else{
								return Content("ok");
							}
                        }
						 }
                else {
                    return e.ActionResult;
                    }
				}
            catch(Exception ex)
            {
					if (!string.IsNullOrEmpty(Request.QueryString["rok"]))
                {
                    throw  ex;
                }
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                model.IsNew = true;
                var me = GetContextModel(UIModelContextTypes.EditForm, model, true, model.GuidStudentLocation);
                Showing(ref me);
                if (isPopUp)
                {
                    
                        ViewData["ispopup"] = isPopUp;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
					if (Request != null)
						return ResolveView(me, model);
					else
						return Content("ok");
            }
        }        
        //
        // GET: /StudentLocations/Edit/5 
        public ActionResult Edit(int id)
        {
            return View();
        }
			
		
		[MyAuthorize("u", "StudentLocation", "MBKYellowBox", typeof(StudentLocationsController))]
		[MvcSiteMapNode(Area="MBKYellowBox", Title="sss", Clickable=false, ParentKey = "MBKYellowBox_StudentLocation_List")]
		public ActionResult EditGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = StudentLocationResources.ENTITY_SINGLE;		 	
  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<StudentLocationModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
            StudentLocationModel model = null;
            // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
			bool dec = false;
            Guid ? idGuidDecripted = null ;
            if (Request != null && Request.QueryString["dec"] == "true")
            {
                dec = true;
                idGuidDecripted = Guid.Parse(id);
            }

            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model,dec,idGuidDecripted);
            Showing(ref me);


            if (!replaceResult)
            {
                 //return View("EditGen", me.Items[0]);
				 return ResolveView(me, me.Items[0]);
            }
            else {
                return e.ActionResult;
            }
        }
			[MyAuthorize("u", "StudentLocation","MBKYellowBox", typeof(StudentLocationsController))]
		public ActionResult EditViewGen(string id)
        {
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

					  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<StudentLocationModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
			
            StudentLocationModel model = null;
			 bool dec = false;
            Guid? guidId = null ;

            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null && System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                dec = true;
                guidId = Guid.Parse(id);
            }
            // si fue implementado el método parcial y no se ha decidido suspender la acción
            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
            var me = GetContextModel(UIModelContextTypes.EditForm, model, dec, guidId);
            Showing(ref me);

            return ResolveView(me, model);
        }
		[MyAuthorize("u", "StudentLocation",  "MBKYellowBox", typeof(StudentLocationsController))]
		[HttpPost]
		[ValidateInput(false)] 
		        public ActionResult EditGen(StudentLocationModel model)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
			e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<StudentLocationModel>() { Item = model });
           
            if (!ModelState.IsValid)
            {
			   	var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
			
				if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"])){
                	ViewData["ispopup"] = true;
					return ResolveView(me, model);
				}
				else
					return ResolveView(me, model);
            }
            try
            {
			
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnEditing(this, e = new ControllerEventArgs<StudentLocationModel>() { Item = model });
                if (e != null) {
                    if (e.Cancel && e.ActionResult != null)
                        return e.ActionResult;
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				ContextRequest context = new ContextRequest();
                context.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;

                StudentLocation resultObj = null;
			    if (!cancel)
                	resultObj = StudentLocationsBR.Instance.Update(model.GetBusinessObject(), GetContextRequest());
				
				OnEdited(this, e = new ControllerEventArgs<StudentLocationModel>() { Item =   new StudentLocationModel(resultObj) });
				if (e != null && e.ActionResult != null) replaceResult = true; 

                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Content("ok");
                }
                else
                {
				if (!replaceResult)
                {
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"])  && string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
						 if (Request != null && Request.QueryString["dec"] == "true")
                        {
                            popupextra.Add("id", model.Id);
                        }
                        else
                        {
							popupextra.Add("id", model.SafeKey);

							
                        }
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                        {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                        popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area"));
                        popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                        popupextra.Add("action", actionDetails);
                        if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
					if (isPopUp)
						return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.UPDATE_DONE));
        			    string returnUrl = null;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"])) {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.Form["ReturnAfter"];
                        else if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"];
                    }
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else{
		RouteValueDictionary popupextra = null; 
						 if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"]))
                            {
							
							popupextra = GetRouteData();
							string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
							if (!string.IsNullOrEmpty(area))
								popupextra.Add("area", area);

							return RedirectToAction("Index", popupextra);
						}else{
							return Content("ok");
						}
						}
				 }
                else {
                    return e.ActionResult;
				}
                }		
            }
          catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
			    if (isPopUp)
                {
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(ex.Message));
                    
                }
                else
                {
                    SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                    
                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
                else {
						  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
							ViewData["ispopup"] = true;
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
							ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
							ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

						var me = GetContextModel(UIModelContextTypes.EditForm, model);
						Showing(ref me);

						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
						{
							ViewData["ispopup"] = true;
							return ResolveView(me, model);
						}
						else
							return ResolveView(me, model);

						
					}
				}
            }
        }
        //
        // POST: /StudentLocations/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                //  Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /StudentLocations/Delete/5
        
		[MyAuthorize("d", "StudentLocation", "MBKYellowBox", typeof(StudentLocationsController))]
		[HttpDelete]
        public ActionResult DeleteGen(string objectKey, string extraParams)
        {
            try
            {
					
			
				Guid guidStudentLocation = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey.Replace("-", "/"), "GuidStudentLocation")); 
                BO.StudentLocation entity = new BO.StudentLocation() { GuidStudentLocation = guidStudentLocation };

                BR.StudentLocationsBR.Instance.Delete(entity, GetContextRequest());               
                return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.DELETE_DONE));

            }
            catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null)
                    {
                        message = ex.Data["usermessage"].ToString();
                    }

                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }
            }
        }
		/*[MyAuthorize()]
		public FileMediaResult Download(string query, bool? allSelected = false,  string selected = null , string orderBy = null , string direction = null , string format = null , string actionKey=null )
        {
			
            List<Guid> keysSelected1 = new List<Guid>();
            if (!string.IsNullOrEmpty(selected)) {
                foreach (var keyString in selected.Split(char.Parse("|")))
                {
				
                    keysSelected1.Add(Guid.Parse(keyString));
                }
            }
				
            query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(query, allSelected.Value, selected, "GuidStudentLocation");
            MyEventArgs<ContextActionModel<StudentLocationModel>> eArgs = null;
            List<StudentLocationModel> results = GetBy(query, null, null, orderBy, direction, GetContextRequest(), keysSelected1);
            OnDownloading(this, eArgs = new MyEventArgs<ContextActionModel<StudentLocationModel>>() { Object = new ContextActionModel<StudentLocationModel>() { Query = query, SelectedItems = results, Selected=selected, SelectAll = allSelected.Value, Direction = direction , OrderBy = orderBy, ActionKey=actionKey  } });

            if (eArgs != null)
            {
                if (eArgs.Object.Result != null)
                    return (FileMediaResult)eArgs.Object.Result;
            }
            

            return (new FeaturesController()).ExportDownload(typeof(StudentLocationModel), results, format, this.GetUIPluralText("MBKYellowBox", "StudentLocation"));
            
        }
			*/
		
		[HttpPost]
        public ActionResult CustomActionExecute(ContextActionModel model) {
		 try
            {
			//List<Guid> keysSelected1 = new List<Guid>();
			List<object> keysSelected1 = new List<object>();
            if (!string.IsNullOrEmpty(model.Selected))
            {
                foreach (var keyString in model.Selected.Split(char.Parse(",")))
                {
				
				keysSelected1.Add(Guid.Parse(keyString.Split(char.Parse("|"))[0]));
                        
                    

			
                }
            }
			DataAction dataAction = DataAction.GetDataAction(Request);
			 model.Selected = dataAction.Selected;

            model.Query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(dataAction.Query, dataAction.AllSelected, dataAction.Selected, "GuidStudentLocation");
           
            
			
			#region implementaci�n de m�todo parcial
            bool replaceResult = false;
            MyEventArgs<ContextActionModel<StudentLocationModel>> actionEventArgs = null;
           
			if (model.ActionKey != "deletemany" && model.ActionKey != "deleterelmany" && model.ActionKey != "updateRel" &&  model.ActionKey != "delete-relation-fk" && model.ActionKey != "restore" )
			{
				ContextRequest context = SFSdotNet.Framework.My.Context.BuildContextRequestSafe(System.Web.HttpContext.Current);
				context.UseMode = dataAction.Usemode;

				if (model.IsBackground)
				{
					System.Threading.Tasks.Task.Run(() => 
						OnCustomActionExecutingBackground(this, actionEventArgs = new MyEventArgs<ContextActionModel<StudentLocationModel>>() { Object = new ContextActionModel<StudentLocationModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } })
					);
				}
				else
				{
					OnCustomActionExecuting(this, actionEventArgs = new MyEventArgs<ContextActionModel<StudentLocationModel>>() {  Object = new ContextActionModel<StudentLocationModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } });
				}
			}
            List<StudentLocationModel> results = null;
	
			if (model.ActionKey == "deletemany") { 
				
				BR.StudentLocationsBR.Instance.Delete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

            }
	
			else if (model.ActionKey == "restore") {
                    BR.StudentLocationsBR.Instance.UnDelete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

                }
            else if (model.ActionKey == "updateRel" || model.ActionKey == "delete-relation-fk" || model.ActionKey == "updateRel-proxyMany")
            {
               try {
                   string valueForUpdate = null;
				   string propForUpdate = null;
				   if (!string.IsNullOrEmpty(Request.Params["propertyForUpdate"])){
						propForUpdate = Request.Params["propertyForUpdate"];
				   }
				    if (string.IsNullOrEmpty(propForUpdate) && !string.IsNullOrEmpty(Request.QueryString["propertyForUpdate"]))
                   {
                       propForUpdate = Request.QueryString["propertyForUpdate"];
                   }
                    if (model.ActionKey != "delete-relation-fk")
                    {
                        valueForUpdate = Request.QueryString["valueForUpdate"];
                    }
                    BR.StudentLocationsBR.Instance.UpdateAssociation(propForUpdate, valueForUpdate, model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());
					
                    if (model.ActionKey == "delete-relation-fk")
                    {
                        MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]);

                        return PartialView("ResultMessageView", message);
                    }
                    else
                    {
                        return Content("ok");
                    }
       
          
                }
                catch (Exception ex)
                {
				        SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
           
                }
            
            }
		
                if (actionEventArgs == null && !model.IsBackground)
                {
                    //if (model.ActionKey != "deletemany"  && model.ActionKey != "deleterelmany")
                    //{
                     //   throw new NotImplementedException("");
                    //}
                }
                else
                {
					if (model.IsBackground == false )
						 replaceResult = actionEventArgs.Object.Result is ActionResult /*actionEventArgs.Object.ReplaceResult*/;
                }
                #endregion
                if (!replaceResult)
                {
                    if (Request != null && Request.IsAjaxRequest())
                    {
						MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]) ;
                        if (model.IsBackground )
                            message.Message = GlobalMessages.THE_PROCESS_HAS_BEEN_STARTED;
                        
                        return PartialView("ResultMessageView", message);                    
					}
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return (ActionResult)actionEventArgs.Object.Result;
                }
            }
            catch (Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null) {
                        message = ex.Data["usermessage"].ToString();
                    }
					 SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }

            }
        }
        //
        // POST: /StudentLocations/Delete/5
        
			
	
    }
}
namespace MBK.YellowBox.Web.Mvc.Controllers
{
	using MBK.YellowBox.Web.Mvc.Models.Transports;

    public partial class TransportsController : MBK.YellowBox.Web.Mvc.ControllerBase<Models.Transports.TransportModel>
    {

       


	#region partial methods
        ControllerEventArgs<Models.Transports.TransportModel> e = null;
        partial void OnValidating(object sender, ControllerEventArgs<Models.Transports.TransportModel> e);
        partial void OnGettingExtraData(object sender, MyEventArgs<UIModel<Models.Transports.TransportModel>> e);
        partial void OnCreating(object sender, ControllerEventArgs<Models.Transports.TransportModel> e);
        partial void OnCreated(object sender, ControllerEventArgs<Models.Transports.TransportModel> e);
        partial void OnEditing(object sender, ControllerEventArgs<Models.Transports.TransportModel> e);
        partial void OnEdited(object sender, ControllerEventArgs<Models.Transports.TransportModel> e);
        partial void OnDeleting(object sender, ControllerEventArgs<Models.Transports.TransportModel> e);
        partial void OnDeleted(object sender, ControllerEventArgs<Models.Transports.TransportModel> e);
    	partial void OnShowing(object sender, MyEventArgs<UIModel<Models.Transports.TransportModel>> e);
    	partial void OnGettingByKey(object sender, ControllerEventArgs<Models.Transports.TransportModel> e);
        partial void OnTaken(object sender, ControllerEventArgs<Models.Transports.TransportModel> e);
       	partial void OnCreateShowing(object sender, ControllerEventArgs<Models.Transports.TransportModel> e);
		partial void OnEditShowing(object sender, ControllerEventArgs<Models.Transports.TransportModel> e);
		partial void OnDetailsShowing(object sender, ControllerEventArgs<Models.Transports.TransportModel> e);
 		partial void OnActionsCreated(object sender, MyEventArgs<UIModel<Models.Transports.TransportModel >> e);
		partial void OnCustomActionExecuting(object sender, MyEventArgs<ContextActionModel<Models.Transports.TransportModel>> e);
		partial void OnCustomActionExecutingBackground(object sender, MyEventArgs<ContextActionModel<Models.Transports.TransportModel>> e);
        partial void OnDownloading(object sender, MyEventArgs<ContextActionModel<Models.Transports.TransportModel>> e);
      	partial void OnAuthorization(object sender, AuthorizationContext context);
		 partial void OnFilterShowing(object sender, MyEventArgs<UIModel<Models.Transports.TransportModel >> e);
         partial void OnSummaryOperationShowing(object sender, MyEventArgs<UIModel<Models.Transports.TransportModel>> e);

        partial void OnExportActionsCreated(object sender, MyEventArgs<UIModel<Models.Transports.TransportModel>> e);


		protected override void OnVirtualFilterShowing(object sender, MyEventArgs<UIModel<TransportModel>> e)
        {
            OnFilterShowing(sender, e);
        }
		 public override void OnVirtualExportActionsCreated(object sender, MyEventArgs<UIModel<TransportModel>> e)
        {
            OnExportActionsCreated(sender, e);
        }
        public override void OnVirtualDownloading(object sender, MyEventArgs<ContextActionModel<TransportModel>> e)
        {
            OnDownloading(sender, e);
        }
        public override void OnVirtualShowing(object sender, MyEventArgs<UIModel<TransportModel>> e)
        {
            OnShowing(sender, e);
        }

	#endregion
	#region API
	 public override ActionResult ApiCreateGen(TransportModel model, ContextRequest contextRequest)
        {
            return CreateGen(model, contextRequest);
        }

              public override ActionResult ApiGetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJson(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }
        public override ActionResult ApiGetByKeyJson(string id, ContextRequest contextRequest)
        {
            return  GetByKeyJson(id, contextRequest, true);
        }
      
		 public override int ApiGetByCount(string filter, ContextRequest contextRequest)
        {
            return GetByCount(filter, contextRequest);
        }
         protected override ActionResult ApiDeleteGen(List<TransportModel> models, ContextRequest contextRequest)
        {
            List<Transport> objs = new List<Transport>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                BR.TransportsBR.Instance.DeleteBulk(objs, contextRequest);
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        protected override ActionResult ApiUpdateGen(List<TransportModel> models, ContextRequest contextRequest)
        {
            List<Transport> objs = new List<Transport>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                foreach (var obj in objs)
                {
                    BR.TransportsBR.Instance.Update(obj, contextRequest);

                }
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }


	#endregion
#region Validation methods	
	    private void Validations(TransportModel model) { 
            #region Remote validations

            #endregion
		}

#endregion
		
 		public AuthorizationContext Authorization(AuthorizationContext context)
        {
            OnAuthorization(this,  context );
            return context ;
        }
		public List<TransportModel> GetAll() {
            			var bos = BR.TransportsBR.Instance.GetBy("",
					new SFSdotNet.Framework.My.ContextRequest()
					{
						CustomQuery = new SFSdotNet.Framework.My.CustomQuery()
						{
							OrderBy = "TransportCode",
							SortDirection = SFSdotNet.Framework.Data.SortDirection.Ascending
						}
					});
            			List<TransportModel> results = new List<TransportModel>();
            TransportModel model = null;
            foreach (var bo in bos)
            {
                model = new TransportModel();
                model.Bind(bo);
                results.Add(model);
            }
            return results;

        }
        //
        // GET: /Transports/
		[MyAuthorize("r", "Transport", "MBKYellowBox", typeof(TransportsController))]
		public ActionResult Index()
        {
    		var uiModel = GetContextModel(UIModelContextTypes.ListForm, null);
			ViewBag.UIModel = uiModel;
			uiModel.FilterStart = (string)ViewData["startFilter"];
                    MyEventArgs<UIModel<TransportModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<TransportModel>>() { Object = uiModel });

			OnExportActionsCreated(this, (me != null ? me : me = new MyEventArgs<UIModel<TransportModel>>() { Object = uiModel }));

            if (me != null)
            {
                uiModel = me.Object;
            }
            if (me == null)
                me = new MyEventArgs<UIModel<TransportModel>>() { Object = uiModel };
           
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;


            //return View("ListGen");
			return ResolveView(uiModel);
        }
		[MyAuthorize("r", "Transport", "MBKYellowBox", typeof(TransportsController))]
		public ActionResult ListViewGen(string idTab, string fk , string fkValue, string startFilter, ListModes  listmode  = ListModes.SimpleList, PropertyDefinition parentRelationProperty = null, object parentRelationPropertyValue = null )
        {
			ViewData["idTab"] = System.Web.HttpContext.Current.Request.QueryString["idTab"]; 
		 	ViewData["detpop"] = true; // details in popup
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"])) {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"]; 
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
            {
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["startFilter"]))
            {
                ViewData["startFilter"] = Request.QueryString["startFilter"];
            }
			
			UIModel<TransportModel> uiModel = GetContextModel(UIModelContextTypes.ListForm, null);

            MyEventArgs<UIModel<TransportModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<TransportModel>>() { Object = uiModel });
            if (me == null)
                me = new MyEventArgs<UIModel<TransportModel>>() { Object = uiModel };
            uiModel.Properties = GetProperties(uiModel);
            uiModel.ContextType = UIModelContextTypes.ListForm;
             uiModel.FilterStart = (string)ViewData["startFilter"];
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;
			 if (listmode == ListModes.SimpleList)
                return ResolveView(uiModel);
            else
            {
                ViewData["parentRelationProperty"] = parentRelationProperty;
                ViewData["parentRelationPropertyValue"] = parentRelationPropertyValue;
                return PartialView("ListForTagSelectView");
            }
            return ResolveView(uiModel);
        }
		List<PropertyDefinition> _properties = null;

		 protected override List<PropertyDefinition> GetProperties(UIModel uiModel,  params string[] specificProperties)
        { 
            return GetProperties(uiModel, false, null, specificProperties);
        }

		protected override List<PropertyDefinition> GetProperties(UIModel uiModel, bool decripted, Guid? id, params string[] specificProperties)
            {

			bool allProperties = true;    
                if (specificProperties != null && specificProperties.Length > 0)
                {
                    allProperties = false;
                }


			List<CustomProperty> customProperties = new List<CustomProperty>();
			if (_properties == null)
                {
                List<PropertyDefinition> results = new List<PropertyDefinition>();

			string idTransport = GetRouteDataOrQueryParam("id");
			if (idTransport != null)
			{
				if (!decripted)
                {
					idTransport = SFSdotNet.Framework.Entities.Utils.GetPropertyKey(idTransport.Replace("-","/"), "GuidTrasport");
				}else{
					if (id != null )
						idTransport = id.Value.ToString();                

				}
			}

			bool visibleProperty = true;	
			 bool conditionalshow =false;
                if (uiModel.ContextType == UIModelContextTypes.EditForm || uiModel.ContextType == UIModelContextTypes.DisplayForm ||  uiModel.ContextType == UIModelContextTypes.GenericForm )
                    conditionalshow = true;
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidTrasport"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidTrasport")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 100,
																	
					CustomProperties = customProperties,

                    PropertyName = "GuidTrasport",

					 MaxLength = 0,
					IsRequired = true ,
					IsHidden = true,
                    SystemProperty =  SystemProperties.Identifier ,
					IsDefaultProperty = false,
                    SortBy = "GuidTrasport",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.TransportResources.GUIDTRASPORT*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("TransportCode"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "TransportCode")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 101,
																	
					CustomProperties = customProperties,

                    PropertyName = "TransportCode",

					 MaxLength = 10,
					 Nullable = true,
					IsDefaultProperty = true,
                    SortBy = "TransportCode",
					
	
                    TypeName = "String",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.TransportResources.TRANSPORTCODE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Description"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Description")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 102,
																	
					CustomProperties = customProperties,

                    PropertyName = "Description",

					 MaxLength = 255,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Description",
					
	
                    TypeName = "String",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.TransportResources.DESCRIPTION*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Capacity"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Capacity")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 103,
																	
					CustomProperties = customProperties,

                    PropertyName = "Capacity",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Capacity",
					
	
                    TypeName = "Int32",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.TransportResources.CAPACITY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidRoute"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidRoute")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 104,
																	IsForeignKey = true,

									
					CustomProperties = customProperties,

                    PropertyName = "GuidRoute",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "GuidRoute",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.TransportResources.GUIDROUTE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 105,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedDate",
					
	
				SystemProperty = SystemProperties.CreatedDate ,
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.TransportResources.CREATEDDATE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 119,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedDate",
					
	
					IsUpdatedDate = true,
					SystemProperty = SystemProperties.UpdatedDate ,
	
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    ,PropertyDisplayName = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.UPDATED

                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 107,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedBy",
					
	
				SystemProperty = SystemProperties.CreatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.TransportResources.CREATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 108,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedBy",
					
	
				SystemProperty = SystemProperties.UpdatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.TransportResources.UPDATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Bytes"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Bytes")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 109,
																	
					CustomProperties = customProperties,

                    PropertyName = "Bytes",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Bytes",
					
	
				SystemProperty = SystemProperties.SizeBytes,
                    TypeName = "Int32",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.TransportResources.BYTES*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("YBRoute"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"Transports" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="YBRoute.GuidRoute")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"YBRoute.GuidRoute" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"YBRoutes" });
			

	
	//fk_Transport_Route
		//if (this.Request.QueryString["fk"] != "YBRoute")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 110,
																
					
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "YBRoute",
					PropertyNavigationKey = "GuidRoute",
					PropertyNavigationText = "Title",
					NavigationPropertyType = NavigationPropertyTypes.SimpleDropDown,
					GetMethodName = "GetAll",
					GetMethodParameters = "",
					GetMethodDisplayText ="Title",
					GetMethodDisplayValue = "GuidRoute",
					
					CustomProperties = customProperties,

                    PropertyName = "YBRoute",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "YBRoute.Title",
					
	
                    TypeName = "MBKYellowBoxModel.YBRoute",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/YBRoutes"
                    /*,PropertyDisplayName = Resources.TransportResources.YBROUTE*/
                });
		//	}
	
	}
	
				
                    _properties = results;
                    return _properties;
                }
                else {
                    return _properties;
                }
            }

		protected override  UIModel<TransportModel> GetByForShow(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, params  object[] extraParams)
        {
			if (Request != null )
				if (!string.IsNullOrEmpty(Request.QueryString["q"]))
					filter = filter + HttpUtility.UrlDecode(Request.QueryString["q"]);
 if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
            }
            var bos = BR.TransportsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), contextRequest, extraParams);
			//var bos = BR.TransportsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), context, extraParams);
            TransportModel model = null;
            List<TransportModel> results = new List<TransportModel>();
            foreach (var item in bos)
            {
                model = new TransportModel();
				model.Bind(item);
				results.Add(model);
            }
            //return results;
			UIModel<TransportModel> uiModel = GetContextModel(UIModelContextTypes.Items, null);
            uiModel.Items = results;
			if (Request != null){
				if (SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(Request.RequestContext, "action") == "Download")
				{
					uiModel.ContextType = UIModelContextTypes.ExportDownload;
				}
			}
            Showing(ref uiModel);
            return uiModel;
		}			
		
		//public List<TransportModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params  object[] extraParams)
        //{
		//	var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, null, extraParams);
        public override List<TransportModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest,  params  object[] extraParams)
        {
            var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
           
            return uiModel.Items;
		
        }
		/*
        [MyAuthorize("r", "Transport", "MBKYellowBox", typeof(TransportsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir)
        {
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir);
        }*/

		  [MyAuthorize("r", "Transport", "MBKYellowBox", typeof(TransportsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir,ContextRequest contextRequest,  object[] extraParams)
        {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir,contextRequest, extraParams);
        }
/*		  [MyAuthorize("r", "Transport", "MBKYellowBox", typeof(TransportsController))]
       public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJsonBase(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }*/
		[MyAuthorize()]
		public int GetByCount(string filter, ContextRequest contextRequest) {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
            return BR.TransportsBR.Instance.GetCount(HttpUtility.UrlDecode(filter), GetUseMode(), contextRequest);
        }
		

		[MyAuthorize("r", "Transport", "MBKYellowBox", typeof(TransportsController))]
        public ActionResult GetByKeyJson(string id, ContextRequest contextRequest,  bool dec = false)
        {
            return Json(GetByKey(id, null, contextRequest, dec), JsonRequestBehavior.AllowGet);
        }
		public TransportModel GetByKey(string id) {
			return GetByKey(id, null,null, false);
       	}
		    public TransportModel GetByKey(string id, string includes)
        {
            return GetByKey(id, includes, false);
        }
		 public  TransportModel GetByKey(string id, string includes, ContextRequest contextRequest)
        {
            return GetByKey(id, includes, contextRequest, false);
        }
		/*
		  public ActionResult ShowField(string fieldName, string idField) {
		   string safePropertyName = fieldName;
              if (fieldName.StartsWith("Fk"))
              {
                  safePropertyName = fieldName.Substring(2, fieldName.Length - 2);
              }

             TransportModel model = new  TransportModel();

            UIModel uiModel = GetUIModel(model, new string[] { "NoField-" });
			
				uiModel.Properties = GetProperties(uiModel, safePropertyName);
		uiModel.Properties.ForEach(p=> p.ContextType = uiModel.ContextType );
            uiModel.ContextType = UIModelContextTypes.FilterFields;
            uiModel.OverrideApp = GetOverrideApp();
            uiModel.UseMode = GetUseMode();

            ViewData["uiModel"] = uiModel;
			var prop = uiModel.Properties.FirstOrDefault(p=>p.PropertyName == safePropertyName);
            //if (prop.IsNavigationProperty && prop.IsNavigationPropertyMany == false)
            //{
            //    ViewData["currentProperty"] = uiModel.Properties.FirstOrDefault(p => p.PropertyName != fieldName + "Text");
            //}else if (prop.IsNavigationProperty == false){
                ViewData["currentProperty"] = prop;
           // }
            ((PropertyDefinition)ViewData["currentProperty"]).RemoveLayout = true;
			ViewData["withContainer"] = false;


            return PartialView("GenericField", model);


        }
      */
	public TransportModel GetByKey(string id, ContextRequest contextRequest, bool dec)
        {
            return GetByKey(id, null, contextRequest, dec);
        }
        public TransportModel GetByKey(string id, string  includes, bool dec)
        {
            return GetByKey(id, includes, null, dec);
        }

        public TransportModel GetByKey(string id, string includes, ContextRequest contextRequest, bool dec) {
		             TransportModel model = null;
            ControllerEventArgs<TransportModel> e = null;
			string objectKey = id.Replace("-","/");
             OnGettingByKey(this, e=  new ControllerEventArgs<TransportModel>() { Id = objectKey  });
             bool cancel = false;
             TransportModel eItem = null;
             if (e != null)
             {
                 cancel = e.Cancel;
                 eItem = e.Item;
             }
			if (cancel == false && eItem == null)
             {
			Guid guidTrasport = Guid.Empty; //new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, "GuidTrasport"));
			if (dec)
                 {
                     guidTrasport = new Guid(id);
                 }
                 else
                 {
                     guidTrasport = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, null));
                 }
			
            
				model = new TransportModel();
                  if (contextRequest == null)
                {
                    contextRequest = GetContextRequest();
                }
				var bo = BR.TransportsBR.Instance.GetByKey(guidTrasport, GetUseMode(), contextRequest,  includes);
				 if (bo != null)
                    model.Bind(bo);
                else
                    return null;
			}
             else {
                 model = eItem;
             }
			model.IsNew = false;

            return model;
        }
        // GET: /Transports/DetailsGen/5
		[MyAuthorize("r", "Transport", "MBKYellowBox", typeof(TransportsController))]
        public ActionResult DetailsGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = TransportResources.ENTITY_PLURAL;
			 #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<TransportModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
            #endregion



			 bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null) {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
			//UIModel<TransportModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, decripted), decripted, guidId);
			var item = GetByKey(id, null, null, decripted);
			if (item == null)
            {
                 RouteValueDictionary rv = new RouteValueDictionary();
                string usemode = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"usemode");
                string overrideModule = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "overrideModule");
                string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");

                if(!string.IsNullOrEmpty(usemode)){
                    rv.Add("usemode", usemode);
                }
                if(!string.IsNullOrEmpty(overrideModule)){
                    rv.Add("overrideModule", overrideModule);
                }
                if (!string.IsNullOrEmpty(area))
                {
                    rv.Add("area", area);
                }

                return RedirectToAction("Index", rv);
            }
            //
            UIModel<TransportModel> uiModel = null;
                uiModel = GetContextModel(UIModelContextTypes.DisplayForm, item, decripted, guidId);



            MyEventArgs<UIModel<TransportModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<TransportModel>>() { Object = uiModel });

            if (me != null) {
                uiModel = me.Object;
            }
			
            Showing(ref uiModel);
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			
            //return View("DisplayGen", uiModel.Items[0]);
			return ResolveView(uiModel, uiModel.Items[0]);

        }
		[MyAuthorize("r", "Transport", "MBKYellowBox", typeof(TransportsController))]
		public ActionResult DetailsViewGen(string id)
        {

		 bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<TransportModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
           
			if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
 			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
           
        	 //var uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id));
			 
            bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
            {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true")
                {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
            UIModel<TransportModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, null, decripted), decripted, guidId);
			

            MyEventArgs<UIModel<TransportModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<TransportModel>>() { Object = uiModel });

            if (me != null)
            {
                uiModel = me.Object;
            }
            
            Showing(ref uiModel);
            return ResolveView(uiModel, uiModel.Items[0]);
        
        }
        //
        // GET: /Transports/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        //
        // GET: /Transports/CreateGen
		[MyAuthorize("c", "Transport", "MBKYellowBox", typeof(TransportsController))]
        public ActionResult CreateGen()
        {
			TransportModel model = new TransportModel();
            model.IsNew = true;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model);

			OnCreateShowing(this, e = new ControllerEventArgs<TransportModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }

             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                 ViewData["ispopup"] = true;
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                 ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                 ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

            Showing(ref me);

			return ResolveView(me, me.Items[0]);
        } 
			
		protected override UIModel<TransportModel> GetContextModel(UIModelContextTypes formMode, TransportModel model)
        {
            return GetContextModel(formMode, model, false, null);
        }
			
		 private UIModel<TransportModel> GetContextModel(UIModelContextTypes formMode, TransportModel model, bool decript, Guid ? id) {
            UIModel<TransportModel> me = new UIModel<TransportModel>(true, "Transports");
			me.UseMode = GetUseMode();
			me.Controller = this;
			me.OverrideApp = GetOverrideApp();
			me.ContextType = formMode ;
			me.Id = "Transport";
			
            me.ModuleKey = "MBKYellowBox";

			me.ModuleNamespace = "MBK.YellowBox";
            me.EntityKey = "Transport";
            me.EntitySetName = "Transports";

			me.AreaAction = "MBKYellowBox";
            me.ControllerAction = "Transports";
            me.PropertyKeyName = "GuidTrasport";

            me.Properties = GetProperties(me, decript, id);

			me.SortBy = "UpdatedDate";
			me.SortDirection = UIModelSortDirection.DESC;

 			
			if (Request != null)
            {
                string actionName = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam( Request.RequestContext, "action");
                if(actionName != null && actionName.ToLower().Contains("create") ){
                    me.IsNew = true;
                }
            }
			 #region Buttons
			 if (Request != null ){
             if (formMode == UIModelContextTypes.DisplayForm || formMode == UIModelContextTypes.EditForm || formMode == UIModelContextTypes.ListForm)
				me.ActionButtons = GetActionButtons(formMode,model != null ?(Request.QueryString["dec"] == "true" ? model.Id : model.SafeKey)  : null, "MBKYellowBox", "Transports", "Transport", me.IsNew);

            //me.ActionButtons.Add(new ActionModel() { ActionKey = "return", Title = GlobalMessages.RETURN, Url = System.Web.VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/Transports" });
			if (this.HttpContext != null &&  !this.HttpContext.SkipAuthorization){
				//antes this.HttpContext
				me.SetAction("u", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("u", "Transport", "MBKYellowBox"));
				me.SetAction("c", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("c", "Transport", "MBKYellowBox"));
				me.SetAction("d", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("d", "Transport", "MBKYellowBox"));
			
			}else{
				me.SetAction("u", true);
				me.SetAction("c", true);
				me.SetAction("d", true);

			}
            #endregion              
         
            switch (formMode)
            {
                case UIModelContextTypes.DisplayForm:
					//me.TitleForm = TransportResources.TRANSPORTS_DETAILS;
                    me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.MODIFY_DATA;
					 me.Properties.Where(p=>p.PropertyName  != "Id" && p.IsForeignKey == false).ToList().ForEach(p => p.IsHidden = false);

					 me.Properties.Where(p => (p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier) ).ToList().ForEach(p=> me.SetHide(p.PropertyName));

                    break;
                case UIModelContextTypes.EditForm:
				  me.Properties.Where(p=>p.SystemProperty != SystemProperties.Identifier && p.IsForeignKey == false && p.PropertyName != "Id").ToList().ForEach(p => p.IsHidden = false);

					if (model != null)
                    {
						

                        me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.SAVE_DATA;                        
                        me.ActionButtons.First(p => p.ActionKey == "c").Title = GlobalMessages.SAVE_DATA;
						if (model.IsNew ){
							//me.TitleForm = TransportResources.TRANSPORTS_ADD_NEW;
							me.ActionName = "CreateGen";
							me.Properties.RemoveAll(p => p.SystemProperty != null || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));
						}else{
							
							me.ActionName = "EditGen";

							//me.TitleForm = TransportResources.TRANSPORTS_EDIT;
							me.Properties.RemoveAll(p => p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));	
						}
						//me.Properties.Remove(me.Properties.Find(p => p.PropertyName == "UpdatedDate"));
					
					}
                    break;
                case UIModelContextTypes.FilterFields:
                    break;
                case UIModelContextTypes.GenericForm:
                    break;
                case UIModelContextTypes.Items:
				//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "TransportCode") != null){
						me.Properties.Find(p => p.PropertyName == "TransportCode").IsHidden = false;
					 }
					 
                    
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					 
                    
					

						 if (me.Properties.Find(p => p.PropertyName == "GuidTrasport") != null){
						me.Properties.Find(p => p.PropertyName == "GuidTrasport").IsHidden = false;
					 }
					 
                    
					


                  


					//}
                    break;
                case UIModelContextTypes.ListForm:
					PropertyDefinition propFinded = null;
					//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "TransportCode") != null){
						me.Properties.Find(p => p.PropertyName == "TransportCode").IsHidden = false;
					 }
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					
					me.PrincipalActionName = "GetByJson";
					//}
					//me.TitleForm = TransportResources.TRANSPORTS_LIST;
                    break;
                default:
                    break;
            }
            	this.SetDefaultProperties(me);
			}
			if (model != null )
            	me.Items.Add(model);
            return me;
        }
		// GET: /Transports/CreateViewGen
		[MyAuthorize("c", "Transport", "MBKYellowBox", typeof(TransportsController))]
        public ActionResult CreateViewGen()
        {
				TransportModel model = new TransportModel();
            model.IsNew = true;
			e= null;
			OnCreateShowing(this, e = new ControllerEventArgs<TransportModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }
			
            var me = GetContextModel(UIModelContextTypes.EditForm, model);

			me.IsPartialView = true;	
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
            {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                me.Properties.Find(p => p.PropertyName == ViewData["fk"].ToString()).IsReadOnly = true;
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
			
      
            //me.Items.Add(model);
            Showing(ref me);
            return ResolveView(me, me.Items[0]);
        }
		protected override  void GettingExtraData(ref UIModel<TransportModel> uiModel)
        {

            MyEventArgs<UIModel<TransportModel>> me = null;
            OnGettingExtraData(this, me = new MyEventArgs<UIModel<TransportModel>>() { Object = uiModel });
            //bool maybeAnyReplaced = false; 
            if (me != null)
            {
                uiModel = me.Object;
                //maybeAnyReplaced = true;
            }
           
			bool canFill = false;
			 string query = null ;
            bool isFK = false;
			PropertyDefinition prop =null;
			var contextRequest = this.GetContextRequest();
            contextRequest.CustomParams.Add(new CustomParam() { Name="ui", Value= Transport.EntityName });

            			canFill = false;
			 query = "";
            isFK =false;
			prop = uiModel.Properties.FirstOrDefault(p => p.PropertyName == "YBRoute");
			if (prop != null)
				if (prop.IsHidden == false && (prop.ContextType == UIModelContextTypes.EditForm || prop.ContextType == UIModelContextTypes.FilterFields  || (prop.ContextType == null && uiModel.ContextType == UIModelContextTypes.EditForm)))
				{
					if (prop.NavigationPropertyType == NavigationPropertyTypes.SimpleDropDown )
						canFill = true;
				}
                else if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidRoute = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidRoute = @GuidRoute";
					
					canFill = true;
                }
				if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidRoute = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidRoute = @GuidRoute";
					canFill = true;
                }
			if (canFill){
			                contextRequest.CustomQuery = new CustomQuery();

				if (!uiModel.ExtraData.Exists(p => p.PropertyName == "YBRoute")) {
					if (!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))				  
						contextRequest.CustomQuery.SetParam("GuidRoute", new Nullable<Guid>(Guid.Parse( Request.QueryString["fkValue"])));

					 if (isFK == true)
                    {
						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.YBRoutesBR()).GetBy(query, contextRequest), "GuidRoute", "Title", Request.QueryString["fkValue"]), PropertyName = "YBRoute" });    
                    }
                    else
                    {

						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.YBRoutesBR()).GetBy(query, contextRequest), "GuidRoute", "Title"), PropertyName = "YBRoute" });    

					}
    if (isFK)
                    {    
						var FkYBRoute = ((SelectList)uiModel.ExtraData.First(p => p.PropertyName == "YBRoute").Data).First();
						uiModel.Items[0].GetType().GetProperty("FkYBRouteText").SetValue(uiModel.Items[0], FkYBRoute.Text);
						uiModel.Items[0].GetType().GetProperty("FkYBRoute").SetValue(uiModel.Items[0], Guid.Parse(FkYBRoute.Value));
                    
					}    
				}
			}
		 
            

        }
		private void Showing(ref UIModel<TransportModel> uiModel) {
          	
			MyEventArgs<UIModel<TransportModel>> me = new MyEventArgs<UIModel<TransportModel>>() { Object = uiModel };
			 OnVirtualLayoutSettings(this, me);


            OnShowing(this, me);

			
			if ((Request != null && Request.QueryString["allFields"] == "1") || Request == null )
			{
				me.Object.Properties.ForEach(p=> p.IsHidden = false);
            }
            if (me != null)
            {
                uiModel = me.Object;
            }
          


			 if (uiModel.ContextType == UIModelContextTypes.EditForm)
			    GettingExtraData(ref uiModel);
            ViewData["UIModel"] = uiModel;

        }
        //
        // POST: /Transports/Create
		[MyAuthorize("c", "Transport", "MBKYellowBox", typeof(TransportsController))]
        [HttpPost]
		[ValidateInput(false)] 
        public ActionResult CreateGen(TransportModel  model,  ContextRequest contextRequest)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
		 	e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<TransportModel>() { Item = model });
           
		  	if (!ModelState.IsValid) {
				model.IsNew = true;
				var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
                 if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                        ViewData["ispopup"] = true;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
                    return ResolveView(me, model);
            }
            try
            {
				if (model.GuidTrasport == null || model.GuidTrasport.ToString().Contains("000000000"))
				model.GuidTrasport = Guid.NewGuid();
	
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnCreating(this, e = new ControllerEventArgs<TransportModel>() { Item = model });
                if (e != null) {
                   if (e.Cancel && e.RedirectValues.Count > 0){
                        RouteValueDictionary rv = new RouteValueDictionary();
                        if (e.RedirectValues["area"] != null ){
                            rv.Add("area", e.RedirectValues["area"].ToString());
                        }
                        foreach (var item in e.RedirectValues.Where(p=>p.Key != "area" && p.Key != "controller" &&  p.Key != "action" ))
	                    {
		                    rv.Add(item.Key, item.Value);
	                    }

                        //if (e.RedirectValues["action"] != null && e.RedirectValues["controller"] != null && e.RedirectValues["area"] != null )
                        return RedirectToAction(e.RedirectValues["action"].ToString(), e.RedirectValues["controller"].ToString(), rv );


                        
                    }else if (e.Cancel && e.ActionResult != null )
                        return e.ActionResult;  
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				if (contextRequest == null || contextRequest.Company == null){
					contextRequest = GetContextRequest();
					
				}
                if (!cancel)
                	model.Bind(TransportsBR.Instance.Create(model.GetBusinessObject(), contextRequest ));
				OnCreated(this, e = new ControllerEventArgs<TransportModel>() { Item = model });
                 if (e != null )
					if (e.ActionResult != null)
                    	replaceResult = true;		
				if (!replaceResult)
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))
                    {
                        ViewData["__continue"] = true;
                    }
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"]) &&  string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
                        popupextra.Add("id", model.SafeKey);
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                       {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                            popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"area"));
                            popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                            popupextra.Add("action", actionDetails);
                       if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
                    {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))

                        {
                            var popupextra = GetRouteData();
                            popupextra.Add("id", model.SafeKey);
                            return RedirectToAction("EditViewGen", popupextra);
                        }
                        else
                        {
                            return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.ADD_DONE));
                        }
                    }        			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                        return Redirect(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]);
                    else{

							RouteValueDictionary popupextra = null; 
							if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"])){
                            popupextra = GetRouteData();
							 string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
                            if (!string.IsNullOrEmpty(area))
                                popupextra.Add("area", area);
                            
                            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"])) {
								popupextra.Add("id", model.SafeKey);
                                return RedirectToAction("EditGen", popupextra);

                            }else{
								
                            return RedirectToAction("Index", popupextra);
							}
							}else{
								return Content("ok");
							}
                        }
						 }
                else {
                    return e.ActionResult;
                    }
				}
            catch(Exception ex)
            {
					if (!string.IsNullOrEmpty(Request.QueryString["rok"]))
                {
                    throw  ex;
                }
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                model.IsNew = true;
                var me = GetContextModel(UIModelContextTypes.EditForm, model, true, model.GuidTrasport);
                Showing(ref me);
                if (isPopUp)
                {
                    
                        ViewData["ispopup"] = isPopUp;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
					if (Request != null)
						return ResolveView(me, model);
					else
						return Content("ok");
            }
        }        
        //
        // GET: /Transports/Edit/5 
        public ActionResult Edit(int id)
        {
            return View();
        }
			
		
		[MyAuthorize("u", "Transport", "MBKYellowBox", typeof(TransportsController))]
		[MvcSiteMapNode(Area="MBKYellowBox", Title="sss", Clickable=false, ParentKey = "MBKYellowBox_Transport_List")]
		public ActionResult EditGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = TransportResources.ENTITY_SINGLE;		 	
  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<TransportModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
            TransportModel model = null;
            // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
			bool dec = false;
            Guid ? idGuidDecripted = null ;
            if (Request != null && Request.QueryString["dec"] == "true")
            {
                dec = true;
                idGuidDecripted = Guid.Parse(id);
            }

            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model,dec,idGuidDecripted);
            Showing(ref me);


            if (!replaceResult)
            {
                 //return View("EditGen", me.Items[0]);
				 return ResolveView(me, me.Items[0]);
            }
            else {
                return e.ActionResult;
            }
        }
			[MyAuthorize("u", "Transport","MBKYellowBox", typeof(TransportsController))]
		public ActionResult EditViewGen(string id)
        {
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

					  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<TransportModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
			
            TransportModel model = null;
			 bool dec = false;
            Guid? guidId = null ;

            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null && System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                dec = true;
                guidId = Guid.Parse(id);
            }
            // si fue implementado el método parcial y no se ha decidido suspender la acción
            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
            var me = GetContextModel(UIModelContextTypes.EditForm, model, dec, guidId);
            Showing(ref me);

            return ResolveView(me, model);
        }
		[MyAuthorize("u", "Transport",  "MBKYellowBox", typeof(TransportsController))]
		[HttpPost]
		[ValidateInput(false)] 
		        public ActionResult EditGen(TransportModel model)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
			e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<TransportModel>() { Item = model });
           
            if (!ModelState.IsValid)
            {
			   	var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
			
				if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"])){
                	ViewData["ispopup"] = true;
					return ResolveView(me, model);
				}
				else
					return ResolveView(me, model);
            }
            try
            {
			
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnEditing(this, e = new ControllerEventArgs<TransportModel>() { Item = model });
                if (e != null) {
                    if (e.Cancel && e.ActionResult != null)
                        return e.ActionResult;
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				ContextRequest context = new ContextRequest();
                context.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;

                Transport resultObj = null;
			    if (!cancel)
                	resultObj = TransportsBR.Instance.Update(model.GetBusinessObject(), GetContextRequest());
				
				OnEdited(this, e = new ControllerEventArgs<TransportModel>() { Item =   new TransportModel(resultObj) });
				if (e != null && e.ActionResult != null) replaceResult = true; 

                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Content("ok");
                }
                else
                {
				if (!replaceResult)
                {
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"])  && string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
						 if (Request != null && Request.QueryString["dec"] == "true")
                        {
                            popupextra.Add("id", model.Id);
                        }
                        else
                        {
							popupextra.Add("id", model.SafeKey);

							
                        }
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                        {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                        popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area"));
                        popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                        popupextra.Add("action", actionDetails);
                        if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
					if (isPopUp)
						return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.UPDATE_DONE));
        			    string returnUrl = null;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"])) {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.Form["ReturnAfter"];
                        else if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"];
                    }
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else{
		RouteValueDictionary popupextra = null; 
						 if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"]))
                            {
							
							popupextra = GetRouteData();
							string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
							if (!string.IsNullOrEmpty(area))
								popupextra.Add("area", area);

							return RedirectToAction("Index", popupextra);
						}else{
							return Content("ok");
						}
						}
				 }
                else {
                    return e.ActionResult;
				}
                }		
            }
          catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
			    if (isPopUp)
                {
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(ex.Message));
                    
                }
                else
                {
                    SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                    
                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
                else {
						  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
							ViewData["ispopup"] = true;
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
							ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
							ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

						var me = GetContextModel(UIModelContextTypes.EditForm, model);
						Showing(ref me);

						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
						{
							ViewData["ispopup"] = true;
							return ResolveView(me, model);
						}
						else
							return ResolveView(me, model);

						
					}
				}
            }
        }
        //
        // POST: /Transports/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                //  Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Transports/Delete/5
        
		[MyAuthorize("d", "Transport", "MBKYellowBox", typeof(TransportsController))]
		[HttpDelete]
        public ActionResult DeleteGen(string objectKey, string extraParams)
        {
            try
            {
					
			
				Guid guidTrasport = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey.Replace("-", "/"), "GuidTrasport")); 
                BO.Transport entity = new BO.Transport() { GuidTrasport = guidTrasport };

                BR.TransportsBR.Instance.Delete(entity, GetContextRequest());               
                return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.DELETE_DONE));

            }
            catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null)
                    {
                        message = ex.Data["usermessage"].ToString();
                    }

                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }
            }
        }
		/*[MyAuthorize()]
		public FileMediaResult Download(string query, bool? allSelected = false,  string selected = null , string orderBy = null , string direction = null , string format = null , string actionKey=null )
        {
			
            List<Guid> keysSelected1 = new List<Guid>();
            if (!string.IsNullOrEmpty(selected)) {
                foreach (var keyString in selected.Split(char.Parse("|")))
                {
				
                    keysSelected1.Add(Guid.Parse(keyString));
                }
            }
				
            query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(query, allSelected.Value, selected, "GuidTrasport");
            MyEventArgs<ContextActionModel<TransportModel>> eArgs = null;
            List<TransportModel> results = GetBy(query, null, null, orderBy, direction, GetContextRequest(), keysSelected1);
            OnDownloading(this, eArgs = new MyEventArgs<ContextActionModel<TransportModel>>() { Object = new ContextActionModel<TransportModel>() { Query = query, SelectedItems = results, Selected=selected, SelectAll = allSelected.Value, Direction = direction , OrderBy = orderBy, ActionKey=actionKey  } });

            if (eArgs != null)
            {
                if (eArgs.Object.Result != null)
                    return (FileMediaResult)eArgs.Object.Result;
            }
            

            return (new FeaturesController()).ExportDownload(typeof(TransportModel), results, format, this.GetUIPluralText("MBKYellowBox", "Transport"));
            
        }
			*/
		
		[HttpPost]
        public ActionResult CustomActionExecute(ContextActionModel model) {
		 try
            {
			//List<Guid> keysSelected1 = new List<Guid>();
			List<object> keysSelected1 = new List<object>();
            if (!string.IsNullOrEmpty(model.Selected))
            {
                foreach (var keyString in model.Selected.Split(char.Parse(",")))
                {
				
				keysSelected1.Add(Guid.Parse(keyString.Split(char.Parse("|"))[0]));
                        
                    

			
                }
            }
			DataAction dataAction = DataAction.GetDataAction(Request);
			 model.Selected = dataAction.Selected;

            model.Query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(dataAction.Query, dataAction.AllSelected, dataAction.Selected, "GuidTrasport");
           
            
			
			#region implementaci�n de m�todo parcial
            bool replaceResult = false;
            MyEventArgs<ContextActionModel<TransportModel>> actionEventArgs = null;
           
			if (model.ActionKey != "deletemany" && model.ActionKey != "deleterelmany" && model.ActionKey != "updateRel" &&  model.ActionKey != "delete-relation-fk" && model.ActionKey != "restore" )
			{
				ContextRequest context = SFSdotNet.Framework.My.Context.BuildContextRequestSafe(System.Web.HttpContext.Current);
				context.UseMode = dataAction.Usemode;

				if (model.IsBackground)
				{
					System.Threading.Tasks.Task.Run(() => 
						OnCustomActionExecutingBackground(this, actionEventArgs = new MyEventArgs<ContextActionModel<TransportModel>>() { Object = new ContextActionModel<TransportModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } })
					);
				}
				else
				{
					OnCustomActionExecuting(this, actionEventArgs = new MyEventArgs<ContextActionModel<TransportModel>>() {  Object = new ContextActionModel<TransportModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } });
				}
			}
            List<TransportModel> results = null;
	
			if (model.ActionKey == "deletemany") { 
				
				BR.TransportsBR.Instance.Delete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

            }
	
			else if (model.ActionKey == "restore") {
                    BR.TransportsBR.Instance.UnDelete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

                }
            else if (model.ActionKey == "updateRel" || model.ActionKey == "delete-relation-fk" || model.ActionKey == "updateRel-proxyMany")
            {
               try {
                   string valueForUpdate = null;
				   string propForUpdate = null;
				   if (!string.IsNullOrEmpty(Request.Params["propertyForUpdate"])){
						propForUpdate = Request.Params["propertyForUpdate"];
				   }
				    if (string.IsNullOrEmpty(propForUpdate) && !string.IsNullOrEmpty(Request.QueryString["propertyForUpdate"]))
                   {
                       propForUpdate = Request.QueryString["propertyForUpdate"];
                   }
                    if (model.ActionKey != "delete-relation-fk")
                    {
                        valueForUpdate = Request.QueryString["valueForUpdate"];
                    }
                    BR.TransportsBR.Instance.UpdateAssociation(propForUpdate, valueForUpdate, model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());
					
                    if (model.ActionKey == "delete-relation-fk")
                    {
                        MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]);

                        return PartialView("ResultMessageView", message);
                    }
                    else
                    {
                        return Content("ok");
                    }
       
          
                }
                catch (Exception ex)
                {
				        SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
           
                }
            
            }
		
                if (actionEventArgs == null && !model.IsBackground)
                {
                    //if (model.ActionKey != "deletemany"  && model.ActionKey != "deleterelmany")
                    //{
                     //   throw new NotImplementedException("");
                    //}
                }
                else
                {
					if (model.IsBackground == false )
						 replaceResult = actionEventArgs.Object.Result is ActionResult /*actionEventArgs.Object.ReplaceResult*/;
                }
                #endregion
                if (!replaceResult)
                {
                    if (Request != null && Request.IsAjaxRequest())
                    {
						MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]) ;
                        if (model.IsBackground )
                            message.Message = GlobalMessages.THE_PROCESS_HAS_BEEN_STARTED;
                        
                        return PartialView("ResultMessageView", message);                    
					}
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return (ActionResult)actionEventArgs.Object.Result;
                }
            }
            catch (Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null) {
                        message = ex.Data["usermessage"].ToString();
                    }
					 SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }

            }
        }
        //
        // POST: /Transports/Delete/5
        
			
	
    }
}
namespace MBK.YellowBox.Web.Mvc.Controllers
{
	using MBK.YellowBox.Web.Mvc.Models.YBLocations;

    public partial class YBLocationsController : MBK.YellowBox.Web.Mvc.ControllerBase<Models.YBLocations.YBLocationModel>
    {

       


	#region partial methods
        ControllerEventArgs<Models.YBLocations.YBLocationModel> e = null;
        partial void OnValidating(object sender, ControllerEventArgs<Models.YBLocations.YBLocationModel> e);
        partial void OnGettingExtraData(object sender, MyEventArgs<UIModel<Models.YBLocations.YBLocationModel>> e);
        partial void OnCreating(object sender, ControllerEventArgs<Models.YBLocations.YBLocationModel> e);
        partial void OnCreated(object sender, ControllerEventArgs<Models.YBLocations.YBLocationModel> e);
        partial void OnEditing(object sender, ControllerEventArgs<Models.YBLocations.YBLocationModel> e);
        partial void OnEdited(object sender, ControllerEventArgs<Models.YBLocations.YBLocationModel> e);
        partial void OnDeleting(object sender, ControllerEventArgs<Models.YBLocations.YBLocationModel> e);
        partial void OnDeleted(object sender, ControllerEventArgs<Models.YBLocations.YBLocationModel> e);
    	partial void OnShowing(object sender, MyEventArgs<UIModel<Models.YBLocations.YBLocationModel>> e);
    	partial void OnGettingByKey(object sender, ControllerEventArgs<Models.YBLocations.YBLocationModel> e);
        partial void OnTaken(object sender, ControllerEventArgs<Models.YBLocations.YBLocationModel> e);
       	partial void OnCreateShowing(object sender, ControllerEventArgs<Models.YBLocations.YBLocationModel> e);
		partial void OnEditShowing(object sender, ControllerEventArgs<Models.YBLocations.YBLocationModel> e);
		partial void OnDetailsShowing(object sender, ControllerEventArgs<Models.YBLocations.YBLocationModel> e);
 		partial void OnActionsCreated(object sender, MyEventArgs<UIModel<Models.YBLocations.YBLocationModel >> e);
		partial void OnCustomActionExecuting(object sender, MyEventArgs<ContextActionModel<Models.YBLocations.YBLocationModel>> e);
		partial void OnCustomActionExecutingBackground(object sender, MyEventArgs<ContextActionModel<Models.YBLocations.YBLocationModel>> e);
        partial void OnDownloading(object sender, MyEventArgs<ContextActionModel<Models.YBLocations.YBLocationModel>> e);
      	partial void OnAuthorization(object sender, AuthorizationContext context);
		 partial void OnFilterShowing(object sender, MyEventArgs<UIModel<Models.YBLocations.YBLocationModel >> e);
         partial void OnSummaryOperationShowing(object sender, MyEventArgs<UIModel<Models.YBLocations.YBLocationModel>> e);

        partial void OnExportActionsCreated(object sender, MyEventArgs<UIModel<Models.YBLocations.YBLocationModel>> e);


		protected override void OnVirtualFilterShowing(object sender, MyEventArgs<UIModel<YBLocationModel>> e)
        {
            OnFilterShowing(sender, e);
        }
		 public override void OnVirtualExportActionsCreated(object sender, MyEventArgs<UIModel<YBLocationModel>> e)
        {
            OnExportActionsCreated(sender, e);
        }
        public override void OnVirtualDownloading(object sender, MyEventArgs<ContextActionModel<YBLocationModel>> e)
        {
            OnDownloading(sender, e);
        }
        public override void OnVirtualShowing(object sender, MyEventArgs<UIModel<YBLocationModel>> e)
        {
            OnShowing(sender, e);
        }

	#endregion
	#region API
	 public override ActionResult ApiCreateGen(YBLocationModel model, ContextRequest contextRequest)
        {
            return CreateGen(model, contextRequest);
        }

              public override ActionResult ApiGetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJson(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }
        public override ActionResult ApiGetByKeyJson(string id, ContextRequest contextRequest)
        {
            return  GetByKeyJson(id, contextRequest, true);
        }
      
		 public override int ApiGetByCount(string filter, ContextRequest contextRequest)
        {
            return GetByCount(filter, contextRequest);
        }
         protected override ActionResult ApiDeleteGen(List<YBLocationModel> models, ContextRequest contextRequest)
        {
            List<YBLocation> objs = new List<YBLocation>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                BR.YBLocationsBR.Instance.DeleteBulk(objs, contextRequest);
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        protected override ActionResult ApiUpdateGen(List<YBLocationModel> models, ContextRequest contextRequest)
        {
            List<YBLocation> objs = new List<YBLocation>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                foreach (var obj in objs)
                {
                    BR.YBLocationsBR.Instance.Update(obj, contextRequest);

                }
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }


	#endregion
#region Validation methods	
	    private void Validations(YBLocationModel model) { 
            #region Remote validations

            #endregion
		}

#endregion
		
 		public AuthorizationContext Authorization(AuthorizationContext context)
        {
            OnAuthorization(this,  context );
            return context ;
        }
		public List<YBLocationModel> GetAll() {
            			var bos = BR.YBLocationsBR.Instance.GetBy("",
					new SFSdotNet.Framework.My.ContextRequest()
					{
						CustomQuery = new SFSdotNet.Framework.My.CustomQuery()
						{
							OrderBy = "Description",
							SortDirection = SFSdotNet.Framework.Data.SortDirection.Ascending
						}
					});
            			List<YBLocationModel> results = new List<YBLocationModel>();
            YBLocationModel model = null;
            foreach (var bo in bos)
            {
                model = new YBLocationModel();
                model.Bind(bo);
                results.Add(model);
            }
            return results;

        }
        //
        // GET: /YBLocations/
		[MyAuthorize("r", "YBLocation", "MBKYellowBox", typeof(YBLocationsController))]
		public ActionResult Index()
        {
    		var uiModel = GetContextModel(UIModelContextTypes.ListForm, null);
			ViewBag.UIModel = uiModel;
			uiModel.FilterStart = (string)ViewData["startFilter"];
                    MyEventArgs<UIModel<YBLocationModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<YBLocationModel>>() { Object = uiModel });

			OnExportActionsCreated(this, (me != null ? me : me = new MyEventArgs<UIModel<YBLocationModel>>() { Object = uiModel }));

            if (me != null)
            {
                uiModel = me.Object;
            }
            if (me == null)
                me = new MyEventArgs<UIModel<YBLocationModel>>() { Object = uiModel };
           
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;


            //return View("ListGen");
			return ResolveView(uiModel);
        }
		[MyAuthorize("r", "YBLocation", "MBKYellowBox", typeof(YBLocationsController))]
		public ActionResult ListViewGen(string idTab, string fk , string fkValue, string startFilter, ListModes  listmode  = ListModes.SimpleList, PropertyDefinition parentRelationProperty = null, object parentRelationPropertyValue = null )
        {
			ViewData["idTab"] = System.Web.HttpContext.Current.Request.QueryString["idTab"]; 
		 	ViewData["detpop"] = true; // details in popup
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"])) {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"]; 
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
            {
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["startFilter"]))
            {
                ViewData["startFilter"] = Request.QueryString["startFilter"];
            }
			
			UIModel<YBLocationModel> uiModel = GetContextModel(UIModelContextTypes.ListForm, null);

            MyEventArgs<UIModel<YBLocationModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<YBLocationModel>>() { Object = uiModel });
            if (me == null)
                me = new MyEventArgs<UIModel<YBLocationModel>>() { Object = uiModel };
            uiModel.Properties = GetProperties(uiModel);
            uiModel.ContextType = UIModelContextTypes.ListForm;
             uiModel.FilterStart = (string)ViewData["startFilter"];
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;
			 if (listmode == ListModes.SimpleList)
                return ResolveView(uiModel);
            else
            {
                ViewData["parentRelationProperty"] = parentRelationProperty;
                ViewData["parentRelationPropertyValue"] = parentRelationPropertyValue;
                return PartialView("ListForTagSelectView");
            }
            return ResolveView(uiModel);
        }
		List<PropertyDefinition> _properties = null;

		 protected override List<PropertyDefinition> GetProperties(UIModel uiModel,  params string[] specificProperties)
        { 
            return GetProperties(uiModel, false, null, specificProperties);
        }

		protected override List<PropertyDefinition> GetProperties(UIModel uiModel, bool decripted, Guid? id, params string[] specificProperties)
            {

			bool allProperties = true;    
                if (specificProperties != null && specificProperties.Length > 0)
                {
                    allProperties = false;
                }


			List<CustomProperty> customProperties = new List<CustomProperty>();
			if (_properties == null)
                {
                List<PropertyDefinition> results = new List<PropertyDefinition>();

			string idYBLocation = GetRouteDataOrQueryParam("id");
			if (idYBLocation != null)
			{
				if (!decripted)
                {
					idYBLocation = SFSdotNet.Framework.Entities.Utils.GetPropertyKey(idYBLocation.Replace("-","/"), "GuidLocation");
				}else{
					if (id != null )
						idYBLocation = id.Value.ToString();                

				}
			}

			bool visibleProperty = true;	
			 bool conditionalshow =false;
                if (uiModel.ContextType == UIModelContextTypes.EditForm || uiModel.ContextType == UIModelContextTypes.DisplayForm ||  uiModel.ContextType == UIModelContextTypes.GenericForm )
                    conditionalshow = true;
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidLocation"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidLocation")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 100,
																	
					CustomProperties = customProperties,

                    PropertyName = "GuidLocation",

					 MaxLength = 0,
					IsRequired = true ,
					IsHidden = true,
                    SystemProperty =  SystemProperties.Identifier ,
					IsDefaultProperty = false,
                    SortBy = "GuidLocation",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBLocationResources.GUIDLOCATION*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidLocationType"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidLocationType")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 101,
																	IsForeignKey = true,

									
					CustomProperties = customProperties,

                    PropertyName = "GuidLocationType",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "GuidLocationType",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBLocationResources.GUIDLOCATIONTYPE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Lat"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Lat")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 102,
																	
					CustomProperties = customProperties,

                    PropertyName = "Lat",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Lat",
					
	
                    TypeName = "Double",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBLocationResources.LAT*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Long"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Long")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 103,
																	
					CustomProperties = customProperties,

                    PropertyName = "Long",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Long",
					
	
                    TypeName = "Double",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBLocationResources.LONG*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Description"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Description")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 104,
																	
					CustomProperties = customProperties,

                    PropertyName = "Description",

					 MaxLength = 255,
					 Nullable = true,
					IsDefaultProperty = true,
                    SortBy = "Description",
					
	
                    TypeName = "String",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBLocationResources.DESCRIPTION*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Address"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Address")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 105,
																	
					CustomProperties = customProperties,

                    PropertyName = "Address",

					 MaxLength = 255,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Address",
					
	
                    TypeName = "String",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBLocationResources.ADDRESS*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 106,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedDate",
					
	
				SystemProperty = SystemProperties.CreatedDate ,
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBLocationResources.CREATEDDATE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 123,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedDate",
					
	
					IsUpdatedDate = true,
					SystemProperty = SystemProperties.UpdatedDate ,
	
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    ,PropertyDisplayName = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.UPDATED

                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 108,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedBy",
					
	
				SystemProperty = SystemProperties.CreatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBLocationResources.CREATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 109,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedBy",
					
	
				SystemProperty = SystemProperties.UpdatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBLocationResources.UPDATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Bytes"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Bytes")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 110,
																	
					CustomProperties = customProperties,

                    PropertyName = "Bytes",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Bytes",
					
	
				SystemProperty = SystemProperties.SizeBytes,
                    TypeName = "Int32",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBLocationResources.BYTES*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("LocationType"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"YBLocations" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="LocationType.GuidLocationType")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"LocationType.GuidLocationType" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"LocationTypes" });
			

	
	//fk_Location_LocationType
		//if (this.Request.QueryString["fk"] != "LocationType")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 111,
																
					
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "LocationType",
					PropertyNavigationKey = "GuidLocationType",
					PropertyNavigationText = "Name",
					NavigationPropertyType = NavigationPropertyTypes.SimpleDropDown,
					GetMethodName = "GetAll",
					GetMethodParameters = "",
					GetMethodDisplayText ="Name",
					GetMethodDisplayValue = "GuidLocationType",
					
					CustomProperties = customProperties,

                    PropertyName = "LocationType",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "LocationType.Name",
					
	
                    TypeName = "MBKYellowBoxModel.LocationType",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/LocationTypes"
                    /*,PropertyDisplayName = Resources.YBLocationResources.LOCATIONTYPE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("RouteLocations"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"YBLocation" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="RouteLocations.GuidRouteLocation")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"RouteLocations.GuidRouteLocation" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"RouteLocations" });
			

	
	//fk_RouteLocation_Location
		//if (this.Request.QueryString["fk"] != "RouteLocations")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 112,
																
					Link = VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/RouteLocations/ListViewGen?overrideModule=" + GetOverrideApp()  + "&pal=False&es=False&pag=10&filterlinks=1&idTab=RouteLocations&fk=YBLocation&startFilter="+ (new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext)).Encode("it.YBLocation.GuidLocation = Guid(\"" + idYBLocation +"\")")+ "&fkValue=" + idYBLocation,
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "RouteLocation",
					
					CustomProperties = customProperties,

                    PropertyName = "RouteLocations",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "RouteLocations.OrderRoute",
					
	
                    TypeName = "MBKYellowBoxModel.RouteLocation",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = true,
                    PathName = "MBKYellowBox/RouteLocations"
                    /*,PropertyDisplayName = Resources.YBLocationResources.ROUTELOCATIONS*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("StudentLocations"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"YBLocation" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="StudentLocations.GuidStudentLocation")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"StudentLocations.GuidStudentLocation" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"StudentLocations" });
			

	
	//fk_StudentLocation_Location
		//if (this.Request.QueryString["fk"] != "StudentLocations")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 113,
																
					Link = VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/StudentLocations/ListViewGen?overrideModule=" + GetOverrideApp()  + "&pal=False&es=False&pag=10&filterlinks=1&idTab=StudentLocations&fk=YBLocation&startFilter="+ (new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext)).Encode("it.YBLocation.GuidLocation = Guid(\"" + idYBLocation +"\")")+ "&fkValue=" + idYBLocation,
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "StudentLocation",
					
					CustomProperties = customProperties,

                    PropertyName = "StudentLocations",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "StudentLocations.Bytes",
					
	
                    TypeName = "MBKYellowBoxModel.StudentLocation",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = true,
                    PathName = "MBKYellowBox/StudentLocations"
                    /*,PropertyDisplayName = Resources.YBLocationResources.STUDENTLOCATIONS*/
                });
		//	}
	
	}
	
				
                    _properties = results;
                    return _properties;
                }
                else {
                    return _properties;
                }
            }

		protected override  UIModel<YBLocationModel> GetByForShow(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, params  object[] extraParams)
        {
			if (Request != null )
				if (!string.IsNullOrEmpty(Request.QueryString["q"]))
					filter = filter + HttpUtility.UrlDecode(Request.QueryString["q"]);
 if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
            }
            var bos = BR.YBLocationsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), contextRequest, extraParams);
			//var bos = BR.YBLocationsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), context, extraParams);
            YBLocationModel model = null;
            List<YBLocationModel> results = new List<YBLocationModel>();
            foreach (var item in bos)
            {
                model = new YBLocationModel();
				model.Bind(item);
				results.Add(model);
            }
            //return results;
			UIModel<YBLocationModel> uiModel = GetContextModel(UIModelContextTypes.Items, null);
            uiModel.Items = results;
			if (Request != null){
				if (SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(Request.RequestContext, "action") == "Download")
				{
					uiModel.ContextType = UIModelContextTypes.ExportDownload;
				}
			}
            Showing(ref uiModel);
            return uiModel;
		}			
		
		//public List<YBLocationModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params  object[] extraParams)
        //{
		//	var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, null, extraParams);
        public override List<YBLocationModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest,  params  object[] extraParams)
        {
            var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
           
            return uiModel.Items;
		
        }
		/*
        [MyAuthorize("r", "YBLocation", "MBKYellowBox", typeof(YBLocationsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir)
        {
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir);
        }*/

		  [MyAuthorize("r", "YBLocation", "MBKYellowBox", typeof(YBLocationsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir,ContextRequest contextRequest,  object[] extraParams)
        {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir,contextRequest, extraParams);
        }
/*		  [MyAuthorize("r", "YBLocation", "MBKYellowBox", typeof(YBLocationsController))]
       public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJsonBase(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }*/
		[MyAuthorize()]
		public int GetByCount(string filter, ContextRequest contextRequest) {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
            return BR.YBLocationsBR.Instance.GetCount(HttpUtility.UrlDecode(filter), GetUseMode(), contextRequest);
        }
		

		[MyAuthorize("r", "YBLocation", "MBKYellowBox", typeof(YBLocationsController))]
        public ActionResult GetByKeyJson(string id, ContextRequest contextRequest,  bool dec = false)
        {
            return Json(GetByKey(id, null, contextRequest, dec), JsonRequestBehavior.AllowGet);
        }
		public YBLocationModel GetByKey(string id) {
			return GetByKey(id, null,null, false);
       	}
		    public YBLocationModel GetByKey(string id, string includes)
        {
            return GetByKey(id, includes, false);
        }
		 public  YBLocationModel GetByKey(string id, string includes, ContextRequest contextRequest)
        {
            return GetByKey(id, includes, contextRequest, false);
        }
		/*
		  public ActionResult ShowField(string fieldName, string idField) {
		   string safePropertyName = fieldName;
              if (fieldName.StartsWith("Fk"))
              {
                  safePropertyName = fieldName.Substring(2, fieldName.Length - 2);
              }

             YBLocationModel model = new  YBLocationModel();

            UIModel uiModel = GetUIModel(model, new string[] { "NoField-" });
			
				uiModel.Properties = GetProperties(uiModel, safePropertyName);
		uiModel.Properties.ForEach(p=> p.ContextType = uiModel.ContextType );
            uiModel.ContextType = UIModelContextTypes.FilterFields;
            uiModel.OverrideApp = GetOverrideApp();
            uiModel.UseMode = GetUseMode();

            ViewData["uiModel"] = uiModel;
			var prop = uiModel.Properties.FirstOrDefault(p=>p.PropertyName == safePropertyName);
            //if (prop.IsNavigationProperty && prop.IsNavigationPropertyMany == false)
            //{
            //    ViewData["currentProperty"] = uiModel.Properties.FirstOrDefault(p => p.PropertyName != fieldName + "Text");
            //}else if (prop.IsNavigationProperty == false){
                ViewData["currentProperty"] = prop;
           // }
            ((PropertyDefinition)ViewData["currentProperty"]).RemoveLayout = true;
			ViewData["withContainer"] = false;


            return PartialView("GenericField", model);


        }
      */
	public YBLocationModel GetByKey(string id, ContextRequest contextRequest, bool dec)
        {
            return GetByKey(id, null, contextRequest, dec);
        }
        public YBLocationModel GetByKey(string id, string  includes, bool dec)
        {
            return GetByKey(id, includes, null, dec);
        }

        public YBLocationModel GetByKey(string id, string includes, ContextRequest contextRequest, bool dec) {
		             YBLocationModel model = null;
            ControllerEventArgs<YBLocationModel> e = null;
			string objectKey = id.Replace("-","/");
             OnGettingByKey(this, e=  new ControllerEventArgs<YBLocationModel>() { Id = objectKey  });
             bool cancel = false;
             YBLocationModel eItem = null;
             if (e != null)
             {
                 cancel = e.Cancel;
                 eItem = e.Item;
             }
			if (cancel == false && eItem == null)
             {
			Guid guidLocation = Guid.Empty; //new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, "GuidLocation"));
			if (dec)
                 {
                     guidLocation = new Guid(id);
                 }
                 else
                 {
                     guidLocation = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, null));
                 }
			
            
				model = new YBLocationModel();
                  if (contextRequest == null)
                {
                    contextRequest = GetContextRequest();
                }
				var bo = BR.YBLocationsBR.Instance.GetByKey(guidLocation, GetUseMode(), contextRequest,  includes);
				 if (bo != null)
                    model.Bind(bo);
                else
                    return null;
			}
             else {
                 model = eItem;
             }
			model.IsNew = false;

            return model;
        }
        // GET: /YBLocations/DetailsGen/5
		[MyAuthorize("r", "YBLocation", "MBKYellowBox", typeof(YBLocationsController))]
        public ActionResult DetailsGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = YBLocationResources.ENTITY_PLURAL;
			 #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<YBLocationModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
            #endregion



			 bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null) {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
			//UIModel<YBLocationModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, decripted), decripted, guidId);
			var item = GetByKey(id, null, null, decripted);
			if (item == null)
            {
                 RouteValueDictionary rv = new RouteValueDictionary();
                string usemode = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"usemode");
                string overrideModule = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "overrideModule");
                string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");

                if(!string.IsNullOrEmpty(usemode)){
                    rv.Add("usemode", usemode);
                }
                if(!string.IsNullOrEmpty(overrideModule)){
                    rv.Add("overrideModule", overrideModule);
                }
                if (!string.IsNullOrEmpty(area))
                {
                    rv.Add("area", area);
                }

                return RedirectToAction("Index", rv);
            }
            //
            UIModel<YBLocationModel> uiModel = null;
                uiModel = GetContextModel(UIModelContextTypes.DisplayForm, item, decripted, guidId);



            MyEventArgs<UIModel<YBLocationModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<YBLocationModel>>() { Object = uiModel });

            if (me != null) {
                uiModel = me.Object;
            }
			
            Showing(ref uiModel);
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			
            //return View("DisplayGen", uiModel.Items[0]);
			return ResolveView(uiModel, uiModel.Items[0]);

        }
		[MyAuthorize("r", "YBLocation", "MBKYellowBox", typeof(YBLocationsController))]
		public ActionResult DetailsViewGen(string id)
        {

		 bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<YBLocationModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
           
			if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
 			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
           
        	 //var uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id));
			 
            bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
            {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true")
                {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
            UIModel<YBLocationModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, null, decripted), decripted, guidId);
			

            MyEventArgs<UIModel<YBLocationModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<YBLocationModel>>() { Object = uiModel });

            if (me != null)
            {
                uiModel = me.Object;
            }
            
            Showing(ref uiModel);
            return ResolveView(uiModel, uiModel.Items[0]);
        
        }
        //
        // GET: /YBLocations/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        //
        // GET: /YBLocations/CreateGen
		[MyAuthorize("c", "YBLocation", "MBKYellowBox", typeof(YBLocationsController))]
        public ActionResult CreateGen()
        {
			YBLocationModel model = new YBLocationModel();
            model.IsNew = true;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model);

			OnCreateShowing(this, e = new ControllerEventArgs<YBLocationModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }

             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                 ViewData["ispopup"] = true;
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                 ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                 ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

            Showing(ref me);

			return ResolveView(me, me.Items[0]);
        } 
			
		protected override UIModel<YBLocationModel> GetContextModel(UIModelContextTypes formMode, YBLocationModel model)
        {
            return GetContextModel(formMode, model, false, null);
        }
			
		 private UIModel<YBLocationModel> GetContextModel(UIModelContextTypes formMode, YBLocationModel model, bool decript, Guid ? id) {
            UIModel<YBLocationModel> me = new UIModel<YBLocationModel>(true, "YBLocations");
			me.UseMode = GetUseMode();
			me.Controller = this;
			me.OverrideApp = GetOverrideApp();
			me.ContextType = formMode ;
			me.Id = "YBLocation";
			
            me.ModuleKey = "MBKYellowBox";

			me.ModuleNamespace = "MBK.YellowBox";
            me.EntityKey = "YBLocation";
            me.EntitySetName = "YBLocations";

			me.AreaAction = "MBKYellowBox";
            me.ControllerAction = "YBLocations";
            me.PropertyKeyName = "GuidLocation";

            me.Properties = GetProperties(me, decript, id);

			me.SortBy = "UpdatedDate";
			me.SortDirection = UIModelSortDirection.DESC;

 			
			if (Request != null)
            {
                string actionName = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam( Request.RequestContext, "action");
                if(actionName != null && actionName.ToLower().Contains("create") ){
                    me.IsNew = true;
                }
            }
			 #region Buttons
			 if (Request != null ){
             if (formMode == UIModelContextTypes.DisplayForm || formMode == UIModelContextTypes.EditForm || formMode == UIModelContextTypes.ListForm)
				me.ActionButtons = GetActionButtons(formMode,model != null ?(Request.QueryString["dec"] == "true" ? model.Id : model.SafeKey)  : null, "MBKYellowBox", "YBLocations", "YBLocation", me.IsNew);

            //me.ActionButtons.Add(new ActionModel() { ActionKey = "return", Title = GlobalMessages.RETURN, Url = System.Web.VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/YBLocations" });
			if (this.HttpContext != null &&  !this.HttpContext.SkipAuthorization){
				//antes this.HttpContext
				me.SetAction("u", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("u", "YBLocation", "MBKYellowBox"));
				me.SetAction("c", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("c", "YBLocation", "MBKYellowBox"));
				me.SetAction("d", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("d", "YBLocation", "MBKYellowBox"));
			
			}else{
				me.SetAction("u", true);
				me.SetAction("c", true);
				me.SetAction("d", true);

			}
            #endregion              
         
            switch (formMode)
            {
                case UIModelContextTypes.DisplayForm:
					//me.TitleForm = YBLocationResources.YBLOCATIONS_DETAILS;
                    me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.MODIFY_DATA;
					 me.Properties.Where(p=>p.PropertyName  != "Id" && p.IsForeignKey == false).ToList().ForEach(p => p.IsHidden = false);

					 me.Properties.Where(p => (p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier) ).ToList().ForEach(p=> me.SetHide(p.PropertyName));

                    break;
                case UIModelContextTypes.EditForm:
				  me.Properties.Where(p=>p.SystemProperty != SystemProperties.Identifier && p.IsForeignKey == false && p.PropertyName != "Id").ToList().ForEach(p => p.IsHidden = false);

					if (model != null)
                    {
						

                        me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.SAVE_DATA;                        
                        me.ActionButtons.First(p => p.ActionKey == "c").Title = GlobalMessages.SAVE_DATA;
						if (model.IsNew ){
							//me.TitleForm = YBLocationResources.YBLOCATIONS_ADD_NEW;
							me.ActionName = "CreateGen";
							me.Properties.RemoveAll(p => p.SystemProperty != null || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));
						}else{
							
							me.ActionName = "EditGen";

							//me.TitleForm = YBLocationResources.YBLOCATIONS_EDIT;
							me.Properties.RemoveAll(p => p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));	
						}
						//me.Properties.Remove(me.Properties.Find(p => p.PropertyName == "UpdatedDate"));
					
					}
                    break;
                case UIModelContextTypes.FilterFields:
                    break;
                case UIModelContextTypes.GenericForm:
                    break;
                case UIModelContextTypes.Items:
				//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "Description") != null){
						me.Properties.Find(p => p.PropertyName == "Description").IsHidden = false;
					 }
					 
                    
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					 
                    
					

						 if (me.Properties.Find(p => p.PropertyName == "GuidLocation") != null){
						me.Properties.Find(p => p.PropertyName == "GuidLocation").IsHidden = false;
					 }
					 
                    
					


                  


					//}
                    break;
                case UIModelContextTypes.ListForm:
					PropertyDefinition propFinded = null;
					//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "Description") != null){
						me.Properties.Find(p => p.PropertyName == "Description").IsHidden = false;
					 }
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					
					me.PrincipalActionName = "GetByJson";
					//}
					//me.TitleForm = YBLocationResources.YBLOCATIONS_LIST;
                    break;
                default:
                    break;
            }
            	this.SetDefaultProperties(me);
			}
			if (model != null )
            	me.Items.Add(model);
            return me;
        }
		// GET: /YBLocations/CreateViewGen
		[MyAuthorize("c", "YBLocation", "MBKYellowBox", typeof(YBLocationsController))]
        public ActionResult CreateViewGen()
        {
				YBLocationModel model = new YBLocationModel();
            model.IsNew = true;
			e= null;
			OnCreateShowing(this, e = new ControllerEventArgs<YBLocationModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }
			
            var me = GetContextModel(UIModelContextTypes.EditForm, model);

			me.IsPartialView = true;	
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
            {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                me.Properties.Find(p => p.PropertyName == ViewData["fk"].ToString()).IsReadOnly = true;
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
			
      
            //me.Items.Add(model);
            Showing(ref me);
            return ResolveView(me, me.Items[0]);
        }
		protected override  void GettingExtraData(ref UIModel<YBLocationModel> uiModel)
        {

            MyEventArgs<UIModel<YBLocationModel>> me = null;
            OnGettingExtraData(this, me = new MyEventArgs<UIModel<YBLocationModel>>() { Object = uiModel });
            //bool maybeAnyReplaced = false; 
            if (me != null)
            {
                uiModel = me.Object;
                //maybeAnyReplaced = true;
            }
           
			bool canFill = false;
			 string query = null ;
            bool isFK = false;
			PropertyDefinition prop =null;
			var contextRequest = this.GetContextRequest();
            contextRequest.CustomParams.Add(new CustomParam() { Name="ui", Value= YBLocation.EntityName });

            			canFill = false;
			 query = "";
            isFK =false;
			prop = uiModel.Properties.FirstOrDefault(p => p.PropertyName == "LocationType");
			if (prop != null)
				if (prop.IsHidden == false && (prop.ContextType == UIModelContextTypes.EditForm || prop.ContextType == UIModelContextTypes.FilterFields  || (prop.ContextType == null && uiModel.ContextType == UIModelContextTypes.EditForm)))
				{
					if (prop.NavigationPropertyType == NavigationPropertyTypes.SimpleDropDown )
						canFill = true;
				}
                else if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidLocationType = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidLocationType = @GuidLocationType";
					
					canFill = true;
                }
				if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidLocationType = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidLocationType = @GuidLocationType";
					canFill = true;
                }
			if (canFill){
			                contextRequest.CustomQuery = new CustomQuery();

				if (!uiModel.ExtraData.Exists(p => p.PropertyName == "LocationType")) {
					if (!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))				  
						contextRequest.CustomQuery.SetParam("GuidLocationType", new Nullable<Guid>(Guid.Parse( Request.QueryString["fkValue"])));

					 if (isFK == true)
                    {
						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.LocationTypesBR()).GetBy(query, contextRequest), "GuidLocationType", "Name", Request.QueryString["fkValue"]), PropertyName = "LocationType" });    
                    }
                    else
                    {

						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.LocationTypesBR()).GetBy(query, contextRequest), "GuidLocationType", "Name"), PropertyName = "LocationType" });    

					}
    if (isFK)
                    {    
						var FkLocationType = ((SelectList)uiModel.ExtraData.First(p => p.PropertyName == "LocationType").Data).First();
						uiModel.Items[0].GetType().GetProperty("FkLocationTypeText").SetValue(uiModel.Items[0], FkLocationType.Text);
						uiModel.Items[0].GetType().GetProperty("FkLocationType").SetValue(uiModel.Items[0], Guid.Parse(FkLocationType.Value));
                    
					}    
				}
			}
		 
            

        }
		private void Showing(ref UIModel<YBLocationModel> uiModel) {
          	
			MyEventArgs<UIModel<YBLocationModel>> me = new MyEventArgs<UIModel<YBLocationModel>>() { Object = uiModel };
			 OnVirtualLayoutSettings(this, me);


            OnShowing(this, me);

			
			if ((Request != null && Request.QueryString["allFields"] == "1") || Request == null )
			{
				me.Object.Properties.ForEach(p=> p.IsHidden = false);
            }
            if (me != null)
            {
                uiModel = me.Object;
            }
          


			 if (uiModel.ContextType == UIModelContextTypes.EditForm)
			    GettingExtraData(ref uiModel);
            ViewData["UIModel"] = uiModel;

        }
        //
        // POST: /YBLocations/Create
		[MyAuthorize("c", "YBLocation", "MBKYellowBox", typeof(YBLocationsController))]
        [HttpPost]
		[ValidateInput(false)] 
        public ActionResult CreateGen(YBLocationModel  model,  ContextRequest contextRequest)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
		 	e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<YBLocationModel>() { Item = model });
           
		  	if (!ModelState.IsValid) {
				model.IsNew = true;
				var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
                 if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                        ViewData["ispopup"] = true;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
                    return ResolveView(me, model);
            }
            try
            {
				if (model.GuidLocation == null || model.GuidLocation.ToString().Contains("000000000"))
				model.GuidLocation = Guid.NewGuid();
	
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnCreating(this, e = new ControllerEventArgs<YBLocationModel>() { Item = model });
                if (e != null) {
                   if (e.Cancel && e.RedirectValues.Count > 0){
                        RouteValueDictionary rv = new RouteValueDictionary();
                        if (e.RedirectValues["area"] != null ){
                            rv.Add("area", e.RedirectValues["area"].ToString());
                        }
                        foreach (var item in e.RedirectValues.Where(p=>p.Key != "area" && p.Key != "controller" &&  p.Key != "action" ))
	                    {
		                    rv.Add(item.Key, item.Value);
	                    }

                        //if (e.RedirectValues["action"] != null && e.RedirectValues["controller"] != null && e.RedirectValues["area"] != null )
                        return RedirectToAction(e.RedirectValues["action"].ToString(), e.RedirectValues["controller"].ToString(), rv );


                        
                    }else if (e.Cancel && e.ActionResult != null )
                        return e.ActionResult;  
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				if (contextRequest == null || contextRequest.Company == null){
					contextRequest = GetContextRequest();
					
				}
                if (!cancel)
                	model.Bind(YBLocationsBR.Instance.Create(model.GetBusinessObject(), contextRequest ));
				OnCreated(this, e = new ControllerEventArgs<YBLocationModel>() { Item = model });
                 if (e != null )
					if (e.ActionResult != null)
                    	replaceResult = true;		
				if (!replaceResult)
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))
                    {
                        ViewData["__continue"] = true;
                    }
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"]) &&  string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
                        popupextra.Add("id", model.SafeKey);
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                       {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                            popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"area"));
                            popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                            popupextra.Add("action", actionDetails);
                       if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
                    {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))

                        {
                            var popupextra = GetRouteData();
                            popupextra.Add("id", model.SafeKey);
                            return RedirectToAction("EditViewGen", popupextra);
                        }
                        else
                        {
                            return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.ADD_DONE));
                        }
                    }        			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                        return Redirect(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]);
                    else{

							RouteValueDictionary popupextra = null; 
							if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"])){
                            popupextra = GetRouteData();
							 string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
                            if (!string.IsNullOrEmpty(area))
                                popupextra.Add("area", area);
                            
                            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"])) {
								popupextra.Add("id", model.SafeKey);
                                return RedirectToAction("EditGen", popupextra);

                            }else{
								
                            return RedirectToAction("Index", popupextra);
							}
							}else{
								return Content("ok");
							}
                        }
						 }
                else {
                    return e.ActionResult;
                    }
				}
            catch(Exception ex)
            {
					if (!string.IsNullOrEmpty(Request.QueryString["rok"]))
                {
                    throw  ex;
                }
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                model.IsNew = true;
                var me = GetContextModel(UIModelContextTypes.EditForm, model, true, model.GuidLocation);
                Showing(ref me);
                if (isPopUp)
                {
                    
                        ViewData["ispopup"] = isPopUp;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
					if (Request != null)
						return ResolveView(me, model);
					else
						return Content("ok");
            }
        }        
        //
        // GET: /YBLocations/Edit/5 
        public ActionResult Edit(int id)
        {
            return View();
        }
			
		
		[MyAuthorize("u", "YBLocation", "MBKYellowBox", typeof(YBLocationsController))]
		[MvcSiteMapNode(Area="MBKYellowBox", Title="sss", Clickable=false, ParentKey = "MBKYellowBox_YBLocation_List")]
		public ActionResult EditGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = YBLocationResources.ENTITY_SINGLE;		 	
  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<YBLocationModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
            YBLocationModel model = null;
            // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
			bool dec = false;
            Guid ? idGuidDecripted = null ;
            if (Request != null && Request.QueryString["dec"] == "true")
            {
                dec = true;
                idGuidDecripted = Guid.Parse(id);
            }

            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model,dec,idGuidDecripted);
            Showing(ref me);


            if (!replaceResult)
            {
                 //return View("EditGen", me.Items[0]);
				 return ResolveView(me, me.Items[0]);
            }
            else {
                return e.ActionResult;
            }
        }
			[MyAuthorize("u", "YBLocation","MBKYellowBox", typeof(YBLocationsController))]
		public ActionResult EditViewGen(string id)
        {
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

					  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<YBLocationModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
			
            YBLocationModel model = null;
			 bool dec = false;
            Guid? guidId = null ;

            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null && System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                dec = true;
                guidId = Guid.Parse(id);
            }
            // si fue implementado el método parcial y no se ha decidido suspender la acción
            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
            var me = GetContextModel(UIModelContextTypes.EditForm, model, dec, guidId);
            Showing(ref me);

            return ResolveView(me, model);
        }
		[MyAuthorize("u", "YBLocation",  "MBKYellowBox", typeof(YBLocationsController))]
		[HttpPost]
		[ValidateInput(false)] 
		        public ActionResult EditGen(YBLocationModel model)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
			e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<YBLocationModel>() { Item = model });
           
            if (!ModelState.IsValid)
            {
			   	var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
			
				if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"])){
                	ViewData["ispopup"] = true;
					return ResolveView(me, model);
				}
				else
					return ResolveView(me, model);
            }
            try
            {
			
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnEditing(this, e = new ControllerEventArgs<YBLocationModel>() { Item = model });
                if (e != null) {
                    if (e.Cancel && e.ActionResult != null)
                        return e.ActionResult;
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				ContextRequest context = new ContextRequest();
                context.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;

                YBLocation resultObj = null;
			    if (!cancel)
                	resultObj = YBLocationsBR.Instance.Update(model.GetBusinessObject(), GetContextRequest());
				
				OnEdited(this, e = new ControllerEventArgs<YBLocationModel>() { Item =   new YBLocationModel(resultObj) });
				if (e != null && e.ActionResult != null) replaceResult = true; 

                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Content("ok");
                }
                else
                {
				if (!replaceResult)
                {
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"])  && string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
						 if (Request != null && Request.QueryString["dec"] == "true")
                        {
                            popupextra.Add("id", model.Id);
                        }
                        else
                        {
							popupextra.Add("id", model.SafeKey);

							
                        }
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                        {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                        popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area"));
                        popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                        popupextra.Add("action", actionDetails);
                        if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
					if (isPopUp)
						return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.UPDATE_DONE));
        			    string returnUrl = null;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"])) {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.Form["ReturnAfter"];
                        else if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"];
                    }
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else{
		RouteValueDictionary popupextra = null; 
						 if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"]))
                            {
							
							popupextra = GetRouteData();
							string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
							if (!string.IsNullOrEmpty(area))
								popupextra.Add("area", area);

							return RedirectToAction("Index", popupextra);
						}else{
							return Content("ok");
						}
						}
				 }
                else {
                    return e.ActionResult;
				}
                }		
            }
          catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
			    if (isPopUp)
                {
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(ex.Message));
                    
                }
                else
                {
                    SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                    
                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
                else {
						  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
							ViewData["ispopup"] = true;
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
							ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
							ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

						var me = GetContextModel(UIModelContextTypes.EditForm, model);
						Showing(ref me);

						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
						{
							ViewData["ispopup"] = true;
							return ResolveView(me, model);
						}
						else
							return ResolveView(me, model);

						
					}
				}
            }
        }
        //
        // POST: /YBLocations/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                //  Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /YBLocations/Delete/5
        
		[MyAuthorize("d", "YBLocation", "MBKYellowBox", typeof(YBLocationsController))]
		[HttpDelete]
        public ActionResult DeleteGen(string objectKey, string extraParams)
        {
            try
            {
					
			
				Guid guidLocation = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey.Replace("-", "/"), "GuidLocation")); 
                BO.YBLocation entity = new BO.YBLocation() { GuidLocation = guidLocation };

                BR.YBLocationsBR.Instance.Delete(entity, GetContextRequest());               
                return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.DELETE_DONE));

            }
            catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null)
                    {
                        message = ex.Data["usermessage"].ToString();
                    }

                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }
            }
        }
		/*[MyAuthorize()]
		public FileMediaResult Download(string query, bool? allSelected = false,  string selected = null , string orderBy = null , string direction = null , string format = null , string actionKey=null )
        {
			
            List<Guid> keysSelected1 = new List<Guid>();
            if (!string.IsNullOrEmpty(selected)) {
                foreach (var keyString in selected.Split(char.Parse("|")))
                {
				
                    keysSelected1.Add(Guid.Parse(keyString));
                }
            }
				
            query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(query, allSelected.Value, selected, "GuidLocation");
            MyEventArgs<ContextActionModel<YBLocationModel>> eArgs = null;
            List<YBLocationModel> results = GetBy(query, null, null, orderBy, direction, GetContextRequest(), keysSelected1);
            OnDownloading(this, eArgs = new MyEventArgs<ContextActionModel<YBLocationModel>>() { Object = new ContextActionModel<YBLocationModel>() { Query = query, SelectedItems = results, Selected=selected, SelectAll = allSelected.Value, Direction = direction , OrderBy = orderBy, ActionKey=actionKey  } });

            if (eArgs != null)
            {
                if (eArgs.Object.Result != null)
                    return (FileMediaResult)eArgs.Object.Result;
            }
            

            return (new FeaturesController()).ExportDownload(typeof(YBLocationModel), results, format, this.GetUIPluralText("MBKYellowBox", "YBLocation"));
            
        }
			*/
		
		[HttpPost]
        public ActionResult CustomActionExecute(ContextActionModel model) {
		 try
            {
			//List<Guid> keysSelected1 = new List<Guid>();
			List<object> keysSelected1 = new List<object>();
            if (!string.IsNullOrEmpty(model.Selected))
            {
                foreach (var keyString in model.Selected.Split(char.Parse(",")))
                {
				
				keysSelected1.Add(Guid.Parse(keyString.Split(char.Parse("|"))[0]));
                        
                    

			
                }
            }
			DataAction dataAction = DataAction.GetDataAction(Request);
			 model.Selected = dataAction.Selected;

            model.Query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(dataAction.Query, dataAction.AllSelected, dataAction.Selected, "GuidLocation");
           
            
			
			#region implementaci�n de m�todo parcial
            bool replaceResult = false;
            MyEventArgs<ContextActionModel<YBLocationModel>> actionEventArgs = null;
           
			if (model.ActionKey != "deletemany" && model.ActionKey != "deleterelmany" && model.ActionKey != "updateRel" &&  model.ActionKey != "delete-relation-fk" && model.ActionKey != "restore" )
			{
				ContextRequest context = SFSdotNet.Framework.My.Context.BuildContextRequestSafe(System.Web.HttpContext.Current);
				context.UseMode = dataAction.Usemode;

				if (model.IsBackground)
				{
					System.Threading.Tasks.Task.Run(() => 
						OnCustomActionExecutingBackground(this, actionEventArgs = new MyEventArgs<ContextActionModel<YBLocationModel>>() { Object = new ContextActionModel<YBLocationModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } })
					);
				}
				else
				{
					OnCustomActionExecuting(this, actionEventArgs = new MyEventArgs<ContextActionModel<YBLocationModel>>() {  Object = new ContextActionModel<YBLocationModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } });
				}
			}
            List<YBLocationModel> results = null;
	
			if (model.ActionKey == "deletemany") { 
				
				BR.YBLocationsBR.Instance.Delete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

            }
	
			else if (model.ActionKey == "restore") {
                    BR.YBLocationsBR.Instance.UnDelete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

                }
            else if (model.ActionKey == "updateRel" || model.ActionKey == "delete-relation-fk" || model.ActionKey == "updateRel-proxyMany")
            {
               try {
                   string valueForUpdate = null;
				   string propForUpdate = null;
				   if (!string.IsNullOrEmpty(Request.Params["propertyForUpdate"])){
						propForUpdate = Request.Params["propertyForUpdate"];
				   }
				    if (string.IsNullOrEmpty(propForUpdate) && !string.IsNullOrEmpty(Request.QueryString["propertyForUpdate"]))
                   {
                       propForUpdate = Request.QueryString["propertyForUpdate"];
                   }
                    if (model.ActionKey != "delete-relation-fk")
                    {
                        valueForUpdate = Request.QueryString["valueForUpdate"];
                    }
                    BR.YBLocationsBR.Instance.UpdateAssociation(propForUpdate, valueForUpdate, model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());
					
                    if (model.ActionKey == "delete-relation-fk")
                    {
                        MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]);

                        return PartialView("ResultMessageView", message);
                    }
                    else
                    {
                        return Content("ok");
                    }
       
          
                }
                catch (Exception ex)
                {
				        SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
           
                }
            
            }
		
                if (actionEventArgs == null && !model.IsBackground)
                {
                    //if (model.ActionKey != "deletemany"  && model.ActionKey != "deleterelmany")
                    //{
                     //   throw new NotImplementedException("");
                    //}
                }
                else
                {
					if (model.IsBackground == false )
						 replaceResult = actionEventArgs.Object.Result is ActionResult /*actionEventArgs.Object.ReplaceResult*/;
                }
                #endregion
                if (!replaceResult)
                {
                    if (Request != null && Request.IsAjaxRequest())
                    {
						MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]) ;
                        if (model.IsBackground )
                            message.Message = GlobalMessages.THE_PROCESS_HAS_BEEN_STARTED;
                        
                        return PartialView("ResultMessageView", message);                    
					}
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return (ActionResult)actionEventArgs.Object.Result;
                }
            }
            catch (Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null) {
                        message = ex.Data["usermessage"].ToString();
                    }
					 SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }

            }
        }
        //
        // POST: /YBLocations/Delete/5
        
			
	
    }
}
namespace MBK.YellowBox.Web.Mvc.Controllers
{
	using MBK.YellowBox.Web.Mvc.Models.YBRoutes;

    public partial class YBRoutesController : MBK.YellowBox.Web.Mvc.ControllerBase<Models.YBRoutes.YBRouteModel>
    {

       


	#region partial methods
        ControllerEventArgs<Models.YBRoutes.YBRouteModel> e = null;
        partial void OnValidating(object sender, ControllerEventArgs<Models.YBRoutes.YBRouteModel> e);
        partial void OnGettingExtraData(object sender, MyEventArgs<UIModel<Models.YBRoutes.YBRouteModel>> e);
        partial void OnCreating(object sender, ControllerEventArgs<Models.YBRoutes.YBRouteModel> e);
        partial void OnCreated(object sender, ControllerEventArgs<Models.YBRoutes.YBRouteModel> e);
        partial void OnEditing(object sender, ControllerEventArgs<Models.YBRoutes.YBRouteModel> e);
        partial void OnEdited(object sender, ControllerEventArgs<Models.YBRoutes.YBRouteModel> e);
        partial void OnDeleting(object sender, ControllerEventArgs<Models.YBRoutes.YBRouteModel> e);
        partial void OnDeleted(object sender, ControllerEventArgs<Models.YBRoutes.YBRouteModel> e);
    	partial void OnShowing(object sender, MyEventArgs<UIModel<Models.YBRoutes.YBRouteModel>> e);
    	partial void OnGettingByKey(object sender, ControllerEventArgs<Models.YBRoutes.YBRouteModel> e);
        partial void OnTaken(object sender, ControllerEventArgs<Models.YBRoutes.YBRouteModel> e);
       	partial void OnCreateShowing(object sender, ControllerEventArgs<Models.YBRoutes.YBRouteModel> e);
		partial void OnEditShowing(object sender, ControllerEventArgs<Models.YBRoutes.YBRouteModel> e);
		partial void OnDetailsShowing(object sender, ControllerEventArgs<Models.YBRoutes.YBRouteModel> e);
 		partial void OnActionsCreated(object sender, MyEventArgs<UIModel<Models.YBRoutes.YBRouteModel >> e);
		partial void OnCustomActionExecuting(object sender, MyEventArgs<ContextActionModel<Models.YBRoutes.YBRouteModel>> e);
		partial void OnCustomActionExecutingBackground(object sender, MyEventArgs<ContextActionModel<Models.YBRoutes.YBRouteModel>> e);
        partial void OnDownloading(object sender, MyEventArgs<ContextActionModel<Models.YBRoutes.YBRouteModel>> e);
      	partial void OnAuthorization(object sender, AuthorizationContext context);
		 partial void OnFilterShowing(object sender, MyEventArgs<UIModel<Models.YBRoutes.YBRouteModel >> e);
         partial void OnSummaryOperationShowing(object sender, MyEventArgs<UIModel<Models.YBRoutes.YBRouteModel>> e);

        partial void OnExportActionsCreated(object sender, MyEventArgs<UIModel<Models.YBRoutes.YBRouteModel>> e);


		protected override void OnVirtualFilterShowing(object sender, MyEventArgs<UIModel<YBRouteModel>> e)
        {
            OnFilterShowing(sender, e);
        }
		 public override void OnVirtualExportActionsCreated(object sender, MyEventArgs<UIModel<YBRouteModel>> e)
        {
            OnExportActionsCreated(sender, e);
        }
        public override void OnVirtualDownloading(object sender, MyEventArgs<ContextActionModel<YBRouteModel>> e)
        {
            OnDownloading(sender, e);
        }
        public override void OnVirtualShowing(object sender, MyEventArgs<UIModel<YBRouteModel>> e)
        {
            OnShowing(sender, e);
        }

	#endregion
	#region API
	 public override ActionResult ApiCreateGen(YBRouteModel model, ContextRequest contextRequest)
        {
            return CreateGen(model, contextRequest);
        }

              public override ActionResult ApiGetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJson(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }
        public override ActionResult ApiGetByKeyJson(string id, ContextRequest contextRequest)
        {
            return  GetByKeyJson(id, contextRequest, true);
        }
      
		 public override int ApiGetByCount(string filter, ContextRequest contextRequest)
        {
            return GetByCount(filter, contextRequest);
        }
         protected override ActionResult ApiDeleteGen(List<YBRouteModel> models, ContextRequest contextRequest)
        {
            List<YBRoute> objs = new List<YBRoute>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                BR.YBRoutesBR.Instance.DeleteBulk(objs, contextRequest);
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        protected override ActionResult ApiUpdateGen(List<YBRouteModel> models, ContextRequest contextRequest)
        {
            List<YBRoute> objs = new List<YBRoute>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                foreach (var obj in objs)
                {
                    BR.YBRoutesBR.Instance.Update(obj, contextRequest);

                }
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }


	#endregion
#region Validation methods	
	    private void Validations(YBRouteModel model) { 
            #region Remote validations

            #endregion
		}

#endregion
		
 		public AuthorizationContext Authorization(AuthorizationContext context)
        {
            OnAuthorization(this,  context );
            return context ;
        }
		public List<YBRouteModel> GetAll() {
            			var bos = BR.YBRoutesBR.Instance.GetBy("",
					new SFSdotNet.Framework.My.ContextRequest()
					{
						CustomQuery = new SFSdotNet.Framework.My.CustomQuery()
						{
							OrderBy = "Title",
							SortDirection = SFSdotNet.Framework.Data.SortDirection.Ascending
						}
					});
            			List<YBRouteModel> results = new List<YBRouteModel>();
            YBRouteModel model = null;
            foreach (var bo in bos)
            {
                model = new YBRouteModel();
                model.Bind(bo);
                results.Add(model);
            }
            return results;

        }
        //
        // GET: /YBRoutes/
		[MyAuthorize("r", "YBRoute", "MBKYellowBox", typeof(YBRoutesController))]
		public ActionResult Index()
        {
    		var uiModel = GetContextModel(UIModelContextTypes.ListForm, null);
			ViewBag.UIModel = uiModel;
			uiModel.FilterStart = (string)ViewData["startFilter"];
                    MyEventArgs<UIModel<YBRouteModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<YBRouteModel>>() { Object = uiModel });

			OnExportActionsCreated(this, (me != null ? me : me = new MyEventArgs<UIModel<YBRouteModel>>() { Object = uiModel }));

            if (me != null)
            {
                uiModel = me.Object;
            }
            if (me == null)
                me = new MyEventArgs<UIModel<YBRouteModel>>() { Object = uiModel };
           
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;


            //return View("ListGen");
			return ResolveView(uiModel);
        }
		[MyAuthorize("r", "YBRoute", "MBKYellowBox", typeof(YBRoutesController))]
		public ActionResult ListViewGen(string idTab, string fk , string fkValue, string startFilter, ListModes  listmode  = ListModes.SimpleList, PropertyDefinition parentRelationProperty = null, object parentRelationPropertyValue = null )
        {
			ViewData["idTab"] = System.Web.HttpContext.Current.Request.QueryString["idTab"]; 
		 	ViewData["detpop"] = true; // details in popup
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"])) {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"]; 
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
            {
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["startFilter"]))
            {
                ViewData["startFilter"] = Request.QueryString["startFilter"];
            }
			
			UIModel<YBRouteModel> uiModel = GetContextModel(UIModelContextTypes.ListForm, null);

            MyEventArgs<UIModel<YBRouteModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<YBRouteModel>>() { Object = uiModel });
            if (me == null)
                me = new MyEventArgs<UIModel<YBRouteModel>>() { Object = uiModel };
            uiModel.Properties = GetProperties(uiModel);
            uiModel.ContextType = UIModelContextTypes.ListForm;
             uiModel.FilterStart = (string)ViewData["startFilter"];
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;
			 if (listmode == ListModes.SimpleList)
                return ResolveView(uiModel);
            else
            {
                ViewData["parentRelationProperty"] = parentRelationProperty;
                ViewData["parentRelationPropertyValue"] = parentRelationPropertyValue;
                return PartialView("ListForTagSelectView");
            }
            return ResolveView(uiModel);
        }
		List<PropertyDefinition> _properties = null;

		 protected override List<PropertyDefinition> GetProperties(UIModel uiModel,  params string[] specificProperties)
        { 
            return GetProperties(uiModel, false, null, specificProperties);
        }

		protected override List<PropertyDefinition> GetProperties(UIModel uiModel, bool decripted, Guid? id, params string[] specificProperties)
            {

			bool allProperties = true;    
                if (specificProperties != null && specificProperties.Length > 0)
                {
                    allProperties = false;
                }


			List<CustomProperty> customProperties = new List<CustomProperty>();
			if (_properties == null)
                {
                List<PropertyDefinition> results = new List<PropertyDefinition>();

			string idYBRoute = GetRouteDataOrQueryParam("id");
			if (idYBRoute != null)
			{
				if (!decripted)
                {
					idYBRoute = SFSdotNet.Framework.Entities.Utils.GetPropertyKey(idYBRoute.Replace("-","/"), "GuidRoute");
				}else{
					if (id != null )
						idYBRoute = id.Value.ToString();                

				}
			}

			bool visibleProperty = true;	
			 bool conditionalshow =false;
                if (uiModel.ContextType == UIModelContextTypes.EditForm || uiModel.ContextType == UIModelContextTypes.DisplayForm ||  uiModel.ContextType == UIModelContextTypes.GenericForm )
                    conditionalshow = true;
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidRoute"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidRoute")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 100,
																	
					CustomProperties = customProperties,

                    PropertyName = "GuidRoute",

					 MaxLength = 0,
					IsRequired = true ,
					IsHidden = true,
                    SystemProperty =  SystemProperties.Identifier ,
					IsDefaultProperty = false,
                    SortBy = "GuidRoute",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBRouteResources.GUIDROUTE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Title"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Title")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 101,
																	
					CustomProperties = customProperties,

                    PropertyName = "Title",

					 MaxLength = 255,
					 Nullable = true,
					IsDefaultProperty = true,
                    SortBy = "Title",
					
	
                    TypeName = "String",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBRouteResources.TITLE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 102,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedDate",
					
	
				SystemProperty = SystemProperties.CreatedDate ,
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBRouteResources.CREATEDDATE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 114,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedDate",
					
	
					IsUpdatedDate = true,
					SystemProperty = SystemProperties.UpdatedDate ,
	
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    ,PropertyDisplayName = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.UPDATED

                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 104,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedBy",
					
	
				SystemProperty = SystemProperties.CreatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBRouteResources.CREATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 105,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedBy",
					
	
				SystemProperty = SystemProperties.UpdatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBRouteResources.UPDATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Bytes"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Bytes")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 106,
																	
					CustomProperties = customProperties,

                    PropertyName = "Bytes",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Bytes",
					
	
				SystemProperty = SystemProperties.SizeBytes,
                    TypeName = "Int32",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.YBRouteResources.BYTES*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("RouteLocations"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"YBRoute" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="RouteLocations.GuidRouteLocation")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"RouteLocations.GuidRouteLocation" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"RouteLocations" });
			

	
	//fk_RouteLocation_Route
		//if (this.Request.QueryString["fk"] != "RouteLocations")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 107,
																
					Link = VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/RouteLocations/ListViewGen?overrideModule=" + GetOverrideApp()  + "&pal=False&es=False&pag=10&filterlinks=1&idTab=RouteLocations&fk=YBRoute&startFilter="+ (new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext)).Encode("it.YBRoute.GuidRoute = Guid(\"" + idYBRoute +"\")")+ "&fkValue=" + idYBRoute,
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "RouteLocation",
					
					CustomProperties = customProperties,

                    PropertyName = "RouteLocations",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "RouteLocations.OrderRoute",
					
	
                    TypeName = "MBKYellowBoxModel.RouteLocation",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = true,
                    PathName = "MBKYellowBox/RouteLocations"
                    /*,PropertyDisplayName = Resources.YBRouteResources.ROUTELOCATIONS*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Transports"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"YBRoute" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="Transports.GuidTrasport")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"Transports.GuidTrasport" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"Transports" });
			

	
	//fk_Transport_Route
		//if (this.Request.QueryString["fk"] != "Transports")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 108,
																
					Link = VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/Transports/ListViewGen?overrideModule=" + GetOverrideApp()  + "&pal=False&es=False&pag=10&filterlinks=1&idTab=Transports&fk=YBRoute&startFilter="+ (new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext)).Encode("it.YBRoute.GuidRoute = Guid(\"" + idYBRoute +"\")")+ "&fkValue=" + idYBRoute,
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "Transport",
					
					CustomProperties = customProperties,

                    PropertyName = "Transports",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Transports.TransportCode",
					
	
                    TypeName = "MBKYellowBoxModel.Transport",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = true,
                    PathName = "MBKYellowBox/Transports"
                    /*,PropertyDisplayName = Resources.YBRouteResources.TRANSPORTS*/
                });
		//	}
	
	}
	
				
                    _properties = results;
                    return _properties;
                }
                else {
                    return _properties;
                }
            }

		protected override  UIModel<YBRouteModel> GetByForShow(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, params  object[] extraParams)
        {
			if (Request != null )
				if (!string.IsNullOrEmpty(Request.QueryString["q"]))
					filter = filter + HttpUtility.UrlDecode(Request.QueryString["q"]);
 if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
            }
            var bos = BR.YBRoutesBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), contextRequest, extraParams);
			//var bos = BR.YBRoutesBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), context, extraParams);
            YBRouteModel model = null;
            List<YBRouteModel> results = new List<YBRouteModel>();
            foreach (var item in bos)
            {
                model = new YBRouteModel();
				model.Bind(item);
				results.Add(model);
            }
            //return results;
			UIModel<YBRouteModel> uiModel = GetContextModel(UIModelContextTypes.Items, null);
            uiModel.Items = results;
			if (Request != null){
				if (SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(Request.RequestContext, "action") == "Download")
				{
					uiModel.ContextType = UIModelContextTypes.ExportDownload;
				}
			}
            Showing(ref uiModel);
            return uiModel;
		}			
		
		//public List<YBRouteModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params  object[] extraParams)
        //{
		//	var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, null, extraParams);
        public override List<YBRouteModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest,  params  object[] extraParams)
        {
            var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
           
            return uiModel.Items;
		
        }
		/*
        [MyAuthorize("r", "YBRoute", "MBKYellowBox", typeof(YBRoutesController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir)
        {
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir);
        }*/

		  [MyAuthorize("r", "YBRoute", "MBKYellowBox", typeof(YBRoutesController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir,ContextRequest contextRequest,  object[] extraParams)
        {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir,contextRequest, extraParams);
        }
/*		  [MyAuthorize("r", "YBRoute", "MBKYellowBox", typeof(YBRoutesController))]
       public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJsonBase(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }*/
		[MyAuthorize()]
		public int GetByCount(string filter, ContextRequest contextRequest) {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
            return BR.YBRoutesBR.Instance.GetCount(HttpUtility.UrlDecode(filter), GetUseMode(), contextRequest);
        }
		

		[MyAuthorize("r", "YBRoute", "MBKYellowBox", typeof(YBRoutesController))]
        public ActionResult GetByKeyJson(string id, ContextRequest contextRequest,  bool dec = false)
        {
            return Json(GetByKey(id, null, contextRequest, dec), JsonRequestBehavior.AllowGet);
        }
		public YBRouteModel GetByKey(string id) {
			return GetByKey(id, null,null, false);
       	}
		    public YBRouteModel GetByKey(string id, string includes)
        {
            return GetByKey(id, includes, false);
        }
		 public  YBRouteModel GetByKey(string id, string includes, ContextRequest contextRequest)
        {
            return GetByKey(id, includes, contextRequest, false);
        }
		/*
		  public ActionResult ShowField(string fieldName, string idField) {
		   string safePropertyName = fieldName;
              if (fieldName.StartsWith("Fk"))
              {
                  safePropertyName = fieldName.Substring(2, fieldName.Length - 2);
              }

             YBRouteModel model = new  YBRouteModel();

            UIModel uiModel = GetUIModel(model, new string[] { "NoField-" });
			
				uiModel.Properties = GetProperties(uiModel, safePropertyName);
		uiModel.Properties.ForEach(p=> p.ContextType = uiModel.ContextType );
            uiModel.ContextType = UIModelContextTypes.FilterFields;
            uiModel.OverrideApp = GetOverrideApp();
            uiModel.UseMode = GetUseMode();

            ViewData["uiModel"] = uiModel;
			var prop = uiModel.Properties.FirstOrDefault(p=>p.PropertyName == safePropertyName);
            //if (prop.IsNavigationProperty && prop.IsNavigationPropertyMany == false)
            //{
            //    ViewData["currentProperty"] = uiModel.Properties.FirstOrDefault(p => p.PropertyName != fieldName + "Text");
            //}else if (prop.IsNavigationProperty == false){
                ViewData["currentProperty"] = prop;
           // }
            ((PropertyDefinition)ViewData["currentProperty"]).RemoveLayout = true;
			ViewData["withContainer"] = false;


            return PartialView("GenericField", model);


        }
      */
	public YBRouteModel GetByKey(string id, ContextRequest contextRequest, bool dec)
        {
            return GetByKey(id, null, contextRequest, dec);
        }
        public YBRouteModel GetByKey(string id, string  includes, bool dec)
        {
            return GetByKey(id, includes, null, dec);
        }

        public YBRouteModel GetByKey(string id, string includes, ContextRequest contextRequest, bool dec) {
		             YBRouteModel model = null;
            ControllerEventArgs<YBRouteModel> e = null;
			string objectKey = id.Replace("-","/");
             OnGettingByKey(this, e=  new ControllerEventArgs<YBRouteModel>() { Id = objectKey  });
             bool cancel = false;
             YBRouteModel eItem = null;
             if (e != null)
             {
                 cancel = e.Cancel;
                 eItem = e.Item;
             }
			if (cancel == false && eItem == null)
             {
			Guid guidRoute = Guid.Empty; //new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, "GuidRoute"));
			if (dec)
                 {
                     guidRoute = new Guid(id);
                 }
                 else
                 {
                     guidRoute = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, null));
                 }
			
            
				model = new YBRouteModel();
                  if (contextRequest == null)
                {
                    contextRequest = GetContextRequest();
                }
				var bo = BR.YBRoutesBR.Instance.GetByKey(guidRoute, GetUseMode(), contextRequest,  includes);
				 if (bo != null)
                    model.Bind(bo);
                else
                    return null;
			}
             else {
                 model = eItem;
             }
			model.IsNew = false;

            return model;
        }
        // GET: /YBRoutes/DetailsGen/5
		[MyAuthorize("r", "YBRoute", "MBKYellowBox", typeof(YBRoutesController))]
        public ActionResult DetailsGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = YBRouteResources.ENTITY_PLURAL;
			 #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<YBRouteModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
            #endregion



			 bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null) {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
			//UIModel<YBRouteModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, decripted), decripted, guidId);
			var item = GetByKey(id, null, null, decripted);
			if (item == null)
            {
                 RouteValueDictionary rv = new RouteValueDictionary();
                string usemode = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"usemode");
                string overrideModule = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "overrideModule");
                string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");

                if(!string.IsNullOrEmpty(usemode)){
                    rv.Add("usemode", usemode);
                }
                if(!string.IsNullOrEmpty(overrideModule)){
                    rv.Add("overrideModule", overrideModule);
                }
                if (!string.IsNullOrEmpty(area))
                {
                    rv.Add("area", area);
                }

                return RedirectToAction("Index", rv);
            }
            //
            UIModel<YBRouteModel> uiModel = null;
                uiModel = GetContextModel(UIModelContextTypes.DisplayForm, item, decripted, guidId);



            MyEventArgs<UIModel<YBRouteModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<YBRouteModel>>() { Object = uiModel });

            if (me != null) {
                uiModel = me.Object;
            }
			
            Showing(ref uiModel);
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			
            //return View("DisplayGen", uiModel.Items[0]);
			return ResolveView(uiModel, uiModel.Items[0]);

        }
		[MyAuthorize("r", "YBRoute", "MBKYellowBox", typeof(YBRoutesController))]
		public ActionResult DetailsViewGen(string id)
        {

		 bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<YBRouteModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
           
			if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
 			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
           
        	 //var uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id));
			 
            bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
            {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true")
                {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
            UIModel<YBRouteModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, null, decripted), decripted, guidId);
			

            MyEventArgs<UIModel<YBRouteModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<YBRouteModel>>() { Object = uiModel });

            if (me != null)
            {
                uiModel = me.Object;
            }
            
            Showing(ref uiModel);
            return ResolveView(uiModel, uiModel.Items[0]);
        
        }
        //
        // GET: /YBRoutes/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        //
        // GET: /YBRoutes/CreateGen
		[MyAuthorize("c", "YBRoute", "MBKYellowBox", typeof(YBRoutesController))]
        public ActionResult CreateGen()
        {
			YBRouteModel model = new YBRouteModel();
            model.IsNew = true;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model);

			OnCreateShowing(this, e = new ControllerEventArgs<YBRouteModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }

             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                 ViewData["ispopup"] = true;
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                 ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                 ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

            Showing(ref me);

			return ResolveView(me, me.Items[0]);
        } 
			
		protected override UIModel<YBRouteModel> GetContextModel(UIModelContextTypes formMode, YBRouteModel model)
        {
            return GetContextModel(formMode, model, false, null);
        }
			
		 private UIModel<YBRouteModel> GetContextModel(UIModelContextTypes formMode, YBRouteModel model, bool decript, Guid ? id) {
            UIModel<YBRouteModel> me = new UIModel<YBRouteModel>(true, "YBRoutes");
			me.UseMode = GetUseMode();
			me.Controller = this;
			me.OverrideApp = GetOverrideApp();
			me.ContextType = formMode ;
			me.Id = "YBRoute";
			
            me.ModuleKey = "MBKYellowBox";

			me.ModuleNamespace = "MBK.YellowBox";
            me.EntityKey = "YBRoute";
            me.EntitySetName = "YBRoutes";

			me.AreaAction = "MBKYellowBox";
            me.ControllerAction = "YBRoutes";
            me.PropertyKeyName = "GuidRoute";

            me.Properties = GetProperties(me, decript, id);

			me.SortBy = "UpdatedDate";
			me.SortDirection = UIModelSortDirection.DESC;

 			
			if (Request != null)
            {
                string actionName = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam( Request.RequestContext, "action");
                if(actionName != null && actionName.ToLower().Contains("create") ){
                    me.IsNew = true;
                }
            }
			 #region Buttons
			 if (Request != null ){
             if (formMode == UIModelContextTypes.DisplayForm || formMode == UIModelContextTypes.EditForm || formMode == UIModelContextTypes.ListForm)
				me.ActionButtons = GetActionButtons(formMode,model != null ?(Request.QueryString["dec"] == "true" ? model.Id : model.SafeKey)  : null, "MBKYellowBox", "YBRoutes", "YBRoute", me.IsNew);

            //me.ActionButtons.Add(new ActionModel() { ActionKey = "return", Title = GlobalMessages.RETURN, Url = System.Web.VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/YBRoutes" });
			if (this.HttpContext != null &&  !this.HttpContext.SkipAuthorization){
				//antes this.HttpContext
				me.SetAction("u", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("u", "YBRoute", "MBKYellowBox"));
				me.SetAction("c", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("c", "YBRoute", "MBKYellowBox"));
				me.SetAction("d", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("d", "YBRoute", "MBKYellowBox"));
			
			}else{
				me.SetAction("u", true);
				me.SetAction("c", true);
				me.SetAction("d", true);

			}
            #endregion              
         
            switch (formMode)
            {
                case UIModelContextTypes.DisplayForm:
					//me.TitleForm = YBRouteResources.YBROUTES_DETAILS;
                    me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.MODIFY_DATA;
					 me.Properties.Where(p=>p.PropertyName  != "Id" && p.IsForeignKey == false).ToList().ForEach(p => p.IsHidden = false);

					 me.Properties.Where(p => (p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier) ).ToList().ForEach(p=> me.SetHide(p.PropertyName));

                    break;
                case UIModelContextTypes.EditForm:
				  me.Properties.Where(p=>p.SystemProperty != SystemProperties.Identifier && p.IsForeignKey == false && p.PropertyName != "Id").ToList().ForEach(p => p.IsHidden = false);

					if (model != null)
                    {
						

                        me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.SAVE_DATA;                        
                        me.ActionButtons.First(p => p.ActionKey == "c").Title = GlobalMessages.SAVE_DATA;
						if (model.IsNew ){
							//me.TitleForm = YBRouteResources.YBROUTES_ADD_NEW;
							me.ActionName = "CreateGen";
							me.Properties.RemoveAll(p => p.SystemProperty != null || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));
						}else{
							
							me.ActionName = "EditGen";

							//me.TitleForm = YBRouteResources.YBROUTES_EDIT;
							me.Properties.RemoveAll(p => p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));	
						}
						//me.Properties.Remove(me.Properties.Find(p => p.PropertyName == "UpdatedDate"));
					
					}
                    break;
                case UIModelContextTypes.FilterFields:
                    break;
                case UIModelContextTypes.GenericForm:
                    break;
                case UIModelContextTypes.Items:
				//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "Title") != null){
						me.Properties.Find(p => p.PropertyName == "Title").IsHidden = false;
					 }
					 
                    
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					 
                    
					

						 if (me.Properties.Find(p => p.PropertyName == "GuidRoute") != null){
						me.Properties.Find(p => p.PropertyName == "GuidRoute").IsHidden = false;
					 }
					 
                    
					


                  


					//}
                    break;
                case UIModelContextTypes.ListForm:
					PropertyDefinition propFinded = null;
					//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "Title") != null){
						me.Properties.Find(p => p.PropertyName == "Title").IsHidden = false;
					 }
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					
					me.PrincipalActionName = "GetByJson";
					//}
					//me.TitleForm = YBRouteResources.YBROUTES_LIST;
                    break;
                default:
                    break;
            }
            	this.SetDefaultProperties(me);
			}
			if (model != null )
            	me.Items.Add(model);
            return me;
        }
		// GET: /YBRoutes/CreateViewGen
		[MyAuthorize("c", "YBRoute", "MBKYellowBox", typeof(YBRoutesController))]
        public ActionResult CreateViewGen()
        {
				YBRouteModel model = new YBRouteModel();
            model.IsNew = true;
			e= null;
			OnCreateShowing(this, e = new ControllerEventArgs<YBRouteModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }
			
            var me = GetContextModel(UIModelContextTypes.EditForm, model);

			me.IsPartialView = true;	
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
            {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                me.Properties.Find(p => p.PropertyName == ViewData["fk"].ToString()).IsReadOnly = true;
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
			
      
            //me.Items.Add(model);
            Showing(ref me);
            return ResolveView(me, me.Items[0]);
        }
		protected override  void GettingExtraData(ref UIModel<YBRouteModel> uiModel)
        {

            MyEventArgs<UIModel<YBRouteModel>> me = null;
            OnGettingExtraData(this, me = new MyEventArgs<UIModel<YBRouteModel>>() { Object = uiModel });
            //bool maybeAnyReplaced = false; 
            if (me != null)
            {
                uiModel = me.Object;
                //maybeAnyReplaced = true;
            }
           
			bool canFill = false;
			 string query = null ;
            bool isFK = false;
			PropertyDefinition prop =null;
			var contextRequest = this.GetContextRequest();
            contextRequest.CustomParams.Add(new CustomParam() { Name="ui", Value= YBRoute.EntityName });

                        

        }
		private void Showing(ref UIModel<YBRouteModel> uiModel) {
          	
			MyEventArgs<UIModel<YBRouteModel>> me = new MyEventArgs<UIModel<YBRouteModel>>() { Object = uiModel };
			 OnVirtualLayoutSettings(this, me);


            OnShowing(this, me);

			
			if ((Request != null && Request.QueryString["allFields"] == "1") || Request == null )
			{
				me.Object.Properties.ForEach(p=> p.IsHidden = false);
            }
            if (me != null)
            {
                uiModel = me.Object;
            }
          


			 if (uiModel.ContextType == UIModelContextTypes.EditForm)
			    GettingExtraData(ref uiModel);
            ViewData["UIModel"] = uiModel;

        }
        //
        // POST: /YBRoutes/Create
		[MyAuthorize("c", "YBRoute", "MBKYellowBox", typeof(YBRoutesController))]
        [HttpPost]
		[ValidateInput(false)] 
        public ActionResult CreateGen(YBRouteModel  model,  ContextRequest contextRequest)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
		 	e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<YBRouteModel>() { Item = model });
           
		  	if (!ModelState.IsValid) {
				model.IsNew = true;
				var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
                 if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                        ViewData["ispopup"] = true;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
                    return ResolveView(me, model);
            }
            try
            {
				if (model.GuidRoute == null || model.GuidRoute.ToString().Contains("000000000"))
				model.GuidRoute = Guid.NewGuid();
	
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnCreating(this, e = new ControllerEventArgs<YBRouteModel>() { Item = model });
                if (e != null) {
                   if (e.Cancel && e.RedirectValues.Count > 0){
                        RouteValueDictionary rv = new RouteValueDictionary();
                        if (e.RedirectValues["area"] != null ){
                            rv.Add("area", e.RedirectValues["area"].ToString());
                        }
                        foreach (var item in e.RedirectValues.Where(p=>p.Key != "area" && p.Key != "controller" &&  p.Key != "action" ))
	                    {
		                    rv.Add(item.Key, item.Value);
	                    }

                        //if (e.RedirectValues["action"] != null && e.RedirectValues["controller"] != null && e.RedirectValues["area"] != null )
                        return RedirectToAction(e.RedirectValues["action"].ToString(), e.RedirectValues["controller"].ToString(), rv );


                        
                    }else if (e.Cancel && e.ActionResult != null )
                        return e.ActionResult;  
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				if (contextRequest == null || contextRequest.Company == null){
					contextRequest = GetContextRequest();
					
				}
                if (!cancel)
                	model.Bind(YBRoutesBR.Instance.Create(model.GetBusinessObject(), contextRequest ));
				OnCreated(this, e = new ControllerEventArgs<YBRouteModel>() { Item = model });
                 if (e != null )
					if (e.ActionResult != null)
                    	replaceResult = true;		
				if (!replaceResult)
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))
                    {
                        ViewData["__continue"] = true;
                    }
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"]) &&  string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
                        popupextra.Add("id", model.SafeKey);
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                       {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                            popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"area"));
                            popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                            popupextra.Add("action", actionDetails);
                       if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
                    {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))

                        {
                            var popupextra = GetRouteData();
                            popupextra.Add("id", model.SafeKey);
                            return RedirectToAction("EditViewGen", popupextra);
                        }
                        else
                        {
                            return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.ADD_DONE));
                        }
                    }        			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                        return Redirect(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]);
                    else{

							RouteValueDictionary popupextra = null; 
							if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"])){
                            popupextra = GetRouteData();
							 string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
                            if (!string.IsNullOrEmpty(area))
                                popupextra.Add("area", area);
                            
                            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"])) {
								popupextra.Add("id", model.SafeKey);
                                return RedirectToAction("EditGen", popupextra);

                            }else{
								
                            return RedirectToAction("Index", popupextra);
							}
							}else{
								return Content("ok");
							}
                        }
						 }
                else {
                    return e.ActionResult;
                    }
				}
            catch(Exception ex)
            {
					if (!string.IsNullOrEmpty(Request.QueryString["rok"]))
                {
                    throw  ex;
                }
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                model.IsNew = true;
                var me = GetContextModel(UIModelContextTypes.EditForm, model, true, model.GuidRoute);
                Showing(ref me);
                if (isPopUp)
                {
                    
                        ViewData["ispopup"] = isPopUp;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
					if (Request != null)
						return ResolveView(me, model);
					else
						return Content("ok");
            }
        }        
        //
        // GET: /YBRoutes/Edit/5 
        public ActionResult Edit(int id)
        {
            return View();
        }
			
		
		[MyAuthorize("u", "YBRoute", "MBKYellowBox", typeof(YBRoutesController))]
		[MvcSiteMapNode(Area="MBKYellowBox", Title="sss", Clickable=false, ParentKey = "MBKYellowBox_YBRoute_List")]
		public ActionResult EditGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = YBRouteResources.ENTITY_SINGLE;		 	
  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<YBRouteModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
            YBRouteModel model = null;
            // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
			bool dec = false;
            Guid ? idGuidDecripted = null ;
            if (Request != null && Request.QueryString["dec"] == "true")
            {
                dec = true;
                idGuidDecripted = Guid.Parse(id);
            }

            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model,dec,idGuidDecripted);
            Showing(ref me);


            if (!replaceResult)
            {
                 //return View("EditGen", me.Items[0]);
				 return ResolveView(me, me.Items[0]);
            }
            else {
                return e.ActionResult;
            }
        }
			[MyAuthorize("u", "YBRoute","MBKYellowBox", typeof(YBRoutesController))]
		public ActionResult EditViewGen(string id)
        {
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

					  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<YBRouteModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
			
            YBRouteModel model = null;
			 bool dec = false;
            Guid? guidId = null ;

            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null && System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                dec = true;
                guidId = Guid.Parse(id);
            }
            // si fue implementado el método parcial y no se ha decidido suspender la acción
            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
            var me = GetContextModel(UIModelContextTypes.EditForm, model, dec, guidId);
            Showing(ref me);

            return ResolveView(me, model);
        }
		[MyAuthorize("u", "YBRoute",  "MBKYellowBox", typeof(YBRoutesController))]
		[HttpPost]
		[ValidateInput(false)] 
		        public ActionResult EditGen(YBRouteModel model)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
			e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<YBRouteModel>() { Item = model });
           
            if (!ModelState.IsValid)
            {
			   	var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
			
				if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"])){
                	ViewData["ispopup"] = true;
					return ResolveView(me, model);
				}
				else
					return ResolveView(me, model);
            }
            try
            {
			
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnEditing(this, e = new ControllerEventArgs<YBRouteModel>() { Item = model });
                if (e != null) {
                    if (e.Cancel && e.ActionResult != null)
                        return e.ActionResult;
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				ContextRequest context = new ContextRequest();
                context.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;

                YBRoute resultObj = null;
			    if (!cancel)
                	resultObj = YBRoutesBR.Instance.Update(model.GetBusinessObject(), GetContextRequest());
				
				OnEdited(this, e = new ControllerEventArgs<YBRouteModel>() { Item =   new YBRouteModel(resultObj) });
				if (e != null && e.ActionResult != null) replaceResult = true; 

                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Content("ok");
                }
                else
                {
				if (!replaceResult)
                {
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"])  && string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
						 if (Request != null && Request.QueryString["dec"] == "true")
                        {
                            popupextra.Add("id", model.Id);
                        }
                        else
                        {
							popupextra.Add("id", model.SafeKey);

							
                        }
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                        {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                        popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area"));
                        popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                        popupextra.Add("action", actionDetails);
                        if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
					if (isPopUp)
						return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.UPDATE_DONE));
        			    string returnUrl = null;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"])) {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.Form["ReturnAfter"];
                        else if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"];
                    }
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else{
		RouteValueDictionary popupextra = null; 
						 if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"]))
                            {
							
							popupextra = GetRouteData();
							string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
							if (!string.IsNullOrEmpty(area))
								popupextra.Add("area", area);

							return RedirectToAction("Index", popupextra);
						}else{
							return Content("ok");
						}
						}
				 }
                else {
                    return e.ActionResult;
				}
                }		
            }
          catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
			    if (isPopUp)
                {
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(ex.Message));
                    
                }
                else
                {
                    SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                    
                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
                else {
						  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
							ViewData["ispopup"] = true;
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
							ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
							ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

						var me = GetContextModel(UIModelContextTypes.EditForm, model);
						Showing(ref me);

						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
						{
							ViewData["ispopup"] = true;
							return ResolveView(me, model);
						}
						else
							return ResolveView(me, model);

						
					}
				}
            }
        }
        //
        // POST: /YBRoutes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                //  Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /YBRoutes/Delete/5
        
		[MyAuthorize("d", "YBRoute", "MBKYellowBox", typeof(YBRoutesController))]
		[HttpDelete]
        public ActionResult DeleteGen(string objectKey, string extraParams)
        {
            try
            {
					
			
				Guid guidRoute = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey.Replace("-", "/"), "GuidRoute")); 
                BO.YBRoute entity = new BO.YBRoute() { GuidRoute = guidRoute };

                BR.YBRoutesBR.Instance.Delete(entity, GetContextRequest());               
                return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.DELETE_DONE));

            }
            catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null)
                    {
                        message = ex.Data["usermessage"].ToString();
                    }

                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }
            }
        }
		/*[MyAuthorize()]
		public FileMediaResult Download(string query, bool? allSelected = false,  string selected = null , string orderBy = null , string direction = null , string format = null , string actionKey=null )
        {
			
            List<Guid> keysSelected1 = new List<Guid>();
            if (!string.IsNullOrEmpty(selected)) {
                foreach (var keyString in selected.Split(char.Parse("|")))
                {
				
                    keysSelected1.Add(Guid.Parse(keyString));
                }
            }
				
            query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(query, allSelected.Value, selected, "GuidRoute");
            MyEventArgs<ContextActionModel<YBRouteModel>> eArgs = null;
            List<YBRouteModel> results = GetBy(query, null, null, orderBy, direction, GetContextRequest(), keysSelected1);
            OnDownloading(this, eArgs = new MyEventArgs<ContextActionModel<YBRouteModel>>() { Object = new ContextActionModel<YBRouteModel>() { Query = query, SelectedItems = results, Selected=selected, SelectAll = allSelected.Value, Direction = direction , OrderBy = orderBy, ActionKey=actionKey  } });

            if (eArgs != null)
            {
                if (eArgs.Object.Result != null)
                    return (FileMediaResult)eArgs.Object.Result;
            }
            

            return (new FeaturesController()).ExportDownload(typeof(YBRouteModel), results, format, this.GetUIPluralText("MBKYellowBox", "YBRoute"));
            
        }
			*/
		
		[HttpPost]
        public ActionResult CustomActionExecute(ContextActionModel model) {
		 try
            {
			//List<Guid> keysSelected1 = new List<Guid>();
			List<object> keysSelected1 = new List<object>();
            if (!string.IsNullOrEmpty(model.Selected))
            {
                foreach (var keyString in model.Selected.Split(char.Parse(",")))
                {
				
				keysSelected1.Add(Guid.Parse(keyString.Split(char.Parse("|"))[0]));
                        
                    

			
                }
            }
			DataAction dataAction = DataAction.GetDataAction(Request);
			 model.Selected = dataAction.Selected;

            model.Query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(dataAction.Query, dataAction.AllSelected, dataAction.Selected, "GuidRoute");
           
            
			
			#region implementaci�n de m�todo parcial
            bool replaceResult = false;
            MyEventArgs<ContextActionModel<YBRouteModel>> actionEventArgs = null;
           
			if (model.ActionKey != "deletemany" && model.ActionKey != "deleterelmany" && model.ActionKey != "updateRel" &&  model.ActionKey != "delete-relation-fk" && model.ActionKey != "restore" )
			{
				ContextRequest context = SFSdotNet.Framework.My.Context.BuildContextRequestSafe(System.Web.HttpContext.Current);
				context.UseMode = dataAction.Usemode;

				if (model.IsBackground)
				{
					System.Threading.Tasks.Task.Run(() => 
						OnCustomActionExecutingBackground(this, actionEventArgs = new MyEventArgs<ContextActionModel<YBRouteModel>>() { Object = new ContextActionModel<YBRouteModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } })
					);
				}
				else
				{
					OnCustomActionExecuting(this, actionEventArgs = new MyEventArgs<ContextActionModel<YBRouteModel>>() {  Object = new ContextActionModel<YBRouteModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } });
				}
			}
            List<YBRouteModel> results = null;
	
			if (model.ActionKey == "deletemany") { 
				
				BR.YBRoutesBR.Instance.Delete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

            }
	
			else if (model.ActionKey == "restore") {
                    BR.YBRoutesBR.Instance.UnDelete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

                }
            else if (model.ActionKey == "updateRel" || model.ActionKey == "delete-relation-fk" || model.ActionKey == "updateRel-proxyMany")
            {
               try {
                   string valueForUpdate = null;
				   string propForUpdate = null;
				   if (!string.IsNullOrEmpty(Request.Params["propertyForUpdate"])){
						propForUpdate = Request.Params["propertyForUpdate"];
				   }
				    if (string.IsNullOrEmpty(propForUpdate) && !string.IsNullOrEmpty(Request.QueryString["propertyForUpdate"]))
                   {
                       propForUpdate = Request.QueryString["propertyForUpdate"];
                   }
                    if (model.ActionKey != "delete-relation-fk")
                    {
                        valueForUpdate = Request.QueryString["valueForUpdate"];
                    }
                    BR.YBRoutesBR.Instance.UpdateAssociation(propForUpdate, valueForUpdate, model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());
					
                    if (model.ActionKey == "delete-relation-fk")
                    {
                        MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]);

                        return PartialView("ResultMessageView", message);
                    }
                    else
                    {
                        return Content("ok");
                    }
       
          
                }
                catch (Exception ex)
                {
				        SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
           
                }
            
            }
		
                if (actionEventArgs == null && !model.IsBackground)
                {
                    //if (model.ActionKey != "deletemany"  && model.ActionKey != "deleterelmany")
                    //{
                     //   throw new NotImplementedException("");
                    //}
                }
                else
                {
					if (model.IsBackground == false )
						 replaceResult = actionEventArgs.Object.Result is ActionResult /*actionEventArgs.Object.ReplaceResult*/;
                }
                #endregion
                if (!replaceResult)
                {
                    if (Request != null && Request.IsAjaxRequest())
                    {
						MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]) ;
                        if (model.IsBackground )
                            message.Message = GlobalMessages.THE_PROCESS_HAS_BEEN_STARTED;
                        
                        return PartialView("ResultMessageView", message);                    
					}
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return (ActionResult)actionEventArgs.Object.Result;
                }
            }
            catch (Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null) {
                        message = ex.Data["usermessage"].ToString();
                    }
					 SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }

            }
        }
        //
        // POST: /YBRoutes/Delete/5
        
			
	
    }
}
namespace MBK.YellowBox.Web.Mvc.Controllers
{
	using MBK.YellowBox.Web.Mvc.Models.StudentParents;

    public partial class StudentParentsController : MBK.YellowBox.Web.Mvc.ControllerBase<Models.StudentParents.StudentParentModel>
    {

       


	#region partial methods
        ControllerEventArgs<Models.StudentParents.StudentParentModel> e = null;
        partial void OnValidating(object sender, ControllerEventArgs<Models.StudentParents.StudentParentModel> e);
        partial void OnGettingExtraData(object sender, MyEventArgs<UIModel<Models.StudentParents.StudentParentModel>> e);
        partial void OnCreating(object sender, ControllerEventArgs<Models.StudentParents.StudentParentModel> e);
        partial void OnCreated(object sender, ControllerEventArgs<Models.StudentParents.StudentParentModel> e);
        partial void OnEditing(object sender, ControllerEventArgs<Models.StudentParents.StudentParentModel> e);
        partial void OnEdited(object sender, ControllerEventArgs<Models.StudentParents.StudentParentModel> e);
        partial void OnDeleting(object sender, ControllerEventArgs<Models.StudentParents.StudentParentModel> e);
        partial void OnDeleted(object sender, ControllerEventArgs<Models.StudentParents.StudentParentModel> e);
    	partial void OnShowing(object sender, MyEventArgs<UIModel<Models.StudentParents.StudentParentModel>> e);
    	partial void OnGettingByKey(object sender, ControllerEventArgs<Models.StudentParents.StudentParentModel> e);
        partial void OnTaken(object sender, ControllerEventArgs<Models.StudentParents.StudentParentModel> e);
       	partial void OnCreateShowing(object sender, ControllerEventArgs<Models.StudentParents.StudentParentModel> e);
		partial void OnEditShowing(object sender, ControllerEventArgs<Models.StudentParents.StudentParentModel> e);
		partial void OnDetailsShowing(object sender, ControllerEventArgs<Models.StudentParents.StudentParentModel> e);
 		partial void OnActionsCreated(object sender, MyEventArgs<UIModel<Models.StudentParents.StudentParentModel >> e);
		partial void OnCustomActionExecuting(object sender, MyEventArgs<ContextActionModel<Models.StudentParents.StudentParentModel>> e);
		partial void OnCustomActionExecutingBackground(object sender, MyEventArgs<ContextActionModel<Models.StudentParents.StudentParentModel>> e);
        partial void OnDownloading(object sender, MyEventArgs<ContextActionModel<Models.StudentParents.StudentParentModel>> e);
      	partial void OnAuthorization(object sender, AuthorizationContext context);
		 partial void OnFilterShowing(object sender, MyEventArgs<UIModel<Models.StudentParents.StudentParentModel >> e);
         partial void OnSummaryOperationShowing(object sender, MyEventArgs<UIModel<Models.StudentParents.StudentParentModel>> e);

        partial void OnExportActionsCreated(object sender, MyEventArgs<UIModel<Models.StudentParents.StudentParentModel>> e);


		protected override void OnVirtualFilterShowing(object sender, MyEventArgs<UIModel<StudentParentModel>> e)
        {
            OnFilterShowing(sender, e);
        }
		 public override void OnVirtualExportActionsCreated(object sender, MyEventArgs<UIModel<StudentParentModel>> e)
        {
            OnExportActionsCreated(sender, e);
        }
        public override void OnVirtualDownloading(object sender, MyEventArgs<ContextActionModel<StudentParentModel>> e)
        {
            OnDownloading(sender, e);
        }
        public override void OnVirtualShowing(object sender, MyEventArgs<UIModel<StudentParentModel>> e)
        {
            OnShowing(sender, e);
        }

	#endregion
	#region API
	 public override ActionResult ApiCreateGen(StudentParentModel model, ContextRequest contextRequest)
        {
            return CreateGen(model, contextRequest);
        }

              public override ActionResult ApiGetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJson(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }
        public override ActionResult ApiGetByKeyJson(string id, ContextRequest contextRequest)
        {
            return  GetByKeyJson(id, contextRequest, true);
        }
      
		 public override int ApiGetByCount(string filter, ContextRequest contextRequest)
        {
            return GetByCount(filter, contextRequest);
        }
         protected override ActionResult ApiDeleteGen(List<StudentParentModel> models, ContextRequest contextRequest)
        {
            List<StudentParent> objs = new List<StudentParent>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                BR.StudentParentsBR.Instance.DeleteBulk(objs, contextRequest);
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        protected override ActionResult ApiUpdateGen(List<StudentParentModel> models, ContextRequest contextRequest)
        {
            List<StudentParent> objs = new List<StudentParent>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                foreach (var obj in objs)
                {
                    BR.StudentParentsBR.Instance.Update(obj, contextRequest);

                }
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }


	#endregion
#region Validation methods	
	    private void Validations(StudentParentModel model) { 
            #region Remote validations

            #endregion
		}

#endregion
		
 		public AuthorizationContext Authorization(AuthorizationContext context)
        {
            OnAuthorization(this,  context );
            return context ;
        }
		public List<StudentParentModel> GetAll() {
            			var bos = BR.StudentParentsBR.Instance.GetBy("",
					new SFSdotNet.Framework.My.ContextRequest()
					{
						CustomQuery = new SFSdotNet.Framework.My.CustomQuery()
						{
							OrderBy = "FullName",
							SortDirection = SFSdotNet.Framework.Data.SortDirection.Ascending
						}
					});
            			List<StudentParentModel> results = new List<StudentParentModel>();
            StudentParentModel model = null;
            foreach (var bo in bos)
            {
                model = new StudentParentModel();
                model.Bind(bo);
                results.Add(model);
            }
            return results;

        }
        //
        // GET: /StudentParents/
		[MyAuthorize("r", "StudentParent", "MBKYellowBox", typeof(StudentParentsController))]
		public ActionResult Index()
        {
    		var uiModel = GetContextModel(UIModelContextTypes.ListForm, null);
			ViewBag.UIModel = uiModel;
			uiModel.FilterStart = (string)ViewData["startFilter"];
                    MyEventArgs<UIModel<StudentParentModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<StudentParentModel>>() { Object = uiModel });

			OnExportActionsCreated(this, (me != null ? me : me = new MyEventArgs<UIModel<StudentParentModel>>() { Object = uiModel }));

            if (me != null)
            {
                uiModel = me.Object;
            }
            if (me == null)
                me = new MyEventArgs<UIModel<StudentParentModel>>() { Object = uiModel };
           
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;


            //return View("ListGen");
			return ResolveView(uiModel);
        }
		[MyAuthorize("r", "StudentParent", "MBKYellowBox", typeof(StudentParentsController))]
		public ActionResult ListViewGen(string idTab, string fk , string fkValue, string startFilter, ListModes  listmode  = ListModes.SimpleList, PropertyDefinition parentRelationProperty = null, object parentRelationPropertyValue = null )
        {
			ViewData["idTab"] = System.Web.HttpContext.Current.Request.QueryString["idTab"]; 
		 	ViewData["detpop"] = true; // details in popup
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"])) {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"]; 
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
            {
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["startFilter"]))
            {
                ViewData["startFilter"] = Request.QueryString["startFilter"];
            }
			
			UIModel<StudentParentModel> uiModel = GetContextModel(UIModelContextTypes.ListForm, null);

            MyEventArgs<UIModel<StudentParentModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<StudentParentModel>>() { Object = uiModel });
            if (me == null)
                me = new MyEventArgs<UIModel<StudentParentModel>>() { Object = uiModel };
            uiModel.Properties = GetProperties(uiModel);
            uiModel.ContextType = UIModelContextTypes.ListForm;
             uiModel.FilterStart = (string)ViewData["startFilter"];
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;
			 if (listmode == ListModes.SimpleList)
                return ResolveView(uiModel);
            else
            {
                ViewData["parentRelationProperty"] = parentRelationProperty;
                ViewData["parentRelationPropertyValue"] = parentRelationPropertyValue;
                return PartialView("ListForTagSelectView");
            }
            return ResolveView(uiModel);
        }
		List<PropertyDefinition> _properties = null;

		 protected override List<PropertyDefinition> GetProperties(UIModel uiModel,  params string[] specificProperties)
        { 
            return GetProperties(uiModel, false, null, specificProperties);
        }

		protected override List<PropertyDefinition> GetProperties(UIModel uiModel, bool decripted, Guid? id, params string[] specificProperties)
            {

			bool allProperties = true;    
                if (specificProperties != null && specificProperties.Length > 0)
                {
                    allProperties = false;
                }


			List<CustomProperty> customProperties = new List<CustomProperty>();
			if (_properties == null)
                {
                List<PropertyDefinition> results = new List<PropertyDefinition>();

			string idStudentParent = GetRouteDataOrQueryParam("id");
			if (idStudentParent != null)
			{
				if (!decripted)
                {
					idStudentParent = SFSdotNet.Framework.Entities.Utils.GetPropertyKey(idStudentParent.Replace("-","/"), "GuidStudentParent");
				}else{
					if (id != null )
						idStudentParent = id.Value.ToString();                

				}
			}

			bool visibleProperty = true;	
			 bool conditionalshow =false;
                if (uiModel.ContextType == UIModelContextTypes.EditForm || uiModel.ContextType == UIModelContextTypes.DisplayForm ||  uiModel.ContextType == UIModelContextTypes.GenericForm )
                    conditionalshow = true;
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidStudentParent"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidStudentParent")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 100,
																	
					CustomProperties = customProperties,

                    PropertyName = "GuidStudentParent",

					 MaxLength = 0,
					IsRequired = true ,
					IsHidden = true,
                    SystemProperty =  SystemProperties.Identifier ,
					IsDefaultProperty = false,
                    SortBy = "GuidStudentParent",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentParentResources.GUIDSTUDENTPARENT*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("FullName"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "FullName")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 101,
																	
					CustomProperties = customProperties,

                    PropertyName = "FullName",

					 MaxLength = 255,
					 Nullable = true,
					IsDefaultProperty = true,
                    SortBy = "FullName",
					
	
                    TypeName = "String",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentParentResources.FULLNAME*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 102,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedDate",
					
	
				SystemProperty = SystemProperties.CreatedDate ,
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentParentResources.CREATEDDATE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 113,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedDate",
					
	
					IsUpdatedDate = true,
					SystemProperty = SystemProperties.UpdatedDate ,
	
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    ,PropertyDisplayName = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.UPDATED

                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 104,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedBy",
					
	
				SystemProperty = SystemProperties.CreatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentParentResources.CREATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 105,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedBy",
					
	
				SystemProperty = SystemProperties.UpdatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentParentResources.UPDATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Bytes"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Bytes")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 106,
																	
					CustomProperties = customProperties,

                    PropertyName = "Bytes",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Bytes",
					
	
				SystemProperty = SystemProperties.SizeBytes,
                    TypeName = "Int32",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.StudentParentResources.BYTES*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Students"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"StudentParent" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="Students.GuidStudent")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"Students.GuidStudent" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"Students" });
			

	
	//fk_Student_Parent
		//if (this.Request.QueryString["fk"] != "Students")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 107,
																
					Link = VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/Students/ListViewGen?overrideModule=" + GetOverrideApp()  + "&pal=False&es=False&pag=10&filterlinks=1&idTab=Students&fk=StudentParent&startFilter="+ (new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext)).Encode("it.StudentParent.GuidStudentParent = Guid(\"" + idStudentParent +"\")")+ "&fkValue=" + idStudentParent,
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "Student",
					
					CustomProperties = customProperties,

                    PropertyName = "Students",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Students.FullName",
					
	
                    TypeName = "MBKYellowBoxModel.Student",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = true,
                    PathName = "MBKYellowBox/Students"
                    /*,PropertyDisplayName = Resources.StudentParentResources.STUDENTS*/
                });
		//	}
	
	}
	
				
                    _properties = results;
                    return _properties;
                }
                else {
                    return _properties;
                }
            }

		protected override  UIModel<StudentParentModel> GetByForShow(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, params  object[] extraParams)
        {
			if (Request != null )
				if (!string.IsNullOrEmpty(Request.QueryString["q"]))
					filter = filter + HttpUtility.UrlDecode(Request.QueryString["q"]);
 if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
            }
            var bos = BR.StudentParentsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), contextRequest, extraParams);
			//var bos = BR.StudentParentsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), context, extraParams);
            StudentParentModel model = null;
            List<StudentParentModel> results = new List<StudentParentModel>();
            foreach (var item in bos)
            {
                model = new StudentParentModel();
				model.Bind(item);
				results.Add(model);
            }
            //return results;
			UIModel<StudentParentModel> uiModel = GetContextModel(UIModelContextTypes.Items, null);
            uiModel.Items = results;
			if (Request != null){
				if (SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(Request.RequestContext, "action") == "Download")
				{
					uiModel.ContextType = UIModelContextTypes.ExportDownload;
				}
			}
            Showing(ref uiModel);
            return uiModel;
		}			
		
		//public List<StudentParentModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params  object[] extraParams)
        //{
		//	var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, null, extraParams);
        public override List<StudentParentModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest,  params  object[] extraParams)
        {
            var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
           
            return uiModel.Items;
		
        }
		/*
        [MyAuthorize("r", "StudentParent", "MBKYellowBox", typeof(StudentParentsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir)
        {
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir);
        }*/

		  [MyAuthorize("r", "StudentParent", "MBKYellowBox", typeof(StudentParentsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir,ContextRequest contextRequest,  object[] extraParams)
        {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir,contextRequest, extraParams);
        }
/*		  [MyAuthorize("r", "StudentParent", "MBKYellowBox", typeof(StudentParentsController))]
       public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJsonBase(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }*/
		[MyAuthorize()]
		public int GetByCount(string filter, ContextRequest contextRequest) {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
            return BR.StudentParentsBR.Instance.GetCount(HttpUtility.UrlDecode(filter), GetUseMode(), contextRequest);
        }
		

		[MyAuthorize("r", "StudentParent", "MBKYellowBox", typeof(StudentParentsController))]
        public ActionResult GetByKeyJson(string id, ContextRequest contextRequest,  bool dec = false)
        {
            return Json(GetByKey(id, null, contextRequest, dec), JsonRequestBehavior.AllowGet);
        }
		public StudentParentModel GetByKey(string id) {
			return GetByKey(id, null,null, false);
       	}
		    public StudentParentModel GetByKey(string id, string includes)
        {
            return GetByKey(id, includes, false);
        }
		 public  StudentParentModel GetByKey(string id, string includes, ContextRequest contextRequest)
        {
            return GetByKey(id, includes, contextRequest, false);
        }
		/*
		  public ActionResult ShowField(string fieldName, string idField) {
		   string safePropertyName = fieldName;
              if (fieldName.StartsWith("Fk"))
              {
                  safePropertyName = fieldName.Substring(2, fieldName.Length - 2);
              }

             StudentParentModel model = new  StudentParentModel();

            UIModel uiModel = GetUIModel(model, new string[] { "NoField-" });
			
				uiModel.Properties = GetProperties(uiModel, safePropertyName);
		uiModel.Properties.ForEach(p=> p.ContextType = uiModel.ContextType );
            uiModel.ContextType = UIModelContextTypes.FilterFields;
            uiModel.OverrideApp = GetOverrideApp();
            uiModel.UseMode = GetUseMode();

            ViewData["uiModel"] = uiModel;
			var prop = uiModel.Properties.FirstOrDefault(p=>p.PropertyName == safePropertyName);
            //if (prop.IsNavigationProperty && prop.IsNavigationPropertyMany == false)
            //{
            //    ViewData["currentProperty"] = uiModel.Properties.FirstOrDefault(p => p.PropertyName != fieldName + "Text");
            //}else if (prop.IsNavigationProperty == false){
                ViewData["currentProperty"] = prop;
           // }
            ((PropertyDefinition)ViewData["currentProperty"]).RemoveLayout = true;
			ViewData["withContainer"] = false;


            return PartialView("GenericField", model);


        }
      */
	public StudentParentModel GetByKey(string id, ContextRequest contextRequest, bool dec)
        {
            return GetByKey(id, null, contextRequest, dec);
        }
        public StudentParentModel GetByKey(string id, string  includes, bool dec)
        {
            return GetByKey(id, includes, null, dec);
        }

        public StudentParentModel GetByKey(string id, string includes, ContextRequest contextRequest, bool dec) {
		             StudentParentModel model = null;
            ControllerEventArgs<StudentParentModel> e = null;
			string objectKey = id.Replace("-","/");
             OnGettingByKey(this, e=  new ControllerEventArgs<StudentParentModel>() { Id = objectKey  });
             bool cancel = false;
             StudentParentModel eItem = null;
             if (e != null)
             {
                 cancel = e.Cancel;
                 eItem = e.Item;
             }
			if (cancel == false && eItem == null)
             {
			Guid guidStudentParent = Guid.Empty; //new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, "GuidStudentParent"));
			if (dec)
                 {
                     guidStudentParent = new Guid(id);
                 }
                 else
                 {
                     guidStudentParent = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, null));
                 }
			
            
				model = new StudentParentModel();
                  if (contextRequest == null)
                {
                    contextRequest = GetContextRequest();
                }
				var bo = BR.StudentParentsBR.Instance.GetByKey(guidStudentParent, GetUseMode(), contextRequest,  includes);
				 if (bo != null)
                    model.Bind(bo);
                else
                    return null;
			}
             else {
                 model = eItem;
             }
			model.IsNew = false;

            return model;
        }
        // GET: /StudentParents/DetailsGen/5
		[MyAuthorize("r", "StudentParent", "MBKYellowBox", typeof(StudentParentsController))]
        public ActionResult DetailsGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = StudentParentResources.ENTITY_PLURAL;
			 #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<StudentParentModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
            #endregion



			 bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null) {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
			//UIModel<StudentParentModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, decripted), decripted, guidId);
			var item = GetByKey(id, null, null, decripted);
			if (item == null)
            {
                 RouteValueDictionary rv = new RouteValueDictionary();
                string usemode = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"usemode");
                string overrideModule = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "overrideModule");
                string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");

                if(!string.IsNullOrEmpty(usemode)){
                    rv.Add("usemode", usemode);
                }
                if(!string.IsNullOrEmpty(overrideModule)){
                    rv.Add("overrideModule", overrideModule);
                }
                if (!string.IsNullOrEmpty(area))
                {
                    rv.Add("area", area);
                }

                return RedirectToAction("Index", rv);
            }
            //
            UIModel<StudentParentModel> uiModel = null;
                uiModel = GetContextModel(UIModelContextTypes.DisplayForm, item, decripted, guidId);



            MyEventArgs<UIModel<StudentParentModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<StudentParentModel>>() { Object = uiModel });

            if (me != null) {
                uiModel = me.Object;
            }
			
            Showing(ref uiModel);
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			
            //return View("DisplayGen", uiModel.Items[0]);
			return ResolveView(uiModel, uiModel.Items[0]);

        }
		[MyAuthorize("r", "StudentParent", "MBKYellowBox", typeof(StudentParentsController))]
		public ActionResult DetailsViewGen(string id)
        {

		 bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<StudentParentModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
           
			if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
 			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
           
        	 //var uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id));
			 
            bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
            {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true")
                {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
            UIModel<StudentParentModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, null, decripted), decripted, guidId);
			

            MyEventArgs<UIModel<StudentParentModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<StudentParentModel>>() { Object = uiModel });

            if (me != null)
            {
                uiModel = me.Object;
            }
            
            Showing(ref uiModel);
            return ResolveView(uiModel, uiModel.Items[0]);
        
        }
        //
        // GET: /StudentParents/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        //
        // GET: /StudentParents/CreateGen
		[MyAuthorize("c", "StudentParent", "MBKYellowBox", typeof(StudentParentsController))]
        public ActionResult CreateGen()
        {
			StudentParentModel model = new StudentParentModel();
            model.IsNew = true;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model);

			OnCreateShowing(this, e = new ControllerEventArgs<StudentParentModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }

             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                 ViewData["ispopup"] = true;
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                 ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                 ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

            Showing(ref me);

			return ResolveView(me, me.Items[0]);
        } 
			
		protected override UIModel<StudentParentModel> GetContextModel(UIModelContextTypes formMode, StudentParentModel model)
        {
            return GetContextModel(formMode, model, false, null);
        }
			
		 private UIModel<StudentParentModel> GetContextModel(UIModelContextTypes formMode, StudentParentModel model, bool decript, Guid ? id) {
            UIModel<StudentParentModel> me = new UIModel<StudentParentModel>(true, "StudentParents");
			me.UseMode = GetUseMode();
			me.Controller = this;
			me.OverrideApp = GetOverrideApp();
			me.ContextType = formMode ;
			me.Id = "StudentParent";
			
            me.ModuleKey = "MBKYellowBox";

			me.ModuleNamespace = "MBK.YellowBox";
            me.EntityKey = "StudentParent";
            me.EntitySetName = "StudentParents";

			me.AreaAction = "MBKYellowBox";
            me.ControllerAction = "StudentParents";
            me.PropertyKeyName = "GuidStudentParent";

            me.Properties = GetProperties(me, decript, id);

			me.SortBy = "UpdatedDate";
			me.SortDirection = UIModelSortDirection.DESC;

 			
			if (Request != null)
            {
                string actionName = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam( Request.RequestContext, "action");
                if(actionName != null && actionName.ToLower().Contains("create") ){
                    me.IsNew = true;
                }
            }
			 #region Buttons
			 if (Request != null ){
             if (formMode == UIModelContextTypes.DisplayForm || formMode == UIModelContextTypes.EditForm || formMode == UIModelContextTypes.ListForm)
				me.ActionButtons = GetActionButtons(formMode,model != null ?(Request.QueryString["dec"] == "true" ? model.Id : model.SafeKey)  : null, "MBKYellowBox", "StudentParents", "StudentParent", me.IsNew);

            //me.ActionButtons.Add(new ActionModel() { ActionKey = "return", Title = GlobalMessages.RETURN, Url = System.Web.VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/StudentParents" });
			if (this.HttpContext != null &&  !this.HttpContext.SkipAuthorization){
				//antes this.HttpContext
				me.SetAction("u", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("u", "StudentParent", "MBKYellowBox"));
				me.SetAction("c", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("c", "StudentParent", "MBKYellowBox"));
				me.SetAction("d", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("d", "StudentParent", "MBKYellowBox"));
			
			}else{
				me.SetAction("u", true);
				me.SetAction("c", true);
				me.SetAction("d", true);

			}
            #endregion              
         
            switch (formMode)
            {
                case UIModelContextTypes.DisplayForm:
					//me.TitleForm = StudentParentResources.STUDENTPARENTS_DETAILS;
                    me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.MODIFY_DATA;
					 me.Properties.Where(p=>p.PropertyName  != "Id" && p.IsForeignKey == false).ToList().ForEach(p => p.IsHidden = false);

					 me.Properties.Where(p => (p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier) ).ToList().ForEach(p=> me.SetHide(p.PropertyName));

                    break;
                case UIModelContextTypes.EditForm:
				  me.Properties.Where(p=>p.SystemProperty != SystemProperties.Identifier && p.IsForeignKey == false && p.PropertyName != "Id").ToList().ForEach(p => p.IsHidden = false);

					if (model != null)
                    {
						

                        me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.SAVE_DATA;                        
                        me.ActionButtons.First(p => p.ActionKey == "c").Title = GlobalMessages.SAVE_DATA;
						if (model.IsNew ){
							//me.TitleForm = StudentParentResources.STUDENTPARENTS_ADD_NEW;
							me.ActionName = "CreateGen";
							me.Properties.RemoveAll(p => p.SystemProperty != null || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));
						}else{
							
							me.ActionName = "EditGen";

							//me.TitleForm = StudentParentResources.STUDENTPARENTS_EDIT;
							me.Properties.RemoveAll(p => p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));	
						}
						//me.Properties.Remove(me.Properties.Find(p => p.PropertyName == "UpdatedDate"));
					
					}
                    break;
                case UIModelContextTypes.FilterFields:
                    break;
                case UIModelContextTypes.GenericForm:
                    break;
                case UIModelContextTypes.Items:
				//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "FullName") != null){
						me.Properties.Find(p => p.PropertyName == "FullName").IsHidden = false;
					 }
					 
                    
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					 
                    
					

						 if (me.Properties.Find(p => p.PropertyName == "GuidStudentParent") != null){
						me.Properties.Find(p => p.PropertyName == "GuidStudentParent").IsHidden = false;
					 }
					 
                    
					


                  


					//}
                    break;
                case UIModelContextTypes.ListForm:
					PropertyDefinition propFinded = null;
					//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "FullName") != null){
						me.Properties.Find(p => p.PropertyName == "FullName").IsHidden = false;
					 }
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					
					me.PrincipalActionName = "GetByJson";
					//}
					//me.TitleForm = StudentParentResources.STUDENTPARENTS_LIST;
                    break;
                default:
                    break;
            }
            	this.SetDefaultProperties(me);
			}
			if (model != null )
            	me.Items.Add(model);
            return me;
        }
		// GET: /StudentParents/CreateViewGen
		[MyAuthorize("c", "StudentParent", "MBKYellowBox", typeof(StudentParentsController))]
        public ActionResult CreateViewGen()
        {
				StudentParentModel model = new StudentParentModel();
            model.IsNew = true;
			e= null;
			OnCreateShowing(this, e = new ControllerEventArgs<StudentParentModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }
			
            var me = GetContextModel(UIModelContextTypes.EditForm, model);

			me.IsPartialView = true;	
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
            {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                me.Properties.Find(p => p.PropertyName == ViewData["fk"].ToString()).IsReadOnly = true;
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
			
      
            //me.Items.Add(model);
            Showing(ref me);
            return ResolveView(me, me.Items[0]);
        }
		protected override  void GettingExtraData(ref UIModel<StudentParentModel> uiModel)
        {

            MyEventArgs<UIModel<StudentParentModel>> me = null;
            OnGettingExtraData(this, me = new MyEventArgs<UIModel<StudentParentModel>>() { Object = uiModel });
            //bool maybeAnyReplaced = false; 
            if (me != null)
            {
                uiModel = me.Object;
                //maybeAnyReplaced = true;
            }
           
			bool canFill = false;
			 string query = null ;
            bool isFK = false;
			PropertyDefinition prop =null;
			var contextRequest = this.GetContextRequest();
            contextRequest.CustomParams.Add(new CustomParam() { Name="ui", Value= StudentParent.EntityName });

                        

        }
		private void Showing(ref UIModel<StudentParentModel> uiModel) {
          	
			MyEventArgs<UIModel<StudentParentModel>> me = new MyEventArgs<UIModel<StudentParentModel>>() { Object = uiModel };
			 OnVirtualLayoutSettings(this, me);


            OnShowing(this, me);

			
			if ((Request != null && Request.QueryString["allFields"] == "1") || Request == null )
			{
				me.Object.Properties.ForEach(p=> p.IsHidden = false);
            }
            if (me != null)
            {
                uiModel = me.Object;
            }
          


			 if (uiModel.ContextType == UIModelContextTypes.EditForm)
			    GettingExtraData(ref uiModel);
            ViewData["UIModel"] = uiModel;

        }
        //
        // POST: /StudentParents/Create
		[MyAuthorize("c", "StudentParent", "MBKYellowBox", typeof(StudentParentsController))]
        [HttpPost]
		[ValidateInput(false)] 
        public ActionResult CreateGen(StudentParentModel  model,  ContextRequest contextRequest)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
		 	e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<StudentParentModel>() { Item = model });
           
		  	if (!ModelState.IsValid) {
				model.IsNew = true;
				var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
                 if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                        ViewData["ispopup"] = true;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
                    return ResolveView(me, model);
            }
            try
            {
				if (model.GuidStudentParent == null || model.GuidStudentParent.ToString().Contains("000000000"))
				model.GuidStudentParent = Guid.NewGuid();
	
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnCreating(this, e = new ControllerEventArgs<StudentParentModel>() { Item = model });
                if (e != null) {
                   if (e.Cancel && e.RedirectValues.Count > 0){
                        RouteValueDictionary rv = new RouteValueDictionary();
                        if (e.RedirectValues["area"] != null ){
                            rv.Add("area", e.RedirectValues["area"].ToString());
                        }
                        foreach (var item in e.RedirectValues.Where(p=>p.Key != "area" && p.Key != "controller" &&  p.Key != "action" ))
	                    {
		                    rv.Add(item.Key, item.Value);
	                    }

                        //if (e.RedirectValues["action"] != null && e.RedirectValues["controller"] != null && e.RedirectValues["area"] != null )
                        return RedirectToAction(e.RedirectValues["action"].ToString(), e.RedirectValues["controller"].ToString(), rv );


                        
                    }else if (e.Cancel && e.ActionResult != null )
                        return e.ActionResult;  
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				if (contextRequest == null || contextRequest.Company == null){
					contextRequest = GetContextRequest();
					
				}
                if (!cancel)
                	model.Bind(StudentParentsBR.Instance.Create(model.GetBusinessObject(), contextRequest ));
				OnCreated(this, e = new ControllerEventArgs<StudentParentModel>() { Item = model });
                 if (e != null )
					if (e.ActionResult != null)
                    	replaceResult = true;		
				if (!replaceResult)
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))
                    {
                        ViewData["__continue"] = true;
                    }
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"]) &&  string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
                        popupextra.Add("id", model.SafeKey);
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                       {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                            popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"area"));
                            popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                            popupextra.Add("action", actionDetails);
                       if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
                    {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))

                        {
                            var popupextra = GetRouteData();
                            popupextra.Add("id", model.SafeKey);
                            return RedirectToAction("EditViewGen", popupextra);
                        }
                        else
                        {
                            return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.ADD_DONE));
                        }
                    }        			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                        return Redirect(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]);
                    else{

							RouteValueDictionary popupextra = null; 
							if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"])){
                            popupextra = GetRouteData();
							 string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
                            if (!string.IsNullOrEmpty(area))
                                popupextra.Add("area", area);
                            
                            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"])) {
								popupextra.Add("id", model.SafeKey);
                                return RedirectToAction("EditGen", popupextra);

                            }else{
								
                            return RedirectToAction("Index", popupextra);
							}
							}else{
								return Content("ok");
							}
                        }
						 }
                else {
                    return e.ActionResult;
                    }
				}
            catch(Exception ex)
            {
					if (!string.IsNullOrEmpty(Request.QueryString["rok"]))
                {
                    throw  ex;
                }
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                model.IsNew = true;
                var me = GetContextModel(UIModelContextTypes.EditForm, model, true, model.GuidStudentParent);
                Showing(ref me);
                if (isPopUp)
                {
                    
                        ViewData["ispopup"] = isPopUp;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
					if (Request != null)
						return ResolveView(me, model);
					else
						return Content("ok");
            }
        }        
        //
        // GET: /StudentParents/Edit/5 
        public ActionResult Edit(int id)
        {
            return View();
        }
			
		
		[MyAuthorize("u", "StudentParent", "MBKYellowBox", typeof(StudentParentsController))]
		[MvcSiteMapNode(Area="MBKYellowBox", Title="sss", Clickable=false, ParentKey = "MBKYellowBox_StudentParent_List")]
		public ActionResult EditGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = StudentParentResources.ENTITY_SINGLE;		 	
  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<StudentParentModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
            StudentParentModel model = null;
            // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
			bool dec = false;
            Guid ? idGuidDecripted = null ;
            if (Request != null && Request.QueryString["dec"] == "true")
            {
                dec = true;
                idGuidDecripted = Guid.Parse(id);
            }

            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model,dec,idGuidDecripted);
            Showing(ref me);


            if (!replaceResult)
            {
                 //return View("EditGen", me.Items[0]);
				 return ResolveView(me, me.Items[0]);
            }
            else {
                return e.ActionResult;
            }
        }
			[MyAuthorize("u", "StudentParent","MBKYellowBox", typeof(StudentParentsController))]
		public ActionResult EditViewGen(string id)
        {
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

					  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<StudentParentModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
			
            StudentParentModel model = null;
			 bool dec = false;
            Guid? guidId = null ;

            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null && System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                dec = true;
                guidId = Guid.Parse(id);
            }
            // si fue implementado el método parcial y no se ha decidido suspender la acción
            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
            var me = GetContextModel(UIModelContextTypes.EditForm, model, dec, guidId);
            Showing(ref me);

            return ResolveView(me, model);
        }
		[MyAuthorize("u", "StudentParent",  "MBKYellowBox", typeof(StudentParentsController))]
		[HttpPost]
		[ValidateInput(false)] 
		        public ActionResult EditGen(StudentParentModel model)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
			e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<StudentParentModel>() { Item = model });
           
            if (!ModelState.IsValid)
            {
			   	var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
			
				if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"])){
                	ViewData["ispopup"] = true;
					return ResolveView(me, model);
				}
				else
					return ResolveView(me, model);
            }
            try
            {
			
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnEditing(this, e = new ControllerEventArgs<StudentParentModel>() { Item = model });
                if (e != null) {
                    if (e.Cancel && e.ActionResult != null)
                        return e.ActionResult;
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				ContextRequest context = new ContextRequest();
                context.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;

                StudentParent resultObj = null;
			    if (!cancel)
                	resultObj = StudentParentsBR.Instance.Update(model.GetBusinessObject(), GetContextRequest());
				
				OnEdited(this, e = new ControllerEventArgs<StudentParentModel>() { Item =   new StudentParentModel(resultObj) });
				if (e != null && e.ActionResult != null) replaceResult = true; 

                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Content("ok");
                }
                else
                {
				if (!replaceResult)
                {
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"])  && string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
						 if (Request != null && Request.QueryString["dec"] == "true")
                        {
                            popupextra.Add("id", model.Id);
                        }
                        else
                        {
							popupextra.Add("id", model.SafeKey);

							
                        }
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                        {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                        popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area"));
                        popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                        popupextra.Add("action", actionDetails);
                        if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
					if (isPopUp)
						return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.UPDATE_DONE));
        			    string returnUrl = null;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"])) {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.Form["ReturnAfter"];
                        else if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"];
                    }
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else{
		RouteValueDictionary popupextra = null; 
						 if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"]))
                            {
							
							popupextra = GetRouteData();
							string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
							if (!string.IsNullOrEmpty(area))
								popupextra.Add("area", area);

							return RedirectToAction("Index", popupextra);
						}else{
							return Content("ok");
						}
						}
				 }
                else {
                    return e.ActionResult;
				}
                }		
            }
          catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
			    if (isPopUp)
                {
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(ex.Message));
                    
                }
                else
                {
                    SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                    
                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
                else {
						  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
							ViewData["ispopup"] = true;
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
							ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
							ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

						var me = GetContextModel(UIModelContextTypes.EditForm, model);
						Showing(ref me);

						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
						{
							ViewData["ispopup"] = true;
							return ResolveView(me, model);
						}
						else
							return ResolveView(me, model);

						
					}
				}
            }
        }
        //
        // POST: /StudentParents/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                //  Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /StudentParents/Delete/5
        
		[MyAuthorize("d", "StudentParent", "MBKYellowBox", typeof(StudentParentsController))]
		[HttpDelete]
        public ActionResult DeleteGen(string objectKey, string extraParams)
        {
            try
            {
					
			
				Guid guidStudentParent = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey.Replace("-", "/"), "GuidStudentParent")); 
                BO.StudentParent entity = new BO.StudentParent() { GuidStudentParent = guidStudentParent };

                BR.StudentParentsBR.Instance.Delete(entity, GetContextRequest());               
                return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.DELETE_DONE));

            }
            catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null)
                    {
                        message = ex.Data["usermessage"].ToString();
                    }

                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }
            }
        }
		/*[MyAuthorize()]
		public FileMediaResult Download(string query, bool? allSelected = false,  string selected = null , string orderBy = null , string direction = null , string format = null , string actionKey=null )
        {
			
            List<Guid> keysSelected1 = new List<Guid>();
            if (!string.IsNullOrEmpty(selected)) {
                foreach (var keyString in selected.Split(char.Parse("|")))
                {
				
                    keysSelected1.Add(Guid.Parse(keyString));
                }
            }
				
            query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(query, allSelected.Value, selected, "GuidStudentParent");
            MyEventArgs<ContextActionModel<StudentParentModel>> eArgs = null;
            List<StudentParentModel> results = GetBy(query, null, null, orderBy, direction, GetContextRequest(), keysSelected1);
            OnDownloading(this, eArgs = new MyEventArgs<ContextActionModel<StudentParentModel>>() { Object = new ContextActionModel<StudentParentModel>() { Query = query, SelectedItems = results, Selected=selected, SelectAll = allSelected.Value, Direction = direction , OrderBy = orderBy, ActionKey=actionKey  } });

            if (eArgs != null)
            {
                if (eArgs.Object.Result != null)
                    return (FileMediaResult)eArgs.Object.Result;
            }
            

            return (new FeaturesController()).ExportDownload(typeof(StudentParentModel), results, format, this.GetUIPluralText("MBKYellowBox", "StudentParent"));
            
        }
			*/
		
		[HttpPost]
        public ActionResult CustomActionExecute(ContextActionModel model) {
		 try
            {
			//List<Guid> keysSelected1 = new List<Guid>();
			List<object> keysSelected1 = new List<object>();
            if (!string.IsNullOrEmpty(model.Selected))
            {
                foreach (var keyString in model.Selected.Split(char.Parse(",")))
                {
				
				keysSelected1.Add(Guid.Parse(keyString.Split(char.Parse("|"))[0]));
                        
                    

			
                }
            }
			DataAction dataAction = DataAction.GetDataAction(Request);
			 model.Selected = dataAction.Selected;

            model.Query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(dataAction.Query, dataAction.AllSelected, dataAction.Selected, "GuidStudentParent");
           
            
			
			#region implementaci�n de m�todo parcial
            bool replaceResult = false;
            MyEventArgs<ContextActionModel<StudentParentModel>> actionEventArgs = null;
           
			if (model.ActionKey != "deletemany" && model.ActionKey != "deleterelmany" && model.ActionKey != "updateRel" &&  model.ActionKey != "delete-relation-fk" && model.ActionKey != "restore" )
			{
				ContextRequest context = SFSdotNet.Framework.My.Context.BuildContextRequestSafe(System.Web.HttpContext.Current);
				context.UseMode = dataAction.Usemode;

				if (model.IsBackground)
				{
					System.Threading.Tasks.Task.Run(() => 
						OnCustomActionExecutingBackground(this, actionEventArgs = new MyEventArgs<ContextActionModel<StudentParentModel>>() { Object = new ContextActionModel<StudentParentModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } })
					);
				}
				else
				{
					OnCustomActionExecuting(this, actionEventArgs = new MyEventArgs<ContextActionModel<StudentParentModel>>() {  Object = new ContextActionModel<StudentParentModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } });
				}
			}
            List<StudentParentModel> results = null;
	
			if (model.ActionKey == "deletemany") { 
				
				BR.StudentParentsBR.Instance.Delete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

            }
	
			else if (model.ActionKey == "restore") {
                    BR.StudentParentsBR.Instance.UnDelete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

                }
            else if (model.ActionKey == "updateRel" || model.ActionKey == "delete-relation-fk" || model.ActionKey == "updateRel-proxyMany")
            {
               try {
                   string valueForUpdate = null;
				   string propForUpdate = null;
				   if (!string.IsNullOrEmpty(Request.Params["propertyForUpdate"])){
						propForUpdate = Request.Params["propertyForUpdate"];
				   }
				    if (string.IsNullOrEmpty(propForUpdate) && !string.IsNullOrEmpty(Request.QueryString["propertyForUpdate"]))
                   {
                       propForUpdate = Request.QueryString["propertyForUpdate"];
                   }
                    if (model.ActionKey != "delete-relation-fk")
                    {
                        valueForUpdate = Request.QueryString["valueForUpdate"];
                    }
                    BR.StudentParentsBR.Instance.UpdateAssociation(propForUpdate, valueForUpdate, model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());
					
                    if (model.ActionKey == "delete-relation-fk")
                    {
                        MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]);

                        return PartialView("ResultMessageView", message);
                    }
                    else
                    {
                        return Content("ok");
                    }
       
          
                }
                catch (Exception ex)
                {
				        SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
           
                }
            
            }
		
                if (actionEventArgs == null && !model.IsBackground)
                {
                    //if (model.ActionKey != "deletemany"  && model.ActionKey != "deleterelmany")
                    //{
                     //   throw new NotImplementedException("");
                    //}
                }
                else
                {
					if (model.IsBackground == false )
						 replaceResult = actionEventArgs.Object.Result is ActionResult /*actionEventArgs.Object.ReplaceResult*/;
                }
                #endregion
                if (!replaceResult)
                {
                    if (Request != null && Request.IsAjaxRequest())
                    {
						MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]) ;
                        if (model.IsBackground )
                            message.Message = GlobalMessages.THE_PROCESS_HAS_BEEN_STARTED;
                        
                        return PartialView("ResultMessageView", message);                    
					}
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return (ActionResult)actionEventArgs.Object.Result;
                }
            }
            catch (Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null) {
                        message = ex.Data["usermessage"].ToString();
                    }
					 SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }

            }
        }
        //
        // POST: /StudentParents/Delete/5
        
			
	
    }
}
namespace MBK.YellowBox.Web.Mvc.Controllers
{
	using MBK.YellowBox.Web.Mvc.Models.AcademyLevels;

    public partial class AcademyLevelsController : MBK.YellowBox.Web.Mvc.ControllerBase<Models.AcademyLevels.AcademyLevelModel>
    {

       


	#region partial methods
        ControllerEventArgs<Models.AcademyLevels.AcademyLevelModel> e = null;
        partial void OnValidating(object sender, ControllerEventArgs<Models.AcademyLevels.AcademyLevelModel> e);
        partial void OnGettingExtraData(object sender, MyEventArgs<UIModel<Models.AcademyLevels.AcademyLevelModel>> e);
        partial void OnCreating(object sender, ControllerEventArgs<Models.AcademyLevels.AcademyLevelModel> e);
        partial void OnCreated(object sender, ControllerEventArgs<Models.AcademyLevels.AcademyLevelModel> e);
        partial void OnEditing(object sender, ControllerEventArgs<Models.AcademyLevels.AcademyLevelModel> e);
        partial void OnEdited(object sender, ControllerEventArgs<Models.AcademyLevels.AcademyLevelModel> e);
        partial void OnDeleting(object sender, ControllerEventArgs<Models.AcademyLevels.AcademyLevelModel> e);
        partial void OnDeleted(object sender, ControllerEventArgs<Models.AcademyLevels.AcademyLevelModel> e);
    	partial void OnShowing(object sender, MyEventArgs<UIModel<Models.AcademyLevels.AcademyLevelModel>> e);
    	partial void OnGettingByKey(object sender, ControllerEventArgs<Models.AcademyLevels.AcademyLevelModel> e);
        partial void OnTaken(object sender, ControllerEventArgs<Models.AcademyLevels.AcademyLevelModel> e);
       	partial void OnCreateShowing(object sender, ControllerEventArgs<Models.AcademyLevels.AcademyLevelModel> e);
		partial void OnEditShowing(object sender, ControllerEventArgs<Models.AcademyLevels.AcademyLevelModel> e);
		partial void OnDetailsShowing(object sender, ControllerEventArgs<Models.AcademyLevels.AcademyLevelModel> e);
 		partial void OnActionsCreated(object sender, MyEventArgs<UIModel<Models.AcademyLevels.AcademyLevelModel >> e);
		partial void OnCustomActionExecuting(object sender, MyEventArgs<ContextActionModel<Models.AcademyLevels.AcademyLevelModel>> e);
		partial void OnCustomActionExecutingBackground(object sender, MyEventArgs<ContextActionModel<Models.AcademyLevels.AcademyLevelModel>> e);
        partial void OnDownloading(object sender, MyEventArgs<ContextActionModel<Models.AcademyLevels.AcademyLevelModel>> e);
      	partial void OnAuthorization(object sender, AuthorizationContext context);
		 partial void OnFilterShowing(object sender, MyEventArgs<UIModel<Models.AcademyLevels.AcademyLevelModel >> e);
         partial void OnSummaryOperationShowing(object sender, MyEventArgs<UIModel<Models.AcademyLevels.AcademyLevelModel>> e);

        partial void OnExportActionsCreated(object sender, MyEventArgs<UIModel<Models.AcademyLevels.AcademyLevelModel>> e);


		protected override void OnVirtualFilterShowing(object sender, MyEventArgs<UIModel<AcademyLevelModel>> e)
        {
            OnFilterShowing(sender, e);
        }
		 public override void OnVirtualExportActionsCreated(object sender, MyEventArgs<UIModel<AcademyLevelModel>> e)
        {
            OnExportActionsCreated(sender, e);
        }
        public override void OnVirtualDownloading(object sender, MyEventArgs<ContextActionModel<AcademyLevelModel>> e)
        {
            OnDownloading(sender, e);
        }
        public override void OnVirtualShowing(object sender, MyEventArgs<UIModel<AcademyLevelModel>> e)
        {
            OnShowing(sender, e);
        }

	#endregion
	#region API
	 public override ActionResult ApiCreateGen(AcademyLevelModel model, ContextRequest contextRequest)
        {
            return CreateGen(model, contextRequest);
        }

              public override ActionResult ApiGetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJson(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }
        public override ActionResult ApiGetByKeyJson(string id, ContextRequest contextRequest)
        {
            return  GetByKeyJson(id, contextRequest, true);
        }
      
		 public override int ApiGetByCount(string filter, ContextRequest contextRequest)
        {
            return GetByCount(filter, contextRequest);
        }
         protected override ActionResult ApiDeleteGen(List<AcademyLevelModel> models, ContextRequest contextRequest)
        {
            List<AcademyLevel> objs = new List<AcademyLevel>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                BR.AcademyLevelsBR.Instance.DeleteBulk(objs, contextRequest);
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        protected override ActionResult ApiUpdateGen(List<AcademyLevelModel> models, ContextRequest contextRequest)
        {
            List<AcademyLevel> objs = new List<AcademyLevel>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                foreach (var obj in objs)
                {
                    BR.AcademyLevelsBR.Instance.Update(obj, contextRequest);

                }
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }


	#endregion
#region Validation methods	
	    private void Validations(AcademyLevelModel model) { 
            #region Remote validations

            #endregion
		}

#endregion
		
 		public AuthorizationContext Authorization(AuthorizationContext context)
        {
            OnAuthorization(this,  context );
            return context ;
        }
		public List<AcademyLevelModel> GetAll() {
            			var bos = BR.AcademyLevelsBR.Instance.GetBy("",
					new SFSdotNet.Framework.My.ContextRequest()
					{
						CustomQuery = new SFSdotNet.Framework.My.CustomQuery()
						{
							OrderBy = "Title",
							SortDirection = SFSdotNet.Framework.Data.SortDirection.Ascending
						}
					});
            			List<AcademyLevelModel> results = new List<AcademyLevelModel>();
            AcademyLevelModel model = null;
            foreach (var bo in bos)
            {
                model = new AcademyLevelModel();
                model.Bind(bo);
                results.Add(model);
            }
            return results;

        }
        //
        // GET: /AcademyLevels/
		[MyAuthorize("r", "AcademyLevel", "MBKYellowBox", typeof(AcademyLevelsController))]
		public ActionResult Index()
        {
    		var uiModel = GetContextModel(UIModelContextTypes.ListForm, null);
			ViewBag.UIModel = uiModel;
			uiModel.FilterStart = (string)ViewData["startFilter"];
                    MyEventArgs<UIModel<AcademyLevelModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<AcademyLevelModel>>() { Object = uiModel });

			OnExportActionsCreated(this, (me != null ? me : me = new MyEventArgs<UIModel<AcademyLevelModel>>() { Object = uiModel }));

            if (me != null)
            {
                uiModel = me.Object;
            }
            if (me == null)
                me = new MyEventArgs<UIModel<AcademyLevelModel>>() { Object = uiModel };
           
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;


            //return View("ListGen");
			return ResolveView(uiModel);
        }
		[MyAuthorize("r", "AcademyLevel", "MBKYellowBox", typeof(AcademyLevelsController))]
		public ActionResult ListViewGen(string idTab, string fk , string fkValue, string startFilter, ListModes  listmode  = ListModes.SimpleList, PropertyDefinition parentRelationProperty = null, object parentRelationPropertyValue = null )
        {
			ViewData["idTab"] = System.Web.HttpContext.Current.Request.QueryString["idTab"]; 
		 	ViewData["detpop"] = true; // details in popup
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"])) {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"]; 
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
            {
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["startFilter"]))
            {
                ViewData["startFilter"] = Request.QueryString["startFilter"];
            }
			
			UIModel<AcademyLevelModel> uiModel = GetContextModel(UIModelContextTypes.ListForm, null);

            MyEventArgs<UIModel<AcademyLevelModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<AcademyLevelModel>>() { Object = uiModel });
            if (me == null)
                me = new MyEventArgs<UIModel<AcademyLevelModel>>() { Object = uiModel };
            uiModel.Properties = GetProperties(uiModel);
            uiModel.ContextType = UIModelContextTypes.ListForm;
             uiModel.FilterStart = (string)ViewData["startFilter"];
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;
			 if (listmode == ListModes.SimpleList)
                return ResolveView(uiModel);
            else
            {
                ViewData["parentRelationProperty"] = parentRelationProperty;
                ViewData["parentRelationPropertyValue"] = parentRelationPropertyValue;
                return PartialView("ListForTagSelectView");
            }
            return ResolveView(uiModel);
        }
		List<PropertyDefinition> _properties = null;

		 protected override List<PropertyDefinition> GetProperties(UIModel uiModel,  params string[] specificProperties)
        { 
            return GetProperties(uiModel, false, null, specificProperties);
        }

		protected override List<PropertyDefinition> GetProperties(UIModel uiModel, bool decripted, Guid? id, params string[] specificProperties)
            {

			bool allProperties = true;    
                if (specificProperties != null && specificProperties.Length > 0)
                {
                    allProperties = false;
                }


			List<CustomProperty> customProperties = new List<CustomProperty>();
			if (_properties == null)
                {
                List<PropertyDefinition> results = new List<PropertyDefinition>();

			string idAcademyLevel = GetRouteDataOrQueryParam("id");
			if (idAcademyLevel != null)
			{
				if (!decripted)
                {
					idAcademyLevel = SFSdotNet.Framework.Entities.Utils.GetPropertyKey(idAcademyLevel.Replace("-","/"), "GuidAcademyLevel");
				}else{
					if (id != null )
						idAcademyLevel = id.Value.ToString();                

				}
			}

			bool visibleProperty = true;	
			 bool conditionalshow =false;
                if (uiModel.ContextType == UIModelContextTypes.EditForm || uiModel.ContextType == UIModelContextTypes.DisplayForm ||  uiModel.ContextType == UIModelContextTypes.GenericForm )
                    conditionalshow = true;
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidAcademyLevel"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidAcademyLevel")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 100,
																	
					CustomProperties = customProperties,

                    PropertyName = "GuidAcademyLevel",

					 MaxLength = 0,
					IsRequired = true ,
					IsHidden = true,
                    SystemProperty =  SystemProperties.Identifier ,
					IsDefaultProperty = false,
                    SortBy = "GuidAcademyLevel",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.AcademyLevelResources.GUIDACADEMYLEVEL*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Title"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Title")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 101,
																	
					CustomProperties = customProperties,

                    PropertyName = "Title",

					 MaxLength = 255,
					 Nullable = true,
					IsDefaultProperty = true,
                    SortBy = "Title",
					
	
                    TypeName = "String",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.AcademyLevelResources.TITLE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 102,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedDate",
					
	
				SystemProperty = SystemProperties.CreatedDate ,
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.AcademyLevelResources.CREATEDDATE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 114,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedDate",
					
	
					IsUpdatedDate = true,
					SystemProperty = SystemProperties.UpdatedDate ,
	
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    ,PropertyDisplayName = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.UPDATED

                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 104,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedBy",
					
	
				SystemProperty = SystemProperties.CreatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.AcademyLevelResources.CREATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 105,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedBy",
					
	
				SystemProperty = SystemProperties.UpdatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.AcademyLevelResources.UPDATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Bytes"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Bytes")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 106,
																	
					CustomProperties = customProperties,

                    PropertyName = "Bytes",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Bytes",
					
	
				SystemProperty = SystemProperties.SizeBytes,
                    TypeName = "Int32",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.AcademyLevelResources.BYTES*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Students"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"AcademyLevel" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="Students.GuidStudent")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"Students.GuidStudent" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"Students" });
			

	
	//fk_Student_AcademyLevel
		//if (this.Request.QueryString["fk"] != "Students")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 107,
																
					Link = VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/Students/ListViewGen?overrideModule=" + GetOverrideApp()  + "&pal=False&es=False&pag=10&filterlinks=1&idTab=Students&fk=AcademyLevel&startFilter="+ (new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext)).Encode("it.AcademyLevel.GuidAcademyLevel = Guid(\"" + idAcademyLevel +"\")")+ "&fkValue=" + idAcademyLevel,
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "Student",
					
					CustomProperties = customProperties,

                    PropertyName = "Students",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Students.FullName",
					
	
                    TypeName = "MBKYellowBoxModel.Student",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = true,
                    PathName = "MBKYellowBox/Students"
                    /*,PropertyDisplayName = Resources.AcademyLevelResources.STUDENTS*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Profesors"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"AcademyLevel" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="Profesors.GuidProfesor")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"Profesors.GuidProfesor" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"Profesors" });
			

	
	//fk_Profesor_AcademyLevel
		//if (this.Request.QueryString["fk"] != "Profesors")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 108,
																
					Link = VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/Profesors/ListViewGen?overrideModule=" + GetOverrideApp()  + "&pal=False&es=False&pag=10&filterlinks=1&idTab=Profesors&fk=AcademyLevel&startFilter="+ (new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext)).Encode("it.AcademyLevel.GuidAcademyLevel = Guid(\"" + idAcademyLevel +"\")")+ "&fkValue=" + idAcademyLevel,
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "Profesor",
					
					CustomProperties = customProperties,

                    PropertyName = "Profesors",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Profesors.FullName",
					
	
                    TypeName = "MBKYellowBoxModel.Profesor",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = true,
                    PathName = "MBKYellowBox/Profesors"
                    /*,PropertyDisplayName = Resources.AcademyLevelResources.PROFESORS*/
                });
		//	}
	
	}
	
				
                    _properties = results;
                    return _properties;
                }
                else {
                    return _properties;
                }
            }

		protected override  UIModel<AcademyLevelModel> GetByForShow(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, params  object[] extraParams)
        {
			if (Request != null )
				if (!string.IsNullOrEmpty(Request.QueryString["q"]))
					filter = filter + HttpUtility.UrlDecode(Request.QueryString["q"]);
 if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
            }
            var bos = BR.AcademyLevelsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), contextRequest, extraParams);
			//var bos = BR.AcademyLevelsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), context, extraParams);
            AcademyLevelModel model = null;
            List<AcademyLevelModel> results = new List<AcademyLevelModel>();
            foreach (var item in bos)
            {
                model = new AcademyLevelModel();
				model.Bind(item);
				results.Add(model);
            }
            //return results;
			UIModel<AcademyLevelModel> uiModel = GetContextModel(UIModelContextTypes.Items, null);
            uiModel.Items = results;
			if (Request != null){
				if (SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(Request.RequestContext, "action") == "Download")
				{
					uiModel.ContextType = UIModelContextTypes.ExportDownload;
				}
			}
            Showing(ref uiModel);
            return uiModel;
		}			
		
		//public List<AcademyLevelModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params  object[] extraParams)
        //{
		//	var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, null, extraParams);
        public override List<AcademyLevelModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest,  params  object[] extraParams)
        {
            var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
           
            return uiModel.Items;
		
        }
		/*
        [MyAuthorize("r", "AcademyLevel", "MBKYellowBox", typeof(AcademyLevelsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir)
        {
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir);
        }*/

		  [MyAuthorize("r", "AcademyLevel", "MBKYellowBox", typeof(AcademyLevelsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir,ContextRequest contextRequest,  object[] extraParams)
        {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir,contextRequest, extraParams);
        }
/*		  [MyAuthorize("r", "AcademyLevel", "MBKYellowBox", typeof(AcademyLevelsController))]
       public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJsonBase(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }*/
		[MyAuthorize()]
		public int GetByCount(string filter, ContextRequest contextRequest) {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
            return BR.AcademyLevelsBR.Instance.GetCount(HttpUtility.UrlDecode(filter), GetUseMode(), contextRequest);
        }
		

		[MyAuthorize("r", "AcademyLevel", "MBKYellowBox", typeof(AcademyLevelsController))]
        public ActionResult GetByKeyJson(string id, ContextRequest contextRequest,  bool dec = false)
        {
            return Json(GetByKey(id, null, contextRequest, dec), JsonRequestBehavior.AllowGet);
        }
		public AcademyLevelModel GetByKey(string id) {
			return GetByKey(id, null,null, false);
       	}
		    public AcademyLevelModel GetByKey(string id, string includes)
        {
            return GetByKey(id, includes, false);
        }
		 public  AcademyLevelModel GetByKey(string id, string includes, ContextRequest contextRequest)
        {
            return GetByKey(id, includes, contextRequest, false);
        }
		/*
		  public ActionResult ShowField(string fieldName, string idField) {
		   string safePropertyName = fieldName;
              if (fieldName.StartsWith("Fk"))
              {
                  safePropertyName = fieldName.Substring(2, fieldName.Length - 2);
              }

             AcademyLevelModel model = new  AcademyLevelModel();

            UIModel uiModel = GetUIModel(model, new string[] { "NoField-" });
			
				uiModel.Properties = GetProperties(uiModel, safePropertyName);
		uiModel.Properties.ForEach(p=> p.ContextType = uiModel.ContextType );
            uiModel.ContextType = UIModelContextTypes.FilterFields;
            uiModel.OverrideApp = GetOverrideApp();
            uiModel.UseMode = GetUseMode();

            ViewData["uiModel"] = uiModel;
			var prop = uiModel.Properties.FirstOrDefault(p=>p.PropertyName == safePropertyName);
            //if (prop.IsNavigationProperty && prop.IsNavigationPropertyMany == false)
            //{
            //    ViewData["currentProperty"] = uiModel.Properties.FirstOrDefault(p => p.PropertyName != fieldName + "Text");
            //}else if (prop.IsNavigationProperty == false){
                ViewData["currentProperty"] = prop;
           // }
            ((PropertyDefinition)ViewData["currentProperty"]).RemoveLayout = true;
			ViewData["withContainer"] = false;


            return PartialView("GenericField", model);


        }
      */
	public AcademyLevelModel GetByKey(string id, ContextRequest contextRequest, bool dec)
        {
            return GetByKey(id, null, contextRequest, dec);
        }
        public AcademyLevelModel GetByKey(string id, string  includes, bool dec)
        {
            return GetByKey(id, includes, null, dec);
        }

        public AcademyLevelModel GetByKey(string id, string includes, ContextRequest contextRequest, bool dec) {
		             AcademyLevelModel model = null;
            ControllerEventArgs<AcademyLevelModel> e = null;
			string objectKey = id.Replace("-","/");
             OnGettingByKey(this, e=  new ControllerEventArgs<AcademyLevelModel>() { Id = objectKey  });
             bool cancel = false;
             AcademyLevelModel eItem = null;
             if (e != null)
             {
                 cancel = e.Cancel;
                 eItem = e.Item;
             }
			if (cancel == false && eItem == null)
             {
			Guid guidAcademyLevel = Guid.Empty; //new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, "GuidAcademyLevel"));
			if (dec)
                 {
                     guidAcademyLevel = new Guid(id);
                 }
                 else
                 {
                     guidAcademyLevel = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, null));
                 }
			
            
				model = new AcademyLevelModel();
                  if (contextRequest == null)
                {
                    contextRequest = GetContextRequest();
                }
				var bo = BR.AcademyLevelsBR.Instance.GetByKey(guidAcademyLevel, GetUseMode(), contextRequest,  includes);
				 if (bo != null)
                    model.Bind(bo);
                else
                    return null;
			}
             else {
                 model = eItem;
             }
			model.IsNew = false;

            return model;
        }
        // GET: /AcademyLevels/DetailsGen/5
		[MyAuthorize("r", "AcademyLevel", "MBKYellowBox", typeof(AcademyLevelsController))]
        public ActionResult DetailsGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = AcademyLevelResources.ENTITY_PLURAL;
			 #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<AcademyLevelModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
            #endregion



			 bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null) {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
			//UIModel<AcademyLevelModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, decripted), decripted, guidId);
			var item = GetByKey(id, null, null, decripted);
			if (item == null)
            {
                 RouteValueDictionary rv = new RouteValueDictionary();
                string usemode = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"usemode");
                string overrideModule = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "overrideModule");
                string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");

                if(!string.IsNullOrEmpty(usemode)){
                    rv.Add("usemode", usemode);
                }
                if(!string.IsNullOrEmpty(overrideModule)){
                    rv.Add("overrideModule", overrideModule);
                }
                if (!string.IsNullOrEmpty(area))
                {
                    rv.Add("area", area);
                }

                return RedirectToAction("Index", rv);
            }
            //
            UIModel<AcademyLevelModel> uiModel = null;
                uiModel = GetContextModel(UIModelContextTypes.DisplayForm, item, decripted, guidId);



            MyEventArgs<UIModel<AcademyLevelModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<AcademyLevelModel>>() { Object = uiModel });

            if (me != null) {
                uiModel = me.Object;
            }
			
            Showing(ref uiModel);
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			
            //return View("DisplayGen", uiModel.Items[0]);
			return ResolveView(uiModel, uiModel.Items[0]);

        }
		[MyAuthorize("r", "AcademyLevel", "MBKYellowBox", typeof(AcademyLevelsController))]
		public ActionResult DetailsViewGen(string id)
        {

		 bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<AcademyLevelModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
           
			if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
 			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
           
        	 //var uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id));
			 
            bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
            {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true")
                {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
            UIModel<AcademyLevelModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, null, decripted), decripted, guidId);
			

            MyEventArgs<UIModel<AcademyLevelModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<AcademyLevelModel>>() { Object = uiModel });

            if (me != null)
            {
                uiModel = me.Object;
            }
            
            Showing(ref uiModel);
            return ResolveView(uiModel, uiModel.Items[0]);
        
        }
        //
        // GET: /AcademyLevels/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        //
        // GET: /AcademyLevels/CreateGen
		[MyAuthorize("c", "AcademyLevel", "MBKYellowBox", typeof(AcademyLevelsController))]
        public ActionResult CreateGen()
        {
			AcademyLevelModel model = new AcademyLevelModel();
            model.IsNew = true;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model);

			OnCreateShowing(this, e = new ControllerEventArgs<AcademyLevelModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }

             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                 ViewData["ispopup"] = true;
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                 ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                 ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

            Showing(ref me);

			return ResolveView(me, me.Items[0]);
        } 
			
		protected override UIModel<AcademyLevelModel> GetContextModel(UIModelContextTypes formMode, AcademyLevelModel model)
        {
            return GetContextModel(formMode, model, false, null);
        }
			
		 private UIModel<AcademyLevelModel> GetContextModel(UIModelContextTypes formMode, AcademyLevelModel model, bool decript, Guid ? id) {
            UIModel<AcademyLevelModel> me = new UIModel<AcademyLevelModel>(true, "AcademyLevels");
			me.UseMode = GetUseMode();
			me.Controller = this;
			me.OverrideApp = GetOverrideApp();
			me.ContextType = formMode ;
			me.Id = "AcademyLevel";
			
            me.ModuleKey = "MBKYellowBox";

			me.ModuleNamespace = "MBK.YellowBox";
            me.EntityKey = "AcademyLevel";
            me.EntitySetName = "AcademyLevels";

			me.AreaAction = "MBKYellowBox";
            me.ControllerAction = "AcademyLevels";
            me.PropertyKeyName = "GuidAcademyLevel";

            me.Properties = GetProperties(me, decript, id);

			me.SortBy = "UpdatedDate";
			me.SortDirection = UIModelSortDirection.DESC;

 			
			if (Request != null)
            {
                string actionName = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam( Request.RequestContext, "action");
                if(actionName != null && actionName.ToLower().Contains("create") ){
                    me.IsNew = true;
                }
            }
			 #region Buttons
			 if (Request != null ){
             if (formMode == UIModelContextTypes.DisplayForm || formMode == UIModelContextTypes.EditForm || formMode == UIModelContextTypes.ListForm)
				me.ActionButtons = GetActionButtons(formMode,model != null ?(Request.QueryString["dec"] == "true" ? model.Id : model.SafeKey)  : null, "MBKYellowBox", "AcademyLevels", "AcademyLevel", me.IsNew);

            //me.ActionButtons.Add(new ActionModel() { ActionKey = "return", Title = GlobalMessages.RETURN, Url = System.Web.VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/AcademyLevels" });
			if (this.HttpContext != null &&  !this.HttpContext.SkipAuthorization){
				//antes this.HttpContext
				me.SetAction("u", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("u", "AcademyLevel", "MBKYellowBox"));
				me.SetAction("c", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("c", "AcademyLevel", "MBKYellowBox"));
				me.SetAction("d", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("d", "AcademyLevel", "MBKYellowBox"));
			
			}else{
				me.SetAction("u", true);
				me.SetAction("c", true);
				me.SetAction("d", true);

			}
            #endregion              
         
            switch (formMode)
            {
                case UIModelContextTypes.DisplayForm:
					//me.TitleForm = AcademyLevelResources.ACADEMYLEVELS_DETAILS;
                    me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.MODIFY_DATA;
					 me.Properties.Where(p=>p.PropertyName  != "Id" && p.IsForeignKey == false).ToList().ForEach(p => p.IsHidden = false);

					 me.Properties.Where(p => (p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier) ).ToList().ForEach(p=> me.SetHide(p.PropertyName));

                    break;
                case UIModelContextTypes.EditForm:
				  me.Properties.Where(p=>p.SystemProperty != SystemProperties.Identifier && p.IsForeignKey == false && p.PropertyName != "Id").ToList().ForEach(p => p.IsHidden = false);

					if (model != null)
                    {
						

                        me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.SAVE_DATA;                        
                        me.ActionButtons.First(p => p.ActionKey == "c").Title = GlobalMessages.SAVE_DATA;
						if (model.IsNew ){
							//me.TitleForm = AcademyLevelResources.ACADEMYLEVELS_ADD_NEW;
							me.ActionName = "CreateGen";
							me.Properties.RemoveAll(p => p.SystemProperty != null || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));
						}else{
							
							me.ActionName = "EditGen";

							//me.TitleForm = AcademyLevelResources.ACADEMYLEVELS_EDIT;
							me.Properties.RemoveAll(p => p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));	
						}
						//me.Properties.Remove(me.Properties.Find(p => p.PropertyName == "UpdatedDate"));
					
					}
                    break;
                case UIModelContextTypes.FilterFields:
                    break;
                case UIModelContextTypes.GenericForm:
                    break;
                case UIModelContextTypes.Items:
				//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "Title") != null){
						me.Properties.Find(p => p.PropertyName == "Title").IsHidden = false;
					 }
					 
                    
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					 
                    
					

						 if (me.Properties.Find(p => p.PropertyName == "GuidAcademyLevel") != null){
						me.Properties.Find(p => p.PropertyName == "GuidAcademyLevel").IsHidden = false;
					 }
					 
                    
					


                  


					//}
                    break;
                case UIModelContextTypes.ListForm:
					PropertyDefinition propFinded = null;
					//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "Title") != null){
						me.Properties.Find(p => p.PropertyName == "Title").IsHidden = false;
					 }
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					
					me.PrincipalActionName = "GetByJson";
					//}
					//me.TitleForm = AcademyLevelResources.ACADEMYLEVELS_LIST;
                    break;
                default:
                    break;
            }
            	this.SetDefaultProperties(me);
			}
			if (model != null )
            	me.Items.Add(model);
            return me;
        }
		// GET: /AcademyLevels/CreateViewGen
		[MyAuthorize("c", "AcademyLevel", "MBKYellowBox", typeof(AcademyLevelsController))]
        public ActionResult CreateViewGen()
        {
				AcademyLevelModel model = new AcademyLevelModel();
            model.IsNew = true;
			e= null;
			OnCreateShowing(this, e = new ControllerEventArgs<AcademyLevelModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }
			
            var me = GetContextModel(UIModelContextTypes.EditForm, model);

			me.IsPartialView = true;	
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
            {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                me.Properties.Find(p => p.PropertyName == ViewData["fk"].ToString()).IsReadOnly = true;
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
			
      
            //me.Items.Add(model);
            Showing(ref me);
            return ResolveView(me, me.Items[0]);
        }
		protected override  void GettingExtraData(ref UIModel<AcademyLevelModel> uiModel)
        {

            MyEventArgs<UIModel<AcademyLevelModel>> me = null;
            OnGettingExtraData(this, me = new MyEventArgs<UIModel<AcademyLevelModel>>() { Object = uiModel });
            //bool maybeAnyReplaced = false; 
            if (me != null)
            {
                uiModel = me.Object;
                //maybeAnyReplaced = true;
            }
           
			bool canFill = false;
			 string query = null ;
            bool isFK = false;
			PropertyDefinition prop =null;
			var contextRequest = this.GetContextRequest();
            contextRequest.CustomParams.Add(new CustomParam() { Name="ui", Value= AcademyLevel.EntityName });

                        

        }
		private void Showing(ref UIModel<AcademyLevelModel> uiModel) {
          	
			MyEventArgs<UIModel<AcademyLevelModel>> me = new MyEventArgs<UIModel<AcademyLevelModel>>() { Object = uiModel };
			 OnVirtualLayoutSettings(this, me);


            OnShowing(this, me);

			
			if ((Request != null && Request.QueryString["allFields"] == "1") || Request == null )
			{
				me.Object.Properties.ForEach(p=> p.IsHidden = false);
            }
            if (me != null)
            {
                uiModel = me.Object;
            }
          


			 if (uiModel.ContextType == UIModelContextTypes.EditForm)
			    GettingExtraData(ref uiModel);
            ViewData["UIModel"] = uiModel;

        }
        //
        // POST: /AcademyLevels/Create
		[MyAuthorize("c", "AcademyLevel", "MBKYellowBox", typeof(AcademyLevelsController))]
        [HttpPost]
		[ValidateInput(false)] 
        public ActionResult CreateGen(AcademyLevelModel  model,  ContextRequest contextRequest)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
		 	e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<AcademyLevelModel>() { Item = model });
           
		  	if (!ModelState.IsValid) {
				model.IsNew = true;
				var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
                 if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                        ViewData["ispopup"] = true;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
                    return ResolveView(me, model);
            }
            try
            {
				if (model.GuidAcademyLevel == null || model.GuidAcademyLevel.ToString().Contains("000000000"))
				model.GuidAcademyLevel = Guid.NewGuid();
	
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnCreating(this, e = new ControllerEventArgs<AcademyLevelModel>() { Item = model });
                if (e != null) {
                   if (e.Cancel && e.RedirectValues.Count > 0){
                        RouteValueDictionary rv = new RouteValueDictionary();
                        if (e.RedirectValues["area"] != null ){
                            rv.Add("area", e.RedirectValues["area"].ToString());
                        }
                        foreach (var item in e.RedirectValues.Where(p=>p.Key != "area" && p.Key != "controller" &&  p.Key != "action" ))
	                    {
		                    rv.Add(item.Key, item.Value);
	                    }

                        //if (e.RedirectValues["action"] != null && e.RedirectValues["controller"] != null && e.RedirectValues["area"] != null )
                        return RedirectToAction(e.RedirectValues["action"].ToString(), e.RedirectValues["controller"].ToString(), rv );


                        
                    }else if (e.Cancel && e.ActionResult != null )
                        return e.ActionResult;  
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				if (contextRequest == null || contextRequest.Company == null){
					contextRequest = GetContextRequest();
					
				}
                if (!cancel)
                	model.Bind(AcademyLevelsBR.Instance.Create(model.GetBusinessObject(), contextRequest ));
				OnCreated(this, e = new ControllerEventArgs<AcademyLevelModel>() { Item = model });
                 if (e != null )
					if (e.ActionResult != null)
                    	replaceResult = true;		
				if (!replaceResult)
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))
                    {
                        ViewData["__continue"] = true;
                    }
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"]) &&  string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
                        popupextra.Add("id", model.SafeKey);
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                       {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                            popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"area"));
                            popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                            popupextra.Add("action", actionDetails);
                       if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
                    {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))

                        {
                            var popupextra = GetRouteData();
                            popupextra.Add("id", model.SafeKey);
                            return RedirectToAction("EditViewGen", popupextra);
                        }
                        else
                        {
                            return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.ADD_DONE));
                        }
                    }        			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                        return Redirect(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]);
                    else{

							RouteValueDictionary popupextra = null; 
							if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"])){
                            popupextra = GetRouteData();
							 string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
                            if (!string.IsNullOrEmpty(area))
                                popupextra.Add("area", area);
                            
                            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"])) {
								popupextra.Add("id", model.SafeKey);
                                return RedirectToAction("EditGen", popupextra);

                            }else{
								
                            return RedirectToAction("Index", popupextra);
							}
							}else{
								return Content("ok");
							}
                        }
						 }
                else {
                    return e.ActionResult;
                    }
				}
            catch(Exception ex)
            {
					if (!string.IsNullOrEmpty(Request.QueryString["rok"]))
                {
                    throw  ex;
                }
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                model.IsNew = true;
                var me = GetContextModel(UIModelContextTypes.EditForm, model, true, model.GuidAcademyLevel);
                Showing(ref me);
                if (isPopUp)
                {
                    
                        ViewData["ispopup"] = isPopUp;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
					if (Request != null)
						return ResolveView(me, model);
					else
						return Content("ok");
            }
        }        
        //
        // GET: /AcademyLevels/Edit/5 
        public ActionResult Edit(int id)
        {
            return View();
        }
			
		
		[MyAuthorize("u", "AcademyLevel", "MBKYellowBox", typeof(AcademyLevelsController))]
		[MvcSiteMapNode(Area="MBKYellowBox", Title="sss", Clickable=false, ParentKey = "MBKYellowBox_AcademyLevel_List")]
		public ActionResult EditGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = AcademyLevelResources.ENTITY_SINGLE;		 	
  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<AcademyLevelModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
            AcademyLevelModel model = null;
            // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
			bool dec = false;
            Guid ? idGuidDecripted = null ;
            if (Request != null && Request.QueryString["dec"] == "true")
            {
                dec = true;
                idGuidDecripted = Guid.Parse(id);
            }

            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model,dec,idGuidDecripted);
            Showing(ref me);


            if (!replaceResult)
            {
                 //return View("EditGen", me.Items[0]);
				 return ResolveView(me, me.Items[0]);
            }
            else {
                return e.ActionResult;
            }
        }
			[MyAuthorize("u", "AcademyLevel","MBKYellowBox", typeof(AcademyLevelsController))]
		public ActionResult EditViewGen(string id)
        {
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

					  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<AcademyLevelModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
			
            AcademyLevelModel model = null;
			 bool dec = false;
            Guid? guidId = null ;

            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null && System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                dec = true;
                guidId = Guid.Parse(id);
            }
            // si fue implementado el método parcial y no se ha decidido suspender la acción
            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
            var me = GetContextModel(UIModelContextTypes.EditForm, model, dec, guidId);
            Showing(ref me);

            return ResolveView(me, model);
        }
		[MyAuthorize("u", "AcademyLevel",  "MBKYellowBox", typeof(AcademyLevelsController))]
		[HttpPost]
		[ValidateInput(false)] 
		        public ActionResult EditGen(AcademyLevelModel model)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
			e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<AcademyLevelModel>() { Item = model });
           
            if (!ModelState.IsValid)
            {
			   	var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
			
				if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"])){
                	ViewData["ispopup"] = true;
					return ResolveView(me, model);
				}
				else
					return ResolveView(me, model);
            }
            try
            {
			
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnEditing(this, e = new ControllerEventArgs<AcademyLevelModel>() { Item = model });
                if (e != null) {
                    if (e.Cancel && e.ActionResult != null)
                        return e.ActionResult;
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				ContextRequest context = new ContextRequest();
                context.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;

                AcademyLevel resultObj = null;
			    if (!cancel)
                	resultObj = AcademyLevelsBR.Instance.Update(model.GetBusinessObject(), GetContextRequest());
				
				OnEdited(this, e = new ControllerEventArgs<AcademyLevelModel>() { Item =   new AcademyLevelModel(resultObj) });
				if (e != null && e.ActionResult != null) replaceResult = true; 

                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Content("ok");
                }
                else
                {
				if (!replaceResult)
                {
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"])  && string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
						 if (Request != null && Request.QueryString["dec"] == "true")
                        {
                            popupextra.Add("id", model.Id);
                        }
                        else
                        {
							popupextra.Add("id", model.SafeKey);

							
                        }
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                        {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                        popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area"));
                        popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                        popupextra.Add("action", actionDetails);
                        if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
					if (isPopUp)
						return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.UPDATE_DONE));
        			    string returnUrl = null;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"])) {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.Form["ReturnAfter"];
                        else if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"];
                    }
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else{
		RouteValueDictionary popupextra = null; 
						 if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"]))
                            {
							
							popupextra = GetRouteData();
							string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
							if (!string.IsNullOrEmpty(area))
								popupextra.Add("area", area);

							return RedirectToAction("Index", popupextra);
						}else{
							return Content("ok");
						}
						}
				 }
                else {
                    return e.ActionResult;
				}
                }		
            }
          catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
			    if (isPopUp)
                {
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(ex.Message));
                    
                }
                else
                {
                    SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                    
                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
                else {
						  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
							ViewData["ispopup"] = true;
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
							ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
							ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

						var me = GetContextModel(UIModelContextTypes.EditForm, model);
						Showing(ref me);

						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
						{
							ViewData["ispopup"] = true;
							return ResolveView(me, model);
						}
						else
							return ResolveView(me, model);

						
					}
				}
            }
        }
        //
        // POST: /AcademyLevels/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                //  Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /AcademyLevels/Delete/5
        
		[MyAuthorize("d", "AcademyLevel", "MBKYellowBox", typeof(AcademyLevelsController))]
		[HttpDelete]
        public ActionResult DeleteGen(string objectKey, string extraParams)
        {
            try
            {
					
			
				Guid guidAcademyLevel = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey.Replace("-", "/"), "GuidAcademyLevel")); 
                BO.AcademyLevel entity = new BO.AcademyLevel() { GuidAcademyLevel = guidAcademyLevel };

                BR.AcademyLevelsBR.Instance.Delete(entity, GetContextRequest());               
                return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.DELETE_DONE));

            }
            catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null)
                    {
                        message = ex.Data["usermessage"].ToString();
                    }

                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }
            }
        }
		/*[MyAuthorize()]
		public FileMediaResult Download(string query, bool? allSelected = false,  string selected = null , string orderBy = null , string direction = null , string format = null , string actionKey=null )
        {
			
            List<Guid> keysSelected1 = new List<Guid>();
            if (!string.IsNullOrEmpty(selected)) {
                foreach (var keyString in selected.Split(char.Parse("|")))
                {
				
                    keysSelected1.Add(Guid.Parse(keyString));
                }
            }
				
            query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(query, allSelected.Value, selected, "GuidAcademyLevel");
            MyEventArgs<ContextActionModel<AcademyLevelModel>> eArgs = null;
            List<AcademyLevelModel> results = GetBy(query, null, null, orderBy, direction, GetContextRequest(), keysSelected1);
            OnDownloading(this, eArgs = new MyEventArgs<ContextActionModel<AcademyLevelModel>>() { Object = new ContextActionModel<AcademyLevelModel>() { Query = query, SelectedItems = results, Selected=selected, SelectAll = allSelected.Value, Direction = direction , OrderBy = orderBy, ActionKey=actionKey  } });

            if (eArgs != null)
            {
                if (eArgs.Object.Result != null)
                    return (FileMediaResult)eArgs.Object.Result;
            }
            

            return (new FeaturesController()).ExportDownload(typeof(AcademyLevelModel), results, format, this.GetUIPluralText("MBKYellowBox", "AcademyLevel"));
            
        }
			*/
		
		[HttpPost]
        public ActionResult CustomActionExecute(ContextActionModel model) {
		 try
            {
			//List<Guid> keysSelected1 = new List<Guid>();
			List<object> keysSelected1 = new List<object>();
            if (!string.IsNullOrEmpty(model.Selected))
            {
                foreach (var keyString in model.Selected.Split(char.Parse(",")))
                {
				
				keysSelected1.Add(Guid.Parse(keyString.Split(char.Parse("|"))[0]));
                        
                    

			
                }
            }
			DataAction dataAction = DataAction.GetDataAction(Request);
			 model.Selected = dataAction.Selected;

            model.Query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(dataAction.Query, dataAction.AllSelected, dataAction.Selected, "GuidAcademyLevel");
           
            
			
			#region implementaci�n de m�todo parcial
            bool replaceResult = false;
            MyEventArgs<ContextActionModel<AcademyLevelModel>> actionEventArgs = null;
           
			if (model.ActionKey != "deletemany" && model.ActionKey != "deleterelmany" && model.ActionKey != "updateRel" &&  model.ActionKey != "delete-relation-fk" && model.ActionKey != "restore" )
			{
				ContextRequest context = SFSdotNet.Framework.My.Context.BuildContextRequestSafe(System.Web.HttpContext.Current);
				context.UseMode = dataAction.Usemode;

				if (model.IsBackground)
				{
					System.Threading.Tasks.Task.Run(() => 
						OnCustomActionExecutingBackground(this, actionEventArgs = new MyEventArgs<ContextActionModel<AcademyLevelModel>>() { Object = new ContextActionModel<AcademyLevelModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } })
					);
				}
				else
				{
					OnCustomActionExecuting(this, actionEventArgs = new MyEventArgs<ContextActionModel<AcademyLevelModel>>() {  Object = new ContextActionModel<AcademyLevelModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } });
				}
			}
            List<AcademyLevelModel> results = null;
	
			if (model.ActionKey == "deletemany") { 
				
				BR.AcademyLevelsBR.Instance.Delete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

            }
	
			else if (model.ActionKey == "restore") {
                    BR.AcademyLevelsBR.Instance.UnDelete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

                }
            else if (model.ActionKey == "updateRel" || model.ActionKey == "delete-relation-fk" || model.ActionKey == "updateRel-proxyMany")
            {
               try {
                   string valueForUpdate = null;
				   string propForUpdate = null;
				   if (!string.IsNullOrEmpty(Request.Params["propertyForUpdate"])){
						propForUpdate = Request.Params["propertyForUpdate"];
				   }
				    if (string.IsNullOrEmpty(propForUpdate) && !string.IsNullOrEmpty(Request.QueryString["propertyForUpdate"]))
                   {
                       propForUpdate = Request.QueryString["propertyForUpdate"];
                   }
                    if (model.ActionKey != "delete-relation-fk")
                    {
                        valueForUpdate = Request.QueryString["valueForUpdate"];
                    }
                    BR.AcademyLevelsBR.Instance.UpdateAssociation(propForUpdate, valueForUpdate, model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());
					
                    if (model.ActionKey == "delete-relation-fk")
                    {
                        MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]);

                        return PartialView("ResultMessageView", message);
                    }
                    else
                    {
                        return Content("ok");
                    }
       
          
                }
                catch (Exception ex)
                {
				        SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
           
                }
            
            }
		
                if (actionEventArgs == null && !model.IsBackground)
                {
                    //if (model.ActionKey != "deletemany"  && model.ActionKey != "deleterelmany")
                    //{
                     //   throw new NotImplementedException("");
                    //}
                }
                else
                {
					if (model.IsBackground == false )
						 replaceResult = actionEventArgs.Object.Result is ActionResult /*actionEventArgs.Object.ReplaceResult*/;
                }
                #endregion
                if (!replaceResult)
                {
                    if (Request != null && Request.IsAjaxRequest())
                    {
						MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]) ;
                        if (model.IsBackground )
                            message.Message = GlobalMessages.THE_PROCESS_HAS_BEEN_STARTED;
                        
                        return PartialView("ResultMessageView", message);                    
					}
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return (ActionResult)actionEventArgs.Object.Result;
                }
            }
            catch (Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null) {
                        message = ex.Data["usermessage"].ToString();
                    }
					 SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }

            }
        }
        //
        // POST: /AcademyLevels/Delete/5
        
			
	
    }
}
namespace MBK.YellowBox.Web.Mvc.Controllers
{
	using MBK.YellowBox.Web.Mvc.Models.Profesors;

    public partial class ProfesorsController : MBK.YellowBox.Web.Mvc.ControllerBase<Models.Profesors.ProfesorModel>
    {

       


	#region partial methods
        ControllerEventArgs<Models.Profesors.ProfesorModel> e = null;
        partial void OnValidating(object sender, ControllerEventArgs<Models.Profesors.ProfesorModel> e);
        partial void OnGettingExtraData(object sender, MyEventArgs<UIModel<Models.Profesors.ProfesorModel>> e);
        partial void OnCreating(object sender, ControllerEventArgs<Models.Profesors.ProfesorModel> e);
        partial void OnCreated(object sender, ControllerEventArgs<Models.Profesors.ProfesorModel> e);
        partial void OnEditing(object sender, ControllerEventArgs<Models.Profesors.ProfesorModel> e);
        partial void OnEdited(object sender, ControllerEventArgs<Models.Profesors.ProfesorModel> e);
        partial void OnDeleting(object sender, ControllerEventArgs<Models.Profesors.ProfesorModel> e);
        partial void OnDeleted(object sender, ControllerEventArgs<Models.Profesors.ProfesorModel> e);
    	partial void OnShowing(object sender, MyEventArgs<UIModel<Models.Profesors.ProfesorModel>> e);
    	partial void OnGettingByKey(object sender, ControllerEventArgs<Models.Profesors.ProfesorModel> e);
        partial void OnTaken(object sender, ControllerEventArgs<Models.Profesors.ProfesorModel> e);
       	partial void OnCreateShowing(object sender, ControllerEventArgs<Models.Profesors.ProfesorModel> e);
		partial void OnEditShowing(object sender, ControllerEventArgs<Models.Profesors.ProfesorModel> e);
		partial void OnDetailsShowing(object sender, ControllerEventArgs<Models.Profesors.ProfesorModel> e);
 		partial void OnActionsCreated(object sender, MyEventArgs<UIModel<Models.Profesors.ProfesorModel >> e);
		partial void OnCustomActionExecuting(object sender, MyEventArgs<ContextActionModel<Models.Profesors.ProfesorModel>> e);
		partial void OnCustomActionExecutingBackground(object sender, MyEventArgs<ContextActionModel<Models.Profesors.ProfesorModel>> e);
        partial void OnDownloading(object sender, MyEventArgs<ContextActionModel<Models.Profesors.ProfesorModel>> e);
      	partial void OnAuthorization(object sender, AuthorizationContext context);
		 partial void OnFilterShowing(object sender, MyEventArgs<UIModel<Models.Profesors.ProfesorModel >> e);
         partial void OnSummaryOperationShowing(object sender, MyEventArgs<UIModel<Models.Profesors.ProfesorModel>> e);

        partial void OnExportActionsCreated(object sender, MyEventArgs<UIModel<Models.Profesors.ProfesorModel>> e);


		protected override void OnVirtualFilterShowing(object sender, MyEventArgs<UIModel<ProfesorModel>> e)
        {
            OnFilterShowing(sender, e);
        }
		 public override void OnVirtualExportActionsCreated(object sender, MyEventArgs<UIModel<ProfesorModel>> e)
        {
            OnExportActionsCreated(sender, e);
        }
        public override void OnVirtualDownloading(object sender, MyEventArgs<ContextActionModel<ProfesorModel>> e)
        {
            OnDownloading(sender, e);
        }
        public override void OnVirtualShowing(object sender, MyEventArgs<UIModel<ProfesorModel>> e)
        {
            OnShowing(sender, e);
        }

	#endregion
	#region API
	 public override ActionResult ApiCreateGen(ProfesorModel model, ContextRequest contextRequest)
        {
            return CreateGen(model, contextRequest);
        }

              public override ActionResult ApiGetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJson(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }
        public override ActionResult ApiGetByKeyJson(string id, ContextRequest contextRequest)
        {
            return  GetByKeyJson(id, contextRequest, true);
        }
      
		 public override int ApiGetByCount(string filter, ContextRequest contextRequest)
        {
            return GetByCount(filter, contextRequest);
        }
         protected override ActionResult ApiDeleteGen(List<ProfesorModel> models, ContextRequest contextRequest)
        {
            List<Profesor> objs = new List<Profesor>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                BR.ProfesorsBR.Instance.DeleteBulk(objs, contextRequest);
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        protected override ActionResult ApiUpdateGen(List<ProfesorModel> models, ContextRequest contextRequest)
        {
            List<Profesor> objs = new List<Profesor>();
            foreach (var model in models)
            {
                objs.Add(model.GetBusinessObject());
            }
            try
            {
                foreach (var obj in objs)
                {
                    BR.ProfesorsBR.Instance.Update(obj, contextRequest);

                }
                return Content("ok");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }


	#endregion
#region Validation methods	
	    private void Validations(ProfesorModel model) { 
            #region Remote validations

            #endregion
		}

#endregion
		
 		public AuthorizationContext Authorization(AuthorizationContext context)
        {
            OnAuthorization(this,  context );
            return context ;
        }
		public List<ProfesorModel> GetAll() {
            			var bos = BR.ProfesorsBR.Instance.GetBy("",
					new SFSdotNet.Framework.My.ContextRequest()
					{
						CustomQuery = new SFSdotNet.Framework.My.CustomQuery()
						{
							OrderBy = "FullName",
							SortDirection = SFSdotNet.Framework.Data.SortDirection.Ascending
						}
					});
            			List<ProfesorModel> results = new List<ProfesorModel>();
            ProfesorModel model = null;
            foreach (var bo in bos)
            {
                model = new ProfesorModel();
                model.Bind(bo);
                results.Add(model);
            }
            return results;

        }
        //
        // GET: /Profesors/
		[MyAuthorize("r", "Profesor", "MBKYellowBox", typeof(ProfesorsController))]
		public ActionResult Index()
        {
    		var uiModel = GetContextModel(UIModelContextTypes.ListForm, null);
			ViewBag.UIModel = uiModel;
			uiModel.FilterStart = (string)ViewData["startFilter"];
                    MyEventArgs<UIModel<ProfesorModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<ProfesorModel>>() { Object = uiModel });

			OnExportActionsCreated(this, (me != null ? me : me = new MyEventArgs<UIModel<ProfesorModel>>() { Object = uiModel }));

            if (me != null)
            {
                uiModel = me.Object;
            }
            if (me == null)
                me = new MyEventArgs<UIModel<ProfesorModel>>() { Object = uiModel };
           
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;


            //return View("ListGen");
			return ResolveView(uiModel);
        }
		[MyAuthorize("r", "Profesor", "MBKYellowBox", typeof(ProfesorsController))]
		public ActionResult ListViewGen(string idTab, string fk , string fkValue, string startFilter, ListModes  listmode  = ListModes.SimpleList, PropertyDefinition parentRelationProperty = null, object parentRelationPropertyValue = null )
        {
			ViewData["idTab"] = System.Web.HttpContext.Current.Request.QueryString["idTab"]; 
		 	ViewData["detpop"] = true; // details in popup
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"])) {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"]; 
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
            {
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["startFilter"]))
            {
                ViewData["startFilter"] = Request.QueryString["startFilter"];
            }
			
			UIModel<ProfesorModel> uiModel = GetContextModel(UIModelContextTypes.ListForm, null);

            MyEventArgs<UIModel<ProfesorModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<ProfesorModel>>() { Object = uiModel });
            if (me == null)
                me = new MyEventArgs<UIModel<ProfesorModel>>() { Object = uiModel };
            uiModel.Properties = GetProperties(uiModel);
            uiModel.ContextType = UIModelContextTypes.ListForm;
             uiModel.FilterStart = (string)ViewData["startFilter"];
            Showing(ref uiModel);
            ViewData["startFilter"] = uiModel.FilterStart;
			 if (listmode == ListModes.SimpleList)
                return ResolveView(uiModel);
            else
            {
                ViewData["parentRelationProperty"] = parentRelationProperty;
                ViewData["parentRelationPropertyValue"] = parentRelationPropertyValue;
                return PartialView("ListForTagSelectView");
            }
            return ResolveView(uiModel);
        }
		List<PropertyDefinition> _properties = null;

		 protected override List<PropertyDefinition> GetProperties(UIModel uiModel,  params string[] specificProperties)
        { 
            return GetProperties(uiModel, false, null, specificProperties);
        }

		protected override List<PropertyDefinition> GetProperties(UIModel uiModel, bool decripted, Guid? id, params string[] specificProperties)
            {

			bool allProperties = true;    
                if (specificProperties != null && specificProperties.Length > 0)
                {
                    allProperties = false;
                }


			List<CustomProperty> customProperties = new List<CustomProperty>();
			if (_properties == null)
                {
                List<PropertyDefinition> results = new List<PropertyDefinition>();

			string idProfesor = GetRouteDataOrQueryParam("id");
			if (idProfesor != null)
			{
				if (!decripted)
                {
					idProfesor = SFSdotNet.Framework.Entities.Utils.GetPropertyKey(idProfesor.Replace("-","/"), "GuidProfesor");
				}else{
					if (id != null )
						idProfesor = id.Value.ToString();                

				}
			}

			bool visibleProperty = true;	
			 bool conditionalshow =false;
                if (uiModel.ContextType == UIModelContextTypes.EditForm || uiModel.ContextType == UIModelContextTypes.DisplayForm ||  uiModel.ContextType == UIModelContextTypes.GenericForm )
                    conditionalshow = true;
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidProfesor"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidProfesor")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 100,
																	
					CustomProperties = customProperties,

                    PropertyName = "GuidProfesor",

					 MaxLength = 0,
					IsRequired = true ,
					IsHidden = true,
                    SystemProperty =  SystemProperties.Identifier ,
					IsDefaultProperty = false,
                    SortBy = "GuidProfesor",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.ProfesorResources.GUIDPROFESOR*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("FullName"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "FullName")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 101,
																	
					CustomProperties = customProperties,

                    PropertyName = "FullName",

					 MaxLength = 255,
					 Nullable = true,
					IsDefaultProperty = true,
                    SortBy = "FullName",
					
	
                    TypeName = "String",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.ProfesorResources.FULLNAME*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("GuidAcademyLevel"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "GuidAcademyLevel")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 102,
																	IsForeignKey = true,

									
					CustomProperties = customProperties,

                    PropertyName = "GuidAcademyLevel",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "GuidAcademyLevel",
					
	
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.ProfesorResources.GUIDACADEMYLEVEL*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 103,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedDate",
					
	
				SystemProperty = SystemProperties.CreatedDate ,
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.ProfesorResources.CREATEDDATE*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedDate"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedDate")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 116,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedDate",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedDate",
					
	
					IsUpdatedDate = true,
					SystemProperty = SystemProperties.UpdatedDate ,
	
                    TypeName = "DateTime",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    ,PropertyDisplayName = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.UPDATED

                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("CreatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "CreatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 105,
																	
					CustomProperties = customProperties,

                    PropertyName = "CreatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "CreatedBy",
					
	
				SystemProperty = SystemProperties.CreatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.ProfesorResources.CREATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("UpdatedBy"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "UpdatedBy")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 106,
																	
					CustomProperties = customProperties,

                    PropertyName = "UpdatedBy",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "UpdatedBy",
					
	
				SystemProperty = SystemProperties.UpdatedUser,
                    TypeName = "Guid",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.ProfesorResources.UPDATEDBY*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Bytes"))
{				
    customProperties = new List<CustomProperty>();

        
	
	//Null
		//if (this.Request.QueryString["fk"] != "Bytes")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 107,
																	
					CustomProperties = customProperties,

                    PropertyName = "Bytes",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Bytes",
					
	
				SystemProperty = SystemProperties.SizeBytes,
                    TypeName = "Int32",
                    IsNavigationProperty = false,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/"
                    /*,PropertyDisplayName = Resources.ProfesorResources.BYTES*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("AcademyLevel"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"Profesors" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="AcademyLevel.GuidAcademyLevel")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"AcademyLevel.GuidAcademyLevel" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"AcademyLevels" });
			

	
	//fk_Profesor_AcademyLevel
		//if (this.Request.QueryString["fk"] != "AcademyLevel")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 108,
																
					
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "AcademyLevel",
					PropertyNavigationKey = "GuidAcademyLevel",
					PropertyNavigationText = "Title",
					NavigationPropertyType = NavigationPropertyTypes.SimpleDropDown,
					GetMethodName = "GetAll",
					GetMethodParameters = "",
					GetMethodDisplayText ="Title",
					GetMethodDisplayValue = "GuidAcademyLevel",
					
					CustomProperties = customProperties,

                    PropertyName = "AcademyLevel",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "AcademyLevel.Title",
					
	
                    TypeName = "MBKYellowBoxModel.AcademyLevel",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = false,
                    PathName = "MBKYellowBox/AcademyLevels"
                    /*,PropertyDisplayName = Resources.ProfesorResources.ACADEMYLEVEL*/
                });
		//	}
	
	}
visibleProperty =allProperties;
if (visibleProperty || specificProperties.Contains("Students"))
{				
    customProperties = new List<CustomProperty>();

        			customProperties.Add(new CustomProperty() { Name="Fk", Value=@"Profesor" });
			//[RelationFilterable(DisableFilterableInSubfilter=true, FiltrablePropertyPathName="Students.GuidStudent")]		
			customProperties.Add(new CustomProperty() { Name="FiltrablePropertyPathName", Value=@"Students.GuidStudent" });
			customProperties.Add(new CustomProperty() { Name = "BusinessObjectSetName", Value = @"Students" });
			

	
	//fk_Student_Profesor
		//if (this.Request.QueryString["fk"] != "Students")
        //	{
				results.Add(new PropertyDefinition()
                {
					Order = 109,
																
					Link = VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/Students/ListViewGen?overrideModule=" + GetOverrideApp()  + "&pal=False&es=False&pag=10&filterlinks=1&idTab=Students&fk=Profesor&startFilter="+ (new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext)).Encode("it.Profesor.GuidProfesor = Guid(\"" + idProfesor +"\")")+ "&fkValue=" + idProfesor,
					ModuleKey = "MBKYellowBox",
					BusinessObjectKey = "Student",
					
					CustomProperties = customProperties,

                    PropertyName = "Students",

					 MaxLength = 0,
					 Nullable = true,
					IsDefaultProperty = false,
                    SortBy = "Students.FullName",
					
	
                    TypeName = "MBKYellowBoxModel.Student",
                    IsNavigationProperty = true,
					IsNavigationPropertyMany = true,
                    PathName = "MBKYellowBox/Students"
                    /*,PropertyDisplayName = Resources.ProfesorResources.STUDENTS*/
                });
		//	}
	
	}
	
				
                    _properties = results;
                    return _properties;
                }
                else {
                    return _properties;
                }
            }

		protected override  UIModel<ProfesorModel> GetByForShow(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, params  object[] extraParams)
        {
			if (Request != null )
				if (!string.IsNullOrEmpty(Request.QueryString["q"]))
					filter = filter + HttpUtility.UrlDecode(Request.QueryString["q"]);
 if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
            }
            var bos = BR.ProfesorsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), contextRequest, extraParams);
			//var bos = BR.ProfesorsBR.Instance.GetBy(HttpUtility.UrlDecode(filter), pageSize, page, orderBy, orderDir, GetUseMode(), context, extraParams);
            ProfesorModel model = null;
            List<ProfesorModel> results = new List<ProfesorModel>();
            foreach (var item in bos)
            {
                model = new ProfesorModel();
				model.Bind(item);
				results.Add(model);
            }
            //return results;
			UIModel<ProfesorModel> uiModel = GetContextModel(UIModelContextTypes.Items, null);
            uiModel.Items = results;
			if (Request != null){
				if (SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(Request.RequestContext, "action") == "Download")
				{
					uiModel.ContextType = UIModelContextTypes.ExportDownload;
				}
			}
            Showing(ref uiModel);
            return uiModel;
		}			
		
		//public List<ProfesorModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params  object[] extraParams)
        //{
		//	var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, null, extraParams);
        public override List<ProfesorModel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest,  params  object[] extraParams)
        {
            var uiModel = GetByForShow(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
           
            return uiModel.Items;
		
        }
		/*
        [MyAuthorize("r", "Profesor", "MBKYellowBox", typeof(ProfesorsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir)
        {
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir);
        }*/

		  [MyAuthorize("r", "Profesor", "MBKYellowBox", typeof(ProfesorsController))]
		public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir,ContextRequest contextRequest,  object[] extraParams)
        {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
			 return GetByJsonBase(filter, pageSize, page, orderBy, orderDir,contextRequest, extraParams);
        }
/*		  [MyAuthorize("r", "Profesor", "MBKYellowBox", typeof(ProfesorsController))]
       public ContentResult GetByJson(string filter, int? pageSize, int? page, string orderBy, string orderDir, ContextRequest contextRequest, object[] extraParams)
        {
            return GetByJsonBase(filter, pageSize, page, orderBy, orderDir, contextRequest, extraParams);
        }*/
		[MyAuthorize()]
		public int GetByCount(string filter, ContextRequest contextRequest) {
			if (contextRequest == null || contextRequest.Company == null || contextRequest.User == null )
            {
                contextRequest = GetContextRequest();
            }
            return BR.ProfesorsBR.Instance.GetCount(HttpUtility.UrlDecode(filter), GetUseMode(), contextRequest);
        }
		

		[MyAuthorize("r", "Profesor", "MBKYellowBox", typeof(ProfesorsController))]
        public ActionResult GetByKeyJson(string id, ContextRequest contextRequest,  bool dec = false)
        {
            return Json(GetByKey(id, null, contextRequest, dec), JsonRequestBehavior.AllowGet);
        }
		public ProfesorModel GetByKey(string id) {
			return GetByKey(id, null,null, false);
       	}
		    public ProfesorModel GetByKey(string id, string includes)
        {
            return GetByKey(id, includes, false);
        }
		 public  ProfesorModel GetByKey(string id, string includes, ContextRequest contextRequest)
        {
            return GetByKey(id, includes, contextRequest, false);
        }
		/*
		  public ActionResult ShowField(string fieldName, string idField) {
		   string safePropertyName = fieldName;
              if (fieldName.StartsWith("Fk"))
              {
                  safePropertyName = fieldName.Substring(2, fieldName.Length - 2);
              }

             ProfesorModel model = new  ProfesorModel();

            UIModel uiModel = GetUIModel(model, new string[] { "NoField-" });
			
				uiModel.Properties = GetProperties(uiModel, safePropertyName);
		uiModel.Properties.ForEach(p=> p.ContextType = uiModel.ContextType );
            uiModel.ContextType = UIModelContextTypes.FilterFields;
            uiModel.OverrideApp = GetOverrideApp();
            uiModel.UseMode = GetUseMode();

            ViewData["uiModel"] = uiModel;
			var prop = uiModel.Properties.FirstOrDefault(p=>p.PropertyName == safePropertyName);
            //if (prop.IsNavigationProperty && prop.IsNavigationPropertyMany == false)
            //{
            //    ViewData["currentProperty"] = uiModel.Properties.FirstOrDefault(p => p.PropertyName != fieldName + "Text");
            //}else if (prop.IsNavigationProperty == false){
                ViewData["currentProperty"] = prop;
           // }
            ((PropertyDefinition)ViewData["currentProperty"]).RemoveLayout = true;
			ViewData["withContainer"] = false;


            return PartialView("GenericField", model);


        }
      */
	public ProfesorModel GetByKey(string id, ContextRequest contextRequest, bool dec)
        {
            return GetByKey(id, null, contextRequest, dec);
        }
        public ProfesorModel GetByKey(string id, string  includes, bool dec)
        {
            return GetByKey(id, includes, null, dec);
        }

        public ProfesorModel GetByKey(string id, string includes, ContextRequest contextRequest, bool dec) {
		             ProfesorModel model = null;
            ControllerEventArgs<ProfesorModel> e = null;
			string objectKey = id.Replace("-","/");
             OnGettingByKey(this, e=  new ControllerEventArgs<ProfesorModel>() { Id = objectKey  });
             bool cancel = false;
             ProfesorModel eItem = null;
             if (e != null)
             {
                 cancel = e.Cancel;
                 eItem = e.Item;
             }
			if (cancel == false && eItem == null)
             {
			Guid guidProfesor = Guid.Empty; //new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, "GuidProfesor"));
			if (dec)
                 {
                     guidProfesor = new Guid(id);
                 }
                 else
                 {
                     guidProfesor = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey, null));
                 }
			
            
				model = new ProfesorModel();
                  if (contextRequest == null)
                {
                    contextRequest = GetContextRequest();
                }
				var bo = BR.ProfesorsBR.Instance.GetByKey(guidProfesor, GetUseMode(), contextRequest,  includes);
				 if (bo != null)
                    model.Bind(bo);
                else
                    return null;
			}
             else {
                 model = eItem;
             }
			model.IsNew = false;

            return model;
        }
        // GET: /Profesors/DetailsGen/5
		[MyAuthorize("r", "Profesor", "MBKYellowBox", typeof(ProfesorsController))]
        public ActionResult DetailsGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = ProfesorResources.ENTITY_PLURAL;
			 #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<ProfesorModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
            #endregion



			 bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null) {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
			//UIModel<ProfesorModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, decripted), decripted, guidId);
			var item = GetByKey(id, null, null, decripted);
			if (item == null)
            {
                 RouteValueDictionary rv = new RouteValueDictionary();
                string usemode = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"usemode");
                string overrideModule = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "overrideModule");
                string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");

                if(!string.IsNullOrEmpty(usemode)){
                    rv.Add("usemode", usemode);
                }
                if(!string.IsNullOrEmpty(overrideModule)){
                    rv.Add("overrideModule", overrideModule);
                }
                if (!string.IsNullOrEmpty(area))
                {
                    rv.Add("area", area);
                }

                return RedirectToAction("Index", rv);
            }
            //
            UIModel<ProfesorModel> uiModel = null;
                uiModel = GetContextModel(UIModelContextTypes.DisplayForm, item, decripted, guidId);



            MyEventArgs<UIModel<ProfesorModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<ProfesorModel>>() { Object = uiModel });

            if (me != null) {
                uiModel = me.Object;
            }
			
            Showing(ref uiModel);
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			
            //return View("DisplayGen", uiModel.Items[0]);
			return ResolveView(uiModel, uiModel.Items[0]);

        }
		[MyAuthorize("r", "Profesor", "MBKYellowBox", typeof(ProfesorsController))]
		public ActionResult DetailsViewGen(string id)
        {

		 bool cancel = false; bool replaceResult = false;
            OnDetailsShowing(this, e = new ControllerEventArgs<ProfesorModel>() { Id = id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                   return e.ActionResult;
            }
           
			if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
 			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
           
        	 //var uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id));
			 
            bool decripted = false;
            Guid? guidId = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
            {
                if (System.Web.HttpContext.Current.Request.QueryString["dec"] == "true")
                {
                    decripted = true;
                    guidId = Guid.Parse(id);
                }
            }
            UIModel<ProfesorModel> uiModel = GetContextModel(UIModelContextTypes.DisplayForm, GetByKey(id, null, null, decripted), decripted, guidId);
			

            MyEventArgs<UIModel<ProfesorModel>> me = null;

            OnActionsCreated(this, me = new MyEventArgs<UIModel<ProfesorModel>>() { Object = uiModel });

            if (me != null)
            {
                uiModel = me.Object;
            }
            
            Showing(ref uiModel);
            return ResolveView(uiModel, uiModel.Items[0]);
        
        }
        //
        // GET: /Profesors/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        //
        // GET: /Profesors/CreateGen
		[MyAuthorize("c", "Profesor", "MBKYellowBox", typeof(ProfesorsController))]
        public ActionResult CreateGen()
        {
			ProfesorModel model = new ProfesorModel();
            model.IsNew = true;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model);

			OnCreateShowing(this, e = new ControllerEventArgs<ProfesorModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }

             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                 ViewData["ispopup"] = true;
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                 ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
             if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                 ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

            Showing(ref me);

			return ResolveView(me, me.Items[0]);
        } 
			
		protected override UIModel<ProfesorModel> GetContextModel(UIModelContextTypes formMode, ProfesorModel model)
        {
            return GetContextModel(formMode, model, false, null);
        }
			
		 private UIModel<ProfesorModel> GetContextModel(UIModelContextTypes formMode, ProfesorModel model, bool decript, Guid ? id) {
            UIModel<ProfesorModel> me = new UIModel<ProfesorModel>(true, "Profesors");
			me.UseMode = GetUseMode();
			me.Controller = this;
			me.OverrideApp = GetOverrideApp();
			me.ContextType = formMode ;
			me.Id = "Profesor";
			
            me.ModuleKey = "MBKYellowBox";

			me.ModuleNamespace = "MBK.YellowBox";
            me.EntityKey = "Profesor";
            me.EntitySetName = "Profesors";

			me.AreaAction = "MBKYellowBox";
            me.ControllerAction = "Profesors";
            me.PropertyKeyName = "GuidProfesor";

            me.Properties = GetProperties(me, decript, id);

			me.SortBy = "UpdatedDate";
			me.SortDirection = UIModelSortDirection.DESC;

 			
			if (Request != null)
            {
                string actionName = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam( Request.RequestContext, "action");
                if(actionName != null && actionName.ToLower().Contains("create") ){
                    me.IsNew = true;
                }
            }
			 #region Buttons
			 if (Request != null ){
             if (formMode == UIModelContextTypes.DisplayForm || formMode == UIModelContextTypes.EditForm || formMode == UIModelContextTypes.ListForm)
				me.ActionButtons = GetActionButtons(formMode,model != null ?(Request.QueryString["dec"] == "true" ? model.Id : model.SafeKey)  : null, "MBKYellowBox", "Profesors", "Profesor", me.IsNew);

            //me.ActionButtons.Add(new ActionModel() { ActionKey = "return", Title = GlobalMessages.RETURN, Url = System.Web.VirtualPathUtility.ToAbsolute("~/") + "MBKYellowBox/Profesors" });
			if (this.HttpContext != null &&  !this.HttpContext.SkipAuthorization){
				//antes this.HttpContext
				me.SetAction("u", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("u", "Profesor", "MBKYellowBox"));
				me.SetAction("c", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("c", "Profesor", "MBKYellowBox"));
				me.SetAction("d", (new SFSdotNet.Framework.Globals.Security.Permission()).IsAllowed("d", "Profesor", "MBKYellowBox"));
			
			}else{
				me.SetAction("u", true);
				me.SetAction("c", true);
				me.SetAction("d", true);

			}
            #endregion              
         
            switch (formMode)
            {
                case UIModelContextTypes.DisplayForm:
					//me.TitleForm = ProfesorResources.PROFESORS_DETAILS;
                    me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.MODIFY_DATA;
					 me.Properties.Where(p=>p.PropertyName  != "Id" && p.IsForeignKey == false).ToList().ForEach(p => p.IsHidden = false);

					 me.Properties.Where(p => (p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier) ).ToList().ForEach(p=> me.SetHide(p.PropertyName));

                    break;
                case UIModelContextTypes.EditForm:
				  me.Properties.Where(p=>p.SystemProperty != SystemProperties.Identifier && p.IsForeignKey == false && p.PropertyName != "Id").ToList().ForEach(p => p.IsHidden = false);

					if (model != null)
                    {
						

                        me.ActionButtons.First(p => p.ActionKey == "u").Title = GlobalMessages.SAVE_DATA;                        
                        me.ActionButtons.First(p => p.ActionKey == "c").Title = GlobalMessages.SAVE_DATA;
						if (model.IsNew ){
							//me.TitleForm = ProfesorResources.PROFESORS_ADD_NEW;
							me.ActionName = "CreateGen";
							me.Properties.RemoveAll(p => p.SystemProperty != null || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));
						}else{
							
							me.ActionName = "EditGen";

							//me.TitleForm = ProfesorResources.PROFESORS_EDIT;
							me.Properties.RemoveAll(p => p.SystemProperty != null && p.SystemProperty != SystemProperties.Identifier || (p.IsNavigationPropertyMany && p.NavigationPropertyType != NavigationPropertyTypes.Tags));	
						}
						//me.Properties.Remove(me.Properties.Find(p => p.PropertyName == "UpdatedDate"));
					
					}
                    break;
                case UIModelContextTypes.FilterFields:
                    break;
                case UIModelContextTypes.GenericForm:
                    break;
                case UIModelContextTypes.Items:
				//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "FullName") != null){
						me.Properties.Find(p => p.PropertyName == "FullName").IsHidden = false;
					 }
					 
                    
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					 
                    
					

						 if (me.Properties.Find(p => p.PropertyName == "GuidProfesor") != null){
						me.Properties.Find(p => p.PropertyName == "GuidProfesor").IsHidden = false;
					 }
					 
                    
					


                  


					//}
                    break;
                case UIModelContextTypes.ListForm:
					PropertyDefinition propFinded = null;
					//if (Request.QueryString["allFields"] != "1"){
					 if (me.Properties.Find(p => p.PropertyName == "FullName") != null){
						me.Properties.Find(p => p.PropertyName == "FullName").IsHidden = false;
					 }
					
					 if (me.Properties.Find(p => p.PropertyName == "UpdatedDate") != null){
						me.Properties.Find(p => p.PropertyName == "UpdatedDate").IsHidden = false;
					 }
					
					me.PrincipalActionName = "GetByJson";
					//}
					//me.TitleForm = ProfesorResources.PROFESORS_LIST;
                    break;
                default:
                    break;
            }
            	this.SetDefaultProperties(me);
			}
			if (model != null )
            	me.Items.Add(model);
            return me;
        }
		// GET: /Profesors/CreateViewGen
		[MyAuthorize("c", "Profesor", "MBKYellowBox", typeof(ProfesorsController))]
        public ActionResult CreateViewGen()
        {
				ProfesorModel model = new ProfesorModel();
            model.IsNew = true;
			e= null;
			OnCreateShowing(this, e = new ControllerEventArgs<ProfesorModel>() { Item = model });
   			if (e != null)
            {
                model = e.Item;
                if (e.ActionResult != null)
                    return e.ActionResult;
            }
			
            var me = GetContextModel(UIModelContextTypes.EditForm, model);

			me.IsPartialView = true;	
            if(!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
            {
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                me.Properties.Find(p => p.PropertyName == ViewData["fk"].ToString()).IsReadOnly = true;
            }
			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];
			
      
            //me.Items.Add(model);
            Showing(ref me);
            return ResolveView(me, me.Items[0]);
        }
		protected override  void GettingExtraData(ref UIModel<ProfesorModel> uiModel)
        {

            MyEventArgs<UIModel<ProfesorModel>> me = null;
            OnGettingExtraData(this, me = new MyEventArgs<UIModel<ProfesorModel>>() { Object = uiModel });
            //bool maybeAnyReplaced = false; 
            if (me != null)
            {
                uiModel = me.Object;
                //maybeAnyReplaced = true;
            }
           
			bool canFill = false;
			 string query = null ;
            bool isFK = false;
			PropertyDefinition prop =null;
			var contextRequest = this.GetContextRequest();
            contextRequest.CustomParams.Add(new CustomParam() { Name="ui", Value= Profesor.EntityName });

            			canFill = false;
			 query = "";
            isFK =false;
			prop = uiModel.Properties.FirstOrDefault(p => p.PropertyName == "AcademyLevel");
			if (prop != null)
				if (prop.IsHidden == false && (prop.ContextType == UIModelContextTypes.EditForm || prop.ContextType == UIModelContextTypes.FilterFields  || (prop.ContextType == null && uiModel.ContextType == UIModelContextTypes.EditForm)))
				{
					if (prop.NavigationPropertyType == NavigationPropertyTypes.SimpleDropDown )
						canFill = true;
				}
                else if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidAcademyLevel = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidAcademyLevel = @GuidAcademyLevel";
					
					canFill = true;
                }
				if (prop.IsHidden == false && UsingFrom(prop.PropertyName) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))
                {
                    isFK = true;
                    // es prop FK y se ve
                    //query = "GuidAcademyLevel = Guid(\"" + Request.QueryString["fkValue"] + "\")";
                    query = "GuidAcademyLevel = @GuidAcademyLevel";
					canFill = true;
                }
			if (canFill){
			                contextRequest.CustomQuery = new CustomQuery();

				if (!uiModel.ExtraData.Exists(p => p.PropertyName == "AcademyLevel")) {
					if (!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(Request.QueryString["fkValue"]))				  
						contextRequest.CustomQuery.SetParam("GuidAcademyLevel", new Nullable<Guid>(Guid.Parse( Request.QueryString["fkValue"])));

					 if (isFK == true)
                    {
						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.AcademyLevelsBR()).GetBy(query, contextRequest), "GuidAcademyLevel", "Title", Request.QueryString["fkValue"]), PropertyName = "AcademyLevel" });    
                    }
                    else
                    {

						uiModel.ExtraData.Add(new ExtraData() { Data = new SelectList((IEnumerable)(new MBK.YellowBox.BR.AcademyLevelsBR()).GetBy(query, contextRequest), "GuidAcademyLevel", "Title"), PropertyName = "AcademyLevel" });    

					}
    if (isFK)
                    {    
						var FkAcademyLevel = ((SelectList)uiModel.ExtraData.First(p => p.PropertyName == "AcademyLevel").Data).First();
						uiModel.Items[0].GetType().GetProperty("FkAcademyLevelText").SetValue(uiModel.Items[0], FkAcademyLevel.Text);
						uiModel.Items[0].GetType().GetProperty("FkAcademyLevel").SetValue(uiModel.Items[0], Guid.Parse(FkAcademyLevel.Value));
                    
					}    
				}
			}
		 
            

        }
		private void Showing(ref UIModel<ProfesorModel> uiModel) {
          	
			MyEventArgs<UIModel<ProfesorModel>> me = new MyEventArgs<UIModel<ProfesorModel>>() { Object = uiModel };
			 OnVirtualLayoutSettings(this, me);


            OnShowing(this, me);

			
			if ((Request != null && Request.QueryString["allFields"] == "1") || Request == null )
			{
				me.Object.Properties.ForEach(p=> p.IsHidden = false);
            }
            if (me != null)
            {
                uiModel = me.Object;
            }
          


			 if (uiModel.ContextType == UIModelContextTypes.EditForm)
			    GettingExtraData(ref uiModel);
            ViewData["UIModel"] = uiModel;

        }
        //
        // POST: /Profesors/Create
		[MyAuthorize("c", "Profesor", "MBKYellowBox", typeof(ProfesorsController))]
        [HttpPost]
		[ValidateInput(false)] 
        public ActionResult CreateGen(ProfesorModel  model,  ContextRequest contextRequest)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
		 	e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<ProfesorModel>() { Item = model });
           
		  	if (!ModelState.IsValid) {
				model.IsNew = true;
				var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
                 if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                        ViewData["ispopup"] = true;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
                    return ResolveView(me, model);
            }
            try
            {
				if (model.GuidProfesor == null || model.GuidProfesor.ToString().Contains("000000000"))
				model.GuidProfesor = Guid.NewGuid();
	
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnCreating(this, e = new ControllerEventArgs<ProfesorModel>() { Item = model });
                if (e != null) {
                   if (e.Cancel && e.RedirectValues.Count > 0){
                        RouteValueDictionary rv = new RouteValueDictionary();
                        if (e.RedirectValues["area"] != null ){
                            rv.Add("area", e.RedirectValues["area"].ToString());
                        }
                        foreach (var item in e.RedirectValues.Where(p=>p.Key != "area" && p.Key != "controller" &&  p.Key != "action" ))
	                    {
		                    rv.Add(item.Key, item.Value);
	                    }

                        //if (e.RedirectValues["action"] != null && e.RedirectValues["controller"] != null && e.RedirectValues["area"] != null )
                        return RedirectToAction(e.RedirectValues["action"].ToString(), e.RedirectValues["controller"].ToString(), rv );


                        
                    }else if (e.Cancel && e.ActionResult != null )
                        return e.ActionResult;  
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				if (contextRequest == null || contextRequest.Company == null){
					contextRequest = GetContextRequest();
					
				}
                if (!cancel)
                	model.Bind(ProfesorsBR.Instance.Create(model.GetBusinessObject(), contextRequest ));
				OnCreated(this, e = new ControllerEventArgs<ProfesorModel>() { Item = model });
                 if (e != null )
					if (e.ActionResult != null)
                    	replaceResult = true;		
				if (!replaceResult)
                {
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))
                    {
                        ViewData["__continue"] = true;
                    }
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"]) &&  string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
                        popupextra.Add("id", model.SafeKey);
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                       {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                            popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext,"area"));
                            popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                            popupextra.Add("action", actionDetails);
                       if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
                    {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"]))

                        {
                            var popupextra = GetRouteData();
                            popupextra.Add("id", model.SafeKey);
                            return RedirectToAction("EditViewGen", popupextra);
                        }
                        else
                        {
                            return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.ADD_DONE));
                        }
                    }        			if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                        return Redirect(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]);
                    else{

							RouteValueDictionary popupextra = null; 
							if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"])){
                            popupextra = GetRouteData();
							 string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
                            if (!string.IsNullOrEmpty(area))
                                popupextra.Add("area", area);
                            
                            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue"])) {
								popupextra.Add("id", model.SafeKey);
                                return RedirectToAction("EditGen", popupextra);

                            }else{
								
                            return RedirectToAction("Index", popupextra);
							}
							}else{
								return Content("ok");
							}
                        }
						 }
                else {
                    return e.ActionResult;
                    }
				}
            catch(Exception ex)
            {
					if (!string.IsNullOrEmpty(Request.QueryString["rok"]))
                {
                    throw  ex;
                }
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                model.IsNew = true;
                var me = GetContextModel(UIModelContextTypes.EditForm, model, true, model.GuidProfesor);
                Showing(ref me);
                if (isPopUp)
                {
                    
                        ViewData["ispopup"] = isPopUp;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                        ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                        ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

                    return ResolveView(me, model);
                }
                else
					if (Request != null)
						return ResolveView(me, model);
					else
						return Content("ok");
            }
        }        
        //
        // GET: /Profesors/Edit/5 
        public ActionResult Edit(int id)
        {
            return View();
        }
			
		
		[MyAuthorize("u", "Profesor", "MBKYellowBox", typeof(ProfesorsController))]
		[MvcSiteMapNode(Area="MBKYellowBox", Title="sss", Clickable=false, ParentKey = "MBKYellowBox_Profesor_List")]
		public ActionResult EditGen(string id)
        {
			//if (System.Web.SiteMap.CurrentNode != null)
			//	System.Web.SiteMap.CurrentNode.Title = ProfesorResources.ENTITY_SINGLE;		 	
  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<ProfesorModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
            ProfesorModel model = null;
            // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
			bool dec = false;
            Guid ? idGuidDecripted = null ;
            if (Request != null && Request.QueryString["dec"] == "true")
            {
                dec = true;
                idGuidDecripted = Guid.Parse(id);
            }

            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
			 var me = GetContextModel(UIModelContextTypes.EditForm, model,dec,idGuidDecripted);
            Showing(ref me);


            if (!replaceResult)
            {
                 //return View("EditGen", me.Items[0]);
				 return ResolveView(me, me.Items[0]);
            }
            else {
                return e.ActionResult;
            }
        }
			[MyAuthorize("u", "Profesor","MBKYellowBox", typeof(ProfesorsController))]
		public ActionResult EditViewGen(string id)
        {
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
                ViewData["ispopup"] = true;
			  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
                ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
                ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

					  // habilitando m�todo parcial
            #region implementaci�n de m�todo parcial

            bool cancel = false; bool replaceResult = false;
            OnEditShowing(this, e = new ControllerEventArgs<ProfesorModel>() { Id= id });
            if (e != null)
            {
                if (e.Cancel && e.ActionResult != null)
                    return e.ActionResult;
                else if (e.Cancel == true)
                    cancel = true;
                else if (e.ActionResult != null)
                    replaceResult = true;
            }
            #endregion
			
            ProfesorModel model = null;
			 bool dec = false;
            Guid? guidId = null ;

            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null && System.Web.HttpContext.Current.Request.QueryString["dec"] == "true") {
                dec = true;
                guidId = Guid.Parse(id);
            }
            // si fue implementado el método parcial y no se ha decidido suspender la acción
            if (!cancel)
                model = GetByKey(id, null, null, dec);
            else
                model = e.Item;
            var me = GetContextModel(UIModelContextTypes.EditForm, model, dec, guidId);
            Showing(ref me);

            return ResolveView(me, model);
        }
		[MyAuthorize("u", "Profesor",  "MBKYellowBox", typeof(ProfesorsController))]
		[HttpPost]
		[ValidateInput(false)] 
		        public ActionResult EditGen(ProfesorModel model)
        {
			bool isPopUp = false;
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
            {
                isPopUp = true;
            }
			e = null;
			this.Validations(model);

            OnValidating(this, e = new ControllerEventArgs<ProfesorModel>() { Item = model });
           
            if (!ModelState.IsValid)
            {
			   	var me = GetContextModel(UIModelContextTypes.EditForm, model);
                Showing(ref me);
			
				if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"])){
                	ViewData["ispopup"] = true;
					return ResolveView(me, model);
				}
				else
					return ResolveView(me, model);
            }
            try
            {
			
				// habilitando m�todo parcial
                #region implementaci�n de m�todo parcial
               
                bool cancel = false; bool replaceResult = false;
                OnEditing(this, e = new ControllerEventArgs<ProfesorModel>() { Item = model });
                if (e != null) {
                    if (e.Cancel && e.ActionResult != null)
                        return e.ActionResult;
                    else if (e.Cancel == true)
                        cancel = true;
                    else if (e.ActionResult != null)
                        replaceResult = true;
                }
                #endregion
                // si fue implementado el m�todo parcial y no se ha decidido suspender la acci�n
				ContextRequest context = new ContextRequest();
                context.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;

                Profesor resultObj = null;
			    if (!cancel)
                	resultObj = ProfesorsBR.Instance.Update(model.GetBusinessObject(), GetContextRequest());
				
				OnEdited(this, e = new ControllerEventArgs<ProfesorModel>() { Item =   new ProfesorModel(resultObj) });
				if (e != null && e.ActionResult != null) replaceResult = true; 

                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Content("ok");
                }
                else
                {
				if (!replaceResult)
                {
					if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["__continue_details"])  && string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                    {
                        var popupextra = GetRouteData();
						 if (Request != null && Request.QueryString["dec"] == "true")
                        {
                            popupextra.Add("id", model.Id);
                        }
                        else
                        {
							popupextra.Add("id", model.SafeKey);

							
                        }
                        string actionDetails = "DetailsGen";
                        if (this.IsPopup())
                        {
                            popupextra.Add("saved", "true");
                            actionDetails = "DetailsViewGen";
                        }
                        popupextra.Add("area", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area"));
                        popupextra.Add("controller", SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "controller"));
                        popupextra.Add("action", actionDetails);
                        if (popupextra.ContainsKey("usemode"))
                        {

                            return RedirectToRoute("area_usemode", popupextra);
                        }
                        else
                        {
                            return RedirectToAction(actionDetails, popupextra);
                        }
                    }
					if (isPopUp)
						return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.UPDATE_DONE));
        			    string returnUrl = null;
                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]) || !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"])) {
                        if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.Form["ReturnAfter"];
                        else if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"]))
                            returnUrl = System.Web.HttpContext.Current.Request.QueryString["ReturnAfter"];
                    }
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else{
		RouteValueDictionary popupextra = null; 
						 if (Request != null && string.IsNullOrEmpty(Request.QueryString["rok"]))
                            {
							
							popupextra = GetRouteData();
							string area = SFSdotNet.Framework.Web.Mvc.Utils.GetRouteDataOrQueryParam(this.Request.RequestContext, "area");
							if (!string.IsNullOrEmpty(area))
								popupextra.Add("area", area);

							return RedirectToAction("Index", popupextra);
						}else{
							return Content("ok");
						}
						}
				 }
                else {
                    return e.ActionResult;
				}
                }		
            }
          catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
			    if (isPopUp)
                {
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(ex.Message));
                    
                }
                else
                {
                    SFSdotNet.Framework.My.Context.CurrentContext.AddMessage(ex.Message, SFSdotNet.Framework.My.MessageResultTypes.Error);
                    
                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["autosave"]))
                {
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
                else {
						  if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["popup"]))
							ViewData["ispopup"] = true;
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fk"]))
							ViewData["fk"] = System.Web.HttpContext.Current.Request.QueryString["fk"];
						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.QueryString["fkValue"]))
							ViewData["fkValue"] = System.Web.HttpContext.Current.Request.QueryString["fkValue"];

						var me = GetContextModel(UIModelContextTypes.EditForm, model);
						Showing(ref me);

						if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["popup"]))
						{
							ViewData["ispopup"] = true;
							return ResolveView(me, model);
						}
						else
							return ResolveView(me, model);

						
					}
				}
            }
        }
        //
        // POST: /Profesors/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                //  Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Profesors/Delete/5
        
		[MyAuthorize("d", "Profesor", "MBKYellowBox", typeof(ProfesorsController))]
		[HttpDelete]
        public ActionResult DeleteGen(string objectKey, string extraParams)
        {
            try
            {
					
			
				Guid guidProfesor = new Guid(SFSdotNet.Framework.Entities.Utils.GetPropertyKey(objectKey.Replace("-", "/"), "GuidProfesor")); 
                BO.Profesor entity = new BO.Profesor() { GuidProfesor = guidProfesor };

                BR.ProfesorsBR.Instance.Delete(entity, GetContextRequest());               
                return PartialView("ResultMessageView", (new MessageModel()).GetDone(GlobalMessages.DELETE_DONE));

            }
            catch(Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null)
                    {
                        message = ex.Data["usermessage"].ToString();
                    }

                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }
            }
        }
		/*[MyAuthorize()]
		public FileMediaResult Download(string query, bool? allSelected = false,  string selected = null , string orderBy = null , string direction = null , string format = null , string actionKey=null )
        {
			
            List<Guid> keysSelected1 = new List<Guid>();
            if (!string.IsNullOrEmpty(selected)) {
                foreach (var keyString in selected.Split(char.Parse("|")))
                {
				
                    keysSelected1.Add(Guid.Parse(keyString));
                }
            }
				
            query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(query, allSelected.Value, selected, "GuidProfesor");
            MyEventArgs<ContextActionModel<ProfesorModel>> eArgs = null;
            List<ProfesorModel> results = GetBy(query, null, null, orderBy, direction, GetContextRequest(), keysSelected1);
            OnDownloading(this, eArgs = new MyEventArgs<ContextActionModel<ProfesorModel>>() { Object = new ContextActionModel<ProfesorModel>() { Query = query, SelectedItems = results, Selected=selected, SelectAll = allSelected.Value, Direction = direction , OrderBy = orderBy, ActionKey=actionKey  } });

            if (eArgs != null)
            {
                if (eArgs.Object.Result != null)
                    return (FileMediaResult)eArgs.Object.Result;
            }
            

            return (new FeaturesController()).ExportDownload(typeof(ProfesorModel), results, format, this.GetUIPluralText("MBKYellowBox", "Profesor"));
            
        }
			*/
		
		[HttpPost]
        public ActionResult CustomActionExecute(ContextActionModel model) {
		 try
            {
			//List<Guid> keysSelected1 = new List<Guid>();
			List<object> keysSelected1 = new List<object>();
            if (!string.IsNullOrEmpty(model.Selected))
            {
                foreach (var keyString in model.Selected.Split(char.Parse(",")))
                {
				
				keysSelected1.Add(Guid.Parse(keyString.Split(char.Parse("|"))[0]));
                        
                    

			
                }
            }
			DataAction dataAction = DataAction.GetDataAction(Request);
			 model.Selected = dataAction.Selected;

            model.Query = SFSdotNet.Framework.Web.Mvc.Lists.GetQuery(dataAction.Query, dataAction.AllSelected, dataAction.Selected, "GuidProfesor");
           
            
			
			#region implementaci�n de m�todo parcial
            bool replaceResult = false;
            MyEventArgs<ContextActionModel<ProfesorModel>> actionEventArgs = null;
           
			if (model.ActionKey != "deletemany" && model.ActionKey != "deleterelmany" && model.ActionKey != "updateRel" &&  model.ActionKey != "delete-relation-fk" && model.ActionKey != "restore" )
			{
				ContextRequest context = SFSdotNet.Framework.My.Context.BuildContextRequestSafe(System.Web.HttpContext.Current);
				context.UseMode = dataAction.Usemode;

				if (model.IsBackground)
				{
					System.Threading.Tasks.Task.Run(() => 
						OnCustomActionExecutingBackground(this, actionEventArgs = new MyEventArgs<ContextActionModel<ProfesorModel>>() { Object = new ContextActionModel<ProfesorModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } })
					);
				}
				else
				{
					OnCustomActionExecuting(this, actionEventArgs = new MyEventArgs<ContextActionModel<ProfesorModel>>() {  Object = new ContextActionModel<ProfesorModel>() { DataAction = dataAction, ContextRequest = context, AllSelected = model.AllSelected, SelectAll = model.AllSelected, IsBackground = model.IsBackground, ActionKey = model.ActionKey, Direction = model.Direction, OrderBy = model.OrderBy, /*SelectedItems = results,*/ SelectedKeys = dataAction.SelectedGuids.Cast<Object>().ToList(), Query = model.Query } });
				}
			}
            List<ProfesorModel> results = null;
	
			if (model.ActionKey == "deletemany") { 
				
				BR.ProfesorsBR.Instance.Delete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

            }
	
			else if (model.ActionKey == "restore") {
                    BR.ProfesorsBR.Instance.UnDelete(model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());

                }
            else if (model.ActionKey == "updateRel" || model.ActionKey == "delete-relation-fk" || model.ActionKey == "updateRel-proxyMany")
            {
               try {
                   string valueForUpdate = null;
				   string propForUpdate = null;
				   if (!string.IsNullOrEmpty(Request.Params["propertyForUpdate"])){
						propForUpdate = Request.Params["propertyForUpdate"];
				   }
				    if (string.IsNullOrEmpty(propForUpdate) && !string.IsNullOrEmpty(Request.QueryString["propertyForUpdate"]))
                   {
                       propForUpdate = Request.QueryString["propertyForUpdate"];
                   }
                    if (model.ActionKey != "delete-relation-fk")
                    {
                        valueForUpdate = Request.QueryString["valueForUpdate"];
                    }
                    BR.ProfesorsBR.Instance.UpdateAssociation(propForUpdate, valueForUpdate, model.Query, dataAction.SelectedGuids.ToArray(), GetContextRequest());
					
                    if (model.ActionKey == "delete-relation-fk")
                    {
                        MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]);

                        return PartialView("ResultMessageView", message);
                    }
                    else
                    {
                        return Content("ok");
                    }
       
          
                }
                catch (Exception ex)
                {
				        SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
           
                }
            
            }
		
                if (actionEventArgs == null && !model.IsBackground)
                {
                    //if (model.ActionKey != "deletemany"  && model.ActionKey != "deleterelmany")
                    //{
                     //   throw new NotImplementedException("");
                    //}
                }
                else
                {
					if (model.IsBackground == false )
						 replaceResult = actionEventArgs.Object.Result is ActionResult /*actionEventArgs.Object.ReplaceResult*/;
                }
                #endregion
                if (!replaceResult)
                {
                    if (Request != null && Request.IsAjaxRequest())
                    {
						MessageModel message = (new MessageModel()).GetDone(GlobalMessages.DONE, Request.Form["lastActionName"]) ;
                        if (model.IsBackground )
                            message.Message = GlobalMessages.THE_PROCESS_HAS_BEEN_STARTED;
                        
                        return PartialView("ResultMessageView", message);                    
					}
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return (ActionResult)actionEventArgs.Object.Result;
                }
            }
            catch (Exception ex)
            {
				SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
			    
                if (Request != null && Request.IsAjaxRequest())
                {
                    string message = GlobalMessages.ERROR_TRY_LATER;
                    if (ex.Data["usermessage"] != null) {
                        message = ex.Data["usermessage"].ToString();
                    }
					 SFSdotNet.Framework.My.EventLog.Exception(ex, GetContextRequest());
                    return PartialView("ResultMessageView", (new MessageModel()).GetException(message));
                }
                else
                {
                    return View();
                }

            }
        }
        //
        // POST: /Profesors/Delete/5
        
			
	
    }
}
