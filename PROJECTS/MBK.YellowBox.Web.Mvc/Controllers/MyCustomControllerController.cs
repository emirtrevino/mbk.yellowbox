﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MBK.YellowBox.BR;
using System.Web.Script.Serialization;
using MBK.YellowBox.Web.Mvc.Models;
using MBK.YellowBox.Web.Mvc.Resources;
using BO = MBK.YellowBox.BusinessObjects;
using SFSdotNet.Framework.Web.Mvc.Security;
using SFSdotNet.Framework.Web.Mvc;
using SFSdotNet.Framework.Web.Mvc.Models;
using SFSdotNet.Framework.Web.Mvc.Resources;
using SFSdotNet.Framework.Web.Mvc.Controllers;


namespace MBK.YellowBox.Web.Mvc.Controllers
{
    public class MyCustomControllerController : MBK.YellowBox.Web.Mvc.ControllerBase<ModelBase>
    {
        /// <summary>
        /// Forma totalmente custom
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {


            return View();
        }
    }
}