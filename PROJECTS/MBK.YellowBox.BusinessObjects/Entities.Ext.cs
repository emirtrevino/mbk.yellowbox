﻿ 
 
// <Template>
//   <SolutionTemplate>EF POCO 1</SolutionTemplate>
//   <Version>20140822.0944</Version>
//   <Update>Metadata de identificador</Update>
// </Template>
#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using SFSdotNet.Framework.Common.Entities.Metadata;
using SFSdotNet.Framework.Common.Entities;
using System.Linq.Dynamic;
//using Repository.Pattern.Ef6;
#endregion
namespace MBK.YellowBox.BusinessObjects
{

	  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidLocationType",PropertyDefaultText="Name", CompanyPropertyName = "GuidCompany",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class LocationType:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Name != null )		
            		return this.Name.ToString();
				else
					return String.Empty;
			}

		//public LocationType()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidLocationType.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidLocationType)
            {
				_GuidLocationType = guidLocationType;

            }
			private Guid _GuidLocationType;
			[DataMember]
			public Guid GuidLocationType
			{
				get{
					return _GuidLocationType;
				}
				set{
                     
					_GuidLocationType = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "LocationType";
		public static readonly string EntitySetName = "LocationTypes";
        public struct PropertyNames {
            public static readonly string GuidLocationType = "GuidLocationType";
            public static readonly string Name = "Name";
            public static readonly string NameKey = "NameKey";
            public static readonly string GuidCompany = "GuidCompany";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string YBLocations = "YBLocations";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidRouteLocation",PropertyDefaultText="OrderRoute", CompanyPropertyName = "GuidCompany",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class RouteLocation:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.OrderRoute != null )		
            		return this.OrderRoute.ToString();
				else
					return String.Empty;
			}

		//public RouteLocation()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidRouteLocation.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidRouteLocation)
            {
				_GuidRouteLocation = guidRouteLocation;

            }
			private Guid _GuidRouteLocation;
			[DataMember]
			public Guid GuidRouteLocation
			{
				get{
					return _GuidRouteLocation;
				}
				set{
                     
					_GuidRouteLocation = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "RouteLocation";
		public static readonly string EntitySetName = "RouteLocations";
        public struct PropertyNames {
            public static readonly string GuidRouteLocation = "GuidRouteLocation";
            public static readonly string GuidRoute = "GuidRoute";
            public static readonly string GuidLocation = "GuidLocation";
            public static readonly string OrderRoute = "OrderRoute";
            public static readonly string GuidStudent = "GuidStudent";
            public static readonly string GuidCompany = "GuidCompany";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string Student = "Student";
            public static readonly string YBLocation = "YBLocation";
            public static readonly string YBRoute = "YBRoute";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidStudent",PropertyDefaultText="FullName", CompanyPropertyName = "GuidCompany",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class Student:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.FullName != null )		
            		return this.FullName.ToString();
				else
					return String.Empty;
			}

		//public Student()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidStudent.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidStudent)
            {
				_GuidStudent = guidStudent;

            }
			private Guid _GuidStudent;
			[DataMember]
			public Guid GuidStudent
			{
				get{
					return _GuidStudent;
				}
				set{
                     
					_GuidStudent = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "Student";
		public static readonly string EntitySetName = "Students";
        public struct PropertyNames {
            public static readonly string GuidStudent = "GuidStudent";
            public static readonly string FullName = "FullName";
            public static readonly string BirthDate = "BirthDate";
            public static readonly string Grade = "Grade";
            public static readonly string Group = "Group";
            public static readonly string GuidParent = "GuidParent";
            public static readonly string GuidCompany = "GuidCompany";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string Comments = "Comments";
            public static readonly string GuidAcademyLevel = "GuidAcademyLevel";
            public static readonly string GuidProfesor = "GuidProfesor";
            public static readonly string RouteLocations = "RouteLocations";
            public static readonly string StudentLocations = "StudentLocations";
            public static readonly string StudentParent = "StudentParent";
            public static readonly string AcademyLevel = "AcademyLevel";
            public static readonly string Profesor = "Profesor";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidStudentLocation",PropertyDefaultText="Bytes", CompanyPropertyName = "GuidCompany",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class StudentLocation:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Bytes != null )		
            		return this.Bytes.ToString();
				else
					return String.Empty;
			}

		//public StudentLocation()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidStudentLocation.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidStudentLocation)
            {
				_GuidStudentLocation = guidStudentLocation;

            }
			private Guid _GuidStudentLocation;
			[DataMember]
			public Guid GuidStudentLocation
			{
				get{
					return _GuidStudentLocation;
				}
				set{
                     
					_GuidStudentLocation = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "StudentLocation";
		public static readonly string EntitySetName = "StudentLocations";
        public struct PropertyNames {
            public static readonly string GuidStudentLocation = "GuidStudentLocation";
            public static readonly string GuidLocation = "GuidLocation";
            public static readonly string GuidStudent = "GuidStudent";
            public static readonly string GuidCompany = "GuidCompany";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string Student = "Student";
            public static readonly string YBLocation = "YBLocation";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidTrasport",PropertyDefaultText="TransportCode", CompanyPropertyName = "GuidCompany",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class Transport:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.TransportCode != null )		
            		return this.TransportCode.ToString();
				else
					return String.Empty;
			}

		//public Transport()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidTrasport.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidTrasport)
            {
				_GuidTrasport = guidTrasport;

            }
			private Guid _GuidTrasport;
			[DataMember]
			public Guid GuidTrasport
			{
				get{
					return _GuidTrasport;
				}
				set{
                     
					_GuidTrasport = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "Transport";
		public static readonly string EntitySetName = "Transports";
        public struct PropertyNames {
            public static readonly string GuidTrasport = "GuidTrasport";
            public static readonly string TransportCode = "TransportCode";
            public static readonly string Description = "Description";
            public static readonly string Capacity = "Capacity";
            public static readonly string GuidRoute = "GuidRoute";
            public static readonly string GuidCompany = "GuidCompany";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string YBRoute = "YBRoute";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidLocation",PropertyDefaultText="Description", CompanyPropertyName = "GuidCompany",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class YBLocation:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Description != null )		
            		return this.Description.ToString();
				else
					return String.Empty;
			}

		//public YBLocation()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidLocation.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidLocation)
            {
				_GuidLocation = guidLocation;

            }
			private Guid _GuidLocation;
			[DataMember]
			public Guid GuidLocation
			{
				get{
					return _GuidLocation;
				}
				set{
                     
					_GuidLocation = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "YBLocation";
		public static readonly string EntitySetName = "YBLocations";
        public struct PropertyNames {
            public static readonly string GuidLocation = "GuidLocation";
            public static readonly string GuidLocationType = "GuidLocationType";
            public static readonly string Lat = "Lat";
            public static readonly string Long = "Long";
            public static readonly string Description = "Description";
            public static readonly string Address = "Address";
            public static readonly string GuidCompany = "GuidCompany";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string LocationType = "LocationType";
            public static readonly string RouteLocations = "RouteLocations";
            public static readonly string StudentLocations = "StudentLocations";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidRoute",PropertyDefaultText="Title", CompanyPropertyName = "GuidCompany",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class YBRoute:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Title != null )		
            		return this.Title.ToString();
				else
					return String.Empty;
			}

		//public YBRoute()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidRoute.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidRoute)
            {
				_GuidRoute = guidRoute;

            }
			private Guid _GuidRoute;
			[DataMember]
			public Guid GuidRoute
			{
				get{
					return _GuidRoute;
				}
				set{
                     
					_GuidRoute = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "YBRoute";
		public static readonly string EntitySetName = "YBRoutes";
        public struct PropertyNames {
            public static readonly string GuidRoute = "GuidRoute";
            public static readonly string Title = "Title";
            public static readonly string GuidCompany = "GuidCompany";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string RouteLocations = "RouteLocations";
            public static readonly string Transports = "Transports";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidStudentParent",PropertyDefaultText="FullName", CompanyPropertyName = "GuidCompany",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class StudentParent:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.FullName != null )		
            		return this.FullName.ToString();
				else
					return String.Empty;
			}

		//public StudentParent()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidStudentParent.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidStudentParent)
            {
				_GuidStudentParent = guidStudentParent;

            }
			private Guid _GuidStudentParent;
			[DataMember]
			public Guid GuidStudentParent
			{
				get{
					return _GuidStudentParent;
				}
				set{
                     
					_GuidStudentParent = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "StudentParent";
		public static readonly string EntitySetName = "StudentParents";
        public struct PropertyNames {
            public static readonly string GuidStudentParent = "GuidStudentParent";
            public static readonly string FullName = "FullName";
            public static readonly string GuidCompany = "GuidCompany";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string Students = "Students";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidAcademyLevel",PropertyDefaultText="Title", CompanyPropertyName = "GuidCompany",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class AcademyLevel:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Title != null )		
            		return this.Title.ToString();
				else
					return String.Empty;
			}

		//public AcademyLevel()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidAcademyLevel.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidAcademyLevel)
            {
				_GuidAcademyLevel = guidAcademyLevel;

            }
			private Guid _GuidAcademyLevel;
			[DataMember]
			public Guid GuidAcademyLevel
			{
				get{
					return _GuidAcademyLevel;
				}
				set{
                     
					_GuidAcademyLevel = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "AcademyLevel";
		public static readonly string EntitySetName = "AcademyLevels";
        public struct PropertyNames {
            public static readonly string GuidAcademyLevel = "GuidAcademyLevel";
            public static readonly string Title = "Title";
            public static readonly string GuidCompany = "GuidCompany";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string Students = "Students";
            public static readonly string Profesors = "Profesors";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidProfesor",PropertyDefaultText="FullName", CompanyPropertyName = "GuidCompany",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class Profesor:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.FullName != null )		
            		return this.FullName.ToString();
				else
					return String.Empty;
			}

		//public Profesor()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidProfesor.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidProfesor)
            {
				_GuidProfesor = guidProfesor;

            }
			private Guid _GuidProfesor;
			[DataMember]
			public Guid GuidProfesor
			{
				get{
					return _GuidProfesor;
				}
				set{
                     
					_GuidProfesor = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "Profesor";
		public static readonly string EntitySetName = "Profesors";
        public struct PropertyNames {
            public static readonly string GuidProfesor = "GuidProfesor";
            public static readonly string FullName = "FullName";
            public static readonly string GuidAcademyLevel = "GuidAcademyLevel";
            public static readonly string GuidCompany = "GuidCompany";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string AcademyLevel = "AcademyLevel";
            public static readonly string Students = "Students";
		}
		#endregion
	}
	 
	
}
