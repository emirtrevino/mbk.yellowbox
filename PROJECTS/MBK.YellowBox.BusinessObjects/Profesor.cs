
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace MBK.YellowBox.BusinessObjects
{

using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using SFSdotNet.Framework.Common.Entities;
    
[JsonObject(IsReference = true)]
[DataContract(IsReference = true, Namespace = "http://schemas.datacontract.org/2004/07/TrackableEntities.Models")]
public partial class Profesor : ITrackable
{

    public Profesor()
    {
	
		this.ModifiedProperties = new List<string>();

        this.Students = new List<Student>();

    }


    [DataMember]
        public System.Guid GuidProfesor { get; set; }

    [DataMember]
        public string FullName { get; set; }

    [DataMember]
        public Nullable<System.Guid> GuidAcademyLevel { get; set; }

    [DataMember]
        public Nullable<System.Guid> GuidCompany { get; set; }

    [DataMember]
        public Nullable<System.DateTime> CreatedDate { get; set; }

    [DataMember]
        public Nullable<System.DateTime> UpdatedDate { get; set; }

    [DataMember]
        public Nullable<System.Guid> CreatedBy { get; set; }

    [DataMember]
        public Nullable<System.Guid> UpdatedBy { get; set; }

    [DataMember]
        public Nullable<int> Bytes { get; set; }

    [DataMember]
        public Nullable<bool> IsDeleted { get; set; }



    [DataMember]
        public AcademyLevel AcademyLevel { get; set; }

    [DataMember]
        public ICollection<Student> Students { get; set; }


    [DataMember]
    public TrackingState TrackingState { get; set; }
    [DataMember]
    public ICollection<string> ModifiedProperties { get; set; }
    [JsonProperty, DataMember]
    private Guid EntityIdentifier { get; set; }
}

}
