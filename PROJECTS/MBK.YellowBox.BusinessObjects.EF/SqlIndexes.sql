﻿ 
 
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'LocationType' and i.name = 'LocationType_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX LocationType_FullStatics_Idx 
    on dbo.LocationType ( 
GuidLocationType 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,GuidCompany 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'RouteLocation' and i.name = 'RouteLocation_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX RouteLocation_FullStatics_Idx 
    on dbo.RouteLocation ( 
GuidRouteLocation 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,GuidCompany 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'Student' and i.name = 'Student_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX Student_FullStatics_Idx 
    on dbo.Student ( 
GuidStudent 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,GuidCompany 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'StudentLocation' and i.name = 'StudentLocation_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX StudentLocation_FullStatics_Idx 
    on dbo.StudentLocation ( 
GuidStudentLocation 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,GuidCompany 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'Transport' and i.name = 'Transport_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX Transport_FullStatics_Idx 
    on dbo.Transport ( 
GuidTrasport 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,GuidCompany 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'YBLocation' and i.name = 'YBLocation_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX YBLocation_FullStatics_Idx 
    on dbo.YBLocation ( 
GuidLocation 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,GuidCompany 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'YBRoute' and i.name = 'YBRoute_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX YBRoute_FullStatics_Idx 
    on dbo.YBRoute ( 
GuidRoute 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,GuidCompany 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'StudentParent' and i.name = 'StudentParent_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX StudentParent_FullStatics_Idx 
    on dbo.StudentParent ( 
GuidStudentParent 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,GuidCompany 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'AcademyLevel' and i.name = 'AcademyLevel_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX AcademyLevel_FullStatics_Idx 
    on dbo.AcademyLevel ( 
GuidAcademyLevel 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,GuidCompany 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)

