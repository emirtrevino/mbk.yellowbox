﻿ 
 
PRINT 'Table LocationType, entity LocationType'
-- GuidCompany
if COLUMNPROPERTY(OBJECT_ID(N'dbo.LocationType', N'U'),'GuidCompany','ColumnId') is null
begin 
  alter table dbo.LocationType 
  add GuidCompany uniqueidentifier null 
end
GO
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.LocationType', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.LocationType 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.LocationType', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.LocationType 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.LocationType', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.LocationType 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.LocationType', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.LocationType 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.LocationType', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.LocationType 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.LocationType', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.LocationType 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table RouteLocation, entity RouteLocation'
-- GuidCompany
if COLUMNPROPERTY(OBJECT_ID(N'dbo.RouteLocation', N'U'),'GuidCompany','ColumnId') is null
begin 
  alter table dbo.RouteLocation 
  add GuidCompany uniqueidentifier null 
end
GO
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.RouteLocation', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.RouteLocation 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.RouteLocation', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.RouteLocation 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.RouteLocation', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.RouteLocation 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.RouteLocation', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.RouteLocation 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.RouteLocation', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.RouteLocation 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.RouteLocation', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.RouteLocation 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table Student, entity Student'
-- GuidCompany
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Student', N'U'),'GuidCompany','ColumnId') is null
begin 
  alter table dbo.Student 
  add GuidCompany uniqueidentifier null 
end
GO
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Student', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.Student 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Student', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.Student 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Student', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.Student 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Student', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.Student 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Student', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.Student 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Student', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.Student 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table StudentLocation, entity StudentLocation'
-- GuidCompany
if COLUMNPROPERTY(OBJECT_ID(N'dbo.StudentLocation', N'U'),'GuidCompany','ColumnId') is null
begin 
  alter table dbo.StudentLocation 
  add GuidCompany uniqueidentifier null 
end
GO
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.StudentLocation', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.StudentLocation 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.StudentLocation', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.StudentLocation 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.StudentLocation', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.StudentLocation 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.StudentLocation', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.StudentLocation 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.StudentLocation', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.StudentLocation 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.StudentLocation', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.StudentLocation 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table Transport, entity Transport'
-- GuidCompany
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Transport', N'U'),'GuidCompany','ColumnId') is null
begin 
  alter table dbo.Transport 
  add GuidCompany uniqueidentifier null 
end
GO
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Transport', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.Transport 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Transport', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.Transport 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Transport', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.Transport 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Transport', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.Transport 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Transport', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.Transport 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Transport', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.Transport 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table YBLocation, entity YBLocation'
-- GuidCompany
if COLUMNPROPERTY(OBJECT_ID(N'dbo.YBLocation', N'U'),'GuidCompany','ColumnId') is null
begin 
  alter table dbo.YBLocation 
  add GuidCompany uniqueidentifier null 
end
GO
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.YBLocation', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.YBLocation 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.YBLocation', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.YBLocation 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.YBLocation', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.YBLocation 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.YBLocation', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.YBLocation 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.YBLocation', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.YBLocation 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.YBLocation', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.YBLocation 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table YBRoute, entity YBRoute'
-- GuidCompany
if COLUMNPROPERTY(OBJECT_ID(N'dbo.YBRoute', N'U'),'GuidCompany','ColumnId') is null
begin 
  alter table dbo.YBRoute 
  add GuidCompany uniqueidentifier null 
end
GO
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.YBRoute', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.YBRoute 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.YBRoute', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.YBRoute 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.YBRoute', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.YBRoute 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.YBRoute', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.YBRoute 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.YBRoute', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.YBRoute 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.YBRoute', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.YBRoute 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table StudentParent, entity StudentParent'
-- GuidCompany
if COLUMNPROPERTY(OBJECT_ID(N'dbo.StudentParent', N'U'),'GuidCompany','ColumnId') is null
begin 
  alter table dbo.StudentParent 
  add GuidCompany uniqueidentifier null 
end
GO
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.StudentParent', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.StudentParent 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.StudentParent', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.StudentParent 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.StudentParent', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.StudentParent 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.StudentParent', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.StudentParent 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.StudentParent', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.StudentParent 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.StudentParent', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.StudentParent 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table AcademyLevel, entity AcademyLevel'
-- GuidCompany
if COLUMNPROPERTY(OBJECT_ID(N'dbo.AcademyLevel', N'U'),'GuidCompany','ColumnId') is null
begin 
  alter table dbo.AcademyLevel 
  add GuidCompany uniqueidentifier null 
end
GO
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.AcademyLevel', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.AcademyLevel 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.AcademyLevel', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.AcademyLevel 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.AcademyLevel', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.AcademyLevel 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.AcademyLevel', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.AcademyLevel 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.AcademyLevel', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.AcademyLevel 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.AcademyLevel', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.AcademyLevel 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table Profesor, entity Profesor'
-- GuidCompany
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Profesor', N'U'),'GuidCompany','ColumnId') is null
begin 
  alter table dbo.Profesor 
  add GuidCompany uniqueidentifier null 
end
GO
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Profesor', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.Profesor 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Profesor', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.Profesor 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Profesor', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.Profesor 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Profesor', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.Profesor 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Profesor', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.Profesor 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Profesor', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.Profesor 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO

