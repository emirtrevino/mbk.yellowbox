﻿ 
 
 
// <Template>
//   <SolutionTemplate>EF POCO 1</SolutionTemplate>
//   <Version>20140213.2136</Version>
//   <Update>mas de contextRequest</Update>
// </Template>
#region using
using System;
using System.Collections.Generic;
using System.Text;
using SFSdotNet.Framework.BR;
using System.Linq.Dynamic;
using System.Collections;
using System.Linq;
using LinqKit;
using SFSdotNet.Framework.Entities;
using SFSdotNet.Framework.Linq;
using System.Linq.Expressions;
using System.Data;
using SFSdotNet.Framework;
using SFSdotNet.Framework.My;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Core.Objects;
using MBK.YellowBox.BusinessObjects;
//using MBK.YellowBox.BusinessObjects.EFPocoAdapter;
using EntityFramework.BulkExtensions;
//using EFPocoAdapter;
using SFSdotNet.Framework.Entities.Trackable;

using System.Data.Entity;


#endregion
namespace MBK.YellowBox.BR
{
public class SinglentonContext
    {
        private static EFContext context = null;
        public static EFContext Instance {
            get {
               if (context == null)
                    context = new EFContext();
                return context;
            }
        }
        /// <summary>
    /// Re-new the singlenton instance
    /// </summary>
    /// <returns></returns>
        public static EFContext RenewInstance() {
            context = new EFContext();
            return context;
        }
    /// <summary>
    /// Get a new instance
    /// </summary>
        public static EFContext NewInstance {
            get {
                return new EFContext();
            }
        }
    }
	
	
		public partial class LocationTypesBR:BRBase<LocationType>{
	 	
           
		 #region Partial methods

           partial void OnUpdating(object sender, BusinessRulesEventArgs<LocationType> e);

            partial void OnUpdated(object sender, BusinessRulesEventArgs<LocationType> e);
			partial void OnUpdatedAgile(object sender, BusinessRulesEventArgs<LocationType> e);

            partial void OnCreating(object sender, BusinessRulesEventArgs<LocationType> e);
            partial void OnCreated(object sender, BusinessRulesEventArgs<LocationType> e);

            partial void OnDeleting(object sender, BusinessRulesEventArgs<LocationType> e);
            partial void OnDeleted(object sender, BusinessRulesEventArgs<LocationType> e);

            partial void OnGetting(object sender, BusinessRulesEventArgs<LocationType> e);
            protected override void OnVirtualGetting(object sender, BusinessRulesEventArgs<LocationType> e)
            {
                OnGetting(sender, e);
            }
			protected override void OnVirtualCounting(object sender, BusinessRulesEventArgs<LocationType> e)
            {
                OnCounting(sender, e);
            }
			partial void OnTaken(object sender, BusinessRulesEventArgs<LocationType> e);
			protected override void OnVirtualTaken(object sender, BusinessRulesEventArgs<LocationType> e)
            {
                OnTaken(sender, e);
            }

            partial void OnCounting(object sender, BusinessRulesEventArgs<LocationType> e);
 
			partial void OnQuerySettings(object sender, BusinessRulesEventArgs<LocationType> e);
          
            #endregion
			
		private static LocationTypesBR singlenton =null;
				public static LocationTypesBR NewInstance(){
					return  new LocationTypesBR();
					
				}
		public static LocationTypesBR Instance{
			get{
				if (singlenton == null)
					singlenton = new LocationTypesBR();
				return singlenton;
			}
		}
		//private bool preventSecurityRestrictions = false;
		 public bool PreventAuditTrail { get; set;  }
		#region Fields
        EFContext context = null;
        #endregion
        #region Constructor
        public LocationTypesBR()
        {
            context = new EFContext();
        }
		 public LocationTypesBR(bool preventSecurity)
            {
                this.preventSecurityRestrictions = preventSecurity;
				context = new EFContext();
            }
        #endregion
		
		#region Get

 		public IQueryable<LocationType> Get()
        {
            using (EFContext con = new EFContext())
            {
				
				var query = con.LocationTypes.AsQueryable();
                con.Configuration.ProxyCreationEnabled = false;

                //query = ContextQueryBuilder<Nutrient>.ApplyContextQuery(query, contextRequest);

                return query;




            }

        }
		


 	
		public List<LocationType> GetAll()
        {
            return this.GetBy(p => true);
        }
        public List<LocationType> GetAll(string includes)
        {
            return this.GetBy(p => true, includes);
        }
        public LocationType GetByKey(Guid guidLocationType)
        {
            return GetByKey(guidLocationType, true);
        }
        public LocationType GetByKey(Guid guidLocationType, bool loadIncludes)
        {
            LocationType item = null;
			var query = PredicateBuilder.True<LocationType>();
                    
			string strWhere = @"GuidLocationType = Guid(""" + guidLocationType.ToString()+@""")";
            Expression<Func<LocationType, bool>> predicate = null;
            //if (!string.IsNullOrEmpty(strWhere))
            //    predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<LocationType, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
			 ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
            contextRequest.CustomQuery.FilterExpressionString = strWhere;

			//item = GetBy(predicate, loadIncludes, contextRequest).FirstOrDefault();
			item = GetBy(strWhere,loadIncludes,contextRequest).FirstOrDefault();
            return item;
        }
         public List<LocationType> GetBy(string strWhere, bool loadRelations, ContextRequest contextRequest)
        {
            if (!loadRelations)
                return GetBy(strWhere, contextRequest);
            else
                return GetBy(strWhere, contextRequest, "");

        }
		  public List<LocationType> GetBy(string strWhere, bool loadRelations)
        {
              if (!loadRelations)
                return GetBy(strWhere, new ContextRequest());
            else
                return GetBy(strWhere, new ContextRequest(), "");

        }
		         public LocationType GetByKey(Guid guidLocationType, params Expression<Func<LocationType, object>>[] includes)
        {
            LocationType item = null;
			string strWhere = @"GuidLocationType = Guid(""" + guidLocationType.ToString()+@""")";
          Expression<Func<LocationType, bool>> predicate = p=> p.GuidLocationType == guidLocationType;
           // if (!string.IsNullOrEmpty(strWhere))
           //     predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<LocationType, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
        item = GetBy(predicate, includes).FirstOrDefault();
         ////   item = GetBy(strWhere,includes).FirstOrDefault();
			return item;

        }
        public LocationType GetByKey(Guid guidLocationType, string includes)
        {
            LocationType item = null;
			string strWhere = @"GuidLocationType = Guid(""" + guidLocationType.ToString()+@""")";
            
			
            item = GetBy(strWhere, includes).FirstOrDefault();
            return item;

        }
		 public LocationType GetByKey(Guid guidLocationType, string usemode, string includes)
		{
			return GetByKey(guidLocationType, usemode, null, includes);

		 }
		 public LocationType GetByKey(Guid guidLocationType, string usemode, ContextRequest context,  string includes)
        {
            LocationType item = null;
			string strWhere = @"GuidLocationType = Guid(""" + guidLocationType.ToString()+@""")";
			if (context == null){
				context = new ContextRequest();
				context.CustomQuery = new CustomQuery();
				context.CustomQuery.IsByKey = true;
				context.CustomQuery.FilterExpressionString = strWhere;
				context.UseMode = usemode;
			}
            item = GetBy(strWhere,context , includes).FirstOrDefault();
            return item;

        }

        #region Dynamic Predicate
        public List<LocationType> GetBy(Expression<Func<LocationType, bool>> predicate, int? pageSize, int? page)
        {
            return this.GetBy(predicate, pageSize, page, null, null);
        }
        public List<LocationType> GetBy(Expression<Func<LocationType, bool>> predicate, ContextRequest contextRequest)
        {

            return GetBy(predicate, contextRequest,"");
        }
        
        public List<LocationType> GetBy(Expression<Func<LocationType, bool>> predicate, ContextRequest contextRequest, params Expression<Func<LocationType, object>>[] includes)
        {
            StringBuilder sb = new StringBuilder();
           if (includes != null)
            {
                foreach (var path in includes)
                {

						if (sb.Length > 0) sb.Append(",");
						sb.Append(SFSdotNet.Framework.Linq.Utils.IncludeToString<LocationType>(path));

               }
            }
            return GetBy(predicate, contextRequest, sb.ToString());
        }
        
        
        public List<LocationType> GetBy(Expression<Func<LocationType, bool>> predicate, string includes)
        {
			ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";

            return GetBy(predicate, context, includes);
        }

        public List<LocationType> GetBy(Expression<Func<LocationType, bool>> predicate, params Expression<Func<LocationType, object>>[] includes)
        {
			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest context = new ContextRequest();
			            context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";
            return GetBy(predicate, context, includes);
        }

      
		public bool DisableCache { get; set; }
		public List<LocationType> GetBy(Expression<Func<LocationType, bool>> predicate, ContextRequest contextRequest, string includes)
		{
            using (EFContext con = new EFContext()) {
				
				string fkIncludes = "";
                List<string> multilangProperties = new List<string>();
				if (predicate == null) predicate = PredicateBuilder.True<LocationType>();
				string isDeletedField = null;
				Expression<Func<LocationType,bool>> notDeletedExpression = null;
	
					bool sharedAndMultiTenant = false;
					Expression<Func<LocationType,bool>> multitenantExpression  = null;
					if (contextRequest != null && contextRequest.Company != null)	                        	
						multitenantExpression = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicate, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression);

#region Old code
/*
				List<LocationType> result = null;
               BusinessRulesEventArgs<LocationType>  e = null;
	
				OnGetting(con, e = new BusinessRulesEventArgs<LocationType>() {  FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null) });

               // OnGetting(con,e = new BusinessRulesEventArgs<LocationType>() { FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = contextRequest.CustomQuery.FilterExpressionString});
				   if (e != null) {
				    predicate = e.FilterExpression;
						if (e.Cancel)
						{
							context = null;
							 if (e.Items == null) e.Items = new List<LocationType>();
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                if (predicate == null) predicate = PredicateBuilder.True<LocationType>();
                
                //var es = _repository.Queryable;

                IQueryable<LocationType> query =  con.LocationTypes.AsQueryable();

                                if (!string.IsNullOrEmpty(includes))
                {
                    foreach (string include in includes.Split(char.Parse(",")))
                    {
						if (!string.IsNullOrEmpty(include))
                            query = query.Include(include);
                    }
                }
					 	if (!preventSecurityRestrictions)
						{
							if (contextRequest != null )
		                    	if (contextRequest.User !=null )
		                        	if (contextRequest.Company != null){
		                        	
										predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
 									
									}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				query =query.AsExpandable().Where(predicate);
                query = ContextQueryBuilder<LocationType>.ApplyContextQuery(query, contextRequest);

                result = query.AsNoTracking().ToList<LocationType>();
				  
                if (e != null)
                {
                    e.Items = result;
                }
				//if (contextRequest != null ){
				//	 contextRequest = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
					contextRequest.CustomQuery = new CustomQuery();

				//}
				OnTaken(this, e == null ? e =  new BusinessRulesEventArgs<LocationType>() { Items= result, IncludingComputedLinq = false, ContextRequest = contextRequest,  FilterExpression = predicate } :  e);
  
			

                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
				*/
#endregion
            }
        }


		/*public int Update(List<LocationType> items, ContextRequest contextRequest)
            {
                int result = 0;
                using (EFContext con = new EFContext())
                {
                   
                

                    foreach (var item in items)
                    {
                        //secMessageToUser messageToUser = new secMessageToUser();
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            item.GetType().GetProperty(prop).SetValue(item, item.GetType().GetProperty(prop).GetValue(item));
                        }
                        //messageToUser.GuidMessageToUser = (Guid)item.GetType().GetProperty("GuidMessageToUser").GetValue(item);

                        var setObject = con.CreateObjectSet<LocationType>("LocationTypes");
                        //messageToUser.Readed = DateTime.UtcNow;
                        setObject.Attach(item);
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            con.ObjectStateManager.GetObjectStateEntry(item).SetModifiedProperty(prop);
                        }
                       
                    }
                    result = con.SaveChanges();

                    


                }
                return result;
            }
           */
		

        public List<LocationType> GetBy(string predicateString, ContextRequest contextRequest, string includes)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
				


				string computedFields = "";
				string fkIncludes = "";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<LocationType>();
				string isDeletedField = null;
				string notDeletedExpression = null;
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					//if (contextRequest != null && contextRequest.Company != null)                      	
					//	 multitenantExpression = @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))";
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicateString, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression,computedFields);


	#region Old Code
	/*
				BusinessRulesEventArgs<LocationType> e = null;

				Filter filter = new Filter();
                if (predicateString.Contains("|"))
                {
                    string ft = GetSpecificFilter(predicateString, contextRequest);
                    if (!string.IsNullOrEmpty(ft))
                        filter.SetFilterPart("ft", ft);
                   
                    contextRequest.FreeText = predicateString.Split(char.Parse("|"))[1];
                    var q1 = predicateString.Split(char.Parse("|"))[0];
                    if (!string.IsNullOrEmpty(q1))
                    {
                        filter.ProcessText(q1);
                    }
                }
                else {
                    filter.ProcessText(predicateString);
                }
				 var includesList = (new List<string>());
                 if (!string.IsNullOrEmpty(includes))
                 {
                     includesList = includes.Split(char.Parse(",")).ToList();
                 }

				List<LocationType> result = new List<LocationType>();
         
			QueryBuild(predicateString, filter, con, contextRequest, "getby", includesList);
			 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }
				
				
					OnGetting(con, e == null ? e = new BusinessRulesEventArgs<LocationType>() { Filter = filter, ContextRequest = contextRequest  } : e );

                  //OnGetting(con,e = new BusinessRulesEventArgs<LocationType>() {  ContextRequest = contextRequest, FilterExpressionString = predicateString });
			   	if (e != null) {
				    //predicateString = e.GetQueryString();
						if (e.Cancel)
						{
							context = null;
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				//	 else {
                //      predicateString = predicateString.Replace("*extraFreeText*", "").Replace("()","");
                //  }
				//con.EnableChangeTrackingUsingProxies = false;
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                //if (predicate == null) predicate = PredicateBuilder.True<LocationType>();
                
                //var es = _repository.Queryable;
				IQueryable<LocationType> query = con.LocationTypes.AsQueryable();
		
				// include relations FK
				if(string.IsNullOrEmpty(includes) ){
					includes ="";
				}
				StringBuilder sbQuerySystem = new StringBuilder();
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
	                    	if (contextRequest.User !=null )
	                        	if (contextRequest.Company != null ){
	                        		//if (sbQuerySystem.Length > 0)
	                        		//	    			sbQuerySystem.Append( " And ");	
									//sbQuerySystem.Append(@" (GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa

									filter.SetFilterPart("co",@"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))");

								}
						}	
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				//string predicateString = predicate.ToDynamicLinq<LocationType>();
				//predicateString += sbQuerySystem.ToString();
				filter.CleanAndProcess("");

				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed(); //SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicateString );               
                string predicateWithManyRelations = filter.GetFilterChildren(); //SFSdotNet.Framework.Linq.Utils.CleanPartExpression(predicateString);

                //QueryUtils.BreakeQuery1(predicateString, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
                var _queryable = query.AsQueryable();
				bool includeAll = true; 
                if (!string.IsNullOrEmpty(predicateWithManyRelations))
                    _queryable = _queryable.Where(predicateWithManyRelations, contextRequest.CustomQuery.ExtraParams);
				if (contextRequest.CustomQuery.SpecificProperties.Count > 0)
                {

				includeAll = false; 
                }

				StringBuilder sbSelect = new StringBuilder();
                sbSelect.Append("new (");
                bool existPrev = false;
                foreach (var selected in contextRequest.CustomQuery.SelectedFields.Where(p=> !string.IsNullOrEmpty(p.Linq)))
                {
                    if (existPrev) sbSelect.Append(", ");
                    if (!selected.Linq.Contains(".") && !selected.Linq.StartsWith("it."))
                        sbSelect.Append("it." + selected.Linq);
                    else
                        sbSelect.Append(selected.Linq);
                    existPrev = true;
                }
                sbSelect.Append(")");
                var queryable = _queryable.Select(sbSelect.ToString());                    


     				
                 if (!string.IsNullOrEmpty(predicateWithFKAndComputed))
                    queryable = queryable.Where(predicateWithFKAndComputed, contextRequest.CustomQuery.ExtraParams);

				QueryComplementOptions queryOps = ContextQueryBuilder.ApplyContextQuery(contextRequest);
            	if (!string.IsNullOrEmpty(queryOps.OrderByAndSort)){
					if (queryOps.OrderBy.Contains(".") && !queryOps.OrderBy.StartsWith("it.")) queryOps.OrderBy = "it." + queryOps.OrderBy;
					queryable = queryable.OrderBy(queryOps.OrderByAndSort);
					}
               	if (queryOps.Skip != null)
                {
                    queryable = queryable.Skip(queryOps.Skip.Value);
                }
                if (queryOps.PageSize != null)
                {
                    queryable = queryable.Take (queryOps.PageSize.Value);
                }


                var resultTemp = queryable.AsQueryable().ToListAsync().Result;
                foreach (var item in resultTemp)
                {

				   result.Add(SFSdotNet.Framework.BR.Utils.GetConverted<LocationType,dynamic>(item, contextRequest.CustomQuery.SelectedFields.Select(p=>p.Name).ToArray()));
                }

			 if (e != null)
                {
                    e.Items = result;
                }
				 contextRequest.CustomQuery = new CustomQuery();
				OnTaken(this, e == null ? e = new BusinessRulesEventArgs<LocationType>() { Items= result, IncludingComputedLinq = true, ContextRequest = contextRequest, FilterExpressionString  = predicateString } :  e);
  
			
  
                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
	
	*/
	#endregion

            }
        }
		public LocationType GetFromOperation(string function, string filterString, string usemode, string fields, ContextRequest contextRequest)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
                string computedFields = "";
               // string fkIncludes = "accContpaqiClassification,accProjectConcept,accProjectType,accProxyUser";
                List<string> multilangProperties = new List<string>();
				string isDeletedField = null;
				string notDeletedExpression = null;
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					if (contextRequest != null && contextRequest.Company != null)
					{
						multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
						contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
					}
					 									
					string multiTenantField = "GuidCompany";


                return GetSummaryOperation(con, new LocationType(), function, filterString, usemode, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields, contextRequest, fields.Split(char.Parse(",")).ToArray());
            }
        }

   protected override void QueryBuild(string predicate, Filter filter, DbContext efContext, ContextRequest contextRequest, string method, List<string> includesList)
      	{
				if (contextRequest.CustomQuery.SpecificProperties.Count == 0)
                {
					contextRequest.CustomQuery.SpecificProperties.Add(LocationType.PropertyNames.Name);
					contextRequest.CustomQuery.SpecificProperties.Add(LocationType.PropertyNames.NameKey);
					contextRequest.CustomQuery.SpecificProperties.Add(LocationType.PropertyNames.GuidCompany);
					contextRequest.CustomQuery.SpecificProperties.Add(LocationType.PropertyNames.CreatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(LocationType.PropertyNames.UpdatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(LocationType.PropertyNames.CreatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(LocationType.PropertyNames.UpdatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(LocationType.PropertyNames.Bytes);
					contextRequest.CustomQuery.SpecificProperties.Add(LocationType.PropertyNames.IsDeleted);
                    
				}

				if (method == "getby" || method == "sum")
				{
					if (!contextRequest.CustomQuery.SpecificProperties.Contains("GuidLocationType")){
						contextRequest.CustomQuery.SpecificProperties.Add("GuidLocationType");
					}

					 if (!string.IsNullOrEmpty(contextRequest.CustomQuery.OrderBy))
					{
						string existPropertyOrderBy = contextRequest.CustomQuery.OrderBy;
						if (contextRequest.CustomQuery.OrderBy.Contains("."))
						{
							existPropertyOrderBy = contextRequest.CustomQuery.OrderBy.Split(char.Parse("."))[0];
						}
						if (!contextRequest.CustomQuery.SpecificProperties.Exists(p => p == existPropertyOrderBy))
						{
							contextRequest.CustomQuery.SpecificProperties.Add(existPropertyOrderBy);
						}
					}

				}
				
	bool isFullDetails = contextRequest.IsFromUI("LocationTypes", UIActions.GetForDetails);
	string filterForTest = predicate  + filter.GetFilterComplete();

				if (isFullDetails || !string.IsNullOrEmpty(predicate))
            {
            } 

			if (method == "sum")
            {
            } 
			if (contextRequest.CustomQuery.SelectedFields.Count == 0)
            {
				foreach (var selected in contextRequest.CustomQuery.SpecificProperties)
                {
					string linq = selected;
					switch (selected)
                    {

					 
						
					 default:
                            break;
                    }
					contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name=selected, Linq=linq});
					if (method == "getby" || method == "sum")
					{
						if (includesList.Contains(selected))
							includesList.Remove(selected);

					}

				}
			}
				if (method == "getby" || method == "sum")
				{
					foreach (var otherInclude in includesList.Where(p=> !string.IsNullOrEmpty(p)))
					{
						contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name = otherInclude, Linq = "it." + otherInclude +" as " + otherInclude });
					}
				}
				BusinessRulesEventArgs<LocationType> e = null;
				if (contextRequest.PreventInterceptors == false)
					OnQuerySettings(efContext, e = new BusinessRulesEventArgs<LocationType>() { Filter = filter, ContextRequest = contextRequest /*, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null)*/ });

				//List<LocationType> result = new List<LocationType>();
                 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }

}
		public List<LocationType> GetBy(Expression<Func<LocationType, bool>> predicate, bool loadRelations, ContextRequest contextRequest)
        {
			if(!loadRelations)
				return GetBy(predicate, contextRequest);
			else
				return GetBy(predicate, contextRequest, "YBLocations");

        }

        public List<LocationType> GetBy(Expression<Func<LocationType, bool>> predicate, int? pageSize, int? page, string orderBy, SFSdotNet.Framework.Data.SortDirection? sortDirection)
        {
            return GetBy(predicate, new ContextRequest() { CustomQuery = new CustomQuery() { Page = page, PageSize = pageSize, OrderBy = orderBy, SortDirection = sortDirection } });
        }
        public List<LocationType> GetBy(Expression<Func<LocationType, bool>> predicate)
        {

			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
			contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
			            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            contextRequest.CustomQuery.FilterExpressionString = null;
            return this.GetBy(predicate, contextRequest, "");
        }
        #endregion
        #region Dynamic String
		protected override string GetSpecificFilter(string filter, ContextRequest contextRequest) {
            string result = "";
		    //string linqFilter = String.Empty;
            string freeTextFilter = String.Empty;
            if (filter.Contains("|"))
            {
               // linqFilter = filter.Split(char.Parse("|"))[0];
                freeTextFilter = filter.Split(char.Parse("|"))[1];
            }
            //else {
            //    freeTextFilter = filter;
            //}
            //else {
            //    linqFilter = filter;
            //}
			// linqFilter = SFSdotNet.Framework.Linq.Utils.ReplaceCustomDateFilters(linqFilter);
            //string specificFilter = linqFilter;
            if (!string.IsNullOrEmpty(freeTextFilter))
            {
                System.Text.StringBuilder sbCont = new System.Text.StringBuilder();
                /*if (specificFilter.Length > 0)
                {
                    sbCont.Append(" AND ");
                    sbCont.Append(" ({0})");
                }
                else
                {
                    sbCont.Append("{0}");
                }*/
                //var words = freeTextFilter.Split(char.Parse(" "));
				var word = freeTextFilter;
                System.Text.StringBuilder sbSpec = new System.Text.StringBuilder();
                 int nWords = 1;
				/*foreach (var word in words)
                {
					if (word.Length > 0){
                    if (sbSpec.Length > 0) sbSpec.Append(" AND ");
					if (words.Length > 1) sbSpec.Append("("); */
					
	
					
					
					
									
					sbSpec.Append(string.Format(@"Name.Contains(""{0}"")", word));
					

					
					
										sbSpec.Append(" OR ");
					
									
					sbSpec.Append(string.Format(@"NameKey.Contains(""{0}"")", word));
					

					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
								 //sbSpec.Append("*extraFreeText*");

                    /*if (words.Length > 1) sbSpec.Append(")");
					
					nWords++;

					}

                }*/
                //specificFilter = string.Format("{0}{1}", specificFilter, string.Format(sbCont.ToString(), sbSpec.ToString()));
                                 result = sbSpec.ToString();  
            }
			//result = specificFilter;
			
			return result;

		}
	
			public List<LocationType> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params object[] extraParams)
        {
			return GetBy(filter, pageSize, page, orderBy, orderDir,  null, extraParams);
		}
           public List<LocationType> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, string usemode, params object[] extraParams)
            { 
                return GetBy(filter, pageSize, page, orderBy, orderDir, usemode, null, extraParams);
            }


		public List<LocationType> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  string usemode, ContextRequest context, params object[] extraParams)

        {

            // string freetext = null;
            //if (filter.Contains("|"))
            //{
            //    int parts = filter.Split(char.Parse("|")).Count();
            //    if (parts > 1)
            //    {

            //        freetext = filter.Split(char.Parse("|"))[1];
            //    }
            //}
		
            //string specificFilter = "";
            //if (!string.IsNullOrEmpty(filter))
            //  specificFilter=  GetSpecificFilter(filter);
            if (string.IsNullOrEmpty(orderBy))
            {
			                orderBy = "UpdatedDate";
            }
			//orderDir = "desc";
			SFSdotNet.Framework.Data.SortDirection direction = SFSdotNet.Framework.Data.SortDirection.Ascending;
            if (!string.IsNullOrEmpty(orderDir))
            {
                if (orderDir == "desc")
                    direction = SFSdotNet.Framework.Data.SortDirection.Descending;
            }
            if (context == null)
                context = new ContextRequest();
			

             context.UseMode = usemode;
             if (context.CustomQuery == null )
                context.CustomQuery =new SFSdotNet.Framework.My.CustomQuery();

 
                context.CustomQuery.ExtraParams = extraParams;

                    context.CustomQuery.OrderBy = orderBy;
                   context.CustomQuery.SortDirection = direction;
                   context.CustomQuery.Page = page;
                  context.CustomQuery.PageSize = pageSize;
               

            

            if (!preventSecurityRestrictions) {
			 if (context.CurrentContext == null)
                {
					if (SFSdotNet.Framework.My.Context.CurrentContext != null &&  SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
					{
						context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
						context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

					}
					else {
						throw new Exception("The security rule require a specific user and company");
					}
				}
            }
            return GetBy(filter, context);
  
        }


        public List<LocationType> GetBy(string strWhere, ContextRequest contextRequest)
        {
        	#region old code
				
				 //Expression<Func<tvsReservationTransport, bool>> predicate = null;
				string strWhereClean = strWhere.Replace("*extraFreeText*", "").Replace("()", "");
                //if (!string.IsNullOrEmpty(strWhereClean)){

                //    object[] extraParams = null;
                //    //if (contextRequest != null )
                //    //    if (contextRequest.CustomQuery != null )
                //    //        extraParams = contextRequest.CustomQuery.ExtraParams;
                //    //predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<tvsReservationTransport, bool>(strWhereClean, extraParams != null? extraParams.Cast<Guid>(): null);				
                //}
				 if (contextRequest == null)
                {
                    contextRequest = new ContextRequest();
                    if (contextRequest.CustomQuery == null)
                        contextRequest.CustomQuery = new CustomQuery();
                }
                  if (!preventSecurityRestrictions) {
					if (contextRequest.User == null || contextRequest.Company == null)
                      {
                     if (SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
                     {
                         contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                         contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

                     }
                     else {
                         throw new Exception("The security rule require a specific User and Company ");
                     }
					 }
                 }
            contextRequest.CustomQuery.FilterExpressionString = strWhere;
				//return GetBy(predicate, contextRequest);  

			#endregion				
				
                    return GetBy(strWhere, contextRequest, "");  


        }
       public List<LocationType> GetBy(string strWhere)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
			
            return GetBy(strWhere, context, null);
        }

        public List<LocationType> GetBy(string strWhere, string includes)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
            return GetBy(strWhere, context, includes);
        }

        #endregion
        #endregion
		
		  #region SaveOrUpdate
        
 		 public LocationType Create(LocationType entity)
        {
				//ObjectContext context = null;
				    if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: Create");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

				return this.Create(entity, contextRequest);


        }
        
       
        public LocationType Create(LocationType entity, ContextRequest contextRequest)
        {
		
		bool graph = false;
	
				bool preventPartial = false;
                if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
               
			using (EFContext con = new EFContext()) {

				LocationType itemForSave = new LocationType();
#region Autos
		if(!preventSecurityRestrictions){

				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion
               BusinessRulesEventArgs<LocationType> e = null;
			    if (preventPartial == false )
                OnCreating(this,e = new BusinessRulesEventArgs<LocationType>() { ContextRequest = contextRequest, Item=entity });
				   if (e != null) {
						if (e.Cancel)
						{
							context = null;
							return e.Item;

						}
					}

                    if (entity.GuidLocationType == Guid.Empty)
                   {
                       entity.GuidLocationType = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   itemForSave.GuidLocationType = entity.GuidLocationType;
				  
		
			itemForSave.GuidLocationType = entity.GuidLocationType;

			itemForSave.Name = entity.Name;

			itemForSave.NameKey = entity.NameKey;

			itemForSave.GuidCompany = entity.GuidCompany;

			itemForSave.CreatedDate = entity.CreatedDate;

			itemForSave.UpdatedDate = entity.UpdatedDate;

			itemForSave.CreatedBy = entity.CreatedBy;

			itemForSave.UpdatedBy = entity.UpdatedBy;

			itemForSave.Bytes = entity.Bytes;

			itemForSave.IsDeleted = entity.IsDeleted;

				
				con.LocationTypes.Add(itemForSave);




                
				con.ChangeTracker.Entries().Where(p => p.Entity != itemForSave && p.State != EntityState.Unchanged).ForEach(p => p.State = EntityState.Detached);

				con.Entry<LocationType>(itemForSave).State = EntityState.Added;

				con.SaveChanges();

					 
				

				//itemResult = entity;
                //if (e != null)
                //{
                 //   e.Item = itemResult;
                //}
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false )
                OnCreated(this, e == null ? e = new BusinessRulesEventArgs<LocationType>() { ContextRequest = contextRequest, Item = entity } : e);



                if (e != null && e.Item != null )
                {
                    return e.Item;
                }
                              return entity;
			}
            
        }
        //BusinessRulesEventArgs<LocationType> e = null;
        public void Create(List<LocationType> entities)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            Create(entities, contextRequest);
        }
        public void Create(List<LocationType> entities, ContextRequest contextRequest)
        
        {
			ObjectContext context = null;
            	foreach (LocationType entity in entities)
				{
					this.Create(entity, contextRequest);
				}
        }
		  public void CreateOrUpdateBulk(List<LocationType> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "cu", contextRequest);
        }

        private void CreateOrUpdateBulk(List<LocationType> entities, string actionKey, ContextRequest contextRequest)
        {
			if (entities.Count() > 0){
            bool graph = false;

            bool preventPartial = false;
            if (contextRequest != null && contextRequest.PreventInterceptors == true)
            {
                preventPartial = true;
            }
            foreach (var entity in entities)
            {
                    if (entity.GuidLocationType == Guid.Empty)
                   {
                       entity.GuidLocationType = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   
				  


#region Autos
		if(!preventSecurityRestrictions){


 if (actionKey != "u")
                        {
				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;


}
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion


		
			//entity.GuidLocationType = entity.GuidLocationType;

			//entity.Name = entity.Name;

			//entity.NameKey = entity.NameKey;

			//entity.GuidCompany = entity.GuidCompany;

			//entity.CreatedDate = entity.CreatedDate;

			//entity.UpdatedDate = entity.UpdatedDate;

			//entity.CreatedBy = entity.CreatedBy;

			//entity.UpdatedBy = entity.UpdatedBy;

			//entity.Bytes = entity.Bytes;

			//entity.IsDeleted = entity.IsDeleted;

				
				




                
				

					 
				

				//itemResult = entity;
            }
            using (EFContext con = new EFContext())
            {
                 if (actionKey == "c")
                    {
                        context.BulkInsert(entities);
                    }else if ( actionKey == "u")
                    {
                        context.BulkUpdate(entities);
                    }else
                    {
                        context.BulkInsertOrUpdate(entities);
                    }
            }

			}
        }
	
		public void CreateBulk(List<LocationType> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "c", contextRequest);
        }


		public void UpdateAgile(LocationType item, params string[] fields)
         {
			UpdateAgile(item, null, fields);
        }
		public void UpdateAgile(LocationType item, ContextRequest contextRequest, params string[] fields)
         {
            
             ContextRequest contextNew = null;
             if (contextRequest != null)
             {
                 contextNew = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
                 if (fields != null && fields.Length > 0)
                 {
                     contextNew.CustomQuery.SpecificProperties  = fields.ToList();
                 }
                 else if(contextRequest.CustomQuery.SpecificProperties.Count > 0)
                 {
                     fields = contextRequest.CustomQuery.SpecificProperties.ToArray();
                 }
             }
			

		   using (EFContext con = new EFContext())
            {



               
					List<string> propForCopy = new List<string>();
                    propForCopy.AddRange(fields);
                    
					  
					if (!propForCopy.Contains("GuidLocationType"))
						propForCopy.Add("GuidLocationType");

					var itemForUpdate = SFSdotNet.Framework.BR.Utils.GetConverted<LocationType,LocationType>(item, propForCopy.ToArray());
					 itemForUpdate.GuidLocationType = item.GuidLocationType;
                  var setT = con.Set<LocationType>().Attach(itemForUpdate);

					if (fields.Count() > 0)
					  {
						  item.ModifiedProperties = fields;
					  }
                    foreach (var property in item.ModifiedProperties)
					{						
                        if (property != "GuidLocationType")
                             con.Entry(setT).Property(property).IsModified = true;

                    }

                
               int result = con.SaveChanges();
               if (result != 1)
               {
                   SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: 1");
               }


            }

			OnUpdatedAgile(this, new BusinessRulesEventArgs<LocationType>() { Item = item, ContextRequest = contextNew  });

         }
		public void UpdateBulk(List<LocationType>  items, params string[] fields)
         {
             SFSdotNet.Framework.My.ContextRequest req = new SFSdotNet.Framework.My.ContextRequest();
             req.CustomQuery = new SFSdotNet.Framework.My.CustomQuery();
             foreach (var field in fields)
             {
                 req.CustomQuery.SpecificProperties.Add(field);
             }
             UpdateBulk(items, req);

         }

		 public void DeleteBulk(List<LocationType> entities, ContextRequest contextRequest = null)
        {

            using (EFContext con = new EFContext())
            {
                foreach (var entity in entities)
                {
					var entityProxy = new LocationType() { GuidLocationType = entity.GuidLocationType };

                    con.Entry<LocationType>(entityProxy).State = EntityState.Deleted;

                }

                int result = con.SaveChanges();
                if (result != entities.Count)
                {
                    SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: " + entities.Count.ToString());
                }
            }

        }

        public void UpdateBulk(List<LocationType> items, ContextRequest contextRequest)
        {
            if (items.Count() > 0){

			 foreach (var entity in items)
            {


#region Autos
		if(!preventSecurityRestrictions){

				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	



			}
#endregion





				}
				using (EFContext con = new EFContext())
				{

                    
                
                   con.BulkUpdate(items);

				}
             
			}	  
        }

         public LocationType Update(LocationType entity)
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return Update(entity, contextRequest);
        }
       
         public LocationType Update(LocationType entity, ContextRequest contextRequest)
        {
		 if ((System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null) && contextRequest == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Update");
            }
            if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            }

			
				LocationType  itemResult = null;

	
			//entity.UpdatedDate = DateTime.Now.ToUniversalTime();
			//if(contextRequest.User != null)
				//entity.UpdatedBy = contextRequest.User.GuidUser;

//	    var oldentity = GetBy(p => p.GuidLocationType == entity.GuidLocationType, contextRequest).FirstOrDefault();
	//	if (oldentity != null) {
		
          //  entity.CreatedDate = oldentity.CreatedDate;
    //        entity.CreatedBy = oldentity.CreatedBy;
	
      //      entity.GuidCompany = oldentity.GuidCompany;
	
			

	
		//}

			 using( EFContext con = new EFContext()){
				BusinessRulesEventArgs<LocationType> e = null;
				bool preventPartial = false; 
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false)
                OnUpdating(this,e = new BusinessRulesEventArgs<LocationType>() { ContextRequest = contextRequest, Item=entity});
				   if (e != null) {
						if (e.Cancel)
						{
							//outcontext = null;
							return e.Item;

						}
					}

	string includes = "";
	IQueryable < LocationType > query = con.LocationTypes.AsQueryable();
	foreach (string include in includes.Split(char.Parse(",")))
                       {
                           if (!string.IsNullOrEmpty(include))
                               query = query.Include(include);
                       }
	var oldentity = query.FirstOrDefault(p => p.GuidLocationType == entity.GuidLocationType);
	if (oldentity.Name != entity.Name)
		oldentity.Name = entity.Name;
	if (oldentity.NameKey != entity.NameKey)
		oldentity.NameKey = entity.NameKey;

				//if (entity.UpdatedDate == null || (contextRequest != null && contextRequest.IsFromUI("LocationTypes", UIActions.Updating)))
			oldentity.UpdatedDate = DateTime.Now.ToUniversalTime();
			if(contextRequest.User != null)
				oldentity.UpdatedBy = contextRequest.User.GuidUser;

           


				if (entity.YBLocations != null)
                {
                    foreach (var item in entity.YBLocations)
                    {


                        
                    }
					
                    

                }


				con.ChangeTracker.Entries().Where(p => p.Entity != oldentity).ForEach(p => p.State = EntityState.Unchanged);  
				  
				con.SaveChanges();
        
					 
					
               
				itemResult = entity;
				if(preventPartial == false)
					OnUpdated(this, e = new BusinessRulesEventArgs<LocationType>() { ContextRequest = contextRequest, Item=itemResult });

              	return itemResult;
			}
			  
        }
        public LocationType Save(LocationType entity)
        {
			return Create(entity);
        }
        public int Save(List<LocationType> entities)
        {
			 Create(entities);
            return entities.Count;

        }
        #endregion
        #region Delete
        public void Delete(LocationType entity)
        {
				this.Delete(entity, null);
			
        }
		 public void Delete(LocationType entity, ContextRequest contextRequest)
        {
				
				  List<LocationType> entities = new List<LocationType>();
				   entities.Add(entity);
				this.Delete(entities, contextRequest);
			
        }

         public void Delete(string query, Guid[] guids, ContextRequest contextRequest)
        {
			var br = new LocationTypesBR(true);
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);
            
            Delete(items, contextRequest);

        }
        public void Delete(LocationType entity,  ContextRequest contextRequest, BusinessRulesEventArgs<LocationType> e = null)
        {
			
				using(EFContext con = new EFContext())
                 {
				
               	BusinessRulesEventArgs<LocationType> _e = null;
               List<LocationType> _items = new List<LocationType>();
                _items.Add(entity);
                if (e == null || e.PreventPartialPropagate == false)
                {
                    OnDeleting(this, _e = (e == null ? new BusinessRulesEventArgs<LocationType>() { ContextRequest = contextRequest, Item = entity, Items = null  } : e));
                }
                if (_e != null)
                {
                    if (_e.Cancel)
						{
							context = null;
							return;

						}
					}


				
									//IsDeleted
					bool logicDelete = true;
					if (entity.IsDeleted != null)
					{
						if (entity.IsDeleted.Value)
							logicDelete = false;
					}
					if (logicDelete)
					{
											//entity = GetBy(p =>, contextRequest).FirstOrDefault();
						entity.IsDeleted = true;
						if (contextRequest != null && contextRequest.User != null)
							entity.UpdatedBy = contextRequest.User.GuidUser;
                        entity.UpdatedDate = DateTime.UtcNow;
						UpdateAgile(entity, "IsDeleted","UpdatedBy","UpdatedDate");

						
					}
					else {
					con.Entry<LocationType>(entity).State = EntityState.Deleted;
					con.SaveChanges();
				
				 
					}
								
				
				 
					
					
			if (e == null || e.PreventPartialPropagate == false)
                {

                    if (_e == null)
                        _e = new BusinessRulesEventArgs<LocationType>() { ContextRequest = contextRequest, Item = entity, Items = null };

                    OnDeleted(this, _e);
                }

				//return null;
			}
        }
 public void UnDelete(string query, Guid[] guids, ContextRequest contextRequest)
        {
            var br = new LocationTypesBR(true);
            contextRequest.CustomQuery.IncludeDeleted = true;
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);

            foreach (var item in items)
            {
                item.IsDeleted = false;
						if (contextRequest != null && contextRequest.User != null)
							item.UpdatedBy = contextRequest.User.GuidUser;
                        item.UpdatedDate = DateTime.UtcNow;
            }

            UpdateBulk(items, "IsDeleted","UpdatedBy","UpdatedDate");
        }

         public void Delete(List<LocationType> entities,  ContextRequest contextRequest = null )
        {
				
			 BusinessRulesEventArgs<LocationType> _e = null;

                OnDeleting(this, _e = new BusinessRulesEventArgs<LocationType>() { ContextRequest = contextRequest, Item = null, Items = entities });
                if (_e != null)
                {
                    if (_e.Cancel)
                    {
                        context = null;
                        return;

                    }
                }
                bool allSucced = true;
                BusinessRulesEventArgs<LocationType> eToChilds = new BusinessRulesEventArgs<LocationType>();
                if (_e != null)
                {
                    eToChilds = _e;
                }
                else
                {
                    eToChilds = new BusinessRulesEventArgs<LocationType>() { ContextRequest = contextRequest, Item = (entities.Count == 1 ? entities[0] : null), Items = entities };
                }
				foreach (LocationType item in entities)
				{
					try
                    {
                        this.Delete(item, contextRequest, e: eToChilds);
                    }
                    catch (Exception ex)
                    {
                        SFSdotNet.Framework.My.EventLog.Error(ex);
                        allSucced = false;
                    }
				}
				if (_e == null)
                    _e = new BusinessRulesEventArgs<LocationType>() { ContextRequest = contextRequest, CountResult = entities.Count, Item = null, Items = entities };
                OnDeleted(this, _e);

			
        }
        #endregion
 
        #region GetCount
		 public int GetCount(Expression<Func<LocationType, bool>> predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

			return GetCount(predicate, contextRequest);
		}
        public int GetCount(Expression<Func<LocationType, bool>> predicate, ContextRequest contextRequest)
        {


		
		 using (EFContext con = new EFContext())
            {


				if (predicate == null) predicate = PredicateBuilder.True<LocationType>();
           		predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted == null);
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    		if (contextRequest.User !=null )
                        		if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
									predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa

								}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				
				IQueryable<LocationType> query = con.LocationTypes.AsQueryable();
                return query.AsExpandable().Count(predicate);

			
				}
			

        }
		  public int GetCount(string predicate,  ContextRequest contextRequest)
         {
             return GetCount(predicate, null, contextRequest);
         }

         public int GetCount(string predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return GetCount(predicate, contextRequest);
        }
		 public int GetCount(string predicate, string usemode){
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
				return GetCount( predicate,  usemode,  contextRequest);
		 }
        public int GetCount(string predicate, string usemode, ContextRequest contextRequest){

		using (EFContext con = new EFContext()) {
				string computedFields = "";
				string fkIncludes = "";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<LocationType>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetCount(con, predicate, usemode, contextRequest, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields);

			}
			#region old code
			 /* string freetext = null;
            Filter filter = new Filter();

              if (predicate.Contains("|"))
              {
                 
                  filter.SetFilterPart("ft", GetSpecificFilter(predicate, contextRequest));
                 
                  filter.ProcessText(predicate.Split(char.Parse("|"))[0]);
                  freetext = predicate.Split(char.Parse("|"))[1];

				  if (!string.IsNullOrEmpty(freetext) && string.IsNullOrEmpty(contextRequest.FreeText))
                  {
                      contextRequest.FreeText = freetext;
                  }
              }
              else {
                  filter.ProcessText(predicate);
              }
			   predicate = filter.GetFilterComplete();
			// BusinessRulesEventArgs<LocationType>  e = null;
           	using (EFContext con = new EFContext())
			{
			
			

			 QueryBuild(predicate, filter, con, contextRequest, "count", new List<string>());


			
			BusinessRulesEventArgs<LocationType> e = null;

			contextRequest.FreeText = freetext;
			contextRequest.UseMode = usemode;
            OnCounting(this, e = new BusinessRulesEventArgs<LocationType>() {  Filter =filter, ContextRequest = contextRequest });
            if (e != null)
            {
                if (e.Cancel)
                {
                    context = null;
                    return e.CountResult;

                }

            

            }
			
			StringBuilder sbQuerySystem = new StringBuilder();
		
					
                    filter.SetFilterPart("de","(IsDeleted != true OR IsDeleted == null)");
			
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    	if (contextRequest.User !=null )
                        	if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
                        		
								filter.SetFilterPart("co", @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa
						
						
							}
							
							}
							if (preventSecurityRestrictions) preventSecurityRestrictions= false;
		
				   
                 filter.CleanAndProcess("");
				//string predicateWithFKAndComputed = SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicate );               
				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed();
               string predicateWithManyRelations = filter.GetFilterChildren();
			   ///QueryUtils.BreakeQuery1(predicate, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
			   predicate = filter.GetFilterComplete();
               if (!string.IsNullOrEmpty(predicate))
               {
				
					
                    return con.LocationTypes.Where(predicate).Count();
					
                }else
                    return con.LocationTypes.Count();
					
			}*/
			#endregion

		}
         public int GetCount()
        {
            return GetCount(p => true);
        }
        #endregion
        
         


        public void Delete(List<LocationType.CompositeKey> entityKeys)
        {

            List<LocationType> items = new List<LocationType>();
            foreach (var itemKey in entityKeys)
            {
                items.Add(GetByKey(itemKey.GuidLocationType));
            }

            Delete(items);

        }
		 public void UpdateAssociation(string relation, string relationValue, string query, Guid[] ids, ContextRequest contextRequest)
        {
            var items = GetBy(query, null, null, null, null, null, contextRequest, ids);
			 var module = SFSdotNet.Framework.Cache.Caching.SystemObjects.GetModuleByKey(SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(System.Web.HttpContext.Current.Request.RequestContext, "area"));
           
            foreach (var item in items)
            {
			  Guid ? guidRelationValue = null ;
                if (!string.IsNullOrEmpty(relationValue)){
                    guidRelationValue = Guid.Parse(relationValue );
                }

				 if (relation.Contains("."))
                {
                    var partsWithOtherProp = relation.Split(char.Parse("|"));
                    var parts = partsWithOtherProp[0].Split(char.Parse("."));

                    string proxyRelName = parts[0];
                    string proxyProperty = parts[1];
                    string proxyPropertyKeyNameFromOther = partsWithOtherProp[1];
                    //string proxyPropertyThis = parts[2];

                    var prop = item.GetType().GetProperty(proxyRelName);
                    //var entityInfo = //SFSdotNet.Framework.
                    // descubrir el tipo de entidad dentro de la colección
                    Type typeEntityInList = SFSdotNet.Framework.Entities.Utils.GetTypeFromList(prop);
                    var newProxyItem = Activator.CreateInstance(typeEntityInList);
                    var propThisForSet = newProxyItem.GetType().GetProperty(proxyProperty);
                    var entityInfoOfProxy = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(typeEntityInList);
                    var propOther = newProxyItem.GetType().GetProperty(proxyPropertyKeyNameFromOther);

                    if (propThisForSet != null && entityInfoOfProxy != null && propOther != null )
                    {
                        var entityInfoThis = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(item.GetType());
                        var valueThisId = item.GetType().GetProperty(entityInfoThis.PropertyKeyName).GetValue(item);
                        if (valueThisId != null)
                            propThisForSet.SetValue(newProxyItem, valueThisId);
                        propOther.SetValue(newProxyItem, Guid.Parse(relationValue));
                        
                        var entityNameProp = newProxyItem.GetType().GetField("EntityName").GetValue(null);
                        var entitySetNameProp = newProxyItem.GetType().GetField("EntitySetName").GetValue(null);

                        SFSdotNet.Framework.Apps.Integration.CreateItemFromApp(entityNameProp.ToString(), entitySetNameProp.ToString(), module.ModuleNamespace, newProxyItem, contextRequest);

                    }

                    // crear una instancia del tipo de entidad
                    // llenar los datos y registrar nuevo


                }
                else
                {
                var prop = item.GetType().GetProperty(relation);
                var entityInfo = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(prop.PropertyType);
                if (entityInfo != null)
                {
                    var ins = Activator.CreateInstance(prop.PropertyType);
                   if (guidRelationValue != null)
                    {
                        prop.PropertyType.GetProperty(entityInfo.PropertyKeyName).SetValue(ins, guidRelationValue);
                        item.GetType().GetProperty(relation).SetValue(item, ins);
                    }
                    else
                    {
                        item.GetType().GetProperty(relation).SetValue(item, null);
                    }

                    Update(item, contextRequest);
                }

				}
            }
        }
		
	}
		public partial class RouteLocationsBR:BRBase<RouteLocation>{
	 	
           
		 #region Partial methods

           partial void OnUpdating(object sender, BusinessRulesEventArgs<RouteLocation> e);

            partial void OnUpdated(object sender, BusinessRulesEventArgs<RouteLocation> e);
			partial void OnUpdatedAgile(object sender, BusinessRulesEventArgs<RouteLocation> e);

            partial void OnCreating(object sender, BusinessRulesEventArgs<RouteLocation> e);
            partial void OnCreated(object sender, BusinessRulesEventArgs<RouteLocation> e);

            partial void OnDeleting(object sender, BusinessRulesEventArgs<RouteLocation> e);
            partial void OnDeleted(object sender, BusinessRulesEventArgs<RouteLocation> e);

            partial void OnGetting(object sender, BusinessRulesEventArgs<RouteLocation> e);
            protected override void OnVirtualGetting(object sender, BusinessRulesEventArgs<RouteLocation> e)
            {
                OnGetting(sender, e);
            }
			protected override void OnVirtualCounting(object sender, BusinessRulesEventArgs<RouteLocation> e)
            {
                OnCounting(sender, e);
            }
			partial void OnTaken(object sender, BusinessRulesEventArgs<RouteLocation> e);
			protected override void OnVirtualTaken(object sender, BusinessRulesEventArgs<RouteLocation> e)
            {
                OnTaken(sender, e);
            }

            partial void OnCounting(object sender, BusinessRulesEventArgs<RouteLocation> e);
 
			partial void OnQuerySettings(object sender, BusinessRulesEventArgs<RouteLocation> e);
          
            #endregion
			
		private static RouteLocationsBR singlenton =null;
				public static RouteLocationsBR NewInstance(){
					return  new RouteLocationsBR();
					
				}
		public static RouteLocationsBR Instance{
			get{
				if (singlenton == null)
					singlenton = new RouteLocationsBR();
				return singlenton;
			}
		}
		//private bool preventSecurityRestrictions = false;
		 public bool PreventAuditTrail { get; set;  }
		#region Fields
        EFContext context = null;
        #endregion
        #region Constructor
        public RouteLocationsBR()
        {
            context = new EFContext();
        }
		 public RouteLocationsBR(bool preventSecurity)
            {
                this.preventSecurityRestrictions = preventSecurity;
				context = new EFContext();
            }
        #endregion
		
		#region Get

 		public IQueryable<RouteLocation> Get()
        {
            using (EFContext con = new EFContext())
            {
				
				var query = con.RouteLocations.AsQueryable();
                con.Configuration.ProxyCreationEnabled = false;

                //query = ContextQueryBuilder<Nutrient>.ApplyContextQuery(query, contextRequest);

                return query;




            }

        }
		


 	
		public List<RouteLocation> GetAll()
        {
            return this.GetBy(p => true);
        }
        public List<RouteLocation> GetAll(string includes)
        {
            return this.GetBy(p => true, includes);
        }
        public RouteLocation GetByKey(Guid guidRouteLocation)
        {
            return GetByKey(guidRouteLocation, true);
        }
        public RouteLocation GetByKey(Guid guidRouteLocation, bool loadIncludes)
        {
            RouteLocation item = null;
			var query = PredicateBuilder.True<RouteLocation>();
                    
			string strWhere = @"GuidRouteLocation = Guid(""" + guidRouteLocation.ToString()+@""")";
            Expression<Func<RouteLocation, bool>> predicate = null;
            //if (!string.IsNullOrEmpty(strWhere))
            //    predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<RouteLocation, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
			 ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
            contextRequest.CustomQuery.FilterExpressionString = strWhere;

			//item = GetBy(predicate, loadIncludes, contextRequest).FirstOrDefault();
			item = GetBy(strWhere,loadIncludes,contextRequest).FirstOrDefault();
            return item;
        }
         public List<RouteLocation> GetBy(string strWhere, bool loadRelations, ContextRequest contextRequest)
        {
            if (!loadRelations)
                return GetBy(strWhere, contextRequest);
            else
                return GetBy(strWhere, contextRequest, "");

        }
		  public List<RouteLocation> GetBy(string strWhere, bool loadRelations)
        {
              if (!loadRelations)
                return GetBy(strWhere, new ContextRequest());
            else
                return GetBy(strWhere, new ContextRequest(), "");

        }
		         public RouteLocation GetByKey(Guid guidRouteLocation, params Expression<Func<RouteLocation, object>>[] includes)
        {
            RouteLocation item = null;
			string strWhere = @"GuidRouteLocation = Guid(""" + guidRouteLocation.ToString()+@""")";
          Expression<Func<RouteLocation, bool>> predicate = p=> p.GuidRouteLocation == guidRouteLocation;
           // if (!string.IsNullOrEmpty(strWhere))
           //     predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<RouteLocation, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
        item = GetBy(predicate, includes).FirstOrDefault();
         ////   item = GetBy(strWhere,includes).FirstOrDefault();
			return item;

        }
        public RouteLocation GetByKey(Guid guidRouteLocation, string includes)
        {
            RouteLocation item = null;
			string strWhere = @"GuidRouteLocation = Guid(""" + guidRouteLocation.ToString()+@""")";
            
			
            item = GetBy(strWhere, includes).FirstOrDefault();
            return item;

        }
		 public RouteLocation GetByKey(Guid guidRouteLocation, string usemode, string includes)
		{
			return GetByKey(guidRouteLocation, usemode, null, includes);

		 }
		 public RouteLocation GetByKey(Guid guidRouteLocation, string usemode, ContextRequest context,  string includes)
        {
            RouteLocation item = null;
			string strWhere = @"GuidRouteLocation = Guid(""" + guidRouteLocation.ToString()+@""")";
			if (context == null){
				context = new ContextRequest();
				context.CustomQuery = new CustomQuery();
				context.CustomQuery.IsByKey = true;
				context.CustomQuery.FilterExpressionString = strWhere;
				context.UseMode = usemode;
			}
            item = GetBy(strWhere,context , includes).FirstOrDefault();
            return item;

        }

        #region Dynamic Predicate
        public List<RouteLocation> GetBy(Expression<Func<RouteLocation, bool>> predicate, int? pageSize, int? page)
        {
            return this.GetBy(predicate, pageSize, page, null, null);
        }
        public List<RouteLocation> GetBy(Expression<Func<RouteLocation, bool>> predicate, ContextRequest contextRequest)
        {

            return GetBy(predicate, contextRequest,"");
        }
        
        public List<RouteLocation> GetBy(Expression<Func<RouteLocation, bool>> predicate, ContextRequest contextRequest, params Expression<Func<RouteLocation, object>>[] includes)
        {
            StringBuilder sb = new StringBuilder();
           if (includes != null)
            {
                foreach (var path in includes)
                {

						if (sb.Length > 0) sb.Append(",");
						sb.Append(SFSdotNet.Framework.Linq.Utils.IncludeToString<RouteLocation>(path));

               }
            }
            return GetBy(predicate, contextRequest, sb.ToString());
        }
        
        
        public List<RouteLocation> GetBy(Expression<Func<RouteLocation, bool>> predicate, string includes)
        {
			ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";

            return GetBy(predicate, context, includes);
        }

        public List<RouteLocation> GetBy(Expression<Func<RouteLocation, bool>> predicate, params Expression<Func<RouteLocation, object>>[] includes)
        {
			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest context = new ContextRequest();
			            context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";
            return GetBy(predicate, context, includes);
        }

      
		public bool DisableCache { get; set; }
		public List<RouteLocation> GetBy(Expression<Func<RouteLocation, bool>> predicate, ContextRequest contextRequest, string includes)
		{
            using (EFContext con = new EFContext()) {
				
				string fkIncludes = "Student,YBLocation,YBRoute";
                List<string> multilangProperties = new List<string>();
				if (predicate == null) predicate = PredicateBuilder.True<RouteLocation>();
                var notDeletedExpression = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;
					Expression<Func<RouteLocation,bool>> multitenantExpression  = null;
					if (contextRequest != null && contextRequest.Company != null)	                        	
						multitenantExpression = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicate, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression);

#region Old code
/*
				List<RouteLocation> result = null;
               BusinessRulesEventArgs<RouteLocation>  e = null;
	
				OnGetting(con, e = new BusinessRulesEventArgs<RouteLocation>() {  FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null) });

               // OnGetting(con,e = new BusinessRulesEventArgs<RouteLocation>() { FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = contextRequest.CustomQuery.FilterExpressionString});
				   if (e != null) {
				    predicate = e.FilterExpression;
						if (e.Cancel)
						{
							context = null;
							 if (e.Items == null) e.Items = new List<RouteLocation>();
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                if (predicate == null) predicate = PredicateBuilder.True<RouteLocation>();
 				string fkIncludes = "Student,YBLocation,YBRoute";
                if(contextRequest!=null){
					if (contextRequest.CustomQuery != null)
					{
						if (contextRequest.CustomQuery.IncludeForeignKeyPaths != null) {
							if (contextRequest.CustomQuery.IncludeForeignKeyPaths.Value == false)
								fkIncludes = "";
						}
					}
				}
				if (!string.IsNullOrEmpty(includes))
					includes = includes + "," + fkIncludes;
				else
					includes = fkIncludes;
                
                //var es = _repository.Queryable;

                IQueryable<RouteLocation> query =  con.RouteLocations.AsQueryable();

                                if (!string.IsNullOrEmpty(includes))
                {
                    foreach (string include in includes.Split(char.Parse(",")))
                    {
						if (!string.IsNullOrEmpty(include))
                            query = query.Include(include);
                    }
                }
                    predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
					 	if (!preventSecurityRestrictions)
						{
							if (contextRequest != null )
		                    	if (contextRequest.User !=null )
		                        	if (contextRequest.Company != null){
		                        	
										predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
 									
									}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				query =query.AsExpandable().Where(predicate);
                query = ContextQueryBuilder<RouteLocation>.ApplyContextQuery(query, contextRequest);

                result = query.AsNoTracking().ToList<RouteLocation>();
				  
                if (e != null)
                {
                    e.Items = result;
                }
				//if (contextRequest != null ){
				//	 contextRequest = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
					contextRequest.CustomQuery = new CustomQuery();

				//}
				OnTaken(this, e == null ? e =  new BusinessRulesEventArgs<RouteLocation>() { Items= result, IncludingComputedLinq = false, ContextRequest = contextRequest,  FilterExpression = predicate } :  e);
  
			

                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
				*/
#endregion
            }
        }


		/*public int Update(List<RouteLocation> items, ContextRequest contextRequest)
            {
                int result = 0;
                using (EFContext con = new EFContext())
                {
                   
                

                    foreach (var item in items)
                    {
                        //secMessageToUser messageToUser = new secMessageToUser();
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            item.GetType().GetProperty(prop).SetValue(item, item.GetType().GetProperty(prop).GetValue(item));
                        }
                        //messageToUser.GuidMessageToUser = (Guid)item.GetType().GetProperty("GuidMessageToUser").GetValue(item);

                        var setObject = con.CreateObjectSet<RouteLocation>("RouteLocations");
                        //messageToUser.Readed = DateTime.UtcNow;
                        setObject.Attach(item);
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            con.ObjectStateManager.GetObjectStateEntry(item).SetModifiedProperty(prop);
                        }
                       
                    }
                    result = con.SaveChanges();

                    


                }
                return result;
            }
           */
		

        public List<RouteLocation> GetBy(string predicateString, ContextRequest contextRequest, string includes)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
				


				string computedFields = "";
				string fkIncludes = "Student,YBLocation,YBRoute";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<RouteLocation>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					//if (contextRequest != null && contextRequest.Company != null)                      	
					//	 multitenantExpression = @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))";
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicateString, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression,computedFields);


	#region Old Code
	/*
				BusinessRulesEventArgs<RouteLocation> e = null;

				Filter filter = new Filter();
                if (predicateString.Contains("|"))
                {
                    string ft = GetSpecificFilter(predicateString, contextRequest);
                    if (!string.IsNullOrEmpty(ft))
                        filter.SetFilterPart("ft", ft);
                   
                    contextRequest.FreeText = predicateString.Split(char.Parse("|"))[1];
                    var q1 = predicateString.Split(char.Parse("|"))[0];
                    if (!string.IsNullOrEmpty(q1))
                    {
                        filter.ProcessText(q1);
                    }
                }
                else {
                    filter.ProcessText(predicateString);
                }
				 var includesList = (new List<string>());
                 if (!string.IsNullOrEmpty(includes))
                 {
                     includesList = includes.Split(char.Parse(",")).ToList();
                 }

				List<RouteLocation> result = new List<RouteLocation>();
         
			QueryBuild(predicateString, filter, con, contextRequest, "getby", includesList);
			 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }
				
				
					OnGetting(con, e == null ? e = new BusinessRulesEventArgs<RouteLocation>() { Filter = filter, ContextRequest = contextRequest  } : e );

                  //OnGetting(con,e = new BusinessRulesEventArgs<RouteLocation>() {  ContextRequest = contextRequest, FilterExpressionString = predicateString });
			   	if (e != null) {
				    //predicateString = e.GetQueryString();
						if (e.Cancel)
						{
							context = null;
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				//	 else {
                //      predicateString = predicateString.Replace("*extraFreeText*", "").Replace("()","");
                //  }
				//con.EnableChangeTrackingUsingProxies = false;
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                //if (predicate == null) predicate = PredicateBuilder.True<RouteLocation>();
 				string fkIncludes = "Student,YBLocation,YBRoute";
                if(contextRequest!=null){
					if (contextRequest.CustomQuery != null)
					{
						if (contextRequest.CustomQuery.IncludeForeignKeyPaths != null) {
							if (contextRequest.CustomQuery.IncludeForeignKeyPaths.Value == false)
								fkIncludes = "";
						}
					}
				}else{
                    contextRequest = new ContextRequest();
                    contextRequest.CustomQuery = new CustomQuery();

                }
				if (!string.IsNullOrEmpty(includes))
					includes = includes + "," + fkIncludes;
				else
					includes = fkIncludes;
                
                //var es = _repository.Queryable;
				IQueryable<RouteLocation> query = con.RouteLocations.AsQueryable();
		
				// include relations FK
				if(string.IsNullOrEmpty(includes) ){
					includes ="";
				}
				StringBuilder sbQuerySystem = new StringBuilder();
                    //predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				

				//if (!string.IsNullOrEmpty(predicateString))
                //      sbQuerySystem.Append(" And ");
                //sbQuerySystem.Append(" (IsDeleted != true Or IsDeleted = null) ");
				 filter.SetFilterPart("de", "(IsDeleted != true OR IsDeleted = null)");


					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
	                    	if (contextRequest.User !=null )
	                        	if (contextRequest.Company != null ){
	                        		//if (sbQuerySystem.Length > 0)
	                        		//	    			sbQuerySystem.Append( " And ");	
									//sbQuerySystem.Append(@" (GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa

									filter.SetFilterPart("co",@"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))");

								}
						}	
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				//string predicateString = predicate.ToDynamicLinq<RouteLocation>();
				//predicateString += sbQuerySystem.ToString();
				filter.CleanAndProcess("");

				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed(); //SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicateString );               
                string predicateWithManyRelations = filter.GetFilterChildren(); //SFSdotNet.Framework.Linq.Utils.CleanPartExpression(predicateString);

                //QueryUtils.BreakeQuery1(predicateString, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
                var _queryable = query.AsQueryable();
				bool includeAll = true; 
                if (!string.IsNullOrEmpty(predicateWithManyRelations))
                    _queryable = _queryable.Where(predicateWithManyRelations, contextRequest.CustomQuery.ExtraParams);
				if (contextRequest.CustomQuery.SpecificProperties.Count > 0)
                {

				includeAll = false; 
                }

				StringBuilder sbSelect = new StringBuilder();
                sbSelect.Append("new (");
                bool existPrev = false;
                foreach (var selected in contextRequest.CustomQuery.SelectedFields.Where(p=> !string.IsNullOrEmpty(p.Linq)))
                {
                    if (existPrev) sbSelect.Append(", ");
                    if (!selected.Linq.Contains(".") && !selected.Linq.StartsWith("it."))
                        sbSelect.Append("it." + selected.Linq);
                    else
                        sbSelect.Append(selected.Linq);
                    existPrev = true;
                }
                sbSelect.Append(")");
                var queryable = _queryable.Select(sbSelect.ToString());                    


     				
                 if (!string.IsNullOrEmpty(predicateWithFKAndComputed))
                    queryable = queryable.Where(predicateWithFKAndComputed, contextRequest.CustomQuery.ExtraParams);

				QueryComplementOptions queryOps = ContextQueryBuilder.ApplyContextQuery(contextRequest);
            	if (!string.IsNullOrEmpty(queryOps.OrderByAndSort)){
					if (queryOps.OrderBy.Contains(".") && !queryOps.OrderBy.StartsWith("it.")) queryOps.OrderBy = "it." + queryOps.OrderBy;
					queryable = queryable.OrderBy(queryOps.OrderByAndSort);
					}
               	if (queryOps.Skip != null)
                {
                    queryable = queryable.Skip(queryOps.Skip.Value);
                }
                if (queryOps.PageSize != null)
                {
                    queryable = queryable.Take (queryOps.PageSize.Value);
                }


                var resultTemp = queryable.AsQueryable().ToListAsync().Result;
                foreach (var item in resultTemp)
                {

				   result.Add(SFSdotNet.Framework.BR.Utils.GetConverted<RouteLocation,dynamic>(item, contextRequest.CustomQuery.SelectedFields.Select(p=>p.Name).ToArray()));
                }

			 if (e != null)
                {
                    e.Items = result;
                }
				 contextRequest.CustomQuery = new CustomQuery();
				OnTaken(this, e == null ? e = new BusinessRulesEventArgs<RouteLocation>() { Items= result, IncludingComputedLinq = true, ContextRequest = contextRequest, FilterExpressionString  = predicateString } :  e);
  
			
  
                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
	
	*/
	#endregion

            }
        }
		public RouteLocation GetFromOperation(string function, string filterString, string usemode, string fields, ContextRequest contextRequest)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
                string computedFields = "";
               // string fkIncludes = "accContpaqiClassification,accProjectConcept,accProjectType,accProxyUser";
                List<string> multilangProperties = new List<string>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					if (contextRequest != null && contextRequest.Company != null)
					{
						multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
						contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
					}
					 									
					string multiTenantField = "GuidCompany";


                return GetSummaryOperation(con, new RouteLocation(), function, filterString, usemode, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields, contextRequest, fields.Split(char.Parse(",")).ToArray());
            }
        }

   protected override void QueryBuild(string predicate, Filter filter, DbContext efContext, ContextRequest contextRequest, string method, List<string> includesList)
      	{
				if (contextRequest.CustomQuery.SpecificProperties.Count == 0)
                {
					contextRequest.CustomQuery.SpecificProperties.Add(RouteLocation.PropertyNames.GuidRoute);
					contextRequest.CustomQuery.SpecificProperties.Add(RouteLocation.PropertyNames.GuidLocation);
					contextRequest.CustomQuery.SpecificProperties.Add(RouteLocation.PropertyNames.OrderRoute);
					contextRequest.CustomQuery.SpecificProperties.Add(RouteLocation.PropertyNames.GuidStudent);
					contextRequest.CustomQuery.SpecificProperties.Add(RouteLocation.PropertyNames.GuidCompany);
					contextRequest.CustomQuery.SpecificProperties.Add(RouteLocation.PropertyNames.CreatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(RouteLocation.PropertyNames.UpdatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(RouteLocation.PropertyNames.CreatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(RouteLocation.PropertyNames.UpdatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(RouteLocation.PropertyNames.Bytes);
					contextRequest.CustomQuery.SpecificProperties.Add(RouteLocation.PropertyNames.IsDeleted);
					contextRequest.CustomQuery.SpecificProperties.Add(RouteLocation.PropertyNames.Student);
					contextRequest.CustomQuery.SpecificProperties.Add(RouteLocation.PropertyNames.YBLocation);
					contextRequest.CustomQuery.SpecificProperties.Add(RouteLocation.PropertyNames.YBRoute);
                    
				}

				if (method == "getby" || method == "sum")
				{
					if (!contextRequest.CustomQuery.SpecificProperties.Contains("GuidRouteLocation")){
						contextRequest.CustomQuery.SpecificProperties.Add("GuidRouteLocation");
					}

					 if (!string.IsNullOrEmpty(contextRequest.CustomQuery.OrderBy))
					{
						string existPropertyOrderBy = contextRequest.CustomQuery.OrderBy;
						if (contextRequest.CustomQuery.OrderBy.Contains("."))
						{
							existPropertyOrderBy = contextRequest.CustomQuery.OrderBy.Split(char.Parse("."))[0];
						}
						if (!contextRequest.CustomQuery.SpecificProperties.Exists(p => p == existPropertyOrderBy))
						{
							contextRequest.CustomQuery.SpecificProperties.Add(existPropertyOrderBy);
						}
					}

				}
				
	bool isFullDetails = contextRequest.IsFromUI("RouteLocations", UIActions.GetForDetails);
	string filterForTest = predicate  + filter.GetFilterComplete();

				if (isFullDetails || !string.IsNullOrEmpty(predicate))
            {
            } 

			if (method == "sum")
            {
            } 
			if (contextRequest.CustomQuery.SelectedFields.Count == 0)
            {
				foreach (var selected in contextRequest.CustomQuery.SpecificProperties)
                {
					string linq = selected;
					switch (selected)
                    {

					case "Student":
					if (includesList.Contains(selected)){
                        linq = "it.Student as Student";
					}
                    else
						linq = "iif(it.Student != null, new (it.Student.GuidStudent, it.Student.FullName), null) as Student";
 					break;
					case "YBLocation":
					if (includesList.Contains(selected)){
                        linq = "it.YBLocation as YBLocation";
					}
                    else
						linq = "iif(it.YBLocation != null, new (it.YBLocation.GuidLocation, it.YBLocation.Description), null) as YBLocation";
 					break;
					case "YBRoute":
					if (includesList.Contains(selected)){
                        linq = "it.YBRoute as YBRoute";
					}
                    else
						linq = "iif(it.YBRoute != null, new (it.YBRoute.GuidRoute, it.YBRoute.Title), null) as YBRoute";
 					break;
					 
						
					 default:
                            break;
                    }
					contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name=selected, Linq=linq});
					if (method == "getby" || method == "sum")
					{
						if (includesList.Contains(selected))
							includesList.Remove(selected);

					}

				}
			}
				if (method == "getby" || method == "sum")
				{
					foreach (var otherInclude in includesList.Where(p=> !string.IsNullOrEmpty(p)))
					{
						contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name = otherInclude, Linq = "it." + otherInclude +" as " + otherInclude });
					}
				}
				BusinessRulesEventArgs<RouteLocation> e = null;
				if (contextRequest.PreventInterceptors == false)
					OnQuerySettings(efContext, e = new BusinessRulesEventArgs<RouteLocation>() { Filter = filter, ContextRequest = contextRequest /*, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null)*/ });

				//List<RouteLocation> result = new List<RouteLocation>();
                 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }

}
		public List<RouteLocation> GetBy(Expression<Func<RouteLocation, bool>> predicate, bool loadRelations, ContextRequest contextRequest)
        {
			if(!loadRelations)
				return GetBy(predicate, contextRequest);
			else
				return GetBy(predicate, contextRequest, "");

        }

        public List<RouteLocation> GetBy(Expression<Func<RouteLocation, bool>> predicate, int? pageSize, int? page, string orderBy, SFSdotNet.Framework.Data.SortDirection? sortDirection)
        {
            return GetBy(predicate, new ContextRequest() { CustomQuery = new CustomQuery() { Page = page, PageSize = pageSize, OrderBy = orderBy, SortDirection = sortDirection } });
        }
        public List<RouteLocation> GetBy(Expression<Func<RouteLocation, bool>> predicate)
        {

			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
			contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
			            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            contextRequest.CustomQuery.FilterExpressionString = null;
            return this.GetBy(predicate, contextRequest, "");
        }
        #endregion
        #region Dynamic String
		protected override string GetSpecificFilter(string filter, ContextRequest contextRequest) {
            string result = "";
		    //string linqFilter = String.Empty;
            string freeTextFilter = String.Empty;
            if (filter.Contains("|"))
            {
               // linqFilter = filter.Split(char.Parse("|"))[0];
                freeTextFilter = filter.Split(char.Parse("|"))[1];
            }
            //else {
            //    freeTextFilter = filter;
            //}
            //else {
            //    linqFilter = filter;
            //}
			// linqFilter = SFSdotNet.Framework.Linq.Utils.ReplaceCustomDateFilters(linqFilter);
            //string specificFilter = linqFilter;
            if (!string.IsNullOrEmpty(freeTextFilter))
            {
                System.Text.StringBuilder sbCont = new System.Text.StringBuilder();
                /*if (specificFilter.Length > 0)
                {
                    sbCont.Append(" AND ");
                    sbCont.Append(" ({0})");
                }
                else
                {
                    sbCont.Append("{0}");
                }*/
                //var words = freeTextFilter.Split(char.Parse(" "));
				var word = freeTextFilter;
                System.Text.StringBuilder sbSpec = new System.Text.StringBuilder();
                 int nWords = 1;
				/*foreach (var word in words)
                {
					if (word.Length > 0){
                    if (sbSpec.Length > 0) sbSpec.Append(" AND ");
					if (words.Length > 1) sbSpec.Append("("); */
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
								
					//if (sbSpec.Length > 2)
					//	sbSpec.Append(" OR "); // test
					sbSpec.Append(string.Format(@"it.Student.FullName.Contains(""{0}"")", word)+" OR "+string.Format(@"it.YBLocation.Description.Contains(""{0}"")", word)+" OR "+string.Format(@"it.YBRoute.Title.Contains(""{0}"")", word));
								 //sbSpec.Append("*extraFreeText*");

                    /*if (words.Length > 1) sbSpec.Append(")");
					
					nWords++;

					}

                }*/
                //specificFilter = string.Format("{0}{1}", specificFilter, string.Format(sbCont.ToString(), sbSpec.ToString()));
                                 result = sbSpec.ToString();  
            }
			//result = specificFilter;
			
			return result;

		}
	
			public List<RouteLocation> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params object[] extraParams)
        {
			return GetBy(filter, pageSize, page, orderBy, orderDir,  null, extraParams);
		}
           public List<RouteLocation> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, string usemode, params object[] extraParams)
            { 
                return GetBy(filter, pageSize, page, orderBy, orderDir, usemode, null, extraParams);
            }


		public List<RouteLocation> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  string usemode, ContextRequest context, params object[] extraParams)

        {

            // string freetext = null;
            //if (filter.Contains("|"))
            //{
            //    int parts = filter.Split(char.Parse("|")).Count();
            //    if (parts > 1)
            //    {

            //        freetext = filter.Split(char.Parse("|"))[1];
            //    }
            //}
		
            //string specificFilter = "";
            //if (!string.IsNullOrEmpty(filter))
            //  specificFilter=  GetSpecificFilter(filter);
            if (string.IsNullOrEmpty(orderBy))
            {
			                orderBy = "UpdatedDate";
            }
			//orderDir = "desc";
			SFSdotNet.Framework.Data.SortDirection direction = SFSdotNet.Framework.Data.SortDirection.Ascending;
            if (!string.IsNullOrEmpty(orderDir))
            {
                if (orderDir == "desc")
                    direction = SFSdotNet.Framework.Data.SortDirection.Descending;
            }
            if (context == null)
                context = new ContextRequest();
			

             context.UseMode = usemode;
             if (context.CustomQuery == null )
                context.CustomQuery =new SFSdotNet.Framework.My.CustomQuery();

 
                context.CustomQuery.ExtraParams = extraParams;

                    context.CustomQuery.OrderBy = orderBy;
                   context.CustomQuery.SortDirection = direction;
                   context.CustomQuery.Page = page;
                  context.CustomQuery.PageSize = pageSize;
               

            

            if (!preventSecurityRestrictions) {
			 if (context.CurrentContext == null)
                {
					if (SFSdotNet.Framework.My.Context.CurrentContext != null &&  SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
					{
						context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
						context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

					}
					else {
						throw new Exception("The security rule require a specific user and company");
					}
				}
            }
            return GetBy(filter, context);
  
        }


        public List<RouteLocation> GetBy(string strWhere, ContextRequest contextRequest)
        {
        	#region old code
				
				 //Expression<Func<tvsReservationTransport, bool>> predicate = null;
				string strWhereClean = strWhere.Replace("*extraFreeText*", "").Replace("()", "");
                //if (!string.IsNullOrEmpty(strWhereClean)){

                //    object[] extraParams = null;
                //    //if (contextRequest != null )
                //    //    if (contextRequest.CustomQuery != null )
                //    //        extraParams = contextRequest.CustomQuery.ExtraParams;
                //    //predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<tvsReservationTransport, bool>(strWhereClean, extraParams != null? extraParams.Cast<Guid>(): null);				
                //}
				 if (contextRequest == null)
                {
                    contextRequest = new ContextRequest();
                    if (contextRequest.CustomQuery == null)
                        contextRequest.CustomQuery = new CustomQuery();
                }
                  if (!preventSecurityRestrictions) {
					if (contextRequest.User == null || contextRequest.Company == null)
                      {
                     if (SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
                     {
                         contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                         contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

                     }
                     else {
                         throw new Exception("The security rule require a specific User and Company ");
                     }
					 }
                 }
            contextRequest.CustomQuery.FilterExpressionString = strWhere;
				//return GetBy(predicate, contextRequest);  

			#endregion				
				
                    return GetBy(strWhere, contextRequest, "");  


        }
       public List<RouteLocation> GetBy(string strWhere)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
			
            return GetBy(strWhere, context, null);
        }

        public List<RouteLocation> GetBy(string strWhere, string includes)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
            return GetBy(strWhere, context, includes);
        }

        #endregion
        #endregion
		
		  #region SaveOrUpdate
        
 		 public RouteLocation Create(RouteLocation entity)
        {
				//ObjectContext context = null;
				    if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: Create");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

				return this.Create(entity, contextRequest);


        }
        
       
        public RouteLocation Create(RouteLocation entity, ContextRequest contextRequest)
        {
		
		bool graph = false;
	
				bool preventPartial = false;
                if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
               
			using (EFContext con = new EFContext()) {

				RouteLocation itemForSave = new RouteLocation();
#region Autos
		if(!preventSecurityRestrictions){

				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion
               BusinessRulesEventArgs<RouteLocation> e = null;
			    if (preventPartial == false )
                OnCreating(this,e = new BusinessRulesEventArgs<RouteLocation>() { ContextRequest = contextRequest, Item=entity });
				   if (e != null) {
						if (e.Cancel)
						{
							context = null;
							return e.Item;

						}
					}

                    if (entity.GuidRouteLocation == Guid.Empty)
                   {
                       entity.GuidRouteLocation = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   itemForSave.GuidRouteLocation = entity.GuidRouteLocation;
				  
		
			itemForSave.GuidRouteLocation = entity.GuidRouteLocation;

			itemForSave.OrderRoute = entity.OrderRoute;

			itemForSave.GuidCompany = entity.GuidCompany;

			itemForSave.CreatedDate = entity.CreatedDate;

			itemForSave.UpdatedDate = entity.UpdatedDate;

			itemForSave.CreatedBy = entity.CreatedBy;

			itemForSave.UpdatedBy = entity.UpdatedBy;

			itemForSave.Bytes = entity.Bytes;

			itemForSave.IsDeleted = entity.IsDeleted;

				
				con.RouteLocations.Add(itemForSave);



					if (entity.Student != null)
					{
						var student = new Student();
						student.GuidStudent = entity.Student.GuidStudent;
						itemForSave.Student = student;
						SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<Student>(con, itemForSave.Student);
			
					}




					if (entity.YBLocation != null)
					{
						var yBLocation = new YBLocation();
						yBLocation.GuidLocation = entity.YBLocation.GuidLocation;
						itemForSave.YBLocation = yBLocation;
						SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<YBLocation>(con, itemForSave.YBLocation);
			
					}




					if (entity.YBRoute != null)
					{
						var yBRoute = new YBRoute();
						yBRoute.GuidRoute = entity.YBRoute.GuidRoute;
						itemForSave.YBRoute = yBRoute;
						SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<YBRoute>(con, itemForSave.YBRoute);
			
					}



                
				con.ChangeTracker.Entries().Where(p => p.Entity != itemForSave && p.State != EntityState.Unchanged).ForEach(p => p.State = EntityState.Detached);

				con.Entry<RouteLocation>(itemForSave).State = EntityState.Added;

				con.SaveChanges();

					 
				

				//itemResult = entity;
                //if (e != null)
                //{
                 //   e.Item = itemResult;
                //}
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false )
                OnCreated(this, e == null ? e = new BusinessRulesEventArgs<RouteLocation>() { ContextRequest = contextRequest, Item = entity } : e);



                if (e != null && e.Item != null )
                {
                    return e.Item;
                }
                              return entity;
			}
            
        }
        //BusinessRulesEventArgs<RouteLocation> e = null;
        public void Create(List<RouteLocation> entities)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            Create(entities, contextRequest);
        }
        public void Create(List<RouteLocation> entities, ContextRequest contextRequest)
        
        {
			ObjectContext context = null;
            	foreach (RouteLocation entity in entities)
				{
					this.Create(entity, contextRequest);
				}
        }
		  public void CreateOrUpdateBulk(List<RouteLocation> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "cu", contextRequest);
        }

        private void CreateOrUpdateBulk(List<RouteLocation> entities, string actionKey, ContextRequest contextRequest)
        {
			if (entities.Count() > 0){
            bool graph = false;

            bool preventPartial = false;
            if (contextRequest != null && contextRequest.PreventInterceptors == true)
            {
                preventPartial = true;
            }
            foreach (var entity in entities)
            {
                    if (entity.GuidRouteLocation == Guid.Empty)
                   {
                       entity.GuidRouteLocation = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   
				  


#region Autos
		if(!preventSecurityRestrictions){


 if (actionKey != "u")
                        {
				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;


}
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion


		
			//entity.GuidRouteLocation = entity.GuidRouteLocation;

			//entity.OrderRoute = entity.OrderRoute;

			//entity.GuidCompany = entity.GuidCompany;

			//entity.CreatedDate = entity.CreatedDate;

			//entity.UpdatedDate = entity.UpdatedDate;

			//entity.CreatedBy = entity.CreatedBy;

			//entity.UpdatedBy = entity.UpdatedBy;

			//entity.Bytes = entity.Bytes;

			//entity.IsDeleted = entity.IsDeleted;

				
				



				    if (entity.Student != null)
					{
						//var student = new Student();
						entity.GuidStudent = entity.Student.GuidStudent;
						//entity.Student = student;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<Student>(con, itemForSave.Student);
			
					}




				    if (entity.YBLocation != null)
					{
						//var yBLocation = new YBLocation();
						entity.GuidLocation = entity.YBLocation.GuidLocation;
						//entity.YBLocation = yBLocation;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<YBLocation>(con, itemForSave.YBLocation);
			
					}




				    if (entity.YBRoute != null)
					{
						//var yBRoute = new YBRoute();
						entity.GuidRoute = entity.YBRoute.GuidRoute;
						//entity.YBRoute = yBRoute;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<YBRoute>(con, itemForSave.YBRoute);
			
					}



                
				

					 
				

				//itemResult = entity;
            }
            using (EFContext con = new EFContext())
            {
                 if (actionKey == "c")
                    {
                        context.BulkInsert(entities);
                    }else if ( actionKey == "u")
                    {
                        context.BulkUpdate(entities);
                    }else
                    {
                        context.BulkInsertOrUpdate(entities);
                    }
            }

			}
        }
	
		public void CreateBulk(List<RouteLocation> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "c", contextRequest);
        }


		public void UpdateAgile(RouteLocation item, params string[] fields)
         {
			UpdateAgile(item, null, fields);
        }
		public void UpdateAgile(RouteLocation item, ContextRequest contextRequest, params string[] fields)
         {
            
             ContextRequest contextNew = null;
             if (contextRequest != null)
             {
                 contextNew = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
                 if (fields != null && fields.Length > 0)
                 {
                     contextNew.CustomQuery.SpecificProperties  = fields.ToList();
                 }
                 else if(contextRequest.CustomQuery.SpecificProperties.Count > 0)
                 {
                     fields = contextRequest.CustomQuery.SpecificProperties.ToArray();
                 }
             }
			

		   using (EFContext con = new EFContext())
            {



               
					List<string> propForCopy = new List<string>();
                    propForCopy.AddRange(fields);
                    
					  
					if (!propForCopy.Contains("GuidRouteLocation"))
						propForCopy.Add("GuidRouteLocation");

					var itemForUpdate = SFSdotNet.Framework.BR.Utils.GetConverted<RouteLocation,RouteLocation>(item, propForCopy.ToArray());
					 itemForUpdate.GuidRouteLocation = item.GuidRouteLocation;
                  var setT = con.Set<RouteLocation>().Attach(itemForUpdate);

					if (fields.Count() > 0)
					  {
						  item.ModifiedProperties = fields;
					  }
                    foreach (var property in item.ModifiedProperties)
					{						
                        if (property != "GuidRouteLocation")
                             con.Entry(setT).Property(property).IsModified = true;

                    }

                
               int result = con.SaveChanges();
               if (result != 1)
               {
                   SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: 1");
               }


            }

			OnUpdatedAgile(this, new BusinessRulesEventArgs<RouteLocation>() { Item = item, ContextRequest = contextNew  });

         }
		public void UpdateBulk(List<RouteLocation>  items, params string[] fields)
         {
             SFSdotNet.Framework.My.ContextRequest req = new SFSdotNet.Framework.My.ContextRequest();
             req.CustomQuery = new SFSdotNet.Framework.My.CustomQuery();
             foreach (var field in fields)
             {
                 req.CustomQuery.SpecificProperties.Add(field);
             }
             UpdateBulk(items, req);

         }

		 public void DeleteBulk(List<RouteLocation> entities, ContextRequest contextRequest = null)
        {

            using (EFContext con = new EFContext())
            {
                foreach (var entity in entities)
                {
					var entityProxy = new RouteLocation() { GuidRouteLocation = entity.GuidRouteLocation };

                    con.Entry<RouteLocation>(entityProxy).State = EntityState.Deleted;

                }

                int result = con.SaveChanges();
                if (result != entities.Count)
                {
                    SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: " + entities.Count.ToString());
                }
            }

        }

        public void UpdateBulk(List<RouteLocation> items, ContextRequest contextRequest)
        {
            if (items.Count() > 0){

			 foreach (var entity in items)
            {


#region Autos
		if(!preventSecurityRestrictions){

				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	



			}
#endregion




				    if (entity.Student != null)
					{
						//var student = new Student();
						entity.GuidStudent = entity.Student.GuidStudent;
						//entity.Student = student;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<Student>(con, itemForSave.Student);
			
					}




				    if (entity.YBLocation != null)
					{
						//var yBLocation = new YBLocation();
						entity.GuidLocation = entity.YBLocation.GuidLocation;
						//entity.YBLocation = yBLocation;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<YBLocation>(con, itemForSave.YBLocation);
			
					}




				    if (entity.YBRoute != null)
					{
						//var yBRoute = new YBRoute();
						entity.GuidRoute = entity.YBRoute.GuidRoute;
						//entity.YBRoute = yBRoute;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<YBRoute>(con, itemForSave.YBRoute);
			
					}



				}
				using (EFContext con = new EFContext())
				{

                    
                
                   con.BulkUpdate(items);

				}
             
			}	  
        }

         public RouteLocation Update(RouteLocation entity)
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return Update(entity, contextRequest);
        }
       
         public RouteLocation Update(RouteLocation entity, ContextRequest contextRequest)
        {
		 if ((System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null) && contextRequest == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Update");
            }
            if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            }

			
				RouteLocation  itemResult = null;

	
			//entity.UpdatedDate = DateTime.Now.ToUniversalTime();
			//if(contextRequest.User != null)
				//entity.UpdatedBy = contextRequest.User.GuidUser;

//	    var oldentity = GetBy(p => p.GuidRouteLocation == entity.GuidRouteLocation, contextRequest).FirstOrDefault();
	//	if (oldentity != null) {
		
          //  entity.CreatedDate = oldentity.CreatedDate;
    //        entity.CreatedBy = oldentity.CreatedBy;
	
      //      entity.GuidCompany = oldentity.GuidCompany;
	
			

	
		//}

			 using( EFContext con = new EFContext()){
				BusinessRulesEventArgs<RouteLocation> e = null;
				bool preventPartial = false; 
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false)
                OnUpdating(this,e = new BusinessRulesEventArgs<RouteLocation>() { ContextRequest = contextRequest, Item=entity});
				   if (e != null) {
						if (e.Cancel)
						{
							//outcontext = null;
							return e.Item;

						}
					}

	string includes = "Student,YBLocation,YBRoute";
	IQueryable < RouteLocation > query = con.RouteLocations.AsQueryable();
	foreach (string include in includes.Split(char.Parse(",")))
                       {
                           if (!string.IsNullOrEmpty(include))
                               query = query.Include(include);
                       }
	var oldentity = query.FirstOrDefault(p => p.GuidRouteLocation == entity.GuidRouteLocation);
	if (oldentity.OrderRoute != entity.OrderRoute)
		oldentity.OrderRoute = entity.OrderRoute;

				//if (entity.UpdatedDate == null || (contextRequest != null && contextRequest.IsFromUI("RouteLocations", UIActions.Updating)))
			oldentity.UpdatedDate = DateTime.Now.ToUniversalTime();
			if(contextRequest.User != null)
				oldentity.UpdatedBy = contextRequest.User.GuidUser;

           


						if (SFSdotNet.Framework.BR.Utils.HasRelationPropertyChanged(oldentity.Student, entity.Student, "GuidStudent"))
							oldentity.Student = entity.Student != null? new Student(){ GuidStudent = entity.Student.GuidStudent } :null;

                


						if (SFSdotNet.Framework.BR.Utils.HasRelationPropertyChanged(oldentity.YBLocation, entity.YBLocation, "GuidLocation"))
							oldentity.YBLocation = entity.YBLocation != null? new YBLocation(){ GuidLocation = entity.YBLocation.GuidLocation } :null;

                


						if (SFSdotNet.Framework.BR.Utils.HasRelationPropertyChanged(oldentity.YBRoute, entity.YBRoute, "GuidRoute"))
							oldentity.YBRoute = entity.YBRoute != null? new YBRoute(){ GuidRoute = entity.YBRoute.GuidRoute } :null;

                


				con.ChangeTracker.Entries().Where(p => p.Entity != oldentity).ForEach(p => p.State = EntityState.Unchanged);  
				  
				con.SaveChanges();
        
					 
					
               
				itemResult = entity;
				if(preventPartial == false)
					OnUpdated(this, e = new BusinessRulesEventArgs<RouteLocation>() { ContextRequest = contextRequest, Item=itemResult });

              	return itemResult;
			}
			  
        }
        public RouteLocation Save(RouteLocation entity)
        {
			return Create(entity);
        }
        public int Save(List<RouteLocation> entities)
        {
			 Create(entities);
            return entities.Count;

        }
        #endregion
        #region Delete
        public void Delete(RouteLocation entity)
        {
				this.Delete(entity, null);
			
        }
		 public void Delete(RouteLocation entity, ContextRequest contextRequest)
        {
				
				  List<RouteLocation> entities = new List<RouteLocation>();
				   entities.Add(entity);
				this.Delete(entities, contextRequest);
			
        }

         public void Delete(string query, Guid[] guids, ContextRequest contextRequest)
        {
			var br = new RouteLocationsBR(true);
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);
            
            Delete(items, contextRequest);

        }
        public void Delete(RouteLocation entity,  ContextRequest contextRequest, BusinessRulesEventArgs<RouteLocation> e = null)
        {
			
				using(EFContext con = new EFContext())
                 {
				
               	BusinessRulesEventArgs<RouteLocation> _e = null;
               List<RouteLocation> _items = new List<RouteLocation>();
                _items.Add(entity);
                if (e == null || e.PreventPartialPropagate == false)
                {
                    OnDeleting(this, _e = (e == null ? new BusinessRulesEventArgs<RouteLocation>() { ContextRequest = contextRequest, Item = entity, Items = null  } : e));
                }
                if (_e != null)
                {
                    if (_e.Cancel)
						{
							context = null;
							return;

						}
					}


				
									//IsDeleted
					bool logicDelete = true;
					if (entity.IsDeleted != null)
					{
						if (entity.IsDeleted.Value)
							logicDelete = false;
					}
					if (logicDelete)
					{
											//entity = GetBy(p =>, contextRequest).FirstOrDefault();
						entity.IsDeleted = true;
						if (contextRequest != null && contextRequest.User != null)
							entity.UpdatedBy = contextRequest.User.GuidUser;
                        entity.UpdatedDate = DateTime.UtcNow;
						UpdateAgile(entity, "IsDeleted","UpdatedBy","UpdatedDate");

						
					}
					else {
					con.Entry<RouteLocation>(entity).State = EntityState.Deleted;
					con.SaveChanges();
				
				 
					}
								
				
				 
					
					
			if (e == null || e.PreventPartialPropagate == false)
                {

                    if (_e == null)
                        _e = new BusinessRulesEventArgs<RouteLocation>() { ContextRequest = contextRequest, Item = entity, Items = null };

                    OnDeleted(this, _e);
                }

				//return null;
			}
        }
 public void UnDelete(string query, Guid[] guids, ContextRequest contextRequest)
        {
            var br = new RouteLocationsBR(true);
            contextRequest.CustomQuery.IncludeDeleted = true;
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);

            foreach (var item in items)
            {
                item.IsDeleted = false;
						if (contextRequest != null && contextRequest.User != null)
							item.UpdatedBy = contextRequest.User.GuidUser;
                        item.UpdatedDate = DateTime.UtcNow;
            }

            UpdateBulk(items, "IsDeleted","UpdatedBy","UpdatedDate");
        }

         public void Delete(List<RouteLocation> entities,  ContextRequest contextRequest = null )
        {
				
			 BusinessRulesEventArgs<RouteLocation> _e = null;

                OnDeleting(this, _e = new BusinessRulesEventArgs<RouteLocation>() { ContextRequest = contextRequest, Item = null, Items = entities });
                if (_e != null)
                {
                    if (_e.Cancel)
                    {
                        context = null;
                        return;

                    }
                }
                bool allSucced = true;
                BusinessRulesEventArgs<RouteLocation> eToChilds = new BusinessRulesEventArgs<RouteLocation>();
                if (_e != null)
                {
                    eToChilds = _e;
                }
                else
                {
                    eToChilds = new BusinessRulesEventArgs<RouteLocation>() { ContextRequest = contextRequest, Item = (entities.Count == 1 ? entities[0] : null), Items = entities };
                }
				foreach (RouteLocation item in entities)
				{
					try
                    {
                        this.Delete(item, contextRequest, e: eToChilds);
                    }
                    catch (Exception ex)
                    {
                        SFSdotNet.Framework.My.EventLog.Error(ex);
                        allSucced = false;
                    }
				}
				if (_e == null)
                    _e = new BusinessRulesEventArgs<RouteLocation>() { ContextRequest = contextRequest, CountResult = entities.Count, Item = null, Items = entities };
                OnDeleted(this, _e);

			
        }
        #endregion
 
        #region GetCount
		 public int GetCount(Expression<Func<RouteLocation, bool>> predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

			return GetCount(predicate, contextRequest);
		}
        public int GetCount(Expression<Func<RouteLocation, bool>> predicate, ContextRequest contextRequest)
        {


		
		 using (EFContext con = new EFContext())
            {


				if (predicate == null) predicate = PredicateBuilder.True<RouteLocation>();
           		predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted == null);
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    		if (contextRequest.User !=null )
                        		if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
									predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa

								}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				
				IQueryable<RouteLocation> query = con.RouteLocations.AsQueryable();
                return query.AsExpandable().Count(predicate);

			
				}
			

        }
		  public int GetCount(string predicate,  ContextRequest contextRequest)
         {
             return GetCount(predicate, null, contextRequest);
         }

         public int GetCount(string predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return GetCount(predicate, contextRequest);
        }
		 public int GetCount(string predicate, string usemode){
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
				return GetCount( predicate,  usemode,  contextRequest);
		 }
        public int GetCount(string predicate, string usemode, ContextRequest contextRequest){

		using (EFContext con = new EFContext()) {
				string computedFields = "";
				string fkIncludes = "Student,YBLocation,YBRoute";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<RouteLocation>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetCount(con, predicate, usemode, contextRequest, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields);

			}
			#region old code
			 /* string freetext = null;
            Filter filter = new Filter();

              if (predicate.Contains("|"))
              {
                 
                  filter.SetFilterPart("ft", GetSpecificFilter(predicate, contextRequest));
                 
                  filter.ProcessText(predicate.Split(char.Parse("|"))[0]);
                  freetext = predicate.Split(char.Parse("|"))[1];

				  if (!string.IsNullOrEmpty(freetext) && string.IsNullOrEmpty(contextRequest.FreeText))
                  {
                      contextRequest.FreeText = freetext;
                  }
              }
              else {
                  filter.ProcessText(predicate);
              }
			   predicate = filter.GetFilterComplete();
			// BusinessRulesEventArgs<RouteLocation>  e = null;
           	using (EFContext con = new EFContext())
			{
			
			

			 QueryBuild(predicate, filter, con, contextRequest, "count", new List<string>());


			
			BusinessRulesEventArgs<RouteLocation> e = null;

			contextRequest.FreeText = freetext;
			contextRequest.UseMode = usemode;
            OnCounting(this, e = new BusinessRulesEventArgs<RouteLocation>() {  Filter =filter, ContextRequest = contextRequest });
            if (e != null)
            {
                if (e.Cancel)
                {
                    context = null;
                    return e.CountResult;

                }

            

            }
			
			StringBuilder sbQuerySystem = new StringBuilder();
		
					
                    filter.SetFilterPart("de","(IsDeleted != true OR IsDeleted == null)");
			
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    	if (contextRequest.User !=null )
                        	if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
                        		
								filter.SetFilterPart("co", @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa
						
						
							}
							
							}
							if (preventSecurityRestrictions) preventSecurityRestrictions= false;
		
				   
                 filter.CleanAndProcess("");
				//string predicateWithFKAndComputed = SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicate );               
				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed();
               string predicateWithManyRelations = filter.GetFilterChildren();
			   ///QueryUtils.BreakeQuery1(predicate, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
			   predicate = filter.GetFilterComplete();
               if (!string.IsNullOrEmpty(predicate))
               {
				
					
                    return con.RouteLocations.Where(predicate).Count();
					
                }else
                    return con.RouteLocations.Count();
					
			}*/
			#endregion

		}
         public int GetCount()
        {
            return GetCount(p => true);
        }
        #endregion
        
         


        public void Delete(List<RouteLocation.CompositeKey> entityKeys)
        {

            List<RouteLocation> items = new List<RouteLocation>();
            foreach (var itemKey in entityKeys)
            {
                items.Add(GetByKey(itemKey.GuidRouteLocation));
            }

            Delete(items);

        }
		 public void UpdateAssociation(string relation, string relationValue, string query, Guid[] ids, ContextRequest contextRequest)
        {
            var items = GetBy(query, null, null, null, null, null, contextRequest, ids);
			 var module = SFSdotNet.Framework.Cache.Caching.SystemObjects.GetModuleByKey(SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(System.Web.HttpContext.Current.Request.RequestContext, "area"));
           
            foreach (var item in items)
            {
			  Guid ? guidRelationValue = null ;
                if (!string.IsNullOrEmpty(relationValue)){
                    guidRelationValue = Guid.Parse(relationValue );
                }

				 if (relation.Contains("."))
                {
                    var partsWithOtherProp = relation.Split(char.Parse("|"));
                    var parts = partsWithOtherProp[0].Split(char.Parse("."));

                    string proxyRelName = parts[0];
                    string proxyProperty = parts[1];
                    string proxyPropertyKeyNameFromOther = partsWithOtherProp[1];
                    //string proxyPropertyThis = parts[2];

                    var prop = item.GetType().GetProperty(proxyRelName);
                    //var entityInfo = //SFSdotNet.Framework.
                    // descubrir el tipo de entidad dentro de la colección
                    Type typeEntityInList = SFSdotNet.Framework.Entities.Utils.GetTypeFromList(prop);
                    var newProxyItem = Activator.CreateInstance(typeEntityInList);
                    var propThisForSet = newProxyItem.GetType().GetProperty(proxyProperty);
                    var entityInfoOfProxy = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(typeEntityInList);
                    var propOther = newProxyItem.GetType().GetProperty(proxyPropertyKeyNameFromOther);

                    if (propThisForSet != null && entityInfoOfProxy != null && propOther != null )
                    {
                        var entityInfoThis = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(item.GetType());
                        var valueThisId = item.GetType().GetProperty(entityInfoThis.PropertyKeyName).GetValue(item);
                        if (valueThisId != null)
                            propThisForSet.SetValue(newProxyItem, valueThisId);
                        propOther.SetValue(newProxyItem, Guid.Parse(relationValue));
                        
                        var entityNameProp = newProxyItem.GetType().GetField("EntityName").GetValue(null);
                        var entitySetNameProp = newProxyItem.GetType().GetField("EntitySetName").GetValue(null);

                        SFSdotNet.Framework.Apps.Integration.CreateItemFromApp(entityNameProp.ToString(), entitySetNameProp.ToString(), module.ModuleNamespace, newProxyItem, contextRequest);

                    }

                    // crear una instancia del tipo de entidad
                    // llenar los datos y registrar nuevo


                }
                else
                {
                var prop = item.GetType().GetProperty(relation);
                var entityInfo = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(prop.PropertyType);
                if (entityInfo != null)
                {
                    var ins = Activator.CreateInstance(prop.PropertyType);
                   if (guidRelationValue != null)
                    {
                        prop.PropertyType.GetProperty(entityInfo.PropertyKeyName).SetValue(ins, guidRelationValue);
                        item.GetType().GetProperty(relation).SetValue(item, ins);
                    }
                    else
                    {
                        item.GetType().GetProperty(relation).SetValue(item, null);
                    }

                    Update(item, contextRequest);
                }

				}
            }
        }
		
	}
		public partial class StudentsBR:BRBase<Student>{
	 	
           
		 #region Partial methods

           partial void OnUpdating(object sender, BusinessRulesEventArgs<Student> e);

            partial void OnUpdated(object sender, BusinessRulesEventArgs<Student> e);
			partial void OnUpdatedAgile(object sender, BusinessRulesEventArgs<Student> e);

            partial void OnCreating(object sender, BusinessRulesEventArgs<Student> e);
            partial void OnCreated(object sender, BusinessRulesEventArgs<Student> e);

            partial void OnDeleting(object sender, BusinessRulesEventArgs<Student> e);
            partial void OnDeleted(object sender, BusinessRulesEventArgs<Student> e);

            partial void OnGetting(object sender, BusinessRulesEventArgs<Student> e);
            protected override void OnVirtualGetting(object sender, BusinessRulesEventArgs<Student> e)
            {
                OnGetting(sender, e);
            }
			protected override void OnVirtualCounting(object sender, BusinessRulesEventArgs<Student> e)
            {
                OnCounting(sender, e);
            }
			partial void OnTaken(object sender, BusinessRulesEventArgs<Student> e);
			protected override void OnVirtualTaken(object sender, BusinessRulesEventArgs<Student> e)
            {
                OnTaken(sender, e);
            }

            partial void OnCounting(object sender, BusinessRulesEventArgs<Student> e);
 
			partial void OnQuerySettings(object sender, BusinessRulesEventArgs<Student> e);
          
            #endregion
			
		private static StudentsBR singlenton =null;
				public static StudentsBR NewInstance(){
					return  new StudentsBR();
					
				}
		public static StudentsBR Instance{
			get{
				if (singlenton == null)
					singlenton = new StudentsBR();
				return singlenton;
			}
		}
		//private bool preventSecurityRestrictions = false;
		 public bool PreventAuditTrail { get; set;  }
		#region Fields
        EFContext context = null;
        #endregion
        #region Constructor
        public StudentsBR()
        {
            context = new EFContext();
        }
		 public StudentsBR(bool preventSecurity)
            {
                this.preventSecurityRestrictions = preventSecurity;
				context = new EFContext();
            }
        #endregion
		
		#region Get

 		public IQueryable<Student> Get()
        {
            using (EFContext con = new EFContext())
            {
				
				var query = con.Students.AsQueryable();
                con.Configuration.ProxyCreationEnabled = false;

                //query = ContextQueryBuilder<Nutrient>.ApplyContextQuery(query, contextRequest);

                return query;




            }

        }
		


 	
		public List<Student> GetAll()
        {
            return this.GetBy(p => true);
        }
        public List<Student> GetAll(string includes)
        {
            return this.GetBy(p => true, includes);
        }
        public Student GetByKey(Guid guidStudent)
        {
            return GetByKey(guidStudent, true);
        }
        public Student GetByKey(Guid guidStudent, bool loadIncludes)
        {
            Student item = null;
			var query = PredicateBuilder.True<Student>();
                    
			string strWhere = @"GuidStudent = Guid(""" + guidStudent.ToString()+@""")";
            Expression<Func<Student, bool>> predicate = null;
            //if (!string.IsNullOrEmpty(strWhere))
            //    predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<Student, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
			 ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
            contextRequest.CustomQuery.FilterExpressionString = strWhere;

			//item = GetBy(predicate, loadIncludes, contextRequest).FirstOrDefault();
			item = GetBy(strWhere,loadIncludes,contextRequest).FirstOrDefault();
            return item;
        }
         public List<Student> GetBy(string strWhere, bool loadRelations, ContextRequest contextRequest)
        {
            if (!loadRelations)
                return GetBy(strWhere, contextRequest);
            else
                return GetBy(strWhere, contextRequest, "");

        }
		  public List<Student> GetBy(string strWhere, bool loadRelations)
        {
              if (!loadRelations)
                return GetBy(strWhere, new ContextRequest());
            else
                return GetBy(strWhere, new ContextRequest(), "");

        }
		         public Student GetByKey(Guid guidStudent, params Expression<Func<Student, object>>[] includes)
        {
            Student item = null;
			string strWhere = @"GuidStudent = Guid(""" + guidStudent.ToString()+@""")";
          Expression<Func<Student, bool>> predicate = p=> p.GuidStudent == guidStudent;
           // if (!string.IsNullOrEmpty(strWhere))
           //     predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<Student, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
        item = GetBy(predicate, includes).FirstOrDefault();
         ////   item = GetBy(strWhere,includes).FirstOrDefault();
			return item;

        }
        public Student GetByKey(Guid guidStudent, string includes)
        {
            Student item = null;
			string strWhere = @"GuidStudent = Guid(""" + guidStudent.ToString()+@""")";
            
			
            item = GetBy(strWhere, includes).FirstOrDefault();
            return item;

        }
		 public Student GetByKey(Guid guidStudent, string usemode, string includes)
		{
			return GetByKey(guidStudent, usemode, null, includes);

		 }
		 public Student GetByKey(Guid guidStudent, string usemode, ContextRequest context,  string includes)
        {
            Student item = null;
			string strWhere = @"GuidStudent = Guid(""" + guidStudent.ToString()+@""")";
			if (context == null){
				context = new ContextRequest();
				context.CustomQuery = new CustomQuery();
				context.CustomQuery.IsByKey = true;
				context.CustomQuery.FilterExpressionString = strWhere;
				context.UseMode = usemode;
			}
            item = GetBy(strWhere,context , includes).FirstOrDefault();
            return item;

        }

        #region Dynamic Predicate
        public List<Student> GetBy(Expression<Func<Student, bool>> predicate, int? pageSize, int? page)
        {
            return this.GetBy(predicate, pageSize, page, null, null);
        }
        public List<Student> GetBy(Expression<Func<Student, bool>> predicate, ContextRequest contextRequest)
        {

            return GetBy(predicate, contextRequest,"");
        }
        
        public List<Student> GetBy(Expression<Func<Student, bool>> predicate, ContextRequest contextRequest, params Expression<Func<Student, object>>[] includes)
        {
            StringBuilder sb = new StringBuilder();
           if (includes != null)
            {
                foreach (var path in includes)
                {

						if (sb.Length > 0) sb.Append(",");
						sb.Append(SFSdotNet.Framework.Linq.Utils.IncludeToString<Student>(path));

               }
            }
            return GetBy(predicate, contextRequest, sb.ToString());
        }
        
        
        public List<Student> GetBy(Expression<Func<Student, bool>> predicate, string includes)
        {
			ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";

            return GetBy(predicate, context, includes);
        }

        public List<Student> GetBy(Expression<Func<Student, bool>> predicate, params Expression<Func<Student, object>>[] includes)
        {
			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest context = new ContextRequest();
			            context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";
            return GetBy(predicate, context, includes);
        }

      
		public bool DisableCache { get; set; }
		public List<Student> GetBy(Expression<Func<Student, bool>> predicate, ContextRequest contextRequest, string includes)
		{
            using (EFContext con = new EFContext()) {
				
				string fkIncludes = "StudentParent,AcademyLevel,Profesor";
                List<string> multilangProperties = new List<string>();
				if (predicate == null) predicate = PredicateBuilder.True<Student>();
                var notDeletedExpression = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;
					Expression<Func<Student,bool>> multitenantExpression  = null;
					if (contextRequest != null && contextRequest.Company != null)	                        	
						multitenantExpression = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicate, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression);

#region Old code
/*
				List<Student> result = null;
               BusinessRulesEventArgs<Student>  e = null;
	
				OnGetting(con, e = new BusinessRulesEventArgs<Student>() {  FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null) });

               // OnGetting(con,e = new BusinessRulesEventArgs<Student>() { FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = contextRequest.CustomQuery.FilterExpressionString});
				   if (e != null) {
				    predicate = e.FilterExpression;
						if (e.Cancel)
						{
							context = null;
							 if (e.Items == null) e.Items = new List<Student>();
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                if (predicate == null) predicate = PredicateBuilder.True<Student>();
 				string fkIncludes = "StudentParent,AcademyLevel,Profesor";
                if(contextRequest!=null){
					if (contextRequest.CustomQuery != null)
					{
						if (contextRequest.CustomQuery.IncludeForeignKeyPaths != null) {
							if (contextRequest.CustomQuery.IncludeForeignKeyPaths.Value == false)
								fkIncludes = "";
						}
					}
				}
				if (!string.IsNullOrEmpty(includes))
					includes = includes + "," + fkIncludes;
				else
					includes = fkIncludes;
                
                //var es = _repository.Queryable;

                IQueryable<Student> query =  con.Students.AsQueryable();

                                if (!string.IsNullOrEmpty(includes))
                {
                    foreach (string include in includes.Split(char.Parse(",")))
                    {
						if (!string.IsNullOrEmpty(include))
                            query = query.Include(include);
                    }
                }
                    predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
					 	if (!preventSecurityRestrictions)
						{
							if (contextRequest != null )
		                    	if (contextRequest.User !=null )
		                        	if (contextRequest.Company != null){
		                        	
										predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
 									
									}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				query =query.AsExpandable().Where(predicate);
                query = ContextQueryBuilder<Student>.ApplyContextQuery(query, contextRequest);

                result = query.AsNoTracking().ToList<Student>();
				  
                if (e != null)
                {
                    e.Items = result;
                }
				//if (contextRequest != null ){
				//	 contextRequest = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
					contextRequest.CustomQuery = new CustomQuery();

				//}
				OnTaken(this, e == null ? e =  new BusinessRulesEventArgs<Student>() { Items= result, IncludingComputedLinq = false, ContextRequest = contextRequest,  FilterExpression = predicate } :  e);
  
			

                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
				*/
#endregion
            }
        }


		/*public int Update(List<Student> items, ContextRequest contextRequest)
            {
                int result = 0;
                using (EFContext con = new EFContext())
                {
                   
                

                    foreach (var item in items)
                    {
                        //secMessageToUser messageToUser = new secMessageToUser();
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            item.GetType().GetProperty(prop).SetValue(item, item.GetType().GetProperty(prop).GetValue(item));
                        }
                        //messageToUser.GuidMessageToUser = (Guid)item.GetType().GetProperty("GuidMessageToUser").GetValue(item);

                        var setObject = con.CreateObjectSet<Student>("Students");
                        //messageToUser.Readed = DateTime.UtcNow;
                        setObject.Attach(item);
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            con.ObjectStateManager.GetObjectStateEntry(item).SetModifiedProperty(prop);
                        }
                       
                    }
                    result = con.SaveChanges();

                    


                }
                return result;
            }
           */
		

        public List<Student> GetBy(string predicateString, ContextRequest contextRequest, string includes)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
				


				string computedFields = "";
				string fkIncludes = "StudentParent,AcademyLevel,Profesor";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<Student>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					//if (contextRequest != null && contextRequest.Company != null)                      	
					//	 multitenantExpression = @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))";
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicateString, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression,computedFields);


	#region Old Code
	/*
				BusinessRulesEventArgs<Student> e = null;

				Filter filter = new Filter();
                if (predicateString.Contains("|"))
                {
                    string ft = GetSpecificFilter(predicateString, contextRequest);
                    if (!string.IsNullOrEmpty(ft))
                        filter.SetFilterPart("ft", ft);
                   
                    contextRequest.FreeText = predicateString.Split(char.Parse("|"))[1];
                    var q1 = predicateString.Split(char.Parse("|"))[0];
                    if (!string.IsNullOrEmpty(q1))
                    {
                        filter.ProcessText(q1);
                    }
                }
                else {
                    filter.ProcessText(predicateString);
                }
				 var includesList = (new List<string>());
                 if (!string.IsNullOrEmpty(includes))
                 {
                     includesList = includes.Split(char.Parse(",")).ToList();
                 }

				List<Student> result = new List<Student>();
         
			QueryBuild(predicateString, filter, con, contextRequest, "getby", includesList);
			 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }
				
				
					OnGetting(con, e == null ? e = new BusinessRulesEventArgs<Student>() { Filter = filter, ContextRequest = contextRequest  } : e );

                  //OnGetting(con,e = new BusinessRulesEventArgs<Student>() {  ContextRequest = contextRequest, FilterExpressionString = predicateString });
			   	if (e != null) {
				    //predicateString = e.GetQueryString();
						if (e.Cancel)
						{
							context = null;
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				//	 else {
                //      predicateString = predicateString.Replace("*extraFreeText*", "").Replace("()","");
                //  }
				//con.EnableChangeTrackingUsingProxies = false;
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                //if (predicate == null) predicate = PredicateBuilder.True<Student>();
 				string fkIncludes = "StudentParent,AcademyLevel,Profesor";
                if(contextRequest!=null){
					if (contextRequest.CustomQuery != null)
					{
						if (contextRequest.CustomQuery.IncludeForeignKeyPaths != null) {
							if (contextRequest.CustomQuery.IncludeForeignKeyPaths.Value == false)
								fkIncludes = "";
						}
					}
				}else{
                    contextRequest = new ContextRequest();
                    contextRequest.CustomQuery = new CustomQuery();

                }
				if (!string.IsNullOrEmpty(includes))
					includes = includes + "," + fkIncludes;
				else
					includes = fkIncludes;
                
                //var es = _repository.Queryable;
				IQueryable<Student> query = con.Students.AsQueryable();
		
				// include relations FK
				if(string.IsNullOrEmpty(includes) ){
					includes ="";
				}
				StringBuilder sbQuerySystem = new StringBuilder();
                    //predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				

				//if (!string.IsNullOrEmpty(predicateString))
                //      sbQuerySystem.Append(" And ");
                //sbQuerySystem.Append(" (IsDeleted != true Or IsDeleted = null) ");
				 filter.SetFilterPart("de", "(IsDeleted != true OR IsDeleted = null)");


					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
	                    	if (contextRequest.User !=null )
	                        	if (contextRequest.Company != null ){
	                        		//if (sbQuerySystem.Length > 0)
	                        		//	    			sbQuerySystem.Append( " And ");	
									//sbQuerySystem.Append(@" (GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa

									filter.SetFilterPart("co",@"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))");

								}
						}	
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				//string predicateString = predicate.ToDynamicLinq<Student>();
				//predicateString += sbQuerySystem.ToString();
				filter.CleanAndProcess("");

				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed(); //SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicateString );               
                string predicateWithManyRelations = filter.GetFilterChildren(); //SFSdotNet.Framework.Linq.Utils.CleanPartExpression(predicateString);

                //QueryUtils.BreakeQuery1(predicateString, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
                var _queryable = query.AsQueryable();
				bool includeAll = true; 
                if (!string.IsNullOrEmpty(predicateWithManyRelations))
                    _queryable = _queryable.Where(predicateWithManyRelations, contextRequest.CustomQuery.ExtraParams);
				if (contextRequest.CustomQuery.SpecificProperties.Count > 0)
                {

				includeAll = false; 
                }

				StringBuilder sbSelect = new StringBuilder();
                sbSelect.Append("new (");
                bool existPrev = false;
                foreach (var selected in contextRequest.CustomQuery.SelectedFields.Where(p=> !string.IsNullOrEmpty(p.Linq)))
                {
                    if (existPrev) sbSelect.Append(", ");
                    if (!selected.Linq.Contains(".") && !selected.Linq.StartsWith("it."))
                        sbSelect.Append("it." + selected.Linq);
                    else
                        sbSelect.Append(selected.Linq);
                    existPrev = true;
                }
                sbSelect.Append(")");
                var queryable = _queryable.Select(sbSelect.ToString());                    


     				
                 if (!string.IsNullOrEmpty(predicateWithFKAndComputed))
                    queryable = queryable.Where(predicateWithFKAndComputed, contextRequest.CustomQuery.ExtraParams);

				QueryComplementOptions queryOps = ContextQueryBuilder.ApplyContextQuery(contextRequest);
            	if (!string.IsNullOrEmpty(queryOps.OrderByAndSort)){
					if (queryOps.OrderBy.Contains(".") && !queryOps.OrderBy.StartsWith("it.")) queryOps.OrderBy = "it." + queryOps.OrderBy;
					queryable = queryable.OrderBy(queryOps.OrderByAndSort);
					}
               	if (queryOps.Skip != null)
                {
                    queryable = queryable.Skip(queryOps.Skip.Value);
                }
                if (queryOps.PageSize != null)
                {
                    queryable = queryable.Take (queryOps.PageSize.Value);
                }


                var resultTemp = queryable.AsQueryable().ToListAsync().Result;
                foreach (var item in resultTemp)
                {

				   result.Add(SFSdotNet.Framework.BR.Utils.GetConverted<Student,dynamic>(item, contextRequest.CustomQuery.SelectedFields.Select(p=>p.Name).ToArray()));
                }

			 if (e != null)
                {
                    e.Items = result;
                }
				 contextRequest.CustomQuery = new CustomQuery();
				OnTaken(this, e == null ? e = new BusinessRulesEventArgs<Student>() { Items= result, IncludingComputedLinq = true, ContextRequest = contextRequest, FilterExpressionString  = predicateString } :  e);
  
			
  
                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
	
	*/
	#endregion

            }
        }
		public Student GetFromOperation(string function, string filterString, string usemode, string fields, ContextRequest contextRequest)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
                string computedFields = "";
               // string fkIncludes = "accContpaqiClassification,accProjectConcept,accProjectType,accProxyUser";
                List<string> multilangProperties = new List<string>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					if (contextRequest != null && contextRequest.Company != null)
					{
						multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
						contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
					}
					 									
					string multiTenantField = "GuidCompany";


                return GetSummaryOperation(con, new Student(), function, filterString, usemode, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields, contextRequest, fields.Split(char.Parse(",")).ToArray());
            }
        }

   protected override void QueryBuild(string predicate, Filter filter, DbContext efContext, ContextRequest contextRequest, string method, List<string> includesList)
      	{
				if (contextRequest.CustomQuery.SpecificProperties.Count == 0)
                {
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.FullName);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.BirthDate);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.Grade);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.Group);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.GuidParent);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.GuidCompany);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.CreatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.UpdatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.CreatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.UpdatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.Bytes);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.IsDeleted);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.Comments);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.GuidAcademyLevel);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.GuidProfesor);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.StudentParent);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.AcademyLevel);
					contextRequest.CustomQuery.SpecificProperties.Add(Student.PropertyNames.Profesor);
                    
				}

				if (method == "getby" || method == "sum")
				{
					if (!contextRequest.CustomQuery.SpecificProperties.Contains("GuidStudent")){
						contextRequest.CustomQuery.SpecificProperties.Add("GuidStudent");
					}

					 if (!string.IsNullOrEmpty(contextRequest.CustomQuery.OrderBy))
					{
						string existPropertyOrderBy = contextRequest.CustomQuery.OrderBy;
						if (contextRequest.CustomQuery.OrderBy.Contains("."))
						{
							existPropertyOrderBy = contextRequest.CustomQuery.OrderBy.Split(char.Parse("."))[0];
						}
						if (!contextRequest.CustomQuery.SpecificProperties.Exists(p => p == existPropertyOrderBy))
						{
							contextRequest.CustomQuery.SpecificProperties.Add(existPropertyOrderBy);
						}
					}

				}
				
	bool isFullDetails = contextRequest.IsFromUI("Students", UIActions.GetForDetails);
	string filterForTest = predicate  + filter.GetFilterComplete();

				if (isFullDetails || !string.IsNullOrEmpty(predicate))
            {
            } 

			if (method == "sum")
            {
            } 
			if (contextRequest.CustomQuery.SelectedFields.Count == 0)
            {
				foreach (var selected in contextRequest.CustomQuery.SpecificProperties)
                {
					string linq = selected;
					switch (selected)
                    {

					case "StudentParent":
					if (includesList.Contains(selected)){
                        linq = "it.StudentParent as StudentParent";
					}
                    else
						linq = "iif(it.StudentParent != null, new (it.StudentParent.GuidStudentParent, it.StudentParent.FullName), null) as StudentParent";
 					break;
					case "AcademyLevel":
					if (includesList.Contains(selected)){
                        linq = "it.AcademyLevel as AcademyLevel";
					}
                    else
						linq = "iif(it.AcademyLevel != null, new (it.AcademyLevel.GuidAcademyLevel, it.AcademyLevel.Title), null) as AcademyLevel";
 					break;
					case "Profesor":
					if (includesList.Contains(selected)){
                        linq = "it.Profesor as Profesor";
					}
                    else
						linq = "iif(it.Profesor != null, new (it.Profesor.GuidProfesor, it.Profesor.FullName), null) as Profesor";
 					break;
					 
						
					 default:
                            break;
                    }
					contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name=selected, Linq=linq});
					if (method == "getby" || method == "sum")
					{
						if (includesList.Contains(selected))
							includesList.Remove(selected);

					}

				}
			}
				if (method == "getby" || method == "sum")
				{
					foreach (var otherInclude in includesList.Where(p=> !string.IsNullOrEmpty(p)))
					{
						contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name = otherInclude, Linq = "it." + otherInclude +" as " + otherInclude });
					}
				}
				BusinessRulesEventArgs<Student> e = null;
				if (contextRequest.PreventInterceptors == false)
					OnQuerySettings(efContext, e = new BusinessRulesEventArgs<Student>() { Filter = filter, ContextRequest = contextRequest /*, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null)*/ });

				//List<Student> result = new List<Student>();
                 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }

}
		public List<Student> GetBy(Expression<Func<Student, bool>> predicate, bool loadRelations, ContextRequest contextRequest)
        {
			if(!loadRelations)
				return GetBy(predicate, contextRequest);
			else
				return GetBy(predicate, contextRequest, "RouteLocations,StudentLocations");

        }

        public List<Student> GetBy(Expression<Func<Student, bool>> predicate, int? pageSize, int? page, string orderBy, SFSdotNet.Framework.Data.SortDirection? sortDirection)
        {
            return GetBy(predicate, new ContextRequest() { CustomQuery = new CustomQuery() { Page = page, PageSize = pageSize, OrderBy = orderBy, SortDirection = sortDirection } });
        }
        public List<Student> GetBy(Expression<Func<Student, bool>> predicate)
        {

			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
			contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
			            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            contextRequest.CustomQuery.FilterExpressionString = null;
            return this.GetBy(predicate, contextRequest, "");
        }
        #endregion
        #region Dynamic String
		protected override string GetSpecificFilter(string filter, ContextRequest contextRequest) {
            string result = "";
		    //string linqFilter = String.Empty;
            string freeTextFilter = String.Empty;
            if (filter.Contains("|"))
            {
               // linqFilter = filter.Split(char.Parse("|"))[0];
                freeTextFilter = filter.Split(char.Parse("|"))[1];
            }
            //else {
            //    freeTextFilter = filter;
            //}
            //else {
            //    linqFilter = filter;
            //}
			// linqFilter = SFSdotNet.Framework.Linq.Utils.ReplaceCustomDateFilters(linqFilter);
            //string specificFilter = linqFilter;
            if (!string.IsNullOrEmpty(freeTextFilter))
            {
                System.Text.StringBuilder sbCont = new System.Text.StringBuilder();
                /*if (specificFilter.Length > 0)
                {
                    sbCont.Append(" AND ");
                    sbCont.Append(" ({0})");
                }
                else
                {
                    sbCont.Append("{0}");
                }*/
                //var words = freeTextFilter.Split(char.Parse(" "));
				var word = freeTextFilter;
                System.Text.StringBuilder sbSpec = new System.Text.StringBuilder();
                 int nWords = 1;
				/*foreach (var word in words)
                {
					if (word.Length > 0){
                    if (sbSpec.Length > 0) sbSpec.Append(" AND ");
					if (words.Length > 1) sbSpec.Append("("); */
					
	
					
					
					
									
					sbSpec.Append(string.Format(@"FullName.Contains(""{0}"")", word));
					

					
	
					
	
					
					
										sbSpec.Append(" OR ");
					
									
					sbSpec.Append(string.Format(@"Group.Contains(""{0}"")", word));
					

					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
					
										sbSpec.Append(" OR ");
					
									
					sbSpec.Append(string.Format(@"Comments.Contains(""{0}"")", word));
					

					
	
					
	
					
	
					
	
					
	
					
								sbSpec.Append(" OR ");
					
					//if (sbSpec.Length > 2)
					//	sbSpec.Append(" OR "); // test
					sbSpec.Append(string.Format(@"it.StudentParent.FullName.Contains(""{0}"")", word)+" OR "+string.Format(@"it.AcademyLevel.Title.Contains(""{0}"")", word)+" OR "+string.Format(@"it.Profesor.FullName.Contains(""{0}"")", word));
								 //sbSpec.Append("*extraFreeText*");

                    /*if (words.Length > 1) sbSpec.Append(")");
					
					nWords++;

					}

                }*/
                //specificFilter = string.Format("{0}{1}", specificFilter, string.Format(sbCont.ToString(), sbSpec.ToString()));
                                 result = sbSpec.ToString();  
            }
			//result = specificFilter;
			
			return result;

		}
	
			public List<Student> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params object[] extraParams)
        {
			return GetBy(filter, pageSize, page, orderBy, orderDir,  null, extraParams);
		}
           public List<Student> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, string usemode, params object[] extraParams)
            { 
                return GetBy(filter, pageSize, page, orderBy, orderDir, usemode, null, extraParams);
            }


		public List<Student> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  string usemode, ContextRequest context, params object[] extraParams)

        {

            // string freetext = null;
            //if (filter.Contains("|"))
            //{
            //    int parts = filter.Split(char.Parse("|")).Count();
            //    if (parts > 1)
            //    {

            //        freetext = filter.Split(char.Parse("|"))[1];
            //    }
            //}
		
            //string specificFilter = "";
            //if (!string.IsNullOrEmpty(filter))
            //  specificFilter=  GetSpecificFilter(filter);
            if (string.IsNullOrEmpty(orderBy))
            {
			                orderBy = "UpdatedDate";
            }
			//orderDir = "desc";
			SFSdotNet.Framework.Data.SortDirection direction = SFSdotNet.Framework.Data.SortDirection.Ascending;
            if (!string.IsNullOrEmpty(orderDir))
            {
                if (orderDir == "desc")
                    direction = SFSdotNet.Framework.Data.SortDirection.Descending;
            }
            if (context == null)
                context = new ContextRequest();
			

             context.UseMode = usemode;
             if (context.CustomQuery == null )
                context.CustomQuery =new SFSdotNet.Framework.My.CustomQuery();

 
                context.CustomQuery.ExtraParams = extraParams;

                    context.CustomQuery.OrderBy = orderBy;
                   context.CustomQuery.SortDirection = direction;
                   context.CustomQuery.Page = page;
                  context.CustomQuery.PageSize = pageSize;
               

            

            if (!preventSecurityRestrictions) {
			 if (context.CurrentContext == null)
                {
					if (SFSdotNet.Framework.My.Context.CurrentContext != null &&  SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
					{
						context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
						context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

					}
					else {
						throw new Exception("The security rule require a specific user and company");
					}
				}
            }
            return GetBy(filter, context);
  
        }


        public List<Student> GetBy(string strWhere, ContextRequest contextRequest)
        {
        	#region old code
				
				 //Expression<Func<tvsReservationTransport, bool>> predicate = null;
				string strWhereClean = strWhere.Replace("*extraFreeText*", "").Replace("()", "");
                //if (!string.IsNullOrEmpty(strWhereClean)){

                //    object[] extraParams = null;
                //    //if (contextRequest != null )
                //    //    if (contextRequest.CustomQuery != null )
                //    //        extraParams = contextRequest.CustomQuery.ExtraParams;
                //    //predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<tvsReservationTransport, bool>(strWhereClean, extraParams != null? extraParams.Cast<Guid>(): null);				
                //}
				 if (contextRequest == null)
                {
                    contextRequest = new ContextRequest();
                    if (contextRequest.CustomQuery == null)
                        contextRequest.CustomQuery = new CustomQuery();
                }
                  if (!preventSecurityRestrictions) {
					if (contextRequest.User == null || contextRequest.Company == null)
                      {
                     if (SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
                     {
                         contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                         contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

                     }
                     else {
                         throw new Exception("The security rule require a specific User and Company ");
                     }
					 }
                 }
            contextRequest.CustomQuery.FilterExpressionString = strWhere;
				//return GetBy(predicate, contextRequest);  

			#endregion				
				
                    return GetBy(strWhere, contextRequest, "");  


        }
       public List<Student> GetBy(string strWhere)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
			
            return GetBy(strWhere, context, null);
        }

        public List<Student> GetBy(string strWhere, string includes)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
            return GetBy(strWhere, context, includes);
        }

        #endregion
        #endregion
		
		  #region SaveOrUpdate
        
 		 public Student Create(Student entity)
        {
				//ObjectContext context = null;
				    if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: Create");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

				return this.Create(entity, contextRequest);


        }
        
       
        public Student Create(Student entity, ContextRequest contextRequest)
        {
		
		bool graph = false;
	
				bool preventPartial = false;
                if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
               
			using (EFContext con = new EFContext()) {

				Student itemForSave = new Student();
#region Autos
		if(!preventSecurityRestrictions){

				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion
               BusinessRulesEventArgs<Student> e = null;
			    if (preventPartial == false )
                OnCreating(this,e = new BusinessRulesEventArgs<Student>() { ContextRequest = contextRequest, Item=entity });
				   if (e != null) {
						if (e.Cancel)
						{
							context = null;
							return e.Item;

						}
					}

                    if (entity.GuidStudent == Guid.Empty)
                   {
                       entity.GuidStudent = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   itemForSave.GuidStudent = entity.GuidStudent;
				  
		
			itemForSave.GuidStudent = entity.GuidStudent;

			itemForSave.FullName = entity.FullName;

			itemForSave.BirthDate = entity.BirthDate;

			itemForSave.Grade = entity.Grade;

			itemForSave.Group = entity.Group;

			itemForSave.GuidCompany = entity.GuidCompany;

			itemForSave.CreatedDate = entity.CreatedDate;

			itemForSave.UpdatedDate = entity.UpdatedDate;

			itemForSave.CreatedBy = entity.CreatedBy;

			itemForSave.UpdatedBy = entity.UpdatedBy;

			itemForSave.Bytes = entity.Bytes;

			itemForSave.IsDeleted = entity.IsDeleted;

			itemForSave.Comments = entity.Comments;

				
				con.Students.Add(itemForSave);







					if (entity.StudentParent != null)
					{
						var studentParent = new StudentParent();
						studentParent.GuidStudentParent = entity.StudentParent.GuidStudentParent;
						itemForSave.StudentParent = studentParent;
						SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<StudentParent>(con, itemForSave.StudentParent);
			
					}




					if (entity.AcademyLevel != null)
					{
						var academyLevel = new AcademyLevel();
						academyLevel.GuidAcademyLevel = entity.AcademyLevel.GuidAcademyLevel;
						itemForSave.AcademyLevel = academyLevel;
						SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<AcademyLevel>(con, itemForSave.AcademyLevel);
			
					}




					if (entity.Profesor != null)
					{
						var profesor = new Profesor();
						profesor.GuidProfesor = entity.Profesor.GuidProfesor;
						itemForSave.Profesor = profesor;
						SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<Profesor>(con, itemForSave.Profesor);
			
					}



                
				con.ChangeTracker.Entries().Where(p => p.Entity != itemForSave && p.State != EntityState.Unchanged).ForEach(p => p.State = EntityState.Detached);

				con.Entry<Student>(itemForSave).State = EntityState.Added;

				con.SaveChanges();

					 
				

				//itemResult = entity;
                //if (e != null)
                //{
                 //   e.Item = itemResult;
                //}
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false )
                OnCreated(this, e == null ? e = new BusinessRulesEventArgs<Student>() { ContextRequest = contextRequest, Item = entity } : e);



                if (e != null && e.Item != null )
                {
                    return e.Item;
                }
                              return entity;
			}
            
        }
        //BusinessRulesEventArgs<Student> e = null;
        public void Create(List<Student> entities)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            Create(entities, contextRequest);
        }
        public void Create(List<Student> entities, ContextRequest contextRequest)
        
        {
			ObjectContext context = null;
            	foreach (Student entity in entities)
				{
					this.Create(entity, contextRequest);
				}
        }
		  public void CreateOrUpdateBulk(List<Student> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "cu", contextRequest);
        }

        private void CreateOrUpdateBulk(List<Student> entities, string actionKey, ContextRequest contextRequest)
        {
			if (entities.Count() > 0){
            bool graph = false;

            bool preventPartial = false;
            if (contextRequest != null && contextRequest.PreventInterceptors == true)
            {
                preventPartial = true;
            }
            foreach (var entity in entities)
            {
                    if (entity.GuidStudent == Guid.Empty)
                   {
                       entity.GuidStudent = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   
				  


#region Autos
		if(!preventSecurityRestrictions){


 if (actionKey != "u")
                        {
				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;


}
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion


		
			//entity.GuidStudent = entity.GuidStudent;

			//entity.FullName = entity.FullName;

			//entity.BirthDate = entity.BirthDate;

			//entity.Grade = entity.Grade;

			//entity.Group = entity.Group;

			//entity.GuidCompany = entity.GuidCompany;

			//entity.CreatedDate = entity.CreatedDate;

			//entity.UpdatedDate = entity.UpdatedDate;

			//entity.CreatedBy = entity.CreatedBy;

			//entity.UpdatedBy = entity.UpdatedBy;

			//entity.Bytes = entity.Bytes;

			//entity.IsDeleted = entity.IsDeleted;

			//entity.Comments = entity.Comments;

				
				







				    if (entity.StudentParent != null)
					{
						//var studentParent = new StudentParent();
						entity.GuidParent = entity.StudentParent.GuidStudentParent;
						//entity.StudentParent = studentParent;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<StudentParent>(con, itemForSave.StudentParent);
			
					}




				    if (entity.AcademyLevel != null)
					{
						//var academyLevel = new AcademyLevel();
						entity.GuidAcademyLevel = entity.AcademyLevel.GuidAcademyLevel;
						//entity.AcademyLevel = academyLevel;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<AcademyLevel>(con, itemForSave.AcademyLevel);
			
					}




				    if (entity.Profesor != null)
					{
						//var profesor = new Profesor();
						entity.GuidProfesor = entity.Profesor.GuidProfesor;
						//entity.Profesor = profesor;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<Profesor>(con, itemForSave.Profesor);
			
					}



                
				

					 
				

				//itemResult = entity;
            }
            using (EFContext con = new EFContext())
            {
                 if (actionKey == "c")
                    {
                        context.BulkInsert(entities);
                    }else if ( actionKey == "u")
                    {
                        context.BulkUpdate(entities);
                    }else
                    {
                        context.BulkInsertOrUpdate(entities);
                    }
            }

			}
        }
	
		public void CreateBulk(List<Student> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "c", contextRequest);
        }


		public void UpdateAgile(Student item, params string[] fields)
         {
			UpdateAgile(item, null, fields);
        }
		public void UpdateAgile(Student item, ContextRequest contextRequest, params string[] fields)
         {
            
             ContextRequest contextNew = null;
             if (contextRequest != null)
             {
                 contextNew = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
                 if (fields != null && fields.Length > 0)
                 {
                     contextNew.CustomQuery.SpecificProperties  = fields.ToList();
                 }
                 else if(contextRequest.CustomQuery.SpecificProperties.Count > 0)
                 {
                     fields = contextRequest.CustomQuery.SpecificProperties.ToArray();
                 }
             }
			

		   using (EFContext con = new EFContext())
            {



               
					List<string> propForCopy = new List<string>();
                    propForCopy.AddRange(fields);
                    
					  
					if (!propForCopy.Contains("GuidStudent"))
						propForCopy.Add("GuidStudent");

					var itemForUpdate = SFSdotNet.Framework.BR.Utils.GetConverted<Student,Student>(item, propForCopy.ToArray());
					 itemForUpdate.GuidStudent = item.GuidStudent;
                  var setT = con.Set<Student>().Attach(itemForUpdate);

					if (fields.Count() > 0)
					  {
						  item.ModifiedProperties = fields;
					  }
                    foreach (var property in item.ModifiedProperties)
					{						
                        if (property != "GuidStudent")
                             con.Entry(setT).Property(property).IsModified = true;

                    }

                
               int result = con.SaveChanges();
               if (result != 1)
               {
                   SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: 1");
               }


            }

			OnUpdatedAgile(this, new BusinessRulesEventArgs<Student>() { Item = item, ContextRequest = contextNew  });

         }
		public void UpdateBulk(List<Student>  items, params string[] fields)
         {
             SFSdotNet.Framework.My.ContextRequest req = new SFSdotNet.Framework.My.ContextRequest();
             req.CustomQuery = new SFSdotNet.Framework.My.CustomQuery();
             foreach (var field in fields)
             {
                 req.CustomQuery.SpecificProperties.Add(field);
             }
             UpdateBulk(items, req);

         }

		 public void DeleteBulk(List<Student> entities, ContextRequest contextRequest = null)
        {

            using (EFContext con = new EFContext())
            {
                foreach (var entity in entities)
                {
					var entityProxy = new Student() { GuidStudent = entity.GuidStudent };

                    con.Entry<Student>(entityProxy).State = EntityState.Deleted;

                }

                int result = con.SaveChanges();
                if (result != entities.Count)
                {
                    SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: " + entities.Count.ToString());
                }
            }

        }

        public void UpdateBulk(List<Student> items, ContextRequest contextRequest)
        {
            if (items.Count() > 0){

			 foreach (var entity in items)
            {


#region Autos
		if(!preventSecurityRestrictions){

				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	



			}
#endregion








				    if (entity.StudentParent != null)
					{
						//var studentParent = new StudentParent();
						entity.GuidParent = entity.StudentParent.GuidStudentParent;
						//entity.StudentParent = studentParent;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<StudentParent>(con, itemForSave.StudentParent);
			
					}




				    if (entity.AcademyLevel != null)
					{
						//var academyLevel = new AcademyLevel();
						entity.GuidAcademyLevel = entity.AcademyLevel.GuidAcademyLevel;
						//entity.AcademyLevel = academyLevel;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<AcademyLevel>(con, itemForSave.AcademyLevel);
			
					}




				    if (entity.Profesor != null)
					{
						//var profesor = new Profesor();
						entity.GuidProfesor = entity.Profesor.GuidProfesor;
						//entity.Profesor = profesor;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<Profesor>(con, itemForSave.Profesor);
			
					}



				}
				using (EFContext con = new EFContext())
				{

                    
                
                   con.BulkUpdate(items);

				}
             
			}	  
        }

         public Student Update(Student entity)
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return Update(entity, contextRequest);
        }
       
         public Student Update(Student entity, ContextRequest contextRequest)
        {
		 if ((System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null) && contextRequest == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Update");
            }
            if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            }

			
				Student  itemResult = null;

	
			//entity.UpdatedDate = DateTime.Now.ToUniversalTime();
			//if(contextRequest.User != null)
				//entity.UpdatedBy = contextRequest.User.GuidUser;

//	    var oldentity = GetBy(p => p.GuidStudent == entity.GuidStudent, contextRequest).FirstOrDefault();
	//	if (oldentity != null) {
		
          //  entity.CreatedDate = oldentity.CreatedDate;
    //        entity.CreatedBy = oldentity.CreatedBy;
	
      //      entity.GuidCompany = oldentity.GuidCompany;
	
			

	
		//}

			 using( EFContext con = new EFContext()){
				BusinessRulesEventArgs<Student> e = null;
				bool preventPartial = false; 
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false)
                OnUpdating(this,e = new BusinessRulesEventArgs<Student>() { ContextRequest = contextRequest, Item=entity});
				   if (e != null) {
						if (e.Cancel)
						{
							//outcontext = null;
							return e.Item;

						}
					}

	string includes = "StudentParent,AcademyLevel,Profesor";
	IQueryable < Student > query = con.Students.AsQueryable();
	foreach (string include in includes.Split(char.Parse(",")))
                       {
                           if (!string.IsNullOrEmpty(include))
                               query = query.Include(include);
                       }
	var oldentity = query.FirstOrDefault(p => p.GuidStudent == entity.GuidStudent);
	if (oldentity.FullName != entity.FullName)
		oldentity.FullName = entity.FullName;
	if (oldentity.BirthDate != entity.BirthDate)
		oldentity.BirthDate = entity.BirthDate;
	if (oldentity.Grade != entity.Grade)
		oldentity.Grade = entity.Grade;
	if (oldentity.Group != entity.Group)
		oldentity.Group = entity.Group;
	if (oldentity.Comments != entity.Comments)
		oldentity.Comments = entity.Comments;

				//if (entity.UpdatedDate == null || (contextRequest != null && contextRequest.IsFromUI("Students", UIActions.Updating)))
			oldentity.UpdatedDate = DateTime.Now.ToUniversalTime();
			if(contextRequest.User != null)
				oldentity.UpdatedBy = contextRequest.User.GuidUser;

           


				if (entity.RouteLocations != null)
                {
                    foreach (var item in entity.RouteLocations)
                    {


                        
                    }
					
                    

                }


				if (entity.StudentLocations != null)
                {
                    foreach (var item in entity.StudentLocations)
                    {


                        
                    }
					
                    

                }


						if (SFSdotNet.Framework.BR.Utils.HasRelationPropertyChanged(oldentity.StudentParent, entity.StudentParent, "GuidStudentParent"))
							oldentity.StudentParent = entity.StudentParent != null? new StudentParent(){ GuidStudentParent = entity.StudentParent.GuidStudentParent } :null;

                


						if (SFSdotNet.Framework.BR.Utils.HasRelationPropertyChanged(oldentity.AcademyLevel, entity.AcademyLevel, "GuidAcademyLevel"))
							oldentity.AcademyLevel = entity.AcademyLevel != null? new AcademyLevel(){ GuidAcademyLevel = entity.AcademyLevel.GuidAcademyLevel } :null;

                


						if (SFSdotNet.Framework.BR.Utils.HasRelationPropertyChanged(oldentity.Profesor, entity.Profesor, "GuidProfesor"))
							oldentity.Profesor = entity.Profesor != null? new Profesor(){ GuidProfesor = entity.Profesor.GuidProfesor } :null;

                


				con.ChangeTracker.Entries().Where(p => p.Entity != oldentity).ForEach(p => p.State = EntityState.Unchanged);  
				  
				con.SaveChanges();
        
					 
					
               
				itemResult = entity;
				if(preventPartial == false)
					OnUpdated(this, e = new BusinessRulesEventArgs<Student>() { ContextRequest = contextRequest, Item=itemResult });

              	return itemResult;
			}
			  
        }
        public Student Save(Student entity)
        {
			return Create(entity);
        }
        public int Save(List<Student> entities)
        {
			 Create(entities);
            return entities.Count;

        }
        #endregion
        #region Delete
        public void Delete(Student entity)
        {
				this.Delete(entity, null);
			
        }
		 public void Delete(Student entity, ContextRequest contextRequest)
        {
				
				  List<Student> entities = new List<Student>();
				   entities.Add(entity);
				this.Delete(entities, contextRequest);
			
        }

         public void Delete(string query, Guid[] guids, ContextRequest contextRequest)
        {
			var br = new StudentsBR(true);
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);
            
            Delete(items, contextRequest);

        }
        public void Delete(Student entity,  ContextRequest contextRequest, BusinessRulesEventArgs<Student> e = null)
        {
			
				using(EFContext con = new EFContext())
                 {
				
               	BusinessRulesEventArgs<Student> _e = null;
               List<Student> _items = new List<Student>();
                _items.Add(entity);
                if (e == null || e.PreventPartialPropagate == false)
                {
                    OnDeleting(this, _e = (e == null ? new BusinessRulesEventArgs<Student>() { ContextRequest = contextRequest, Item = entity, Items = null  } : e));
                }
                if (_e != null)
                {
                    if (_e.Cancel)
						{
							context = null;
							return;

						}
					}


				
									//IsDeleted
					bool logicDelete = true;
					if (entity.IsDeleted != null)
					{
						if (entity.IsDeleted.Value)
							logicDelete = false;
					}
					if (logicDelete)
					{
											//entity = GetBy(p =>, contextRequest).FirstOrDefault();
						entity.IsDeleted = true;
						if (contextRequest != null && contextRequest.User != null)
							entity.UpdatedBy = contextRequest.User.GuidUser;
                        entity.UpdatedDate = DateTime.UtcNow;
						UpdateAgile(entity, "IsDeleted","UpdatedBy","UpdatedDate");

						
					}
					else {
					con.Entry<Student>(entity).State = EntityState.Deleted;
					con.SaveChanges();
				
				 
					}
								
				
				 
					
					
			if (e == null || e.PreventPartialPropagate == false)
                {

                    if (_e == null)
                        _e = new BusinessRulesEventArgs<Student>() { ContextRequest = contextRequest, Item = entity, Items = null };

                    OnDeleted(this, _e);
                }

				//return null;
			}
        }
 public void UnDelete(string query, Guid[] guids, ContextRequest contextRequest)
        {
            var br = new StudentsBR(true);
            contextRequest.CustomQuery.IncludeDeleted = true;
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);

            foreach (var item in items)
            {
                item.IsDeleted = false;
						if (contextRequest != null && contextRequest.User != null)
							item.UpdatedBy = contextRequest.User.GuidUser;
                        item.UpdatedDate = DateTime.UtcNow;
            }

            UpdateBulk(items, "IsDeleted","UpdatedBy","UpdatedDate");
        }

         public void Delete(List<Student> entities,  ContextRequest contextRequest = null )
        {
				
			 BusinessRulesEventArgs<Student> _e = null;

                OnDeleting(this, _e = new BusinessRulesEventArgs<Student>() { ContextRequest = contextRequest, Item = null, Items = entities });
                if (_e != null)
                {
                    if (_e.Cancel)
                    {
                        context = null;
                        return;

                    }
                }
                bool allSucced = true;
                BusinessRulesEventArgs<Student> eToChilds = new BusinessRulesEventArgs<Student>();
                if (_e != null)
                {
                    eToChilds = _e;
                }
                else
                {
                    eToChilds = new BusinessRulesEventArgs<Student>() { ContextRequest = contextRequest, Item = (entities.Count == 1 ? entities[0] : null), Items = entities };
                }
				foreach (Student item in entities)
				{
					try
                    {
                        this.Delete(item, contextRequest, e: eToChilds);
                    }
                    catch (Exception ex)
                    {
                        SFSdotNet.Framework.My.EventLog.Error(ex);
                        allSucced = false;
                    }
				}
				if (_e == null)
                    _e = new BusinessRulesEventArgs<Student>() { ContextRequest = contextRequest, CountResult = entities.Count, Item = null, Items = entities };
                OnDeleted(this, _e);

			
        }
        #endregion
 
        #region GetCount
		 public int GetCount(Expression<Func<Student, bool>> predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

			return GetCount(predicate, contextRequest);
		}
        public int GetCount(Expression<Func<Student, bool>> predicate, ContextRequest contextRequest)
        {


		
		 using (EFContext con = new EFContext())
            {


				if (predicate == null) predicate = PredicateBuilder.True<Student>();
           		predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted == null);
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    		if (contextRequest.User !=null )
                        		if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
									predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa

								}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				
				IQueryable<Student> query = con.Students.AsQueryable();
                return query.AsExpandable().Count(predicate);

			
				}
			

        }
		  public int GetCount(string predicate,  ContextRequest contextRequest)
         {
             return GetCount(predicate, null, contextRequest);
         }

         public int GetCount(string predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return GetCount(predicate, contextRequest);
        }
		 public int GetCount(string predicate, string usemode){
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
				return GetCount( predicate,  usemode,  contextRequest);
		 }
        public int GetCount(string predicate, string usemode, ContextRequest contextRequest){

		using (EFContext con = new EFContext()) {
				string computedFields = "";
				string fkIncludes = "StudentParent,AcademyLevel,Profesor";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<Student>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetCount(con, predicate, usemode, contextRequest, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields);

			}
			#region old code
			 /* string freetext = null;
            Filter filter = new Filter();

              if (predicate.Contains("|"))
              {
                 
                  filter.SetFilterPart("ft", GetSpecificFilter(predicate, contextRequest));
                 
                  filter.ProcessText(predicate.Split(char.Parse("|"))[0]);
                  freetext = predicate.Split(char.Parse("|"))[1];

				  if (!string.IsNullOrEmpty(freetext) && string.IsNullOrEmpty(contextRequest.FreeText))
                  {
                      contextRequest.FreeText = freetext;
                  }
              }
              else {
                  filter.ProcessText(predicate);
              }
			   predicate = filter.GetFilterComplete();
			// BusinessRulesEventArgs<Student>  e = null;
           	using (EFContext con = new EFContext())
			{
			
			

			 QueryBuild(predicate, filter, con, contextRequest, "count", new List<string>());


			
			BusinessRulesEventArgs<Student> e = null;

			contextRequest.FreeText = freetext;
			contextRequest.UseMode = usemode;
            OnCounting(this, e = new BusinessRulesEventArgs<Student>() {  Filter =filter, ContextRequest = contextRequest });
            if (e != null)
            {
                if (e.Cancel)
                {
                    context = null;
                    return e.CountResult;

                }

            

            }
			
			StringBuilder sbQuerySystem = new StringBuilder();
		
					
                    filter.SetFilterPart("de","(IsDeleted != true OR IsDeleted == null)");
			
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    	if (contextRequest.User !=null )
                        	if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
                        		
								filter.SetFilterPart("co", @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa
						
						
							}
							
							}
							if (preventSecurityRestrictions) preventSecurityRestrictions= false;
		
				   
                 filter.CleanAndProcess("");
				//string predicateWithFKAndComputed = SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicate );               
				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed();
               string predicateWithManyRelations = filter.GetFilterChildren();
			   ///QueryUtils.BreakeQuery1(predicate, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
			   predicate = filter.GetFilterComplete();
               if (!string.IsNullOrEmpty(predicate))
               {
				
					
                    return con.Students.Where(predicate).Count();
					
                }else
                    return con.Students.Count();
					
			}*/
			#endregion

		}
         public int GetCount()
        {
            return GetCount(p => true);
        }
        #endregion
        
         


        public void Delete(List<Student.CompositeKey> entityKeys)
        {

            List<Student> items = new List<Student>();
            foreach (var itemKey in entityKeys)
            {
                items.Add(GetByKey(itemKey.GuidStudent));
            }

            Delete(items);

        }
		 public void UpdateAssociation(string relation, string relationValue, string query, Guid[] ids, ContextRequest contextRequest)
        {
            var items = GetBy(query, null, null, null, null, null, contextRequest, ids);
			 var module = SFSdotNet.Framework.Cache.Caching.SystemObjects.GetModuleByKey(SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(System.Web.HttpContext.Current.Request.RequestContext, "area"));
           
            foreach (var item in items)
            {
			  Guid ? guidRelationValue = null ;
                if (!string.IsNullOrEmpty(relationValue)){
                    guidRelationValue = Guid.Parse(relationValue );
                }

				 if (relation.Contains("."))
                {
                    var partsWithOtherProp = relation.Split(char.Parse("|"));
                    var parts = partsWithOtherProp[0].Split(char.Parse("."));

                    string proxyRelName = parts[0];
                    string proxyProperty = parts[1];
                    string proxyPropertyKeyNameFromOther = partsWithOtherProp[1];
                    //string proxyPropertyThis = parts[2];

                    var prop = item.GetType().GetProperty(proxyRelName);
                    //var entityInfo = //SFSdotNet.Framework.
                    // descubrir el tipo de entidad dentro de la colección
                    Type typeEntityInList = SFSdotNet.Framework.Entities.Utils.GetTypeFromList(prop);
                    var newProxyItem = Activator.CreateInstance(typeEntityInList);
                    var propThisForSet = newProxyItem.GetType().GetProperty(proxyProperty);
                    var entityInfoOfProxy = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(typeEntityInList);
                    var propOther = newProxyItem.GetType().GetProperty(proxyPropertyKeyNameFromOther);

                    if (propThisForSet != null && entityInfoOfProxy != null && propOther != null )
                    {
                        var entityInfoThis = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(item.GetType());
                        var valueThisId = item.GetType().GetProperty(entityInfoThis.PropertyKeyName).GetValue(item);
                        if (valueThisId != null)
                            propThisForSet.SetValue(newProxyItem, valueThisId);
                        propOther.SetValue(newProxyItem, Guid.Parse(relationValue));
                        
                        var entityNameProp = newProxyItem.GetType().GetField("EntityName").GetValue(null);
                        var entitySetNameProp = newProxyItem.GetType().GetField("EntitySetName").GetValue(null);

                        SFSdotNet.Framework.Apps.Integration.CreateItemFromApp(entityNameProp.ToString(), entitySetNameProp.ToString(), module.ModuleNamespace, newProxyItem, contextRequest);

                    }

                    // crear una instancia del tipo de entidad
                    // llenar los datos y registrar nuevo


                }
                else
                {
                var prop = item.GetType().GetProperty(relation);
                var entityInfo = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(prop.PropertyType);
                if (entityInfo != null)
                {
                    var ins = Activator.CreateInstance(prop.PropertyType);
                   if (guidRelationValue != null)
                    {
                        prop.PropertyType.GetProperty(entityInfo.PropertyKeyName).SetValue(ins, guidRelationValue);
                        item.GetType().GetProperty(relation).SetValue(item, ins);
                    }
                    else
                    {
                        item.GetType().GetProperty(relation).SetValue(item, null);
                    }

                    Update(item, contextRequest);
                }

				}
            }
        }
		
	}
		public partial class StudentLocationsBR:BRBase<StudentLocation>{
	 	
           
		 #region Partial methods

           partial void OnUpdating(object sender, BusinessRulesEventArgs<StudentLocation> e);

            partial void OnUpdated(object sender, BusinessRulesEventArgs<StudentLocation> e);
			partial void OnUpdatedAgile(object sender, BusinessRulesEventArgs<StudentLocation> e);

            partial void OnCreating(object sender, BusinessRulesEventArgs<StudentLocation> e);
            partial void OnCreated(object sender, BusinessRulesEventArgs<StudentLocation> e);

            partial void OnDeleting(object sender, BusinessRulesEventArgs<StudentLocation> e);
            partial void OnDeleted(object sender, BusinessRulesEventArgs<StudentLocation> e);

            partial void OnGetting(object sender, BusinessRulesEventArgs<StudentLocation> e);
            protected override void OnVirtualGetting(object sender, BusinessRulesEventArgs<StudentLocation> e)
            {
                OnGetting(sender, e);
            }
			protected override void OnVirtualCounting(object sender, BusinessRulesEventArgs<StudentLocation> e)
            {
                OnCounting(sender, e);
            }
			partial void OnTaken(object sender, BusinessRulesEventArgs<StudentLocation> e);
			protected override void OnVirtualTaken(object sender, BusinessRulesEventArgs<StudentLocation> e)
            {
                OnTaken(sender, e);
            }

            partial void OnCounting(object sender, BusinessRulesEventArgs<StudentLocation> e);
 
			partial void OnQuerySettings(object sender, BusinessRulesEventArgs<StudentLocation> e);
          
            #endregion
			
		private static StudentLocationsBR singlenton =null;
				public static StudentLocationsBR NewInstance(){
					return  new StudentLocationsBR();
					
				}
		public static StudentLocationsBR Instance{
			get{
				if (singlenton == null)
					singlenton = new StudentLocationsBR();
				return singlenton;
			}
		}
		//private bool preventSecurityRestrictions = false;
		 public bool PreventAuditTrail { get; set;  }
		#region Fields
        EFContext context = null;
        #endregion
        #region Constructor
        public StudentLocationsBR()
        {
            context = new EFContext();
        }
		 public StudentLocationsBR(bool preventSecurity)
            {
                this.preventSecurityRestrictions = preventSecurity;
				context = new EFContext();
            }
        #endregion
		
		#region Get

 		public IQueryable<StudentLocation> Get()
        {
            using (EFContext con = new EFContext())
            {
				
				var query = con.StudentLocations.AsQueryable();
                con.Configuration.ProxyCreationEnabled = false;

                //query = ContextQueryBuilder<Nutrient>.ApplyContextQuery(query, contextRequest);

                return query;




            }

        }
		


 	
		public List<StudentLocation> GetAll()
        {
            return this.GetBy(p => true);
        }
        public List<StudentLocation> GetAll(string includes)
        {
            return this.GetBy(p => true, includes);
        }
        public StudentLocation GetByKey(Guid guidStudentLocation)
        {
            return GetByKey(guidStudentLocation, true);
        }
        public StudentLocation GetByKey(Guid guidStudentLocation, bool loadIncludes)
        {
            StudentLocation item = null;
			var query = PredicateBuilder.True<StudentLocation>();
                    
			string strWhere = @"GuidStudentLocation = Guid(""" + guidStudentLocation.ToString()+@""")";
            Expression<Func<StudentLocation, bool>> predicate = null;
            //if (!string.IsNullOrEmpty(strWhere))
            //    predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<StudentLocation, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
			 ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
            contextRequest.CustomQuery.FilterExpressionString = strWhere;

			//item = GetBy(predicate, loadIncludes, contextRequest).FirstOrDefault();
			item = GetBy(strWhere,loadIncludes,contextRequest).FirstOrDefault();
            return item;
        }
         public List<StudentLocation> GetBy(string strWhere, bool loadRelations, ContextRequest contextRequest)
        {
            if (!loadRelations)
                return GetBy(strWhere, contextRequest);
            else
                return GetBy(strWhere, contextRequest, "");

        }
		  public List<StudentLocation> GetBy(string strWhere, bool loadRelations)
        {
              if (!loadRelations)
                return GetBy(strWhere, new ContextRequest());
            else
                return GetBy(strWhere, new ContextRequest(), "");

        }
		         public StudentLocation GetByKey(Guid guidStudentLocation, params Expression<Func<StudentLocation, object>>[] includes)
        {
            StudentLocation item = null;
			string strWhere = @"GuidStudentLocation = Guid(""" + guidStudentLocation.ToString()+@""")";
          Expression<Func<StudentLocation, bool>> predicate = p=> p.GuidStudentLocation == guidStudentLocation;
           // if (!string.IsNullOrEmpty(strWhere))
           //     predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<StudentLocation, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
        item = GetBy(predicate, includes).FirstOrDefault();
         ////   item = GetBy(strWhere,includes).FirstOrDefault();
			return item;

        }
        public StudentLocation GetByKey(Guid guidStudentLocation, string includes)
        {
            StudentLocation item = null;
			string strWhere = @"GuidStudentLocation = Guid(""" + guidStudentLocation.ToString()+@""")";
            
			
            item = GetBy(strWhere, includes).FirstOrDefault();
            return item;

        }
		 public StudentLocation GetByKey(Guid guidStudentLocation, string usemode, string includes)
		{
			return GetByKey(guidStudentLocation, usemode, null, includes);

		 }
		 public StudentLocation GetByKey(Guid guidStudentLocation, string usemode, ContextRequest context,  string includes)
        {
            StudentLocation item = null;
			string strWhere = @"GuidStudentLocation = Guid(""" + guidStudentLocation.ToString()+@""")";
			if (context == null){
				context = new ContextRequest();
				context.CustomQuery = new CustomQuery();
				context.CustomQuery.IsByKey = true;
				context.CustomQuery.FilterExpressionString = strWhere;
				context.UseMode = usemode;
			}
            item = GetBy(strWhere,context , includes).FirstOrDefault();
            return item;

        }

        #region Dynamic Predicate
        public List<StudentLocation> GetBy(Expression<Func<StudentLocation, bool>> predicate, int? pageSize, int? page)
        {
            return this.GetBy(predicate, pageSize, page, null, null);
        }
        public List<StudentLocation> GetBy(Expression<Func<StudentLocation, bool>> predicate, ContextRequest contextRequest)
        {

            return GetBy(predicate, contextRequest,"");
        }
        
        public List<StudentLocation> GetBy(Expression<Func<StudentLocation, bool>> predicate, ContextRequest contextRequest, params Expression<Func<StudentLocation, object>>[] includes)
        {
            StringBuilder sb = new StringBuilder();
           if (includes != null)
            {
                foreach (var path in includes)
                {

						if (sb.Length > 0) sb.Append(",");
						sb.Append(SFSdotNet.Framework.Linq.Utils.IncludeToString<StudentLocation>(path));

               }
            }
            return GetBy(predicate, contextRequest, sb.ToString());
        }
        
        
        public List<StudentLocation> GetBy(Expression<Func<StudentLocation, bool>> predicate, string includes)
        {
			ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";

            return GetBy(predicate, context, includes);
        }

        public List<StudentLocation> GetBy(Expression<Func<StudentLocation, bool>> predicate, params Expression<Func<StudentLocation, object>>[] includes)
        {
			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest context = new ContextRequest();
			            context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";
            return GetBy(predicate, context, includes);
        }

      
		public bool DisableCache { get; set; }
		public List<StudentLocation> GetBy(Expression<Func<StudentLocation, bool>> predicate, ContextRequest contextRequest, string includes)
		{
            using (EFContext con = new EFContext()) {
				
				string fkIncludes = "Student,YBLocation";
                List<string> multilangProperties = new List<string>();
				if (predicate == null) predicate = PredicateBuilder.True<StudentLocation>();
                var notDeletedExpression = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;
					Expression<Func<StudentLocation,bool>> multitenantExpression  = null;
					if (contextRequest != null && contextRequest.Company != null)	                        	
						multitenantExpression = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicate, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression);

#region Old code
/*
				List<StudentLocation> result = null;
               BusinessRulesEventArgs<StudentLocation>  e = null;
	
				OnGetting(con, e = new BusinessRulesEventArgs<StudentLocation>() {  FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null) });

               // OnGetting(con,e = new BusinessRulesEventArgs<StudentLocation>() { FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = contextRequest.CustomQuery.FilterExpressionString});
				   if (e != null) {
				    predicate = e.FilterExpression;
						if (e.Cancel)
						{
							context = null;
							 if (e.Items == null) e.Items = new List<StudentLocation>();
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                if (predicate == null) predicate = PredicateBuilder.True<StudentLocation>();
 				string fkIncludes = "Student,YBLocation";
                if(contextRequest!=null){
					if (contextRequest.CustomQuery != null)
					{
						if (contextRequest.CustomQuery.IncludeForeignKeyPaths != null) {
							if (contextRequest.CustomQuery.IncludeForeignKeyPaths.Value == false)
								fkIncludes = "";
						}
					}
				}
				if (!string.IsNullOrEmpty(includes))
					includes = includes + "," + fkIncludes;
				else
					includes = fkIncludes;
                
                //var es = _repository.Queryable;

                IQueryable<StudentLocation> query =  con.StudentLocations.AsQueryable();

                                if (!string.IsNullOrEmpty(includes))
                {
                    foreach (string include in includes.Split(char.Parse(",")))
                    {
						if (!string.IsNullOrEmpty(include))
                            query = query.Include(include);
                    }
                }
                    predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
					 	if (!preventSecurityRestrictions)
						{
							if (contextRequest != null )
		                    	if (contextRequest.User !=null )
		                        	if (contextRequest.Company != null){
		                        	
										predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
 									
									}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				query =query.AsExpandable().Where(predicate);
                query = ContextQueryBuilder<StudentLocation>.ApplyContextQuery(query, contextRequest);

                result = query.AsNoTracking().ToList<StudentLocation>();
				  
                if (e != null)
                {
                    e.Items = result;
                }
				//if (contextRequest != null ){
				//	 contextRequest = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
					contextRequest.CustomQuery = new CustomQuery();

				//}
				OnTaken(this, e == null ? e =  new BusinessRulesEventArgs<StudentLocation>() { Items= result, IncludingComputedLinq = false, ContextRequest = contextRequest,  FilterExpression = predicate } :  e);
  
			

                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
				*/
#endregion
            }
        }


		/*public int Update(List<StudentLocation> items, ContextRequest contextRequest)
            {
                int result = 0;
                using (EFContext con = new EFContext())
                {
                   
                

                    foreach (var item in items)
                    {
                        //secMessageToUser messageToUser = new secMessageToUser();
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            item.GetType().GetProperty(prop).SetValue(item, item.GetType().GetProperty(prop).GetValue(item));
                        }
                        //messageToUser.GuidMessageToUser = (Guid)item.GetType().GetProperty("GuidMessageToUser").GetValue(item);

                        var setObject = con.CreateObjectSet<StudentLocation>("StudentLocations");
                        //messageToUser.Readed = DateTime.UtcNow;
                        setObject.Attach(item);
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            con.ObjectStateManager.GetObjectStateEntry(item).SetModifiedProperty(prop);
                        }
                       
                    }
                    result = con.SaveChanges();

                    


                }
                return result;
            }
           */
		

        public List<StudentLocation> GetBy(string predicateString, ContextRequest contextRequest, string includes)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
				


				string computedFields = "";
				string fkIncludes = "Student,YBLocation";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<StudentLocation>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					//if (contextRequest != null && contextRequest.Company != null)                      	
					//	 multitenantExpression = @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))";
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicateString, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression,computedFields);


	#region Old Code
	/*
				BusinessRulesEventArgs<StudentLocation> e = null;

				Filter filter = new Filter();
                if (predicateString.Contains("|"))
                {
                    string ft = GetSpecificFilter(predicateString, contextRequest);
                    if (!string.IsNullOrEmpty(ft))
                        filter.SetFilterPart("ft", ft);
                   
                    contextRequest.FreeText = predicateString.Split(char.Parse("|"))[1];
                    var q1 = predicateString.Split(char.Parse("|"))[0];
                    if (!string.IsNullOrEmpty(q1))
                    {
                        filter.ProcessText(q1);
                    }
                }
                else {
                    filter.ProcessText(predicateString);
                }
				 var includesList = (new List<string>());
                 if (!string.IsNullOrEmpty(includes))
                 {
                     includesList = includes.Split(char.Parse(",")).ToList();
                 }

				List<StudentLocation> result = new List<StudentLocation>();
         
			QueryBuild(predicateString, filter, con, contextRequest, "getby", includesList);
			 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }
				
				
					OnGetting(con, e == null ? e = new BusinessRulesEventArgs<StudentLocation>() { Filter = filter, ContextRequest = contextRequest  } : e );

                  //OnGetting(con,e = new BusinessRulesEventArgs<StudentLocation>() {  ContextRequest = contextRequest, FilterExpressionString = predicateString });
			   	if (e != null) {
				    //predicateString = e.GetQueryString();
						if (e.Cancel)
						{
							context = null;
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				//	 else {
                //      predicateString = predicateString.Replace("*extraFreeText*", "").Replace("()","");
                //  }
				//con.EnableChangeTrackingUsingProxies = false;
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                //if (predicate == null) predicate = PredicateBuilder.True<StudentLocation>();
 				string fkIncludes = "Student,YBLocation";
                if(contextRequest!=null){
					if (contextRequest.CustomQuery != null)
					{
						if (contextRequest.CustomQuery.IncludeForeignKeyPaths != null) {
							if (contextRequest.CustomQuery.IncludeForeignKeyPaths.Value == false)
								fkIncludes = "";
						}
					}
				}else{
                    contextRequest = new ContextRequest();
                    contextRequest.CustomQuery = new CustomQuery();

                }
				if (!string.IsNullOrEmpty(includes))
					includes = includes + "," + fkIncludes;
				else
					includes = fkIncludes;
                
                //var es = _repository.Queryable;
				IQueryable<StudentLocation> query = con.StudentLocations.AsQueryable();
		
				// include relations FK
				if(string.IsNullOrEmpty(includes) ){
					includes ="";
				}
				StringBuilder sbQuerySystem = new StringBuilder();
                    //predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				

				//if (!string.IsNullOrEmpty(predicateString))
                //      sbQuerySystem.Append(" And ");
                //sbQuerySystem.Append(" (IsDeleted != true Or IsDeleted = null) ");
				 filter.SetFilterPart("de", "(IsDeleted != true OR IsDeleted = null)");


					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
	                    	if (contextRequest.User !=null )
	                        	if (contextRequest.Company != null ){
	                        		//if (sbQuerySystem.Length > 0)
	                        		//	    			sbQuerySystem.Append( " And ");	
									//sbQuerySystem.Append(@" (GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa

									filter.SetFilterPart("co",@"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))");

								}
						}	
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				//string predicateString = predicate.ToDynamicLinq<StudentLocation>();
				//predicateString += sbQuerySystem.ToString();
				filter.CleanAndProcess("");

				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed(); //SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicateString );               
                string predicateWithManyRelations = filter.GetFilterChildren(); //SFSdotNet.Framework.Linq.Utils.CleanPartExpression(predicateString);

                //QueryUtils.BreakeQuery1(predicateString, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
                var _queryable = query.AsQueryable();
				bool includeAll = true; 
                if (!string.IsNullOrEmpty(predicateWithManyRelations))
                    _queryable = _queryable.Where(predicateWithManyRelations, contextRequest.CustomQuery.ExtraParams);
				if (contextRequest.CustomQuery.SpecificProperties.Count > 0)
                {

				includeAll = false; 
                }

				StringBuilder sbSelect = new StringBuilder();
                sbSelect.Append("new (");
                bool existPrev = false;
                foreach (var selected in contextRequest.CustomQuery.SelectedFields.Where(p=> !string.IsNullOrEmpty(p.Linq)))
                {
                    if (existPrev) sbSelect.Append(", ");
                    if (!selected.Linq.Contains(".") && !selected.Linq.StartsWith("it."))
                        sbSelect.Append("it." + selected.Linq);
                    else
                        sbSelect.Append(selected.Linq);
                    existPrev = true;
                }
                sbSelect.Append(")");
                var queryable = _queryable.Select(sbSelect.ToString());                    


     				
                 if (!string.IsNullOrEmpty(predicateWithFKAndComputed))
                    queryable = queryable.Where(predicateWithFKAndComputed, contextRequest.CustomQuery.ExtraParams);

				QueryComplementOptions queryOps = ContextQueryBuilder.ApplyContextQuery(contextRequest);
            	if (!string.IsNullOrEmpty(queryOps.OrderByAndSort)){
					if (queryOps.OrderBy.Contains(".") && !queryOps.OrderBy.StartsWith("it.")) queryOps.OrderBy = "it." + queryOps.OrderBy;
					queryable = queryable.OrderBy(queryOps.OrderByAndSort);
					}
               	if (queryOps.Skip != null)
                {
                    queryable = queryable.Skip(queryOps.Skip.Value);
                }
                if (queryOps.PageSize != null)
                {
                    queryable = queryable.Take (queryOps.PageSize.Value);
                }


                var resultTemp = queryable.AsQueryable().ToListAsync().Result;
                foreach (var item in resultTemp)
                {

				   result.Add(SFSdotNet.Framework.BR.Utils.GetConverted<StudentLocation,dynamic>(item, contextRequest.CustomQuery.SelectedFields.Select(p=>p.Name).ToArray()));
                }

			 if (e != null)
                {
                    e.Items = result;
                }
				 contextRequest.CustomQuery = new CustomQuery();
				OnTaken(this, e == null ? e = new BusinessRulesEventArgs<StudentLocation>() { Items= result, IncludingComputedLinq = true, ContextRequest = contextRequest, FilterExpressionString  = predicateString } :  e);
  
			
  
                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
	
	*/
	#endregion

            }
        }
		public StudentLocation GetFromOperation(string function, string filterString, string usemode, string fields, ContextRequest contextRequest)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
                string computedFields = "";
               // string fkIncludes = "accContpaqiClassification,accProjectConcept,accProjectType,accProxyUser";
                List<string> multilangProperties = new List<string>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					if (contextRequest != null && contextRequest.Company != null)
					{
						multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
						contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
					}
					 									
					string multiTenantField = "GuidCompany";


                return GetSummaryOperation(con, new StudentLocation(), function, filterString, usemode, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields, contextRequest, fields.Split(char.Parse(",")).ToArray());
            }
        }

   protected override void QueryBuild(string predicate, Filter filter, DbContext efContext, ContextRequest contextRequest, string method, List<string> includesList)
      	{
				if (contextRequest.CustomQuery.SpecificProperties.Count == 0)
                {
					contextRequest.CustomQuery.SpecificProperties.Add(StudentLocation.PropertyNames.GuidLocation);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentLocation.PropertyNames.GuidStudent);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentLocation.PropertyNames.GuidCompany);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentLocation.PropertyNames.CreatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentLocation.PropertyNames.UpdatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentLocation.PropertyNames.CreatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentLocation.PropertyNames.UpdatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentLocation.PropertyNames.Bytes);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentLocation.PropertyNames.IsDeleted);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentLocation.PropertyNames.Student);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentLocation.PropertyNames.YBLocation);
                    
				}

				if (method == "getby" || method == "sum")
				{
					if (!contextRequest.CustomQuery.SpecificProperties.Contains("GuidStudentLocation")){
						contextRequest.CustomQuery.SpecificProperties.Add("GuidStudentLocation");
					}

					 if (!string.IsNullOrEmpty(contextRequest.CustomQuery.OrderBy))
					{
						string existPropertyOrderBy = contextRequest.CustomQuery.OrderBy;
						if (contextRequest.CustomQuery.OrderBy.Contains("."))
						{
							existPropertyOrderBy = contextRequest.CustomQuery.OrderBy.Split(char.Parse("."))[0];
						}
						if (!contextRequest.CustomQuery.SpecificProperties.Exists(p => p == existPropertyOrderBy))
						{
							contextRequest.CustomQuery.SpecificProperties.Add(existPropertyOrderBy);
						}
					}

				}
				
	bool isFullDetails = contextRequest.IsFromUI("StudentLocations", UIActions.GetForDetails);
	string filterForTest = predicate  + filter.GetFilterComplete();

				if (isFullDetails || !string.IsNullOrEmpty(predicate))
            {
            } 

			if (method == "sum")
            {
            } 
			if (contextRequest.CustomQuery.SelectedFields.Count == 0)
            {
				foreach (var selected in contextRequest.CustomQuery.SpecificProperties)
                {
					string linq = selected;
					switch (selected)
                    {

					case "Student":
					if (includesList.Contains(selected)){
                        linq = "it.Student as Student";
					}
                    else
						linq = "iif(it.Student != null, new (it.Student.GuidStudent, it.Student.FullName), null) as Student";
 					break;
					case "YBLocation":
					if (includesList.Contains(selected)){
                        linq = "it.YBLocation as YBLocation";
					}
                    else
						linq = "iif(it.YBLocation != null, new (it.YBLocation.GuidLocation, it.YBLocation.Description), null) as YBLocation";
 					break;
					 
						
					 default:
                            break;
                    }
					contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name=selected, Linq=linq});
					if (method == "getby" || method == "sum")
					{
						if (includesList.Contains(selected))
							includesList.Remove(selected);

					}

				}
			}
				if (method == "getby" || method == "sum")
				{
					foreach (var otherInclude in includesList.Where(p=> !string.IsNullOrEmpty(p)))
					{
						contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name = otherInclude, Linq = "it." + otherInclude +" as " + otherInclude });
					}
				}
				BusinessRulesEventArgs<StudentLocation> e = null;
				if (contextRequest.PreventInterceptors == false)
					OnQuerySettings(efContext, e = new BusinessRulesEventArgs<StudentLocation>() { Filter = filter, ContextRequest = contextRequest /*, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null)*/ });

				//List<StudentLocation> result = new List<StudentLocation>();
                 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }

}
		public List<StudentLocation> GetBy(Expression<Func<StudentLocation, bool>> predicate, bool loadRelations, ContextRequest contextRequest)
        {
			if(!loadRelations)
				return GetBy(predicate, contextRequest);
			else
				return GetBy(predicate, contextRequest, "");

        }

        public List<StudentLocation> GetBy(Expression<Func<StudentLocation, bool>> predicate, int? pageSize, int? page, string orderBy, SFSdotNet.Framework.Data.SortDirection? sortDirection)
        {
            return GetBy(predicate, new ContextRequest() { CustomQuery = new CustomQuery() { Page = page, PageSize = pageSize, OrderBy = orderBy, SortDirection = sortDirection } });
        }
        public List<StudentLocation> GetBy(Expression<Func<StudentLocation, bool>> predicate)
        {

			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
			contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
			            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            contextRequest.CustomQuery.FilterExpressionString = null;
            return this.GetBy(predicate, contextRequest, "");
        }
        #endregion
        #region Dynamic String
		protected override string GetSpecificFilter(string filter, ContextRequest contextRequest) {
            string result = "";
		    //string linqFilter = String.Empty;
            string freeTextFilter = String.Empty;
            if (filter.Contains("|"))
            {
               // linqFilter = filter.Split(char.Parse("|"))[0];
                freeTextFilter = filter.Split(char.Parse("|"))[1];
            }
            //else {
            //    freeTextFilter = filter;
            //}
            //else {
            //    linqFilter = filter;
            //}
			// linqFilter = SFSdotNet.Framework.Linq.Utils.ReplaceCustomDateFilters(linqFilter);
            //string specificFilter = linqFilter;
            if (!string.IsNullOrEmpty(freeTextFilter))
            {
                System.Text.StringBuilder sbCont = new System.Text.StringBuilder();
                /*if (specificFilter.Length > 0)
                {
                    sbCont.Append(" AND ");
                    sbCont.Append(" ({0})");
                }
                else
                {
                    sbCont.Append("{0}");
                }*/
                //var words = freeTextFilter.Split(char.Parse(" "));
				var word = freeTextFilter;
                System.Text.StringBuilder sbSpec = new System.Text.StringBuilder();
                 int nWords = 1;
				/*foreach (var word in words)
                {
					if (word.Length > 0){
                    if (sbSpec.Length > 0) sbSpec.Append(" AND ");
					if (words.Length > 1) sbSpec.Append("("); */
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
								
					//if (sbSpec.Length > 2)
					//	sbSpec.Append(" OR "); // test
					sbSpec.Append(string.Format(@"it.Student.FullName.Contains(""{0}"")", word)+" OR "+string.Format(@"it.YBLocation.Description.Contains(""{0}"")", word));
								 //sbSpec.Append("*extraFreeText*");

                    /*if (words.Length > 1) sbSpec.Append(")");
					
					nWords++;

					}

                }*/
                //specificFilter = string.Format("{0}{1}", specificFilter, string.Format(sbCont.ToString(), sbSpec.ToString()));
                                 result = sbSpec.ToString();  
            }
			//result = specificFilter;
			
			return result;

		}
	
			public List<StudentLocation> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params object[] extraParams)
        {
			return GetBy(filter, pageSize, page, orderBy, orderDir,  null, extraParams);
		}
           public List<StudentLocation> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, string usemode, params object[] extraParams)
            { 
                return GetBy(filter, pageSize, page, orderBy, orderDir, usemode, null, extraParams);
            }


		public List<StudentLocation> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  string usemode, ContextRequest context, params object[] extraParams)

        {

            // string freetext = null;
            //if (filter.Contains("|"))
            //{
            //    int parts = filter.Split(char.Parse("|")).Count();
            //    if (parts > 1)
            //    {

            //        freetext = filter.Split(char.Parse("|"))[1];
            //    }
            //}
		
            //string specificFilter = "";
            //if (!string.IsNullOrEmpty(filter))
            //  specificFilter=  GetSpecificFilter(filter);
            if (string.IsNullOrEmpty(orderBy))
            {
			                orderBy = "UpdatedDate";
            }
			//orderDir = "desc";
			SFSdotNet.Framework.Data.SortDirection direction = SFSdotNet.Framework.Data.SortDirection.Ascending;
            if (!string.IsNullOrEmpty(orderDir))
            {
                if (orderDir == "desc")
                    direction = SFSdotNet.Framework.Data.SortDirection.Descending;
            }
            if (context == null)
                context = new ContextRequest();
			

             context.UseMode = usemode;
             if (context.CustomQuery == null )
                context.CustomQuery =new SFSdotNet.Framework.My.CustomQuery();

 
                context.CustomQuery.ExtraParams = extraParams;

                    context.CustomQuery.OrderBy = orderBy;
                   context.CustomQuery.SortDirection = direction;
                   context.CustomQuery.Page = page;
                  context.CustomQuery.PageSize = pageSize;
               

            

            if (!preventSecurityRestrictions) {
			 if (context.CurrentContext == null)
                {
					if (SFSdotNet.Framework.My.Context.CurrentContext != null &&  SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
					{
						context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
						context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

					}
					else {
						throw new Exception("The security rule require a specific user and company");
					}
				}
            }
            return GetBy(filter, context);
  
        }


        public List<StudentLocation> GetBy(string strWhere, ContextRequest contextRequest)
        {
        	#region old code
				
				 //Expression<Func<tvsReservationTransport, bool>> predicate = null;
				string strWhereClean = strWhere.Replace("*extraFreeText*", "").Replace("()", "");
                //if (!string.IsNullOrEmpty(strWhereClean)){

                //    object[] extraParams = null;
                //    //if (contextRequest != null )
                //    //    if (contextRequest.CustomQuery != null )
                //    //        extraParams = contextRequest.CustomQuery.ExtraParams;
                //    //predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<tvsReservationTransport, bool>(strWhereClean, extraParams != null? extraParams.Cast<Guid>(): null);				
                //}
				 if (contextRequest == null)
                {
                    contextRequest = new ContextRequest();
                    if (contextRequest.CustomQuery == null)
                        contextRequest.CustomQuery = new CustomQuery();
                }
                  if (!preventSecurityRestrictions) {
					if (contextRequest.User == null || contextRequest.Company == null)
                      {
                     if (SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
                     {
                         contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                         contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

                     }
                     else {
                         throw new Exception("The security rule require a specific User and Company ");
                     }
					 }
                 }
            contextRequest.CustomQuery.FilterExpressionString = strWhere;
				//return GetBy(predicate, contextRequest);  

			#endregion				
				
                    return GetBy(strWhere, contextRequest, "");  


        }
       public List<StudentLocation> GetBy(string strWhere)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
			
            return GetBy(strWhere, context, null);
        }

        public List<StudentLocation> GetBy(string strWhere, string includes)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
            return GetBy(strWhere, context, includes);
        }

        #endregion
        #endregion
		
		  #region SaveOrUpdate
        
 		 public StudentLocation Create(StudentLocation entity)
        {
				//ObjectContext context = null;
				    if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: Create");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

				return this.Create(entity, contextRequest);


        }
        
       
        public StudentLocation Create(StudentLocation entity, ContextRequest contextRequest)
        {
		
		bool graph = false;
	
				bool preventPartial = false;
                if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
               
			using (EFContext con = new EFContext()) {

				StudentLocation itemForSave = new StudentLocation();
#region Autos
		if(!preventSecurityRestrictions){

				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion
               BusinessRulesEventArgs<StudentLocation> e = null;
			    if (preventPartial == false )
                OnCreating(this,e = new BusinessRulesEventArgs<StudentLocation>() { ContextRequest = contextRequest, Item=entity });
				   if (e != null) {
						if (e.Cancel)
						{
							context = null;
							return e.Item;

						}
					}

                    if (entity.GuidStudentLocation == Guid.Empty)
                   {
                       entity.GuidStudentLocation = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   itemForSave.GuidStudentLocation = entity.GuidStudentLocation;
				  
		
			itemForSave.GuidStudentLocation = entity.GuidStudentLocation;

			itemForSave.GuidCompany = entity.GuidCompany;

			itemForSave.CreatedDate = entity.CreatedDate;

			itemForSave.UpdatedDate = entity.UpdatedDate;

			itemForSave.CreatedBy = entity.CreatedBy;

			itemForSave.UpdatedBy = entity.UpdatedBy;

			itemForSave.Bytes = entity.Bytes;

			itemForSave.IsDeleted = entity.IsDeleted;

				
				con.StudentLocations.Add(itemForSave);



					if (entity.Student != null)
					{
						var student = new Student();
						student.GuidStudent = entity.Student.GuidStudent;
						itemForSave.Student = student;
						SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<Student>(con, itemForSave.Student);
			
					}




					if (entity.YBLocation != null)
					{
						var yBLocation = new YBLocation();
						yBLocation.GuidLocation = entity.YBLocation.GuidLocation;
						itemForSave.YBLocation = yBLocation;
						SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<YBLocation>(con, itemForSave.YBLocation);
			
					}



                
				con.ChangeTracker.Entries().Where(p => p.Entity != itemForSave && p.State != EntityState.Unchanged).ForEach(p => p.State = EntityState.Detached);

				con.Entry<StudentLocation>(itemForSave).State = EntityState.Added;

				con.SaveChanges();

					 
				

				//itemResult = entity;
                //if (e != null)
                //{
                 //   e.Item = itemResult;
                //}
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false )
                OnCreated(this, e == null ? e = new BusinessRulesEventArgs<StudentLocation>() { ContextRequest = contextRequest, Item = entity } : e);



                if (e != null && e.Item != null )
                {
                    return e.Item;
                }
                              return entity;
			}
            
        }
        //BusinessRulesEventArgs<StudentLocation> e = null;
        public void Create(List<StudentLocation> entities)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            Create(entities, contextRequest);
        }
        public void Create(List<StudentLocation> entities, ContextRequest contextRequest)
        
        {
			ObjectContext context = null;
            	foreach (StudentLocation entity in entities)
				{
					this.Create(entity, contextRequest);
				}
        }
		  public void CreateOrUpdateBulk(List<StudentLocation> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "cu", contextRequest);
        }

        private void CreateOrUpdateBulk(List<StudentLocation> entities, string actionKey, ContextRequest contextRequest)
        {
			if (entities.Count() > 0){
            bool graph = false;

            bool preventPartial = false;
            if (contextRequest != null && contextRequest.PreventInterceptors == true)
            {
                preventPartial = true;
            }
            foreach (var entity in entities)
            {
                    if (entity.GuidStudentLocation == Guid.Empty)
                   {
                       entity.GuidStudentLocation = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   
				  


#region Autos
		if(!preventSecurityRestrictions){


 if (actionKey != "u")
                        {
				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;


}
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion


		
			//entity.GuidStudentLocation = entity.GuidStudentLocation;

			//entity.GuidCompany = entity.GuidCompany;

			//entity.CreatedDate = entity.CreatedDate;

			//entity.UpdatedDate = entity.UpdatedDate;

			//entity.CreatedBy = entity.CreatedBy;

			//entity.UpdatedBy = entity.UpdatedBy;

			//entity.Bytes = entity.Bytes;

			//entity.IsDeleted = entity.IsDeleted;

				
				



				    if (entity.Student != null)
					{
						//var student = new Student();
						entity.GuidStudent = entity.Student.GuidStudent;
						//entity.Student = student;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<Student>(con, itemForSave.Student);
			
					}




				    if (entity.YBLocation != null)
					{
						//var yBLocation = new YBLocation();
						entity.GuidLocation = entity.YBLocation.GuidLocation;
						//entity.YBLocation = yBLocation;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<YBLocation>(con, itemForSave.YBLocation);
			
					}



                
				

					 
				

				//itemResult = entity;
            }
            using (EFContext con = new EFContext())
            {
                 if (actionKey == "c")
                    {
                        context.BulkInsert(entities);
                    }else if ( actionKey == "u")
                    {
                        context.BulkUpdate(entities);
                    }else
                    {
                        context.BulkInsertOrUpdate(entities);
                    }
            }

			}
        }
	
		public void CreateBulk(List<StudentLocation> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "c", contextRequest);
        }


		public void UpdateAgile(StudentLocation item, params string[] fields)
         {
			UpdateAgile(item, null, fields);
        }
		public void UpdateAgile(StudentLocation item, ContextRequest contextRequest, params string[] fields)
         {
            
             ContextRequest contextNew = null;
             if (contextRequest != null)
             {
                 contextNew = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
                 if (fields != null && fields.Length > 0)
                 {
                     contextNew.CustomQuery.SpecificProperties  = fields.ToList();
                 }
                 else if(contextRequest.CustomQuery.SpecificProperties.Count > 0)
                 {
                     fields = contextRequest.CustomQuery.SpecificProperties.ToArray();
                 }
             }
			

		   using (EFContext con = new EFContext())
            {



               
					List<string> propForCopy = new List<string>();
                    propForCopy.AddRange(fields);
                    
					  
					if (!propForCopy.Contains("GuidStudentLocation"))
						propForCopy.Add("GuidStudentLocation");

					var itemForUpdate = SFSdotNet.Framework.BR.Utils.GetConverted<StudentLocation,StudentLocation>(item, propForCopy.ToArray());
					 itemForUpdate.GuidStudentLocation = item.GuidStudentLocation;
                  var setT = con.Set<StudentLocation>().Attach(itemForUpdate);

					if (fields.Count() > 0)
					  {
						  item.ModifiedProperties = fields;
					  }
                    foreach (var property in item.ModifiedProperties)
					{						
                        if (property != "GuidStudentLocation")
                             con.Entry(setT).Property(property).IsModified = true;

                    }

                
               int result = con.SaveChanges();
               if (result != 1)
               {
                   SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: 1");
               }


            }

			OnUpdatedAgile(this, new BusinessRulesEventArgs<StudentLocation>() { Item = item, ContextRequest = contextNew  });

         }
		public void UpdateBulk(List<StudentLocation>  items, params string[] fields)
         {
             SFSdotNet.Framework.My.ContextRequest req = new SFSdotNet.Framework.My.ContextRequest();
             req.CustomQuery = new SFSdotNet.Framework.My.CustomQuery();
             foreach (var field in fields)
             {
                 req.CustomQuery.SpecificProperties.Add(field);
             }
             UpdateBulk(items, req);

         }

		 public void DeleteBulk(List<StudentLocation> entities, ContextRequest contextRequest = null)
        {

            using (EFContext con = new EFContext())
            {
                foreach (var entity in entities)
                {
					var entityProxy = new StudentLocation() { GuidStudentLocation = entity.GuidStudentLocation };

                    con.Entry<StudentLocation>(entityProxy).State = EntityState.Deleted;

                }

                int result = con.SaveChanges();
                if (result != entities.Count)
                {
                    SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: " + entities.Count.ToString());
                }
            }

        }

        public void UpdateBulk(List<StudentLocation> items, ContextRequest contextRequest)
        {
            if (items.Count() > 0){

			 foreach (var entity in items)
            {


#region Autos
		if(!preventSecurityRestrictions){

				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	



			}
#endregion




				    if (entity.Student != null)
					{
						//var student = new Student();
						entity.GuidStudent = entity.Student.GuidStudent;
						//entity.Student = student;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<Student>(con, itemForSave.Student);
			
					}




				    if (entity.YBLocation != null)
					{
						//var yBLocation = new YBLocation();
						entity.GuidLocation = entity.YBLocation.GuidLocation;
						//entity.YBLocation = yBLocation;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<YBLocation>(con, itemForSave.YBLocation);
			
					}



				}
				using (EFContext con = new EFContext())
				{

                    
                
                   con.BulkUpdate(items);

				}
             
			}	  
        }

         public StudentLocation Update(StudentLocation entity)
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return Update(entity, contextRequest);
        }
       
         public StudentLocation Update(StudentLocation entity, ContextRequest contextRequest)
        {
		 if ((System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null) && contextRequest == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Update");
            }
            if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            }

			
				StudentLocation  itemResult = null;

	
			//entity.UpdatedDate = DateTime.Now.ToUniversalTime();
			//if(contextRequest.User != null)
				//entity.UpdatedBy = contextRequest.User.GuidUser;

//	    var oldentity = GetBy(p => p.GuidStudentLocation == entity.GuidStudentLocation, contextRequest).FirstOrDefault();
	//	if (oldentity != null) {
		
          //  entity.CreatedDate = oldentity.CreatedDate;
    //        entity.CreatedBy = oldentity.CreatedBy;
	
      //      entity.GuidCompany = oldentity.GuidCompany;
	
			

	
		//}

			 using( EFContext con = new EFContext()){
				BusinessRulesEventArgs<StudentLocation> e = null;
				bool preventPartial = false; 
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false)
                OnUpdating(this,e = new BusinessRulesEventArgs<StudentLocation>() { ContextRequest = contextRequest, Item=entity});
				   if (e != null) {
						if (e.Cancel)
						{
							//outcontext = null;
							return e.Item;

						}
					}

	string includes = "Student,YBLocation";
	IQueryable < StudentLocation > query = con.StudentLocations.AsQueryable();
	foreach (string include in includes.Split(char.Parse(",")))
                       {
                           if (!string.IsNullOrEmpty(include))
                               query = query.Include(include);
                       }
	var oldentity = query.FirstOrDefault(p => p.GuidStudentLocation == entity.GuidStudentLocation);

				//if (entity.UpdatedDate == null || (contextRequest != null && contextRequest.IsFromUI("StudentLocations", UIActions.Updating)))
			oldentity.UpdatedDate = DateTime.Now.ToUniversalTime();
			if(contextRequest.User != null)
				oldentity.UpdatedBy = contextRequest.User.GuidUser;

           


						if (SFSdotNet.Framework.BR.Utils.HasRelationPropertyChanged(oldentity.Student, entity.Student, "GuidStudent"))
							oldentity.Student = entity.Student != null? new Student(){ GuidStudent = entity.Student.GuidStudent } :null;

                


						if (SFSdotNet.Framework.BR.Utils.HasRelationPropertyChanged(oldentity.YBLocation, entity.YBLocation, "GuidLocation"))
							oldentity.YBLocation = entity.YBLocation != null? new YBLocation(){ GuidLocation = entity.YBLocation.GuidLocation } :null;

                


				con.ChangeTracker.Entries().Where(p => p.Entity != oldentity).ForEach(p => p.State = EntityState.Unchanged);  
				  
				con.SaveChanges();
        
					 
					
               
				itemResult = entity;
				if(preventPartial == false)
					OnUpdated(this, e = new BusinessRulesEventArgs<StudentLocation>() { ContextRequest = contextRequest, Item=itemResult });

              	return itemResult;
			}
			  
        }
        public StudentLocation Save(StudentLocation entity)
        {
			return Create(entity);
        }
        public int Save(List<StudentLocation> entities)
        {
			 Create(entities);
            return entities.Count;

        }
        #endregion
        #region Delete
        public void Delete(StudentLocation entity)
        {
				this.Delete(entity, null);
			
        }
		 public void Delete(StudentLocation entity, ContextRequest contextRequest)
        {
				
				  List<StudentLocation> entities = new List<StudentLocation>();
				   entities.Add(entity);
				this.Delete(entities, contextRequest);
			
        }

         public void Delete(string query, Guid[] guids, ContextRequest contextRequest)
        {
			var br = new StudentLocationsBR(true);
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);
            
            Delete(items, contextRequest);

        }
        public void Delete(StudentLocation entity,  ContextRequest contextRequest, BusinessRulesEventArgs<StudentLocation> e = null)
        {
			
				using(EFContext con = new EFContext())
                 {
				
               	BusinessRulesEventArgs<StudentLocation> _e = null;
               List<StudentLocation> _items = new List<StudentLocation>();
                _items.Add(entity);
                if (e == null || e.PreventPartialPropagate == false)
                {
                    OnDeleting(this, _e = (e == null ? new BusinessRulesEventArgs<StudentLocation>() { ContextRequest = contextRequest, Item = entity, Items = null  } : e));
                }
                if (_e != null)
                {
                    if (_e.Cancel)
						{
							context = null;
							return;

						}
					}


				
									//IsDeleted
					bool logicDelete = true;
					if (entity.IsDeleted != null)
					{
						if (entity.IsDeleted.Value)
							logicDelete = false;
					}
					if (logicDelete)
					{
											//entity = GetBy(p =>, contextRequest).FirstOrDefault();
						entity.IsDeleted = true;
						if (contextRequest != null && contextRequest.User != null)
							entity.UpdatedBy = contextRequest.User.GuidUser;
                        entity.UpdatedDate = DateTime.UtcNow;
						UpdateAgile(entity, "IsDeleted","UpdatedBy","UpdatedDate");

						
					}
					else {
					con.Entry<StudentLocation>(entity).State = EntityState.Deleted;
					con.SaveChanges();
				
				 
					}
								
				
				 
					
					
			if (e == null || e.PreventPartialPropagate == false)
                {

                    if (_e == null)
                        _e = new BusinessRulesEventArgs<StudentLocation>() { ContextRequest = contextRequest, Item = entity, Items = null };

                    OnDeleted(this, _e);
                }

				//return null;
			}
        }
 public void UnDelete(string query, Guid[] guids, ContextRequest contextRequest)
        {
            var br = new StudentLocationsBR(true);
            contextRequest.CustomQuery.IncludeDeleted = true;
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);

            foreach (var item in items)
            {
                item.IsDeleted = false;
						if (contextRequest != null && contextRequest.User != null)
							item.UpdatedBy = contextRequest.User.GuidUser;
                        item.UpdatedDate = DateTime.UtcNow;
            }

            UpdateBulk(items, "IsDeleted","UpdatedBy","UpdatedDate");
        }

         public void Delete(List<StudentLocation> entities,  ContextRequest contextRequest = null )
        {
				
			 BusinessRulesEventArgs<StudentLocation> _e = null;

                OnDeleting(this, _e = new BusinessRulesEventArgs<StudentLocation>() { ContextRequest = contextRequest, Item = null, Items = entities });
                if (_e != null)
                {
                    if (_e.Cancel)
                    {
                        context = null;
                        return;

                    }
                }
                bool allSucced = true;
                BusinessRulesEventArgs<StudentLocation> eToChilds = new BusinessRulesEventArgs<StudentLocation>();
                if (_e != null)
                {
                    eToChilds = _e;
                }
                else
                {
                    eToChilds = new BusinessRulesEventArgs<StudentLocation>() { ContextRequest = contextRequest, Item = (entities.Count == 1 ? entities[0] : null), Items = entities };
                }
				foreach (StudentLocation item in entities)
				{
					try
                    {
                        this.Delete(item, contextRequest, e: eToChilds);
                    }
                    catch (Exception ex)
                    {
                        SFSdotNet.Framework.My.EventLog.Error(ex);
                        allSucced = false;
                    }
				}
				if (_e == null)
                    _e = new BusinessRulesEventArgs<StudentLocation>() { ContextRequest = contextRequest, CountResult = entities.Count, Item = null, Items = entities };
                OnDeleted(this, _e);

			
        }
        #endregion
 
        #region GetCount
		 public int GetCount(Expression<Func<StudentLocation, bool>> predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

			return GetCount(predicate, contextRequest);
		}
        public int GetCount(Expression<Func<StudentLocation, bool>> predicate, ContextRequest contextRequest)
        {


		
		 using (EFContext con = new EFContext())
            {


				if (predicate == null) predicate = PredicateBuilder.True<StudentLocation>();
           		predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted == null);
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    		if (contextRequest.User !=null )
                        		if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
									predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa

								}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				
				IQueryable<StudentLocation> query = con.StudentLocations.AsQueryable();
                return query.AsExpandable().Count(predicate);

			
				}
			

        }
		  public int GetCount(string predicate,  ContextRequest contextRequest)
         {
             return GetCount(predicate, null, contextRequest);
         }

         public int GetCount(string predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return GetCount(predicate, contextRequest);
        }
		 public int GetCount(string predicate, string usemode){
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
				return GetCount( predicate,  usemode,  contextRequest);
		 }
        public int GetCount(string predicate, string usemode, ContextRequest contextRequest){

		using (EFContext con = new EFContext()) {
				string computedFields = "";
				string fkIncludes = "Student,YBLocation";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<StudentLocation>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetCount(con, predicate, usemode, contextRequest, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields);

			}
			#region old code
			 /* string freetext = null;
            Filter filter = new Filter();

              if (predicate.Contains("|"))
              {
                 
                  filter.SetFilterPart("ft", GetSpecificFilter(predicate, contextRequest));
                 
                  filter.ProcessText(predicate.Split(char.Parse("|"))[0]);
                  freetext = predicate.Split(char.Parse("|"))[1];

				  if (!string.IsNullOrEmpty(freetext) && string.IsNullOrEmpty(contextRequest.FreeText))
                  {
                      contextRequest.FreeText = freetext;
                  }
              }
              else {
                  filter.ProcessText(predicate);
              }
			   predicate = filter.GetFilterComplete();
			// BusinessRulesEventArgs<StudentLocation>  e = null;
           	using (EFContext con = new EFContext())
			{
			
			

			 QueryBuild(predicate, filter, con, contextRequest, "count", new List<string>());


			
			BusinessRulesEventArgs<StudentLocation> e = null;

			contextRequest.FreeText = freetext;
			contextRequest.UseMode = usemode;
            OnCounting(this, e = new BusinessRulesEventArgs<StudentLocation>() {  Filter =filter, ContextRequest = contextRequest });
            if (e != null)
            {
                if (e.Cancel)
                {
                    context = null;
                    return e.CountResult;

                }

            

            }
			
			StringBuilder sbQuerySystem = new StringBuilder();
		
					
                    filter.SetFilterPart("de","(IsDeleted != true OR IsDeleted == null)");
			
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    	if (contextRequest.User !=null )
                        	if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
                        		
								filter.SetFilterPart("co", @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa
						
						
							}
							
							}
							if (preventSecurityRestrictions) preventSecurityRestrictions= false;
		
				   
                 filter.CleanAndProcess("");
				//string predicateWithFKAndComputed = SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicate );               
				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed();
               string predicateWithManyRelations = filter.GetFilterChildren();
			   ///QueryUtils.BreakeQuery1(predicate, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
			   predicate = filter.GetFilterComplete();
               if (!string.IsNullOrEmpty(predicate))
               {
				
					
                    return con.StudentLocations.Where(predicate).Count();
					
                }else
                    return con.StudentLocations.Count();
					
			}*/
			#endregion

		}
         public int GetCount()
        {
            return GetCount(p => true);
        }
        #endregion
        
         


        public void Delete(List<StudentLocation.CompositeKey> entityKeys)
        {

            List<StudentLocation> items = new List<StudentLocation>();
            foreach (var itemKey in entityKeys)
            {
                items.Add(GetByKey(itemKey.GuidStudentLocation));
            }

            Delete(items);

        }
		 public void UpdateAssociation(string relation, string relationValue, string query, Guid[] ids, ContextRequest contextRequest)
        {
            var items = GetBy(query, null, null, null, null, null, contextRequest, ids);
			 var module = SFSdotNet.Framework.Cache.Caching.SystemObjects.GetModuleByKey(SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(System.Web.HttpContext.Current.Request.RequestContext, "area"));
           
            foreach (var item in items)
            {
			  Guid ? guidRelationValue = null ;
                if (!string.IsNullOrEmpty(relationValue)){
                    guidRelationValue = Guid.Parse(relationValue );
                }

				 if (relation.Contains("."))
                {
                    var partsWithOtherProp = relation.Split(char.Parse("|"));
                    var parts = partsWithOtherProp[0].Split(char.Parse("."));

                    string proxyRelName = parts[0];
                    string proxyProperty = parts[1];
                    string proxyPropertyKeyNameFromOther = partsWithOtherProp[1];
                    //string proxyPropertyThis = parts[2];

                    var prop = item.GetType().GetProperty(proxyRelName);
                    //var entityInfo = //SFSdotNet.Framework.
                    // descubrir el tipo de entidad dentro de la colección
                    Type typeEntityInList = SFSdotNet.Framework.Entities.Utils.GetTypeFromList(prop);
                    var newProxyItem = Activator.CreateInstance(typeEntityInList);
                    var propThisForSet = newProxyItem.GetType().GetProperty(proxyProperty);
                    var entityInfoOfProxy = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(typeEntityInList);
                    var propOther = newProxyItem.GetType().GetProperty(proxyPropertyKeyNameFromOther);

                    if (propThisForSet != null && entityInfoOfProxy != null && propOther != null )
                    {
                        var entityInfoThis = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(item.GetType());
                        var valueThisId = item.GetType().GetProperty(entityInfoThis.PropertyKeyName).GetValue(item);
                        if (valueThisId != null)
                            propThisForSet.SetValue(newProxyItem, valueThisId);
                        propOther.SetValue(newProxyItem, Guid.Parse(relationValue));
                        
                        var entityNameProp = newProxyItem.GetType().GetField("EntityName").GetValue(null);
                        var entitySetNameProp = newProxyItem.GetType().GetField("EntitySetName").GetValue(null);

                        SFSdotNet.Framework.Apps.Integration.CreateItemFromApp(entityNameProp.ToString(), entitySetNameProp.ToString(), module.ModuleNamespace, newProxyItem, contextRequest);

                    }

                    // crear una instancia del tipo de entidad
                    // llenar los datos y registrar nuevo


                }
                else
                {
                var prop = item.GetType().GetProperty(relation);
                var entityInfo = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(prop.PropertyType);
                if (entityInfo != null)
                {
                    var ins = Activator.CreateInstance(prop.PropertyType);
                   if (guidRelationValue != null)
                    {
                        prop.PropertyType.GetProperty(entityInfo.PropertyKeyName).SetValue(ins, guidRelationValue);
                        item.GetType().GetProperty(relation).SetValue(item, ins);
                    }
                    else
                    {
                        item.GetType().GetProperty(relation).SetValue(item, null);
                    }

                    Update(item, contextRequest);
                }

				}
            }
        }
		
	}
		public partial class TransportsBR:BRBase<Transport>{
	 	
           
		 #region Partial methods

           partial void OnUpdating(object sender, BusinessRulesEventArgs<Transport> e);

            partial void OnUpdated(object sender, BusinessRulesEventArgs<Transport> e);
			partial void OnUpdatedAgile(object sender, BusinessRulesEventArgs<Transport> e);

            partial void OnCreating(object sender, BusinessRulesEventArgs<Transport> e);
            partial void OnCreated(object sender, BusinessRulesEventArgs<Transport> e);

            partial void OnDeleting(object sender, BusinessRulesEventArgs<Transport> e);
            partial void OnDeleted(object sender, BusinessRulesEventArgs<Transport> e);

            partial void OnGetting(object sender, BusinessRulesEventArgs<Transport> e);
            protected override void OnVirtualGetting(object sender, BusinessRulesEventArgs<Transport> e)
            {
                OnGetting(sender, e);
            }
			protected override void OnVirtualCounting(object sender, BusinessRulesEventArgs<Transport> e)
            {
                OnCounting(sender, e);
            }
			partial void OnTaken(object sender, BusinessRulesEventArgs<Transport> e);
			protected override void OnVirtualTaken(object sender, BusinessRulesEventArgs<Transport> e)
            {
                OnTaken(sender, e);
            }

            partial void OnCounting(object sender, BusinessRulesEventArgs<Transport> e);
 
			partial void OnQuerySettings(object sender, BusinessRulesEventArgs<Transport> e);
          
            #endregion
			
		private static TransportsBR singlenton =null;
				public static TransportsBR NewInstance(){
					return  new TransportsBR();
					
				}
		public static TransportsBR Instance{
			get{
				if (singlenton == null)
					singlenton = new TransportsBR();
				return singlenton;
			}
		}
		//private bool preventSecurityRestrictions = false;
		 public bool PreventAuditTrail { get; set;  }
		#region Fields
        EFContext context = null;
        #endregion
        #region Constructor
        public TransportsBR()
        {
            context = new EFContext();
        }
		 public TransportsBR(bool preventSecurity)
            {
                this.preventSecurityRestrictions = preventSecurity;
				context = new EFContext();
            }
        #endregion
		
		#region Get

 		public IQueryable<Transport> Get()
        {
            using (EFContext con = new EFContext())
            {
				
				var query = con.Transports.AsQueryable();
                con.Configuration.ProxyCreationEnabled = false;

                //query = ContextQueryBuilder<Nutrient>.ApplyContextQuery(query, contextRequest);

                return query;




            }

        }
		


 	
		public List<Transport> GetAll()
        {
            return this.GetBy(p => true);
        }
        public List<Transport> GetAll(string includes)
        {
            return this.GetBy(p => true, includes);
        }
        public Transport GetByKey(Guid guidTrasport)
        {
            return GetByKey(guidTrasport, true);
        }
        public Transport GetByKey(Guid guidTrasport, bool loadIncludes)
        {
            Transport item = null;
			var query = PredicateBuilder.True<Transport>();
                    
			string strWhere = @"GuidTrasport = Guid(""" + guidTrasport.ToString()+@""")";
            Expression<Func<Transport, bool>> predicate = null;
            //if (!string.IsNullOrEmpty(strWhere))
            //    predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<Transport, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
			 ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
            contextRequest.CustomQuery.FilterExpressionString = strWhere;

			//item = GetBy(predicate, loadIncludes, contextRequest).FirstOrDefault();
			item = GetBy(strWhere,loadIncludes,contextRequest).FirstOrDefault();
            return item;
        }
         public List<Transport> GetBy(string strWhere, bool loadRelations, ContextRequest contextRequest)
        {
            if (!loadRelations)
                return GetBy(strWhere, contextRequest);
            else
                return GetBy(strWhere, contextRequest, "");

        }
		  public List<Transport> GetBy(string strWhere, bool loadRelations)
        {
              if (!loadRelations)
                return GetBy(strWhere, new ContextRequest());
            else
                return GetBy(strWhere, new ContextRequest(), "");

        }
		         public Transport GetByKey(Guid guidTrasport, params Expression<Func<Transport, object>>[] includes)
        {
            Transport item = null;
			string strWhere = @"GuidTrasport = Guid(""" + guidTrasport.ToString()+@""")";
          Expression<Func<Transport, bool>> predicate = p=> p.GuidTrasport == guidTrasport;
           // if (!string.IsNullOrEmpty(strWhere))
           //     predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<Transport, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
        item = GetBy(predicate, includes).FirstOrDefault();
         ////   item = GetBy(strWhere,includes).FirstOrDefault();
			return item;

        }
        public Transport GetByKey(Guid guidTrasport, string includes)
        {
            Transport item = null;
			string strWhere = @"GuidTrasport = Guid(""" + guidTrasport.ToString()+@""")";
            
			
            item = GetBy(strWhere, includes).FirstOrDefault();
            return item;

        }
		 public Transport GetByKey(Guid guidTrasport, string usemode, string includes)
		{
			return GetByKey(guidTrasport, usemode, null, includes);

		 }
		 public Transport GetByKey(Guid guidTrasport, string usemode, ContextRequest context,  string includes)
        {
            Transport item = null;
			string strWhere = @"GuidTrasport = Guid(""" + guidTrasport.ToString()+@""")";
			if (context == null){
				context = new ContextRequest();
				context.CustomQuery = new CustomQuery();
				context.CustomQuery.IsByKey = true;
				context.CustomQuery.FilterExpressionString = strWhere;
				context.UseMode = usemode;
			}
            item = GetBy(strWhere,context , includes).FirstOrDefault();
            return item;

        }

        #region Dynamic Predicate
        public List<Transport> GetBy(Expression<Func<Transport, bool>> predicate, int? pageSize, int? page)
        {
            return this.GetBy(predicate, pageSize, page, null, null);
        }
        public List<Transport> GetBy(Expression<Func<Transport, bool>> predicate, ContextRequest contextRequest)
        {

            return GetBy(predicate, contextRequest,"");
        }
        
        public List<Transport> GetBy(Expression<Func<Transport, bool>> predicate, ContextRequest contextRequest, params Expression<Func<Transport, object>>[] includes)
        {
            StringBuilder sb = new StringBuilder();
           if (includes != null)
            {
                foreach (var path in includes)
                {

						if (sb.Length > 0) sb.Append(",");
						sb.Append(SFSdotNet.Framework.Linq.Utils.IncludeToString<Transport>(path));

               }
            }
            return GetBy(predicate, contextRequest, sb.ToString());
        }
        
        
        public List<Transport> GetBy(Expression<Func<Transport, bool>> predicate, string includes)
        {
			ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";

            return GetBy(predicate, context, includes);
        }

        public List<Transport> GetBy(Expression<Func<Transport, bool>> predicate, params Expression<Func<Transport, object>>[] includes)
        {
			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest context = new ContextRequest();
			            context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";
            return GetBy(predicate, context, includes);
        }

      
		public bool DisableCache { get; set; }
		public List<Transport> GetBy(Expression<Func<Transport, bool>> predicate, ContextRequest contextRequest, string includes)
		{
            using (EFContext con = new EFContext()) {
				
				string fkIncludes = "YBRoute";
                List<string> multilangProperties = new List<string>();
				if (predicate == null) predicate = PredicateBuilder.True<Transport>();
                var notDeletedExpression = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;
					Expression<Func<Transport,bool>> multitenantExpression  = null;
					if (contextRequest != null && contextRequest.Company != null)	                        	
						multitenantExpression = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicate, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression);

#region Old code
/*
				List<Transport> result = null;
               BusinessRulesEventArgs<Transport>  e = null;
	
				OnGetting(con, e = new BusinessRulesEventArgs<Transport>() {  FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null) });

               // OnGetting(con,e = new BusinessRulesEventArgs<Transport>() { FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = contextRequest.CustomQuery.FilterExpressionString});
				   if (e != null) {
				    predicate = e.FilterExpression;
						if (e.Cancel)
						{
							context = null;
							 if (e.Items == null) e.Items = new List<Transport>();
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                if (predicate == null) predicate = PredicateBuilder.True<Transport>();
 				string fkIncludes = "YBRoute";
                if(contextRequest!=null){
					if (contextRequest.CustomQuery != null)
					{
						if (contextRequest.CustomQuery.IncludeForeignKeyPaths != null) {
							if (contextRequest.CustomQuery.IncludeForeignKeyPaths.Value == false)
								fkIncludes = "";
						}
					}
				}
				if (!string.IsNullOrEmpty(includes))
					includes = includes + "," + fkIncludes;
				else
					includes = fkIncludes;
                
                //var es = _repository.Queryable;

                IQueryable<Transport> query =  con.Transports.AsQueryable();

                                if (!string.IsNullOrEmpty(includes))
                {
                    foreach (string include in includes.Split(char.Parse(",")))
                    {
						if (!string.IsNullOrEmpty(include))
                            query = query.Include(include);
                    }
                }
                    predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
					 	if (!preventSecurityRestrictions)
						{
							if (contextRequest != null )
		                    	if (contextRequest.User !=null )
		                        	if (contextRequest.Company != null){
		                        	
										predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
 									
									}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				query =query.AsExpandable().Where(predicate);
                query = ContextQueryBuilder<Transport>.ApplyContextQuery(query, contextRequest);

                result = query.AsNoTracking().ToList<Transport>();
				  
                if (e != null)
                {
                    e.Items = result;
                }
				//if (contextRequest != null ){
				//	 contextRequest = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
					contextRequest.CustomQuery = new CustomQuery();

				//}
				OnTaken(this, e == null ? e =  new BusinessRulesEventArgs<Transport>() { Items= result, IncludingComputedLinq = false, ContextRequest = contextRequest,  FilterExpression = predicate } :  e);
  
			

                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
				*/
#endregion
            }
        }


		/*public int Update(List<Transport> items, ContextRequest contextRequest)
            {
                int result = 0;
                using (EFContext con = new EFContext())
                {
                   
                

                    foreach (var item in items)
                    {
                        //secMessageToUser messageToUser = new secMessageToUser();
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            item.GetType().GetProperty(prop).SetValue(item, item.GetType().GetProperty(prop).GetValue(item));
                        }
                        //messageToUser.GuidMessageToUser = (Guid)item.GetType().GetProperty("GuidMessageToUser").GetValue(item);

                        var setObject = con.CreateObjectSet<Transport>("Transports");
                        //messageToUser.Readed = DateTime.UtcNow;
                        setObject.Attach(item);
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            con.ObjectStateManager.GetObjectStateEntry(item).SetModifiedProperty(prop);
                        }
                       
                    }
                    result = con.SaveChanges();

                    


                }
                return result;
            }
           */
		

        public List<Transport> GetBy(string predicateString, ContextRequest contextRequest, string includes)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
				


				string computedFields = "";
				string fkIncludes = "YBRoute";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<Transport>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					//if (contextRequest != null && contextRequest.Company != null)                      	
					//	 multitenantExpression = @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))";
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicateString, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression,computedFields);


	#region Old Code
	/*
				BusinessRulesEventArgs<Transport> e = null;

				Filter filter = new Filter();
                if (predicateString.Contains("|"))
                {
                    string ft = GetSpecificFilter(predicateString, contextRequest);
                    if (!string.IsNullOrEmpty(ft))
                        filter.SetFilterPart("ft", ft);
                   
                    contextRequest.FreeText = predicateString.Split(char.Parse("|"))[1];
                    var q1 = predicateString.Split(char.Parse("|"))[0];
                    if (!string.IsNullOrEmpty(q1))
                    {
                        filter.ProcessText(q1);
                    }
                }
                else {
                    filter.ProcessText(predicateString);
                }
				 var includesList = (new List<string>());
                 if (!string.IsNullOrEmpty(includes))
                 {
                     includesList = includes.Split(char.Parse(",")).ToList();
                 }

				List<Transport> result = new List<Transport>();
         
			QueryBuild(predicateString, filter, con, contextRequest, "getby", includesList);
			 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }
				
				
					OnGetting(con, e == null ? e = new BusinessRulesEventArgs<Transport>() { Filter = filter, ContextRequest = contextRequest  } : e );

                  //OnGetting(con,e = new BusinessRulesEventArgs<Transport>() {  ContextRequest = contextRequest, FilterExpressionString = predicateString });
			   	if (e != null) {
				    //predicateString = e.GetQueryString();
						if (e.Cancel)
						{
							context = null;
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				//	 else {
                //      predicateString = predicateString.Replace("*extraFreeText*", "").Replace("()","");
                //  }
				//con.EnableChangeTrackingUsingProxies = false;
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                //if (predicate == null) predicate = PredicateBuilder.True<Transport>();
 				string fkIncludes = "YBRoute";
                if(contextRequest!=null){
					if (contextRequest.CustomQuery != null)
					{
						if (contextRequest.CustomQuery.IncludeForeignKeyPaths != null) {
							if (contextRequest.CustomQuery.IncludeForeignKeyPaths.Value == false)
								fkIncludes = "";
						}
					}
				}else{
                    contextRequest = new ContextRequest();
                    contextRequest.CustomQuery = new CustomQuery();

                }
				if (!string.IsNullOrEmpty(includes))
					includes = includes + "," + fkIncludes;
				else
					includes = fkIncludes;
                
                //var es = _repository.Queryable;
				IQueryable<Transport> query = con.Transports.AsQueryable();
		
				// include relations FK
				if(string.IsNullOrEmpty(includes) ){
					includes ="";
				}
				StringBuilder sbQuerySystem = new StringBuilder();
                    //predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				

				//if (!string.IsNullOrEmpty(predicateString))
                //      sbQuerySystem.Append(" And ");
                //sbQuerySystem.Append(" (IsDeleted != true Or IsDeleted = null) ");
				 filter.SetFilterPart("de", "(IsDeleted != true OR IsDeleted = null)");


					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
	                    	if (contextRequest.User !=null )
	                        	if (contextRequest.Company != null ){
	                        		//if (sbQuerySystem.Length > 0)
	                        		//	    			sbQuerySystem.Append( " And ");	
									//sbQuerySystem.Append(@" (GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa

									filter.SetFilterPart("co",@"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))");

								}
						}	
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				//string predicateString = predicate.ToDynamicLinq<Transport>();
				//predicateString += sbQuerySystem.ToString();
				filter.CleanAndProcess("");

				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed(); //SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicateString );               
                string predicateWithManyRelations = filter.GetFilterChildren(); //SFSdotNet.Framework.Linq.Utils.CleanPartExpression(predicateString);

                //QueryUtils.BreakeQuery1(predicateString, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
                var _queryable = query.AsQueryable();
				bool includeAll = true; 
                if (!string.IsNullOrEmpty(predicateWithManyRelations))
                    _queryable = _queryable.Where(predicateWithManyRelations, contextRequest.CustomQuery.ExtraParams);
				if (contextRequest.CustomQuery.SpecificProperties.Count > 0)
                {

				includeAll = false; 
                }

				StringBuilder sbSelect = new StringBuilder();
                sbSelect.Append("new (");
                bool existPrev = false;
                foreach (var selected in contextRequest.CustomQuery.SelectedFields.Where(p=> !string.IsNullOrEmpty(p.Linq)))
                {
                    if (existPrev) sbSelect.Append(", ");
                    if (!selected.Linq.Contains(".") && !selected.Linq.StartsWith("it."))
                        sbSelect.Append("it." + selected.Linq);
                    else
                        sbSelect.Append(selected.Linq);
                    existPrev = true;
                }
                sbSelect.Append(")");
                var queryable = _queryable.Select(sbSelect.ToString());                    


     				
                 if (!string.IsNullOrEmpty(predicateWithFKAndComputed))
                    queryable = queryable.Where(predicateWithFKAndComputed, contextRequest.CustomQuery.ExtraParams);

				QueryComplementOptions queryOps = ContextQueryBuilder.ApplyContextQuery(contextRequest);
            	if (!string.IsNullOrEmpty(queryOps.OrderByAndSort)){
					if (queryOps.OrderBy.Contains(".") && !queryOps.OrderBy.StartsWith("it.")) queryOps.OrderBy = "it." + queryOps.OrderBy;
					queryable = queryable.OrderBy(queryOps.OrderByAndSort);
					}
               	if (queryOps.Skip != null)
                {
                    queryable = queryable.Skip(queryOps.Skip.Value);
                }
                if (queryOps.PageSize != null)
                {
                    queryable = queryable.Take (queryOps.PageSize.Value);
                }


                var resultTemp = queryable.AsQueryable().ToListAsync().Result;
                foreach (var item in resultTemp)
                {

				   result.Add(SFSdotNet.Framework.BR.Utils.GetConverted<Transport,dynamic>(item, contextRequest.CustomQuery.SelectedFields.Select(p=>p.Name).ToArray()));
                }

			 if (e != null)
                {
                    e.Items = result;
                }
				 contextRequest.CustomQuery = new CustomQuery();
				OnTaken(this, e == null ? e = new BusinessRulesEventArgs<Transport>() { Items= result, IncludingComputedLinq = true, ContextRequest = contextRequest, FilterExpressionString  = predicateString } :  e);
  
			
  
                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
	
	*/
	#endregion

            }
        }
		public Transport GetFromOperation(string function, string filterString, string usemode, string fields, ContextRequest contextRequest)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
                string computedFields = "";
               // string fkIncludes = "accContpaqiClassification,accProjectConcept,accProjectType,accProxyUser";
                List<string> multilangProperties = new List<string>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					if (contextRequest != null && contextRequest.Company != null)
					{
						multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
						contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
					}
					 									
					string multiTenantField = "GuidCompany";


                return GetSummaryOperation(con, new Transport(), function, filterString, usemode, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields, contextRequest, fields.Split(char.Parse(",")).ToArray());
            }
        }

   protected override void QueryBuild(string predicate, Filter filter, DbContext efContext, ContextRequest contextRequest, string method, List<string> includesList)
      	{
				if (contextRequest.CustomQuery.SpecificProperties.Count == 0)
                {
					contextRequest.CustomQuery.SpecificProperties.Add(Transport.PropertyNames.TransportCode);
					contextRequest.CustomQuery.SpecificProperties.Add(Transport.PropertyNames.Description);
					contextRequest.CustomQuery.SpecificProperties.Add(Transport.PropertyNames.Capacity);
					contextRequest.CustomQuery.SpecificProperties.Add(Transport.PropertyNames.GuidRoute);
					contextRequest.CustomQuery.SpecificProperties.Add(Transport.PropertyNames.GuidCompany);
					contextRequest.CustomQuery.SpecificProperties.Add(Transport.PropertyNames.CreatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(Transport.PropertyNames.UpdatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(Transport.PropertyNames.CreatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(Transport.PropertyNames.UpdatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(Transport.PropertyNames.Bytes);
					contextRequest.CustomQuery.SpecificProperties.Add(Transport.PropertyNames.IsDeleted);
					contextRequest.CustomQuery.SpecificProperties.Add(Transport.PropertyNames.YBRoute);
                    
				}

				if (method == "getby" || method == "sum")
				{
					if (!contextRequest.CustomQuery.SpecificProperties.Contains("GuidTrasport")){
						contextRequest.CustomQuery.SpecificProperties.Add("GuidTrasport");
					}

					 if (!string.IsNullOrEmpty(contextRequest.CustomQuery.OrderBy))
					{
						string existPropertyOrderBy = contextRequest.CustomQuery.OrderBy;
						if (contextRequest.CustomQuery.OrderBy.Contains("."))
						{
							existPropertyOrderBy = contextRequest.CustomQuery.OrderBy.Split(char.Parse("."))[0];
						}
						if (!contextRequest.CustomQuery.SpecificProperties.Exists(p => p == existPropertyOrderBy))
						{
							contextRequest.CustomQuery.SpecificProperties.Add(existPropertyOrderBy);
						}
					}

				}
				
	bool isFullDetails = contextRequest.IsFromUI("Transports", UIActions.GetForDetails);
	string filterForTest = predicate  + filter.GetFilterComplete();

				if (isFullDetails || !string.IsNullOrEmpty(predicate))
            {
            } 

			if (method == "sum")
            {
            } 
			if (contextRequest.CustomQuery.SelectedFields.Count == 0)
            {
				foreach (var selected in contextRequest.CustomQuery.SpecificProperties)
                {
					string linq = selected;
					switch (selected)
                    {

					case "YBRoute":
					if (includesList.Contains(selected)){
                        linq = "it.YBRoute as YBRoute";
					}
                    else
						linq = "iif(it.YBRoute != null, new (it.YBRoute.GuidRoute, it.YBRoute.Title), null) as YBRoute";
 					break;
					 
						
					 default:
                            break;
                    }
					contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name=selected, Linq=linq});
					if (method == "getby" || method == "sum")
					{
						if (includesList.Contains(selected))
							includesList.Remove(selected);

					}

				}
			}
				if (method == "getby" || method == "sum")
				{
					foreach (var otherInclude in includesList.Where(p=> !string.IsNullOrEmpty(p)))
					{
						contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name = otherInclude, Linq = "it." + otherInclude +" as " + otherInclude });
					}
				}
				BusinessRulesEventArgs<Transport> e = null;
				if (contextRequest.PreventInterceptors == false)
					OnQuerySettings(efContext, e = new BusinessRulesEventArgs<Transport>() { Filter = filter, ContextRequest = contextRequest /*, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null)*/ });

				//List<Transport> result = new List<Transport>();
                 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }

}
		public List<Transport> GetBy(Expression<Func<Transport, bool>> predicate, bool loadRelations, ContextRequest contextRequest)
        {
			if(!loadRelations)
				return GetBy(predicate, contextRequest);
			else
				return GetBy(predicate, contextRequest, "");

        }

        public List<Transport> GetBy(Expression<Func<Transport, bool>> predicate, int? pageSize, int? page, string orderBy, SFSdotNet.Framework.Data.SortDirection? sortDirection)
        {
            return GetBy(predicate, new ContextRequest() { CustomQuery = new CustomQuery() { Page = page, PageSize = pageSize, OrderBy = orderBy, SortDirection = sortDirection } });
        }
        public List<Transport> GetBy(Expression<Func<Transport, bool>> predicate)
        {

			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
			contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
			            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            contextRequest.CustomQuery.FilterExpressionString = null;
            return this.GetBy(predicate, contextRequest, "");
        }
        #endregion
        #region Dynamic String
		protected override string GetSpecificFilter(string filter, ContextRequest contextRequest) {
            string result = "";
		    //string linqFilter = String.Empty;
            string freeTextFilter = String.Empty;
            if (filter.Contains("|"))
            {
               // linqFilter = filter.Split(char.Parse("|"))[0];
                freeTextFilter = filter.Split(char.Parse("|"))[1];
            }
            //else {
            //    freeTextFilter = filter;
            //}
            //else {
            //    linqFilter = filter;
            //}
			// linqFilter = SFSdotNet.Framework.Linq.Utils.ReplaceCustomDateFilters(linqFilter);
            //string specificFilter = linqFilter;
            if (!string.IsNullOrEmpty(freeTextFilter))
            {
                System.Text.StringBuilder sbCont = new System.Text.StringBuilder();
                /*if (specificFilter.Length > 0)
                {
                    sbCont.Append(" AND ");
                    sbCont.Append(" ({0})");
                }
                else
                {
                    sbCont.Append("{0}");
                }*/
                //var words = freeTextFilter.Split(char.Parse(" "));
				var word = freeTextFilter;
                System.Text.StringBuilder sbSpec = new System.Text.StringBuilder();
                 int nWords = 1;
				/*foreach (var word in words)
                {
					if (word.Length > 0){
                    if (sbSpec.Length > 0) sbSpec.Append(" AND ");
					if (words.Length > 1) sbSpec.Append("("); */
					
	
					
					
					
									
					sbSpec.Append(string.Format(@"TransportCode.Contains(""{0}"")", word));
					

					
					
										sbSpec.Append(" OR ");
					
									
					sbSpec.Append(string.Format(@"Description.Contains(""{0}"")", word));
					

					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
								sbSpec.Append(" OR ");
					
					//if (sbSpec.Length > 2)
					//	sbSpec.Append(" OR "); // test
					sbSpec.Append(string.Format(@"it.YBRoute.Title.Contains(""{0}"")", word));
								 //sbSpec.Append("*extraFreeText*");

                    /*if (words.Length > 1) sbSpec.Append(")");
					
					nWords++;

					}

                }*/
                //specificFilter = string.Format("{0}{1}", specificFilter, string.Format(sbCont.ToString(), sbSpec.ToString()));
                                 result = sbSpec.ToString();  
            }
			//result = specificFilter;
			
			return result;

		}
	
			public List<Transport> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params object[] extraParams)
        {
			return GetBy(filter, pageSize, page, orderBy, orderDir,  null, extraParams);
		}
           public List<Transport> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, string usemode, params object[] extraParams)
            { 
                return GetBy(filter, pageSize, page, orderBy, orderDir, usemode, null, extraParams);
            }


		public List<Transport> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  string usemode, ContextRequest context, params object[] extraParams)

        {

            // string freetext = null;
            //if (filter.Contains("|"))
            //{
            //    int parts = filter.Split(char.Parse("|")).Count();
            //    if (parts > 1)
            //    {

            //        freetext = filter.Split(char.Parse("|"))[1];
            //    }
            //}
		
            //string specificFilter = "";
            //if (!string.IsNullOrEmpty(filter))
            //  specificFilter=  GetSpecificFilter(filter);
            if (string.IsNullOrEmpty(orderBy))
            {
			                orderBy = "UpdatedDate";
            }
			//orderDir = "desc";
			SFSdotNet.Framework.Data.SortDirection direction = SFSdotNet.Framework.Data.SortDirection.Ascending;
            if (!string.IsNullOrEmpty(orderDir))
            {
                if (orderDir == "desc")
                    direction = SFSdotNet.Framework.Data.SortDirection.Descending;
            }
            if (context == null)
                context = new ContextRequest();
			

             context.UseMode = usemode;
             if (context.CustomQuery == null )
                context.CustomQuery =new SFSdotNet.Framework.My.CustomQuery();

 
                context.CustomQuery.ExtraParams = extraParams;

                    context.CustomQuery.OrderBy = orderBy;
                   context.CustomQuery.SortDirection = direction;
                   context.CustomQuery.Page = page;
                  context.CustomQuery.PageSize = pageSize;
               

            

            if (!preventSecurityRestrictions) {
			 if (context.CurrentContext == null)
                {
					if (SFSdotNet.Framework.My.Context.CurrentContext != null &&  SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
					{
						context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
						context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

					}
					else {
						throw new Exception("The security rule require a specific user and company");
					}
				}
            }
            return GetBy(filter, context);
  
        }


        public List<Transport> GetBy(string strWhere, ContextRequest contextRequest)
        {
        	#region old code
				
				 //Expression<Func<tvsReservationTransport, bool>> predicate = null;
				string strWhereClean = strWhere.Replace("*extraFreeText*", "").Replace("()", "");
                //if (!string.IsNullOrEmpty(strWhereClean)){

                //    object[] extraParams = null;
                //    //if (contextRequest != null )
                //    //    if (contextRequest.CustomQuery != null )
                //    //        extraParams = contextRequest.CustomQuery.ExtraParams;
                //    //predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<tvsReservationTransport, bool>(strWhereClean, extraParams != null? extraParams.Cast<Guid>(): null);				
                //}
				 if (contextRequest == null)
                {
                    contextRequest = new ContextRequest();
                    if (contextRequest.CustomQuery == null)
                        contextRequest.CustomQuery = new CustomQuery();
                }
                  if (!preventSecurityRestrictions) {
					if (contextRequest.User == null || contextRequest.Company == null)
                      {
                     if (SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
                     {
                         contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                         contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

                     }
                     else {
                         throw new Exception("The security rule require a specific User and Company ");
                     }
					 }
                 }
            contextRequest.CustomQuery.FilterExpressionString = strWhere;
				//return GetBy(predicate, contextRequest);  

			#endregion				
				
                    return GetBy(strWhere, contextRequest, "");  


        }
       public List<Transport> GetBy(string strWhere)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
			
            return GetBy(strWhere, context, null);
        }

        public List<Transport> GetBy(string strWhere, string includes)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
            return GetBy(strWhere, context, includes);
        }

        #endregion
        #endregion
		
		  #region SaveOrUpdate
        
 		 public Transport Create(Transport entity)
        {
				//ObjectContext context = null;
				    if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: Create");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

				return this.Create(entity, contextRequest);


        }
        
       
        public Transport Create(Transport entity, ContextRequest contextRequest)
        {
		
		bool graph = false;
	
				bool preventPartial = false;
                if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
               
			using (EFContext con = new EFContext()) {

				Transport itemForSave = new Transport();
#region Autos
		if(!preventSecurityRestrictions){

				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion
               BusinessRulesEventArgs<Transport> e = null;
			    if (preventPartial == false )
                OnCreating(this,e = new BusinessRulesEventArgs<Transport>() { ContextRequest = contextRequest, Item=entity });
				   if (e != null) {
						if (e.Cancel)
						{
							context = null;
							return e.Item;

						}
					}

                    if (entity.GuidTrasport == Guid.Empty)
                   {
                       entity.GuidTrasport = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   itemForSave.GuidTrasport = entity.GuidTrasport;
				  
		
			itemForSave.GuidTrasport = entity.GuidTrasport;

			itemForSave.TransportCode = entity.TransportCode;

			itemForSave.Description = entity.Description;

			itemForSave.Capacity = entity.Capacity;

			itemForSave.GuidCompany = entity.GuidCompany;

			itemForSave.CreatedDate = entity.CreatedDate;

			itemForSave.UpdatedDate = entity.UpdatedDate;

			itemForSave.CreatedBy = entity.CreatedBy;

			itemForSave.UpdatedBy = entity.UpdatedBy;

			itemForSave.Bytes = entity.Bytes;

			itemForSave.IsDeleted = entity.IsDeleted;

				
				con.Transports.Add(itemForSave);



					if (entity.YBRoute != null)
					{
						var yBRoute = new YBRoute();
						yBRoute.GuidRoute = entity.YBRoute.GuidRoute;
						itemForSave.YBRoute = yBRoute;
						SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<YBRoute>(con, itemForSave.YBRoute);
			
					}



                
				con.ChangeTracker.Entries().Where(p => p.Entity != itemForSave && p.State != EntityState.Unchanged).ForEach(p => p.State = EntityState.Detached);

				con.Entry<Transport>(itemForSave).State = EntityState.Added;

				con.SaveChanges();

					 
				

				//itemResult = entity;
                //if (e != null)
                //{
                 //   e.Item = itemResult;
                //}
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false )
                OnCreated(this, e == null ? e = new BusinessRulesEventArgs<Transport>() { ContextRequest = contextRequest, Item = entity } : e);



                if (e != null && e.Item != null )
                {
                    return e.Item;
                }
                              return entity;
			}
            
        }
        //BusinessRulesEventArgs<Transport> e = null;
        public void Create(List<Transport> entities)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            Create(entities, contextRequest);
        }
        public void Create(List<Transport> entities, ContextRequest contextRequest)
        
        {
			ObjectContext context = null;
            	foreach (Transport entity in entities)
				{
					this.Create(entity, contextRequest);
				}
        }
		  public void CreateOrUpdateBulk(List<Transport> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "cu", contextRequest);
        }

        private void CreateOrUpdateBulk(List<Transport> entities, string actionKey, ContextRequest contextRequest)
        {
			if (entities.Count() > 0){
            bool graph = false;

            bool preventPartial = false;
            if (contextRequest != null && contextRequest.PreventInterceptors == true)
            {
                preventPartial = true;
            }
            foreach (var entity in entities)
            {
                    if (entity.GuidTrasport == Guid.Empty)
                   {
                       entity.GuidTrasport = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   
				  


#region Autos
		if(!preventSecurityRestrictions){


 if (actionKey != "u")
                        {
				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;


}
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion


		
			//entity.GuidTrasport = entity.GuidTrasport;

			//entity.TransportCode = entity.TransportCode;

			//entity.Description = entity.Description;

			//entity.Capacity = entity.Capacity;

			//entity.GuidCompany = entity.GuidCompany;

			//entity.CreatedDate = entity.CreatedDate;

			//entity.UpdatedDate = entity.UpdatedDate;

			//entity.CreatedBy = entity.CreatedBy;

			//entity.UpdatedBy = entity.UpdatedBy;

			//entity.Bytes = entity.Bytes;

			//entity.IsDeleted = entity.IsDeleted;

				
				



				    if (entity.YBRoute != null)
					{
						//var yBRoute = new YBRoute();
						entity.GuidRoute = entity.YBRoute.GuidRoute;
						//entity.YBRoute = yBRoute;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<YBRoute>(con, itemForSave.YBRoute);
			
					}



                
				

					 
				

				//itemResult = entity;
            }
            using (EFContext con = new EFContext())
            {
                 if (actionKey == "c")
                    {
                        context.BulkInsert(entities);
                    }else if ( actionKey == "u")
                    {
                        context.BulkUpdate(entities);
                    }else
                    {
                        context.BulkInsertOrUpdate(entities);
                    }
            }

			}
        }
	
		public void CreateBulk(List<Transport> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "c", contextRequest);
        }


		public void UpdateAgile(Transport item, params string[] fields)
         {
			UpdateAgile(item, null, fields);
        }
		public void UpdateAgile(Transport item, ContextRequest contextRequest, params string[] fields)
         {
            
             ContextRequest contextNew = null;
             if (contextRequest != null)
             {
                 contextNew = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
                 if (fields != null && fields.Length > 0)
                 {
                     contextNew.CustomQuery.SpecificProperties  = fields.ToList();
                 }
                 else if(contextRequest.CustomQuery.SpecificProperties.Count > 0)
                 {
                     fields = contextRequest.CustomQuery.SpecificProperties.ToArray();
                 }
             }
			

		   using (EFContext con = new EFContext())
            {



               
					List<string> propForCopy = new List<string>();
                    propForCopy.AddRange(fields);
                    
					  
					if (!propForCopy.Contains("GuidTrasport"))
						propForCopy.Add("GuidTrasport");

					var itemForUpdate = SFSdotNet.Framework.BR.Utils.GetConverted<Transport,Transport>(item, propForCopy.ToArray());
					 itemForUpdate.GuidTrasport = item.GuidTrasport;
                  var setT = con.Set<Transport>().Attach(itemForUpdate);

					if (fields.Count() > 0)
					  {
						  item.ModifiedProperties = fields;
					  }
                    foreach (var property in item.ModifiedProperties)
					{						
                        if (property != "GuidTrasport")
                             con.Entry(setT).Property(property).IsModified = true;

                    }

                
               int result = con.SaveChanges();
               if (result != 1)
               {
                   SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: 1");
               }


            }

			OnUpdatedAgile(this, new BusinessRulesEventArgs<Transport>() { Item = item, ContextRequest = contextNew  });

         }
		public void UpdateBulk(List<Transport>  items, params string[] fields)
         {
             SFSdotNet.Framework.My.ContextRequest req = new SFSdotNet.Framework.My.ContextRequest();
             req.CustomQuery = new SFSdotNet.Framework.My.CustomQuery();
             foreach (var field in fields)
             {
                 req.CustomQuery.SpecificProperties.Add(field);
             }
             UpdateBulk(items, req);

         }

		 public void DeleteBulk(List<Transport> entities, ContextRequest contextRequest = null)
        {

            using (EFContext con = new EFContext())
            {
                foreach (var entity in entities)
                {
					var entityProxy = new Transport() { GuidTrasport = entity.GuidTrasport };

                    con.Entry<Transport>(entityProxy).State = EntityState.Deleted;

                }

                int result = con.SaveChanges();
                if (result != entities.Count)
                {
                    SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: " + entities.Count.ToString());
                }
            }

        }

        public void UpdateBulk(List<Transport> items, ContextRequest contextRequest)
        {
            if (items.Count() > 0){

			 foreach (var entity in items)
            {


#region Autos
		if(!preventSecurityRestrictions){

				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	



			}
#endregion




				    if (entity.YBRoute != null)
					{
						//var yBRoute = new YBRoute();
						entity.GuidRoute = entity.YBRoute.GuidRoute;
						//entity.YBRoute = yBRoute;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<YBRoute>(con, itemForSave.YBRoute);
			
					}



				}
				using (EFContext con = new EFContext())
				{

                    
                
                   con.BulkUpdate(items);

				}
             
			}	  
        }

         public Transport Update(Transport entity)
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return Update(entity, contextRequest);
        }
       
         public Transport Update(Transport entity, ContextRequest contextRequest)
        {
		 if ((System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null) && contextRequest == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Update");
            }
            if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            }

			
				Transport  itemResult = null;

	
			//entity.UpdatedDate = DateTime.Now.ToUniversalTime();
			//if(contextRequest.User != null)
				//entity.UpdatedBy = contextRequest.User.GuidUser;

//	    var oldentity = GetBy(p => p.GuidTrasport == entity.GuidTrasport, contextRequest).FirstOrDefault();
	//	if (oldentity != null) {
		
          //  entity.CreatedDate = oldentity.CreatedDate;
    //        entity.CreatedBy = oldentity.CreatedBy;
	
      //      entity.GuidCompany = oldentity.GuidCompany;
	
			

	
		//}

			 using( EFContext con = new EFContext()){
				BusinessRulesEventArgs<Transport> e = null;
				bool preventPartial = false; 
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false)
                OnUpdating(this,e = new BusinessRulesEventArgs<Transport>() { ContextRequest = contextRequest, Item=entity});
				   if (e != null) {
						if (e.Cancel)
						{
							//outcontext = null;
							return e.Item;

						}
					}

	string includes = "YBRoute";
	IQueryable < Transport > query = con.Transports.AsQueryable();
	foreach (string include in includes.Split(char.Parse(",")))
                       {
                           if (!string.IsNullOrEmpty(include))
                               query = query.Include(include);
                       }
	var oldentity = query.FirstOrDefault(p => p.GuidTrasport == entity.GuidTrasport);
	if (oldentity.TransportCode != entity.TransportCode)
		oldentity.TransportCode = entity.TransportCode;
	if (oldentity.Description != entity.Description)
		oldentity.Description = entity.Description;
	if (oldentity.Capacity != entity.Capacity)
		oldentity.Capacity = entity.Capacity;

				//if (entity.UpdatedDate == null || (contextRequest != null && contextRequest.IsFromUI("Transports", UIActions.Updating)))
			oldentity.UpdatedDate = DateTime.Now.ToUniversalTime();
			if(contextRequest.User != null)
				oldentity.UpdatedBy = contextRequest.User.GuidUser;

           


						if (SFSdotNet.Framework.BR.Utils.HasRelationPropertyChanged(oldentity.YBRoute, entity.YBRoute, "GuidRoute"))
							oldentity.YBRoute = entity.YBRoute != null? new YBRoute(){ GuidRoute = entity.YBRoute.GuidRoute } :null;

                


				con.ChangeTracker.Entries().Where(p => p.Entity != oldentity).ForEach(p => p.State = EntityState.Unchanged);  
				  
				con.SaveChanges();
        
					 
					
               
				itemResult = entity;
				if(preventPartial == false)
					OnUpdated(this, e = new BusinessRulesEventArgs<Transport>() { ContextRequest = contextRequest, Item=itemResult });

              	return itemResult;
			}
			  
        }
        public Transport Save(Transport entity)
        {
			return Create(entity);
        }
        public int Save(List<Transport> entities)
        {
			 Create(entities);
            return entities.Count;

        }
        #endregion
        #region Delete
        public void Delete(Transport entity)
        {
				this.Delete(entity, null);
			
        }
		 public void Delete(Transport entity, ContextRequest contextRequest)
        {
				
				  List<Transport> entities = new List<Transport>();
				   entities.Add(entity);
				this.Delete(entities, contextRequest);
			
        }

         public void Delete(string query, Guid[] guids, ContextRequest contextRequest)
        {
			var br = new TransportsBR(true);
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);
            
            Delete(items, contextRequest);

        }
        public void Delete(Transport entity,  ContextRequest contextRequest, BusinessRulesEventArgs<Transport> e = null)
        {
			
				using(EFContext con = new EFContext())
                 {
				
               	BusinessRulesEventArgs<Transport> _e = null;
               List<Transport> _items = new List<Transport>();
                _items.Add(entity);
                if (e == null || e.PreventPartialPropagate == false)
                {
                    OnDeleting(this, _e = (e == null ? new BusinessRulesEventArgs<Transport>() { ContextRequest = contextRequest, Item = entity, Items = null  } : e));
                }
                if (_e != null)
                {
                    if (_e.Cancel)
						{
							context = null;
							return;

						}
					}


				
									//IsDeleted
					bool logicDelete = true;
					if (entity.IsDeleted != null)
					{
						if (entity.IsDeleted.Value)
							logicDelete = false;
					}
					if (logicDelete)
					{
											//entity = GetBy(p =>, contextRequest).FirstOrDefault();
						entity.IsDeleted = true;
						if (contextRequest != null && contextRequest.User != null)
							entity.UpdatedBy = contextRequest.User.GuidUser;
                        entity.UpdatedDate = DateTime.UtcNow;
						UpdateAgile(entity, "IsDeleted","UpdatedBy","UpdatedDate");

						
					}
					else {
					con.Entry<Transport>(entity).State = EntityState.Deleted;
					con.SaveChanges();
				
				 
					}
								
				
				 
					
					
			if (e == null || e.PreventPartialPropagate == false)
                {

                    if (_e == null)
                        _e = new BusinessRulesEventArgs<Transport>() { ContextRequest = contextRequest, Item = entity, Items = null };

                    OnDeleted(this, _e);
                }

				//return null;
			}
        }
 public void UnDelete(string query, Guid[] guids, ContextRequest contextRequest)
        {
            var br = new TransportsBR(true);
            contextRequest.CustomQuery.IncludeDeleted = true;
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);

            foreach (var item in items)
            {
                item.IsDeleted = false;
						if (contextRequest != null && contextRequest.User != null)
							item.UpdatedBy = contextRequest.User.GuidUser;
                        item.UpdatedDate = DateTime.UtcNow;
            }

            UpdateBulk(items, "IsDeleted","UpdatedBy","UpdatedDate");
        }

         public void Delete(List<Transport> entities,  ContextRequest contextRequest = null )
        {
				
			 BusinessRulesEventArgs<Transport> _e = null;

                OnDeleting(this, _e = new BusinessRulesEventArgs<Transport>() { ContextRequest = contextRequest, Item = null, Items = entities });
                if (_e != null)
                {
                    if (_e.Cancel)
                    {
                        context = null;
                        return;

                    }
                }
                bool allSucced = true;
                BusinessRulesEventArgs<Transport> eToChilds = new BusinessRulesEventArgs<Transport>();
                if (_e != null)
                {
                    eToChilds = _e;
                }
                else
                {
                    eToChilds = new BusinessRulesEventArgs<Transport>() { ContextRequest = contextRequest, Item = (entities.Count == 1 ? entities[0] : null), Items = entities };
                }
				foreach (Transport item in entities)
				{
					try
                    {
                        this.Delete(item, contextRequest, e: eToChilds);
                    }
                    catch (Exception ex)
                    {
                        SFSdotNet.Framework.My.EventLog.Error(ex);
                        allSucced = false;
                    }
				}
				if (_e == null)
                    _e = new BusinessRulesEventArgs<Transport>() { ContextRequest = contextRequest, CountResult = entities.Count, Item = null, Items = entities };
                OnDeleted(this, _e);

			
        }
        #endregion
 
        #region GetCount
		 public int GetCount(Expression<Func<Transport, bool>> predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

			return GetCount(predicate, contextRequest);
		}
        public int GetCount(Expression<Func<Transport, bool>> predicate, ContextRequest contextRequest)
        {


		
		 using (EFContext con = new EFContext())
            {


				if (predicate == null) predicate = PredicateBuilder.True<Transport>();
           		predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted == null);
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    		if (contextRequest.User !=null )
                        		if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
									predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa

								}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				
				IQueryable<Transport> query = con.Transports.AsQueryable();
                return query.AsExpandable().Count(predicate);

			
				}
			

        }
		  public int GetCount(string predicate,  ContextRequest contextRequest)
         {
             return GetCount(predicate, null, contextRequest);
         }

         public int GetCount(string predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return GetCount(predicate, contextRequest);
        }
		 public int GetCount(string predicate, string usemode){
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
				return GetCount( predicate,  usemode,  contextRequest);
		 }
        public int GetCount(string predicate, string usemode, ContextRequest contextRequest){

		using (EFContext con = new EFContext()) {
				string computedFields = "";
				string fkIncludes = "YBRoute";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<Transport>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetCount(con, predicate, usemode, contextRequest, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields);

			}
			#region old code
			 /* string freetext = null;
            Filter filter = new Filter();

              if (predicate.Contains("|"))
              {
                 
                  filter.SetFilterPart("ft", GetSpecificFilter(predicate, contextRequest));
                 
                  filter.ProcessText(predicate.Split(char.Parse("|"))[0]);
                  freetext = predicate.Split(char.Parse("|"))[1];

				  if (!string.IsNullOrEmpty(freetext) && string.IsNullOrEmpty(contextRequest.FreeText))
                  {
                      contextRequest.FreeText = freetext;
                  }
              }
              else {
                  filter.ProcessText(predicate);
              }
			   predicate = filter.GetFilterComplete();
			// BusinessRulesEventArgs<Transport>  e = null;
           	using (EFContext con = new EFContext())
			{
			
			

			 QueryBuild(predicate, filter, con, contextRequest, "count", new List<string>());


			
			BusinessRulesEventArgs<Transport> e = null;

			contextRequest.FreeText = freetext;
			contextRequest.UseMode = usemode;
            OnCounting(this, e = new BusinessRulesEventArgs<Transport>() {  Filter =filter, ContextRequest = contextRequest });
            if (e != null)
            {
                if (e.Cancel)
                {
                    context = null;
                    return e.CountResult;

                }

            

            }
			
			StringBuilder sbQuerySystem = new StringBuilder();
		
					
                    filter.SetFilterPart("de","(IsDeleted != true OR IsDeleted == null)");
			
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    	if (contextRequest.User !=null )
                        	if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
                        		
								filter.SetFilterPart("co", @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa
						
						
							}
							
							}
							if (preventSecurityRestrictions) preventSecurityRestrictions= false;
		
				   
                 filter.CleanAndProcess("");
				//string predicateWithFKAndComputed = SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicate );               
				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed();
               string predicateWithManyRelations = filter.GetFilterChildren();
			   ///QueryUtils.BreakeQuery1(predicate, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
			   predicate = filter.GetFilterComplete();
               if (!string.IsNullOrEmpty(predicate))
               {
				
					
                    return con.Transports.Where(predicate).Count();
					
                }else
                    return con.Transports.Count();
					
			}*/
			#endregion

		}
         public int GetCount()
        {
            return GetCount(p => true);
        }
        #endregion
        
         


        public void Delete(List<Transport.CompositeKey> entityKeys)
        {

            List<Transport> items = new List<Transport>();
            foreach (var itemKey in entityKeys)
            {
                items.Add(GetByKey(itemKey.GuidTrasport));
            }

            Delete(items);

        }
		 public void UpdateAssociation(string relation, string relationValue, string query, Guid[] ids, ContextRequest contextRequest)
        {
            var items = GetBy(query, null, null, null, null, null, contextRequest, ids);
			 var module = SFSdotNet.Framework.Cache.Caching.SystemObjects.GetModuleByKey(SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(System.Web.HttpContext.Current.Request.RequestContext, "area"));
           
            foreach (var item in items)
            {
			  Guid ? guidRelationValue = null ;
                if (!string.IsNullOrEmpty(relationValue)){
                    guidRelationValue = Guid.Parse(relationValue );
                }

				 if (relation.Contains("."))
                {
                    var partsWithOtherProp = relation.Split(char.Parse("|"));
                    var parts = partsWithOtherProp[0].Split(char.Parse("."));

                    string proxyRelName = parts[0];
                    string proxyProperty = parts[1];
                    string proxyPropertyKeyNameFromOther = partsWithOtherProp[1];
                    //string proxyPropertyThis = parts[2];

                    var prop = item.GetType().GetProperty(proxyRelName);
                    //var entityInfo = //SFSdotNet.Framework.
                    // descubrir el tipo de entidad dentro de la colección
                    Type typeEntityInList = SFSdotNet.Framework.Entities.Utils.GetTypeFromList(prop);
                    var newProxyItem = Activator.CreateInstance(typeEntityInList);
                    var propThisForSet = newProxyItem.GetType().GetProperty(proxyProperty);
                    var entityInfoOfProxy = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(typeEntityInList);
                    var propOther = newProxyItem.GetType().GetProperty(proxyPropertyKeyNameFromOther);

                    if (propThisForSet != null && entityInfoOfProxy != null && propOther != null )
                    {
                        var entityInfoThis = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(item.GetType());
                        var valueThisId = item.GetType().GetProperty(entityInfoThis.PropertyKeyName).GetValue(item);
                        if (valueThisId != null)
                            propThisForSet.SetValue(newProxyItem, valueThisId);
                        propOther.SetValue(newProxyItem, Guid.Parse(relationValue));
                        
                        var entityNameProp = newProxyItem.GetType().GetField("EntityName").GetValue(null);
                        var entitySetNameProp = newProxyItem.GetType().GetField("EntitySetName").GetValue(null);

                        SFSdotNet.Framework.Apps.Integration.CreateItemFromApp(entityNameProp.ToString(), entitySetNameProp.ToString(), module.ModuleNamespace, newProxyItem, contextRequest);

                    }

                    // crear una instancia del tipo de entidad
                    // llenar los datos y registrar nuevo


                }
                else
                {
                var prop = item.GetType().GetProperty(relation);
                var entityInfo = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(prop.PropertyType);
                if (entityInfo != null)
                {
                    var ins = Activator.CreateInstance(prop.PropertyType);
                   if (guidRelationValue != null)
                    {
                        prop.PropertyType.GetProperty(entityInfo.PropertyKeyName).SetValue(ins, guidRelationValue);
                        item.GetType().GetProperty(relation).SetValue(item, ins);
                    }
                    else
                    {
                        item.GetType().GetProperty(relation).SetValue(item, null);
                    }

                    Update(item, contextRequest);
                }

				}
            }
        }
		
	}
		public partial class YBLocationsBR:BRBase<YBLocation>{
	 	
           
		 #region Partial methods

           partial void OnUpdating(object sender, BusinessRulesEventArgs<YBLocation> e);

            partial void OnUpdated(object sender, BusinessRulesEventArgs<YBLocation> e);
			partial void OnUpdatedAgile(object sender, BusinessRulesEventArgs<YBLocation> e);

            partial void OnCreating(object sender, BusinessRulesEventArgs<YBLocation> e);
            partial void OnCreated(object sender, BusinessRulesEventArgs<YBLocation> e);

            partial void OnDeleting(object sender, BusinessRulesEventArgs<YBLocation> e);
            partial void OnDeleted(object sender, BusinessRulesEventArgs<YBLocation> e);

            partial void OnGetting(object sender, BusinessRulesEventArgs<YBLocation> e);
            protected override void OnVirtualGetting(object sender, BusinessRulesEventArgs<YBLocation> e)
            {
                OnGetting(sender, e);
            }
			protected override void OnVirtualCounting(object sender, BusinessRulesEventArgs<YBLocation> e)
            {
                OnCounting(sender, e);
            }
			partial void OnTaken(object sender, BusinessRulesEventArgs<YBLocation> e);
			protected override void OnVirtualTaken(object sender, BusinessRulesEventArgs<YBLocation> e)
            {
                OnTaken(sender, e);
            }

            partial void OnCounting(object sender, BusinessRulesEventArgs<YBLocation> e);
 
			partial void OnQuerySettings(object sender, BusinessRulesEventArgs<YBLocation> e);
          
            #endregion
			
		private static YBLocationsBR singlenton =null;
				public static YBLocationsBR NewInstance(){
					return  new YBLocationsBR();
					
				}
		public static YBLocationsBR Instance{
			get{
				if (singlenton == null)
					singlenton = new YBLocationsBR();
				return singlenton;
			}
		}
		//private bool preventSecurityRestrictions = false;
		 public bool PreventAuditTrail { get; set;  }
		#region Fields
        EFContext context = null;
        #endregion
        #region Constructor
        public YBLocationsBR()
        {
            context = new EFContext();
        }
		 public YBLocationsBR(bool preventSecurity)
            {
                this.preventSecurityRestrictions = preventSecurity;
				context = new EFContext();
            }
        #endregion
		
		#region Get

 		public IQueryable<YBLocation> Get()
        {
            using (EFContext con = new EFContext())
            {
				
				var query = con.YBLocations.AsQueryable();
                con.Configuration.ProxyCreationEnabled = false;

                //query = ContextQueryBuilder<Nutrient>.ApplyContextQuery(query, contextRequest);

                return query;




            }

        }
		


 	
		public List<YBLocation> GetAll()
        {
            return this.GetBy(p => true);
        }
        public List<YBLocation> GetAll(string includes)
        {
            return this.GetBy(p => true, includes);
        }
        public YBLocation GetByKey(Guid guidLocation)
        {
            return GetByKey(guidLocation, true);
        }
        public YBLocation GetByKey(Guid guidLocation, bool loadIncludes)
        {
            YBLocation item = null;
			var query = PredicateBuilder.True<YBLocation>();
                    
			string strWhere = @"GuidLocation = Guid(""" + guidLocation.ToString()+@""")";
            Expression<Func<YBLocation, bool>> predicate = null;
            //if (!string.IsNullOrEmpty(strWhere))
            //    predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<YBLocation, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
			 ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
            contextRequest.CustomQuery.FilterExpressionString = strWhere;

			//item = GetBy(predicate, loadIncludes, contextRequest).FirstOrDefault();
			item = GetBy(strWhere,loadIncludes,contextRequest).FirstOrDefault();
            return item;
        }
         public List<YBLocation> GetBy(string strWhere, bool loadRelations, ContextRequest contextRequest)
        {
            if (!loadRelations)
                return GetBy(strWhere, contextRequest);
            else
                return GetBy(strWhere, contextRequest, "");

        }
		  public List<YBLocation> GetBy(string strWhere, bool loadRelations)
        {
              if (!loadRelations)
                return GetBy(strWhere, new ContextRequest());
            else
                return GetBy(strWhere, new ContextRequest(), "");

        }
		         public YBLocation GetByKey(Guid guidLocation, params Expression<Func<YBLocation, object>>[] includes)
        {
            YBLocation item = null;
			string strWhere = @"GuidLocation = Guid(""" + guidLocation.ToString()+@""")";
          Expression<Func<YBLocation, bool>> predicate = p=> p.GuidLocation == guidLocation;
           // if (!string.IsNullOrEmpty(strWhere))
           //     predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<YBLocation, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
        item = GetBy(predicate, includes).FirstOrDefault();
         ////   item = GetBy(strWhere,includes).FirstOrDefault();
			return item;

        }
        public YBLocation GetByKey(Guid guidLocation, string includes)
        {
            YBLocation item = null;
			string strWhere = @"GuidLocation = Guid(""" + guidLocation.ToString()+@""")";
            
			
            item = GetBy(strWhere, includes).FirstOrDefault();
            return item;

        }
		 public YBLocation GetByKey(Guid guidLocation, string usemode, string includes)
		{
			return GetByKey(guidLocation, usemode, null, includes);

		 }
		 public YBLocation GetByKey(Guid guidLocation, string usemode, ContextRequest context,  string includes)
        {
            YBLocation item = null;
			string strWhere = @"GuidLocation = Guid(""" + guidLocation.ToString()+@""")";
			if (context == null){
				context = new ContextRequest();
				context.CustomQuery = new CustomQuery();
				context.CustomQuery.IsByKey = true;
				context.CustomQuery.FilterExpressionString = strWhere;
				context.UseMode = usemode;
			}
            item = GetBy(strWhere,context , includes).FirstOrDefault();
            return item;

        }

        #region Dynamic Predicate
        public List<YBLocation> GetBy(Expression<Func<YBLocation, bool>> predicate, int? pageSize, int? page)
        {
            return this.GetBy(predicate, pageSize, page, null, null);
        }
        public List<YBLocation> GetBy(Expression<Func<YBLocation, bool>> predicate, ContextRequest contextRequest)
        {

            return GetBy(predicate, contextRequest,"");
        }
        
        public List<YBLocation> GetBy(Expression<Func<YBLocation, bool>> predicate, ContextRequest contextRequest, params Expression<Func<YBLocation, object>>[] includes)
        {
            StringBuilder sb = new StringBuilder();
           if (includes != null)
            {
                foreach (var path in includes)
                {

						if (sb.Length > 0) sb.Append(",");
						sb.Append(SFSdotNet.Framework.Linq.Utils.IncludeToString<YBLocation>(path));

               }
            }
            return GetBy(predicate, contextRequest, sb.ToString());
        }
        
        
        public List<YBLocation> GetBy(Expression<Func<YBLocation, bool>> predicate, string includes)
        {
			ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";

            return GetBy(predicate, context, includes);
        }

        public List<YBLocation> GetBy(Expression<Func<YBLocation, bool>> predicate, params Expression<Func<YBLocation, object>>[] includes)
        {
			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest context = new ContextRequest();
			            context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";
            return GetBy(predicate, context, includes);
        }

      
		public bool DisableCache { get; set; }
		public List<YBLocation> GetBy(Expression<Func<YBLocation, bool>> predicate, ContextRequest contextRequest, string includes)
		{
            using (EFContext con = new EFContext()) {
				
				string fkIncludes = "LocationType";
                List<string> multilangProperties = new List<string>();
				if (predicate == null) predicate = PredicateBuilder.True<YBLocation>();
                var notDeletedExpression = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;
					Expression<Func<YBLocation,bool>> multitenantExpression  = null;
					if (contextRequest != null && contextRequest.Company != null)	                        	
						multitenantExpression = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicate, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression);

#region Old code
/*
				List<YBLocation> result = null;
               BusinessRulesEventArgs<YBLocation>  e = null;
	
				OnGetting(con, e = new BusinessRulesEventArgs<YBLocation>() {  FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null) });

               // OnGetting(con,e = new BusinessRulesEventArgs<YBLocation>() { FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = contextRequest.CustomQuery.FilterExpressionString});
				   if (e != null) {
				    predicate = e.FilterExpression;
						if (e.Cancel)
						{
							context = null;
							 if (e.Items == null) e.Items = new List<YBLocation>();
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                if (predicate == null) predicate = PredicateBuilder.True<YBLocation>();
 				string fkIncludes = "LocationType";
                if(contextRequest!=null){
					if (contextRequest.CustomQuery != null)
					{
						if (contextRequest.CustomQuery.IncludeForeignKeyPaths != null) {
							if (contextRequest.CustomQuery.IncludeForeignKeyPaths.Value == false)
								fkIncludes = "";
						}
					}
				}
				if (!string.IsNullOrEmpty(includes))
					includes = includes + "," + fkIncludes;
				else
					includes = fkIncludes;
                
                //var es = _repository.Queryable;

                IQueryable<YBLocation> query =  con.YBLocations.AsQueryable();

                                if (!string.IsNullOrEmpty(includes))
                {
                    foreach (string include in includes.Split(char.Parse(",")))
                    {
						if (!string.IsNullOrEmpty(include))
                            query = query.Include(include);
                    }
                }
                    predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
					 	if (!preventSecurityRestrictions)
						{
							if (contextRequest != null )
		                    	if (contextRequest.User !=null )
		                        	if (contextRequest.Company != null){
		                        	
										predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
 									
									}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				query =query.AsExpandable().Where(predicate);
                query = ContextQueryBuilder<YBLocation>.ApplyContextQuery(query, contextRequest);

                result = query.AsNoTracking().ToList<YBLocation>();
				  
                if (e != null)
                {
                    e.Items = result;
                }
				//if (contextRequest != null ){
				//	 contextRequest = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
					contextRequest.CustomQuery = new CustomQuery();

				//}
				OnTaken(this, e == null ? e =  new BusinessRulesEventArgs<YBLocation>() { Items= result, IncludingComputedLinq = false, ContextRequest = contextRequest,  FilterExpression = predicate } :  e);
  
			

                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
				*/
#endregion
            }
        }


		/*public int Update(List<YBLocation> items, ContextRequest contextRequest)
            {
                int result = 0;
                using (EFContext con = new EFContext())
                {
                   
                

                    foreach (var item in items)
                    {
                        //secMessageToUser messageToUser = new secMessageToUser();
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            item.GetType().GetProperty(prop).SetValue(item, item.GetType().GetProperty(prop).GetValue(item));
                        }
                        //messageToUser.GuidMessageToUser = (Guid)item.GetType().GetProperty("GuidMessageToUser").GetValue(item);

                        var setObject = con.CreateObjectSet<YBLocation>("YBLocations");
                        //messageToUser.Readed = DateTime.UtcNow;
                        setObject.Attach(item);
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            con.ObjectStateManager.GetObjectStateEntry(item).SetModifiedProperty(prop);
                        }
                       
                    }
                    result = con.SaveChanges();

                    


                }
                return result;
            }
           */
		

        public List<YBLocation> GetBy(string predicateString, ContextRequest contextRequest, string includes)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
				


				string computedFields = "";
				string fkIncludes = "LocationType";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<YBLocation>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					//if (contextRequest != null && contextRequest.Company != null)                      	
					//	 multitenantExpression = @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))";
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicateString, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression,computedFields);


	#region Old Code
	/*
				BusinessRulesEventArgs<YBLocation> e = null;

				Filter filter = new Filter();
                if (predicateString.Contains("|"))
                {
                    string ft = GetSpecificFilter(predicateString, contextRequest);
                    if (!string.IsNullOrEmpty(ft))
                        filter.SetFilterPart("ft", ft);
                   
                    contextRequest.FreeText = predicateString.Split(char.Parse("|"))[1];
                    var q1 = predicateString.Split(char.Parse("|"))[0];
                    if (!string.IsNullOrEmpty(q1))
                    {
                        filter.ProcessText(q1);
                    }
                }
                else {
                    filter.ProcessText(predicateString);
                }
				 var includesList = (new List<string>());
                 if (!string.IsNullOrEmpty(includes))
                 {
                     includesList = includes.Split(char.Parse(",")).ToList();
                 }

				List<YBLocation> result = new List<YBLocation>();
         
			QueryBuild(predicateString, filter, con, contextRequest, "getby", includesList);
			 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }
				
				
					OnGetting(con, e == null ? e = new BusinessRulesEventArgs<YBLocation>() { Filter = filter, ContextRequest = contextRequest  } : e );

                  //OnGetting(con,e = new BusinessRulesEventArgs<YBLocation>() {  ContextRequest = contextRequest, FilterExpressionString = predicateString });
			   	if (e != null) {
				    //predicateString = e.GetQueryString();
						if (e.Cancel)
						{
							context = null;
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				//	 else {
                //      predicateString = predicateString.Replace("*extraFreeText*", "").Replace("()","");
                //  }
				//con.EnableChangeTrackingUsingProxies = false;
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                //if (predicate == null) predicate = PredicateBuilder.True<YBLocation>();
 				string fkIncludes = "LocationType";
                if(contextRequest!=null){
					if (contextRequest.CustomQuery != null)
					{
						if (contextRequest.CustomQuery.IncludeForeignKeyPaths != null) {
							if (contextRequest.CustomQuery.IncludeForeignKeyPaths.Value == false)
								fkIncludes = "";
						}
					}
				}else{
                    contextRequest = new ContextRequest();
                    contextRequest.CustomQuery = new CustomQuery();

                }
				if (!string.IsNullOrEmpty(includes))
					includes = includes + "," + fkIncludes;
				else
					includes = fkIncludes;
                
                //var es = _repository.Queryable;
				IQueryable<YBLocation> query = con.YBLocations.AsQueryable();
		
				// include relations FK
				if(string.IsNullOrEmpty(includes) ){
					includes ="";
				}
				StringBuilder sbQuerySystem = new StringBuilder();
                    //predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				

				//if (!string.IsNullOrEmpty(predicateString))
                //      sbQuerySystem.Append(" And ");
                //sbQuerySystem.Append(" (IsDeleted != true Or IsDeleted = null) ");
				 filter.SetFilterPart("de", "(IsDeleted != true OR IsDeleted = null)");


					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
	                    	if (contextRequest.User !=null )
	                        	if (contextRequest.Company != null ){
	                        		//if (sbQuerySystem.Length > 0)
	                        		//	    			sbQuerySystem.Append( " And ");	
									//sbQuerySystem.Append(@" (GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa

									filter.SetFilterPart("co",@"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))");

								}
						}	
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				//string predicateString = predicate.ToDynamicLinq<YBLocation>();
				//predicateString += sbQuerySystem.ToString();
				filter.CleanAndProcess("");

				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed(); //SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicateString );               
                string predicateWithManyRelations = filter.GetFilterChildren(); //SFSdotNet.Framework.Linq.Utils.CleanPartExpression(predicateString);

                //QueryUtils.BreakeQuery1(predicateString, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
                var _queryable = query.AsQueryable();
				bool includeAll = true; 
                if (!string.IsNullOrEmpty(predicateWithManyRelations))
                    _queryable = _queryable.Where(predicateWithManyRelations, contextRequest.CustomQuery.ExtraParams);
				if (contextRequest.CustomQuery.SpecificProperties.Count > 0)
                {

				includeAll = false; 
                }

				StringBuilder sbSelect = new StringBuilder();
                sbSelect.Append("new (");
                bool existPrev = false;
                foreach (var selected in contextRequest.CustomQuery.SelectedFields.Where(p=> !string.IsNullOrEmpty(p.Linq)))
                {
                    if (existPrev) sbSelect.Append(", ");
                    if (!selected.Linq.Contains(".") && !selected.Linq.StartsWith("it."))
                        sbSelect.Append("it." + selected.Linq);
                    else
                        sbSelect.Append(selected.Linq);
                    existPrev = true;
                }
                sbSelect.Append(")");
                var queryable = _queryable.Select(sbSelect.ToString());                    


     				
                 if (!string.IsNullOrEmpty(predicateWithFKAndComputed))
                    queryable = queryable.Where(predicateWithFKAndComputed, contextRequest.CustomQuery.ExtraParams);

				QueryComplementOptions queryOps = ContextQueryBuilder.ApplyContextQuery(contextRequest);
            	if (!string.IsNullOrEmpty(queryOps.OrderByAndSort)){
					if (queryOps.OrderBy.Contains(".") && !queryOps.OrderBy.StartsWith("it.")) queryOps.OrderBy = "it." + queryOps.OrderBy;
					queryable = queryable.OrderBy(queryOps.OrderByAndSort);
					}
               	if (queryOps.Skip != null)
                {
                    queryable = queryable.Skip(queryOps.Skip.Value);
                }
                if (queryOps.PageSize != null)
                {
                    queryable = queryable.Take (queryOps.PageSize.Value);
                }


                var resultTemp = queryable.AsQueryable().ToListAsync().Result;
                foreach (var item in resultTemp)
                {

				   result.Add(SFSdotNet.Framework.BR.Utils.GetConverted<YBLocation,dynamic>(item, contextRequest.CustomQuery.SelectedFields.Select(p=>p.Name).ToArray()));
                }

			 if (e != null)
                {
                    e.Items = result;
                }
				 contextRequest.CustomQuery = new CustomQuery();
				OnTaken(this, e == null ? e = new BusinessRulesEventArgs<YBLocation>() { Items= result, IncludingComputedLinq = true, ContextRequest = contextRequest, FilterExpressionString  = predicateString } :  e);
  
			
  
                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
	
	*/
	#endregion

            }
        }
		public YBLocation GetFromOperation(string function, string filterString, string usemode, string fields, ContextRequest contextRequest)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
                string computedFields = "";
               // string fkIncludes = "accContpaqiClassification,accProjectConcept,accProjectType,accProxyUser";
                List<string> multilangProperties = new List<string>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					if (contextRequest != null && contextRequest.Company != null)
					{
						multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
						contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
					}
					 									
					string multiTenantField = "GuidCompany";


                return GetSummaryOperation(con, new YBLocation(), function, filterString, usemode, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields, contextRequest, fields.Split(char.Parse(",")).ToArray());
            }
        }

   protected override void QueryBuild(string predicate, Filter filter, DbContext efContext, ContextRequest contextRequest, string method, List<string> includesList)
      	{
				if (contextRequest.CustomQuery.SpecificProperties.Count == 0)
                {
					contextRequest.CustomQuery.SpecificProperties.Add(YBLocation.PropertyNames.GuidLocationType);
					contextRequest.CustomQuery.SpecificProperties.Add(YBLocation.PropertyNames.Lat);
					contextRequest.CustomQuery.SpecificProperties.Add(YBLocation.PropertyNames.Long);
					contextRequest.CustomQuery.SpecificProperties.Add(YBLocation.PropertyNames.Description);
					contextRequest.CustomQuery.SpecificProperties.Add(YBLocation.PropertyNames.Address);
					contextRequest.CustomQuery.SpecificProperties.Add(YBLocation.PropertyNames.GuidCompany);
					contextRequest.CustomQuery.SpecificProperties.Add(YBLocation.PropertyNames.CreatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(YBLocation.PropertyNames.UpdatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(YBLocation.PropertyNames.CreatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(YBLocation.PropertyNames.UpdatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(YBLocation.PropertyNames.Bytes);
					contextRequest.CustomQuery.SpecificProperties.Add(YBLocation.PropertyNames.IsDeleted);
					contextRequest.CustomQuery.SpecificProperties.Add(YBLocation.PropertyNames.LocationType);
                    
				}

				if (method == "getby" || method == "sum")
				{
					if (!contextRequest.CustomQuery.SpecificProperties.Contains("GuidLocation")){
						contextRequest.CustomQuery.SpecificProperties.Add("GuidLocation");
					}

					 if (!string.IsNullOrEmpty(contextRequest.CustomQuery.OrderBy))
					{
						string existPropertyOrderBy = contextRequest.CustomQuery.OrderBy;
						if (contextRequest.CustomQuery.OrderBy.Contains("."))
						{
							existPropertyOrderBy = contextRequest.CustomQuery.OrderBy.Split(char.Parse("."))[0];
						}
						if (!contextRequest.CustomQuery.SpecificProperties.Exists(p => p == existPropertyOrderBy))
						{
							contextRequest.CustomQuery.SpecificProperties.Add(existPropertyOrderBy);
						}
					}

				}
				
	bool isFullDetails = contextRequest.IsFromUI("YBLocations", UIActions.GetForDetails);
	string filterForTest = predicate  + filter.GetFilterComplete();

				if (isFullDetails || !string.IsNullOrEmpty(predicate))
            {
            } 

			if (method == "sum")
            {
            } 
			if (contextRequest.CustomQuery.SelectedFields.Count == 0)
            {
				foreach (var selected in contextRequest.CustomQuery.SpecificProperties)
                {
					string linq = selected;
					switch (selected)
                    {

					case "LocationType":
					if (includesList.Contains(selected)){
                        linq = "it.LocationType as LocationType";
					}
                    else
						linq = "iif(it.LocationType != null, new (it.LocationType.GuidLocationType, it.LocationType.Name), null) as LocationType";
 					break;
					 
						
					 default:
                            break;
                    }
					contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name=selected, Linq=linq});
					if (method == "getby" || method == "sum")
					{
						if (includesList.Contains(selected))
							includesList.Remove(selected);

					}

				}
			}
				if (method == "getby" || method == "sum")
				{
					foreach (var otherInclude in includesList.Where(p=> !string.IsNullOrEmpty(p)))
					{
						contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name = otherInclude, Linq = "it." + otherInclude +" as " + otherInclude });
					}
				}
				BusinessRulesEventArgs<YBLocation> e = null;
				if (contextRequest.PreventInterceptors == false)
					OnQuerySettings(efContext, e = new BusinessRulesEventArgs<YBLocation>() { Filter = filter, ContextRequest = contextRequest /*, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null)*/ });

				//List<YBLocation> result = new List<YBLocation>();
                 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }

}
		public List<YBLocation> GetBy(Expression<Func<YBLocation, bool>> predicate, bool loadRelations, ContextRequest contextRequest)
        {
			if(!loadRelations)
				return GetBy(predicate, contextRequest);
			else
				return GetBy(predicate, contextRequest, "RouteLocations,StudentLocations");

        }

        public List<YBLocation> GetBy(Expression<Func<YBLocation, bool>> predicate, int? pageSize, int? page, string orderBy, SFSdotNet.Framework.Data.SortDirection? sortDirection)
        {
            return GetBy(predicate, new ContextRequest() { CustomQuery = new CustomQuery() { Page = page, PageSize = pageSize, OrderBy = orderBy, SortDirection = sortDirection } });
        }
        public List<YBLocation> GetBy(Expression<Func<YBLocation, bool>> predicate)
        {

			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
			contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
			            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            contextRequest.CustomQuery.FilterExpressionString = null;
            return this.GetBy(predicate, contextRequest, "");
        }
        #endregion
        #region Dynamic String
		protected override string GetSpecificFilter(string filter, ContextRequest contextRequest) {
            string result = "";
		    //string linqFilter = String.Empty;
            string freeTextFilter = String.Empty;
            if (filter.Contains("|"))
            {
               // linqFilter = filter.Split(char.Parse("|"))[0];
                freeTextFilter = filter.Split(char.Parse("|"))[1];
            }
            //else {
            //    freeTextFilter = filter;
            //}
            //else {
            //    linqFilter = filter;
            //}
			// linqFilter = SFSdotNet.Framework.Linq.Utils.ReplaceCustomDateFilters(linqFilter);
            //string specificFilter = linqFilter;
            if (!string.IsNullOrEmpty(freeTextFilter))
            {
                System.Text.StringBuilder sbCont = new System.Text.StringBuilder();
                /*if (specificFilter.Length > 0)
                {
                    sbCont.Append(" AND ");
                    sbCont.Append(" ({0})");
                }
                else
                {
                    sbCont.Append("{0}");
                }*/
                //var words = freeTextFilter.Split(char.Parse(" "));
				var word = freeTextFilter;
                System.Text.StringBuilder sbSpec = new System.Text.StringBuilder();
                 int nWords = 1;
				/*foreach (var word in words)
                {
					if (word.Length > 0){
                    if (sbSpec.Length > 0) sbSpec.Append(" AND ");
					if (words.Length > 1) sbSpec.Append("("); */
					
	
					
	
					
	
					
	
					
					
					
									
					sbSpec.Append(string.Format(@"Description.Contains(""{0}"")", word));
					

					
					
										sbSpec.Append(" OR ");
					
									
					sbSpec.Append(string.Format(@"Address.Contains(""{0}"")", word));
					

					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
								sbSpec.Append(" OR ");
					
					//if (sbSpec.Length > 2)
					//	sbSpec.Append(" OR "); // test
					sbSpec.Append(string.Format(@"it.LocationType.Name.Contains(""{0}"")", word));
								 //sbSpec.Append("*extraFreeText*");

                    /*if (words.Length > 1) sbSpec.Append(")");
					
					nWords++;

					}

                }*/
                //specificFilter = string.Format("{0}{1}", specificFilter, string.Format(sbCont.ToString(), sbSpec.ToString()));
                                 result = sbSpec.ToString();  
            }
			//result = specificFilter;
			
			return result;

		}
	
			public List<YBLocation> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params object[] extraParams)
        {
			return GetBy(filter, pageSize, page, orderBy, orderDir,  null, extraParams);
		}
           public List<YBLocation> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, string usemode, params object[] extraParams)
            { 
                return GetBy(filter, pageSize, page, orderBy, orderDir, usemode, null, extraParams);
            }


		public List<YBLocation> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  string usemode, ContextRequest context, params object[] extraParams)

        {

            // string freetext = null;
            //if (filter.Contains("|"))
            //{
            //    int parts = filter.Split(char.Parse("|")).Count();
            //    if (parts > 1)
            //    {

            //        freetext = filter.Split(char.Parse("|"))[1];
            //    }
            //}
		
            //string specificFilter = "";
            //if (!string.IsNullOrEmpty(filter))
            //  specificFilter=  GetSpecificFilter(filter);
            if (string.IsNullOrEmpty(orderBy))
            {
			                orderBy = "UpdatedDate";
            }
			//orderDir = "desc";
			SFSdotNet.Framework.Data.SortDirection direction = SFSdotNet.Framework.Data.SortDirection.Ascending;
            if (!string.IsNullOrEmpty(orderDir))
            {
                if (orderDir == "desc")
                    direction = SFSdotNet.Framework.Data.SortDirection.Descending;
            }
            if (context == null)
                context = new ContextRequest();
			

             context.UseMode = usemode;
             if (context.CustomQuery == null )
                context.CustomQuery =new SFSdotNet.Framework.My.CustomQuery();

 
                context.CustomQuery.ExtraParams = extraParams;

                    context.CustomQuery.OrderBy = orderBy;
                   context.CustomQuery.SortDirection = direction;
                   context.CustomQuery.Page = page;
                  context.CustomQuery.PageSize = pageSize;
               

            

            if (!preventSecurityRestrictions) {
			 if (context.CurrentContext == null)
                {
					if (SFSdotNet.Framework.My.Context.CurrentContext != null &&  SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
					{
						context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
						context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

					}
					else {
						throw new Exception("The security rule require a specific user and company");
					}
				}
            }
            return GetBy(filter, context);
  
        }


        public List<YBLocation> GetBy(string strWhere, ContextRequest contextRequest)
        {
        	#region old code
				
				 //Expression<Func<tvsReservationTransport, bool>> predicate = null;
				string strWhereClean = strWhere.Replace("*extraFreeText*", "").Replace("()", "");
                //if (!string.IsNullOrEmpty(strWhereClean)){

                //    object[] extraParams = null;
                //    //if (contextRequest != null )
                //    //    if (contextRequest.CustomQuery != null )
                //    //        extraParams = contextRequest.CustomQuery.ExtraParams;
                //    //predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<tvsReservationTransport, bool>(strWhereClean, extraParams != null? extraParams.Cast<Guid>(): null);				
                //}
				 if (contextRequest == null)
                {
                    contextRequest = new ContextRequest();
                    if (contextRequest.CustomQuery == null)
                        contextRequest.CustomQuery = new CustomQuery();
                }
                  if (!preventSecurityRestrictions) {
					if (contextRequest.User == null || contextRequest.Company == null)
                      {
                     if (SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
                     {
                         contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                         contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

                     }
                     else {
                         throw new Exception("The security rule require a specific User and Company ");
                     }
					 }
                 }
            contextRequest.CustomQuery.FilterExpressionString = strWhere;
				//return GetBy(predicate, contextRequest);  

			#endregion				
				
                    return GetBy(strWhere, contextRequest, "");  


        }
       public List<YBLocation> GetBy(string strWhere)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
			
            return GetBy(strWhere, context, null);
        }

        public List<YBLocation> GetBy(string strWhere, string includes)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
            return GetBy(strWhere, context, includes);
        }

        #endregion
        #endregion
		
		  #region SaveOrUpdate
        
 		 public YBLocation Create(YBLocation entity)
        {
				//ObjectContext context = null;
				    if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: Create");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

				return this.Create(entity, contextRequest);


        }
        
       
        public YBLocation Create(YBLocation entity, ContextRequest contextRequest)
        {
		
		bool graph = false;
	
				bool preventPartial = false;
                if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
               
			using (EFContext con = new EFContext()) {

				YBLocation itemForSave = new YBLocation();
#region Autos
		if(!preventSecurityRestrictions){

				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion
               BusinessRulesEventArgs<YBLocation> e = null;
			    if (preventPartial == false )
                OnCreating(this,e = new BusinessRulesEventArgs<YBLocation>() { ContextRequest = contextRequest, Item=entity });
				   if (e != null) {
						if (e.Cancel)
						{
							context = null;
							return e.Item;

						}
					}

                    if (entity.GuidLocation == Guid.Empty)
                   {
                       entity.GuidLocation = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   itemForSave.GuidLocation = entity.GuidLocation;
				  
		
			itemForSave.GuidLocation = entity.GuidLocation;

			itemForSave.Lat = entity.Lat;

			itemForSave.Long = entity.Long;

			itemForSave.Description = entity.Description;

			itemForSave.Address = entity.Address;

			itemForSave.GuidCompany = entity.GuidCompany;

			itemForSave.CreatedDate = entity.CreatedDate;

			itemForSave.UpdatedDate = entity.UpdatedDate;

			itemForSave.CreatedBy = entity.CreatedBy;

			itemForSave.UpdatedBy = entity.UpdatedBy;

			itemForSave.Bytes = entity.Bytes;

			itemForSave.IsDeleted = entity.IsDeleted;

				
				con.YBLocations.Add(itemForSave);



					if (entity.LocationType != null)
					{
						var locationType = new LocationType();
						locationType.GuidLocationType = entity.LocationType.GuidLocationType;
						itemForSave.LocationType = locationType;
						SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<LocationType>(con, itemForSave.LocationType);
			
					}







                
				con.ChangeTracker.Entries().Where(p => p.Entity != itemForSave && p.State != EntityState.Unchanged).ForEach(p => p.State = EntityState.Detached);

				con.Entry<YBLocation>(itemForSave).State = EntityState.Added;

				con.SaveChanges();

					 
				

				//itemResult = entity;
                //if (e != null)
                //{
                 //   e.Item = itemResult;
                //}
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false )
                OnCreated(this, e == null ? e = new BusinessRulesEventArgs<YBLocation>() { ContextRequest = contextRequest, Item = entity } : e);



                if (e != null && e.Item != null )
                {
                    return e.Item;
                }
                              return entity;
			}
            
        }
        //BusinessRulesEventArgs<YBLocation> e = null;
        public void Create(List<YBLocation> entities)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            Create(entities, contextRequest);
        }
        public void Create(List<YBLocation> entities, ContextRequest contextRequest)
        
        {
			ObjectContext context = null;
            	foreach (YBLocation entity in entities)
				{
					this.Create(entity, contextRequest);
				}
        }
		  public void CreateOrUpdateBulk(List<YBLocation> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "cu", contextRequest);
        }

        private void CreateOrUpdateBulk(List<YBLocation> entities, string actionKey, ContextRequest contextRequest)
        {
			if (entities.Count() > 0){
            bool graph = false;

            bool preventPartial = false;
            if (contextRequest != null && contextRequest.PreventInterceptors == true)
            {
                preventPartial = true;
            }
            foreach (var entity in entities)
            {
                    if (entity.GuidLocation == Guid.Empty)
                   {
                       entity.GuidLocation = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   
				  


#region Autos
		if(!preventSecurityRestrictions){


 if (actionKey != "u")
                        {
				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;


}
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion


		
			//entity.GuidLocation = entity.GuidLocation;

			//entity.Lat = entity.Lat;

			//entity.Long = entity.Long;

			//entity.Description = entity.Description;

			//entity.Address = entity.Address;

			//entity.GuidCompany = entity.GuidCompany;

			//entity.CreatedDate = entity.CreatedDate;

			//entity.UpdatedDate = entity.UpdatedDate;

			//entity.CreatedBy = entity.CreatedBy;

			//entity.UpdatedBy = entity.UpdatedBy;

			//entity.Bytes = entity.Bytes;

			//entity.IsDeleted = entity.IsDeleted;

				
				



				    if (entity.LocationType != null)
					{
						//var locationType = new LocationType();
						entity.GuidLocationType = entity.LocationType.GuidLocationType;
						//entity.LocationType = locationType;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<LocationType>(con, itemForSave.LocationType);
			
					}







                
				

					 
				

				//itemResult = entity;
            }
            using (EFContext con = new EFContext())
            {
                 if (actionKey == "c")
                    {
                        context.BulkInsert(entities);
                    }else if ( actionKey == "u")
                    {
                        context.BulkUpdate(entities);
                    }else
                    {
                        context.BulkInsertOrUpdate(entities);
                    }
            }

			}
        }
	
		public void CreateBulk(List<YBLocation> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "c", contextRequest);
        }


		public void UpdateAgile(YBLocation item, params string[] fields)
         {
			UpdateAgile(item, null, fields);
        }
		public void UpdateAgile(YBLocation item, ContextRequest contextRequest, params string[] fields)
         {
            
             ContextRequest contextNew = null;
             if (contextRequest != null)
             {
                 contextNew = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
                 if (fields != null && fields.Length > 0)
                 {
                     contextNew.CustomQuery.SpecificProperties  = fields.ToList();
                 }
                 else if(contextRequest.CustomQuery.SpecificProperties.Count > 0)
                 {
                     fields = contextRequest.CustomQuery.SpecificProperties.ToArray();
                 }
             }
			

		   using (EFContext con = new EFContext())
            {



               
					List<string> propForCopy = new List<string>();
                    propForCopy.AddRange(fields);
                    
					  
					if (!propForCopy.Contains("GuidLocation"))
						propForCopy.Add("GuidLocation");

					var itemForUpdate = SFSdotNet.Framework.BR.Utils.GetConverted<YBLocation,YBLocation>(item, propForCopy.ToArray());
					 itemForUpdate.GuidLocation = item.GuidLocation;
                  var setT = con.Set<YBLocation>().Attach(itemForUpdate);

					if (fields.Count() > 0)
					  {
						  item.ModifiedProperties = fields;
					  }
                    foreach (var property in item.ModifiedProperties)
					{						
                        if (property != "GuidLocation")
                             con.Entry(setT).Property(property).IsModified = true;

                    }

                
               int result = con.SaveChanges();
               if (result != 1)
               {
                   SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: 1");
               }


            }

			OnUpdatedAgile(this, new BusinessRulesEventArgs<YBLocation>() { Item = item, ContextRequest = contextNew  });

         }
		public void UpdateBulk(List<YBLocation>  items, params string[] fields)
         {
             SFSdotNet.Framework.My.ContextRequest req = new SFSdotNet.Framework.My.ContextRequest();
             req.CustomQuery = new SFSdotNet.Framework.My.CustomQuery();
             foreach (var field in fields)
             {
                 req.CustomQuery.SpecificProperties.Add(field);
             }
             UpdateBulk(items, req);

         }

		 public void DeleteBulk(List<YBLocation> entities, ContextRequest contextRequest = null)
        {

            using (EFContext con = new EFContext())
            {
                foreach (var entity in entities)
                {
					var entityProxy = new YBLocation() { GuidLocation = entity.GuidLocation };

                    con.Entry<YBLocation>(entityProxy).State = EntityState.Deleted;

                }

                int result = con.SaveChanges();
                if (result != entities.Count)
                {
                    SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: " + entities.Count.ToString());
                }
            }

        }

        public void UpdateBulk(List<YBLocation> items, ContextRequest contextRequest)
        {
            if (items.Count() > 0){

			 foreach (var entity in items)
            {


#region Autos
		if(!preventSecurityRestrictions){

				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	



			}
#endregion




				    if (entity.LocationType != null)
					{
						//var locationType = new LocationType();
						entity.GuidLocationType = entity.LocationType.GuidLocationType;
						//entity.LocationType = locationType;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<LocationType>(con, itemForSave.LocationType);
			
					}







				}
				using (EFContext con = new EFContext())
				{

                    
                
                   con.BulkUpdate(items);

				}
             
			}	  
        }

         public YBLocation Update(YBLocation entity)
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return Update(entity, contextRequest);
        }
       
         public YBLocation Update(YBLocation entity, ContextRequest contextRequest)
        {
		 if ((System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null) && contextRequest == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Update");
            }
            if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            }

			
				YBLocation  itemResult = null;

	
			//entity.UpdatedDate = DateTime.Now.ToUniversalTime();
			//if(contextRequest.User != null)
				//entity.UpdatedBy = contextRequest.User.GuidUser;

//	    var oldentity = GetBy(p => p.GuidLocation == entity.GuidLocation, contextRequest).FirstOrDefault();
	//	if (oldentity != null) {
		
          //  entity.CreatedDate = oldentity.CreatedDate;
    //        entity.CreatedBy = oldentity.CreatedBy;
	
      //      entity.GuidCompany = oldentity.GuidCompany;
	
			

	
		//}

			 using( EFContext con = new EFContext()){
				BusinessRulesEventArgs<YBLocation> e = null;
				bool preventPartial = false; 
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false)
                OnUpdating(this,e = new BusinessRulesEventArgs<YBLocation>() { ContextRequest = contextRequest, Item=entity});
				   if (e != null) {
						if (e.Cancel)
						{
							//outcontext = null;
							return e.Item;

						}
					}

	string includes = "LocationType";
	IQueryable < YBLocation > query = con.YBLocations.AsQueryable();
	foreach (string include in includes.Split(char.Parse(",")))
                       {
                           if (!string.IsNullOrEmpty(include))
                               query = query.Include(include);
                       }
	var oldentity = query.FirstOrDefault(p => p.GuidLocation == entity.GuidLocation);
	if (oldentity.Lat != entity.Lat)
		oldentity.Lat = entity.Lat;
	if (oldentity.Long != entity.Long)
		oldentity.Long = entity.Long;
	if (oldentity.Description != entity.Description)
		oldentity.Description = entity.Description;
	if (oldentity.Address != entity.Address)
		oldentity.Address = entity.Address;

				//if (entity.UpdatedDate == null || (contextRequest != null && contextRequest.IsFromUI("YBLocations", UIActions.Updating)))
			oldentity.UpdatedDate = DateTime.Now.ToUniversalTime();
			if(contextRequest.User != null)
				oldentity.UpdatedBy = contextRequest.User.GuidUser;

           


						if (SFSdotNet.Framework.BR.Utils.HasRelationPropertyChanged(oldentity.LocationType, entity.LocationType, "GuidLocationType"))
							oldentity.LocationType = entity.LocationType != null? new LocationType(){ GuidLocationType = entity.LocationType.GuidLocationType } :null;

                


				if (entity.RouteLocations != null)
                {
                    foreach (var item in entity.RouteLocations)
                    {


                        
                    }
					
                    

                }


				if (entity.StudentLocations != null)
                {
                    foreach (var item in entity.StudentLocations)
                    {


                        
                    }
					
                    

                }


				con.ChangeTracker.Entries().Where(p => p.Entity != oldentity).ForEach(p => p.State = EntityState.Unchanged);  
				  
				con.SaveChanges();
        
					 
					
               
				itemResult = entity;
				if(preventPartial == false)
					OnUpdated(this, e = new BusinessRulesEventArgs<YBLocation>() { ContextRequest = contextRequest, Item=itemResult });

              	return itemResult;
			}
			  
        }
        public YBLocation Save(YBLocation entity)
        {
			return Create(entity);
        }
        public int Save(List<YBLocation> entities)
        {
			 Create(entities);
            return entities.Count;

        }
        #endregion
        #region Delete
        public void Delete(YBLocation entity)
        {
				this.Delete(entity, null);
			
        }
		 public void Delete(YBLocation entity, ContextRequest contextRequest)
        {
				
				  List<YBLocation> entities = new List<YBLocation>();
				   entities.Add(entity);
				this.Delete(entities, contextRequest);
			
        }

         public void Delete(string query, Guid[] guids, ContextRequest contextRequest)
        {
			var br = new YBLocationsBR(true);
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);
            
            Delete(items, contextRequest);

        }
        public void Delete(YBLocation entity,  ContextRequest contextRequest, BusinessRulesEventArgs<YBLocation> e = null)
        {
			
				using(EFContext con = new EFContext())
                 {
				
               	BusinessRulesEventArgs<YBLocation> _e = null;
               List<YBLocation> _items = new List<YBLocation>();
                _items.Add(entity);
                if (e == null || e.PreventPartialPropagate == false)
                {
                    OnDeleting(this, _e = (e == null ? new BusinessRulesEventArgs<YBLocation>() { ContextRequest = contextRequest, Item = entity, Items = null  } : e));
                }
                if (_e != null)
                {
                    if (_e.Cancel)
						{
							context = null;
							return;

						}
					}


				
									//IsDeleted
					bool logicDelete = true;
					if (entity.IsDeleted != null)
					{
						if (entity.IsDeleted.Value)
							logicDelete = false;
					}
					if (logicDelete)
					{
											//entity = GetBy(p =>, contextRequest).FirstOrDefault();
						entity.IsDeleted = true;
						if (contextRequest != null && contextRequest.User != null)
							entity.UpdatedBy = contextRequest.User.GuidUser;
                        entity.UpdatedDate = DateTime.UtcNow;
						UpdateAgile(entity, "IsDeleted","UpdatedBy","UpdatedDate");

						
					}
					else {
					con.Entry<YBLocation>(entity).State = EntityState.Deleted;
					con.SaveChanges();
				
				 
					}
								
				
				 
					
					
			if (e == null || e.PreventPartialPropagate == false)
                {

                    if (_e == null)
                        _e = new BusinessRulesEventArgs<YBLocation>() { ContextRequest = contextRequest, Item = entity, Items = null };

                    OnDeleted(this, _e);
                }

				//return null;
			}
        }
 public void UnDelete(string query, Guid[] guids, ContextRequest contextRequest)
        {
            var br = new YBLocationsBR(true);
            contextRequest.CustomQuery.IncludeDeleted = true;
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);

            foreach (var item in items)
            {
                item.IsDeleted = false;
						if (contextRequest != null && contextRequest.User != null)
							item.UpdatedBy = contextRequest.User.GuidUser;
                        item.UpdatedDate = DateTime.UtcNow;
            }

            UpdateBulk(items, "IsDeleted","UpdatedBy","UpdatedDate");
        }

         public void Delete(List<YBLocation> entities,  ContextRequest contextRequest = null )
        {
				
			 BusinessRulesEventArgs<YBLocation> _e = null;

                OnDeleting(this, _e = new BusinessRulesEventArgs<YBLocation>() { ContextRequest = contextRequest, Item = null, Items = entities });
                if (_e != null)
                {
                    if (_e.Cancel)
                    {
                        context = null;
                        return;

                    }
                }
                bool allSucced = true;
                BusinessRulesEventArgs<YBLocation> eToChilds = new BusinessRulesEventArgs<YBLocation>();
                if (_e != null)
                {
                    eToChilds = _e;
                }
                else
                {
                    eToChilds = new BusinessRulesEventArgs<YBLocation>() { ContextRequest = contextRequest, Item = (entities.Count == 1 ? entities[0] : null), Items = entities };
                }
				foreach (YBLocation item in entities)
				{
					try
                    {
                        this.Delete(item, contextRequest, e: eToChilds);
                    }
                    catch (Exception ex)
                    {
                        SFSdotNet.Framework.My.EventLog.Error(ex);
                        allSucced = false;
                    }
				}
				if (_e == null)
                    _e = new BusinessRulesEventArgs<YBLocation>() { ContextRequest = contextRequest, CountResult = entities.Count, Item = null, Items = entities };
                OnDeleted(this, _e);

			
        }
        #endregion
 
        #region GetCount
		 public int GetCount(Expression<Func<YBLocation, bool>> predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

			return GetCount(predicate, contextRequest);
		}
        public int GetCount(Expression<Func<YBLocation, bool>> predicate, ContextRequest contextRequest)
        {


		
		 using (EFContext con = new EFContext())
            {


				if (predicate == null) predicate = PredicateBuilder.True<YBLocation>();
           		predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted == null);
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    		if (contextRequest.User !=null )
                        		if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
									predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa

								}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				
				IQueryable<YBLocation> query = con.YBLocations.AsQueryable();
                return query.AsExpandable().Count(predicate);

			
				}
			

        }
		  public int GetCount(string predicate,  ContextRequest contextRequest)
         {
             return GetCount(predicate, null, contextRequest);
         }

         public int GetCount(string predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return GetCount(predicate, contextRequest);
        }
		 public int GetCount(string predicate, string usemode){
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
				return GetCount( predicate,  usemode,  contextRequest);
		 }
        public int GetCount(string predicate, string usemode, ContextRequest contextRequest){

		using (EFContext con = new EFContext()) {
				string computedFields = "";
				string fkIncludes = "LocationType";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<YBLocation>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetCount(con, predicate, usemode, contextRequest, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields);

			}
			#region old code
			 /* string freetext = null;
            Filter filter = new Filter();

              if (predicate.Contains("|"))
              {
                 
                  filter.SetFilterPart("ft", GetSpecificFilter(predicate, contextRequest));
                 
                  filter.ProcessText(predicate.Split(char.Parse("|"))[0]);
                  freetext = predicate.Split(char.Parse("|"))[1];

				  if (!string.IsNullOrEmpty(freetext) && string.IsNullOrEmpty(contextRequest.FreeText))
                  {
                      contextRequest.FreeText = freetext;
                  }
              }
              else {
                  filter.ProcessText(predicate);
              }
			   predicate = filter.GetFilterComplete();
			// BusinessRulesEventArgs<YBLocation>  e = null;
           	using (EFContext con = new EFContext())
			{
			
			

			 QueryBuild(predicate, filter, con, contextRequest, "count", new List<string>());


			
			BusinessRulesEventArgs<YBLocation> e = null;

			contextRequest.FreeText = freetext;
			contextRequest.UseMode = usemode;
            OnCounting(this, e = new BusinessRulesEventArgs<YBLocation>() {  Filter =filter, ContextRequest = contextRequest });
            if (e != null)
            {
                if (e.Cancel)
                {
                    context = null;
                    return e.CountResult;

                }

            

            }
			
			StringBuilder sbQuerySystem = new StringBuilder();
		
					
                    filter.SetFilterPart("de","(IsDeleted != true OR IsDeleted == null)");
			
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    	if (contextRequest.User !=null )
                        	if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
                        		
								filter.SetFilterPart("co", @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa
						
						
							}
							
							}
							if (preventSecurityRestrictions) preventSecurityRestrictions= false;
		
				   
                 filter.CleanAndProcess("");
				//string predicateWithFKAndComputed = SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicate );               
				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed();
               string predicateWithManyRelations = filter.GetFilterChildren();
			   ///QueryUtils.BreakeQuery1(predicate, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
			   predicate = filter.GetFilterComplete();
               if (!string.IsNullOrEmpty(predicate))
               {
				
					
                    return con.YBLocations.Where(predicate).Count();
					
                }else
                    return con.YBLocations.Count();
					
			}*/
			#endregion

		}
         public int GetCount()
        {
            return GetCount(p => true);
        }
        #endregion
        
         


        public void Delete(List<YBLocation.CompositeKey> entityKeys)
        {

            List<YBLocation> items = new List<YBLocation>();
            foreach (var itemKey in entityKeys)
            {
                items.Add(GetByKey(itemKey.GuidLocation));
            }

            Delete(items);

        }
		 public void UpdateAssociation(string relation, string relationValue, string query, Guid[] ids, ContextRequest contextRequest)
        {
            var items = GetBy(query, null, null, null, null, null, contextRequest, ids);
			 var module = SFSdotNet.Framework.Cache.Caching.SystemObjects.GetModuleByKey(SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(System.Web.HttpContext.Current.Request.RequestContext, "area"));
           
            foreach (var item in items)
            {
			  Guid ? guidRelationValue = null ;
                if (!string.IsNullOrEmpty(relationValue)){
                    guidRelationValue = Guid.Parse(relationValue );
                }

				 if (relation.Contains("."))
                {
                    var partsWithOtherProp = relation.Split(char.Parse("|"));
                    var parts = partsWithOtherProp[0].Split(char.Parse("."));

                    string proxyRelName = parts[0];
                    string proxyProperty = parts[1];
                    string proxyPropertyKeyNameFromOther = partsWithOtherProp[1];
                    //string proxyPropertyThis = parts[2];

                    var prop = item.GetType().GetProperty(proxyRelName);
                    //var entityInfo = //SFSdotNet.Framework.
                    // descubrir el tipo de entidad dentro de la colección
                    Type typeEntityInList = SFSdotNet.Framework.Entities.Utils.GetTypeFromList(prop);
                    var newProxyItem = Activator.CreateInstance(typeEntityInList);
                    var propThisForSet = newProxyItem.GetType().GetProperty(proxyProperty);
                    var entityInfoOfProxy = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(typeEntityInList);
                    var propOther = newProxyItem.GetType().GetProperty(proxyPropertyKeyNameFromOther);

                    if (propThisForSet != null && entityInfoOfProxy != null && propOther != null )
                    {
                        var entityInfoThis = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(item.GetType());
                        var valueThisId = item.GetType().GetProperty(entityInfoThis.PropertyKeyName).GetValue(item);
                        if (valueThisId != null)
                            propThisForSet.SetValue(newProxyItem, valueThisId);
                        propOther.SetValue(newProxyItem, Guid.Parse(relationValue));
                        
                        var entityNameProp = newProxyItem.GetType().GetField("EntityName").GetValue(null);
                        var entitySetNameProp = newProxyItem.GetType().GetField("EntitySetName").GetValue(null);

                        SFSdotNet.Framework.Apps.Integration.CreateItemFromApp(entityNameProp.ToString(), entitySetNameProp.ToString(), module.ModuleNamespace, newProxyItem, contextRequest);

                    }

                    // crear una instancia del tipo de entidad
                    // llenar los datos y registrar nuevo


                }
                else
                {
                var prop = item.GetType().GetProperty(relation);
                var entityInfo = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(prop.PropertyType);
                if (entityInfo != null)
                {
                    var ins = Activator.CreateInstance(prop.PropertyType);
                   if (guidRelationValue != null)
                    {
                        prop.PropertyType.GetProperty(entityInfo.PropertyKeyName).SetValue(ins, guidRelationValue);
                        item.GetType().GetProperty(relation).SetValue(item, ins);
                    }
                    else
                    {
                        item.GetType().GetProperty(relation).SetValue(item, null);
                    }

                    Update(item, contextRequest);
                }

				}
            }
        }
		
	}
		public partial class YBRoutesBR:BRBase<YBRoute>{
	 	
           
		 #region Partial methods

           partial void OnUpdating(object sender, BusinessRulesEventArgs<YBRoute> e);

            partial void OnUpdated(object sender, BusinessRulesEventArgs<YBRoute> e);
			partial void OnUpdatedAgile(object sender, BusinessRulesEventArgs<YBRoute> e);

            partial void OnCreating(object sender, BusinessRulesEventArgs<YBRoute> e);
            partial void OnCreated(object sender, BusinessRulesEventArgs<YBRoute> e);

            partial void OnDeleting(object sender, BusinessRulesEventArgs<YBRoute> e);
            partial void OnDeleted(object sender, BusinessRulesEventArgs<YBRoute> e);

            partial void OnGetting(object sender, BusinessRulesEventArgs<YBRoute> e);
            protected override void OnVirtualGetting(object sender, BusinessRulesEventArgs<YBRoute> e)
            {
                OnGetting(sender, e);
            }
			protected override void OnVirtualCounting(object sender, BusinessRulesEventArgs<YBRoute> e)
            {
                OnCounting(sender, e);
            }
			partial void OnTaken(object sender, BusinessRulesEventArgs<YBRoute> e);
			protected override void OnVirtualTaken(object sender, BusinessRulesEventArgs<YBRoute> e)
            {
                OnTaken(sender, e);
            }

            partial void OnCounting(object sender, BusinessRulesEventArgs<YBRoute> e);
 
			partial void OnQuerySettings(object sender, BusinessRulesEventArgs<YBRoute> e);
          
            #endregion
			
		private static YBRoutesBR singlenton =null;
				public static YBRoutesBR NewInstance(){
					return  new YBRoutesBR();
					
				}
		public static YBRoutesBR Instance{
			get{
				if (singlenton == null)
					singlenton = new YBRoutesBR();
				return singlenton;
			}
		}
		//private bool preventSecurityRestrictions = false;
		 public bool PreventAuditTrail { get; set;  }
		#region Fields
        EFContext context = null;
        #endregion
        #region Constructor
        public YBRoutesBR()
        {
            context = new EFContext();
        }
		 public YBRoutesBR(bool preventSecurity)
            {
                this.preventSecurityRestrictions = preventSecurity;
				context = new EFContext();
            }
        #endregion
		
		#region Get

 		public IQueryable<YBRoute> Get()
        {
            using (EFContext con = new EFContext())
            {
				
				var query = con.YBRoutes.AsQueryable();
                con.Configuration.ProxyCreationEnabled = false;

                //query = ContextQueryBuilder<Nutrient>.ApplyContextQuery(query, contextRequest);

                return query;




            }

        }
		


 	
		public List<YBRoute> GetAll()
        {
            return this.GetBy(p => true);
        }
        public List<YBRoute> GetAll(string includes)
        {
            return this.GetBy(p => true, includes);
        }
        public YBRoute GetByKey(Guid guidRoute)
        {
            return GetByKey(guidRoute, true);
        }
        public YBRoute GetByKey(Guid guidRoute, bool loadIncludes)
        {
            YBRoute item = null;
			var query = PredicateBuilder.True<YBRoute>();
                    
			string strWhere = @"GuidRoute = Guid(""" + guidRoute.ToString()+@""")";
            Expression<Func<YBRoute, bool>> predicate = null;
            //if (!string.IsNullOrEmpty(strWhere))
            //    predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<YBRoute, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
			 ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
            contextRequest.CustomQuery.FilterExpressionString = strWhere;

			//item = GetBy(predicate, loadIncludes, contextRequest).FirstOrDefault();
			item = GetBy(strWhere,loadIncludes,contextRequest).FirstOrDefault();
            return item;
        }
         public List<YBRoute> GetBy(string strWhere, bool loadRelations, ContextRequest contextRequest)
        {
            if (!loadRelations)
                return GetBy(strWhere, contextRequest);
            else
                return GetBy(strWhere, contextRequest, "");

        }
		  public List<YBRoute> GetBy(string strWhere, bool loadRelations)
        {
              if (!loadRelations)
                return GetBy(strWhere, new ContextRequest());
            else
                return GetBy(strWhere, new ContextRequest(), "");

        }
		         public YBRoute GetByKey(Guid guidRoute, params Expression<Func<YBRoute, object>>[] includes)
        {
            YBRoute item = null;
			string strWhere = @"GuidRoute = Guid(""" + guidRoute.ToString()+@""")";
          Expression<Func<YBRoute, bool>> predicate = p=> p.GuidRoute == guidRoute;
           // if (!string.IsNullOrEmpty(strWhere))
           //     predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<YBRoute, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
        item = GetBy(predicate, includes).FirstOrDefault();
         ////   item = GetBy(strWhere,includes).FirstOrDefault();
			return item;

        }
        public YBRoute GetByKey(Guid guidRoute, string includes)
        {
            YBRoute item = null;
			string strWhere = @"GuidRoute = Guid(""" + guidRoute.ToString()+@""")";
            
			
            item = GetBy(strWhere, includes).FirstOrDefault();
            return item;

        }
		 public YBRoute GetByKey(Guid guidRoute, string usemode, string includes)
		{
			return GetByKey(guidRoute, usemode, null, includes);

		 }
		 public YBRoute GetByKey(Guid guidRoute, string usemode, ContextRequest context,  string includes)
        {
            YBRoute item = null;
			string strWhere = @"GuidRoute = Guid(""" + guidRoute.ToString()+@""")";
			if (context == null){
				context = new ContextRequest();
				context.CustomQuery = new CustomQuery();
				context.CustomQuery.IsByKey = true;
				context.CustomQuery.FilterExpressionString = strWhere;
				context.UseMode = usemode;
			}
            item = GetBy(strWhere,context , includes).FirstOrDefault();
            return item;

        }

        #region Dynamic Predicate
        public List<YBRoute> GetBy(Expression<Func<YBRoute, bool>> predicate, int? pageSize, int? page)
        {
            return this.GetBy(predicate, pageSize, page, null, null);
        }
        public List<YBRoute> GetBy(Expression<Func<YBRoute, bool>> predicate, ContextRequest contextRequest)
        {

            return GetBy(predicate, contextRequest,"");
        }
        
        public List<YBRoute> GetBy(Expression<Func<YBRoute, bool>> predicate, ContextRequest contextRequest, params Expression<Func<YBRoute, object>>[] includes)
        {
            StringBuilder sb = new StringBuilder();
           if (includes != null)
            {
                foreach (var path in includes)
                {

						if (sb.Length > 0) sb.Append(",");
						sb.Append(SFSdotNet.Framework.Linq.Utils.IncludeToString<YBRoute>(path));

               }
            }
            return GetBy(predicate, contextRequest, sb.ToString());
        }
        
        
        public List<YBRoute> GetBy(Expression<Func<YBRoute, bool>> predicate, string includes)
        {
			ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";

            return GetBy(predicate, context, includes);
        }

        public List<YBRoute> GetBy(Expression<Func<YBRoute, bool>> predicate, params Expression<Func<YBRoute, object>>[] includes)
        {
			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest context = new ContextRequest();
			            context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";
            return GetBy(predicate, context, includes);
        }

      
		public bool DisableCache { get; set; }
		public List<YBRoute> GetBy(Expression<Func<YBRoute, bool>> predicate, ContextRequest contextRequest, string includes)
		{
            using (EFContext con = new EFContext()) {
				
				string fkIncludes = "";
                List<string> multilangProperties = new List<string>();
				if (predicate == null) predicate = PredicateBuilder.True<YBRoute>();
                var notDeletedExpression = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;
					Expression<Func<YBRoute,bool>> multitenantExpression  = null;
					if (contextRequest != null && contextRequest.Company != null)	                        	
						multitenantExpression = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicate, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression);

#region Old code
/*
				List<YBRoute> result = null;
               BusinessRulesEventArgs<YBRoute>  e = null;
	
				OnGetting(con, e = new BusinessRulesEventArgs<YBRoute>() {  FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null) });

               // OnGetting(con,e = new BusinessRulesEventArgs<YBRoute>() { FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = contextRequest.CustomQuery.FilterExpressionString});
				   if (e != null) {
				    predicate = e.FilterExpression;
						if (e.Cancel)
						{
							context = null;
							 if (e.Items == null) e.Items = new List<YBRoute>();
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                if (predicate == null) predicate = PredicateBuilder.True<YBRoute>();
                
                //var es = _repository.Queryable;

                IQueryable<YBRoute> query =  con.YBRoutes.AsQueryable();

                                if (!string.IsNullOrEmpty(includes))
                {
                    foreach (string include in includes.Split(char.Parse(",")))
                    {
						if (!string.IsNullOrEmpty(include))
                            query = query.Include(include);
                    }
                }
                    predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
					 	if (!preventSecurityRestrictions)
						{
							if (contextRequest != null )
		                    	if (contextRequest.User !=null )
		                        	if (contextRequest.Company != null){
		                        	
										predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
 									
									}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				query =query.AsExpandable().Where(predicate);
                query = ContextQueryBuilder<YBRoute>.ApplyContextQuery(query, contextRequest);

                result = query.AsNoTracking().ToList<YBRoute>();
				  
                if (e != null)
                {
                    e.Items = result;
                }
				//if (contextRequest != null ){
				//	 contextRequest = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
					contextRequest.CustomQuery = new CustomQuery();

				//}
				OnTaken(this, e == null ? e =  new BusinessRulesEventArgs<YBRoute>() { Items= result, IncludingComputedLinq = false, ContextRequest = contextRequest,  FilterExpression = predicate } :  e);
  
			

                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
				*/
#endregion
            }
        }


		/*public int Update(List<YBRoute> items, ContextRequest contextRequest)
            {
                int result = 0;
                using (EFContext con = new EFContext())
                {
                   
                

                    foreach (var item in items)
                    {
                        //secMessageToUser messageToUser = new secMessageToUser();
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            item.GetType().GetProperty(prop).SetValue(item, item.GetType().GetProperty(prop).GetValue(item));
                        }
                        //messageToUser.GuidMessageToUser = (Guid)item.GetType().GetProperty("GuidMessageToUser").GetValue(item);

                        var setObject = con.CreateObjectSet<YBRoute>("YBRoutes");
                        //messageToUser.Readed = DateTime.UtcNow;
                        setObject.Attach(item);
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            con.ObjectStateManager.GetObjectStateEntry(item).SetModifiedProperty(prop);
                        }
                       
                    }
                    result = con.SaveChanges();

                    


                }
                return result;
            }
           */
		

        public List<YBRoute> GetBy(string predicateString, ContextRequest contextRequest, string includes)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
				


				string computedFields = "";
				string fkIncludes = "";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<YBRoute>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					//if (contextRequest != null && contextRequest.Company != null)                      	
					//	 multitenantExpression = @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))";
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicateString, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression,computedFields);


	#region Old Code
	/*
				BusinessRulesEventArgs<YBRoute> e = null;

				Filter filter = new Filter();
                if (predicateString.Contains("|"))
                {
                    string ft = GetSpecificFilter(predicateString, contextRequest);
                    if (!string.IsNullOrEmpty(ft))
                        filter.SetFilterPart("ft", ft);
                   
                    contextRequest.FreeText = predicateString.Split(char.Parse("|"))[1];
                    var q1 = predicateString.Split(char.Parse("|"))[0];
                    if (!string.IsNullOrEmpty(q1))
                    {
                        filter.ProcessText(q1);
                    }
                }
                else {
                    filter.ProcessText(predicateString);
                }
				 var includesList = (new List<string>());
                 if (!string.IsNullOrEmpty(includes))
                 {
                     includesList = includes.Split(char.Parse(",")).ToList();
                 }

				List<YBRoute> result = new List<YBRoute>();
         
			QueryBuild(predicateString, filter, con, contextRequest, "getby", includesList);
			 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }
				
				
					OnGetting(con, e == null ? e = new BusinessRulesEventArgs<YBRoute>() { Filter = filter, ContextRequest = contextRequest  } : e );

                  //OnGetting(con,e = new BusinessRulesEventArgs<YBRoute>() {  ContextRequest = contextRequest, FilterExpressionString = predicateString });
			   	if (e != null) {
				    //predicateString = e.GetQueryString();
						if (e.Cancel)
						{
							context = null;
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				//	 else {
                //      predicateString = predicateString.Replace("*extraFreeText*", "").Replace("()","");
                //  }
				//con.EnableChangeTrackingUsingProxies = false;
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                //if (predicate == null) predicate = PredicateBuilder.True<YBRoute>();
                
                //var es = _repository.Queryable;
				IQueryable<YBRoute> query = con.YBRoutes.AsQueryable();
		
				// include relations FK
				if(string.IsNullOrEmpty(includes) ){
					includes ="";
				}
				StringBuilder sbQuerySystem = new StringBuilder();
                    //predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				

				//if (!string.IsNullOrEmpty(predicateString))
                //      sbQuerySystem.Append(" And ");
                //sbQuerySystem.Append(" (IsDeleted != true Or IsDeleted = null) ");
				 filter.SetFilterPart("de", "(IsDeleted != true OR IsDeleted = null)");


					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
	                    	if (contextRequest.User !=null )
	                        	if (contextRequest.Company != null ){
	                        		//if (sbQuerySystem.Length > 0)
	                        		//	    			sbQuerySystem.Append( " And ");	
									//sbQuerySystem.Append(@" (GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa

									filter.SetFilterPart("co",@"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))");

								}
						}	
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				//string predicateString = predicate.ToDynamicLinq<YBRoute>();
				//predicateString += sbQuerySystem.ToString();
				filter.CleanAndProcess("");

				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed(); //SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicateString );               
                string predicateWithManyRelations = filter.GetFilterChildren(); //SFSdotNet.Framework.Linq.Utils.CleanPartExpression(predicateString);

                //QueryUtils.BreakeQuery1(predicateString, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
                var _queryable = query.AsQueryable();
				bool includeAll = true; 
                if (!string.IsNullOrEmpty(predicateWithManyRelations))
                    _queryable = _queryable.Where(predicateWithManyRelations, contextRequest.CustomQuery.ExtraParams);
				if (contextRequest.CustomQuery.SpecificProperties.Count > 0)
                {

				includeAll = false; 
                }

				StringBuilder sbSelect = new StringBuilder();
                sbSelect.Append("new (");
                bool existPrev = false;
                foreach (var selected in contextRequest.CustomQuery.SelectedFields.Where(p=> !string.IsNullOrEmpty(p.Linq)))
                {
                    if (existPrev) sbSelect.Append(", ");
                    if (!selected.Linq.Contains(".") && !selected.Linq.StartsWith("it."))
                        sbSelect.Append("it." + selected.Linq);
                    else
                        sbSelect.Append(selected.Linq);
                    existPrev = true;
                }
                sbSelect.Append(")");
                var queryable = _queryable.Select(sbSelect.ToString());                    


     				
                 if (!string.IsNullOrEmpty(predicateWithFKAndComputed))
                    queryable = queryable.Where(predicateWithFKAndComputed, contextRequest.CustomQuery.ExtraParams);

				QueryComplementOptions queryOps = ContextQueryBuilder.ApplyContextQuery(contextRequest);
            	if (!string.IsNullOrEmpty(queryOps.OrderByAndSort)){
					if (queryOps.OrderBy.Contains(".") && !queryOps.OrderBy.StartsWith("it.")) queryOps.OrderBy = "it." + queryOps.OrderBy;
					queryable = queryable.OrderBy(queryOps.OrderByAndSort);
					}
               	if (queryOps.Skip != null)
                {
                    queryable = queryable.Skip(queryOps.Skip.Value);
                }
                if (queryOps.PageSize != null)
                {
                    queryable = queryable.Take (queryOps.PageSize.Value);
                }


                var resultTemp = queryable.AsQueryable().ToListAsync().Result;
                foreach (var item in resultTemp)
                {

				   result.Add(SFSdotNet.Framework.BR.Utils.GetConverted<YBRoute,dynamic>(item, contextRequest.CustomQuery.SelectedFields.Select(p=>p.Name).ToArray()));
                }

			 if (e != null)
                {
                    e.Items = result;
                }
				 contextRequest.CustomQuery = new CustomQuery();
				OnTaken(this, e == null ? e = new BusinessRulesEventArgs<YBRoute>() { Items= result, IncludingComputedLinq = true, ContextRequest = contextRequest, FilterExpressionString  = predicateString } :  e);
  
			
  
                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
	
	*/
	#endregion

            }
        }
		public YBRoute GetFromOperation(string function, string filterString, string usemode, string fields, ContextRequest contextRequest)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
                string computedFields = "";
               // string fkIncludes = "accContpaqiClassification,accProjectConcept,accProjectType,accProxyUser";
                List<string> multilangProperties = new List<string>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					if (contextRequest != null && contextRequest.Company != null)
					{
						multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
						contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
					}
					 									
					string multiTenantField = "GuidCompany";


                return GetSummaryOperation(con, new YBRoute(), function, filterString, usemode, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields, contextRequest, fields.Split(char.Parse(",")).ToArray());
            }
        }

   protected override void QueryBuild(string predicate, Filter filter, DbContext efContext, ContextRequest contextRequest, string method, List<string> includesList)
      	{
				if (contextRequest.CustomQuery.SpecificProperties.Count == 0)
                {
					contextRequest.CustomQuery.SpecificProperties.Add(YBRoute.PropertyNames.Title);
					contextRequest.CustomQuery.SpecificProperties.Add(YBRoute.PropertyNames.GuidCompany);
					contextRequest.CustomQuery.SpecificProperties.Add(YBRoute.PropertyNames.CreatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(YBRoute.PropertyNames.UpdatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(YBRoute.PropertyNames.CreatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(YBRoute.PropertyNames.UpdatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(YBRoute.PropertyNames.Bytes);
					contextRequest.CustomQuery.SpecificProperties.Add(YBRoute.PropertyNames.IsDeleted);
                    
				}

				if (method == "getby" || method == "sum")
				{
					if (!contextRequest.CustomQuery.SpecificProperties.Contains("GuidRoute")){
						contextRequest.CustomQuery.SpecificProperties.Add("GuidRoute");
					}

					 if (!string.IsNullOrEmpty(contextRequest.CustomQuery.OrderBy))
					{
						string existPropertyOrderBy = contextRequest.CustomQuery.OrderBy;
						if (contextRequest.CustomQuery.OrderBy.Contains("."))
						{
							existPropertyOrderBy = contextRequest.CustomQuery.OrderBy.Split(char.Parse("."))[0];
						}
						if (!contextRequest.CustomQuery.SpecificProperties.Exists(p => p == existPropertyOrderBy))
						{
							contextRequest.CustomQuery.SpecificProperties.Add(existPropertyOrderBy);
						}
					}

				}
				
	bool isFullDetails = contextRequest.IsFromUI("YBRoutes", UIActions.GetForDetails);
	string filterForTest = predicate  + filter.GetFilterComplete();

				if (isFullDetails || !string.IsNullOrEmpty(predicate))
            {
            } 

			if (method == "sum")
            {
            } 
			if (contextRequest.CustomQuery.SelectedFields.Count == 0)
            {
				foreach (var selected in contextRequest.CustomQuery.SpecificProperties)
                {
					string linq = selected;
					switch (selected)
                    {

					 
						
					 default:
                            break;
                    }
					contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name=selected, Linq=linq});
					if (method == "getby" || method == "sum")
					{
						if (includesList.Contains(selected))
							includesList.Remove(selected);

					}

				}
			}
				if (method == "getby" || method == "sum")
				{
					foreach (var otherInclude in includesList.Where(p=> !string.IsNullOrEmpty(p)))
					{
						contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name = otherInclude, Linq = "it." + otherInclude +" as " + otherInclude });
					}
				}
				BusinessRulesEventArgs<YBRoute> e = null;
				if (contextRequest.PreventInterceptors == false)
					OnQuerySettings(efContext, e = new BusinessRulesEventArgs<YBRoute>() { Filter = filter, ContextRequest = contextRequest /*, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null)*/ });

				//List<YBRoute> result = new List<YBRoute>();
                 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }

}
		public List<YBRoute> GetBy(Expression<Func<YBRoute, bool>> predicate, bool loadRelations, ContextRequest contextRequest)
        {
			if(!loadRelations)
				return GetBy(predicate, contextRequest);
			else
				return GetBy(predicate, contextRequest, "RouteLocations,Transports");

        }

        public List<YBRoute> GetBy(Expression<Func<YBRoute, bool>> predicate, int? pageSize, int? page, string orderBy, SFSdotNet.Framework.Data.SortDirection? sortDirection)
        {
            return GetBy(predicate, new ContextRequest() { CustomQuery = new CustomQuery() { Page = page, PageSize = pageSize, OrderBy = orderBy, SortDirection = sortDirection } });
        }
        public List<YBRoute> GetBy(Expression<Func<YBRoute, bool>> predicate)
        {

			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
			contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
			            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            contextRequest.CustomQuery.FilterExpressionString = null;
            return this.GetBy(predicate, contextRequest, "");
        }
        #endregion
        #region Dynamic String
		protected override string GetSpecificFilter(string filter, ContextRequest contextRequest) {
            string result = "";
		    //string linqFilter = String.Empty;
            string freeTextFilter = String.Empty;
            if (filter.Contains("|"))
            {
               // linqFilter = filter.Split(char.Parse("|"))[0];
                freeTextFilter = filter.Split(char.Parse("|"))[1];
            }
            //else {
            //    freeTextFilter = filter;
            //}
            //else {
            //    linqFilter = filter;
            //}
			// linqFilter = SFSdotNet.Framework.Linq.Utils.ReplaceCustomDateFilters(linqFilter);
            //string specificFilter = linqFilter;
            if (!string.IsNullOrEmpty(freeTextFilter))
            {
                System.Text.StringBuilder sbCont = new System.Text.StringBuilder();
                /*if (specificFilter.Length > 0)
                {
                    sbCont.Append(" AND ");
                    sbCont.Append(" ({0})");
                }
                else
                {
                    sbCont.Append("{0}");
                }*/
                //var words = freeTextFilter.Split(char.Parse(" "));
				var word = freeTextFilter;
                System.Text.StringBuilder sbSpec = new System.Text.StringBuilder();
                 int nWords = 1;
				/*foreach (var word in words)
                {
					if (word.Length > 0){
                    if (sbSpec.Length > 0) sbSpec.Append(" AND ");
					if (words.Length > 1) sbSpec.Append("("); */
					
	
					
					
					
									
					sbSpec.Append(string.Format(@"Title.Contains(""{0}"")", word));
					

					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
								 //sbSpec.Append("*extraFreeText*");

                    /*if (words.Length > 1) sbSpec.Append(")");
					
					nWords++;

					}

                }*/
                //specificFilter = string.Format("{0}{1}", specificFilter, string.Format(sbCont.ToString(), sbSpec.ToString()));
                                 result = sbSpec.ToString();  
            }
			//result = specificFilter;
			
			return result;

		}
	
			public List<YBRoute> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params object[] extraParams)
        {
			return GetBy(filter, pageSize, page, orderBy, orderDir,  null, extraParams);
		}
           public List<YBRoute> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, string usemode, params object[] extraParams)
            { 
                return GetBy(filter, pageSize, page, orderBy, orderDir, usemode, null, extraParams);
            }


		public List<YBRoute> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  string usemode, ContextRequest context, params object[] extraParams)

        {

            // string freetext = null;
            //if (filter.Contains("|"))
            //{
            //    int parts = filter.Split(char.Parse("|")).Count();
            //    if (parts > 1)
            //    {

            //        freetext = filter.Split(char.Parse("|"))[1];
            //    }
            //}
		
            //string specificFilter = "";
            //if (!string.IsNullOrEmpty(filter))
            //  specificFilter=  GetSpecificFilter(filter);
            if (string.IsNullOrEmpty(orderBy))
            {
			                orderBy = "UpdatedDate";
            }
			//orderDir = "desc";
			SFSdotNet.Framework.Data.SortDirection direction = SFSdotNet.Framework.Data.SortDirection.Ascending;
            if (!string.IsNullOrEmpty(orderDir))
            {
                if (orderDir == "desc")
                    direction = SFSdotNet.Framework.Data.SortDirection.Descending;
            }
            if (context == null)
                context = new ContextRequest();
			

             context.UseMode = usemode;
             if (context.CustomQuery == null )
                context.CustomQuery =new SFSdotNet.Framework.My.CustomQuery();

 
                context.CustomQuery.ExtraParams = extraParams;

                    context.CustomQuery.OrderBy = orderBy;
                   context.CustomQuery.SortDirection = direction;
                   context.CustomQuery.Page = page;
                  context.CustomQuery.PageSize = pageSize;
               

            

            if (!preventSecurityRestrictions) {
			 if (context.CurrentContext == null)
                {
					if (SFSdotNet.Framework.My.Context.CurrentContext != null &&  SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
					{
						context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
						context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

					}
					else {
						throw new Exception("The security rule require a specific user and company");
					}
				}
            }
            return GetBy(filter, context);
  
        }


        public List<YBRoute> GetBy(string strWhere, ContextRequest contextRequest)
        {
        	#region old code
				
				 //Expression<Func<tvsReservationTransport, bool>> predicate = null;
				string strWhereClean = strWhere.Replace("*extraFreeText*", "").Replace("()", "");
                //if (!string.IsNullOrEmpty(strWhereClean)){

                //    object[] extraParams = null;
                //    //if (contextRequest != null )
                //    //    if (contextRequest.CustomQuery != null )
                //    //        extraParams = contextRequest.CustomQuery.ExtraParams;
                //    //predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<tvsReservationTransport, bool>(strWhereClean, extraParams != null? extraParams.Cast<Guid>(): null);				
                //}
				 if (contextRequest == null)
                {
                    contextRequest = new ContextRequest();
                    if (contextRequest.CustomQuery == null)
                        contextRequest.CustomQuery = new CustomQuery();
                }
                  if (!preventSecurityRestrictions) {
					if (contextRequest.User == null || contextRequest.Company == null)
                      {
                     if (SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
                     {
                         contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                         contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

                     }
                     else {
                         throw new Exception("The security rule require a specific User and Company ");
                     }
					 }
                 }
            contextRequest.CustomQuery.FilterExpressionString = strWhere;
				//return GetBy(predicate, contextRequest);  

			#endregion				
				
                    return GetBy(strWhere, contextRequest, "");  


        }
       public List<YBRoute> GetBy(string strWhere)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
			
            return GetBy(strWhere, context, null);
        }

        public List<YBRoute> GetBy(string strWhere, string includes)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
            return GetBy(strWhere, context, includes);
        }

        #endregion
        #endregion
		
		  #region SaveOrUpdate
        
 		 public YBRoute Create(YBRoute entity)
        {
				//ObjectContext context = null;
				    if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: Create");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

				return this.Create(entity, contextRequest);


        }
        
       
        public YBRoute Create(YBRoute entity, ContextRequest contextRequest)
        {
		
		bool graph = false;
	
				bool preventPartial = false;
                if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
               
			using (EFContext con = new EFContext()) {

				YBRoute itemForSave = new YBRoute();
#region Autos
		if(!preventSecurityRestrictions){

				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion
               BusinessRulesEventArgs<YBRoute> e = null;
			    if (preventPartial == false )
                OnCreating(this,e = new BusinessRulesEventArgs<YBRoute>() { ContextRequest = contextRequest, Item=entity });
				   if (e != null) {
						if (e.Cancel)
						{
							context = null;
							return e.Item;

						}
					}

                    if (entity.GuidRoute == Guid.Empty)
                   {
                       entity.GuidRoute = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   itemForSave.GuidRoute = entity.GuidRoute;
				  
		
			itemForSave.GuidRoute = entity.GuidRoute;

			itemForSave.Title = entity.Title;

			itemForSave.GuidCompany = entity.GuidCompany;

			itemForSave.CreatedDate = entity.CreatedDate;

			itemForSave.UpdatedDate = entity.UpdatedDate;

			itemForSave.CreatedBy = entity.CreatedBy;

			itemForSave.UpdatedBy = entity.UpdatedBy;

			itemForSave.Bytes = entity.Bytes;

			itemForSave.IsDeleted = entity.IsDeleted;

				
				con.YBRoutes.Add(itemForSave);






                
				con.ChangeTracker.Entries().Where(p => p.Entity != itemForSave && p.State != EntityState.Unchanged).ForEach(p => p.State = EntityState.Detached);

				con.Entry<YBRoute>(itemForSave).State = EntityState.Added;

				con.SaveChanges();

					 
				

				//itemResult = entity;
                //if (e != null)
                //{
                 //   e.Item = itemResult;
                //}
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false )
                OnCreated(this, e == null ? e = new BusinessRulesEventArgs<YBRoute>() { ContextRequest = contextRequest, Item = entity } : e);



                if (e != null && e.Item != null )
                {
                    return e.Item;
                }
                              return entity;
			}
            
        }
        //BusinessRulesEventArgs<YBRoute> e = null;
        public void Create(List<YBRoute> entities)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            Create(entities, contextRequest);
        }
        public void Create(List<YBRoute> entities, ContextRequest contextRequest)
        
        {
			ObjectContext context = null;
            	foreach (YBRoute entity in entities)
				{
					this.Create(entity, contextRequest);
				}
        }
		  public void CreateOrUpdateBulk(List<YBRoute> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "cu", contextRequest);
        }

        private void CreateOrUpdateBulk(List<YBRoute> entities, string actionKey, ContextRequest contextRequest)
        {
			if (entities.Count() > 0){
            bool graph = false;

            bool preventPartial = false;
            if (contextRequest != null && contextRequest.PreventInterceptors == true)
            {
                preventPartial = true;
            }
            foreach (var entity in entities)
            {
                    if (entity.GuidRoute == Guid.Empty)
                   {
                       entity.GuidRoute = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   
				  


#region Autos
		if(!preventSecurityRestrictions){


 if (actionKey != "u")
                        {
				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;


}
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion


		
			//entity.GuidRoute = entity.GuidRoute;

			//entity.Title = entity.Title;

			//entity.GuidCompany = entity.GuidCompany;

			//entity.CreatedDate = entity.CreatedDate;

			//entity.UpdatedDate = entity.UpdatedDate;

			//entity.CreatedBy = entity.CreatedBy;

			//entity.UpdatedBy = entity.UpdatedBy;

			//entity.Bytes = entity.Bytes;

			//entity.IsDeleted = entity.IsDeleted;

				
				






                
				

					 
				

				//itemResult = entity;
            }
            using (EFContext con = new EFContext())
            {
                 if (actionKey == "c")
                    {
                        context.BulkInsert(entities);
                    }else if ( actionKey == "u")
                    {
                        context.BulkUpdate(entities);
                    }else
                    {
                        context.BulkInsertOrUpdate(entities);
                    }
            }

			}
        }
	
		public void CreateBulk(List<YBRoute> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "c", contextRequest);
        }


		public void UpdateAgile(YBRoute item, params string[] fields)
         {
			UpdateAgile(item, null, fields);
        }
		public void UpdateAgile(YBRoute item, ContextRequest contextRequest, params string[] fields)
         {
            
             ContextRequest contextNew = null;
             if (contextRequest != null)
             {
                 contextNew = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
                 if (fields != null && fields.Length > 0)
                 {
                     contextNew.CustomQuery.SpecificProperties  = fields.ToList();
                 }
                 else if(contextRequest.CustomQuery.SpecificProperties.Count > 0)
                 {
                     fields = contextRequest.CustomQuery.SpecificProperties.ToArray();
                 }
             }
			

		   using (EFContext con = new EFContext())
            {



               
					List<string> propForCopy = new List<string>();
                    propForCopy.AddRange(fields);
                    
					  
					if (!propForCopy.Contains("GuidRoute"))
						propForCopy.Add("GuidRoute");

					var itemForUpdate = SFSdotNet.Framework.BR.Utils.GetConverted<YBRoute,YBRoute>(item, propForCopy.ToArray());
					 itemForUpdate.GuidRoute = item.GuidRoute;
                  var setT = con.Set<YBRoute>().Attach(itemForUpdate);

					if (fields.Count() > 0)
					  {
						  item.ModifiedProperties = fields;
					  }
                    foreach (var property in item.ModifiedProperties)
					{						
                        if (property != "GuidRoute")
                             con.Entry(setT).Property(property).IsModified = true;

                    }

                
               int result = con.SaveChanges();
               if (result != 1)
               {
                   SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: 1");
               }


            }

			OnUpdatedAgile(this, new BusinessRulesEventArgs<YBRoute>() { Item = item, ContextRequest = contextNew  });

         }
		public void UpdateBulk(List<YBRoute>  items, params string[] fields)
         {
             SFSdotNet.Framework.My.ContextRequest req = new SFSdotNet.Framework.My.ContextRequest();
             req.CustomQuery = new SFSdotNet.Framework.My.CustomQuery();
             foreach (var field in fields)
             {
                 req.CustomQuery.SpecificProperties.Add(field);
             }
             UpdateBulk(items, req);

         }

		 public void DeleteBulk(List<YBRoute> entities, ContextRequest contextRequest = null)
        {

            using (EFContext con = new EFContext())
            {
                foreach (var entity in entities)
                {
					var entityProxy = new YBRoute() { GuidRoute = entity.GuidRoute };

                    con.Entry<YBRoute>(entityProxy).State = EntityState.Deleted;

                }

                int result = con.SaveChanges();
                if (result != entities.Count)
                {
                    SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: " + entities.Count.ToString());
                }
            }

        }

        public void UpdateBulk(List<YBRoute> items, ContextRequest contextRequest)
        {
            if (items.Count() > 0){

			 foreach (var entity in items)
            {


#region Autos
		if(!preventSecurityRestrictions){

				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	



			}
#endregion







				}
				using (EFContext con = new EFContext())
				{

                    
                
                   con.BulkUpdate(items);

				}
             
			}	  
        }

         public YBRoute Update(YBRoute entity)
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return Update(entity, contextRequest);
        }
       
         public YBRoute Update(YBRoute entity, ContextRequest contextRequest)
        {
		 if ((System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null) && contextRequest == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Update");
            }
            if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            }

			
				YBRoute  itemResult = null;

	
			//entity.UpdatedDate = DateTime.Now.ToUniversalTime();
			//if(contextRequest.User != null)
				//entity.UpdatedBy = contextRequest.User.GuidUser;

//	    var oldentity = GetBy(p => p.GuidRoute == entity.GuidRoute, contextRequest).FirstOrDefault();
	//	if (oldentity != null) {
		
          //  entity.CreatedDate = oldentity.CreatedDate;
    //        entity.CreatedBy = oldentity.CreatedBy;
	
      //      entity.GuidCompany = oldentity.GuidCompany;
	
			

	
		//}

			 using( EFContext con = new EFContext()){
				BusinessRulesEventArgs<YBRoute> e = null;
				bool preventPartial = false; 
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false)
                OnUpdating(this,e = new BusinessRulesEventArgs<YBRoute>() { ContextRequest = contextRequest, Item=entity});
				   if (e != null) {
						if (e.Cancel)
						{
							//outcontext = null;
							return e.Item;

						}
					}

	string includes = "";
	IQueryable < YBRoute > query = con.YBRoutes.AsQueryable();
	foreach (string include in includes.Split(char.Parse(",")))
                       {
                           if (!string.IsNullOrEmpty(include))
                               query = query.Include(include);
                       }
	var oldentity = query.FirstOrDefault(p => p.GuidRoute == entity.GuidRoute);
	if (oldentity.Title != entity.Title)
		oldentity.Title = entity.Title;

				//if (entity.UpdatedDate == null || (contextRequest != null && contextRequest.IsFromUI("YBRoutes", UIActions.Updating)))
			oldentity.UpdatedDate = DateTime.Now.ToUniversalTime();
			if(contextRequest.User != null)
				oldentity.UpdatedBy = contextRequest.User.GuidUser;

           


				if (entity.RouteLocations != null)
                {
                    foreach (var item in entity.RouteLocations)
                    {


                        
                    }
					
                    

                }


				if (entity.Transports != null)
                {
                    foreach (var item in entity.Transports)
                    {


                        
                    }
					
                    

                }


				con.ChangeTracker.Entries().Where(p => p.Entity != oldentity).ForEach(p => p.State = EntityState.Unchanged);  
				  
				con.SaveChanges();
        
					 
					
               
				itemResult = entity;
				if(preventPartial == false)
					OnUpdated(this, e = new BusinessRulesEventArgs<YBRoute>() { ContextRequest = contextRequest, Item=itemResult });

              	return itemResult;
			}
			  
        }
        public YBRoute Save(YBRoute entity)
        {
			return Create(entity);
        }
        public int Save(List<YBRoute> entities)
        {
			 Create(entities);
            return entities.Count;

        }
        #endregion
        #region Delete
        public void Delete(YBRoute entity)
        {
				this.Delete(entity, null);
			
        }
		 public void Delete(YBRoute entity, ContextRequest contextRequest)
        {
				
				  List<YBRoute> entities = new List<YBRoute>();
				   entities.Add(entity);
				this.Delete(entities, contextRequest);
			
        }

         public void Delete(string query, Guid[] guids, ContextRequest contextRequest)
        {
			var br = new YBRoutesBR(true);
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);
            
            Delete(items, contextRequest);

        }
        public void Delete(YBRoute entity,  ContextRequest contextRequest, BusinessRulesEventArgs<YBRoute> e = null)
        {
			
				using(EFContext con = new EFContext())
                 {
				
               	BusinessRulesEventArgs<YBRoute> _e = null;
               List<YBRoute> _items = new List<YBRoute>();
                _items.Add(entity);
                if (e == null || e.PreventPartialPropagate == false)
                {
                    OnDeleting(this, _e = (e == null ? new BusinessRulesEventArgs<YBRoute>() { ContextRequest = contextRequest, Item = entity, Items = null  } : e));
                }
                if (_e != null)
                {
                    if (_e.Cancel)
						{
							context = null;
							return;

						}
					}


				
									//IsDeleted
					bool logicDelete = true;
					if (entity.IsDeleted != null)
					{
						if (entity.IsDeleted.Value)
							logicDelete = false;
					}
					if (logicDelete)
					{
											//entity = GetBy(p =>, contextRequest).FirstOrDefault();
						entity.IsDeleted = true;
						if (contextRequest != null && contextRequest.User != null)
							entity.UpdatedBy = contextRequest.User.GuidUser;
                        entity.UpdatedDate = DateTime.UtcNow;
						UpdateAgile(entity, "IsDeleted","UpdatedBy","UpdatedDate");

						
					}
					else {
					con.Entry<YBRoute>(entity).State = EntityState.Deleted;
					con.SaveChanges();
				
				 
					}
								
				
				 
					
					
			if (e == null || e.PreventPartialPropagate == false)
                {

                    if (_e == null)
                        _e = new BusinessRulesEventArgs<YBRoute>() { ContextRequest = contextRequest, Item = entity, Items = null };

                    OnDeleted(this, _e);
                }

				//return null;
			}
        }
 public void UnDelete(string query, Guid[] guids, ContextRequest contextRequest)
        {
            var br = new YBRoutesBR(true);
            contextRequest.CustomQuery.IncludeDeleted = true;
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);

            foreach (var item in items)
            {
                item.IsDeleted = false;
						if (contextRequest != null && contextRequest.User != null)
							item.UpdatedBy = contextRequest.User.GuidUser;
                        item.UpdatedDate = DateTime.UtcNow;
            }

            UpdateBulk(items, "IsDeleted","UpdatedBy","UpdatedDate");
        }

         public void Delete(List<YBRoute> entities,  ContextRequest contextRequest = null )
        {
				
			 BusinessRulesEventArgs<YBRoute> _e = null;

                OnDeleting(this, _e = new BusinessRulesEventArgs<YBRoute>() { ContextRequest = contextRequest, Item = null, Items = entities });
                if (_e != null)
                {
                    if (_e.Cancel)
                    {
                        context = null;
                        return;

                    }
                }
                bool allSucced = true;
                BusinessRulesEventArgs<YBRoute> eToChilds = new BusinessRulesEventArgs<YBRoute>();
                if (_e != null)
                {
                    eToChilds = _e;
                }
                else
                {
                    eToChilds = new BusinessRulesEventArgs<YBRoute>() { ContextRequest = contextRequest, Item = (entities.Count == 1 ? entities[0] : null), Items = entities };
                }
				foreach (YBRoute item in entities)
				{
					try
                    {
                        this.Delete(item, contextRequest, e: eToChilds);
                    }
                    catch (Exception ex)
                    {
                        SFSdotNet.Framework.My.EventLog.Error(ex);
                        allSucced = false;
                    }
				}
				if (_e == null)
                    _e = new BusinessRulesEventArgs<YBRoute>() { ContextRequest = contextRequest, CountResult = entities.Count, Item = null, Items = entities };
                OnDeleted(this, _e);

			
        }
        #endregion
 
        #region GetCount
		 public int GetCount(Expression<Func<YBRoute, bool>> predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

			return GetCount(predicate, contextRequest);
		}
        public int GetCount(Expression<Func<YBRoute, bool>> predicate, ContextRequest contextRequest)
        {


		
		 using (EFContext con = new EFContext())
            {


				if (predicate == null) predicate = PredicateBuilder.True<YBRoute>();
           		predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted == null);
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    		if (contextRequest.User !=null )
                        		if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
									predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa

								}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				
				IQueryable<YBRoute> query = con.YBRoutes.AsQueryable();
                return query.AsExpandable().Count(predicate);

			
				}
			

        }
		  public int GetCount(string predicate,  ContextRequest contextRequest)
         {
             return GetCount(predicate, null, contextRequest);
         }

         public int GetCount(string predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return GetCount(predicate, contextRequest);
        }
		 public int GetCount(string predicate, string usemode){
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
				return GetCount( predicate,  usemode,  contextRequest);
		 }
        public int GetCount(string predicate, string usemode, ContextRequest contextRequest){

		using (EFContext con = new EFContext()) {
				string computedFields = "";
				string fkIncludes = "";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<YBRoute>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetCount(con, predicate, usemode, contextRequest, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields);

			}
			#region old code
			 /* string freetext = null;
            Filter filter = new Filter();

              if (predicate.Contains("|"))
              {
                 
                  filter.SetFilterPart("ft", GetSpecificFilter(predicate, contextRequest));
                 
                  filter.ProcessText(predicate.Split(char.Parse("|"))[0]);
                  freetext = predicate.Split(char.Parse("|"))[1];

				  if (!string.IsNullOrEmpty(freetext) && string.IsNullOrEmpty(contextRequest.FreeText))
                  {
                      contextRequest.FreeText = freetext;
                  }
              }
              else {
                  filter.ProcessText(predicate);
              }
			   predicate = filter.GetFilterComplete();
			// BusinessRulesEventArgs<YBRoute>  e = null;
           	using (EFContext con = new EFContext())
			{
			
			

			 QueryBuild(predicate, filter, con, contextRequest, "count", new List<string>());


			
			BusinessRulesEventArgs<YBRoute> e = null;

			contextRequest.FreeText = freetext;
			contextRequest.UseMode = usemode;
            OnCounting(this, e = new BusinessRulesEventArgs<YBRoute>() {  Filter =filter, ContextRequest = contextRequest });
            if (e != null)
            {
                if (e.Cancel)
                {
                    context = null;
                    return e.CountResult;

                }

            

            }
			
			StringBuilder sbQuerySystem = new StringBuilder();
		
					
                    filter.SetFilterPart("de","(IsDeleted != true OR IsDeleted == null)");
			
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    	if (contextRequest.User !=null )
                        	if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
                        		
								filter.SetFilterPart("co", @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa
						
						
							}
							
							}
							if (preventSecurityRestrictions) preventSecurityRestrictions= false;
		
				   
                 filter.CleanAndProcess("");
				//string predicateWithFKAndComputed = SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicate );               
				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed();
               string predicateWithManyRelations = filter.GetFilterChildren();
			   ///QueryUtils.BreakeQuery1(predicate, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
			   predicate = filter.GetFilterComplete();
               if (!string.IsNullOrEmpty(predicate))
               {
				
					
                    return con.YBRoutes.Where(predicate).Count();
					
                }else
                    return con.YBRoutes.Count();
					
			}*/
			#endregion

		}
         public int GetCount()
        {
            return GetCount(p => true);
        }
        #endregion
        
         


        public void Delete(List<YBRoute.CompositeKey> entityKeys)
        {

            List<YBRoute> items = new List<YBRoute>();
            foreach (var itemKey in entityKeys)
            {
                items.Add(GetByKey(itemKey.GuidRoute));
            }

            Delete(items);

        }
		 public void UpdateAssociation(string relation, string relationValue, string query, Guid[] ids, ContextRequest contextRequest)
        {
            var items = GetBy(query, null, null, null, null, null, contextRequest, ids);
			 var module = SFSdotNet.Framework.Cache.Caching.SystemObjects.GetModuleByKey(SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(System.Web.HttpContext.Current.Request.RequestContext, "area"));
           
            foreach (var item in items)
            {
			  Guid ? guidRelationValue = null ;
                if (!string.IsNullOrEmpty(relationValue)){
                    guidRelationValue = Guid.Parse(relationValue );
                }

				 if (relation.Contains("."))
                {
                    var partsWithOtherProp = relation.Split(char.Parse("|"));
                    var parts = partsWithOtherProp[0].Split(char.Parse("."));

                    string proxyRelName = parts[0];
                    string proxyProperty = parts[1];
                    string proxyPropertyKeyNameFromOther = partsWithOtherProp[1];
                    //string proxyPropertyThis = parts[2];

                    var prop = item.GetType().GetProperty(proxyRelName);
                    //var entityInfo = //SFSdotNet.Framework.
                    // descubrir el tipo de entidad dentro de la colección
                    Type typeEntityInList = SFSdotNet.Framework.Entities.Utils.GetTypeFromList(prop);
                    var newProxyItem = Activator.CreateInstance(typeEntityInList);
                    var propThisForSet = newProxyItem.GetType().GetProperty(proxyProperty);
                    var entityInfoOfProxy = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(typeEntityInList);
                    var propOther = newProxyItem.GetType().GetProperty(proxyPropertyKeyNameFromOther);

                    if (propThisForSet != null && entityInfoOfProxy != null && propOther != null )
                    {
                        var entityInfoThis = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(item.GetType());
                        var valueThisId = item.GetType().GetProperty(entityInfoThis.PropertyKeyName).GetValue(item);
                        if (valueThisId != null)
                            propThisForSet.SetValue(newProxyItem, valueThisId);
                        propOther.SetValue(newProxyItem, Guid.Parse(relationValue));
                        
                        var entityNameProp = newProxyItem.GetType().GetField("EntityName").GetValue(null);
                        var entitySetNameProp = newProxyItem.GetType().GetField("EntitySetName").GetValue(null);

                        SFSdotNet.Framework.Apps.Integration.CreateItemFromApp(entityNameProp.ToString(), entitySetNameProp.ToString(), module.ModuleNamespace, newProxyItem, contextRequest);

                    }

                    // crear una instancia del tipo de entidad
                    // llenar los datos y registrar nuevo


                }
                else
                {
                var prop = item.GetType().GetProperty(relation);
                var entityInfo = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(prop.PropertyType);
                if (entityInfo != null)
                {
                    var ins = Activator.CreateInstance(prop.PropertyType);
                   if (guidRelationValue != null)
                    {
                        prop.PropertyType.GetProperty(entityInfo.PropertyKeyName).SetValue(ins, guidRelationValue);
                        item.GetType().GetProperty(relation).SetValue(item, ins);
                    }
                    else
                    {
                        item.GetType().GetProperty(relation).SetValue(item, null);
                    }

                    Update(item, contextRequest);
                }

				}
            }
        }
		
	}
		public partial class StudentParentsBR:BRBase<StudentParent>{
	 	
           
		 #region Partial methods

           partial void OnUpdating(object sender, BusinessRulesEventArgs<StudentParent> e);

            partial void OnUpdated(object sender, BusinessRulesEventArgs<StudentParent> e);
			partial void OnUpdatedAgile(object sender, BusinessRulesEventArgs<StudentParent> e);

            partial void OnCreating(object sender, BusinessRulesEventArgs<StudentParent> e);
            partial void OnCreated(object sender, BusinessRulesEventArgs<StudentParent> e);

            partial void OnDeleting(object sender, BusinessRulesEventArgs<StudentParent> e);
            partial void OnDeleted(object sender, BusinessRulesEventArgs<StudentParent> e);

            partial void OnGetting(object sender, BusinessRulesEventArgs<StudentParent> e);
            protected override void OnVirtualGetting(object sender, BusinessRulesEventArgs<StudentParent> e)
            {
                OnGetting(sender, e);
            }
			protected override void OnVirtualCounting(object sender, BusinessRulesEventArgs<StudentParent> e)
            {
                OnCounting(sender, e);
            }
			partial void OnTaken(object sender, BusinessRulesEventArgs<StudentParent> e);
			protected override void OnVirtualTaken(object sender, BusinessRulesEventArgs<StudentParent> e)
            {
                OnTaken(sender, e);
            }

            partial void OnCounting(object sender, BusinessRulesEventArgs<StudentParent> e);
 
			partial void OnQuerySettings(object sender, BusinessRulesEventArgs<StudentParent> e);
          
            #endregion
			
		private static StudentParentsBR singlenton =null;
				public static StudentParentsBR NewInstance(){
					return  new StudentParentsBR();
					
				}
		public static StudentParentsBR Instance{
			get{
				if (singlenton == null)
					singlenton = new StudentParentsBR();
				return singlenton;
			}
		}
		//private bool preventSecurityRestrictions = false;
		 public bool PreventAuditTrail { get; set;  }
		#region Fields
        EFContext context = null;
        #endregion
        #region Constructor
        public StudentParentsBR()
        {
            context = new EFContext();
        }
		 public StudentParentsBR(bool preventSecurity)
            {
                this.preventSecurityRestrictions = preventSecurity;
				context = new EFContext();
            }
        #endregion
		
		#region Get

 		public IQueryable<StudentParent> Get()
        {
            using (EFContext con = new EFContext())
            {
				
				var query = con.StudentParents.AsQueryable();
                con.Configuration.ProxyCreationEnabled = false;

                //query = ContextQueryBuilder<Nutrient>.ApplyContextQuery(query, contextRequest);

                return query;




            }

        }
		


 	
		public List<StudentParent> GetAll()
        {
            return this.GetBy(p => true);
        }
        public List<StudentParent> GetAll(string includes)
        {
            return this.GetBy(p => true, includes);
        }
        public StudentParent GetByKey(Guid guidStudentParent)
        {
            return GetByKey(guidStudentParent, true);
        }
        public StudentParent GetByKey(Guid guidStudentParent, bool loadIncludes)
        {
            StudentParent item = null;
			var query = PredicateBuilder.True<StudentParent>();
                    
			string strWhere = @"GuidStudentParent = Guid(""" + guidStudentParent.ToString()+@""")";
            Expression<Func<StudentParent, bool>> predicate = null;
            //if (!string.IsNullOrEmpty(strWhere))
            //    predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<StudentParent, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
			 ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
            contextRequest.CustomQuery.FilterExpressionString = strWhere;

			//item = GetBy(predicate, loadIncludes, contextRequest).FirstOrDefault();
			item = GetBy(strWhere,loadIncludes,contextRequest).FirstOrDefault();
            return item;
        }
         public List<StudentParent> GetBy(string strWhere, bool loadRelations, ContextRequest contextRequest)
        {
            if (!loadRelations)
                return GetBy(strWhere, contextRequest);
            else
                return GetBy(strWhere, contextRequest, "");

        }
		  public List<StudentParent> GetBy(string strWhere, bool loadRelations)
        {
              if (!loadRelations)
                return GetBy(strWhere, new ContextRequest());
            else
                return GetBy(strWhere, new ContextRequest(), "");

        }
		         public StudentParent GetByKey(Guid guidStudentParent, params Expression<Func<StudentParent, object>>[] includes)
        {
            StudentParent item = null;
			string strWhere = @"GuidStudentParent = Guid(""" + guidStudentParent.ToString()+@""")";
          Expression<Func<StudentParent, bool>> predicate = p=> p.GuidStudentParent == guidStudentParent;
           // if (!string.IsNullOrEmpty(strWhere))
           //     predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<StudentParent, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
        item = GetBy(predicate, includes).FirstOrDefault();
         ////   item = GetBy(strWhere,includes).FirstOrDefault();
			return item;

        }
        public StudentParent GetByKey(Guid guidStudentParent, string includes)
        {
            StudentParent item = null;
			string strWhere = @"GuidStudentParent = Guid(""" + guidStudentParent.ToString()+@""")";
            
			
            item = GetBy(strWhere, includes).FirstOrDefault();
            return item;

        }
		 public StudentParent GetByKey(Guid guidStudentParent, string usemode, string includes)
		{
			return GetByKey(guidStudentParent, usemode, null, includes);

		 }
		 public StudentParent GetByKey(Guid guidStudentParent, string usemode, ContextRequest context,  string includes)
        {
            StudentParent item = null;
			string strWhere = @"GuidStudentParent = Guid(""" + guidStudentParent.ToString()+@""")";
			if (context == null){
				context = new ContextRequest();
				context.CustomQuery = new CustomQuery();
				context.CustomQuery.IsByKey = true;
				context.CustomQuery.FilterExpressionString = strWhere;
				context.UseMode = usemode;
			}
            item = GetBy(strWhere,context , includes).FirstOrDefault();
            return item;

        }

        #region Dynamic Predicate
        public List<StudentParent> GetBy(Expression<Func<StudentParent, bool>> predicate, int? pageSize, int? page)
        {
            return this.GetBy(predicate, pageSize, page, null, null);
        }
        public List<StudentParent> GetBy(Expression<Func<StudentParent, bool>> predicate, ContextRequest contextRequest)
        {

            return GetBy(predicate, contextRequest,"");
        }
        
        public List<StudentParent> GetBy(Expression<Func<StudentParent, bool>> predicate, ContextRequest contextRequest, params Expression<Func<StudentParent, object>>[] includes)
        {
            StringBuilder sb = new StringBuilder();
           if (includes != null)
            {
                foreach (var path in includes)
                {

						if (sb.Length > 0) sb.Append(",");
						sb.Append(SFSdotNet.Framework.Linq.Utils.IncludeToString<StudentParent>(path));

               }
            }
            return GetBy(predicate, contextRequest, sb.ToString());
        }
        
        
        public List<StudentParent> GetBy(Expression<Func<StudentParent, bool>> predicate, string includes)
        {
			ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";

            return GetBy(predicate, context, includes);
        }

        public List<StudentParent> GetBy(Expression<Func<StudentParent, bool>> predicate, params Expression<Func<StudentParent, object>>[] includes)
        {
			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest context = new ContextRequest();
			            context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";
            return GetBy(predicate, context, includes);
        }

      
		public bool DisableCache { get; set; }
		public List<StudentParent> GetBy(Expression<Func<StudentParent, bool>> predicate, ContextRequest contextRequest, string includes)
		{
            using (EFContext con = new EFContext()) {
				
				string fkIncludes = "";
                List<string> multilangProperties = new List<string>();
				if (predicate == null) predicate = PredicateBuilder.True<StudentParent>();
                var notDeletedExpression = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;
					Expression<Func<StudentParent,bool>> multitenantExpression  = null;
					if (contextRequest != null && contextRequest.Company != null)	                        	
						multitenantExpression = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicate, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression);

#region Old code
/*
				List<StudentParent> result = null;
               BusinessRulesEventArgs<StudentParent>  e = null;
	
				OnGetting(con, e = new BusinessRulesEventArgs<StudentParent>() {  FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null) });

               // OnGetting(con,e = new BusinessRulesEventArgs<StudentParent>() { FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = contextRequest.CustomQuery.FilterExpressionString});
				   if (e != null) {
				    predicate = e.FilterExpression;
						if (e.Cancel)
						{
							context = null;
							 if (e.Items == null) e.Items = new List<StudentParent>();
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                if (predicate == null) predicate = PredicateBuilder.True<StudentParent>();
                
                //var es = _repository.Queryable;

                IQueryable<StudentParent> query =  con.StudentParents.AsQueryable();

                                if (!string.IsNullOrEmpty(includes))
                {
                    foreach (string include in includes.Split(char.Parse(",")))
                    {
						if (!string.IsNullOrEmpty(include))
                            query = query.Include(include);
                    }
                }
                    predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
					 	if (!preventSecurityRestrictions)
						{
							if (contextRequest != null )
		                    	if (contextRequest.User !=null )
		                        	if (contextRequest.Company != null){
		                        	
										predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
 									
									}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				query =query.AsExpandable().Where(predicate);
                query = ContextQueryBuilder<StudentParent>.ApplyContextQuery(query, contextRequest);

                result = query.AsNoTracking().ToList<StudentParent>();
				  
                if (e != null)
                {
                    e.Items = result;
                }
				//if (contextRequest != null ){
				//	 contextRequest = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
					contextRequest.CustomQuery = new CustomQuery();

				//}
				OnTaken(this, e == null ? e =  new BusinessRulesEventArgs<StudentParent>() { Items= result, IncludingComputedLinq = false, ContextRequest = contextRequest,  FilterExpression = predicate } :  e);
  
			

                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
				*/
#endregion
            }
        }


		/*public int Update(List<StudentParent> items, ContextRequest contextRequest)
            {
                int result = 0;
                using (EFContext con = new EFContext())
                {
                   
                

                    foreach (var item in items)
                    {
                        //secMessageToUser messageToUser = new secMessageToUser();
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            item.GetType().GetProperty(prop).SetValue(item, item.GetType().GetProperty(prop).GetValue(item));
                        }
                        //messageToUser.GuidMessageToUser = (Guid)item.GetType().GetProperty("GuidMessageToUser").GetValue(item);

                        var setObject = con.CreateObjectSet<StudentParent>("StudentParents");
                        //messageToUser.Readed = DateTime.UtcNow;
                        setObject.Attach(item);
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            con.ObjectStateManager.GetObjectStateEntry(item).SetModifiedProperty(prop);
                        }
                       
                    }
                    result = con.SaveChanges();

                    


                }
                return result;
            }
           */
		

        public List<StudentParent> GetBy(string predicateString, ContextRequest contextRequest, string includes)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
				


				string computedFields = "";
				string fkIncludes = "";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<StudentParent>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					//if (contextRequest != null && contextRequest.Company != null)                      	
					//	 multitenantExpression = @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))";
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicateString, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression,computedFields);


	#region Old Code
	/*
				BusinessRulesEventArgs<StudentParent> e = null;

				Filter filter = new Filter();
                if (predicateString.Contains("|"))
                {
                    string ft = GetSpecificFilter(predicateString, contextRequest);
                    if (!string.IsNullOrEmpty(ft))
                        filter.SetFilterPart("ft", ft);
                   
                    contextRequest.FreeText = predicateString.Split(char.Parse("|"))[1];
                    var q1 = predicateString.Split(char.Parse("|"))[0];
                    if (!string.IsNullOrEmpty(q1))
                    {
                        filter.ProcessText(q1);
                    }
                }
                else {
                    filter.ProcessText(predicateString);
                }
				 var includesList = (new List<string>());
                 if (!string.IsNullOrEmpty(includes))
                 {
                     includesList = includes.Split(char.Parse(",")).ToList();
                 }

				List<StudentParent> result = new List<StudentParent>();
         
			QueryBuild(predicateString, filter, con, contextRequest, "getby", includesList);
			 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }
				
				
					OnGetting(con, e == null ? e = new BusinessRulesEventArgs<StudentParent>() { Filter = filter, ContextRequest = contextRequest  } : e );

                  //OnGetting(con,e = new BusinessRulesEventArgs<StudentParent>() {  ContextRequest = contextRequest, FilterExpressionString = predicateString });
			   	if (e != null) {
				    //predicateString = e.GetQueryString();
						if (e.Cancel)
						{
							context = null;
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				//	 else {
                //      predicateString = predicateString.Replace("*extraFreeText*", "").Replace("()","");
                //  }
				//con.EnableChangeTrackingUsingProxies = false;
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                //if (predicate == null) predicate = PredicateBuilder.True<StudentParent>();
                
                //var es = _repository.Queryable;
				IQueryable<StudentParent> query = con.StudentParents.AsQueryable();
		
				// include relations FK
				if(string.IsNullOrEmpty(includes) ){
					includes ="";
				}
				StringBuilder sbQuerySystem = new StringBuilder();
                    //predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				

				//if (!string.IsNullOrEmpty(predicateString))
                //      sbQuerySystem.Append(" And ");
                //sbQuerySystem.Append(" (IsDeleted != true Or IsDeleted = null) ");
				 filter.SetFilterPart("de", "(IsDeleted != true OR IsDeleted = null)");


					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
	                    	if (contextRequest.User !=null )
	                        	if (contextRequest.Company != null ){
	                        		//if (sbQuerySystem.Length > 0)
	                        		//	    			sbQuerySystem.Append( " And ");	
									//sbQuerySystem.Append(@" (GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa

									filter.SetFilterPart("co",@"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))");

								}
						}	
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				//string predicateString = predicate.ToDynamicLinq<StudentParent>();
				//predicateString += sbQuerySystem.ToString();
				filter.CleanAndProcess("");

				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed(); //SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicateString );               
                string predicateWithManyRelations = filter.GetFilterChildren(); //SFSdotNet.Framework.Linq.Utils.CleanPartExpression(predicateString);

                //QueryUtils.BreakeQuery1(predicateString, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
                var _queryable = query.AsQueryable();
				bool includeAll = true; 
                if (!string.IsNullOrEmpty(predicateWithManyRelations))
                    _queryable = _queryable.Where(predicateWithManyRelations, contextRequest.CustomQuery.ExtraParams);
				if (contextRequest.CustomQuery.SpecificProperties.Count > 0)
                {

				includeAll = false; 
                }

				StringBuilder sbSelect = new StringBuilder();
                sbSelect.Append("new (");
                bool existPrev = false;
                foreach (var selected in contextRequest.CustomQuery.SelectedFields.Where(p=> !string.IsNullOrEmpty(p.Linq)))
                {
                    if (existPrev) sbSelect.Append(", ");
                    if (!selected.Linq.Contains(".") && !selected.Linq.StartsWith("it."))
                        sbSelect.Append("it." + selected.Linq);
                    else
                        sbSelect.Append(selected.Linq);
                    existPrev = true;
                }
                sbSelect.Append(")");
                var queryable = _queryable.Select(sbSelect.ToString());                    


     				
                 if (!string.IsNullOrEmpty(predicateWithFKAndComputed))
                    queryable = queryable.Where(predicateWithFKAndComputed, contextRequest.CustomQuery.ExtraParams);

				QueryComplementOptions queryOps = ContextQueryBuilder.ApplyContextQuery(contextRequest);
            	if (!string.IsNullOrEmpty(queryOps.OrderByAndSort)){
					if (queryOps.OrderBy.Contains(".") && !queryOps.OrderBy.StartsWith("it.")) queryOps.OrderBy = "it." + queryOps.OrderBy;
					queryable = queryable.OrderBy(queryOps.OrderByAndSort);
					}
               	if (queryOps.Skip != null)
                {
                    queryable = queryable.Skip(queryOps.Skip.Value);
                }
                if (queryOps.PageSize != null)
                {
                    queryable = queryable.Take (queryOps.PageSize.Value);
                }


                var resultTemp = queryable.AsQueryable().ToListAsync().Result;
                foreach (var item in resultTemp)
                {

				   result.Add(SFSdotNet.Framework.BR.Utils.GetConverted<StudentParent,dynamic>(item, contextRequest.CustomQuery.SelectedFields.Select(p=>p.Name).ToArray()));
                }

			 if (e != null)
                {
                    e.Items = result;
                }
				 contextRequest.CustomQuery = new CustomQuery();
				OnTaken(this, e == null ? e = new BusinessRulesEventArgs<StudentParent>() { Items= result, IncludingComputedLinq = true, ContextRequest = contextRequest, FilterExpressionString  = predicateString } :  e);
  
			
  
                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
	
	*/
	#endregion

            }
        }
		public StudentParent GetFromOperation(string function, string filterString, string usemode, string fields, ContextRequest contextRequest)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
                string computedFields = "";
               // string fkIncludes = "accContpaqiClassification,accProjectConcept,accProjectType,accProxyUser";
                List<string> multilangProperties = new List<string>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					if (contextRequest != null && contextRequest.Company != null)
					{
						multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
						contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
					}
					 									
					string multiTenantField = "GuidCompany";


                return GetSummaryOperation(con, new StudentParent(), function, filterString, usemode, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields, contextRequest, fields.Split(char.Parse(",")).ToArray());
            }
        }

   protected override void QueryBuild(string predicate, Filter filter, DbContext efContext, ContextRequest contextRequest, string method, List<string> includesList)
      	{
				if (contextRequest.CustomQuery.SpecificProperties.Count == 0)
                {
					contextRequest.CustomQuery.SpecificProperties.Add(StudentParent.PropertyNames.FullName);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentParent.PropertyNames.GuidCompany);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentParent.PropertyNames.CreatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentParent.PropertyNames.UpdatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentParent.PropertyNames.CreatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentParent.PropertyNames.UpdatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentParent.PropertyNames.Bytes);
					contextRequest.CustomQuery.SpecificProperties.Add(StudentParent.PropertyNames.IsDeleted);
                    
				}

				if (method == "getby" || method == "sum")
				{
					if (!contextRequest.CustomQuery.SpecificProperties.Contains("GuidStudentParent")){
						contextRequest.CustomQuery.SpecificProperties.Add("GuidStudentParent");
					}

					 if (!string.IsNullOrEmpty(contextRequest.CustomQuery.OrderBy))
					{
						string existPropertyOrderBy = contextRequest.CustomQuery.OrderBy;
						if (contextRequest.CustomQuery.OrderBy.Contains("."))
						{
							existPropertyOrderBy = contextRequest.CustomQuery.OrderBy.Split(char.Parse("."))[0];
						}
						if (!contextRequest.CustomQuery.SpecificProperties.Exists(p => p == existPropertyOrderBy))
						{
							contextRequest.CustomQuery.SpecificProperties.Add(existPropertyOrderBy);
						}
					}

				}
				
	bool isFullDetails = contextRequest.IsFromUI("StudentParents", UIActions.GetForDetails);
	string filterForTest = predicate  + filter.GetFilterComplete();

				if (isFullDetails || !string.IsNullOrEmpty(predicate))
            {
            } 

			if (method == "sum")
            {
            } 
			if (contextRequest.CustomQuery.SelectedFields.Count == 0)
            {
				foreach (var selected in contextRequest.CustomQuery.SpecificProperties)
                {
					string linq = selected;
					switch (selected)
                    {

					 
						
					 default:
                            break;
                    }
					contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name=selected, Linq=linq});
					if (method == "getby" || method == "sum")
					{
						if (includesList.Contains(selected))
							includesList.Remove(selected);

					}

				}
			}
				if (method == "getby" || method == "sum")
				{
					foreach (var otherInclude in includesList.Where(p=> !string.IsNullOrEmpty(p)))
					{
						contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name = otherInclude, Linq = "it." + otherInclude +" as " + otherInclude });
					}
				}
				BusinessRulesEventArgs<StudentParent> e = null;
				if (contextRequest.PreventInterceptors == false)
					OnQuerySettings(efContext, e = new BusinessRulesEventArgs<StudentParent>() { Filter = filter, ContextRequest = contextRequest /*, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null)*/ });

				//List<StudentParent> result = new List<StudentParent>();
                 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }

}
		public List<StudentParent> GetBy(Expression<Func<StudentParent, bool>> predicate, bool loadRelations, ContextRequest contextRequest)
        {
			if(!loadRelations)
				return GetBy(predicate, contextRequest);
			else
				return GetBy(predicate, contextRequest, "Students");

        }

        public List<StudentParent> GetBy(Expression<Func<StudentParent, bool>> predicate, int? pageSize, int? page, string orderBy, SFSdotNet.Framework.Data.SortDirection? sortDirection)
        {
            return GetBy(predicate, new ContextRequest() { CustomQuery = new CustomQuery() { Page = page, PageSize = pageSize, OrderBy = orderBy, SortDirection = sortDirection } });
        }
        public List<StudentParent> GetBy(Expression<Func<StudentParent, bool>> predicate)
        {

			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
			contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
			            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            contextRequest.CustomQuery.FilterExpressionString = null;
            return this.GetBy(predicate, contextRequest, "");
        }
        #endregion
        #region Dynamic String
		protected override string GetSpecificFilter(string filter, ContextRequest contextRequest) {
            string result = "";
		    //string linqFilter = String.Empty;
            string freeTextFilter = String.Empty;
            if (filter.Contains("|"))
            {
               // linqFilter = filter.Split(char.Parse("|"))[0];
                freeTextFilter = filter.Split(char.Parse("|"))[1];
            }
            //else {
            //    freeTextFilter = filter;
            //}
            //else {
            //    linqFilter = filter;
            //}
			// linqFilter = SFSdotNet.Framework.Linq.Utils.ReplaceCustomDateFilters(linqFilter);
            //string specificFilter = linqFilter;
            if (!string.IsNullOrEmpty(freeTextFilter))
            {
                System.Text.StringBuilder sbCont = new System.Text.StringBuilder();
                /*if (specificFilter.Length > 0)
                {
                    sbCont.Append(" AND ");
                    sbCont.Append(" ({0})");
                }
                else
                {
                    sbCont.Append("{0}");
                }*/
                //var words = freeTextFilter.Split(char.Parse(" "));
				var word = freeTextFilter;
                System.Text.StringBuilder sbSpec = new System.Text.StringBuilder();
                 int nWords = 1;
				/*foreach (var word in words)
                {
					if (word.Length > 0){
                    if (sbSpec.Length > 0) sbSpec.Append(" AND ");
					if (words.Length > 1) sbSpec.Append("("); */
					
	
					
					
					
									
					sbSpec.Append(string.Format(@"FullName.Contains(""{0}"")", word));
					

					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
								 //sbSpec.Append("*extraFreeText*");

                    /*if (words.Length > 1) sbSpec.Append(")");
					
					nWords++;

					}

                }*/
                //specificFilter = string.Format("{0}{1}", specificFilter, string.Format(sbCont.ToString(), sbSpec.ToString()));
                                 result = sbSpec.ToString();  
            }
			//result = specificFilter;
			
			return result;

		}
	
			public List<StudentParent> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params object[] extraParams)
        {
			return GetBy(filter, pageSize, page, orderBy, orderDir,  null, extraParams);
		}
           public List<StudentParent> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, string usemode, params object[] extraParams)
            { 
                return GetBy(filter, pageSize, page, orderBy, orderDir, usemode, null, extraParams);
            }


		public List<StudentParent> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  string usemode, ContextRequest context, params object[] extraParams)

        {

            // string freetext = null;
            //if (filter.Contains("|"))
            //{
            //    int parts = filter.Split(char.Parse("|")).Count();
            //    if (parts > 1)
            //    {

            //        freetext = filter.Split(char.Parse("|"))[1];
            //    }
            //}
		
            //string specificFilter = "";
            //if (!string.IsNullOrEmpty(filter))
            //  specificFilter=  GetSpecificFilter(filter);
            if (string.IsNullOrEmpty(orderBy))
            {
			                orderBy = "UpdatedDate";
            }
			//orderDir = "desc";
			SFSdotNet.Framework.Data.SortDirection direction = SFSdotNet.Framework.Data.SortDirection.Ascending;
            if (!string.IsNullOrEmpty(orderDir))
            {
                if (orderDir == "desc")
                    direction = SFSdotNet.Framework.Data.SortDirection.Descending;
            }
            if (context == null)
                context = new ContextRequest();
			

             context.UseMode = usemode;
             if (context.CustomQuery == null )
                context.CustomQuery =new SFSdotNet.Framework.My.CustomQuery();

 
                context.CustomQuery.ExtraParams = extraParams;

                    context.CustomQuery.OrderBy = orderBy;
                   context.CustomQuery.SortDirection = direction;
                   context.CustomQuery.Page = page;
                  context.CustomQuery.PageSize = pageSize;
               

            

            if (!preventSecurityRestrictions) {
			 if (context.CurrentContext == null)
                {
					if (SFSdotNet.Framework.My.Context.CurrentContext != null &&  SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
					{
						context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
						context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

					}
					else {
						throw new Exception("The security rule require a specific user and company");
					}
				}
            }
            return GetBy(filter, context);
  
        }


        public List<StudentParent> GetBy(string strWhere, ContextRequest contextRequest)
        {
        	#region old code
				
				 //Expression<Func<tvsReservationTransport, bool>> predicate = null;
				string strWhereClean = strWhere.Replace("*extraFreeText*", "").Replace("()", "");
                //if (!string.IsNullOrEmpty(strWhereClean)){

                //    object[] extraParams = null;
                //    //if (contextRequest != null )
                //    //    if (contextRequest.CustomQuery != null )
                //    //        extraParams = contextRequest.CustomQuery.ExtraParams;
                //    //predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<tvsReservationTransport, bool>(strWhereClean, extraParams != null? extraParams.Cast<Guid>(): null);				
                //}
				 if (contextRequest == null)
                {
                    contextRequest = new ContextRequest();
                    if (contextRequest.CustomQuery == null)
                        contextRequest.CustomQuery = new CustomQuery();
                }
                  if (!preventSecurityRestrictions) {
					if (contextRequest.User == null || contextRequest.Company == null)
                      {
                     if (SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
                     {
                         contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                         contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

                     }
                     else {
                         throw new Exception("The security rule require a specific User and Company ");
                     }
					 }
                 }
            contextRequest.CustomQuery.FilterExpressionString = strWhere;
				//return GetBy(predicate, contextRequest);  

			#endregion				
				
                    return GetBy(strWhere, contextRequest, "");  


        }
       public List<StudentParent> GetBy(string strWhere)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
			
            return GetBy(strWhere, context, null);
        }

        public List<StudentParent> GetBy(string strWhere, string includes)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
            return GetBy(strWhere, context, includes);
        }

        #endregion
        #endregion
		
		  #region SaveOrUpdate
        
 		 public StudentParent Create(StudentParent entity)
        {
				//ObjectContext context = null;
				    if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: Create");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

				return this.Create(entity, contextRequest);


        }
        
       
        public StudentParent Create(StudentParent entity, ContextRequest contextRequest)
        {
		
		bool graph = false;
	
				bool preventPartial = false;
                if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
               
			using (EFContext con = new EFContext()) {

				StudentParent itemForSave = new StudentParent();
#region Autos
		if(!preventSecurityRestrictions){

				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion
               BusinessRulesEventArgs<StudentParent> e = null;
			    if (preventPartial == false )
                OnCreating(this,e = new BusinessRulesEventArgs<StudentParent>() { ContextRequest = contextRequest, Item=entity });
				   if (e != null) {
						if (e.Cancel)
						{
							context = null;
							return e.Item;

						}
					}

                    if (entity.GuidStudentParent == Guid.Empty)
                   {
                       entity.GuidStudentParent = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   itemForSave.GuidStudentParent = entity.GuidStudentParent;
				  
		
			itemForSave.GuidStudentParent = entity.GuidStudentParent;

			itemForSave.FullName = entity.FullName;

			itemForSave.GuidCompany = entity.GuidCompany;

			itemForSave.CreatedDate = entity.CreatedDate;

			itemForSave.UpdatedDate = entity.UpdatedDate;

			itemForSave.CreatedBy = entity.CreatedBy;

			itemForSave.UpdatedBy = entity.UpdatedBy;

			itemForSave.Bytes = entity.Bytes;

			itemForSave.IsDeleted = entity.IsDeleted;

				
				con.StudentParents.Add(itemForSave);




                
				con.ChangeTracker.Entries().Where(p => p.Entity != itemForSave && p.State != EntityState.Unchanged).ForEach(p => p.State = EntityState.Detached);

				con.Entry<StudentParent>(itemForSave).State = EntityState.Added;

				con.SaveChanges();

					 
				

				//itemResult = entity;
                //if (e != null)
                //{
                 //   e.Item = itemResult;
                //}
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false )
                OnCreated(this, e == null ? e = new BusinessRulesEventArgs<StudentParent>() { ContextRequest = contextRequest, Item = entity } : e);



                if (e != null && e.Item != null )
                {
                    return e.Item;
                }
                              return entity;
			}
            
        }
        //BusinessRulesEventArgs<StudentParent> e = null;
        public void Create(List<StudentParent> entities)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            Create(entities, contextRequest);
        }
        public void Create(List<StudentParent> entities, ContextRequest contextRequest)
        
        {
			ObjectContext context = null;
            	foreach (StudentParent entity in entities)
				{
					this.Create(entity, contextRequest);
				}
        }
		  public void CreateOrUpdateBulk(List<StudentParent> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "cu", contextRequest);
        }

        private void CreateOrUpdateBulk(List<StudentParent> entities, string actionKey, ContextRequest contextRequest)
        {
			if (entities.Count() > 0){
            bool graph = false;

            bool preventPartial = false;
            if (contextRequest != null && contextRequest.PreventInterceptors == true)
            {
                preventPartial = true;
            }
            foreach (var entity in entities)
            {
                    if (entity.GuidStudentParent == Guid.Empty)
                   {
                       entity.GuidStudentParent = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   
				  


#region Autos
		if(!preventSecurityRestrictions){


 if (actionKey != "u")
                        {
				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;


}
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion


		
			//entity.GuidStudentParent = entity.GuidStudentParent;

			//entity.FullName = entity.FullName;

			//entity.GuidCompany = entity.GuidCompany;

			//entity.CreatedDate = entity.CreatedDate;

			//entity.UpdatedDate = entity.UpdatedDate;

			//entity.CreatedBy = entity.CreatedBy;

			//entity.UpdatedBy = entity.UpdatedBy;

			//entity.Bytes = entity.Bytes;

			//entity.IsDeleted = entity.IsDeleted;

				
				




                
				

					 
				

				//itemResult = entity;
            }
            using (EFContext con = new EFContext())
            {
                 if (actionKey == "c")
                    {
                        context.BulkInsert(entities);
                    }else if ( actionKey == "u")
                    {
                        context.BulkUpdate(entities);
                    }else
                    {
                        context.BulkInsertOrUpdate(entities);
                    }
            }

			}
        }
	
		public void CreateBulk(List<StudentParent> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "c", contextRequest);
        }


		public void UpdateAgile(StudentParent item, params string[] fields)
         {
			UpdateAgile(item, null, fields);
        }
		public void UpdateAgile(StudentParent item, ContextRequest contextRequest, params string[] fields)
         {
            
             ContextRequest contextNew = null;
             if (contextRequest != null)
             {
                 contextNew = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
                 if (fields != null && fields.Length > 0)
                 {
                     contextNew.CustomQuery.SpecificProperties  = fields.ToList();
                 }
                 else if(contextRequest.CustomQuery.SpecificProperties.Count > 0)
                 {
                     fields = contextRequest.CustomQuery.SpecificProperties.ToArray();
                 }
             }
			

		   using (EFContext con = new EFContext())
            {



               
					List<string> propForCopy = new List<string>();
                    propForCopy.AddRange(fields);
                    
					  
					if (!propForCopy.Contains("GuidStudentParent"))
						propForCopy.Add("GuidStudentParent");

					var itemForUpdate = SFSdotNet.Framework.BR.Utils.GetConverted<StudentParent,StudentParent>(item, propForCopy.ToArray());
					 itemForUpdate.GuidStudentParent = item.GuidStudentParent;
                  var setT = con.Set<StudentParent>().Attach(itemForUpdate);

					if (fields.Count() > 0)
					  {
						  item.ModifiedProperties = fields;
					  }
                    foreach (var property in item.ModifiedProperties)
					{						
                        if (property != "GuidStudentParent")
                             con.Entry(setT).Property(property).IsModified = true;

                    }

                
               int result = con.SaveChanges();
               if (result != 1)
               {
                   SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: 1");
               }


            }

			OnUpdatedAgile(this, new BusinessRulesEventArgs<StudentParent>() { Item = item, ContextRequest = contextNew  });

         }
		public void UpdateBulk(List<StudentParent>  items, params string[] fields)
         {
             SFSdotNet.Framework.My.ContextRequest req = new SFSdotNet.Framework.My.ContextRequest();
             req.CustomQuery = new SFSdotNet.Framework.My.CustomQuery();
             foreach (var field in fields)
             {
                 req.CustomQuery.SpecificProperties.Add(field);
             }
             UpdateBulk(items, req);

         }

		 public void DeleteBulk(List<StudentParent> entities, ContextRequest contextRequest = null)
        {

            using (EFContext con = new EFContext())
            {
                foreach (var entity in entities)
                {
					var entityProxy = new StudentParent() { GuidStudentParent = entity.GuidStudentParent };

                    con.Entry<StudentParent>(entityProxy).State = EntityState.Deleted;

                }

                int result = con.SaveChanges();
                if (result != entities.Count)
                {
                    SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: " + entities.Count.ToString());
                }
            }

        }

        public void UpdateBulk(List<StudentParent> items, ContextRequest contextRequest)
        {
            if (items.Count() > 0){

			 foreach (var entity in items)
            {


#region Autos
		if(!preventSecurityRestrictions){

				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	



			}
#endregion





				}
				using (EFContext con = new EFContext())
				{

                    
                
                   con.BulkUpdate(items);

				}
             
			}	  
        }

         public StudentParent Update(StudentParent entity)
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return Update(entity, contextRequest);
        }
       
         public StudentParent Update(StudentParent entity, ContextRequest contextRequest)
        {
		 if ((System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null) && contextRequest == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Update");
            }
            if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            }

			
				StudentParent  itemResult = null;

	
			//entity.UpdatedDate = DateTime.Now.ToUniversalTime();
			//if(contextRequest.User != null)
				//entity.UpdatedBy = contextRequest.User.GuidUser;

//	    var oldentity = GetBy(p => p.GuidStudentParent == entity.GuidStudentParent, contextRequest).FirstOrDefault();
	//	if (oldentity != null) {
		
          //  entity.CreatedDate = oldentity.CreatedDate;
    //        entity.CreatedBy = oldentity.CreatedBy;
	
      //      entity.GuidCompany = oldentity.GuidCompany;
	
			

	
		//}

			 using( EFContext con = new EFContext()){
				BusinessRulesEventArgs<StudentParent> e = null;
				bool preventPartial = false; 
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false)
                OnUpdating(this,e = new BusinessRulesEventArgs<StudentParent>() { ContextRequest = contextRequest, Item=entity});
				   if (e != null) {
						if (e.Cancel)
						{
							//outcontext = null;
							return e.Item;

						}
					}

	string includes = "";
	IQueryable < StudentParent > query = con.StudentParents.AsQueryable();
	foreach (string include in includes.Split(char.Parse(",")))
                       {
                           if (!string.IsNullOrEmpty(include))
                               query = query.Include(include);
                       }
	var oldentity = query.FirstOrDefault(p => p.GuidStudentParent == entity.GuidStudentParent);
	if (oldentity.FullName != entity.FullName)
		oldentity.FullName = entity.FullName;

				//if (entity.UpdatedDate == null || (contextRequest != null && contextRequest.IsFromUI("StudentParents", UIActions.Updating)))
			oldentity.UpdatedDate = DateTime.Now.ToUniversalTime();
			if(contextRequest.User != null)
				oldentity.UpdatedBy = contextRequest.User.GuidUser;

           


				if (entity.Students != null)
                {
                    foreach (var item in entity.Students)
                    {


                        
                    }
					
                    

                }


				con.ChangeTracker.Entries().Where(p => p.Entity != oldentity).ForEach(p => p.State = EntityState.Unchanged);  
				  
				con.SaveChanges();
        
					 
					
               
				itemResult = entity;
				if(preventPartial == false)
					OnUpdated(this, e = new BusinessRulesEventArgs<StudentParent>() { ContextRequest = contextRequest, Item=itemResult });

              	return itemResult;
			}
			  
        }
        public StudentParent Save(StudentParent entity)
        {
			return Create(entity);
        }
        public int Save(List<StudentParent> entities)
        {
			 Create(entities);
            return entities.Count;

        }
        #endregion
        #region Delete
        public void Delete(StudentParent entity)
        {
				this.Delete(entity, null);
			
        }
		 public void Delete(StudentParent entity, ContextRequest contextRequest)
        {
				
				  List<StudentParent> entities = new List<StudentParent>();
				   entities.Add(entity);
				this.Delete(entities, contextRequest);
			
        }

         public void Delete(string query, Guid[] guids, ContextRequest contextRequest)
        {
			var br = new StudentParentsBR(true);
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);
            
            Delete(items, contextRequest);

        }
        public void Delete(StudentParent entity,  ContextRequest contextRequest, BusinessRulesEventArgs<StudentParent> e = null)
        {
			
				using(EFContext con = new EFContext())
                 {
				
               	BusinessRulesEventArgs<StudentParent> _e = null;
               List<StudentParent> _items = new List<StudentParent>();
                _items.Add(entity);
                if (e == null || e.PreventPartialPropagate == false)
                {
                    OnDeleting(this, _e = (e == null ? new BusinessRulesEventArgs<StudentParent>() { ContextRequest = contextRequest, Item = entity, Items = null  } : e));
                }
                if (_e != null)
                {
                    if (_e.Cancel)
						{
							context = null;
							return;

						}
					}


				
									//IsDeleted
					bool logicDelete = true;
					if (entity.IsDeleted != null)
					{
						if (entity.IsDeleted.Value)
							logicDelete = false;
					}
					if (logicDelete)
					{
											//entity = GetBy(p =>, contextRequest).FirstOrDefault();
						entity.IsDeleted = true;
						if (contextRequest != null && contextRequest.User != null)
							entity.UpdatedBy = contextRequest.User.GuidUser;
                        entity.UpdatedDate = DateTime.UtcNow;
						UpdateAgile(entity, "IsDeleted","UpdatedBy","UpdatedDate");

						
					}
					else {
					con.Entry<StudentParent>(entity).State = EntityState.Deleted;
					con.SaveChanges();
				
				 
					}
								
				
				 
					
					
			if (e == null || e.PreventPartialPropagate == false)
                {

                    if (_e == null)
                        _e = new BusinessRulesEventArgs<StudentParent>() { ContextRequest = contextRequest, Item = entity, Items = null };

                    OnDeleted(this, _e);
                }

				//return null;
			}
        }
 public void UnDelete(string query, Guid[] guids, ContextRequest contextRequest)
        {
            var br = new StudentParentsBR(true);
            contextRequest.CustomQuery.IncludeDeleted = true;
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);

            foreach (var item in items)
            {
                item.IsDeleted = false;
						if (contextRequest != null && contextRequest.User != null)
							item.UpdatedBy = contextRequest.User.GuidUser;
                        item.UpdatedDate = DateTime.UtcNow;
            }

            UpdateBulk(items, "IsDeleted","UpdatedBy","UpdatedDate");
        }

         public void Delete(List<StudentParent> entities,  ContextRequest contextRequest = null )
        {
				
			 BusinessRulesEventArgs<StudentParent> _e = null;

                OnDeleting(this, _e = new BusinessRulesEventArgs<StudentParent>() { ContextRequest = contextRequest, Item = null, Items = entities });
                if (_e != null)
                {
                    if (_e.Cancel)
                    {
                        context = null;
                        return;

                    }
                }
                bool allSucced = true;
                BusinessRulesEventArgs<StudentParent> eToChilds = new BusinessRulesEventArgs<StudentParent>();
                if (_e != null)
                {
                    eToChilds = _e;
                }
                else
                {
                    eToChilds = new BusinessRulesEventArgs<StudentParent>() { ContextRequest = contextRequest, Item = (entities.Count == 1 ? entities[0] : null), Items = entities };
                }
				foreach (StudentParent item in entities)
				{
					try
                    {
                        this.Delete(item, contextRequest, e: eToChilds);
                    }
                    catch (Exception ex)
                    {
                        SFSdotNet.Framework.My.EventLog.Error(ex);
                        allSucced = false;
                    }
				}
				if (_e == null)
                    _e = new BusinessRulesEventArgs<StudentParent>() { ContextRequest = contextRequest, CountResult = entities.Count, Item = null, Items = entities };
                OnDeleted(this, _e);

			
        }
        #endregion
 
        #region GetCount
		 public int GetCount(Expression<Func<StudentParent, bool>> predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

			return GetCount(predicate, contextRequest);
		}
        public int GetCount(Expression<Func<StudentParent, bool>> predicate, ContextRequest contextRequest)
        {


		
		 using (EFContext con = new EFContext())
            {


				if (predicate == null) predicate = PredicateBuilder.True<StudentParent>();
           		predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted == null);
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    		if (contextRequest.User !=null )
                        		if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
									predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa

								}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				
				IQueryable<StudentParent> query = con.StudentParents.AsQueryable();
                return query.AsExpandable().Count(predicate);

			
				}
			

        }
		  public int GetCount(string predicate,  ContextRequest contextRequest)
         {
             return GetCount(predicate, null, contextRequest);
         }

         public int GetCount(string predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return GetCount(predicate, contextRequest);
        }
		 public int GetCount(string predicate, string usemode){
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
				return GetCount( predicate,  usemode,  contextRequest);
		 }
        public int GetCount(string predicate, string usemode, ContextRequest contextRequest){

		using (EFContext con = new EFContext()) {
				string computedFields = "";
				string fkIncludes = "";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<StudentParent>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetCount(con, predicate, usemode, contextRequest, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields);

			}
			#region old code
			 /* string freetext = null;
            Filter filter = new Filter();

              if (predicate.Contains("|"))
              {
                 
                  filter.SetFilterPart("ft", GetSpecificFilter(predicate, contextRequest));
                 
                  filter.ProcessText(predicate.Split(char.Parse("|"))[0]);
                  freetext = predicate.Split(char.Parse("|"))[1];

				  if (!string.IsNullOrEmpty(freetext) && string.IsNullOrEmpty(contextRequest.FreeText))
                  {
                      contextRequest.FreeText = freetext;
                  }
              }
              else {
                  filter.ProcessText(predicate);
              }
			   predicate = filter.GetFilterComplete();
			// BusinessRulesEventArgs<StudentParent>  e = null;
           	using (EFContext con = new EFContext())
			{
			
			

			 QueryBuild(predicate, filter, con, contextRequest, "count", new List<string>());


			
			BusinessRulesEventArgs<StudentParent> e = null;

			contextRequest.FreeText = freetext;
			contextRequest.UseMode = usemode;
            OnCounting(this, e = new BusinessRulesEventArgs<StudentParent>() {  Filter =filter, ContextRequest = contextRequest });
            if (e != null)
            {
                if (e.Cancel)
                {
                    context = null;
                    return e.CountResult;

                }

            

            }
			
			StringBuilder sbQuerySystem = new StringBuilder();
		
					
                    filter.SetFilterPart("de","(IsDeleted != true OR IsDeleted == null)");
			
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    	if (contextRequest.User !=null )
                        	if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
                        		
								filter.SetFilterPart("co", @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa
						
						
							}
							
							}
							if (preventSecurityRestrictions) preventSecurityRestrictions= false;
		
				   
                 filter.CleanAndProcess("");
				//string predicateWithFKAndComputed = SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicate );               
				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed();
               string predicateWithManyRelations = filter.GetFilterChildren();
			   ///QueryUtils.BreakeQuery1(predicate, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
			   predicate = filter.GetFilterComplete();
               if (!string.IsNullOrEmpty(predicate))
               {
				
					
                    return con.StudentParents.Where(predicate).Count();
					
                }else
                    return con.StudentParents.Count();
					
			}*/
			#endregion

		}
         public int GetCount()
        {
            return GetCount(p => true);
        }
        #endregion
        
         


        public void Delete(List<StudentParent.CompositeKey> entityKeys)
        {

            List<StudentParent> items = new List<StudentParent>();
            foreach (var itemKey in entityKeys)
            {
                items.Add(GetByKey(itemKey.GuidStudentParent));
            }

            Delete(items);

        }
		 public void UpdateAssociation(string relation, string relationValue, string query, Guid[] ids, ContextRequest contextRequest)
        {
            var items = GetBy(query, null, null, null, null, null, contextRequest, ids);
			 var module = SFSdotNet.Framework.Cache.Caching.SystemObjects.GetModuleByKey(SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(System.Web.HttpContext.Current.Request.RequestContext, "area"));
           
            foreach (var item in items)
            {
			  Guid ? guidRelationValue = null ;
                if (!string.IsNullOrEmpty(relationValue)){
                    guidRelationValue = Guid.Parse(relationValue );
                }

				 if (relation.Contains("."))
                {
                    var partsWithOtherProp = relation.Split(char.Parse("|"));
                    var parts = partsWithOtherProp[0].Split(char.Parse("."));

                    string proxyRelName = parts[0];
                    string proxyProperty = parts[1];
                    string proxyPropertyKeyNameFromOther = partsWithOtherProp[1];
                    //string proxyPropertyThis = parts[2];

                    var prop = item.GetType().GetProperty(proxyRelName);
                    //var entityInfo = //SFSdotNet.Framework.
                    // descubrir el tipo de entidad dentro de la colección
                    Type typeEntityInList = SFSdotNet.Framework.Entities.Utils.GetTypeFromList(prop);
                    var newProxyItem = Activator.CreateInstance(typeEntityInList);
                    var propThisForSet = newProxyItem.GetType().GetProperty(proxyProperty);
                    var entityInfoOfProxy = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(typeEntityInList);
                    var propOther = newProxyItem.GetType().GetProperty(proxyPropertyKeyNameFromOther);

                    if (propThisForSet != null && entityInfoOfProxy != null && propOther != null )
                    {
                        var entityInfoThis = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(item.GetType());
                        var valueThisId = item.GetType().GetProperty(entityInfoThis.PropertyKeyName).GetValue(item);
                        if (valueThisId != null)
                            propThisForSet.SetValue(newProxyItem, valueThisId);
                        propOther.SetValue(newProxyItem, Guid.Parse(relationValue));
                        
                        var entityNameProp = newProxyItem.GetType().GetField("EntityName").GetValue(null);
                        var entitySetNameProp = newProxyItem.GetType().GetField("EntitySetName").GetValue(null);

                        SFSdotNet.Framework.Apps.Integration.CreateItemFromApp(entityNameProp.ToString(), entitySetNameProp.ToString(), module.ModuleNamespace, newProxyItem, contextRequest);

                    }

                    // crear una instancia del tipo de entidad
                    // llenar los datos y registrar nuevo


                }
                else
                {
                var prop = item.GetType().GetProperty(relation);
                var entityInfo = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(prop.PropertyType);
                if (entityInfo != null)
                {
                    var ins = Activator.CreateInstance(prop.PropertyType);
                   if (guidRelationValue != null)
                    {
                        prop.PropertyType.GetProperty(entityInfo.PropertyKeyName).SetValue(ins, guidRelationValue);
                        item.GetType().GetProperty(relation).SetValue(item, ins);
                    }
                    else
                    {
                        item.GetType().GetProperty(relation).SetValue(item, null);
                    }

                    Update(item, contextRequest);
                }

				}
            }
        }
		
	}
		public partial class AcademyLevelsBR:BRBase<AcademyLevel>{
	 	
           
		 #region Partial methods

           partial void OnUpdating(object sender, BusinessRulesEventArgs<AcademyLevel> e);

            partial void OnUpdated(object sender, BusinessRulesEventArgs<AcademyLevel> e);
			partial void OnUpdatedAgile(object sender, BusinessRulesEventArgs<AcademyLevel> e);

            partial void OnCreating(object sender, BusinessRulesEventArgs<AcademyLevel> e);
            partial void OnCreated(object sender, BusinessRulesEventArgs<AcademyLevel> e);

            partial void OnDeleting(object sender, BusinessRulesEventArgs<AcademyLevel> e);
            partial void OnDeleted(object sender, BusinessRulesEventArgs<AcademyLevel> e);

            partial void OnGetting(object sender, BusinessRulesEventArgs<AcademyLevel> e);
            protected override void OnVirtualGetting(object sender, BusinessRulesEventArgs<AcademyLevel> e)
            {
                OnGetting(sender, e);
            }
			protected override void OnVirtualCounting(object sender, BusinessRulesEventArgs<AcademyLevel> e)
            {
                OnCounting(sender, e);
            }
			partial void OnTaken(object sender, BusinessRulesEventArgs<AcademyLevel> e);
			protected override void OnVirtualTaken(object sender, BusinessRulesEventArgs<AcademyLevel> e)
            {
                OnTaken(sender, e);
            }

            partial void OnCounting(object sender, BusinessRulesEventArgs<AcademyLevel> e);
 
			partial void OnQuerySettings(object sender, BusinessRulesEventArgs<AcademyLevel> e);
          
            #endregion
			
		private static AcademyLevelsBR singlenton =null;
				public static AcademyLevelsBR NewInstance(){
					return  new AcademyLevelsBR();
					
				}
		public static AcademyLevelsBR Instance{
			get{
				if (singlenton == null)
					singlenton = new AcademyLevelsBR();
				return singlenton;
			}
		}
		//private bool preventSecurityRestrictions = false;
		 public bool PreventAuditTrail { get; set;  }
		#region Fields
        EFContext context = null;
        #endregion
        #region Constructor
        public AcademyLevelsBR()
        {
            context = new EFContext();
        }
		 public AcademyLevelsBR(bool preventSecurity)
            {
                this.preventSecurityRestrictions = preventSecurity;
				context = new EFContext();
            }
        #endregion
		
		#region Get

 		public IQueryable<AcademyLevel> Get()
        {
            using (EFContext con = new EFContext())
            {
				
				var query = con.AcademyLevels.AsQueryable();
                con.Configuration.ProxyCreationEnabled = false;

                //query = ContextQueryBuilder<Nutrient>.ApplyContextQuery(query, contextRequest);

                return query;




            }

        }
		


 	
		public List<AcademyLevel> GetAll()
        {
            return this.GetBy(p => true);
        }
        public List<AcademyLevel> GetAll(string includes)
        {
            return this.GetBy(p => true, includes);
        }
        public AcademyLevel GetByKey(Guid guidAcademyLevel)
        {
            return GetByKey(guidAcademyLevel, true);
        }
        public AcademyLevel GetByKey(Guid guidAcademyLevel, bool loadIncludes)
        {
            AcademyLevel item = null;
			var query = PredicateBuilder.True<AcademyLevel>();
                    
			string strWhere = @"GuidAcademyLevel = Guid(""" + guidAcademyLevel.ToString()+@""")";
            Expression<Func<AcademyLevel, bool>> predicate = null;
            //if (!string.IsNullOrEmpty(strWhere))
            //    predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<AcademyLevel, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
			 ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
            contextRequest.CustomQuery.FilterExpressionString = strWhere;

			//item = GetBy(predicate, loadIncludes, contextRequest).FirstOrDefault();
			item = GetBy(strWhere,loadIncludes,contextRequest).FirstOrDefault();
            return item;
        }
         public List<AcademyLevel> GetBy(string strWhere, bool loadRelations, ContextRequest contextRequest)
        {
            if (!loadRelations)
                return GetBy(strWhere, contextRequest);
            else
                return GetBy(strWhere, contextRequest, "");

        }
		  public List<AcademyLevel> GetBy(string strWhere, bool loadRelations)
        {
              if (!loadRelations)
                return GetBy(strWhere, new ContextRequest());
            else
                return GetBy(strWhere, new ContextRequest(), "");

        }
		         public AcademyLevel GetByKey(Guid guidAcademyLevel, params Expression<Func<AcademyLevel, object>>[] includes)
        {
            AcademyLevel item = null;
			string strWhere = @"GuidAcademyLevel = Guid(""" + guidAcademyLevel.ToString()+@""")";
          Expression<Func<AcademyLevel, bool>> predicate = p=> p.GuidAcademyLevel == guidAcademyLevel;
           // if (!string.IsNullOrEmpty(strWhere))
           //     predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<AcademyLevel, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
        item = GetBy(predicate, includes).FirstOrDefault();
         ////   item = GetBy(strWhere,includes).FirstOrDefault();
			return item;

        }
        public AcademyLevel GetByKey(Guid guidAcademyLevel, string includes)
        {
            AcademyLevel item = null;
			string strWhere = @"GuidAcademyLevel = Guid(""" + guidAcademyLevel.ToString()+@""")";
            
			
            item = GetBy(strWhere, includes).FirstOrDefault();
            return item;

        }
		 public AcademyLevel GetByKey(Guid guidAcademyLevel, string usemode, string includes)
		{
			return GetByKey(guidAcademyLevel, usemode, null, includes);

		 }
		 public AcademyLevel GetByKey(Guid guidAcademyLevel, string usemode, ContextRequest context,  string includes)
        {
            AcademyLevel item = null;
			string strWhere = @"GuidAcademyLevel = Guid(""" + guidAcademyLevel.ToString()+@""")";
			if (context == null){
				context = new ContextRequest();
				context.CustomQuery = new CustomQuery();
				context.CustomQuery.IsByKey = true;
				context.CustomQuery.FilterExpressionString = strWhere;
				context.UseMode = usemode;
			}
            item = GetBy(strWhere,context , includes).FirstOrDefault();
            return item;

        }

        #region Dynamic Predicate
        public List<AcademyLevel> GetBy(Expression<Func<AcademyLevel, bool>> predicate, int? pageSize, int? page)
        {
            return this.GetBy(predicate, pageSize, page, null, null);
        }
        public List<AcademyLevel> GetBy(Expression<Func<AcademyLevel, bool>> predicate, ContextRequest contextRequest)
        {

            return GetBy(predicate, contextRequest,"");
        }
        
        public List<AcademyLevel> GetBy(Expression<Func<AcademyLevel, bool>> predicate, ContextRequest contextRequest, params Expression<Func<AcademyLevel, object>>[] includes)
        {
            StringBuilder sb = new StringBuilder();
           if (includes != null)
            {
                foreach (var path in includes)
                {

						if (sb.Length > 0) sb.Append(",");
						sb.Append(SFSdotNet.Framework.Linq.Utils.IncludeToString<AcademyLevel>(path));

               }
            }
            return GetBy(predicate, contextRequest, sb.ToString());
        }
        
        
        public List<AcademyLevel> GetBy(Expression<Func<AcademyLevel, bool>> predicate, string includes)
        {
			ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";

            return GetBy(predicate, context, includes);
        }

        public List<AcademyLevel> GetBy(Expression<Func<AcademyLevel, bool>> predicate, params Expression<Func<AcademyLevel, object>>[] includes)
        {
			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest context = new ContextRequest();
			            context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";
            return GetBy(predicate, context, includes);
        }

      
		public bool DisableCache { get; set; }
		public List<AcademyLevel> GetBy(Expression<Func<AcademyLevel, bool>> predicate, ContextRequest contextRequest, string includes)
		{
            using (EFContext con = new EFContext()) {
				
				string fkIncludes = "";
                List<string> multilangProperties = new List<string>();
				if (predicate == null) predicate = PredicateBuilder.True<AcademyLevel>();
                var notDeletedExpression = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;
					Expression<Func<AcademyLevel,bool>> multitenantExpression  = null;
					if (contextRequest != null && contextRequest.Company != null)	                        	
						multitenantExpression = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicate, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression);

#region Old code
/*
				List<AcademyLevel> result = null;
               BusinessRulesEventArgs<AcademyLevel>  e = null;
	
				OnGetting(con, e = new BusinessRulesEventArgs<AcademyLevel>() {  FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null) });

               // OnGetting(con,e = new BusinessRulesEventArgs<AcademyLevel>() { FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = contextRequest.CustomQuery.FilterExpressionString});
				   if (e != null) {
				    predicate = e.FilterExpression;
						if (e.Cancel)
						{
							context = null;
							 if (e.Items == null) e.Items = new List<AcademyLevel>();
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                if (predicate == null) predicate = PredicateBuilder.True<AcademyLevel>();
                
                //var es = _repository.Queryable;

                IQueryable<AcademyLevel> query =  con.AcademyLevels.AsQueryable();

                                if (!string.IsNullOrEmpty(includes))
                {
                    foreach (string include in includes.Split(char.Parse(",")))
                    {
						if (!string.IsNullOrEmpty(include))
                            query = query.Include(include);
                    }
                }
                    predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
					 	if (!preventSecurityRestrictions)
						{
							if (contextRequest != null )
		                    	if (contextRequest.User !=null )
		                        	if (contextRequest.Company != null){
		                        	
										predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
 									
									}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				query =query.AsExpandable().Where(predicate);
                query = ContextQueryBuilder<AcademyLevel>.ApplyContextQuery(query, contextRequest);

                result = query.AsNoTracking().ToList<AcademyLevel>();
				  
                if (e != null)
                {
                    e.Items = result;
                }
				//if (contextRequest != null ){
				//	 contextRequest = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
					contextRequest.CustomQuery = new CustomQuery();

				//}
				OnTaken(this, e == null ? e =  new BusinessRulesEventArgs<AcademyLevel>() { Items= result, IncludingComputedLinq = false, ContextRequest = contextRequest,  FilterExpression = predicate } :  e);
  
			

                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
				*/
#endregion
            }
        }


		/*public int Update(List<AcademyLevel> items, ContextRequest contextRequest)
            {
                int result = 0;
                using (EFContext con = new EFContext())
                {
                   
                

                    foreach (var item in items)
                    {
                        //secMessageToUser messageToUser = new secMessageToUser();
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            item.GetType().GetProperty(prop).SetValue(item, item.GetType().GetProperty(prop).GetValue(item));
                        }
                        //messageToUser.GuidMessageToUser = (Guid)item.GetType().GetProperty("GuidMessageToUser").GetValue(item);

                        var setObject = con.CreateObjectSet<AcademyLevel>("AcademyLevels");
                        //messageToUser.Readed = DateTime.UtcNow;
                        setObject.Attach(item);
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            con.ObjectStateManager.GetObjectStateEntry(item).SetModifiedProperty(prop);
                        }
                       
                    }
                    result = con.SaveChanges();

                    


                }
                return result;
            }
           */
		

        public List<AcademyLevel> GetBy(string predicateString, ContextRequest contextRequest, string includes)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
				


				string computedFields = "";
				string fkIncludes = "";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<AcademyLevel>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					//if (contextRequest != null && contextRequest.Company != null)                      	
					//	 multitenantExpression = @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))";
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicateString, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression,computedFields);


	#region Old Code
	/*
				BusinessRulesEventArgs<AcademyLevel> e = null;

				Filter filter = new Filter();
                if (predicateString.Contains("|"))
                {
                    string ft = GetSpecificFilter(predicateString, contextRequest);
                    if (!string.IsNullOrEmpty(ft))
                        filter.SetFilterPart("ft", ft);
                   
                    contextRequest.FreeText = predicateString.Split(char.Parse("|"))[1];
                    var q1 = predicateString.Split(char.Parse("|"))[0];
                    if (!string.IsNullOrEmpty(q1))
                    {
                        filter.ProcessText(q1);
                    }
                }
                else {
                    filter.ProcessText(predicateString);
                }
				 var includesList = (new List<string>());
                 if (!string.IsNullOrEmpty(includes))
                 {
                     includesList = includes.Split(char.Parse(",")).ToList();
                 }

				List<AcademyLevel> result = new List<AcademyLevel>();
         
			QueryBuild(predicateString, filter, con, contextRequest, "getby", includesList);
			 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }
				
				
					OnGetting(con, e == null ? e = new BusinessRulesEventArgs<AcademyLevel>() { Filter = filter, ContextRequest = contextRequest  } : e );

                  //OnGetting(con,e = new BusinessRulesEventArgs<AcademyLevel>() {  ContextRequest = contextRequest, FilterExpressionString = predicateString });
			   	if (e != null) {
				    //predicateString = e.GetQueryString();
						if (e.Cancel)
						{
							context = null;
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				//	 else {
                //      predicateString = predicateString.Replace("*extraFreeText*", "").Replace("()","");
                //  }
				//con.EnableChangeTrackingUsingProxies = false;
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                //if (predicate == null) predicate = PredicateBuilder.True<AcademyLevel>();
                
                //var es = _repository.Queryable;
				IQueryable<AcademyLevel> query = con.AcademyLevels.AsQueryable();
		
				// include relations FK
				if(string.IsNullOrEmpty(includes) ){
					includes ="";
				}
				StringBuilder sbQuerySystem = new StringBuilder();
                    //predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				

				//if (!string.IsNullOrEmpty(predicateString))
                //      sbQuerySystem.Append(" And ");
                //sbQuerySystem.Append(" (IsDeleted != true Or IsDeleted = null) ");
				 filter.SetFilterPart("de", "(IsDeleted != true OR IsDeleted = null)");


					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
	                    	if (contextRequest.User !=null )
	                        	if (contextRequest.Company != null ){
	                        		//if (sbQuerySystem.Length > 0)
	                        		//	    			sbQuerySystem.Append( " And ");	
									//sbQuerySystem.Append(@" (GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa

									filter.SetFilterPart("co",@"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))");

								}
						}	
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				//string predicateString = predicate.ToDynamicLinq<AcademyLevel>();
				//predicateString += sbQuerySystem.ToString();
				filter.CleanAndProcess("");

				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed(); //SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicateString );               
                string predicateWithManyRelations = filter.GetFilterChildren(); //SFSdotNet.Framework.Linq.Utils.CleanPartExpression(predicateString);

                //QueryUtils.BreakeQuery1(predicateString, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
                var _queryable = query.AsQueryable();
				bool includeAll = true; 
                if (!string.IsNullOrEmpty(predicateWithManyRelations))
                    _queryable = _queryable.Where(predicateWithManyRelations, contextRequest.CustomQuery.ExtraParams);
				if (contextRequest.CustomQuery.SpecificProperties.Count > 0)
                {

				includeAll = false; 
                }

				StringBuilder sbSelect = new StringBuilder();
                sbSelect.Append("new (");
                bool existPrev = false;
                foreach (var selected in contextRequest.CustomQuery.SelectedFields.Where(p=> !string.IsNullOrEmpty(p.Linq)))
                {
                    if (existPrev) sbSelect.Append(", ");
                    if (!selected.Linq.Contains(".") && !selected.Linq.StartsWith("it."))
                        sbSelect.Append("it." + selected.Linq);
                    else
                        sbSelect.Append(selected.Linq);
                    existPrev = true;
                }
                sbSelect.Append(")");
                var queryable = _queryable.Select(sbSelect.ToString());                    


     				
                 if (!string.IsNullOrEmpty(predicateWithFKAndComputed))
                    queryable = queryable.Where(predicateWithFKAndComputed, contextRequest.CustomQuery.ExtraParams);

				QueryComplementOptions queryOps = ContextQueryBuilder.ApplyContextQuery(contextRequest);
            	if (!string.IsNullOrEmpty(queryOps.OrderByAndSort)){
					if (queryOps.OrderBy.Contains(".") && !queryOps.OrderBy.StartsWith("it.")) queryOps.OrderBy = "it." + queryOps.OrderBy;
					queryable = queryable.OrderBy(queryOps.OrderByAndSort);
					}
               	if (queryOps.Skip != null)
                {
                    queryable = queryable.Skip(queryOps.Skip.Value);
                }
                if (queryOps.PageSize != null)
                {
                    queryable = queryable.Take (queryOps.PageSize.Value);
                }


                var resultTemp = queryable.AsQueryable().ToListAsync().Result;
                foreach (var item in resultTemp)
                {

				   result.Add(SFSdotNet.Framework.BR.Utils.GetConverted<AcademyLevel,dynamic>(item, contextRequest.CustomQuery.SelectedFields.Select(p=>p.Name).ToArray()));
                }

			 if (e != null)
                {
                    e.Items = result;
                }
				 contextRequest.CustomQuery = new CustomQuery();
				OnTaken(this, e == null ? e = new BusinessRulesEventArgs<AcademyLevel>() { Items= result, IncludingComputedLinq = true, ContextRequest = contextRequest, FilterExpressionString  = predicateString } :  e);
  
			
  
                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
	
	*/
	#endregion

            }
        }
		public AcademyLevel GetFromOperation(string function, string filterString, string usemode, string fields, ContextRequest contextRequest)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
                string computedFields = "";
               // string fkIncludes = "accContpaqiClassification,accProjectConcept,accProjectType,accProxyUser";
                List<string> multilangProperties = new List<string>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					if (contextRequest != null && contextRequest.Company != null)
					{
						multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
						contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
					}
					 									
					string multiTenantField = "GuidCompany";


                return GetSummaryOperation(con, new AcademyLevel(), function, filterString, usemode, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields, contextRequest, fields.Split(char.Parse(",")).ToArray());
            }
        }

   protected override void QueryBuild(string predicate, Filter filter, DbContext efContext, ContextRequest contextRequest, string method, List<string> includesList)
      	{
				if (contextRequest.CustomQuery.SpecificProperties.Count == 0)
                {
					contextRequest.CustomQuery.SpecificProperties.Add(AcademyLevel.PropertyNames.Title);
					contextRequest.CustomQuery.SpecificProperties.Add(AcademyLevel.PropertyNames.GuidCompany);
					contextRequest.CustomQuery.SpecificProperties.Add(AcademyLevel.PropertyNames.CreatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(AcademyLevel.PropertyNames.UpdatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(AcademyLevel.PropertyNames.CreatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(AcademyLevel.PropertyNames.UpdatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(AcademyLevel.PropertyNames.Bytes);
					contextRequest.CustomQuery.SpecificProperties.Add(AcademyLevel.PropertyNames.IsDeleted);
                    
				}

				if (method == "getby" || method == "sum")
				{
					if (!contextRequest.CustomQuery.SpecificProperties.Contains("GuidAcademyLevel")){
						contextRequest.CustomQuery.SpecificProperties.Add("GuidAcademyLevel");
					}

					 if (!string.IsNullOrEmpty(contextRequest.CustomQuery.OrderBy))
					{
						string existPropertyOrderBy = contextRequest.CustomQuery.OrderBy;
						if (contextRequest.CustomQuery.OrderBy.Contains("."))
						{
							existPropertyOrderBy = contextRequest.CustomQuery.OrderBy.Split(char.Parse("."))[0];
						}
						if (!contextRequest.CustomQuery.SpecificProperties.Exists(p => p == existPropertyOrderBy))
						{
							contextRequest.CustomQuery.SpecificProperties.Add(existPropertyOrderBy);
						}
					}

				}
				
	bool isFullDetails = contextRequest.IsFromUI("AcademyLevels", UIActions.GetForDetails);
	string filterForTest = predicate  + filter.GetFilterComplete();

				if (isFullDetails || !string.IsNullOrEmpty(predicate))
            {
            } 

			if (method == "sum")
            {
            } 
			if (contextRequest.CustomQuery.SelectedFields.Count == 0)
            {
				foreach (var selected in contextRequest.CustomQuery.SpecificProperties)
                {
					string linq = selected;
					switch (selected)
                    {

					 
						
					 default:
                            break;
                    }
					contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name=selected, Linq=linq});
					if (method == "getby" || method == "sum")
					{
						if (includesList.Contains(selected))
							includesList.Remove(selected);

					}

				}
			}
				if (method == "getby" || method == "sum")
				{
					foreach (var otherInclude in includesList.Where(p=> !string.IsNullOrEmpty(p)))
					{
						contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name = otherInclude, Linq = "it." + otherInclude +" as " + otherInclude });
					}
				}
				BusinessRulesEventArgs<AcademyLevel> e = null;
				if (contextRequest.PreventInterceptors == false)
					OnQuerySettings(efContext, e = new BusinessRulesEventArgs<AcademyLevel>() { Filter = filter, ContextRequest = contextRequest /*, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null)*/ });

				//List<AcademyLevel> result = new List<AcademyLevel>();
                 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }

}
		public List<AcademyLevel> GetBy(Expression<Func<AcademyLevel, bool>> predicate, bool loadRelations, ContextRequest contextRequest)
        {
			if(!loadRelations)
				return GetBy(predicate, contextRequest);
			else
				return GetBy(predicate, contextRequest, "Students,Profesors");

        }

        public List<AcademyLevel> GetBy(Expression<Func<AcademyLevel, bool>> predicate, int? pageSize, int? page, string orderBy, SFSdotNet.Framework.Data.SortDirection? sortDirection)
        {
            return GetBy(predicate, new ContextRequest() { CustomQuery = new CustomQuery() { Page = page, PageSize = pageSize, OrderBy = orderBy, SortDirection = sortDirection } });
        }
        public List<AcademyLevel> GetBy(Expression<Func<AcademyLevel, bool>> predicate)
        {

			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
			contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
			            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            contextRequest.CustomQuery.FilterExpressionString = null;
            return this.GetBy(predicate, contextRequest, "");
        }
        #endregion
        #region Dynamic String
		protected override string GetSpecificFilter(string filter, ContextRequest contextRequest) {
            string result = "";
		    //string linqFilter = String.Empty;
            string freeTextFilter = String.Empty;
            if (filter.Contains("|"))
            {
               // linqFilter = filter.Split(char.Parse("|"))[0];
                freeTextFilter = filter.Split(char.Parse("|"))[1];
            }
            //else {
            //    freeTextFilter = filter;
            //}
            //else {
            //    linqFilter = filter;
            //}
			// linqFilter = SFSdotNet.Framework.Linq.Utils.ReplaceCustomDateFilters(linqFilter);
            //string specificFilter = linqFilter;
            if (!string.IsNullOrEmpty(freeTextFilter))
            {
                System.Text.StringBuilder sbCont = new System.Text.StringBuilder();
                /*if (specificFilter.Length > 0)
                {
                    sbCont.Append(" AND ");
                    sbCont.Append(" ({0})");
                }
                else
                {
                    sbCont.Append("{0}");
                }*/
                //var words = freeTextFilter.Split(char.Parse(" "));
				var word = freeTextFilter;
                System.Text.StringBuilder sbSpec = new System.Text.StringBuilder();
                 int nWords = 1;
				/*foreach (var word in words)
                {
					if (word.Length > 0){
                    if (sbSpec.Length > 0) sbSpec.Append(" AND ");
					if (words.Length > 1) sbSpec.Append("("); */
					
	
					
					
					
									
					sbSpec.Append(string.Format(@"Title.Contains(""{0}"")", word));
					

					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
								 //sbSpec.Append("*extraFreeText*");

                    /*if (words.Length > 1) sbSpec.Append(")");
					
					nWords++;

					}

                }*/
                //specificFilter = string.Format("{0}{1}", specificFilter, string.Format(sbCont.ToString(), sbSpec.ToString()));
                                 result = sbSpec.ToString();  
            }
			//result = specificFilter;
			
			return result;

		}
	
			public List<AcademyLevel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params object[] extraParams)
        {
			return GetBy(filter, pageSize, page, orderBy, orderDir,  null, extraParams);
		}
           public List<AcademyLevel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, string usemode, params object[] extraParams)
            { 
                return GetBy(filter, pageSize, page, orderBy, orderDir, usemode, null, extraParams);
            }


		public List<AcademyLevel> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  string usemode, ContextRequest context, params object[] extraParams)

        {

            // string freetext = null;
            //if (filter.Contains("|"))
            //{
            //    int parts = filter.Split(char.Parse("|")).Count();
            //    if (parts > 1)
            //    {

            //        freetext = filter.Split(char.Parse("|"))[1];
            //    }
            //}
		
            //string specificFilter = "";
            //if (!string.IsNullOrEmpty(filter))
            //  specificFilter=  GetSpecificFilter(filter);
            if (string.IsNullOrEmpty(orderBy))
            {
			                orderBy = "UpdatedDate";
            }
			//orderDir = "desc";
			SFSdotNet.Framework.Data.SortDirection direction = SFSdotNet.Framework.Data.SortDirection.Ascending;
            if (!string.IsNullOrEmpty(orderDir))
            {
                if (orderDir == "desc")
                    direction = SFSdotNet.Framework.Data.SortDirection.Descending;
            }
            if (context == null)
                context = new ContextRequest();
			

             context.UseMode = usemode;
             if (context.CustomQuery == null )
                context.CustomQuery =new SFSdotNet.Framework.My.CustomQuery();

 
                context.CustomQuery.ExtraParams = extraParams;

                    context.CustomQuery.OrderBy = orderBy;
                   context.CustomQuery.SortDirection = direction;
                   context.CustomQuery.Page = page;
                  context.CustomQuery.PageSize = pageSize;
               

            

            if (!preventSecurityRestrictions) {
			 if (context.CurrentContext == null)
                {
					if (SFSdotNet.Framework.My.Context.CurrentContext != null &&  SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
					{
						context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
						context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

					}
					else {
						throw new Exception("The security rule require a specific user and company");
					}
				}
            }
            return GetBy(filter, context);
  
        }


        public List<AcademyLevel> GetBy(string strWhere, ContextRequest contextRequest)
        {
        	#region old code
				
				 //Expression<Func<tvsReservationTransport, bool>> predicate = null;
				string strWhereClean = strWhere.Replace("*extraFreeText*", "").Replace("()", "");
                //if (!string.IsNullOrEmpty(strWhereClean)){

                //    object[] extraParams = null;
                //    //if (contextRequest != null )
                //    //    if (contextRequest.CustomQuery != null )
                //    //        extraParams = contextRequest.CustomQuery.ExtraParams;
                //    //predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<tvsReservationTransport, bool>(strWhereClean, extraParams != null? extraParams.Cast<Guid>(): null);				
                //}
				 if (contextRequest == null)
                {
                    contextRequest = new ContextRequest();
                    if (contextRequest.CustomQuery == null)
                        contextRequest.CustomQuery = new CustomQuery();
                }
                  if (!preventSecurityRestrictions) {
					if (contextRequest.User == null || contextRequest.Company == null)
                      {
                     if (SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
                     {
                         contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                         contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

                     }
                     else {
                         throw new Exception("The security rule require a specific User and Company ");
                     }
					 }
                 }
            contextRequest.CustomQuery.FilterExpressionString = strWhere;
				//return GetBy(predicate, contextRequest);  

			#endregion				
				
                    return GetBy(strWhere, contextRequest, "");  


        }
       public List<AcademyLevel> GetBy(string strWhere)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
			
            return GetBy(strWhere, context, null);
        }

        public List<AcademyLevel> GetBy(string strWhere, string includes)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
            return GetBy(strWhere, context, includes);
        }

        #endregion
        #endregion
		
		  #region SaveOrUpdate
        
 		 public AcademyLevel Create(AcademyLevel entity)
        {
				//ObjectContext context = null;
				    if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: Create");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

				return this.Create(entity, contextRequest);


        }
        
       
        public AcademyLevel Create(AcademyLevel entity, ContextRequest contextRequest)
        {
		
		bool graph = false;
	
				bool preventPartial = false;
                if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
               
			using (EFContext con = new EFContext()) {

				AcademyLevel itemForSave = new AcademyLevel();
#region Autos
		if(!preventSecurityRestrictions){

				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion
               BusinessRulesEventArgs<AcademyLevel> e = null;
			    if (preventPartial == false )
                OnCreating(this,e = new BusinessRulesEventArgs<AcademyLevel>() { ContextRequest = contextRequest, Item=entity });
				   if (e != null) {
						if (e.Cancel)
						{
							context = null;
							return e.Item;

						}
					}

                    if (entity.GuidAcademyLevel == Guid.Empty)
                   {
                       entity.GuidAcademyLevel = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   itemForSave.GuidAcademyLevel = entity.GuidAcademyLevel;
				  
		
			itemForSave.GuidAcademyLevel = entity.GuidAcademyLevel;

			itemForSave.Title = entity.Title;

			itemForSave.GuidCompany = entity.GuidCompany;

			itemForSave.CreatedDate = entity.CreatedDate;

			itemForSave.UpdatedDate = entity.UpdatedDate;

			itemForSave.CreatedBy = entity.CreatedBy;

			itemForSave.UpdatedBy = entity.UpdatedBy;

			itemForSave.Bytes = entity.Bytes;

			itemForSave.IsDeleted = entity.IsDeleted;

				
				con.AcademyLevels.Add(itemForSave);






                
				con.ChangeTracker.Entries().Where(p => p.Entity != itemForSave && p.State != EntityState.Unchanged).ForEach(p => p.State = EntityState.Detached);

				con.Entry<AcademyLevel>(itemForSave).State = EntityState.Added;

				con.SaveChanges();

					 
				

				//itemResult = entity;
                //if (e != null)
                //{
                 //   e.Item = itemResult;
                //}
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false )
                OnCreated(this, e == null ? e = new BusinessRulesEventArgs<AcademyLevel>() { ContextRequest = contextRequest, Item = entity } : e);



                if (e != null && e.Item != null )
                {
                    return e.Item;
                }
                              return entity;
			}
            
        }
        //BusinessRulesEventArgs<AcademyLevel> e = null;
        public void Create(List<AcademyLevel> entities)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            Create(entities, contextRequest);
        }
        public void Create(List<AcademyLevel> entities, ContextRequest contextRequest)
        
        {
			ObjectContext context = null;
            	foreach (AcademyLevel entity in entities)
				{
					this.Create(entity, contextRequest);
				}
        }
		  public void CreateOrUpdateBulk(List<AcademyLevel> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "cu", contextRequest);
        }

        private void CreateOrUpdateBulk(List<AcademyLevel> entities, string actionKey, ContextRequest contextRequest)
        {
			if (entities.Count() > 0){
            bool graph = false;

            bool preventPartial = false;
            if (contextRequest != null && contextRequest.PreventInterceptors == true)
            {
                preventPartial = true;
            }
            foreach (var entity in entities)
            {
                    if (entity.GuidAcademyLevel == Guid.Empty)
                   {
                       entity.GuidAcademyLevel = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   
				  


#region Autos
		if(!preventSecurityRestrictions){


 if (actionKey != "u")
                        {
				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;


}
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion


		
			//entity.GuidAcademyLevel = entity.GuidAcademyLevel;

			//entity.Title = entity.Title;

			//entity.GuidCompany = entity.GuidCompany;

			//entity.CreatedDate = entity.CreatedDate;

			//entity.UpdatedDate = entity.UpdatedDate;

			//entity.CreatedBy = entity.CreatedBy;

			//entity.UpdatedBy = entity.UpdatedBy;

			//entity.Bytes = entity.Bytes;

			//entity.IsDeleted = entity.IsDeleted;

				
				






                
				

					 
				

				//itemResult = entity;
            }
            using (EFContext con = new EFContext())
            {
                 if (actionKey == "c")
                    {
                        context.BulkInsert(entities);
                    }else if ( actionKey == "u")
                    {
                        context.BulkUpdate(entities);
                    }else
                    {
                        context.BulkInsertOrUpdate(entities);
                    }
            }

			}
        }
	
		public void CreateBulk(List<AcademyLevel> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "c", contextRequest);
        }


		public void UpdateAgile(AcademyLevel item, params string[] fields)
         {
			UpdateAgile(item, null, fields);
        }
		public void UpdateAgile(AcademyLevel item, ContextRequest contextRequest, params string[] fields)
         {
            
             ContextRequest contextNew = null;
             if (contextRequest != null)
             {
                 contextNew = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
                 if (fields != null && fields.Length > 0)
                 {
                     contextNew.CustomQuery.SpecificProperties  = fields.ToList();
                 }
                 else if(contextRequest.CustomQuery.SpecificProperties.Count > 0)
                 {
                     fields = contextRequest.CustomQuery.SpecificProperties.ToArray();
                 }
             }
			

		   using (EFContext con = new EFContext())
            {



               
					List<string> propForCopy = new List<string>();
                    propForCopy.AddRange(fields);
                    
					  
					if (!propForCopy.Contains("GuidAcademyLevel"))
						propForCopy.Add("GuidAcademyLevel");

					var itemForUpdate = SFSdotNet.Framework.BR.Utils.GetConverted<AcademyLevel,AcademyLevel>(item, propForCopy.ToArray());
					 itemForUpdate.GuidAcademyLevel = item.GuidAcademyLevel;
                  var setT = con.Set<AcademyLevel>().Attach(itemForUpdate);

					if (fields.Count() > 0)
					  {
						  item.ModifiedProperties = fields;
					  }
                    foreach (var property in item.ModifiedProperties)
					{						
                        if (property != "GuidAcademyLevel")
                             con.Entry(setT).Property(property).IsModified = true;

                    }

                
               int result = con.SaveChanges();
               if (result != 1)
               {
                   SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: 1");
               }


            }

			OnUpdatedAgile(this, new BusinessRulesEventArgs<AcademyLevel>() { Item = item, ContextRequest = contextNew  });

         }
		public void UpdateBulk(List<AcademyLevel>  items, params string[] fields)
         {
             SFSdotNet.Framework.My.ContextRequest req = new SFSdotNet.Framework.My.ContextRequest();
             req.CustomQuery = new SFSdotNet.Framework.My.CustomQuery();
             foreach (var field in fields)
             {
                 req.CustomQuery.SpecificProperties.Add(field);
             }
             UpdateBulk(items, req);

         }

		 public void DeleteBulk(List<AcademyLevel> entities, ContextRequest contextRequest = null)
        {

            using (EFContext con = new EFContext())
            {
                foreach (var entity in entities)
                {
					var entityProxy = new AcademyLevel() { GuidAcademyLevel = entity.GuidAcademyLevel };

                    con.Entry<AcademyLevel>(entityProxy).State = EntityState.Deleted;

                }

                int result = con.SaveChanges();
                if (result != entities.Count)
                {
                    SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: " + entities.Count.ToString());
                }
            }

        }

        public void UpdateBulk(List<AcademyLevel> items, ContextRequest contextRequest)
        {
            if (items.Count() > 0){

			 foreach (var entity in items)
            {


#region Autos
		if(!preventSecurityRestrictions){

				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	



			}
#endregion







				}
				using (EFContext con = new EFContext())
				{

                    
                
                   con.BulkUpdate(items);

				}
             
			}	  
        }

         public AcademyLevel Update(AcademyLevel entity)
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return Update(entity, contextRequest);
        }
       
         public AcademyLevel Update(AcademyLevel entity, ContextRequest contextRequest)
        {
		 if ((System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null) && contextRequest == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Update");
            }
            if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            }

			
				AcademyLevel  itemResult = null;

	
			//entity.UpdatedDate = DateTime.Now.ToUniversalTime();
			//if(contextRequest.User != null)
				//entity.UpdatedBy = contextRequest.User.GuidUser;

//	    var oldentity = GetBy(p => p.GuidAcademyLevel == entity.GuidAcademyLevel, contextRequest).FirstOrDefault();
	//	if (oldentity != null) {
		
          //  entity.CreatedDate = oldentity.CreatedDate;
    //        entity.CreatedBy = oldentity.CreatedBy;
	
      //      entity.GuidCompany = oldentity.GuidCompany;
	
			

	
		//}

			 using( EFContext con = new EFContext()){
				BusinessRulesEventArgs<AcademyLevel> e = null;
				bool preventPartial = false; 
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false)
                OnUpdating(this,e = new BusinessRulesEventArgs<AcademyLevel>() { ContextRequest = contextRequest, Item=entity});
				   if (e != null) {
						if (e.Cancel)
						{
							//outcontext = null;
							return e.Item;

						}
					}

	string includes = "";
	IQueryable < AcademyLevel > query = con.AcademyLevels.AsQueryable();
	foreach (string include in includes.Split(char.Parse(",")))
                       {
                           if (!string.IsNullOrEmpty(include))
                               query = query.Include(include);
                       }
	var oldentity = query.FirstOrDefault(p => p.GuidAcademyLevel == entity.GuidAcademyLevel);
	if (oldentity.Title != entity.Title)
		oldentity.Title = entity.Title;

				//if (entity.UpdatedDate == null || (contextRequest != null && contextRequest.IsFromUI("AcademyLevels", UIActions.Updating)))
			oldentity.UpdatedDate = DateTime.Now.ToUniversalTime();
			if(contextRequest.User != null)
				oldentity.UpdatedBy = contextRequest.User.GuidUser;

           


				if (entity.Students != null)
                {
                    foreach (var item in entity.Students)
                    {


                        
                    }
					
                    

                }


				if (entity.Profesors != null)
                {
                    foreach (var item in entity.Profesors)
                    {


                        
                    }
					
                    

                }


				con.ChangeTracker.Entries().Where(p => p.Entity != oldentity).ForEach(p => p.State = EntityState.Unchanged);  
				  
				con.SaveChanges();
        
					 
					
               
				itemResult = entity;
				if(preventPartial == false)
					OnUpdated(this, e = new BusinessRulesEventArgs<AcademyLevel>() { ContextRequest = contextRequest, Item=itemResult });

              	return itemResult;
			}
			  
        }
        public AcademyLevel Save(AcademyLevel entity)
        {
			return Create(entity);
        }
        public int Save(List<AcademyLevel> entities)
        {
			 Create(entities);
            return entities.Count;

        }
        #endregion
        #region Delete
        public void Delete(AcademyLevel entity)
        {
				this.Delete(entity, null);
			
        }
		 public void Delete(AcademyLevel entity, ContextRequest contextRequest)
        {
				
				  List<AcademyLevel> entities = new List<AcademyLevel>();
				   entities.Add(entity);
				this.Delete(entities, contextRequest);
			
        }

         public void Delete(string query, Guid[] guids, ContextRequest contextRequest)
        {
			var br = new AcademyLevelsBR(true);
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);
            
            Delete(items, contextRequest);

        }
        public void Delete(AcademyLevel entity,  ContextRequest contextRequest, BusinessRulesEventArgs<AcademyLevel> e = null)
        {
			
				using(EFContext con = new EFContext())
                 {
				
               	BusinessRulesEventArgs<AcademyLevel> _e = null;
               List<AcademyLevel> _items = new List<AcademyLevel>();
                _items.Add(entity);
                if (e == null || e.PreventPartialPropagate == false)
                {
                    OnDeleting(this, _e = (e == null ? new BusinessRulesEventArgs<AcademyLevel>() { ContextRequest = contextRequest, Item = entity, Items = null  } : e));
                }
                if (_e != null)
                {
                    if (_e.Cancel)
						{
							context = null;
							return;

						}
					}


				
									//IsDeleted
					bool logicDelete = true;
					if (entity.IsDeleted != null)
					{
						if (entity.IsDeleted.Value)
							logicDelete = false;
					}
					if (logicDelete)
					{
											//entity = GetBy(p =>, contextRequest).FirstOrDefault();
						entity.IsDeleted = true;
						if (contextRequest != null && contextRequest.User != null)
							entity.UpdatedBy = contextRequest.User.GuidUser;
                        entity.UpdatedDate = DateTime.UtcNow;
						UpdateAgile(entity, "IsDeleted","UpdatedBy","UpdatedDate");

						
					}
					else {
					con.Entry<AcademyLevel>(entity).State = EntityState.Deleted;
					con.SaveChanges();
				
				 
					}
								
				
				 
					
					
			if (e == null || e.PreventPartialPropagate == false)
                {

                    if (_e == null)
                        _e = new BusinessRulesEventArgs<AcademyLevel>() { ContextRequest = contextRequest, Item = entity, Items = null };

                    OnDeleted(this, _e);
                }

				//return null;
			}
        }
 public void UnDelete(string query, Guid[] guids, ContextRequest contextRequest)
        {
            var br = new AcademyLevelsBR(true);
            contextRequest.CustomQuery.IncludeDeleted = true;
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);

            foreach (var item in items)
            {
                item.IsDeleted = false;
						if (contextRequest != null && contextRequest.User != null)
							item.UpdatedBy = contextRequest.User.GuidUser;
                        item.UpdatedDate = DateTime.UtcNow;
            }

            UpdateBulk(items, "IsDeleted","UpdatedBy","UpdatedDate");
        }

         public void Delete(List<AcademyLevel> entities,  ContextRequest contextRequest = null )
        {
				
			 BusinessRulesEventArgs<AcademyLevel> _e = null;

                OnDeleting(this, _e = new BusinessRulesEventArgs<AcademyLevel>() { ContextRequest = contextRequest, Item = null, Items = entities });
                if (_e != null)
                {
                    if (_e.Cancel)
                    {
                        context = null;
                        return;

                    }
                }
                bool allSucced = true;
                BusinessRulesEventArgs<AcademyLevel> eToChilds = new BusinessRulesEventArgs<AcademyLevel>();
                if (_e != null)
                {
                    eToChilds = _e;
                }
                else
                {
                    eToChilds = new BusinessRulesEventArgs<AcademyLevel>() { ContextRequest = contextRequest, Item = (entities.Count == 1 ? entities[0] : null), Items = entities };
                }
				foreach (AcademyLevel item in entities)
				{
					try
                    {
                        this.Delete(item, contextRequest, e: eToChilds);
                    }
                    catch (Exception ex)
                    {
                        SFSdotNet.Framework.My.EventLog.Error(ex);
                        allSucced = false;
                    }
				}
				if (_e == null)
                    _e = new BusinessRulesEventArgs<AcademyLevel>() { ContextRequest = contextRequest, CountResult = entities.Count, Item = null, Items = entities };
                OnDeleted(this, _e);

			
        }
        #endregion
 
        #region GetCount
		 public int GetCount(Expression<Func<AcademyLevel, bool>> predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

			return GetCount(predicate, contextRequest);
		}
        public int GetCount(Expression<Func<AcademyLevel, bool>> predicate, ContextRequest contextRequest)
        {


		
		 using (EFContext con = new EFContext())
            {


				if (predicate == null) predicate = PredicateBuilder.True<AcademyLevel>();
           		predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted == null);
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    		if (contextRequest.User !=null )
                        		if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
									predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa

								}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				
				IQueryable<AcademyLevel> query = con.AcademyLevels.AsQueryable();
                return query.AsExpandable().Count(predicate);

			
				}
			

        }
		  public int GetCount(string predicate,  ContextRequest contextRequest)
         {
             return GetCount(predicate, null, contextRequest);
         }

         public int GetCount(string predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return GetCount(predicate, contextRequest);
        }
		 public int GetCount(string predicate, string usemode){
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
				return GetCount( predicate,  usemode,  contextRequest);
		 }
        public int GetCount(string predicate, string usemode, ContextRequest contextRequest){

		using (EFContext con = new EFContext()) {
				string computedFields = "";
				string fkIncludes = "";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<AcademyLevel>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetCount(con, predicate, usemode, contextRequest, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields);

			}
			#region old code
			 /* string freetext = null;
            Filter filter = new Filter();

              if (predicate.Contains("|"))
              {
                 
                  filter.SetFilterPart("ft", GetSpecificFilter(predicate, contextRequest));
                 
                  filter.ProcessText(predicate.Split(char.Parse("|"))[0]);
                  freetext = predicate.Split(char.Parse("|"))[1];

				  if (!string.IsNullOrEmpty(freetext) && string.IsNullOrEmpty(contextRequest.FreeText))
                  {
                      contextRequest.FreeText = freetext;
                  }
              }
              else {
                  filter.ProcessText(predicate);
              }
			   predicate = filter.GetFilterComplete();
			// BusinessRulesEventArgs<AcademyLevel>  e = null;
           	using (EFContext con = new EFContext())
			{
			
			

			 QueryBuild(predicate, filter, con, contextRequest, "count", new List<string>());


			
			BusinessRulesEventArgs<AcademyLevel> e = null;

			contextRequest.FreeText = freetext;
			contextRequest.UseMode = usemode;
            OnCounting(this, e = new BusinessRulesEventArgs<AcademyLevel>() {  Filter =filter, ContextRequest = contextRequest });
            if (e != null)
            {
                if (e.Cancel)
                {
                    context = null;
                    return e.CountResult;

                }

            

            }
			
			StringBuilder sbQuerySystem = new StringBuilder();
		
					
                    filter.SetFilterPart("de","(IsDeleted != true OR IsDeleted == null)");
			
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    	if (contextRequest.User !=null )
                        	if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
                        		
								filter.SetFilterPart("co", @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa
						
						
							}
							
							}
							if (preventSecurityRestrictions) preventSecurityRestrictions= false;
		
				   
                 filter.CleanAndProcess("");
				//string predicateWithFKAndComputed = SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicate );               
				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed();
               string predicateWithManyRelations = filter.GetFilterChildren();
			   ///QueryUtils.BreakeQuery1(predicate, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
			   predicate = filter.GetFilterComplete();
               if (!string.IsNullOrEmpty(predicate))
               {
				
					
                    return con.AcademyLevels.Where(predicate).Count();
					
                }else
                    return con.AcademyLevels.Count();
					
			}*/
			#endregion

		}
         public int GetCount()
        {
            return GetCount(p => true);
        }
        #endregion
        
         


        public void Delete(List<AcademyLevel.CompositeKey> entityKeys)
        {

            List<AcademyLevel> items = new List<AcademyLevel>();
            foreach (var itemKey in entityKeys)
            {
                items.Add(GetByKey(itemKey.GuidAcademyLevel));
            }

            Delete(items);

        }
		 public void UpdateAssociation(string relation, string relationValue, string query, Guid[] ids, ContextRequest contextRequest)
        {
            var items = GetBy(query, null, null, null, null, null, contextRequest, ids);
			 var module = SFSdotNet.Framework.Cache.Caching.SystemObjects.GetModuleByKey(SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(System.Web.HttpContext.Current.Request.RequestContext, "area"));
           
            foreach (var item in items)
            {
			  Guid ? guidRelationValue = null ;
                if (!string.IsNullOrEmpty(relationValue)){
                    guidRelationValue = Guid.Parse(relationValue );
                }

				 if (relation.Contains("."))
                {
                    var partsWithOtherProp = relation.Split(char.Parse("|"));
                    var parts = partsWithOtherProp[0].Split(char.Parse("."));

                    string proxyRelName = parts[0];
                    string proxyProperty = parts[1];
                    string proxyPropertyKeyNameFromOther = partsWithOtherProp[1];
                    //string proxyPropertyThis = parts[2];

                    var prop = item.GetType().GetProperty(proxyRelName);
                    //var entityInfo = //SFSdotNet.Framework.
                    // descubrir el tipo de entidad dentro de la colección
                    Type typeEntityInList = SFSdotNet.Framework.Entities.Utils.GetTypeFromList(prop);
                    var newProxyItem = Activator.CreateInstance(typeEntityInList);
                    var propThisForSet = newProxyItem.GetType().GetProperty(proxyProperty);
                    var entityInfoOfProxy = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(typeEntityInList);
                    var propOther = newProxyItem.GetType().GetProperty(proxyPropertyKeyNameFromOther);

                    if (propThisForSet != null && entityInfoOfProxy != null && propOther != null )
                    {
                        var entityInfoThis = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(item.GetType());
                        var valueThisId = item.GetType().GetProperty(entityInfoThis.PropertyKeyName).GetValue(item);
                        if (valueThisId != null)
                            propThisForSet.SetValue(newProxyItem, valueThisId);
                        propOther.SetValue(newProxyItem, Guid.Parse(relationValue));
                        
                        var entityNameProp = newProxyItem.GetType().GetField("EntityName").GetValue(null);
                        var entitySetNameProp = newProxyItem.GetType().GetField("EntitySetName").GetValue(null);

                        SFSdotNet.Framework.Apps.Integration.CreateItemFromApp(entityNameProp.ToString(), entitySetNameProp.ToString(), module.ModuleNamespace, newProxyItem, contextRequest);

                    }

                    // crear una instancia del tipo de entidad
                    // llenar los datos y registrar nuevo


                }
                else
                {
                var prop = item.GetType().GetProperty(relation);
                var entityInfo = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(prop.PropertyType);
                if (entityInfo != null)
                {
                    var ins = Activator.CreateInstance(prop.PropertyType);
                   if (guidRelationValue != null)
                    {
                        prop.PropertyType.GetProperty(entityInfo.PropertyKeyName).SetValue(ins, guidRelationValue);
                        item.GetType().GetProperty(relation).SetValue(item, ins);
                    }
                    else
                    {
                        item.GetType().GetProperty(relation).SetValue(item, null);
                    }

                    Update(item, contextRequest);
                }

				}
            }
        }
		
	}
		public partial class ProfesorsBR:BRBase<Profesor>{
	 	
           
		 #region Partial methods

           partial void OnUpdating(object sender, BusinessRulesEventArgs<Profesor> e);

            partial void OnUpdated(object sender, BusinessRulesEventArgs<Profesor> e);
			partial void OnUpdatedAgile(object sender, BusinessRulesEventArgs<Profesor> e);

            partial void OnCreating(object sender, BusinessRulesEventArgs<Profesor> e);
            partial void OnCreated(object sender, BusinessRulesEventArgs<Profesor> e);

            partial void OnDeleting(object sender, BusinessRulesEventArgs<Profesor> e);
            partial void OnDeleted(object sender, BusinessRulesEventArgs<Profesor> e);

            partial void OnGetting(object sender, BusinessRulesEventArgs<Profesor> e);
            protected override void OnVirtualGetting(object sender, BusinessRulesEventArgs<Profesor> e)
            {
                OnGetting(sender, e);
            }
			protected override void OnVirtualCounting(object sender, BusinessRulesEventArgs<Profesor> e)
            {
                OnCounting(sender, e);
            }
			partial void OnTaken(object sender, BusinessRulesEventArgs<Profesor> e);
			protected override void OnVirtualTaken(object sender, BusinessRulesEventArgs<Profesor> e)
            {
                OnTaken(sender, e);
            }

            partial void OnCounting(object sender, BusinessRulesEventArgs<Profesor> e);
 
			partial void OnQuerySettings(object sender, BusinessRulesEventArgs<Profesor> e);
          
            #endregion
			
		private static ProfesorsBR singlenton =null;
				public static ProfesorsBR NewInstance(){
					return  new ProfesorsBR();
					
				}
		public static ProfesorsBR Instance{
			get{
				if (singlenton == null)
					singlenton = new ProfesorsBR();
				return singlenton;
			}
		}
		//private bool preventSecurityRestrictions = false;
		 public bool PreventAuditTrail { get; set;  }
		#region Fields
        EFContext context = null;
        #endregion
        #region Constructor
        public ProfesorsBR()
        {
            context = new EFContext();
        }
		 public ProfesorsBR(bool preventSecurity)
            {
                this.preventSecurityRestrictions = preventSecurity;
				context = new EFContext();
            }
        #endregion
		
		#region Get

 		public IQueryable<Profesor> Get()
        {
            using (EFContext con = new EFContext())
            {
				
				var query = con.Profesors.AsQueryable();
                con.Configuration.ProxyCreationEnabled = false;

                //query = ContextQueryBuilder<Nutrient>.ApplyContextQuery(query, contextRequest);

                return query;




            }

        }
		


 	
		public List<Profesor> GetAll()
        {
            return this.GetBy(p => true);
        }
        public List<Profesor> GetAll(string includes)
        {
            return this.GetBy(p => true, includes);
        }
        public Profesor GetByKey(Guid guidProfesor)
        {
            return GetByKey(guidProfesor, true);
        }
        public Profesor GetByKey(Guid guidProfesor, bool loadIncludes)
        {
            Profesor item = null;
			var query = PredicateBuilder.True<Profesor>();
                    
			string strWhere = @"GuidProfesor = Guid(""" + guidProfesor.ToString()+@""")";
            Expression<Func<Profesor, bool>> predicate = null;
            //if (!string.IsNullOrEmpty(strWhere))
            //    predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<Profesor, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
			 ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
            contextRequest.CustomQuery.FilterExpressionString = strWhere;

			//item = GetBy(predicate, loadIncludes, contextRequest).FirstOrDefault();
			item = GetBy(strWhere,loadIncludes,contextRequest).FirstOrDefault();
            return item;
        }
         public List<Profesor> GetBy(string strWhere, bool loadRelations, ContextRequest contextRequest)
        {
            if (!loadRelations)
                return GetBy(strWhere, contextRequest);
            else
                return GetBy(strWhere, contextRequest, "");

        }
		  public List<Profesor> GetBy(string strWhere, bool loadRelations)
        {
              if (!loadRelations)
                return GetBy(strWhere, new ContextRequest());
            else
                return GetBy(strWhere, new ContextRequest(), "");

        }
		         public Profesor GetByKey(Guid guidProfesor, params Expression<Func<Profesor, object>>[] includes)
        {
            Profesor item = null;
			string strWhere = @"GuidProfesor = Guid(""" + guidProfesor.ToString()+@""")";
          Expression<Func<Profesor, bool>> predicate = p=> p.GuidProfesor == guidProfesor;
           // if (!string.IsNullOrEmpty(strWhere))
           //     predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<Profesor, bool>(strWhere.Replace("*extraFreeText*", "").Replace("()",""));
			
        item = GetBy(predicate, includes).FirstOrDefault();
         ////   item = GetBy(strWhere,includes).FirstOrDefault();
			return item;

        }
        public Profesor GetByKey(Guid guidProfesor, string includes)
        {
            Profesor item = null;
			string strWhere = @"GuidProfesor = Guid(""" + guidProfesor.ToString()+@""")";
            
			
            item = GetBy(strWhere, includes).FirstOrDefault();
            return item;

        }
		 public Profesor GetByKey(Guid guidProfesor, string usemode, string includes)
		{
			return GetByKey(guidProfesor, usemode, null, includes);

		 }
		 public Profesor GetByKey(Guid guidProfesor, string usemode, ContextRequest context,  string includes)
        {
            Profesor item = null;
			string strWhere = @"GuidProfesor = Guid(""" + guidProfesor.ToString()+@""")";
			if (context == null){
				context = new ContextRequest();
				context.CustomQuery = new CustomQuery();
				context.CustomQuery.IsByKey = true;
				context.CustomQuery.FilterExpressionString = strWhere;
				context.UseMode = usemode;
			}
            item = GetBy(strWhere,context , includes).FirstOrDefault();
            return item;

        }

        #region Dynamic Predicate
        public List<Profesor> GetBy(Expression<Func<Profesor, bool>> predicate, int? pageSize, int? page)
        {
            return this.GetBy(predicate, pageSize, page, null, null);
        }
        public List<Profesor> GetBy(Expression<Func<Profesor, bool>> predicate, ContextRequest contextRequest)
        {

            return GetBy(predicate, contextRequest,"");
        }
        
        public List<Profesor> GetBy(Expression<Func<Profesor, bool>> predicate, ContextRequest contextRequest, params Expression<Func<Profesor, object>>[] includes)
        {
            StringBuilder sb = new StringBuilder();
           if (includes != null)
            {
                foreach (var path in includes)
                {

						if (sb.Length > 0) sb.Append(",");
						sb.Append(SFSdotNet.Framework.Linq.Utils.IncludeToString<Profesor>(path));

               }
            }
            return GetBy(predicate, contextRequest, sb.ToString());
        }
        
        
        public List<Profesor> GetBy(Expression<Func<Profesor, bool>> predicate, string includes)
        {
			ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";

            return GetBy(predicate, context, includes);
        }

        public List<Profesor> GetBy(Expression<Func<Profesor, bool>> predicate, params Expression<Func<Profesor, object>>[] includes)
        {
			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest context = new ContextRequest();
			            context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = "";
            return GetBy(predicate, context, includes);
        }

      
		public bool DisableCache { get; set; }
		public List<Profesor> GetBy(Expression<Func<Profesor, bool>> predicate, ContextRequest contextRequest, string includes)
		{
            using (EFContext con = new EFContext()) {
				
				string fkIncludes = "AcademyLevel";
                List<string> multilangProperties = new List<string>();
				if (predicate == null) predicate = PredicateBuilder.True<Profesor>();
                var notDeletedExpression = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;
					Expression<Func<Profesor,bool>> multitenantExpression  = null;
					if (contextRequest != null && contextRequest.Company != null)	                        	
						multitenantExpression = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicate, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression);

#region Old code
/*
				List<Profesor> result = null;
               BusinessRulesEventArgs<Profesor>  e = null;
	
				OnGetting(con, e = new BusinessRulesEventArgs<Profesor>() {  FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null) });

               // OnGetting(con,e = new BusinessRulesEventArgs<Profesor>() { FilterExpression = predicate, ContextRequest = contextRequest, FilterExpressionString = contextRequest.CustomQuery.FilterExpressionString});
				   if (e != null) {
				    predicate = e.FilterExpression;
						if (e.Cancel)
						{
							context = null;
							 if (e.Items == null) e.Items = new List<Profesor>();
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                if (predicate == null) predicate = PredicateBuilder.True<Profesor>();
 				string fkIncludes = "AcademyLevel";
                if(contextRequest!=null){
					if (contextRequest.CustomQuery != null)
					{
						if (contextRequest.CustomQuery.IncludeForeignKeyPaths != null) {
							if (contextRequest.CustomQuery.IncludeForeignKeyPaths.Value == false)
								fkIncludes = "";
						}
					}
				}
				if (!string.IsNullOrEmpty(includes))
					includes = includes + "," + fkIncludes;
				else
					includes = fkIncludes;
                
                //var es = _repository.Queryable;

                IQueryable<Profesor> query =  con.Profesors.AsQueryable();

                                if (!string.IsNullOrEmpty(includes))
                {
                    foreach (string include in includes.Split(char.Parse(",")))
                    {
						if (!string.IsNullOrEmpty(include))
                            query = query.Include(include);
                    }
                }
                    predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
					 	if (!preventSecurityRestrictions)
						{
							if (contextRequest != null )
		                    	if (contextRequest.User !=null )
		                        	if (contextRequest.Company != null){
		                        	
										predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa
 									
									}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				query =query.AsExpandable().Where(predicate);
                query = ContextQueryBuilder<Profesor>.ApplyContextQuery(query, contextRequest);

                result = query.AsNoTracking().ToList<Profesor>();
				  
                if (e != null)
                {
                    e.Items = result;
                }
				//if (contextRequest != null ){
				//	 contextRequest = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
					contextRequest.CustomQuery = new CustomQuery();

				//}
				OnTaken(this, e == null ? e =  new BusinessRulesEventArgs<Profesor>() { Items= result, IncludingComputedLinq = false, ContextRequest = contextRequest,  FilterExpression = predicate } :  e);
  
			

                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
				*/
#endregion
            }
        }


		/*public int Update(List<Profesor> items, ContextRequest contextRequest)
            {
                int result = 0;
                using (EFContext con = new EFContext())
                {
                   
                

                    foreach (var item in items)
                    {
                        //secMessageToUser messageToUser = new secMessageToUser();
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            item.GetType().GetProperty(prop).SetValue(item, item.GetType().GetProperty(prop).GetValue(item));
                        }
                        //messageToUser.GuidMessageToUser = (Guid)item.GetType().GetProperty("GuidMessageToUser").GetValue(item);

                        var setObject = con.CreateObjectSet<Profesor>("Profesors");
                        //messageToUser.Readed = DateTime.UtcNow;
                        setObject.Attach(item);
                        foreach (var prop in contextRequest.CustomQuery.SpecificProperties)
                        {
                            con.ObjectStateManager.GetObjectStateEntry(item).SetModifiedProperty(prop);
                        }
                       
                    }
                    result = con.SaveChanges();

                    


                }
                return result;
            }
           */
		

        public List<Profesor> GetBy(string predicateString, ContextRequest contextRequest, string includes)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
				


				string computedFields = "";
				string fkIncludes = "AcademyLevel";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<Profesor>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					//if (contextRequest != null && contextRequest.Company != null)                      	
					//	 multitenantExpression = @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))";
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetBy(con, predicateString, contextRequest, includes, fkIncludes, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression,computedFields);


	#region Old Code
	/*
				BusinessRulesEventArgs<Profesor> e = null;

				Filter filter = new Filter();
                if (predicateString.Contains("|"))
                {
                    string ft = GetSpecificFilter(predicateString, contextRequest);
                    if (!string.IsNullOrEmpty(ft))
                        filter.SetFilterPart("ft", ft);
                   
                    contextRequest.FreeText = predicateString.Split(char.Parse("|"))[1];
                    var q1 = predicateString.Split(char.Parse("|"))[0];
                    if (!string.IsNullOrEmpty(q1))
                    {
                        filter.ProcessText(q1);
                    }
                }
                else {
                    filter.ProcessText(predicateString);
                }
				 var includesList = (new List<string>());
                 if (!string.IsNullOrEmpty(includes))
                 {
                     includesList = includes.Split(char.Parse(",")).ToList();
                 }

				List<Profesor> result = new List<Profesor>();
         
			QueryBuild(predicateString, filter, con, contextRequest, "getby", includesList);
			 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }
				
				
					OnGetting(con, e == null ? e = new BusinessRulesEventArgs<Profesor>() { Filter = filter, ContextRequest = contextRequest  } : e );

                  //OnGetting(con,e = new BusinessRulesEventArgs<Profesor>() {  ContextRequest = contextRequest, FilterExpressionString = predicateString });
			   	if (e != null) {
				    //predicateString = e.GetQueryString();
						if (e.Cancel)
						{
							context = null;
							return e.Items;

						}
						if (!string.IsNullOrEmpty(e.StringIncludes))
                            includes = e.StringIncludes;
					}
				//	 else {
                //      predicateString = predicateString.Replace("*extraFreeText*", "").Replace("()","");
                //  }
				//con.EnableChangeTrackingUsingProxies = false;
				con.Configuration.ProxyCreationEnabled = false;
                con.Configuration.AutoDetectChangesEnabled = false;
                con.Configuration.ValidateOnSaveEnabled = false;

                //if (predicate == null) predicate = PredicateBuilder.True<Profesor>();
 				string fkIncludes = "AcademyLevel";
                if(contextRequest!=null){
					if (contextRequest.CustomQuery != null)
					{
						if (contextRequest.CustomQuery.IncludeForeignKeyPaths != null) {
							if (contextRequest.CustomQuery.IncludeForeignKeyPaths.Value == false)
								fkIncludes = "";
						}
					}
				}else{
                    contextRequest = new ContextRequest();
                    contextRequest.CustomQuery = new CustomQuery();

                }
				if (!string.IsNullOrEmpty(includes))
					includes = includes + "," + fkIncludes;
				else
					includes = fkIncludes;
                
                //var es = _repository.Queryable;
				IQueryable<Profesor> query = con.Profesors.AsQueryable();
		
				// include relations FK
				if(string.IsNullOrEmpty(includes) ){
					includes ="";
				}
				StringBuilder sbQuerySystem = new StringBuilder();
                    //predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted ==null );
				

				//if (!string.IsNullOrEmpty(predicateString))
                //      sbQuerySystem.Append(" And ");
                //sbQuerySystem.Append(" (IsDeleted != true Or IsDeleted = null) ");
				 filter.SetFilterPart("de", "(IsDeleted != true OR IsDeleted = null)");


					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
	                    	if (contextRequest.User !=null )
	                        	if (contextRequest.Company != null ){
	                        		//if (sbQuerySystem.Length > 0)
	                        		//	    			sbQuerySystem.Append( " And ");	
									//sbQuerySystem.Append(@" (GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa

									filter.SetFilterPart("co",@"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @"""))");

								}
						}	
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				//string predicateString = predicate.ToDynamicLinq<Profesor>();
				//predicateString += sbQuerySystem.ToString();
				filter.CleanAndProcess("");

				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed(); //SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicateString );               
                string predicateWithManyRelations = filter.GetFilterChildren(); //SFSdotNet.Framework.Linq.Utils.CleanPartExpression(predicateString);

                //QueryUtils.BreakeQuery1(predicateString, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
                var _queryable = query.AsQueryable();
				bool includeAll = true; 
                if (!string.IsNullOrEmpty(predicateWithManyRelations))
                    _queryable = _queryable.Where(predicateWithManyRelations, contextRequest.CustomQuery.ExtraParams);
				if (contextRequest.CustomQuery.SpecificProperties.Count > 0)
                {

				includeAll = false; 
                }

				StringBuilder sbSelect = new StringBuilder();
                sbSelect.Append("new (");
                bool existPrev = false;
                foreach (var selected in contextRequest.CustomQuery.SelectedFields.Where(p=> !string.IsNullOrEmpty(p.Linq)))
                {
                    if (existPrev) sbSelect.Append(", ");
                    if (!selected.Linq.Contains(".") && !selected.Linq.StartsWith("it."))
                        sbSelect.Append("it." + selected.Linq);
                    else
                        sbSelect.Append(selected.Linq);
                    existPrev = true;
                }
                sbSelect.Append(")");
                var queryable = _queryable.Select(sbSelect.ToString());                    


     				
                 if (!string.IsNullOrEmpty(predicateWithFKAndComputed))
                    queryable = queryable.Where(predicateWithFKAndComputed, contextRequest.CustomQuery.ExtraParams);

				QueryComplementOptions queryOps = ContextQueryBuilder.ApplyContextQuery(contextRequest);
            	if (!string.IsNullOrEmpty(queryOps.OrderByAndSort)){
					if (queryOps.OrderBy.Contains(".") && !queryOps.OrderBy.StartsWith("it.")) queryOps.OrderBy = "it." + queryOps.OrderBy;
					queryable = queryable.OrderBy(queryOps.OrderByAndSort);
					}
               	if (queryOps.Skip != null)
                {
                    queryable = queryable.Skip(queryOps.Skip.Value);
                }
                if (queryOps.PageSize != null)
                {
                    queryable = queryable.Take (queryOps.PageSize.Value);
                }


                var resultTemp = queryable.AsQueryable().ToListAsync().Result;
                foreach (var item in resultTemp)
                {

				   result.Add(SFSdotNet.Framework.BR.Utils.GetConverted<Profesor,dynamic>(item, contextRequest.CustomQuery.SelectedFields.Select(p=>p.Name).ToArray()));
                }

			 if (e != null)
                {
                    e.Items = result;
                }
				 contextRequest.CustomQuery = new CustomQuery();
				OnTaken(this, e == null ? e = new BusinessRulesEventArgs<Profesor>() { Items= result, IncludingComputedLinq = true, ContextRequest = contextRequest, FilterExpressionString  = predicateString } :  e);
  
			
  
                if (e != null) {
                    //if (e.ReplaceResult)
                        result = e.Items;
                }
                return result;
	
	*/
	#endregion

            }
        }
		public Profesor GetFromOperation(string function, string filterString, string usemode, string fields, ContextRequest contextRequest)
        {
            using (EFContext con = new EFContext(contextRequest))
            {
                string computedFields = "";
               // string fkIncludes = "accContpaqiClassification,accProjectConcept,accProjectType,accProxyUser";
                List<string> multilangProperties = new List<string>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
					if (contextRequest != null && contextRequest.Company != null)
					{
						multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
						contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
					}
					 									
					string multiTenantField = "GuidCompany";


                return GetSummaryOperation(con, new Profesor(), function, filterString, usemode, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields, contextRequest, fields.Split(char.Parse(",")).ToArray());
            }
        }

   protected override void QueryBuild(string predicate, Filter filter, DbContext efContext, ContextRequest contextRequest, string method, List<string> includesList)
      	{
				if (contextRequest.CustomQuery.SpecificProperties.Count == 0)
                {
					contextRequest.CustomQuery.SpecificProperties.Add(Profesor.PropertyNames.FullName);
					contextRequest.CustomQuery.SpecificProperties.Add(Profesor.PropertyNames.GuidAcademyLevel);
					contextRequest.CustomQuery.SpecificProperties.Add(Profesor.PropertyNames.GuidCompany);
					contextRequest.CustomQuery.SpecificProperties.Add(Profesor.PropertyNames.CreatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(Profesor.PropertyNames.UpdatedDate);
					contextRequest.CustomQuery.SpecificProperties.Add(Profesor.PropertyNames.CreatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(Profesor.PropertyNames.UpdatedBy);
					contextRequest.CustomQuery.SpecificProperties.Add(Profesor.PropertyNames.Bytes);
					contextRequest.CustomQuery.SpecificProperties.Add(Profesor.PropertyNames.IsDeleted);
					contextRequest.CustomQuery.SpecificProperties.Add(Profesor.PropertyNames.AcademyLevel);
                    
				}

				if (method == "getby" || method == "sum")
				{
					if (!contextRequest.CustomQuery.SpecificProperties.Contains("GuidProfesor")){
						contextRequest.CustomQuery.SpecificProperties.Add("GuidProfesor");
					}

					 if (!string.IsNullOrEmpty(contextRequest.CustomQuery.OrderBy))
					{
						string existPropertyOrderBy = contextRequest.CustomQuery.OrderBy;
						if (contextRequest.CustomQuery.OrderBy.Contains("."))
						{
							existPropertyOrderBy = contextRequest.CustomQuery.OrderBy.Split(char.Parse("."))[0];
						}
						if (!contextRequest.CustomQuery.SpecificProperties.Exists(p => p == existPropertyOrderBy))
						{
							contextRequest.CustomQuery.SpecificProperties.Add(existPropertyOrderBy);
						}
					}

				}
				
	bool isFullDetails = contextRequest.IsFromUI("Profesors", UIActions.GetForDetails);
	string filterForTest = predicate  + filter.GetFilterComplete();

				if (isFullDetails || !string.IsNullOrEmpty(predicate))
            {
            } 

			if (method == "sum")
            {
            } 
			if (contextRequest.CustomQuery.SelectedFields.Count == 0)
            {
				foreach (var selected in contextRequest.CustomQuery.SpecificProperties)
                {
					string linq = selected;
					switch (selected)
                    {

					case "AcademyLevel":
					if (includesList.Contains(selected)){
                        linq = "it.AcademyLevel as AcademyLevel";
					}
                    else
						linq = "iif(it.AcademyLevel != null, new (it.AcademyLevel.GuidAcademyLevel, it.AcademyLevel.Title), null) as AcademyLevel";
 					break;
					 
						
					 default:
                            break;
                    }
					contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name=selected, Linq=linq});
					if (method == "getby" || method == "sum")
					{
						if (includesList.Contains(selected))
							includesList.Remove(selected);

					}

				}
			}
				if (method == "getby" || method == "sum")
				{
					foreach (var otherInclude in includesList.Where(p=> !string.IsNullOrEmpty(p)))
					{
						contextRequest.CustomQuery.SelectedFields.Add(new SelectedField() { Name = otherInclude, Linq = "it." + otherInclude +" as " + otherInclude });
					}
				}
				BusinessRulesEventArgs<Profesor> e = null;
				if (contextRequest.PreventInterceptors == false)
					OnQuerySettings(efContext, e = new BusinessRulesEventArgs<Profesor>() { Filter = filter, ContextRequest = contextRequest /*, FilterExpressionString = (contextRequest != null ? (contextRequest.CustomQuery != null ? contextRequest.CustomQuery.FilterExpressionString : null) : null)*/ });

				//List<Profesor> result = new List<Profesor>();
                 if (e != null)
                {
                    contextRequest = e.ContextRequest;
                }

}
		public List<Profesor> GetBy(Expression<Func<Profesor, bool>> predicate, bool loadRelations, ContextRequest contextRequest)
        {
			if(!loadRelations)
				return GetBy(predicate, contextRequest);
			else
				return GetBy(predicate, contextRequest, "Students");

        }

        public List<Profesor> GetBy(Expression<Func<Profesor, bool>> predicate, int? pageSize, int? page, string orderBy, SFSdotNet.Framework.Data.SortDirection? sortDirection)
        {
            return GetBy(predicate, new ContextRequest() { CustomQuery = new CustomQuery() { Page = page, PageSize = pageSize, OrderBy = orderBy, SortDirection = sortDirection } });
        }
        public List<Profesor> GetBy(Expression<Func<Profesor, bool>> predicate)
        {

			if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: GetBy");
            }
			ContextRequest contextRequest = new ContextRequest();
            contextRequest.CustomQuery = new CustomQuery();
			contextRequest.CurrentContext = SFSdotNet.Framework.My.Context.CurrentContext;
			            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

            contextRequest.CustomQuery.FilterExpressionString = null;
            return this.GetBy(predicate, contextRequest, "");
        }
        #endregion
        #region Dynamic String
		protected override string GetSpecificFilter(string filter, ContextRequest contextRequest) {
            string result = "";
		    //string linqFilter = String.Empty;
            string freeTextFilter = String.Empty;
            if (filter.Contains("|"))
            {
               // linqFilter = filter.Split(char.Parse("|"))[0];
                freeTextFilter = filter.Split(char.Parse("|"))[1];
            }
            //else {
            //    freeTextFilter = filter;
            //}
            //else {
            //    linqFilter = filter;
            //}
			// linqFilter = SFSdotNet.Framework.Linq.Utils.ReplaceCustomDateFilters(linqFilter);
            //string specificFilter = linqFilter;
            if (!string.IsNullOrEmpty(freeTextFilter))
            {
                System.Text.StringBuilder sbCont = new System.Text.StringBuilder();
                /*if (specificFilter.Length > 0)
                {
                    sbCont.Append(" AND ");
                    sbCont.Append(" ({0})");
                }
                else
                {
                    sbCont.Append("{0}");
                }*/
                //var words = freeTextFilter.Split(char.Parse(" "));
				var word = freeTextFilter;
                System.Text.StringBuilder sbSpec = new System.Text.StringBuilder();
                 int nWords = 1;
				/*foreach (var word in words)
                {
					if (word.Length > 0){
                    if (sbSpec.Length > 0) sbSpec.Append(" AND ");
					if (words.Length > 1) sbSpec.Append("("); */
					
	
					
					
					
									
					sbSpec.Append(string.Format(@"FullName.Contains(""{0}"")", word));
					

					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
	
					
								sbSpec.Append(" OR ");
					
					//if (sbSpec.Length > 2)
					//	sbSpec.Append(" OR "); // test
					sbSpec.Append(string.Format(@"it.AcademyLevel.Title.Contains(""{0}"")", word));
								 //sbSpec.Append("*extraFreeText*");

                    /*if (words.Length > 1) sbSpec.Append(")");
					
					nWords++;

					}

                }*/
                //specificFilter = string.Format("{0}{1}", specificFilter, string.Format(sbCont.ToString(), sbSpec.ToString()));
                                 result = sbSpec.ToString();  
            }
			//result = specificFilter;
			
			return result;

		}
	
			public List<Profesor> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  params object[] extraParams)
        {
			return GetBy(filter, pageSize, page, orderBy, orderDir,  null, extraParams);
		}
           public List<Profesor> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir, string usemode, params object[] extraParams)
            { 
                return GetBy(filter, pageSize, page, orderBy, orderDir, usemode, null, extraParams);
            }


		public List<Profesor> GetBy(string filter, int? pageSize, int? page, string orderBy, string orderDir,  string usemode, ContextRequest context, params object[] extraParams)

        {

            // string freetext = null;
            //if (filter.Contains("|"))
            //{
            //    int parts = filter.Split(char.Parse("|")).Count();
            //    if (parts > 1)
            //    {

            //        freetext = filter.Split(char.Parse("|"))[1];
            //    }
            //}
		
            //string specificFilter = "";
            //if (!string.IsNullOrEmpty(filter))
            //  specificFilter=  GetSpecificFilter(filter);
            if (string.IsNullOrEmpty(orderBy))
            {
			                orderBy = "UpdatedDate";
            }
			//orderDir = "desc";
			SFSdotNet.Framework.Data.SortDirection direction = SFSdotNet.Framework.Data.SortDirection.Ascending;
            if (!string.IsNullOrEmpty(orderDir))
            {
                if (orderDir == "desc")
                    direction = SFSdotNet.Framework.Data.SortDirection.Descending;
            }
            if (context == null)
                context = new ContextRequest();
			

             context.UseMode = usemode;
             if (context.CustomQuery == null )
                context.CustomQuery =new SFSdotNet.Framework.My.CustomQuery();

 
                context.CustomQuery.ExtraParams = extraParams;

                    context.CustomQuery.OrderBy = orderBy;
                   context.CustomQuery.SortDirection = direction;
                   context.CustomQuery.Page = page;
                  context.CustomQuery.PageSize = pageSize;
               

            

            if (!preventSecurityRestrictions) {
			 if (context.CurrentContext == null)
                {
					if (SFSdotNet.Framework.My.Context.CurrentContext != null &&  SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
					{
						context.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
						context.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

					}
					else {
						throw new Exception("The security rule require a specific user and company");
					}
				}
            }
            return GetBy(filter, context);
  
        }


        public List<Profesor> GetBy(string strWhere, ContextRequest contextRequest)
        {
        	#region old code
				
				 //Expression<Func<tvsReservationTransport, bool>> predicate = null;
				string strWhereClean = strWhere.Replace("*extraFreeText*", "").Replace("()", "");
                //if (!string.IsNullOrEmpty(strWhereClean)){

                //    object[] extraParams = null;
                //    //if (contextRequest != null )
                //    //    if (contextRequest.CustomQuery != null )
                //    //        extraParams = contextRequest.CustomQuery.ExtraParams;
                //    //predicate = System.Linq.Dynamic.DynamicExpression.ParseLambda<tvsReservationTransport, bool>(strWhereClean, extraParams != null? extraParams.Cast<Guid>(): null);				
                //}
				 if (contextRequest == null)
                {
                    contextRequest = new ContextRequest();
                    if (contextRequest.CustomQuery == null)
                        contextRequest.CustomQuery = new CustomQuery();
                }
                  if (!preventSecurityRestrictions) {
					if (contextRequest.User == null || contextRequest.Company == null)
                      {
                     if (SFSdotNet.Framework.My.Context.CurrentContext.Company != null && SFSdotNet.Framework.My.Context.CurrentContext.User != null)
                     {
                         contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                         contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

                     }
                     else {
                         throw new Exception("The security rule require a specific User and Company ");
                     }
					 }
                 }
            contextRequest.CustomQuery.FilterExpressionString = strWhere;
				//return GetBy(predicate, contextRequest);  

			#endregion				
				
                    return GetBy(strWhere, contextRequest, "");  


        }
       public List<Profesor> GetBy(string strWhere)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
			
            return GetBy(strWhere, context, null);
        }

        public List<Profesor> GetBy(string strWhere, string includes)
        {
		 	ContextRequest context = new ContextRequest();
            context.CustomQuery = new CustomQuery();
            context.CustomQuery.FilterExpressionString = strWhere;
            return GetBy(strWhere, context, includes);
        }

        #endregion
        #endregion
		
		  #region SaveOrUpdate
        
 		 public Profesor Create(Profesor entity)
        {
				//ObjectContext context = null;
				    if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: Create");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

				return this.Create(entity, contextRequest);


        }
        
       
        public Profesor Create(Profesor entity, ContextRequest contextRequest)
        {
		
		bool graph = false;
	
				bool preventPartial = false;
                if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
               
			using (EFContext con = new EFContext()) {

				Profesor itemForSave = new Profesor();
#region Autos
		if(!preventSecurityRestrictions){

				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion
               BusinessRulesEventArgs<Profesor> e = null;
			    if (preventPartial == false )
                OnCreating(this,e = new BusinessRulesEventArgs<Profesor>() { ContextRequest = contextRequest, Item=entity });
				   if (e != null) {
						if (e.Cancel)
						{
							context = null;
							return e.Item;

						}
					}

                    if (entity.GuidProfesor == Guid.Empty)
                   {
                       entity.GuidProfesor = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   itemForSave.GuidProfesor = entity.GuidProfesor;
				  
		
			itemForSave.GuidProfesor = entity.GuidProfesor;

			itemForSave.FullName = entity.FullName;

			itemForSave.GuidCompany = entity.GuidCompany;

			itemForSave.CreatedDate = entity.CreatedDate;

			itemForSave.UpdatedDate = entity.UpdatedDate;

			itemForSave.CreatedBy = entity.CreatedBy;

			itemForSave.UpdatedBy = entity.UpdatedBy;

			itemForSave.Bytes = entity.Bytes;

			itemForSave.IsDeleted = entity.IsDeleted;

				
				con.Profesors.Add(itemForSave);



					if (entity.AcademyLevel != null)
					{
						var academyLevel = new AcademyLevel();
						academyLevel.GuidAcademyLevel = entity.AcademyLevel.GuidAcademyLevel;
						itemForSave.AcademyLevel = academyLevel;
						SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<AcademyLevel>(con, itemForSave.AcademyLevel);
			
					}





                
				con.ChangeTracker.Entries().Where(p => p.Entity != itemForSave && p.State != EntityState.Unchanged).ForEach(p => p.State = EntityState.Detached);

				con.Entry<Profesor>(itemForSave).State = EntityState.Added;

				con.SaveChanges();

					 
				

				//itemResult = entity;
                //if (e != null)
                //{
                 //   e.Item = itemResult;
                //}
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false )
                OnCreated(this, e == null ? e = new BusinessRulesEventArgs<Profesor>() { ContextRequest = contextRequest, Item = entity } : e);



                if (e != null && e.Item != null )
                {
                    return e.Item;
                }
                              return entity;
			}
            
        }
        //BusinessRulesEventArgs<Profesor> e = null;
        public void Create(List<Profesor> entities)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            Create(entities, contextRequest);
        }
        public void Create(List<Profesor> entities, ContextRequest contextRequest)
        
        {
			ObjectContext context = null;
            	foreach (Profesor entity in entities)
				{
					this.Create(entity, contextRequest);
				}
        }
		  public void CreateOrUpdateBulk(List<Profesor> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "cu", contextRequest);
        }

        private void CreateOrUpdateBulk(List<Profesor> entities, string actionKey, ContextRequest contextRequest)
        {
			if (entities.Count() > 0){
            bool graph = false;

            bool preventPartial = false;
            if (contextRequest != null && contextRequest.PreventInterceptors == true)
            {
                preventPartial = true;
            }
            foreach (var entity in entities)
            {
                    if (entity.GuidProfesor == Guid.Empty)
                   {
                       entity.GuidProfesor = SFSdotNet.Framework.Utilities.UUID.NewSequential();
					   
                   }
				   
				  


#region Autos
		if(!preventSecurityRestrictions){


 if (actionKey != "u")
                        {
				if (entity.CreatedDate == null )
			entity.CreatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.CreatedBy = contextRequest.User.GuidUser;


}
				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	
			if (contextRequest != null)
				if(contextRequest.User != null)
					if (contextRequest.Company != null)
						entity.GuidCompany = contextRequest.Company.GuidCompany;
	


			}
#endregion


		
			//entity.GuidProfesor = entity.GuidProfesor;

			//entity.FullName = entity.FullName;

			//entity.GuidCompany = entity.GuidCompany;

			//entity.CreatedDate = entity.CreatedDate;

			//entity.UpdatedDate = entity.UpdatedDate;

			//entity.CreatedBy = entity.CreatedBy;

			//entity.UpdatedBy = entity.UpdatedBy;

			//entity.Bytes = entity.Bytes;

			//entity.IsDeleted = entity.IsDeleted;

				
				



				    if (entity.AcademyLevel != null)
					{
						//var academyLevel = new AcademyLevel();
						entity.GuidAcademyLevel = entity.AcademyLevel.GuidAcademyLevel;
						//entity.AcademyLevel = academyLevel;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<AcademyLevel>(con, itemForSave.AcademyLevel);
			
					}





                
				

					 
				

				//itemResult = entity;
            }
            using (EFContext con = new EFContext())
            {
                 if (actionKey == "c")
                    {
                        context.BulkInsert(entities);
                    }else if ( actionKey == "u")
                    {
                        context.BulkUpdate(entities);
                    }else
                    {
                        context.BulkInsertOrUpdate(entities);
                    }
            }

			}
        }
	
		public void CreateBulk(List<Profesor> entities, ContextRequest contextRequest)
        {
            CreateOrUpdateBulk(entities, "c", contextRequest);
        }


		public void UpdateAgile(Profesor item, params string[] fields)
         {
			UpdateAgile(item, null, fields);
        }
		public void UpdateAgile(Profesor item, ContextRequest contextRequest, params string[] fields)
         {
            
             ContextRequest contextNew = null;
             if (contextRequest != null)
             {
                 contextNew = SFSdotNet.Framework.My.Context.BuildContextRequestCopySafe(contextRequest);
                 if (fields != null && fields.Length > 0)
                 {
                     contextNew.CustomQuery.SpecificProperties  = fields.ToList();
                 }
                 else if(contextRequest.CustomQuery.SpecificProperties.Count > 0)
                 {
                     fields = contextRequest.CustomQuery.SpecificProperties.ToArray();
                 }
             }
			

		   using (EFContext con = new EFContext())
            {



               
					List<string> propForCopy = new List<string>();
                    propForCopy.AddRange(fields);
                    
					  
					if (!propForCopy.Contains("GuidProfesor"))
						propForCopy.Add("GuidProfesor");

					var itemForUpdate = SFSdotNet.Framework.BR.Utils.GetConverted<Profesor,Profesor>(item, propForCopy.ToArray());
					 itemForUpdate.GuidProfesor = item.GuidProfesor;
                  var setT = con.Set<Profesor>().Attach(itemForUpdate);

					if (fields.Count() > 0)
					  {
						  item.ModifiedProperties = fields;
					  }
                    foreach (var property in item.ModifiedProperties)
					{						
                        if (property != "GuidProfesor")
                             con.Entry(setT).Property(property).IsModified = true;

                    }

                
               int result = con.SaveChanges();
               if (result != 1)
               {
                   SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: 1");
               }


            }

			OnUpdatedAgile(this, new BusinessRulesEventArgs<Profesor>() { Item = item, ContextRequest = contextNew  });

         }
		public void UpdateBulk(List<Profesor>  items, params string[] fields)
         {
             SFSdotNet.Framework.My.ContextRequest req = new SFSdotNet.Framework.My.ContextRequest();
             req.CustomQuery = new SFSdotNet.Framework.My.CustomQuery();
             foreach (var field in fields)
             {
                 req.CustomQuery.SpecificProperties.Add(field);
             }
             UpdateBulk(items, req);

         }

		 public void DeleteBulk(List<Profesor> entities, ContextRequest contextRequest = null)
        {

            using (EFContext con = new EFContext())
            {
                foreach (var entity in entities)
                {
					var entityProxy = new Profesor() { GuidProfesor = entity.GuidProfesor };

                    con.Entry<Profesor>(entityProxy).State = EntityState.Deleted;

                }

                int result = con.SaveChanges();
                if (result != entities.Count)
                {
                    SFSdotNet.Framework.My.EventLog.Error("Has been changed " + result.ToString() + " items but the expected value is: " + entities.Count.ToString());
                }
            }

        }

        public void UpdateBulk(List<Profesor> items, ContextRequest contextRequest)
        {
            if (items.Count() > 0){

			 foreach (var entity in items)
            {


#region Autos
		if(!preventSecurityRestrictions){

				if (entity.UpdatedDate == null )
			entity.UpdatedDate = DateTime.Now.ToUniversalTime();
		if(contextRequest.User != null)
			entity.UpdatedBy = contextRequest.User.GuidUser;
	



			}
#endregion




				    if (entity.AcademyLevel != null)
					{
						//var academyLevel = new AcademyLevel();
						entity.GuidAcademyLevel = entity.AcademyLevel.GuidAcademyLevel;
						//entity.AcademyLevel = academyLevel;
						//SFSdotNet.Framework.BR.Utils.TryAttachFKRelation<AcademyLevel>(con, itemForSave.AcademyLevel);
			
					}





				}
				using (EFContext con = new EFContext())
				{

                    
                
                   con.BulkUpdate(items);

				}
             
			}	  
        }

         public Profesor Update(Profesor entity)
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Create");
            }

            ContextRequest contextRequest = new ContextRequest();
            contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
            contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return Update(entity, contextRequest);
        }
       
         public Profesor Update(Profesor entity, ContextRequest contextRequest)
        {
		 if ((System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null) && contextRequest == null)
            {
                throw new Exception("Please, specific the contextRequest parameter in the method: Update");
            }
            if (contextRequest == null)
            {
                contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            }

			
				Profesor  itemResult = null;

	
			//entity.UpdatedDate = DateTime.Now.ToUniversalTime();
			//if(contextRequest.User != null)
				//entity.UpdatedBy = contextRequest.User.GuidUser;

//	    var oldentity = GetBy(p => p.GuidProfesor == entity.GuidProfesor, contextRequest).FirstOrDefault();
	//	if (oldentity != null) {
		
          //  entity.CreatedDate = oldentity.CreatedDate;
    //        entity.CreatedBy = oldentity.CreatedBy;
	
      //      entity.GuidCompany = oldentity.GuidCompany;
	
			

	
		//}

			 using( EFContext con = new EFContext()){
				BusinessRulesEventArgs<Profesor> e = null;
				bool preventPartial = false; 
				if (contextRequest != null && contextRequest.PreventInterceptors == true )
                {
                    preventPartial = true;
                } 
				if (preventPartial == false)
                OnUpdating(this,e = new BusinessRulesEventArgs<Profesor>() { ContextRequest = contextRequest, Item=entity});
				   if (e != null) {
						if (e.Cancel)
						{
							//outcontext = null;
							return e.Item;

						}
					}

	string includes = "AcademyLevel";
	IQueryable < Profesor > query = con.Profesors.AsQueryable();
	foreach (string include in includes.Split(char.Parse(",")))
                       {
                           if (!string.IsNullOrEmpty(include))
                               query = query.Include(include);
                       }
	var oldentity = query.FirstOrDefault(p => p.GuidProfesor == entity.GuidProfesor);
	if (oldentity.FullName != entity.FullName)
		oldentity.FullName = entity.FullName;

				//if (entity.UpdatedDate == null || (contextRequest != null && contextRequest.IsFromUI("Profesors", UIActions.Updating)))
			oldentity.UpdatedDate = DateTime.Now.ToUniversalTime();
			if(contextRequest.User != null)
				oldentity.UpdatedBy = contextRequest.User.GuidUser;

           


						if (SFSdotNet.Framework.BR.Utils.HasRelationPropertyChanged(oldentity.AcademyLevel, entity.AcademyLevel, "GuidAcademyLevel"))
							oldentity.AcademyLevel = entity.AcademyLevel != null? new AcademyLevel(){ GuidAcademyLevel = entity.AcademyLevel.GuidAcademyLevel } :null;

                


				if (entity.Students != null)
                {
                    foreach (var item in entity.Students)
                    {


                        
                    }
					
                    

                }


				con.ChangeTracker.Entries().Where(p => p.Entity != oldentity).ForEach(p => p.State = EntityState.Unchanged);  
				  
				con.SaveChanges();
        
					 
					
               
				itemResult = entity;
				if(preventPartial == false)
					OnUpdated(this, e = new BusinessRulesEventArgs<Profesor>() { ContextRequest = contextRequest, Item=itemResult });

              	return itemResult;
			}
			  
        }
        public Profesor Save(Profesor entity)
        {
			return Create(entity);
        }
        public int Save(List<Profesor> entities)
        {
			 Create(entities);
            return entities.Count;

        }
        #endregion
        #region Delete
        public void Delete(Profesor entity)
        {
				this.Delete(entity, null);
			
        }
		 public void Delete(Profesor entity, ContextRequest contextRequest)
        {
				
				  List<Profesor> entities = new List<Profesor>();
				   entities.Add(entity);
				this.Delete(entities, contextRequest);
			
        }

         public void Delete(string query, Guid[] guids, ContextRequest contextRequest)
        {
			var br = new ProfesorsBR(true);
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);
            
            Delete(items, contextRequest);

        }
        public void Delete(Profesor entity,  ContextRequest contextRequest, BusinessRulesEventArgs<Profesor> e = null)
        {
			
				using(EFContext con = new EFContext())
                 {
				
               	BusinessRulesEventArgs<Profesor> _e = null;
               List<Profesor> _items = new List<Profesor>();
                _items.Add(entity);
                if (e == null || e.PreventPartialPropagate == false)
                {
                    OnDeleting(this, _e = (e == null ? new BusinessRulesEventArgs<Profesor>() { ContextRequest = contextRequest, Item = entity, Items = null  } : e));
                }
                if (_e != null)
                {
                    if (_e.Cancel)
						{
							context = null;
							return;

						}
					}


				
									//IsDeleted
					bool logicDelete = true;
					if (entity.IsDeleted != null)
					{
						if (entity.IsDeleted.Value)
							logicDelete = false;
					}
					if (logicDelete)
					{
											//entity = GetBy(p =>, contextRequest).FirstOrDefault();
						entity.IsDeleted = true;
						if (contextRequest != null && contextRequest.User != null)
							entity.UpdatedBy = contextRequest.User.GuidUser;
                        entity.UpdatedDate = DateTime.UtcNow;
						UpdateAgile(entity, "IsDeleted","UpdatedBy","UpdatedDate");

						
					}
					else {
					con.Entry<Profesor>(entity).State = EntityState.Deleted;
					con.SaveChanges();
				
				 
					}
								
				
				 
					
					
			if (e == null || e.PreventPartialPropagate == false)
                {

                    if (_e == null)
                        _e = new BusinessRulesEventArgs<Profesor>() { ContextRequest = contextRequest, Item = entity, Items = null };

                    OnDeleted(this, _e);
                }

				//return null;
			}
        }
 public void UnDelete(string query, Guid[] guids, ContextRequest contextRequest)
        {
            var br = new ProfesorsBR(true);
            contextRequest.CustomQuery.IncludeDeleted = true;
            var items = br.GetBy(query, null, null, null, null, null, contextRequest, guids);

            foreach (var item in items)
            {
                item.IsDeleted = false;
						if (contextRequest != null && contextRequest.User != null)
							item.UpdatedBy = contextRequest.User.GuidUser;
                        item.UpdatedDate = DateTime.UtcNow;
            }

            UpdateBulk(items, "IsDeleted","UpdatedBy","UpdatedDate");
        }

         public void Delete(List<Profesor> entities,  ContextRequest contextRequest = null )
        {
				
			 BusinessRulesEventArgs<Profesor> _e = null;

                OnDeleting(this, _e = new BusinessRulesEventArgs<Profesor>() { ContextRequest = contextRequest, Item = null, Items = entities });
                if (_e != null)
                {
                    if (_e.Cancel)
                    {
                        context = null;
                        return;

                    }
                }
                bool allSucced = true;
                BusinessRulesEventArgs<Profesor> eToChilds = new BusinessRulesEventArgs<Profesor>();
                if (_e != null)
                {
                    eToChilds = _e;
                }
                else
                {
                    eToChilds = new BusinessRulesEventArgs<Profesor>() { ContextRequest = contextRequest, Item = (entities.Count == 1 ? entities[0] : null), Items = entities };
                }
				foreach (Profesor item in entities)
				{
					try
                    {
                        this.Delete(item, contextRequest, e: eToChilds);
                    }
                    catch (Exception ex)
                    {
                        SFSdotNet.Framework.My.EventLog.Error(ex);
                        allSucced = false;
                    }
				}
				if (_e == null)
                    _e = new BusinessRulesEventArgs<Profesor>() { ContextRequest = contextRequest, CountResult = entities.Count, Item = null, Items = entities };
                OnDeleted(this, _e);

			
        }
        #endregion
 
        #region GetCount
		 public int GetCount(Expression<Func<Profesor, bool>> predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;

			return GetCount(predicate, contextRequest);
		}
        public int GetCount(Expression<Func<Profesor, bool>> predicate, ContextRequest contextRequest)
        {


		
		 using (EFContext con = new EFContext())
            {


				if (predicate == null) predicate = PredicateBuilder.True<Profesor>();
           		predicate = predicate.And(p => p.IsDeleted != true || p.IsDeleted == null);
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    		if (contextRequest.User !=null )
                        		if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
									predicate = predicate.And(p => p.GuidCompany == contextRequest.Company.GuidCompany); //todo: multiempresa

								}
						}
						if (preventSecurityRestrictions) preventSecurityRestrictions= false;
				
				IQueryable<Profesor> query = con.Profesors.AsQueryable();
                return query.AsExpandable().Count(predicate);

			
				}
			

        }
		  public int GetCount(string predicate,  ContextRequest contextRequest)
         {
             return GetCount(predicate, null, contextRequest);
         }

         public int GetCount(string predicate)
        {
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
            return GetCount(predicate, contextRequest);
        }
		 public int GetCount(string predicate, string usemode){
				if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session  == null){
                    throw new Exception("Please, specific the contextRequest parameter in the method: GetCount");
                }

                ContextRequest contextRequest = new ContextRequest();
                contextRequest.User = SFSdotNet.Framework.My.Context.CurrentContext.User;
                contextRequest.Company = SFSdotNet.Framework.My.Context.CurrentContext.Company;
				return GetCount( predicate,  usemode,  contextRequest);
		 }
        public int GetCount(string predicate, string usemode, ContextRequest contextRequest){

		using (EFContext con = new EFContext()) {
				string computedFields = "";
				string fkIncludes = "AcademyLevel";
                List<string> multilangProperties = new List<string>();
				//if (predicate == null) predicate = PredicateBuilder.True<Profesor>();
                var notDeletedExpression = "(IsDeleted != true OR IsDeleted = null)";
				string isDeletedField = "IsDeleted";
	
					bool sharedAndMultiTenant = false;	  
					string multitenantExpression = null;
				if (contextRequest != null && contextRequest.Company != null)
				 {
                    multitenantExpression = @"(GuidCompany = @GuidCompanyMultiTenant)";
                    contextRequest.CustomQuery.SetParam("GuidCompanyMultiTenant", new Nullable<Guid>(contextRequest.Company.GuidCompany));
                }
					 									
					string multiTenantField = "GuidCompany";

                
                return GetCount(con, predicate, usemode, contextRequest, multilangProperties, multiTenantField, isDeletedField, sharedAndMultiTenant, notDeletedExpression, multitenantExpression, computedFields);

			}
			#region old code
			 /* string freetext = null;
            Filter filter = new Filter();

              if (predicate.Contains("|"))
              {
                 
                  filter.SetFilterPart("ft", GetSpecificFilter(predicate, contextRequest));
                 
                  filter.ProcessText(predicate.Split(char.Parse("|"))[0]);
                  freetext = predicate.Split(char.Parse("|"))[1];

				  if (!string.IsNullOrEmpty(freetext) && string.IsNullOrEmpty(contextRequest.FreeText))
                  {
                      contextRequest.FreeText = freetext;
                  }
              }
              else {
                  filter.ProcessText(predicate);
              }
			   predicate = filter.GetFilterComplete();
			// BusinessRulesEventArgs<Profesor>  e = null;
           	using (EFContext con = new EFContext())
			{
			
			

			 QueryBuild(predicate, filter, con, contextRequest, "count", new List<string>());


			
			BusinessRulesEventArgs<Profesor> e = null;

			contextRequest.FreeText = freetext;
			contextRequest.UseMode = usemode;
            OnCounting(this, e = new BusinessRulesEventArgs<Profesor>() {  Filter =filter, ContextRequest = contextRequest });
            if (e != null)
            {
                if (e.Cancel)
                {
                    context = null;
                    return e.CountResult;

                }

            

            }
			
			StringBuilder sbQuerySystem = new StringBuilder();
		
					
                    filter.SetFilterPart("de","(IsDeleted != true OR IsDeleted == null)");
			
					if (!preventSecurityRestrictions)
						{
						if (contextRequest != null )
                    	if (contextRequest.User !=null )
                        	if (contextRequest.Company != null && contextRequest.CustomQuery.IncludeAllCompanies == false){
                        		
								filter.SetFilterPart("co", @"(GuidCompany = Guid(""" + contextRequest.Company.GuidCompany + @""")) "); //todo: multiempresa
						
						
							}
							
							}
							if (preventSecurityRestrictions) preventSecurityRestrictions= false;
		
				   
                 filter.CleanAndProcess("");
				//string predicateWithFKAndComputed = SFSdotNet.Framework.Linq.Utils.ExtractSpecificProperties("", ref predicate );               
				string predicateWithFKAndComputed = filter.GetFilterParentAndCoumputed();
               string predicateWithManyRelations = filter.GetFilterChildren();
			   ///QueryUtils.BreakeQuery1(predicate, ref predicateWithManyRelations, ref predicateWithFKAndComputed);
			   predicate = filter.GetFilterComplete();
               if (!string.IsNullOrEmpty(predicate))
               {
				
					
                    return con.Profesors.Where(predicate).Count();
					
                }else
                    return con.Profesors.Count();
					
			}*/
			#endregion

		}
         public int GetCount()
        {
            return GetCount(p => true);
        }
        #endregion
        
         


        public void Delete(List<Profesor.CompositeKey> entityKeys)
        {

            List<Profesor> items = new List<Profesor>();
            foreach (var itemKey in entityKeys)
            {
                items.Add(GetByKey(itemKey.GuidProfesor));
            }

            Delete(items);

        }
		 public void UpdateAssociation(string relation, string relationValue, string query, Guid[] ids, ContextRequest contextRequest)
        {
            var items = GetBy(query, null, null, null, null, null, contextRequest, ids);
			 var module = SFSdotNet.Framework.Cache.Caching.SystemObjects.GetModuleByKey(SFSdotNet.Framework.Web.Utils.GetRouteDataOrQueryParam(System.Web.HttpContext.Current.Request.RequestContext, "area"));
           
            foreach (var item in items)
            {
			  Guid ? guidRelationValue = null ;
                if (!string.IsNullOrEmpty(relationValue)){
                    guidRelationValue = Guid.Parse(relationValue );
                }

				 if (relation.Contains("."))
                {
                    var partsWithOtherProp = relation.Split(char.Parse("|"));
                    var parts = partsWithOtherProp[0].Split(char.Parse("."));

                    string proxyRelName = parts[0];
                    string proxyProperty = parts[1];
                    string proxyPropertyKeyNameFromOther = partsWithOtherProp[1];
                    //string proxyPropertyThis = parts[2];

                    var prop = item.GetType().GetProperty(proxyRelName);
                    //var entityInfo = //SFSdotNet.Framework.
                    // descubrir el tipo de entidad dentro de la colección
                    Type typeEntityInList = SFSdotNet.Framework.Entities.Utils.GetTypeFromList(prop);
                    var newProxyItem = Activator.CreateInstance(typeEntityInList);
                    var propThisForSet = newProxyItem.GetType().GetProperty(proxyProperty);
                    var entityInfoOfProxy = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(typeEntityInList);
                    var propOther = newProxyItem.GetType().GetProperty(proxyPropertyKeyNameFromOther);

                    if (propThisForSet != null && entityInfoOfProxy != null && propOther != null )
                    {
                        var entityInfoThis = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(item.GetType());
                        var valueThisId = item.GetType().GetProperty(entityInfoThis.PropertyKeyName).GetValue(item);
                        if (valueThisId != null)
                            propThisForSet.SetValue(newProxyItem, valueThisId);
                        propOther.SetValue(newProxyItem, Guid.Parse(relationValue));
                        
                        var entityNameProp = newProxyItem.GetType().GetField("EntityName").GetValue(null);
                        var entitySetNameProp = newProxyItem.GetType().GetField("EntitySetName").GetValue(null);

                        SFSdotNet.Framework.Apps.Integration.CreateItemFromApp(entityNameProp.ToString(), entitySetNameProp.ToString(), module.ModuleNamespace, newProxyItem, contextRequest);

                    }

                    // crear una instancia del tipo de entidad
                    // llenar los datos y registrar nuevo


                }
                else
                {
                var prop = item.GetType().GetProperty(relation);
                var entityInfo = SFSdotNet.Framework.Common.Entities.Metadata.MetadataAttributes.GetMyAttribute<SFSdotNet.Framework.Common.Entities.Metadata.EntityInfoAttribute>(prop.PropertyType);
                if (entityInfo != null)
                {
                    var ins = Activator.CreateInstance(prop.PropertyType);
                   if (guidRelationValue != null)
                    {
                        prop.PropertyType.GetProperty(entityInfo.PropertyKeyName).SetValue(ins, guidRelationValue);
                        item.GetType().GetProperty(relation).SetValue(item, ins);
                    }
                    else
                    {
                        item.GetType().GetProperty(relation).SetValue(item, null);
                    }

                    Update(item, contextRequest);
                }

				}
            }
        }
		
	}
	 
}


